# Commands
GCC = g++
# Flags
# GCCFLAGS = -I./src/include -fopenmp -Wall -DGRAPHS_OK -DDISPLAY_OK -DOLDINT_OK -O2
# GCCFLAGS = -I./src/include -fopenmp -Wall -DGRAPHS_OK -DDISPLAY_OK -ggdb -O0
# GCCFLAGS = -I./src/include -fopenmp -Wall -ggdb -O0
GCCFLAGS = -I./src/include -fopenmp -Wall -DDISPLAY_OK -DGRAPHS_OK -ggdb -O0
# GCCFLAGS = -I./src/include -fopenmp -DGRAPHS_OK -DDISPLAY_OK -Wall -Wvla
# GCCFLAGS = -I./src/include -fopenmp -Wall -DGRAPHS_OK -DDISPLAY_OK -O2
LFLAGS = -lpthread -lm -lncurses -ltinfo -lmgl-fltk -lmgl-qt5 -lmgl -lmatio -lz

# Directories
OBJECT_DIR = objs
# Objects
objects = $(patsubst src/%.c,$(OBJECT_DIR)/%.o,$(wildcard src/*.c))
out = test

# Build
all:	test

test:	$(objects)
	$(GCC) $(GCCFLAGS) $(LFLAGS) -o test $(objects)

$(OBJECT_DIR)/%.o:	src/%.c $(wildcard src/include/*.h)
			$(GCC) $(GCCFLAGS) -o $@ -c $<

delete: 
		rm -f *.mat ./data/CPU/*.dat ./data/GPU/*.dat ./LOG/*.dat ./TMP/*.dat ./GEOM/*.dat ./data/CPU/*.mat ./data/GPU/*.mat ./TMP/*.mat ./GEOM/*.mat

clean:
		rm -f $(OBJECT_DIR)/*.o

cleanest: 
		rm -f $(OBJECT_DIR)/*.o test client *.mat ./data/CPU/*.dat ./data/GPU/*.dat ./LOG/*.dat ./TMP/*.dat ./GEOM/*.dat ./data/CPU/*.mat ./data/GPU/*.mat ./TMP/*.mat ./GEOM/*.mat

client:		client_src/client.c
		c++ client_src/client.c -lpthread -lmgl-fltk -lmgl -o client
