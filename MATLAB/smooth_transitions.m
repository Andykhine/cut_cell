function smooth_transitions
%% Data
close all

dx2 = 1;
dx1 = 2;

r = dx2/dx1;

if (r < 0.8)
    disp('r < 0.8');
    dx0 = dx1;    
    b = 0.8;
elseif (r > 1.2)
    disp('r > 1.2');
    dx0 = dx1;
    b = 1.2;
end

% Original number of blocks that compose L
N = 10;
% This length must be preserved
L = dx0*(N*r);
%% Constraints
Nv = 0:0.01:N;
% Last dxN = rdx0, this in general cannot be achieved
G1 = @(N,b,r) b.^N - r;
% Length of section
G2 = @(N,b,r) (1+N*r)*(1-b) - (1-b.^(N+1));
% dG2 = @(N,b,r) r*(1-b) + b.^(N+1)*log(b);

% Vector of number of blocks, should be integer but we will floor its value
% as in Nreal = floor(Nsolved). This will give L > Lsolved, and we will
% proceed from there accordingly

%% Solve for N
% Solving for b given N
Nsolved = (b/r) * (1-r)/(1-b); % newtraph(G2, dG2, 20, 1e-4, 50, b, r);
Nfloor = floor(Nsolved);

% Array of dx
dx = dx0*ones(Nfloor+1,1);

% Constructing dx
for i=1:(Nfloor+1)
    dx(i) = dx0*(b^(i-1));
end

Lsolved = sum(dx);
DeltaL = L - Lsolved;

dx_tail = DeltaL/(N-Nfloor);
b_tail = dx_tail/dx(end);
%% Graphs
figure;
subplot(1,2,1)
hold on
plot([Nv(1) Nv(end)], [0 0], '--k');
plot(Nv, G1(Nv,b,r));
plot(Nv, G2(Nv,b,r));
hold off
xlabel('N');
ylabel('G(N)');
title(['r = ' num2str(r) ', b = ' num2str(b) ...
    ', Nsolved = ' num2str(Nsolved) ...
    ', L = ' num2str(L) ...
    ', L solved = ' num2str(sum(dx)) ...
    ', \DeltaL = ' num2str(DeltaL)]);
box on
legend('G(N) = 0', 'G_1(N)', 'G_2(N)');

subplot(1,2,2)
plot(0:Nfloor, dx, '-ob');
xlabel('I');
ylabel('dx');
title(['\Deltax tail = ' num2str(dx_tail) ...
    ', b tail = ' num2str(b_tail)]);
end

function xr = newtraph(func, dfunc, xr, es, maxit, varargin)
% newtraph : Newton - Raphson root location zeroes
% xr = newtraph (func , dfunc ,xr ,es , maxit ,p1 ,p2 ,...): 
% uses Newton - Raphson method to find the root of func
% input :
% func = name of function
% dfunc = name of derivative of function
% xr = initial guess
% es = desired relative error ( default = 0.0001 %)
% maxit = maximum allowable iterations ( default = 50)
% p1 ,p2 ,... = additional parameters used by function

%% VALORES POR DEFECTO
if (nargin < 1), func = @(x) sqrt((x*9.8)/(0.25))*tanh((sqrt((9.8*0.25)/x))*4)-36; end
if (nargin < 2), dfunc = @(x) (3.1305*tanh(6.26099/x^0.5))/x^0.5-(19.6*sech(6.26099/x^0.5)^2)/x; end
if (nargin < 3), xr=100; end
if (nargin < 4) || isempty (es), es = 0.0001; end
if (nargin < 5) || isempty (maxit), maxit = 50; end

fprintf('Método de Newton-Raphson\n\n')
iter = 0;
fprintf('%14s %14s %14s %14s\n', 'iter', 'xr', 'fval', 'ea');
 while (1)
xrold = xr;
xr = xr - func(xr, varargin{:})/dfunc(xr, varargin{:});
iter = iter + 1;
if (xr ~= 0), ea = abs (( xr - xrold )/ xr) * 100; end
if (ea <= es) 
   break
end
if ( iter >= maxit )
    error('Se ha alcanzado el número máximo de iteraciones, cambie el valor de xr'); 
end
fprintf('%14d %14f %14f %14f\n', iter, xr, func(xr, varargin{:}), ea);
 end
fprintf('\nLa raíz encontrada es \nroot = %f, con un error aproximado \nea = %e y se realizaron %d iteraciones\n', xr, ea, iter);
end