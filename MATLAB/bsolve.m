function fvalue = bsolve(x)
%BSOLVE Summary of this function goes here
%   Detailed explanation goes here
N = 4;
r = 2;
fvalue(1) = 1 + N*x(2)*x(1)^(N-1) + x(3)*((1+N)*x(1)^N - (1+N*r));
fvalue(2) = x(1)^N - r;
fvalue(3) = (1+N*r)*(1-x(1)) - (1-x(1).^(N+1));
end

