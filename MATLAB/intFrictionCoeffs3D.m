function CDy = intFrictionCoeffs3D
% Estimate forces over solid
% Returns CDy assuming that flow is in the y direction

%% Load data and consistency check

close all

load post_process.mat

plotFlag = [
    false % Esfuerzo dirección x
    false % Presión dirección x
    false % Esfuerzo dirección y
    false % Presión dirección y
    false % Esfuerzo dirección z
    false % Presión dirección z
    ];  % 

% Print input values to check for inconsistencies

fprintf("Solid center coordinates: %10.5f [m] %10.5f [m] %10.5f [m]\n", ...
  solid_center_coord);
fprintf("rho: %10.5f [kg/m^3] mu: %10.5f [Pa * s]\n", ...
  rho, mu);
fprintf("v_inf: %10.5f [m/s] D: %10.5f [m]\n", ...
  v_inf, D);
fprintf("Re: %10.5f\n", rho*v_inf*D / mu);

% Arrays to store results 

F = zeros(3, 1);
FP = zeros(3, 1);
CD = zeros(3, 1);
CP = zeros(3, 1);

validValues = zeros(3, 1);
area = zeros(3, 1);

%% U: tau interpolation

% Relative coordinates

xRel = u_sol_factor_inters_x - solid_center_coord(1);
yRel = u_sol_factor_inters_y - solid_center_coord(2);
zRel = u_sol_factor_inters_z - solid_center_coord(3);

% Compute angles over sphere surface

r = sqrt(xRel.^2 + yRel.^2 + zRel.^2);
theta_angle = atan2(yRel, xRel);
phi_angle = acos(zRel ./ r);

% Regular cells (type 0)

% Some cells have NAN values for _sol_factor_inters coordinates
% due to being cells with too small solid area

valid_vec = (~isnan(theta_angle)) & (~isnan(phi_angle));
validValues(1) = sum(valid_vec);
data_vec = (u_sol_factor_type == 0) & valid_vec;
int_vec = (~data_vec) & valid_vec;
    
thetaData = theta_angle(data_vec);
phiData = phi_angle(data_vec);
tauData = u_sol_factor_tau(data_vec);
thetaInt = theta_angle(int_vec);
phiInt = phi_angle(int_vec);

% The estimated data size is 2N in theta and N in phi
N = round(sqrt(size(theta_angle, 1) / 2));

% Interpolate on regular grid to add surf plot, for better visualization
[THETA, PHI] = meshgrid(linspace(min(theta_angle), max(theta_angle), 2*N), ...
    linspace(min(phi_angle), max(phi_angle), N));

% Evaluate interpolated tau values
tauInt = griddata(thetaData, phiData, tauData, ...
    thetaInt, phiInt, 'v4');
% TAU is used for better visualization of tau values
TAU = griddata(thetaData, phiData, tauData, ...
    THETA, PHI, 'v4');

if plotFlag(1)
  h1 = figure(1);
  box on
  hold on
  ph1(1) = plot3(thetaData, phiData, tauData, '.k');
  ph1(2) = plot3(thetaInt, phiInt, tauInt, '.r');
  surf(THETA, PHI, TAU)
  xlabel('theta')
  ylabel('phi')
  zlabel('\tau [Pa]')
  legend(ph1,'Available points', 'Interpolated points')
end

area(1) = sum(u_sol_factor_area(data_vec | int_vec));
F(1) = sum(u_sol_factor_area(data_vec) .* tauData) + ...
    sum(u_sol_factor_area(int_vec) .* tauInt);
CD(1) = F(1) / (0.5 * area(1) * rho * v_inf^2);

%% U: pressure interpolation

data_vec = ~(isnan(u_sol_factor_p)) & valid_vec;
int_vec = (~data_vec) & valid_vec;

thetaData = theta_angle(data_vec);
phiData = phi_angle(data_vec);
pData = u_sol_factor_p(data_vec);
thetaInt = theta_angle(int_vec);
phiInt = phi_angle(int_vec);

% Evaluate interpolated values
pInt = griddata(thetaData, phiData, pData, ...
    thetaInt, phiInt, 'v4');
P = griddata(thetaData, phiData, pData, ...
    THETA, PHI, 'v4');

if plotFlag(2)
  h2 = figure(2);
  box on
  hold on
  ph2(1) = plot3(thetaData, phiData, pData, '.k');
  ph2(2) = plot3(thetaInt, phiInt, pInt, '.r');
  surf(THETA, PHI, P)
  xlabel('theta')
  ylabel('phi')
  zlabel('p [Pa]')
  legend(ph2,'Available points', 'Interpolated points')
end

FP(1) = -(sum(u_p_factor(data_vec) .* pData) + ...
    sum(u_p_factor(int_vec) .* pInt));
CP(1) = FP(1) / (0.5 * area(1) * rho * v_inf^2);

%% V: tau interpolation

% Relative coordinates

xRel = v_sol_factor_inters_x - solid_center_coord(1);
yRel = v_sol_factor_inters_y - solid_center_coord(2);
zRel = v_sol_factor_inters_z - solid_center_coord(3);

% Compute angles over sphere surface

r = sqrt(xRel.^2 + yRel.^2 + zRel.^2);
theta_angle = atan2(yRel, xRel);
phi_angle = acos(zRel ./ r);

% Regular cells (type 0)

valid_vec = (~isnan(theta_angle)) & (~isnan(phi_angle));  
validValues(2) = sum(valid_vec);
data_vec = (v_sol_factor_type == 0) & valid_vec;
int_vec = (~data_vec) & valid_vec;

thetaData = theta_angle(data_vec);
phiData = phi_angle(data_vec);
tauData = v_sol_factor_tau(data_vec);
thetaInt = theta_angle(int_vec);
phiInt = phi_angle(int_vec);

% The estimated data size is 2N in theta and N in phi
N = round(sqrt(size(theta_angle, 1) / 2));

% Interpolate on regular grid to add surf plot, for better visualization
[THETA, PHI] = meshgrid(linspace(min(theta_angle), max(theta_angle), 2*N), ...
    linspace(min(phi_angle), max(phi_angle), N));

% Evaluate interpolated tau values
tauInt = griddata(thetaData, phiData, tauData, ...
    thetaInt, phiInt, 'v4');
% TAU is used for better visualization of tau values
TAU = griddata(thetaData, phiData, tauData, ...
    THETA, PHI, 'v4');

if plotFlag(3)
  h3 = figure(3);
  box on
  hold on
  ph3(1) = plot3(thetaData, phiData, tauData, '.k');
  ph3(2) = plot3(thetaInt, phiInt, tauInt, '.r');
  surf(THETA, PHI, TAU)
  xlabel('theta')
  ylabel('phi')
  zlabel('\tau [Pa]')
  legend(ph3, 'Available points', 'Interpolated points')
end

area(2) = sum(v_sol_factor_area(data_vec | int_vec));
F(2) = sum(v_sol_factor_area(data_vec) .* tauData) + ...
    sum(v_sol_factor_area(int_vec) .* tauInt);
CD(2) = F(2) / (0.5 * area(2) * rho * v_inf^2);

%% V: pressure interpolation

data_vec = ~(isnan(v_sol_factor_p)) & valid_vec;
int_vec = (~data_vec) & valid_vec;

thetaData = theta_angle(data_vec);
phiData = phi_angle(data_vec);
pData = v_sol_factor_p(data_vec);
thetaInt = theta_angle(int_vec);
phiInt = phi_angle(int_vec);

% Evaluate interpolated values
pInt = griddata(thetaData, phiData, pData, ...
    thetaInt, phiInt, 'v4');
P = griddata(thetaData, phiData, pData, ...
    THETA, PHI, 'v4');

if plotFlag(4)
  h4 = figure(4);
  box on
  hold on
  ph4(1) = plot3(thetaData, phiData, pData, '.k');
  ph4(2) = plot3(thetaInt, phiInt, pInt, '.r');
  surf(THETA, PHI, P)
  xlabel('theta')
  ylabel('phi')
  zlabel('p [Pa]')
  legend(ph4, 'Available points', 'Interpolated points')
end

FP(2) = -(sum(v_p_factor(data_vec) .* pData) + ...
    sum(v_p_factor(int_vec) .* pInt));
CP(2) = FP(2) / (0.5 * area(2) * rho * v_inf^2);

%% W: tau interpolation

% Relative coordinates

xRel = w_sol_factor_inters_x - solid_center_coord(1);
yRel = w_sol_factor_inters_y - solid_center_coord(2);
zRel = w_sol_factor_inters_z - solid_center_coord(3);

% Compute angles over sphere surface

r = sqrt(xRel.^2 + yRel.^2 + zRel.^2);
theta_angle = atan2(yRel, xRel);
phi_angle = acos(zRel ./ r);

% Regular cells (type 0)

valid_vec = (~isnan(theta_angle)) & (~isnan(phi_angle));
validValues(3) = sum(valid_vec);
data_vec = (w_sol_factor_type == 0) & valid_vec;
int_vec = (~data_vec) & valid_vec;

thetaData = theta_angle(data_vec);
phiData = phi_angle(data_vec);
tauData = w_sol_factor_tau(data_vec);
thetaInt = theta_angle(int_vec);
phiInt = phi_angle(int_vec);

% The estimated data size is 2N in theta and N in phi
N = round(sqrt(size(theta_angle, 1) / 2));

% Interpolate on regular grid to add surf plot, for better visualization
[THETA, PHI] = meshgrid(linspace(min(theta_angle), max(theta_angle), 2*N), ...
    linspace(min(phi_angle), max(phi_angle), N));

% Evaluate interpolated tau values
tauInt = griddata(thetaData, phiData, tauData, ...
    thetaInt, phiInt, 'v4');
% TAU is used for better visualization of tau values
TAU = griddata(thetaData, phiData, tauData, ...
    THETA, PHI, 'v4');

if plotFlag(5)
  h5 = figure(5);
  box on
  hold on
  ph5(1) = plot3(thetaData, phiData, tauData, '.k');
  ph5(2) = plot3(thetaInt, phiInt, tauInt, '.r');
  surf(THETA, PHI, TAU)
  xlabel('theta')
  ylabel('phi')
  zlabel('\tau [Pa]')
  legend(ph5, 'Available points', 'Interpolated points')
end

area(3) = sum(w_sol_factor_area(data_vec | int_vec));
F(3) = sum(w_sol_factor_area(data_vec) .* tauData) + ...
    sum(w_sol_factor_area(int_vec) .* tauInt);
CD(3) = F(3) / (0.5 * area(3) * rho * v_inf^2);

%% W: pressure interpolation

data_vec = ~(isnan(w_sol_factor_p)) & valid_vec;
int_vec = (~data_vec) & valid_vec;

thetaData = theta_angle(data_vec);
phiData = phi_angle(data_vec);
pData = w_sol_factor_p(data_vec);
thetaInt = theta_angle(int_vec);
phiInt = phi_angle(int_vec);

% Evaluate interpolated values
pInt = griddata(thetaData, phiData, pData, ...
    thetaInt, phiInt, 'v4');
P = griddata(thetaData, phiData, pData, ...
    THETA, PHI, 'v4');

if plotFlag(6)
  h6 = figure(6);
  box on
  hold on
  ph6(1) = plot3(thetaData, phiData, pData, '.k');
  ph6(2) = plot3(thetaInt, phiInt, pInt, '.r');
  surf(THETA, PHI, P)
  xlabel('theta')
  ylabel('phi')
  zlabel('p [Pa]')
  legend(ph6, 'Available points', 'Interpolated points')
end

FP(3) = -(sum(w_p_factor(data_vec) .* pData) + ...
    sum(w_p_factor(int_vec) .* pInt));
CP(3) = FP(3) / (0.5 * area(3) * rho * v_inf^2);


%% Print results

fprintf("Valid values u: %i (%10.5f %%)\n", validValues(1), ...
    validValues(1) * 100 / numel(u_sol_factor));
fprintf("Valid values v: %i (%10.5f %%)\n", validValues(2), ...
    validValues(2) * 100 / numel(v_sol_factor));
fprintf("Valid values w: %i (%10.5f %%)\n", validValues(3), ...
    validValues(3) * 100 / numel(w_sol_factor));
fprintf("Areas u: %10.5f [m2], v: %10.5f [m2], w: %10.5f [m2]\n", area);
fprintf("CDx: %10.5f CDy: %10.5f CDz: %10.5f\n", CD);
fprintf("CDPx: %10.5f CDPy: %10.5f CDPz: %10.5f\n", CP);
fprintf("U Regular nodes: %i\n", sum(u_sol_factor_type == 0));
fprintf("U Slave nodes: %i\n", sum(u_sol_factor_type == 1));
fprintf("U Solid nodes: %i\n", sum(u_sol_factor_type == 2));
fprintf("V Regular nodes: %i\n", sum(v_sol_factor_type == 0));
fprintf("V Slave nodes: %i\n", sum(v_sol_factor_type == 1));
fprintf("V Solid nodes: %i\n", sum(v_sol_factor_type == 2));
fprintf("W Regular nodes: %i\n", sum(w_sol_factor_type == 0));
fprintf("W Slave nodes: %i\n", sum(w_sol_factor_type == 1));
fprintf("W Solid nodes: %i\n", sum(w_sol_factor_type == 2));

CDy = F(2) / (0.5 * rho * v_inf^2 * pi *D^2 / 4);

fprintf('Based on a circular area: CDy = %10.5f\n', CDy);

end