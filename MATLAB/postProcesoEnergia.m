% Graficos de temperatura de aire, temperatura al centro de los cilindros
% y fracción de líquido
% Cargar los siguientes datos
% lfrac0.mat a lfrac5.mat 
% Tc0.mat a Tc5.mat 
% Ta0.ma Ta10.mat Ta11.mat Ta20.mat Ta21.mat Ta3.mat tiempo.mat
% Ta3avg.mat

load('setDatos/simulacion/v15/lfrac0.mat')
load('setDatos/simulacion/v15/lfrac1.mat')
load('setDatos/simulacion/v15/lfrac2.mat')
load('setDatos/simulacion/v15/lfrac3.mat')
load('setDatos/simulacion/v15/lfrac4.mat')
load('setDatos/simulacion/v15/lfrac5.mat')
load('setDatos/simulacion/v15/Tc0.mat')
load('setDatos/simulacion/v15/Tc1.mat')
load('setDatos/simulacion/v15/Tc2.mat')
load('setDatos/simulacion/v15/Tc3.mat')
load('setDatos/simulacion/v15/Tc4.mat')
load('setDatos/simulacion/v15/Tc5.mat')
load('setDatos/simulacion/v15/Ta0.mat')
load('setDatos/simulacion/v15/Ta10.mat')
load('setDatos/simulacion/v15/Ta11.mat')
load('setDatos/simulacion/v15/Ta20.mat')
load('setDatos/simulacion/v15/Ta21.mat')
load('setDatos/simulacion/v15/Ta3avg.mat')
load('setDatos/simulacion/v15/tiempo.mat')

close all

%% Control flags

plotLiquidFraction = true;
plotCylinderCenterTemp = true;
plotAirTemp = true;
transformData = true;
plotTotal = true;


%% Transform data

if transformData
  % Tiempo en minutos
  tiempoM = tiempo / 60;
  Tc0C = Tc0 - 273;
  Tc1C = Tc1 - 273;
  Tc2C = Tc2 - 273;
  Tc3C = Tc3 - 273;
  Tc4C = Tc4 - 273;
  Tc5C = Tc5 - 273;
  
  Ta0C = Ta0 - 273;
  Ta10C = Ta10 - 273;
  Ta11C = Ta11 - 273;
  Ta20C = Ta20 - 273;
  Ta21C = Ta21 - 273;
  % Use average outlet temperature
  Ta3C = Ta3avg - 273;
end

%% Plots

% Liquid fraction

if plotLiquidFraction
  % Fila 1
  h1 = figure(1);
  hold on
  box on
  plot(tiempoM(3:3:end), lfrac0(3:3:end), '-b')
  plot(tiempoM(3:3:end), lfrac1(3:3:end), '-r')
  plot(tiempoM(3:3:end), lfrac2(3:3:end), '-k')
  xlabel('Tiempo [min]')
  ylabel('Fracción de líquido [-]')
  legend('c0', 'c1', 'c2', 'Location', 'NorthEast')
  axis([-Inf Inf -0.05 1])
  % Fila 2
  h2 = figure(2);
  hold on
  box on
  plot(tiempoM(3:3:end), lfrac3(3:3:end), '-b')
  plot(tiempoM(3:3:end), lfrac4(3:3:end), '-r')
  plot(tiempoM(3:3:end), lfrac5(3:3:end), '-k')
  xlabel('Tiempo [min]')
  ylabel('Fracción de líquido [-]')
  legend('c3', 'c4', 'c5', 'Location', 'NorthEast')
  axis([-Inf Inf -0.05 1])
end

% Cylinder center temperatures

if plotCylinderCenterTemp
  h3 = figure(3);
  hold on
  box on
  plot(tiempoM(3:3:end), Tc0C(3:3:end), '-b')
  plot(tiempoM(3:3:end), Tc1C(3:3:end), '-r')
  plot(tiempoM(3:3:end), Tc2C(3:3:end), '-k')
  xlabel('Tiempo [min]')
  ylabel('Temperatura [C]')
  legend('c0', 'c1', 'c2', 'Location', 'NorthEast')
  %
  h4 = figure(4);
  hold on
  box on
  plot(tiempoM(3:3:end), Tc3C(3:3:end), '-b')
  plot(tiempoM(3:3:end), Tc4C(3:3:end), '-r')
  plot(tiempoM(3:3:end), Tc5C(3:3:end), '-k')
  xlabel('Tiempo [min]')
  ylabel('Temperatura [C]')
  legend('c3', 'c4', 'c5', 'Location', 'NorthEast')
end

% Air temperature

if plotAirTemp
  h5 = figure(5);
  hold on
  box on
  plot(tiempoM(3:3:end), Ta0C(3:3:end), '-k')
  plot(tiempoM(3:3:end), Ta10C(3:3:end), '-b')
  plot(tiempoM(3:3:end), Ta11C(3:3:end), '--b')
  plot(tiempoM(3:3:end), Ta20C(3:3:end), '-g')
  plot(tiempoM(3:3:end), Ta21C(3:3:end), '--g')
  plot(tiempoM(3:3:end), Ta3C(3:3:end), '-r')
  xlabel('Tiempo [min]')
  ylabel('Temperatura [C]')
  legend('Ta0', 'Ta10', 'Ta11', 'Ta20', 'Ta21', 'Ta3', ...
  'Location', 'NorthEast')
  axis([-Inf Inf 18 Inf])
end
