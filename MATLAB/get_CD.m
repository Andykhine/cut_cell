function get_CD
%GET_CD Get the CD from sphere computations

clc

methods = {'upwind/', 'QUICK/', 'QUICK-TVD/'};
interps = {'N/', 'P/'};
kappas = {'k_05/', 'k_10/', 'k_20/', 'k_30/', 'k_40/'};
grid = 'D/';
Res = 'R100/';

base_dir = pwd;

nm = length(methods);
ni = length(interps);
nk = length(kappas);

CDs = zeros(nm,ni,nk);

for i=1:nm
    for j=1:ni
        for k=1:nk            
            folder = [base_dir '/' cell2mat(methods(i)) ...
                Res ...
                cell2mat(interps(j)) ...
                cell2mat(kappas(k))...
                grid];
            
            cd(folder);
            try
                CDs(i,j,k) = intFrictionCoeffs3D;
            catch
                CDs(i,j,k) = NaN;
            end
            
        end
    end
end

fprintf('%5s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\n', ...
    ' ', ' ', 'upwind', ' ', 'QUICK', ' ', 'vL');
fprintf('%5s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\n', ...
    'kappa', 'N', 'P', 'N', 'P', 'N', 'P');

L = [ [0.05 0.10 0.20 0.30 0.40]' ...
    squeeze(CDs(1,1,:)) squeeze(CDs(1,2,:)) ...
    squeeze(CDs(2,1,:)) squeeze(CDs(2,2,:)) ...
    squeeze(CDs(3,1,:)) squeeze(CDs(3,2,:)) ]';

fprintf('%5.2f\t%7.5f\t%7.5f\t%7.5f\t%7.5f\t%7.5f\t%7.5f\n', L);

end
