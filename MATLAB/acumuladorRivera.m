% Required files

% tiempo Ta3avg15 
% Tc3 Tc4 Tc5 tiempo
% en carpeta

load('setDatos/simulacion/v15/tiempo.mat')
load('setDatos/simulacion/v15/Ta3avg15.mat')
load('setDatos/simulacion/v15/Tc3.mat')
load('setDatos/simulacion/v15/Tc4.mat')
load('setDatos/simulacion/v15/Tc5.mat')

% datos Rivera
%  dataAireRivera.mat dataCenterRivera.mat dataSurfaceRivera.mat

load('setDatos/experimentosRivera/dataAireRivera.mat')
load('setDatos/experimentosRivera/dataCenterRivera.mat')
load('setDatos/experimentosRivera/dataSurfaceRivera.mat')

close all

plotFigures = [1 0 0 0 0];

% plotFigures(1): Temperatura aire
% plotFigures(2): Temperatura al centro
% plotFigures(3): Temperatura de superficie
% plotFigures(4): Aumento de temperatura 
% plotFigures(5): Temperatura cilindros experimental y simulacion en un
% solo grafico

if ~exist('Ta3avg15','var')
  fprintf(1, 'Remember to load Ta3avg15.mat file\n')
end

if ~exist('dataAire', 'var')
  fprintf(1, 'Remember to load dataAireRivera.mat file\n')
end

if ~exist('dataCenter', 'var')
  fprintf(1, 'Remember to load dataCenterRivera.mat file\n')
end

if ~exist('dataSurface', 'var')
  fprintf(1, 'Remember to load dataSurfaceRivera.mat file\n')
end

if ~exist('Tc3', 'var')
  fprintf(1, 'Remember to load Tc3.mat file\n')
end

if ~exist('Tc4', 'var')
  fprintf(1, 'Remember to load Tc4.mat file\n')
end

if ~exist('Tc5', 'var')
  fprintf(1, 'Remember to load Tc5.mat file\n')
end

if ~exist('tiempo', 'var')
  fprintf(1, 'Remember to load tiempo.mat file\n')
end

% Cut for splitting data in two sets

cut = 20;

counter = sum((dataAire(:,5) - dataAire(:,2)) > 10);
fprintf(1, 'Tiempo tal que Delta T es mayor a 10 [C]: %2.2f\n', ...
  dataAire(counter, 1))

counter = sum((dataAire(:,5) - dataAire(:,2)) > 5);
fprintf(1, 'Tiempo tal que Delta T es mayor a 5 [C]: %2.2f\n', ...
  dataAire(counter, 1))

counter = sum((dataAire(:,5) - dataAire(:,2)) > 4);
fprintf(1, 'Tiempo tal que Delta T es mayor a 4 [C]: %2.2f\n', ...
  dataAire(counter, 1))

if plotFigures(1)
  h1 = figure(1);
  box on
  hold on
  plot(dataAire(2:end,1), dataAire(2:end,2), '.k')
  plot(dataAire(2:end,1), dataAire(2:end,3), '.b')
  plot(dataAire(2:end,1), dataAire(2:end,4), '.g')
  plot(dataAire(2:end,1), dataAire(2:end,5), '.r')
  xlabel('Time [min]')
  ylabel('Temperature [ºC]')
  legend('T_{a1}', 'T_{a2}', 'T_{a3}', 'T_{a4}', ...
    'location', 'NorthEast');
set(gca, 'XLim', [0 60])
hold off
Graph_Format
end

if plotFigures(2)
  h2 = figure(2);
  box on
  hold on
  plot(dataCenter(:,1), dataCenter(:,2), '.b')
  plot(dataCenter(:,1), dataCenter(:,3), '.r')
  plot(dataCenter(:,1), dataCenter(:,4), '.k')
  xlabel('Time [min]')
  ylabel('Temperature [ºC]')
  legend('T_{c1}', 'T_{c2}', 'T_{c3}', ...
    'location', 'NorthEast')
set(gca, 'XLim', [0 60])
hold off
Graph_Format
end

if plotFigures(3)
  h3 = figure(3);
  box on
  hold on
  plot(dataSurface(:,1), dataSurface(:,2), '.b')
  plot(dataSurface(:,1), dataSurface(:,3), '.r')
  plot(dataSurface(:,1), dataSurface(:,4), '.k')
  xlabel('Time [min]')
  ylabel('Temperature [ºC]')
  legend('T_{s1}', 'T_{s2}', 'T_{s3}', ...
    'location', 'NorthEast')
set(gca, 'XLim', [0 60])
hold off
Graph_Format
end
  
if plotFigures(4)
  h4 = figure(4);
  box on
  hold on
  plot(dataAire(:,1), dataAire(:,5) - dataAire(:,2), 'or', ...
      'MarkerSize',2.5)
  plot(tiempo(3:3:end)/60, Ta3avg15(3:3:end) - 293, '-b')  
  xlabel('Time [min]')
  ylabel('Temperaturre increase [ºC]')
  legend('Exp.', 'Calc.', ...
    'location', 'NorthEast')
set(gca, 'XLim', [0 60])
hold off
Graph_Format
end

if plotFigures(5)
  h5 = figure(5);
  box on
  hold on
  plot(tiempo(3:3:end)/60, Tc3(3:3:end) - 273, '-b')
  plot(tiempo(3:3:end)/60, Tc4(3:3:end) - 273, '-r')
  plot(tiempo(3:3:end)/60, Tc5(3:3:end) - 273, '-k')
  plot(dataCenter(:,1), dataCenter(:,2), '.b')
  plot(dataCenter(:,1), dataCenter(:,3), '.r')
  plot(dataCenter(:,1), dataCenter(:,4), '.k')
  xlabel('Time [min]')
  ylabel('Temperature [ºC]')
  legend('Sim. T_{c3}', 'Sim. T_{c4}', 'Sim. T_{c5}', 'Exp. T_{c1}', ...
      'Exp. T_{c2}', 'Exp. T_{c3}', 'location', 'SouthWest')
set(gca, 'XLim', [0 60])
hold off
Graph_Format
end
