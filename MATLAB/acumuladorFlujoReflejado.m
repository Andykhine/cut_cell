% Gráficos para mostrar los resultados para el flujo en el acumulador.

% Archivos requeridos
% en carpeta /home/francisco/Documents/Tesis/produccion/pcOficina/resultados/acumulador/simetria/v1.5/turbulentoCont/dominioReflejado
% nodesX.mat nodesY.mat
% p.mat UCenter.mat VCenter.mat pDomMatrix.mat
% domMatrix.mat

load('setDatos/simulacion/v15/flujo/nodesX.mat')
load('setDatos/simulacion/v15/flujo/nodesY.mat')
load('setDatos/simulacion/v15/flujo/p.mat')
load('setDatos/simulacion/v15/flujo/UCenter.mat')
load('setDatos/simulacion/v15/flujo/VCenter.mat')
load('setDatos/simulacion/v15/flujo/pDomMatrix.mat')
load('setDatos/simulacion/v15/flujo/domMatrix.mat')

%% Control flags

% Flags que controlan la parte gráfica

close all

plotPressureMagnitud = 1;
plotVelocityMagnitud = 1;
plotStreamlines = 1;

reduceData = 0;

%% Data reduction
if reduceData
  nodesX(2:2:end, :) = []; nodesX(:, 2:2:end) = [];
  nodesY(2:2:end, :) = []; nodesY(:, 2:2:end) = [];
  p(2:2:end, :) = []; p(:, 2:2:end) = [];
  UCenter(2:2:end, :) = []; UCenter(:, 2:2:end) = [];
  VCenter(2:2:end, :) = []; VCenter(:, 2:2:end) = [];  
  pDomMatrix(2:2:end, :) = []; pDomMatrix(:, 2:2:end) = [];
  domMatrix(2:2:end, :) = []; domMatrix(:, 2:2:end) = [];
end

%% Magnitud de presión

if plotPressureMagnitud
  h1 = figure(1);
  temp = p;
  temp(pDomMatrix ~= 0) = nan;
  surf(nodesX(2:end-1,2:end-1), nodesY(2:end-1,2:end-1), temp(2:end-1,2:end-1), ...
    'EdgeColor', 'none')
  view(2)
  colorbar()
  colormap parula
  grid off
  box on
  axis equal
  axis([0 0.52 0 0.35])
  xlabel('x[m]')
  ylabel('y[m]')
  
  tmpChildren = get(gcf, 'Children');
  tmpLabel = get(tmpChildren(1), 'Label');
  set(tmpLabel, 'String', '[Pa]');
end

%% Magnitud de velocidad

if plotVelocityMagnitud
  h2 = figure(2);
  VMag = sqrt(UCenter.^2 + VCenter.^2);
  VMag(domMatrix ~= 0) = nan;
  % UCenter, VCenter values only defined for interior nodes
  surf(nodesX(2:end-1,2:end-1), nodesY(2:end-1,2:end-1), ...
    VMag(2:end-1,2:end-1), 'EdgeColor', 'none')
  view(2)
  colorbar()
  colormap parula
  grid off
  box on
  axis equal
  axis([0 0.52 0 0.35])
  xlabel('x[m]')
  ylabel('y[m]')
  tmpChildren = get(gcf, 'Children');
  tmpLabel = get(tmpChildren(1), 'Label');
  set(tmpLabel, 'String', '[m/s]');
end


%% Streamlines

if plotStreamlines
  
  nDataInlet = 13;
  yStart = linspace(nodesY(2, 3), nodesY(2, end/2 -1), nDataInlet);
  xStart = ones(1, numel(yStart)) * nodesX(2, 1);
  
  X = nodesX(2:end-1, 2:end-1)';
  Y = nodesY(2:end-1, 2:end-1)';
  U = UCenter(2:end-1, 2:end-1)';
  V = VCenter(2:end-1, 2:end-1)';
  
  yMax = nodesY(1,end);
  
  figure(5) 
  hold on 
  st1 = stream2(X, Y, U, V, xStart, yStart);
  h1 = streamline(st1);
  set(h1, 'Color', 'k')

  yStart = linspace(0.0290, 0.0510, 4);
  xStart = ones(1, numel(yStart)) * 0.46;
  st2 = stream2(X, Y, U, V, xStart, yStart, [0.1 5000]);
  h2 = streamline(st2);
  set(h2, 'Color', 'k')
    
  yStart = linspace(0.1190, 0.1410, 4);
  xStart = ones(1, numel(yStart)) * 0.40;
  st3 = stream2(X, Y, U, V, xStart, yStart, [0.1 2500]);
  h3 = streamline(st3);
  set(h3, 'Color', 'k')
  
  yStart = linspace(0.0510, 0.0721, 4);
  xStart = ones(1, numel(yStart)) * 0.50;
  st4 = stream2(X, Y, U, V, xStart, yStart, [0.1 5000]);
  h4 = streamline(st4);
  set(h4, 'Color', 'k')
  
  % Duplicate streamlines. Plot directly didn't work due to unknown reasons
  
  st5 = st1;
  for i = 1:numel(st1)
    temp = st5{i};
    temp(:,2) = yMax - temp(:,2);
    st5{i} = temp;
  end
  h5 = streamline(st5);
  set(h5, 'Color', 'k')
  
  st6 = st2;
  for i = 1:numel(st6)
    temp = st6{i};
    temp(:,2) = yMax - temp(:,2);
    st6{i} = temp;
  end
  h6 = streamline(st6);
  set(h6, 'Color', 'k')
  
  st7 = st3;
  for i = 1:numel(st7)
    temp = st7{i};
    temp(:,2) = yMax - temp(:,2);
    st7{i} = temp;
  end
  h7 = streamline(st7);
  set(h7, 'Color', 'k')
  
  st8 = st4;
  for i = 1:numel(st8)
    temp = st8{i};
    temp(:,2) = yMax - temp(:,2);
    st8{i} = temp;
  end
  h8 = streamline(st8);
  set(h8, 'Color', 'k')
  
  axis equal
  box on
  xlabel('x[m]')
  ylabel('y[m]')
  axis([0 0.52 0 0.35])
end