function Figure_Format(S, painters_OK)
% Adjust the paper to figure size
% Usage: FIGURE_FORMAT(['PDF' | 'EPS' | 'PS' | 'TIFF' | 'EMF'], [true | false])
% Contact: luis.henriquez.gk@gmail.com

if nargin < 2
    disp('Input argument of type LOGICAL required, set TRUE as default');
    painters_OK = true;
end

if nargin < 1
    disp('Input argument of type STRING required, set PDF as default');
    S = 'PDF';
end
if ~ischar(S)
    error('Input argument must be of type STRING');
end

if(painters_OK)
        % For vector graphics
        S_renderer = '-painters';
else
        % For bitmaps (!?)
        S_renderer = '-opengl';
end

%% Obteniendo tamaño de la figura.
set(gcf,'Units','centimeters');
Pos_fig = get(gcf,'Position');
%% Ajustando el tamaño del papel al de la figura.
set(gcf,'PaperType','<custom>','PaperPositionMode','auto');
set(gcf,'PaperUnits','centimeters','PaperSize',[Pos_fig(3) Pos_fig(4)]);
%% Exportando a PDF, EPS (TIFF preview), TIFF, EMF
filename = get(gcf, 'FileName');
filename = filename(1:(max(size(filename))-4));
switch S
    case {'PDF', 'pdf'}
        print(S_renderer, '-dpdf', filename);
        disp('Done PDF!!!!!!!!!');
    case {'EPS', 'eps'}
        print(S_renderer, '-depsc2', filename);
        disp('Done EPS!!!!!!!!!');        
    case {'PS', 'ps'}
        print(S_renderer, '-dps', filename);
        disp('Done PS!!!!!!!!!');                
    case {'TIFF', 'tiff'}
        print('-dtiff', filename);
        disp('Done TIFF!!!!!!!!!');
    case {'EMF', 'emf'}
        if isunix
            error('MATLAB UNIX version does not support export to EMF');
        else
            print(S_renderer, '-dmeta', filename);
            disp('Done EMF!!!!!!!!!');
        end    
    otherwise
        disp('Usage: Figure_Format(S, painters_OK)');
        disp('S = "PDF", "EPS", "PS", "TIFF", "EMF"');
        disp('painters_OK = true, false');
end
end
