function graph_sigmas(Stitle)
%GRAPH_SIGMAS Graphs the sigmas from sigmas.dat file
if nargin <1, Stitle = []; end
%% Clean
close all
clc
%% Settings
set(groot,'DefaultAxesTickLabelInterpreter','Latex');
set(groot,'DefaultLegendInterpreter','Latex');

system('mv sigmas.dat sigmas.m');

sigmas

its = sflow_cpu_var(:,1);
su = sflow_cpu_var(:,2);
sv = sflow_cpu_var(:,3);
sw = sflow_cpu_var(:,4);
sc = sflow_cpu_var(:,5);
sm = sflow_cpu_var(:,8);

figure
hold on
plot(its, su, '-b');
plot(its, sv, '-k');
plot(its, sw, '-r');
plot(its, sc, '--m');
plot(its, sm, '--g');
hold off
set(gca, 'XScale', 'log', 'YScale', 'log');
box on
xlabel('iteration');
ylabel('\sigma');
legend('su', 'sv', 'sw', 'sc', 'sm', 'Location', 'sw');
if ~isempty(Stitle)
    title(Stitle);
end

fprintf('Global residues u = %e, v = %e, w = %e\n', ...
    glob_norm_res_var(1), glob_norm_res_var(2), glob_norm_res_var(3));
end

