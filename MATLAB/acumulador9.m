function acumulador9  %#ok<*NODEF,*NASGU,*COLND>
%% Clean space
clc
clear
close all
%% Call grid independence
% grid_independence_flow;
% grid_independence_phi;
%% COMSOL comparison
% COMSOL_comparison;
%% Call discharge graphs
exp_sim_55;
%% Call flow field
% SPA_flow_field_full;
end

function grid_independence_flow
%% Load data
% COMSOL
load datos/9_COMSOL.mat

% A
load datos/9_A.mat

mut_A = squeeze(mu_turb(2:end-1,84));
k_A = squeeze(k_turb(2:end-1,84));
e_A = squeeze(eps_turb(2:end-1,84));
mut2_A = squeeze(mu_turb(134,2:end-1));
k2_A = squeeze(k_turb(134,2:end-1));
e2_A = squeeze(eps_turb(134,2:end-1));
U2_A = squeeze(U(134,2:end-1));
V2_A = squeeze(V(134,2:end-1));
vel2_A = sqrt(U2_A.^2 + V2_A.^2);
vel_A = sqrt(U(2:end-1,84).^2 + V(2:end-1,84).^2);
x_A = squeeze(x(2:end-1,1));
y_A = squeeze(y(1,2:end-1));

% B
load datos/9_B.mat

mut_B = squeeze(0.5*(mu_turb(2:end-1,124) + mu_turb(2:end-1,125)));
k_B = squeeze(0.5*(k_turb(2:end-1,124) + k_turb(2:end-1,125)));
e_B = squeeze(0.5*(eps_turb(2:end-1,124) + eps_turb(2:end-1,125)));
U_B = squeeze(0.5*(U(2:end-1,124) + U(2:end-1,125)));
V_B = squeeze(0.5*(V(2:end-1,124) + V(2:end-1,125)));
mut2_B = squeeze(0.5*(mu_turb(184,2:end-1) + mu_turb(185,2:end-1)));
k2_B = squeeze(0.5*(k_turb(184,2:end-1) + k_turb(185,2:end-1)));
e2_B = squeeze(0.5*(eps_turb(184,2:end-1) + eps_turb(185,2:end-1)));
U2_B = squeeze(0.5*(U(184,2:end-1) + U(185,2:end-1)));
V2_B = squeeze(0.5*(V(184,2:end-1) + V(185,2:end-1)));
vel2_B = sqrt(U2_B.^2 + V2_B.^2);
vel_B = sqrt(U_B.^2 + V_B.^2);
x_B = squeeze(x(2:end-1,1));
y_B = squeeze(y(1,2:end-1));

% C
load datos/9_C.mat

mut_C = squeeze(mu_turb(2:end-1,146));
k_C = squeeze(k_turb(2:end-1,146));
e_C = squeeze(eps_turb(2:end-1,146));
vel_C = sqrt(U(2:end-1,146).^2 + V(2:end-1,146).^2);
mut2_C = squeeze(0.5*(mu_turb(229,2:end-1) + mu_turb(230,2:end-1)));
k2_C = squeeze(0.5*(k_turb(229,2:end-1) + k_turb(230,2:end-1)));
e2_C = squeeze(0.5*(eps_turb(229,2:end-1) + eps_turb(230,2:end-1)));
U2_C = squeeze(0.5*(U(229,2:end-1) + U(230,2:end-1)));
V2_C = squeeze(0.5*(V(229,2:end-1) + V(230,2:end-1)));
vel2_C = sqrt(U2_C.^2 + V2_C.^2);
x_C = squeeze(x(2:end-1,1));
y_C = squeeze(y(1,2:end-1));

% % D
% load datos/GRID_D.mat
%
% k_D = squeeze(k_turb(2:end-1,end));
% e_D = squeeze(eps_turb(2:end-1,end));
% k2_D = squeeze(0.5*(k_turb(274,2:end-1) + k_turb(275,2:end-1)));
% e2_D = squeeze(0.5*(eps_turb(274,2:end-1) + eps_turb(275,2:end-1)));
% U2_D = squeeze(0.5*(U(274,2:end-1) + U(275,2:end-1)));
% V2_D = squeeze(0.5*(V(274,2:end-1) + V(275,2:end-1)));
% vel2_D = sqrt(U2_D.^2 + V2_D.^2);
% vel_D = sqrt(U(2:end-1,end-1).^2 + V(2:end-1,end-1).^2);
% x_D = squeeze(x(2:end-1,1));
% y_D = squeeze(y(1,2:end-1));

% % E
% load datos/GRID_E.mat
%
% k_E = squeeze(k_turb(2:end-1,end));
% e_E = squeeze(eps_turb(2:end-1,end));
% k2_E = squeeze(0.5*(k_turb(304,2:end-1) + k_turb(305,2:end-1)));
% e2_E = squeeze(0.5*(eps_turb(304,2:end-1) + eps_turb(305,2:end-1)));
% U2_E = squeeze(0.5*(U(304,2:end-1) + U(305,2:end-1)));
% V2_E = squeeze(0.5*(V(304,2:end-1) + V(305,2:end-1)));
% vel2_E = sqrt(U2_E.^2 + V2_E.^2);
% vel_E = sqrt(U(2:end-1,end-1).^2 + V(2:end-1,end-1).^2);
% x_E = squeeze(x(2:end-1,1));
% y_E = squeeze(y(1,2:end-1));
%% Graphs
% vel
figure
hold on
% plot(x_COMSOL3D, vel_COMSOL3D, '+k');
% plot(x_COMSOL, vel_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(x_A, vel_A, '-.b');
plot(x_B, vel_B, '--r');
plot(x_C, vel_C, 'k');
% plot(x_D, vel_D, 'g');
% plot(x_E, vel_E, 'm');
hold off
xlabel('x [m]'); ylabel('Vel [m/s]');
set(gca, 'XLim', [-0.005 0.785]);
box on
grid off
legend('A', 'B', 'C', 'Location', 's');
Graph_Format;
% vel2
figure
hold on
% plot(y_COMSOL3D, vel2_COMSOL3D, '+k');
% plot(y_COMSOL, vel2_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(y_A, vel2_A, '-.b');
plot(y_B, vel2_B, '--r');
plot(y_C, vel2_C, 'k');
% plot(y_D, vel_D, 'g');
% plot(y_E, vel_E, 'm');
hold off
xlabel('y [m]'); ylabel('Vel [m/s]');
set(gca, 'XLim', [-0.005 0.18]);
box on
grid off
legend('A', 'B', 'C', 'Location', 'ne');
Graph_Format;
% k
figure
hold on
% plot(x_COMSOL3D, k_COMSOL3D, '+k');
% plot(x_COMSOL, k_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(x_A, k_A, '-.b');
plot(x_B, k_B, '--r');
plot(x_C, k_C, 'k');
% plot(y_D, k2_D, 'g');
% plot(y_E, k2_E, 'm');
hold off
xlabel('x [m]'); ylabel('k [m^2/s^2]');
set(gca, 'XLim', [-0.005 0.785]);
box on
grid off
legend('A', 'B', 'C', 'Location', 'nw');
Graph_Format;
% k2
figure
hold on
% plot(y_COMSOL3D, k2_COMSOL3D, '+k');
% plot(y_COMSOL, k2_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(y_A, k2_A, '-.b');
plot(y_B, k2_B, '--r');
plot(y_C, k2_C, 'k');
% plot(y_D, k2_D, 'g');
% plot(y_E, k2_E, 'm');
hold off
xlabel('y [m]'); ylabel('k [m^2/s^2]');
set(gca, 'XLim', [-0.005 0.18]);
box on
grid off
legend('A', 'B', 'C', 'Location', 'nw');
Graph_Format;
% e
figure
hold on
% plot(x_COMSOL3D, e_COMSOL3D, '+k');
% plot(x_COMSOL, e_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(x_A, e_A, '-.b');
plot(x_B, e_B, '--r');
plot(x_C, e_C, 'k');
% plot(x_D, e_D, 'g');
% plot(x_E, e_E, 'm');
hold off
xlabel('x [m]'); ylabel('e [m^2/s^3]');
set(gca, 'XLim', [-0.005 0.785]);
box on
grid off
legend('A', 'B', 'C', 'Location', 'nw');
Graph_Format;
% e2
figure
hold on
% plot(y_COMSOL3D, e2_COMSOL3D, '+k');
% plot(y_COMSOL, e2_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(y_A, e2_A, '-.b');
plot(y_B, e2_B, '--r');
plot(y_C, e2_C, 'k');
% plot(y_D, e2_D, 'g');
% plot(y_E, e2_E, 'm');
hold off
xlabel('y [m]'); ylabel('e [m^2/s^3]');
set(gca, 'XLim', [-0.005 0.18]);
box on
grid off
legend('A', 'B', 'C', 'Location', 'nw');
Graph_Format;
% mut
figure
hold on
% plot(x_COMSOL3D, mut_COMSOL3D, '+k');
% plot(x_COMSOL, mut_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(x_A, mut_A, '-.b');
plot(x_B, mut_B, '--r');
plot(x_C, mut_C, 'k');
% plot(x_D, mut_D, 'g');
% plot(x_E, mut_E, 'm');
hold off
xlabel('x [m]'); ylabel('mut [Pa s]');
set(gca, 'XLim', [-0.005 0.785]);
box on
grid off
legend('A', 'B', 'C', 'Location', 'n');
Graph_Format;
% mut2
figure
hold on
% plot(y_COMSOL3D, mut2_COMSOL3D, '+k');
% plot(y_COMSOL, mut2_COMSOL, 'ok', 'MarkerFaceColor', 'w');
plot(y_A, mut2_A, '-.b');
plot(y_B, mut2_B, '--r');
plot(y_C, mut2_C, 'k');
% plot(y_D, mut2_D, 'g');
% plot(y_E, mut2_E, 'm');
hold off
xlabel('y [m]'); ylabel('mut [Pa s]');
set(gca, 'XLim', [-0.005 0.18]);
box on
grid off
legend('A', 'B', 'C', 'Location', 'ne');
Graph_Format;
end

function grid_independence_phi
%% Load data
% 0: 10 (s)
load datos/12_dt0.mat

Ta5_0 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_0 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 1: 1 (s)
load datos/12_dt1.mat

Ta5_1 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_1 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 2: 0.25 (s)
load datos/12_dt2.mat

Ta5_2 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_2 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 3: 0.1 (s)
load datos/12_dt3.mat

Ta5_3 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_3 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 4: 0.01 (s)
load datos/12_dt4.mat

Ta5_4 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_4 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));
%% Graphs
figure
hold on
plot(1:5, Ta5_0 - 273.15, '-ks', 'MarkerFaceColor', 'w');
plot(1:5, Ta5_1 - 273.15, '--ko', 'MarkerFaceColor', 'k');
plot(1:5, Ta5_2 - 273.15, '--b+');
plot(1:5, Ta5_3 - 273.15, '-ko', 'MarkerFaceColor', 'w');
plot(1:5, Ta5_4 - 273.15, '--kv', 'MarkerFaceColor', 'w');
hold off
xlabel('t [min]'); ylabel('T_{a5} [ºC]');
set(gca, 'XLim', [0 6], 'YLim', [19 31]);
box on
grid off
% legend('10@2000', '1@2000', '0.25@640', ...
%     '0.1@2000', '0.01@2000', 'Location', 'e');
legend('10', '1', '0.25', ...
    '0.1', '0.01', 'Location', 'e');
Graph_Format;
end

function SPA_flow_field_full
%% Load data
r = sqrt(0.00109);

load datos/9_C.mat

velL = sqrt(U.^2 + V.^2);
velL(p_dom_matrix ~= 0) = nan;

pL = p;
pL(p_dom_matrix ~= 0) = nan;

% load datos/SPA_55_simulation.mat

velR = sqrt(U.^2 + V.^2);
velR(p_dom_matrix ~= 0) = nan;

pR = p;
pR(p_dom_matrix ~= 0) = nan;
%% Joining data in a specular way
vel = join_array(velL, velR);
p = join_array(pL, pR);
% Dont consider data on simmetry line
tempX = join_array(x,x);
tempY = [y(:,1:end-1) y(:,2:end)+0.175]';
%% Graphs flow field
% vel zoom
figure
hold on
[C, h] = contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
    vel(2:end-1,2:end-1), 'Fill', 'on');
axis equal
set(gca, 'XLim', [0.1 0.41], 'YLim', [0 0.175]);
colorbar('NorthOutside', 'Color', 'k');
colormap jet
grid off
box on
xlabel('x [m]');
ylabel('y [m]');
tmpChildren = get(gcf, 'Children');
tmpLabel = get(tmpChildren(1), 'Label');
set(tmpLabel, 'String', '[m/s]');
Graph_Format;
clabel(C,h,'manual',...
    'Fontweight', 'bold', 'FontName', 'palatino', 'FontSize', 10, ...
    'Color','k','EdgeColor','none','BackgroundColor','w');
hold off
%
% vel
figure
hold on
axis equal
contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
    vel(2:end-1,2:end-1), 'Fill', 'on');
colorbar('NorthOutside', 'Color', 'k');
colormap jet
grid off
box on
axis equal
xlabel('x [m]');
ylabel('y [m]');
tmpChildren = get(gcf, 'Children');
tmpLabel = get(tmpChildren(1), 'Label');
set(tmpLabel, 'String', '[m/s]');
Graph_Format;
hold off
%% Graphs pressure field
% p zoom
figure
hold on
[C, h] = contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
    p(2:end-1,2:end-1), 'Fill', 'on');
axis equal
set(gca, 'XLim', [0.1 0.41], 'YLim', [0 0.175]);
colorbar('NorthOutside', 'Color', 'k');
colormap jet
grid off
box on
xlabel('x [m]');
ylabel('y [m]');
tmpChildren = get(gcf, 'Children');
tmpLabel = get(tmpChildren(1), 'Label');
set(tmpLabel, 'String', '[Pa]');
Graph_Format;
clabel(C,h,'manual',...
    'Fontweight', 'bold', 'FontName', 'palatino', 'FontSize', 10, ...
    'Color','k','EdgeColor','none','BackgroundColor','w');
hold off
% set(gcf, 'Position', [9.8425 4.4715 22.0927 16.0073]);

% p
figure
hold on
axis equal
contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
    p(2:end-1,2:end-1), 'Fill', 'on');
colorbar('NorthOutside', 'Color', 'k');
colormap jet
grid off
box on
axis equal
xlabel('x [m]');
ylabel('y [m]');
tmpChildren = get(gcf, 'Children');
tmpLabel = get(tmpChildren(1), 'Label');
set(tmpLabel, 'String', '[Pa]');
Graph_Format;
hold off
end

function M = join_array(L, R)
% Joins matrices L and R to the left and right with the right being the
% specular image. Further, the transpose is given as results (R on top).
M = [L(:,1:end-1) fliplr(R(:,1:end-1))]';
end

function h = circle(x,y,r)
th = 0:pi/50:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
h = plot(xunit, yunit, 'k');
end

function exp_sim_55
%% Load data
load datos/9_55_exp_pure.mat
load datos/9_C.mat
%% Set simulation probe vectors
% Coordinates for GRID_C for comparison with comsol
% (0.210, 0.123)
Ta2Sim = squeeze(0.5*(phi_tr(229,146,1,:) + phi_tr(230,146,1,:)));
% (0.310, 0.123)
Ta3Sim = squeeze(0.5*(phi_tr(347,146,1,:) + phi_tr(348,146,1,:)));
% (0.420, 0.123)
Ta4Sim = squeeze(phi_tr(477,146,1,:));
% (0.78, :)
Ta5Sim = mean(squeeze(phi_tr(end-1,:,1,:)),1);

% (0.160, 0.175)
Tc1Sim = squeeze(phi_tr(171,end,1,:));
% (0.260, 0.175)
Tc2Sim = 0.5*(squeeze(phi_tr(288,end,1,:)) + squeeze(phi_tr(289,end,1,:)));
% (0.360, 0.175)
Tc3Sim = squeeze(phi_tr(406,end,1,:));

% Coordinates for GRID_A
% Ta5Sim = mean(squeeze(phi_tr(end-1,:,1,:)),1);
% Ta4Sim = squeeze(phi_tr(279,end,1,:));
% Ta3Sim = squeeze(phi_tr(203,end,1,:));
% Ta2Sim = squeeze(phi_tr(134,end,1,:));
% 
% Tc3Sim = 0.25*(...
%     squeeze(phi_tr(237,88,1,:)) + squeeze(phi_tr(238,88,1,:)) + ...
%     squeeze(phi_tr(237,89,1,:)) + squeeze(phi_tr(238,89,1,:)));
% Tc2Sim = 0.25*(...
%     squeeze(phi_tr(168,88,1,:)) + squeeze(phi_tr(169,88,1,:)) + ...
%     squeeze(phi_tr(168,89,1,:)) + squeeze(phi_tr(169,89,1,:)));
% Tc1Sim = 0.25*(...
%     squeeze(phi_tr(99,88,1,:)) + squeeze(phi_tr(100,88,1,:)) + ...
%     squeeze(phi_tr(99,89,1,:)) + squeeze(phi_tr(100,89,1,:)));

tsim = 2*length(Tc1Sim);
% Step for experimental data (6 == 1 (min), 12 == 2 (min))
stride = 12;
%% Air temperatures
figure;
hold on
% Exps
% plot(t(1:stride:end)/60, Ta1(1:stride:end), '-sm', 'MarkerSize', 3, ...
%     'MarkerFaceColor', 'm');
plot(t(12:stride:end)/60, Ta2(12:stride:end) - Ta1(12:stride:end) + 25.77, 'sk', 'MarkerSize', 3);
plot(t(12:stride:end)/60, Ta3(12:stride:end) - Ta1(12:stride:end) + 25.77, 'or', 'MarkerSize', 3);
plot(t(12:stride:end)/60, Ta4(12:stride:end) - Ta1(12:stride:end) + 25.77, 'vb', 'MarkerSize', 3);
% Sims
plot(2:2:tsim, Ta2Sim-273.15, 'k');
plot(2:2:tsim, Ta3Sim-273.15, '--r');
plot(2:2:tsim, Ta4Sim-273.15, '-.b');
plot(2:2:tsim, Ta5Sim-273.15, '-+m');
hold off
% set(gca, 'XLim', [0 tsim+1], 'YLim', [20 36]);
xlabel('t [min]'); ylabel('T [ºC]');
% title('SPA 55%, pure wax, top');
box on
legend(...
    'Exp: T_{a2}', 'Exp: T_{a3}', 'Exp: T_{a4}', ...
    'Sim: T_{a2}', 'Sim: T_{a3}', 'Sim: T_{a4}', 'Sim: T_{a5}');
Graph_Format
%% Wax temperatures
figure;
hold on
% Exps
plot(t(12:stride:end)/60, Tc1(12:stride:end), 'ok', 'MarkerSize', 3);
% plot(t(12:stride:end)/60, Tc2(12:stride:end), 'vr', 'MarkerSize', 3);
plot(t(12:stride:end)/60, Tc3(12:stride:end), 'sb', 'MarkerSize', 3);
% Sims
plot(2:2:tsim, Tc1Sim-273.15, 'k');
% plot(2:2:tsim, Tc2Sim-273.15, '-r');
plot(2:2:tsim, Tc3Sim-273.15, '-.b');

hold off
% set(gca, 'XLim', [0 tsim+1], 'YLim', [58 72]);
xlabel('t [min]'); ylabel('T [ºC]');
% title('SPA 55%, pure wax, top');
box on
legend(...
    'Exp: T_{c1}', 'Exp: T_{c2}', ...
    'Sim: T_{c1}', 'Sim: T_{c2}');
Graph_Format
%% Errors
% Air
Ea = abs(Ta2(12:stride:end) - Ta1(12:stride:end) + 25.77 - (Ta2Sim(1:end-1) - 273.15));
fprintf('Ea Ta2: av = %.2f, max = %.2f, var = %.2g\n', ...
    mean(Ea), max(Ea), var(Ea));
Ea = abs(Ta3(12:stride:end) - Ta1(12:stride:end) + 25.77 - (Ta3Sim(1:end-1) - 273.15));
fprintf('Ea Ta3: av = %.2f, max = %.2f, var = %.2g\n', ...
    mean(Ea), max(Ea), var(Ea));
Ea = abs(Ta4(12:stride:end) - Ta1(12:stride:end) + 25.77 - (Ta4Sim(1:end-1) - 273.15));
fprintf('Ea Ta4: av = %.2f, max = %.2f, var = %.2g\n', ...
    mean(Ea), max(Ea), var(Ea));
% Wax
Ea = abs(Tc1(12:stride:end) - (Tc1Sim(1:end-1) - 273.15));
fprintf('Ea Tc1: av = %.2f, max = %.2f, var = %.2g\n', ...
    mean(Ea), max(Ea), var(Ea));
Ea = abs(Tc3(12:stride:end) - (Tc3Sim(1:end-1) - 273.15));
fprintf('Ea Tc3: av = %.2f, max = %.2f, var = %.2g\n', ...
    mean(Ea), max(Ea), var(Ea));
end

function COMSOL_comparison
%% Load data
load datos/9_COMSOL.mat
load datos/9_C.mat
%% Set simulation probe vectors
% Coordinates for GRID_C for comparison with comsol
% (0.210, 0.123)
Ta2Sim = squeeze(0.5*(phi_tr(229,146,1,:) + phi_tr(230,146,1,:)));
% (0.310, 0.123)
Ta3Sim = squeeze(0.5*(phi_tr(347,146,1,:) + phi_tr(348,146,1,:)));
% (0.420, 0.123)
Ta4Sim = squeeze(phi_tr(477,146,1,:));
% (0.78, :)
Ta5Sim = mean(squeeze(phi_tr(end-1,:,1,:)),1);

% (0.160, 0.175)
Tc1Sim = squeeze(phi_tr(171,end,1,:));
% (0.260, 0.175)
Tc2Sim = 0.5*(squeeze(phi_tr(288,end,1,:)) + squeeze(phi_tr(289,end,1,:)));
% (0.360, 0.175)
Tc3Sim = squeeze(phi_tr(406,end,1,:));

tsim = length(Tc1Sim);
%% Air temperatures
figure;
hold on
% COMSOL
plot(0:2:60, Ta2_COMSOL3D, 'sk', 'MarkerSize', 3, 'MarkerFaceColor', 'k');
plot(0:2:60, Ta3_COMSOL3D, 'or', 'MarkerSize', 3, 'MarkerFaceColor', 'r');
plot(0:2:60, Ta4_COMSOL3D, 'vb', 'MarkerSize', 3, 'MarkerFaceColor', 'b');
plot(0:2:60, Ta2_COMSOL, 'sk', 'MarkerSize', 3, 'MarkerFaceColor', 'w');
plot(0:2:60, Ta3_COMSOL, 'or', 'MarkerSize', 3, 'MarkerFaceColor', 'w');
plot(0:2:60, Ta4_COMSOL, 'vb', 'MarkerSize', 3, 'MarkerFaceColor', 'w');
% Sims
plot(1:tsim, Ta2Sim-273.15, 'k');
plot(1:tsim, Ta3Sim-273.15, '--r');
plot(1:tsim, Ta4Sim-273.15, 'b');
plot(1:tsim, Ta5Sim-273.15, '-.m');
hold off
set(gca, 'XLim', [0 tsim+1]);
xlabel('t [min]'); ylabel('T [ºC]');
box on
legend(... 
    '3D: T_{a2}', '3D: T_{a3}', '3D: T_{a4}', ...
    '2D: T_{a2}', '2D: T_{a3}', '2D: T_{a4}', ...
    'Sim: T_{a2}', 'Sim: T_{a3}', 'Sim: T_{a4}', 'Sim: T_{a5}');
Graph_Format
%% Wax temperatures
figure;
hold on
% COMSOL
plot(0:2:60, Tc1_COMSOL3D, 'sk', 'MarkerSize', 3, 'MarkerFaceColor', 'k');
plot(0:2:60, Tc2_COMSOL3D, 'or', 'MarkerSize', 3, 'MarkerFaceColor', 'r');
plot(0:2:60, Tc3_COMSOL3D, 'vb', 'MarkerSize', 3, 'MarkerFaceColor', 'b');
plot(0:2:60, Tc1_COMSOL, 'sk', 'MarkerSize', 3, 'MarkerFaceColor', 'w');
plot(0:2:60, Tc2_COMSOL, 'or', 'MarkerSize', 3, 'MarkerFaceColor', 'w');
plot(0:2:60, Tc3_COMSOL, 'vb', 'MarkerSize', 3, 'MarkerFaceColor', 'w');
% Sims
plot(1:tsim, Tc1Sim-273.15, '--k');
plot(1:tsim, Tc2Sim-273.15, '-r');
plot(1:tsim, Tc3Sim-273.15, '-.b');

hold off
set(gca, 'XLim', [0 tsim+1]);
xlabel('t [min]'); ylabel('T [ºC]');
box on
legend(... 
    '3D: T_{c1}', '3D: T_{c2}', '3D: T_{c3}', ...
    '2D: T_{c1}', '2D: T_{c2}', '2D: T_{c3}', ...
    'Sim: T_{c1}', 'Sim: T_{c2}', 'Sim: T_{c3}');
Graph_Format
end