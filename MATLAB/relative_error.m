
cd ./CPU/
clear all

%Determinar archivos a analizar

f=dir('phi_nt*.dat');

if size(f,1)==0
 error('No existen archivos analizables');
end

n_of_files=size(f,1);

%Extraer valores de phi a una celda
for i=1:n_of_files
    j=strfind(f(i).name,'.dat');
    names{i}=strcat(f(i).name(1:j-1),'_data.m'); 
    s=copyfile(f(i).name,names{i});
    run(names{i});
    phi{i}=eval(strcat(f(i).name(1:j-1),'_cpu_var'));
end

%Asume que el primer archivo es la base de comparación!!!

phi{1}=phi{1}+eps;

fprintf('Error relativo - absoluto\nn')
fprintf('Archivo base de comparacion: %20s\n\n',names{1})


for i=2:n_of_files
    max_rel_error=max(max(abs((phi{1}-phi{i})./phi{1})));
    max_abs_error=max(max(abs(phi{1}-phi{i})));
    fprintf('File: %20s, Max_rel_error: %9.4f Max_abs_error: %9.4f\n',names{i},max_rel_error,max_abs_error)
end

%Comando opcional para eliminar todos los archivos
%!rm *.m

