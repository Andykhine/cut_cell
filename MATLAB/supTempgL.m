% Gráficos para mostrar los resultados para el flujo en el acumulador.

% Archivos requeridos
% en carpeta 
% /home/francisco/Documents/Tesis/produccion/pcOficina/resultados/acumulador/energiaProd/v1.5/turbulentoCont/25-30min
% nodes_x.dat nodes_y.dat
% phi_tr.dat gL_tr.dat
% dom_matrix.dat

run setDatos/simulacion/v15/25-30min/nodes_x.m
run setDatos/simulacion/v15/25-30min/nodes_y.m
run setDatos/simulacion/v15/25-30min/phi_tr.m
run setDatos/simulacion/v15/25-30min/gL_tr.m
run setDatos/simulacion/v15/25-30min/dom_matrix.m

%% Control flags

% Flags que controlan la parte gráfica

close all

plotTemperatureMagnitud = 1;
plotgLMagnitud = 1;
plotAirTemperatureMagnitud = 1;
plotCombined = 1;

reduceData = 0;

index = 3;

%% Data short names

nodesX = nodes_x_cpu_var;
nodesY = nodes_y_cpu_var;
Temp = phi_tr_cpu_var(:,:,index);
gL = gL_tr_cpu_var(:,:,index);
domMatrix = dom_matrix_cpu_var;


%% Data reduction
if reduceData
  nodesX(2:2:end, :) = []; nodesX(:, 2:2:end) = [];
  nodesY(2:2:end, :) = []; nodesY(:, 2:2:end) = [];
  Temp(2:2:end, :) = []; Temp(:,2:2:end) = [];
  gL(2:2:end, :) = []; gL(:,2:2:end) = [];
  domMatrix(2:2:end, :) = []; domMatrix(:, 2:2:end) = [];
end

%% gL Magnitud

if plotgLMagnitud
  h1 = figure(1);
  tmp = gL;
  tmp(domMatrix == 0) = nan;
  surf(nodesX(2:end-1,2:end-1), nodesY(2:end-1,2:end-1), tmp(2:end-1,2:end-1), ...
    'EdgeColor', 'none')
  view(2)
  colorbar()
  colormap parula
  grid off
  box on
  axis equal
  axis([0 0.52 0 0.175])
  xlabel('x[m]')
  ylabel('y[m]')
  tmp = get(gcf, 'Children');
  tmp2 = get(tmp(1), 'Label');
  set(tmp2, 'String', '[-]')
end

%% Temperature magnitud

if plotTemperatureMagnitud
  h2 = figure(2);
  surf(nodesX(2:end-1,2:end-1), nodesY(2:end-1,2:end-1), ...
    Temp(2:end-1,2:end-1), 'EdgeColor', 'none')
  view(2)
  colorbar()
  colormap parula
  grid off
  box on
  axis equal
  axis([0 0.52 0 0.175])
  xlabel('x[m]')
  ylabel('y[m]')
  tmpChildren = get(gcf, 'Children');
  tmpLabel = get(tmpChildren(1), 'Label');
  set(tmpLabel, 'String', '[C]');
end

if plotAirTemperatureMagnitud
  h3 = figure(3);
  tmp = Temp;
  tmp(domMatrix ~= 0) = nan;
  surf(nodesX(2:end-1,2:end-1), nodesY(2:end-1,2:end-1), ...
    tmp(2:end-1,2:end-1), 'EdgeColor', 'none')
  view(2)
  colorbar()
  colormap parula
  grid off
  box on
  axis equal
  axis([0 0.52 0 0.175])
  xlabel('x[m]')
  ylabel('y[m]')
  tmpChildren = get(gcf, 'Children');
  tmpLabel = get(tmpChildren(1), 'Label');
  set(tmpLabel, 'String', '[C]');
end

if plotCombined
  h4 = figure(4);
  subplot(2,1,1);
  tmp = gL;
  tmp(domMatrix == 0) = nan;
  surf(nodesX(2:end-1,2:end-1), nodesY(2:end-1,2:end-1), tmp(2:end-1,2:end-1), ...
    'EdgeColor', 'none')
  view(2)
  colorbar()
  colormap parula
  grid off
  box on
  axis equal
  axis([0 0.52 0 0.175])
  xlabel('x[m]')
  ylabel('y[m]')
  tmp = get(gcf, 'Children');
  tmp2 = get(tmp(1), 'Label');
  set(tmp2, 'String', '[-]')
  %
  subplot(2,1,2);
   surf(nodesX(2:end-1,2:end-1), nodesY(2:end-1,2:end-1), ...
    Temp(2:end-1,2:end-1) - 273, 'EdgeColor', 'none')
  view(2)
  colorbar()
  colormap parula
  grid off
  box on
  axis equal
  axis([0 0.52 0 0.175])
  xlabel('x[m]')
  ylabel('y[m]') 
  tmp = get(gcf, 'Children');
  tmp2 = get(tmp(1), 'Label');
  set(tmp2, 'String', '[C]')
end