function acumulador  %#ok<*NODEF,*NASGU,*COLND>
%% Clean space
clc
clear
close all
%% Call grid independence
% grid_independence_flow;
% grid_independence_phi;
%% Call discharge graphs
acumulador_55;
% acumulador_90;
%% Call flow field
% SPA_flow_field_full;
end

function grid_independence_flow
%% Load data
% COMSOL
load datos/COMSOL_Flow.mat

% A
load datos/GRID_A.mat

k_A = squeeze(k_turb(2:end-1,end));
e_A = squeeze(eps_turb(2:end-1,end));
k2_A = squeeze(k_turb(134,2:end-1));
e2_A = squeeze(eps_turb(134,2:end-1));
U2_A = squeeze(U(134,2:end-1));
V2_A = squeeze(V(134,2:end-1));
vel2_A = sqrt(U2_A.^2 + V2_A.^2);
vel_A = sqrt(U(2:end-1,end-1).^2 + V(2:end-1,end-1).^2);
x_A = squeeze(x(2:end-1,1));
y_A = squeeze(y(1,2:end-1));

% B
load datos/GRID_B.mat

k_B = squeeze(k_turb(2:end-1,end));
e_B = squeeze(eps_turb(2:end-1,end));
k2_B = squeeze(0.5*(k_turb(184,2:end-1) + k_turb(185,2:end-1)));
e2_B = squeeze(0.5*(eps_turb(184,2:end-1) + eps_turb(185,2:end-1)));
U2_B = squeeze(0.5*(U(184,2:end-1) + U(185,2:end-1)));
V2_B = squeeze(0.5*(V(184,2:end-1) + V(185,2:end-1)));
vel2_B = sqrt(U2_B.^2 + V2_B.^2);
vel_B = sqrt(U(2:end-1,end-1).^2 + V(2:end-1,end-1).^2);
x_B = squeeze(x(2:end-1,1));
y_B = squeeze(y(1,2:end-1));

% C
load datos/GRID_C.mat

k_C = squeeze(k_turb(2:end-1,end));
e_C = squeeze(eps_turb(2:end-1,end));
k2_C = squeeze(0.5*(k_turb(229,2:end-1) + k_turb(230,2:end-1)));
e2_C = squeeze(0.5*(eps_turb(229,2:end-1) + eps_turb(230,2:end-1)));
U2_C = squeeze(0.5*(U(229,2:end-1) + U(230,2:end-1)));
V2_C = squeeze(0.5*(V(229,2:end-1) + V(230,2:end-1)));
vel2_C = sqrt(U2_C.^2 + V2_C.^2);
vel_C = sqrt(U(2:end-1,end-1).^2 + V(2:end-1,end-1).^2);
x_C = squeeze(x(2:end-1,1));
y_C = squeeze(y(1,2:end-1));

% % D
% load datos/GRID_D.mat
%
% k_D = squeeze(k_turb(2:end-1,end));
% e_D = squeeze(eps_turb(2:end-1,end));
% k2_D = squeeze(0.5*(k_turb(274,2:end-1) + k_turb(275,2:end-1)));
% e2_D = squeeze(0.5*(eps_turb(274,2:end-1) + eps_turb(275,2:end-1)));
% U2_D = squeeze(0.5*(U(274,2:end-1) + U(275,2:end-1)));
% V2_D = squeeze(0.5*(V(274,2:end-1) + V(275,2:end-1)));
% vel2_D = sqrt(U2_D.^2 + V2_D.^2);
% vel_D = sqrt(U(2:end-1,end-1).^2 + V(2:end-1,end-1).^2);
% x_D = squeeze(x(2:end-1,1));
% y_D = squeeze(y(1,2:end-1));

% % E
% load datos/GRID_E.mat
%
% k_E = squeeze(k_turb(2:end-1,end));
% e_E = squeeze(eps_turb(2:end-1,end));
% k2_E = squeeze(0.5*(k_turb(304,2:end-1) + k_turb(305,2:end-1)));
% e2_E = squeeze(0.5*(eps_turb(304,2:end-1) + eps_turb(305,2:end-1)));
% U2_E = squeeze(0.5*(U(304,2:end-1) + U(305,2:end-1)));
% V2_E = squeeze(0.5*(V(304,2:end-1) + V(305,2:end-1)));
% vel2_E = sqrt(U2_E.^2 + V2_E.^2);
% vel_E = sqrt(U(2:end-1,end-1).^2 + V(2:end-1,end-1).^2);
% x_E = squeeze(x(2:end-1,1));
% y_E = squeeze(y(1,2:end-1));
%% Graphs
% vel2
figure
hold on
plot(y_COMSOL, vel2_COMSOL, '--ok', 'MarkerFaceColor', 'w');
plot(y_A, vel2_A, 'b');
plot(y_B, vel2_B, 'r');
plot(y_C, vel2_C, 'k');
% plot(y_D, vel_D, 'g');
% plot(y_E, vel_E, 'm');
hold off
xlabel('y [m]'); ylabel('Vel [m/s]');
set(gca, 'XLim', [-0.005 0.18]);
box on
grid off
legend('COMSOL', 'A', 'B', 'C', 'D', 'E', 'Location', 'se');
Graph_Format;
% vel
figure
hold on
plot(x_COMSOL, vel_COMSOL, '--ok', 'MarkerFaceColor', 'w');
plot(x_A, vel_A, 'b');
plot(x_B, vel_B, 'r');
plot(x_C, vel_C, 'k');
% plot(x_D, vel_D, 'g');
% plot(x_E, vel_E, 'm');
hold off
xlabel('x [m]'); ylabel('Vel [m/s]');
set(gca, 'XLim', [-0.005 0.785]);
box on
grid off
legend('COMSOL', 'A', 'B', 'C', 'D', 'E', 'Location', 'se');
Graph_Format;
% k2
figure
hold on
plot(y_COMSOL, k2_COMSOL, '--ok', 'MarkerFaceColor', 'w');
plot(y_A, k2_A, 'b');
plot(y_B, k2_B, 'r');
plot(y_C, k2_C, 'k');
% plot(y_D, k2_D, 'g');
% plot(y_E, k2_E, 'm');
hold off
xlabel('y [m]'); ylabel('k [m^2/s^2]');
set(gca, 'XLim', [-0.005 0.18]);
box on
grid off
legend('COMSOL', 'A', 'B', 'C', 'D', 'E', 'Location', 'nw');
Graph_Format;
% e2
figure
hold on
plot(y_COMSOL, e2_COMSOL, '--ok', 'MarkerFaceColor', 'w');
plot(y_A, e2_A, 'b');
plot(y_B, e2_B, 'r');
plot(y_C, e2_C, 'k');
% plot(y_D, e2_D, 'g');
% plot(y_E, e2_E, 'm');
hold off
xlabel('y [m]'); ylabel('e [m^2/s^3]');
set(gca, 'XLim', [-0.005 0.18]);
box on
grid off
legend('COMSOL', 'A', 'B', 'C', 'D', 'E', 'Location', 'nw');
Graph_Format;
% k
figure
hold on
plot(x_COMSOL, k_COMSOL, '--ok', 'MarkerFaceColor', 'w');
plot(x_A, k_A, 'b');
plot(x_B, k_B, 'r');
plot(x_C, k_C, 'k');
% plot(y_D, k2_D, 'g');
% plot(y_E, k2_E, 'm');
hold off
xlabel('x [m]'); ylabel('k [m^2/s^2]');
set(gca, 'XLim', [-0.005 0.785]);
box on
grid off
legend('COMSOL', 'C', 'F', 'G', 'Location', 'nw');
Graph_Format;
% e
figure
hold on
plot(x_COMSOL, e_COMSOL, '--ok', 'MarkerFaceColor', 'w');
plot(x_A, e_A, 'b');
plot(x_B, e_B, 'r');
plot(x_C, e_C, 'k');
% plot(x_D, e_D, 'g');
% plot(x_E, e_E, 'm');
hold off
xlabel('x [m]'); ylabel('e [m^2/s^3]');
set(gca, 'XLim', [-0.005 0.785]);
box on
grid off
legend('COMSOL', 'A', 'B', 'C', 'D', 'E', 'Location', 'nw');
Graph_Format;
end

function grid_independence_phi
%% Load data
% 0: 10 (s)
load datos/dt0.mat

Ta5_0 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_0 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 1: 1 (s)
load datos/dt1.mat

Ta5_1 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_1 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 2: 0.25 (s)
load datos/dt2.mat

Ta5_2 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_2 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 3: 0.1 (s)
load datos/dt3.mat

Ta5_3 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_3 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));

% 4: 0.01 (s)
load datos/dt4.mat

Ta5_4 = squeeze(phi_tr(end,end-1,1,1:5));
Ta2_4 = squeeze(0.5*(phi_tr(229,end,1,1:5) + phi_tr(230,end,1,1:5)));
%% Graphs
figure
hold on
plot(1:5, Ta5_0 - 273.15, '-ks', 'MarkerFaceColor', 'w');
plot(1:5, Ta5_1 - 273.15, '--ko', 'MarkerFaceColor', 'k');
plot(1:5, Ta5_2 - 273.15, '--ro', 'MarkerFaceColor', 'w');
plot(1:5, Ta5_3 - 273.15, '-ko', 'MarkerFaceColor', 'w');
plot(1:5, Ta5_4 - 273.15, '--kv', 'MarkerFaceColor', 'w');
hold off
xlabel('t [min]'); ylabel('T_{a5} [ºC]');
set(gca, 'XLim', [0 6], 'YLim', [19 31]);
box on
grid off
legend('10@2000', '1@2000', '0.25@640', ...
    '0.1@2000', '0.01@2000', 'Location', 'e');
Graph_Format;
end

function SPA_flow_field_full
%% Load data
r = sqrt(0.00109);

load datos/SPA_55_simulation.mat

velL = sqrt(U.^2 + V.^2);
velL(dom_matrix ~= 0) = nan;

pL = p;
pL(dom_matrix ~= 0) = nan;

% load datos/SPA_55_simulation.mat

velR = sqrt(U.^2 + V.^2);
velR(dom_matrix ~= 0) = nan;

pR = p;
pR(dom_matrix ~= 0) = nan;
%% Joining data in a specular way
vel = join_array(velL, velR);
p = join_array(pL, pR);
% Dont consider data on simmetry line
tempX = join_array(x,x);
tempY = [y(:,1:end-1) y(:,2:end)+0.175]';
%% Graphs flow field
% vel zoom
figure
hold on
circle(0.16, 0.04, r);
circle(0.26, 0.04, r);
circle(0.36, 0.04, r);
circle(0.16, 0.13, r);
circle(0.26, 0.13, r);
circle(0.36, 0.13, r);
circle(0.16, 0.22, r);
circle(0.26, 0.22, r);
circle(0.36, 0.22, r);
circle(0.16, 0.31, r);
circle(0.26, 0.31, r);
circle(0.36, 0.31, r);
[C, h] = contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
    vel(2:end-1,2:end-1), 'Fill', 'on');
axis equal
set(gca, 'XLim', [0.1 0.41], 'YLim', [0 0.175]);
colorbar('NorthOutside', 'Color', 'k');
colormap jet
grid off
box on
xlabel('x [m]');
ylabel('y [m]');
tmpChildren = get(gcf, 'Children');
tmpLabel = get(tmpChildren(1), 'Label');
set(tmpLabel, 'String', '[m/s]');
Graph_Format;
clabel(C,h,'manual',...
    'Fontweight', 'bold', 'FontName', 'palatino', 'FontSize', 10, ...
    'Color','k','EdgeColor','none','BackgroundColor','w');
hold off
%
% % vel
% figure
% hold on
% circle(0.16, 0.04, r);
% circle(0.26, 0.04, r);
% circle(0.36, 0.04, r);
% circle(0.16, 0.13, r);
% circle(0.26, 0.13, r);
% circle(0.36, 0.13, r);
% circle(0.16, 0.22, r);
% circle(0.26, 0.22, r);
% circle(0.36, 0.22, r);
% circle(0.16, 0.31, r);
% circle(0.26, 0.31, r);
% circle(0.36, 0.31, r);
% axis equal
% contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
%     vel(2:end-1,2:end-1), 'Fill', 'on');
% colorbar('NorthOutside', 'Color', 'k');
% colormap jet
% grid off
% box on
% axis equal
% xlabel('x [m]');
% ylabel('y [m]');
% tmpChildren = get(gcf, 'Children');
% tmpLabel = get(tmpChildren(1), 'Label');
% set(tmpLabel, 'String', '[m/s]');
% Graph_Format;
% hold off
%% Graphs pressure field
% % p zoom
% figure
% hold on
% circle(0.16, 0.04, r);
% circle(0.26, 0.04, r);
% circle(0.36, 0.04, r);
% circle(0.16, 0.13, r);
% circle(0.26, 0.13, r);
% circle(0.36, 0.13, r);
% circle(0.16, 0.22, r);
% circle(0.26, 0.22, r);
% circle(0.36, 0.22, r);
% circle(0.16, 0.31, r);
% circle(0.26, 0.31, r);
% circle(0.36, 0.31, r);
% [C, h] = contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
%     p(2:end-1,2:end-1), 'Fill', 'on');
% axis equal
% set(gca, 'XLim', [0.1 0.41], 'YLim', [0 0.175]);
% colorbar('NorthOutside', 'Color', 'k');
% colormap jet
% grid off
% box on
% xlabel('x [m]');
% ylabel('y [m]');
% tmpChildren = get(gcf, 'Children');
% tmpLabel = get(tmpChildren(1), 'Label');
% set(tmpLabel, 'String', '[Pa]');
% Graph_Format;
% clabel(C,h,'manual',...
%     'Fontweight', 'bold', 'FontName', 'palatino', 'FontSize', 10, ...
%     'Color','k','EdgeColor','none','BackgroundColor','w');
% hold off
% % set(gcf, 'Position', [9.8425 4.4715 22.0927 16.0073]);
%
% % p
% figure
% hold on
% circle(0.16, 0.04, r);
% circle(0.26, 0.04, r);
% circle(0.36, 0.04, r);
% circle(0.16, 0.13, r);
% circle(0.26, 0.13, r);
% circle(0.36, 0.13, r);
% circle(0.16, 0.22, r);
% circle(0.26, 0.22, r);
% circle(0.36, 0.22, r);
% circle(0.16, 0.31, r);
% circle(0.26, 0.31, r);
% circle(0.36, 0.31, r);
% axis equal
% contourf(tempX(2:end-1,2:end-1), tempY(2:end-1,2:end-1), ...
%     p(2:end-1,2:end-1), 'Fill', 'on');
% colorbar('NorthOutside', 'Color', 'k');
% colormap jet
% grid off
% box on
% axis equal
% xlabel('x [m]');
% ylabel('y [m]');
% tmpChildren = get(gcf, 'Children');
% tmpLabel = get(tmpChildren(1), 'Label');
% set(tmpLabel, 'String', '[Pa]');
% Graph_Format;
% hold off
end

function M = join_array(L, R)
% Joins matrices L and R to the left and right with the right being the
% specular image. Further, the transpose is given as results (R on top).
M = [L(:,1:end-1) fliplr(R(:,1:end-1))]';
end

function h = circle(x,y,r)
th = 0:pi/50:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
h = plot(xunit, yunit, 'k');
end

function acumulador_90
%% Load data
load datos/SPA_90_new_experiment.mat
load datos/SPA_90_simulation.mat
%% Set simulation probe vectors
% Ta5Sim = mean(squeeze(phi_tr(end-1,:,1,:)),1);
Ta4Sim = squeeze(phi_tr(477,208,1,:));
Ta3Sim = 0.5*(squeeze(phi_tr(347,208,1,:)) + squeeze(phi_tr(348,208,1,:)));
Ta2Sim = 0.5*(squeeze(phi_tr(229,208,1,:)) + squeeze(phi_tr(230,208,1,:)));

Tc3Sim = 0.25*(...
    squeeze(phi_tr(406,154,1,:)) + squeeze(phi_tr(407,154,1,:)) + ...
    squeeze(phi_tr(406,155,1,:)) + squeeze(phi_tr(407,155,1,:)));
Tc2Sim = 0.25*(...
    squeeze(phi_tr(288,154,1,:)) + squeeze(phi_tr(289,154,1,:)) + ...
    squeeze(phi_tr(288,155,1,:)) + squeeze(phi_tr(289,155,1,:)));
Tc1Sim = 0.25*(...
    squeeze(phi_tr(170,154,1,:)) + squeeze(phi_tr(171,154,1,:)) + ...
    squeeze(phi_tr(170,155,1,:)) + squeeze(phi_tr(171,155,1,:)));
% Step for experimental data
stride = 5;
%% Air temperatures
figure;
hold on
% Exps
plot(t(1:stride:end)/60, Ta2v1(1:stride:end)-Ta1v1(1:stride:end)+20, 'sk', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Ta3v1(1:stride:end)-Ta1v1(1:stride:end)+20, 'or', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Ta4(1:stride:end)-Ta1v1(1:stride:end)+20, 'vb', 'MarkerSize', 3);
% plot(t(1:stride:end)/60, Ta5(1:stride:end)-Ta1v2(1:stride:end)+20, 'sk', 'MarkerSize', 3);
% Sims
plot(1:150, Ta2Sim-273.15, 'k');
plot(1:150, Ta3Sim-273.15, '--r');
plot(1:150, Ta4Sim-273.15, '-b');
% plot(1:150, Ta5Sim-273.15, '-.k');
hold off
set(gca, 'XLim', [0 150], 'YLim', [18 38]);
xlabel('t [min]'); ylabel('T [ºC]');
title('SPA 90%');
box on
legend(...
    'Exp: T_{a2}', 'Exp: T_{a3}', 'Exp: T_{a4}', ...
    'Sim: T_{a2}', 'Sim: T_{a3}', 'Sim: T_{a4}');
Graph_Format
%% Wax temperatures
figure;
hold on
% Exps
plot(t(1:stride:end)/60, Tc1(1:stride:end), 'or', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Tc2(1:stride:end), 'vb', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Tc3(1:stride:end), 'sk', 'MarkerSize', 3);
% Sims
plot(1:150, Tc1Sim-273.15, '--r');
plot(1:150, Tc2Sim-273.15, '-b');
plot(1:150, Tc3Sim-273.15, '-.k');

hold off
set(gca, 'XLim', [0 150], 'YLim', [15 65]);
xlabel('t [min]'); ylabel('T [ºC]');
title('SPA 90%');
box on
legend(...
    'Exp: T_{c1}', 'Exp: T_{c2}', 'Exp: T_{c3}', ...
    'Sim: T_{c1}', 'Sim: T_{c2}', 'Sim: T_{c3}');
Graph_Format
end

function acumulador_55
%% Load data
load datos/SPA55_Exp_pure_wax
load datos/b2.mat
%% Set simulation probe vectors
% Coordinates for GRID_C
Ta5Sim = mean(squeeze(phi_tr(end-1,:,1,:)),1);
Ta4Sim = squeeze(phi_tr(477,end,1,:));
Ta3Sim = squeeze(0.5*(phi_tr(347,end,1,:) + phi_tr(348,end,1,:)));
Ta2Sim = squeeze(0.5*(phi_tr(229,end,1,:) + phi_tr(230,end,1,:)));

Tc3Sim = 0.25*(...
    squeeze(phi_tr(406,154,1,:)) + squeeze(phi_tr(407,154,1,:)) + ...
    squeeze(phi_tr(406,155,1,:)) + squeeze(phi_tr(407,155,1,:)));
Tc2Sim = 0.25*(...
    squeeze(phi_tr(288,154,1,:)) + squeeze(phi_tr(289,154,1,:)) + ...
    squeeze(phi_tr(288,155,1,:)) + squeeze(phi_tr(289,155,1,:)));
Tc1Sim = 0.25*(...
    squeeze(phi_tr(170,154,1,:)) + squeeze(phi_tr(171,154,1,:)) + ...
    squeeze(phi_tr(170,155,1,:)) + squeeze(phi_tr(171,155,1,:)));

% Coordinates for GRID_A
% Ta5Sim = mean(squeeze(phi_tr(end-1,:,1,:)),1);
% Ta4Sim = squeeze(phi_tr(279,end,1,:));
% Ta3Sim = squeeze(phi_tr(203,end,1,:));
% Ta2Sim = squeeze(phi_tr(134,end,1,:));
% 
% Tc3Sim = 0.25*(...
%     squeeze(phi_tr(237,88,1,:)) + squeeze(phi_tr(238,88,1,:)) + ...
%     squeeze(phi_tr(237,89,1,:)) + squeeze(phi_tr(238,89,1,:)));
% Tc2Sim = 0.25*(...
%     squeeze(phi_tr(168,88,1,:)) + squeeze(phi_tr(169,88,1,:)) + ...
%     squeeze(phi_tr(168,89,1,:)) + squeeze(phi_tr(169,89,1,:)));
% Tc1Sim = 0.25*(...
%     squeeze(phi_tr(99,88,1,:)) + squeeze(phi_tr(100,88,1,:)) + ...
%     squeeze(phi_tr(99,89,1,:)) + squeeze(phi_tr(100,89,1,:)));

tsim = length(Tc1Sim);
% Step for experimental data
stride = 5;
%% Air temperatures
figure;
hold on
% Exps
plot(t(1:stride:end)/60, Ta2(1:stride:end), 'sk', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Ta3(1:stride:end), 'or', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Ta4(1:stride:end), 'vb', 'MarkerSize', 3);
% Sims
plot(1:tsim, Ta2Sim-273.15, 'k');
plot(1:tsim, Ta3Sim-273.15, '--r');
plot(1:tsim, Ta4Sim-273.15, 'b');
% plot(1:tsim, Ta5Sim-273.15, '-.m');
hold off
set(gca, 'XLim', [0 tsim+1], 'YLim', [20 36]);
xlabel('t [min]'); ylabel('T [ºC]');
% title('SPA 55%');
box on
legend(...
    'Exp: T_{a2}', 'Exp: T_{a3}', 'Exp: T_{a4}', ...
    'Sim: T_{a2}', 'Sim: T_{a3}', 'Sim: T_{a4}', 'Sim: T_{a5}');
Graph_Format
%% Wax temperatures
figure;
hold on
% Exps
plot(t(1:stride:end)/60, Tc1(1:stride:end), 'or', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Tc2(1:stride:end), 'vb', 'MarkerSize', 3);
plot(t(1:stride:end)/60, Tc3(1:stride:end), 'sk', 'MarkerSize', 3);
% Sims
plot(1:tsim, Tc1Sim-273.15, '--r');
plot(1:tsim, Tc2Sim-273.15, '-b');
plot(1:tsim, Tc3Sim-273.15, '-.k');

hold off
set(gca, 'XLim', [0 tsim+1], 'YLim', [58 72]);
xlabel('t [min]'); ylabel('T [ºC]');
% title('SPA 55%');
box on
legend(...
    'Exp: T_{c1}', 'Exp: T_{c2}', 'Exp: T_{c3}', ...
    'Sim: T_{c1}', 'Sim: T_{c2}', 'Sim: T_{c3}');
Graph_Format
end