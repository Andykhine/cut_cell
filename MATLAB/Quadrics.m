function Quadrics
close all

set(groot,'DefaultAxesTickLabelInterpreter','Latex');
set(groot,'DefaultLegendInterpreter','Latex');
%% Inputs
lambda_1 = 1;
lambda_2 = 1;
lambda_3 = 1;
angle_x = 0;
angle_y = 0;
angle_z = 0;
tcompx = 0;
tcompy = 0;
tcompz = 1;
constant_d = -64e-2;
Lx = 1;
Ly = 1;
Lz = 1;
%% Construct quadrics
for i=1:length(lambda_1)
    [x, y, z, q(i,:,:,:)] = Quadrics_maker(...
        lambda_1(i), lambda_2(i), lambda_3(i), ...
        angle_x(i), angle_y(i), angle_z(i), ...
        tcompx(i), tcompy(i), tcompz(i), ...
        constant_d(i), ...
        Lx, Ly, Lz); %#ok<AGROW>
S_cell(i) = {...
    sprintf(['(%.1f, %.1f, %.1f)\n' ...
    '(%.1f, %.1f, %.1f)\n' ... 
    '(%.1f, %.1f, %.1f)\n' ...
    '%.4f'], ...
    lambda_1(i), lambda_2(i), lambda_3(i), ...
    angle_x(i), angle_y(i), angle_z(i), ...
    tcompx(i), tcompy(i), tcompz(i), ...
    constant_d(i))}; %#ok<AGROW>
end
%% Graphics
figure;
hold on

for i=1:length(lambda_1)
    v = squeeze(q(i,:,:,:));
    hpatch = patch(isosurface(x, y, z, v, 0));
    isonormals(x,y,z,v,hpatch)
    hpatch.FaceColor = rand(1,3);
    hpatch.EdgeColor = 'none';
    camlight left;
    lighting gouraud
end

% nx = 0.016; ny = 4.88e-4; nz = -0.9999;
% Dx = 0.8; Dy = 0.025; Dz = 0.175;
% m = (0.8 - Dz) / nz;
% plot3([Dx Dx+m*nx], [Dy Dy+m*ny], [Dz Dz+m*nz], 'r');
% plot3([0 0.675], [0 0.175], [1 0.625], 'r');
% 
% nx = 0.1383; ny = 1.297e-2; nz = -0.9903;
% Dx = 0.8; Dy = 0.075; Dz = 0.175;
% m = (0.8 - Dz) / nz;
% plot3([Dx Dx+m*nx], [Dy Dy+m*ny], [Dz Dz+m*nz], 'r');

% n
nx = 0.17133034898181931;
ny = 0.044418979365658112;
nz = 0.9842117992535353;
% nq
nqx = 0.85253712023340866;
nqy = 0.22102814228273587;
nqz = -0.47363173346300325;

% x
Dx = 0.675;
Dy = 0.175; 
Dz = 0.625;
% xint
Dxi = 0.67064803152350516;
Dyi = 0.1738717118764645;
Dzi = 0.59999999999999998;

% q
q = @(x,y,z) x^2 + y^2 + (z-1)^2 - 64e-2;

fprintf('q at x = %e\n', q(Dx,Dy,Dz));
fprintf('q at xint = %e\n', q(Dxi,Dyi,Dzi));

% Step to one domain limit
m = abs((0 - Dz) / nz);
mq = abs((0 - Dz) / nqz);

% Line through both points x and xint
plot3([Dxi Dx], [Dyi Dy], [Dzi Dz], 'ok', 'MarkerfaceColor', 'w');
% Line traveling with normal q vector from x point
plot3([Dx Dx+mq*nqx], [Dy Dy+mq*nqy], [Dz Dz+mq*nqz], 'r');
plot3([Dx Dx-mq*nqx], [Dy Dy-mq*nqy], [Dz Dz-mq*nqz], '--r');
% Line traveling with normal vector from x point
plot3([Dx Dx+m*nx], [Dy Dy+m*ny], [Dz Dz+m*nz], 'b');
plot3([Dx Dx-m*nx], [Dy Dy-m*ny], [Dz Dz-m*nz], '--b');

hold off
box on
xlabel('$x$', 'Interpreter', 'Latex');
ylabel('$y$', 'Interpreter', 'Latex');
zlabel('$z$', 'Interpreter', 'Latex');

axis([Dxi-0.1*Dxi Dx+0.1*Dx Dyi-0.1*Dyi Dy+0.1*Dy Dzi-0.1*Dzi Dz+0.1*Dz]);
view(37.5, 30);

legend('$q$', '$x:x_i$', '$x:n_q +$', '$x:n_q -$', '$x:n +$', '$x:n -$');

% lgd = legend(S_cell, 'Location', 'westoutside', 'Orientation', 'vertical');
% title(lgd, sprintf(['$(\\lambda _1, \\lambda _2, \\lambda _3)$\n' ...
%     '$(\\alpha _1, \\alpha _2, \\alpha _3)$\n' ... 
%     '$(t_x, t_y, t_z)$\n' ...
%     '$d$']));
% legend('boxoff')
end

function [x, y, z, q] = Quadrics_maker(lambda_1, lambda_2, lambda_3, ...
    angle_x, angle_y, angle_z, ...
    tcompx, tcompy, tcompz, ...
    constant_d, Lx, Ly, Lz)
%% Data
ndata = 22;
X = linspace(0, Lx, ndata);
Y = linspace(0, Ly, ndata);
Z = linspace(0, 0.6, ndata);

[x, y, z] = meshgrid(X, Y, Z);

% Preallocation
R_x = zeros(3,3);
R_y = R_x;
R_z = R_x;
Trans_ = zeros(1,3);
matrix_c = zeros(3,3);
vector_a = zeros(1,3);
poly_coeffs = zeros(10,1);
%% C_CODE: MAKE_SURFACE
for j=1:3
    for i=1:3
        
        if(i == 1 && j == 1)
            matrix_c(i,j) = lambda_1;
            R_x(i,j) = 1;
            R_y(i,j) = cos(angle_y);
            R_z(i,j) = cos(angle_z);
            Trans_(i,j) = tcompx;
        end
        
        if(i == 1 && j == 2)
            R_x(i,j) = 0;
            R_y(i,j) = 0;
            R_z(i,j) = sin(angle_z);
        end
        
        if(i == 1 && j == 3)
            R_x(i,j) = 0;
            R_y(i,j) = -sin(angle_y);
            R_z(i,j) = 0;
        end
        
        if(i == 2 && j == 1)
            R_x(i,j) = 0;
            R_y(i,j) = 0;
            R_z(i,j) = -sin(angle_z);
            Trans_(i,j) = tcompy;
        end
        
        if(i == 2 && j == 2)
            matrix_c(i,j) = lambda_2;
            R_x(i,j) = cos(angle_x);
            R_y(i,j) = 1;
            R_z(i,j) = cos(angle_z);
        end
        
        if(i == 2 && j == 3)
            R_x(i,j) = sin(angle_x);
            R_y(i,j) = 0;
            R_z(i,j) = 0;
        end
        
        if(i == 3 && j == 1)
            R_x(i,j) = 0;
            R_y(i,j) = sin(angle_y);
            R_z(i,j) = 0;
            Trans_(i,j) = tcompz;
        end
        
        if(i == 3 && j == 2)
            R_x(i,j) = -sin(angle_x);
            R_y(i,j) = 0;
            R_z(i,j) = 0;
        end
        
        if(i == 3 && j == 3)
            matrix_c(i,j) = lambda_3;
            R_x(i,j) = cos(angle_x);
            R_y(i,j) = cos(angle_y);
            R_z(i,j) = 1;
        end
        
    end
end
%% C_CODE: Rotation Matrix
intermediate = R_y*R_x;
Rot_ = R_z*intermediate;
Rot_T = Rot_';
intermediate = Rot_T*matrix_c;
matrix_A = intermediate*Rot_;

vector_a(1) = -(2*matrix_A(1,1)*Trans_(1) + matrix_A(2,1)*Trans_(2) + ...
    matrix_A(3,1)*Trans_(3) + matrix_A(1,2)*Trans_(2) + ...
    matrix_A(1,3)*Trans_(3));

vector_a(2) = -(matrix_A(2,1)*Trans_(2) + 2*matrix_A(2,2)*Trans_(2) + ...
    matrix_A(3,2)*Trans_(3) + matrix_A(1,2)*Trans_(1) + ...
    matrix_A(2,3)*Trans_(3));

vector_a(3) = -(matrix_A(1,3)*Trans_(1) + matrix_A(2,3)*Trans_(2) + ...
    2*matrix_A(3,3)*Trans_(3) + matrix_A(3,1)*Trans_(2) + ...
    matrix_A(3,2)*Trans_(2));

constant_a = ...
    matrix_A(1,1)*Trans_(1)*Trans_(1) + ...
    matrix_A(2,1)*Trans_(2)*Trans_(1) + ...
    matrix_A(3,1)*Trans_(3)*Trans_(1) + ...
    matrix_A(1,2)*Trans_(1)*Trans_(2) + ...
    matrix_A(2,2)*Trans_(2)*Trans_(2) + ...
    matrix_A(3,2)*Trans_(3)*Trans_(2) + ...
    matrix_A(1,3)*Trans_(1)*Trans_(3) + ...
    matrix_A(2,3)*Trans_(2)*Trans_(3) + ...
    matrix_A(3,3)*Trans_(3)*Trans_(3) + ...
    constant_d;

%% SAVING POLYNOMIAL COEFFICIENTES
poly_coeffs(1) = matrix_A(1,1);
poly_coeffs(2) = matrix_A(2,2);
poly_coeffs(3) = matrix_A(3,3);

% The factor of 2 is included within the coefficients
poly_coeffs(4) = 2*matrix_A(1,2);
poly_coeffs(5) = 2*matrix_A(1,3);
poly_coeffs(6) = 2*matrix_A(2,3);

poly_coeffs(7) = vector_a(1);
poly_coeffs(8) = vector_a(2);
poly_coeffs(9) = vector_a(3);
poly_coeffs(10) = constant_a;

q = poly_coeffs(1)*x.^2 + poly_coeffs(2)*y.^2 + poly_coeffs(3)*z.^2 + ...
            poly_coeffs(4)*x.*y + poly_coeffs(5)*x.*z + ...
            poly_coeffs(6)*y.*z + poly_coeffs(7)*x + poly_coeffs(8)*y + ...
            poly_coeffs(9)*z + poly_coeffs(10);
end