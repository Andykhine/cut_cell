function process_cyl
%% Cleaning
clc
close all
%% See if we are in the correct folder
continue_OK = exist('g8', 'dir') && exist('g12', 'dir') && ...
    exist('g15', 'dir') && exist('g20', 'dir') && ...
    exist('g25', 'dir') && exist('g30', 'dir');

if ~continue_OK
    error('Looks like we are not in the apropriate folder');
end
%% Settings
% Puts graphs in logarithmic values
log_graphs = false;

set(groot,'DefaultAxesTickLabelInterpreter','Latex');
set(groot,'DefaultLegendInterpreter','Latex');

base_dir = pwd;

% Rename .dat to .m
unix("find . -type f -name 'post_process.dat' -print0 | xargs --null rename .dat .m");

mstyle='xos';
istyle={'--', '-'};
kstyle='kbr';
%% Data
% S_grids = {'g8', 'g12', 'g15', 'g20', 'g25', 'g30'};
S_grids = {'g8', 'g12', 'g15', 'g20', 'g25'};
S_methods = {'upwind', 'QUICK', 'wahyd'};
S_interps = {'N', 'P'};
sinterps= 'AB';
S_Res = {'7', '10', '20'};
% S_kappas = {'05', '20', '40'};
S_kappas = {'05', '40'};

% Store reference values for Cf, Cp, p0, ppi as function of Re
Cfr = [1.553 1.246 0.812];
Cpr = [1.868 1.600 1.223];
p0r = [-0.87 -0.742 -0.589];
ppir = [1.66 1.489 1.269];

% Weighted cell sizes for each grid
%dx = [1.08e-2 7.25e-3 5.83e-3 4.39e-3 3.82e-3 3.51e-3];
%dx = [1.08e-2 7.25e-3 5.83e-3 4.39e-3 3.82e-3];
% spoofed
dx = [1.08e-2 7.25e-3 5.83e-3 4.39e-3 3.51e-3];

ldx = log(dx);
%% Preallocation
Ng = length(S_grids);
Nh = length(S_methods);
Ni = length(S_interps);
Nj = length(S_Res);
Nk = length(S_kappas);

kappas = [0.05 0.20 0.40];

% Wake length/Diameter
wake_D = NaN*ones(Ng,Nh,Ni,Nj,Nk);
% Related to viscous force over kinetic energy
Cf = NaN*ones(Ng,Nh,Ni,Nj,Nk);
% Related to pressure force over kinetic energy
Cp = NaN*ones(Ng,Nh,Ni,Nj,Nk);
% Pressure at theta = 0
p0 = NaN*ones(Ng,Nh,Ni,Nj,Nk);
% Pressure at theta = pi
ppi = NaN*ones(Ng,Nh,Ni,Nj,Nk);

% Error arrays
eCf = Cf;
eCp = Cp;
ep0 = p0;
eppi = ppi;
%% Check convergence
info_cases(base_dir, S_grids, S_methods, S_interps, S_Res, S_kappas);
%% Get data
for g=1:Ng
    S_grid = cell2mat(S_grids(g));
    for h=1:Nh
        S_method = cell2mat(S_methods(h));
        for i=1:Ni
            S_interp = cell2mat(S_interps(i));
            for j=1:Nj
                S_Re = cell2mat(S_Res(j));
                for k=1:Nk
                    S_kappa = cell2mat(S_kappas(k));

                    kappas(k) = str2double(['0.' S_kappa]);

                    pathtofile = [base_dir '/' S_grid '/' ...
                        S_method '/' S_interp ...
                        '/Re' S_Re '/k_' S_kappa];

                    try
                        [wake_D(g,h,i,j,k), Cf(g,h,i,j,k), Cp(g,h,i,j,k), p0(g,h,i,j,k), ppi(g,h,i,j,k)] = intFrictionCoeffs(pathtofile, 0, 0);
                    catch
                        fprintf('Setting NaN due to error in %s:%s:%s:%s:%s\n', S_grid, S_method, S_interp, S_Re, S_kappa);

                        wake_D(g,h,i,j,k) = NaN;
                        Cf(g,h,i,j,k) = NaN;
                        Cp(g,h,i,j,k) = NaN;
                        p0(g,h,i,j,k) = NaN;
                        ppi(g,h,i,j,k) = NaN;
                    end

                    % Compute error for series
                    eCf(g,h,i,j,k) = abs((Cfr(j) - Cf(g,h,i,j,k))/Cfr(j));
                    eCp(g,h,i,j,k) = abs((Cpr(j) - Cp(g,h,i,j,k))/Cpr(j));
                    ep0(g,h,i,j,k) = abs((p0r(j) - p0(g,h,i,j,k))/p0r(j));
                    eppi(g,h,i,j,k) = abs((ppir(j) - ppi(g,h,i,j,k))/ppir(j));
                end
            end
        end
    end
end
%% Graphs
% ldx vs lephi, 1 fig per Re, method and interp series

choice = 4;

if choice == 1
    psi = eCf;
    spsi = '$eC_f$';
elseif choice == 2
    psi = eCp;
    spsi = '$eC_p$';
elseif choice == 3
    psi = ep0;
    spsi = '$e\hat{p}(0)$';
elseif choice == 4
    psi = eppi;
    spsi = '$e\hat{p}(\pi)$';
end

if choice <= 4

    for j=1:Nj
        S_Re = cell2mat(S_Res(j));

        count = 0;

        figure
        hold on
        for k=1:Nk % kappa
            S_kappa = cell2mat(S_kappas(k));
            for h=1:Nh % method
                S_method = cell2mat(S_methods(h));

                if(h == 3)
                    S_method = upper(S_method);
                end

                for i=1:Ni % interp
                    S_interp = sinterps(i);

                    style = [cell2mat(istyle(i)) mstyle(h) kstyle(k)];
                    if log_graphs
                        plot(ldx, log(psi(:,h,i,j,k)), style);
                    else
                        plot(dx, psi(:,h,i,j,k), style);
                    end

                    count = count + 1;
                    slegend(count) = {[S_interp ':' S_method ':' ...
                        '$\kappa$:0.' S_kappa]}; %#ok<AGROW>
                end
            end
        end
        hold off
        box on
        if log_graphs
            xlabel('log $dx$', 'Interpreter', 'Latex');
            ylabel(['log ' spsi], 'Interpreter', 'Latex');
        else
            xlabel('$dx$, m', 'Interpreter', 'Latex');
            ylabel(spsi, 'Interpreter', 'Latex');
        end

        hl = legend(slegend, 'Location', 'Best', 'NumColumns', 2);
        title(hl, ['Re = ' S_Re], 'Interpreter', 'Latex');

        % eCf
%         set(gca, 'YLim', [0.017 0.23]);
    end
end
%% Return to base_dir
cd(base_dir);
end

function info_cases(base_dir, S_grids, S_methods, S_interps, S_Res, S_kappas)
% check the convergence info for every case and prints table

fprintf('%30s %5s %8s %8s %8s %8s %8s\n', ...
    'case', 'it', 'su', 'sv', 'sc', 'gu', 'gv');

Ng = length(S_grids);
Nh = length(S_methods);
Ni = length(S_interps);
Nj = length(S_Res);
Nk = length(S_kappas);

for g=1:Ng
    S_grid = cell2mat(S_grids(g));
    for h=1:Nh
        S_method = cell2mat(S_methods(h));
        for i=1:Ni
            S_interp = cell2mat(S_interps(i));
            for j=1:Nj
                S_Re = cell2mat(S_Res(j));
                for k=1:Nk
                    S_kappa = cell2mat(S_kappas(k));

                    pathtofile = [base_dir '/' S_grid '/' ...
                        S_method '/' S_interp ...
                        '/Re' S_Re '/k_' S_kappa '/results.mat'];

                    info_cases_(pathtofile);

                end
            end
        end
    end
end
end

function info_cases_(sfile)
%% See if file exists
try
    load(sfile); %#ok<*LOAD>
catch
    fprintf('%30s %5s %8s %8s %8s %8s %8s\n', ...
        sfile, 'file', ' not', ' found', '!!', ' ', ' ');
    return;
end
%% See if convergence was attained
if (( (glob_norm_flow(1) >= tol_flow) || ...
        (glob_norm_flow(2) >= tol_flow) ))

    fprintf('%30s %5.2f %8.2e %8.2e %8.2e %8.2e %8.2e\n', ...
        sfile, double(it_flow)/double(max_it_flow), ...
        sigmas_flow(1), sigmas_flow(2), sigmas_flow(4), ...
        glob_norm_flow(1), glob_norm_flow(2));

end
end