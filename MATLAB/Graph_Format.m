function Graph_Format(S, latex_OK)
%GRAPH_FORMAT Formatea la figura y ejes activos
%% Standar values
if nargin < 2
    latex_OK = true;
end

if nargin < 1
    S = 'palatino';
end

S_weight = 'bold';
size_axes = 13;
size_texts = 13;
size_labels = 14;
line_width = 1.0;

%% Puts LaTeX capabilities in axis and ticks this might override some fonts
if latex_OK
    set(groot,'DefaultAxesTickLabelInterpreter','Latex');
    set(groot,'DefaultLegendInterpreter','Latex');
end

%% Formateando fondo figura
set(gcf, 'Color', [1 1 1]);
%% Getting text handles (this excludes axes title and includes legend :C, thus calling it first)
handles_text = findobj(gcf, 'type', 'text');
if ~isempty(handles_text)
    for i = 1:length(handles_text)
        %% Formateando etiquetas ejes
        set(handles_text(i), 'FontName', S, 'FontSize', size_texts);
    end
end
%% Getting axes handles
handles_axes = findobj(gcf, 'type', 'axes');
if ~isempty(handles_axes)
    for i = 1:length(handles_axes)
        %% Formateando etiquetas ejes
        set(get(handles_axes(i), 'XLabel'), 'FontName', S, 'FontSize', size_labels, 'FontWeight', S_weight);
        set(get(handles_axes(i), 'YLabel'), 'FontName', S, 'FontSize', size_labels, 'FontWeight', S_weight);
        set(get(handles_axes(i), 'ZLabel'), 'FontName', S, 'FontSize', size_labels, 'FontWeight', S_weight);
        %% Formateando ejes, título y leyenda
        set(handles_axes(i), 'FontName', S, 'FontSize', size_axes);
        handle_title = get(handles_axes(i), 'Title');
        if ~isempty(handle_title)
            set(handle_title, 'FontName', S, 'FontSize', size_labels);
        end
    end
end
%% Setting lines width
for i=1:length(handles_axes)
    handles_lines = findobj(handles_axes(i), 'Type', 'Line');
    for j=1:length(handles_lines)
        set(handles_lines(j), 'LineWidth', line_width);
    end
end
