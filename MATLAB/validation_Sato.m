function validation_Sato
% Validates flow and temperature fields by comparing with analytical
% solutions

% Requires: nodes_x_cpu_var, UCenter_cpu_var, VCenter_cpu_var
% All numerical solution taken below 1e-8

clear
clc
close all
%% Data
S = {'Sato_042', 'Sato_062', 'Sato_122', 'Sato_242', 'Sato_482', ...
    'Sato_962'};
S_oldint = {'Sato_oldint_042', 'Sato_oldint_062', 'Sato_oldint_122', ...
    'Sato_oldint_242', 'Sato_oldint_482', 'Sato_oldint_962'};
rhof = 1.0;
mu = 0.2;
kf = 10;
ks = 100;
Ftheta = 122000;
rsi = 4e-3;
ri = 12e-3;
ro = 20e-3;
rc = (ri+ro)/2;
Tsi = 1;
To = 0;
Lx = 0.096;
Ly = 0.096;
d = 4e-3;
%% Making
xabs = linspace(0, Lx, 50);
yabs = linspace(0, Ly, 50);

x = xabs - Lx/2;
y = yabs - Ly/2;

N = length(x);

% xGrid = (x') * ones(1, N);
% yGrid = ones(N, 1) * y;

% Preallocate
vtheta = zeros(N,N);
vx = zeros(N,N);
vy = zeros(N,N);
vmod = zeros(N,N);

for i=1:N
    for j=1:N
        % Get r, theta from x, y
        r = sqrt(x(i)^2 + y(j)^2);
        theta = atan2(y(j), x(i));
        
        % Velocity
        if ( (r >= ri) && (r <= ro) )
            vtheta(i,j) = (Ftheta*r) / (2*mu*(ro^2 - ri^2)) * ...
                (ro^2*log(r/ro) + ri^2*log(ri/r) - (ri*ro./r)^2*log(ri/ro));
        else
            vtheta(i,j) = NaN;
        end
        
        % Get vx, vy from vtheta
        vx(i,j) = -vtheta(i,j)*sin(theta);
        vy(i,j) = vtheta(i,j)*cos(theta);
        
        vmod(i,j) = sqrt(vx(i,j)^2 + vy(i,j)^2);
    end
end

% rVector
% rVector = linspace(ri, ro, 50);
% vtheta_r
vtheta_r = @(r) ((Ftheta * r) / (2 * mu * (ro^2 - ri^2)) .* ...
    (ro^2 * log(r / ro) + ri^2 * log(ri ./ r) - ...
    (ri * ro./r).^2 * log(ri/ro)));
% tau_{r,theta}
tau_r_theta = @(r) (Ftheta / (2 * (ro^2 - ri^2))) * ....
    (ro^2 - ri^2 + 2 * ro^2 * ri^2 * log(ri / ro) ./ r.^2);

% p
alpha = Ftheta/(2*mu*(ro^2 - ri^2));
beta = (ro^2 - ri^2);
gamma = ri^2 * log(ri) - ro^2 * log(ro);
delta = (ri*ro)^2*log(ro/ri);

C = @(r) -rhof*alpha^2* ( ...
    beta^2* (r.^2 .* log(r) .* log(r)/2 - r.^2 .* log(r)/2 + r.^2 /4) + ...
    beta*gamma*(r.^2 .* log(r) - r.^2/2) + ...
    beta*delta* log(r) .* log(r) + ...
    2*gamma*delta* log(r) + ...
    gamma*gamma* r.^2/2 - delta*delta./(2*r.^2));

p = @(r) rhof*alpha^2* ( ...
    beta^2* (r.^2 .* log(r) .* log(r)/2 - r.^2 .* log(r)/2 + r.^2 /4) + ...
    beta*gamma*(r.^2 .* log(r) - r.^2/2) + ...
    beta*delta* log(r) .* log(r) + ...
    2*gamma*delta* log(r) + ...
    gamma*gamma* r.^2/2 - delta*delta./(2*r.^2)) + C(rc);

% r
rsii = linspace(rsi, ri, 50);
rio = linspace(ri, ro, 50);
% T
Tsii = @(r) Tsi + (To-Tsi)*log(r/rsi) / (log(ri/rsi) + (ks/kf)*log(ro/ri));
Tio = @(r) To - (To-Tsi)*log(ro./r) / ((kf/ks)*log(ri/rsi) + log(ro/ri));

% Reynolds and Prandlt number
u_i = sqrt(tau_r_theta(ri) / rhof);
u_o = sqrt(tau_r_theta(ro) / rhof);
u_t = sqrt( ((ri*u_i)^2 + (ro*u_o)^2) / (2*rc^2) );
tau_t = rhof * u_t ^2;
% Re = rhof*u_t*d / mu;
% Pr = cf*mu/kf;

%% Graphs
% Velocity
figure
hold on
% Analytical solution
huv1 = plot(rio/d, vtheta_r(rio) / u_t, 'k');
set(gca, 'ColorOrder', [0 0 0]);
set(gca, 'LineStyleOrder', {'*', '^', 's', 'o','x', '+'});
% Numerical solution
[huvs, Slegend] = plot_flow_num_data_oldint(d, u_t, gca, S_oldint, vtheta_r, true);
hold off
xlabel('r^*');
ylabel('v_{\theta} ^*');
box on
title('oldint');
legend([huv1, huvs], Slegend, 'Location', 'North');
set(gca, 'XLim', [3 5]);

figure
hold on
% Analytical solution
huv1 = plot(rio/d, vtheta_r(rio) / u_t, 'k');
set(gca, 'ColorOrder', [0 0 0]);
set(gca, 'LineStyleOrder', {'*', '^', 's', 'o','x', '+'});
% Numerical solution
[huvs, Slegend] = plot_flow_num_data(d, u_t, gca, S, vtheta_r, true);
hold off
xlabel('r^*');
ylabel('v_{\theta} ^*');
box on
legend([huv1, huvs], Slegend, 'Location', 'North');
set(gca, 'XLim', [3 5]);

% Pressure
figure
hold on
% Analytical solution
hp1 = plot(rio/d, p(rio) / tau_t, 'k');
set(gca, 'ColorOrder', [0 0 0]);
set(gca, 'LineStyleOrder', {'*', '^', 's', 'o','x', '+'});
% Numerical solution
[hps, Slegend] = plot_p_num_data_oldint(d, tau_t, gca, S_oldint, p, true);
hold off
xlabel('r^*');
ylabel('\Delta p ^*');
box on
title('oldint');
legend([hp1, hps], Slegend, 'Location', 'NorthWest');
set(gca, 'XLim', [3 5]);

figure
hold on
% Analytical solution
hp1 = plot(rio/d, p(rio) / tau_t, 'k');
set(gca, 'ColorOrder', [0 0 0]);
set(gca, 'LineStyleOrder', {'*', '^', 's', 'o','x', '+'});
% Numerical solution
[hps, Slegend] = plot_p_num_data(d, tau_t, gca, S, p, true);
hold off
xlabel('r^*');
ylabel('\Delta p ^*');
box on
legend([hp1, hps], Slegend, 'Location', 'NorthWest');
set(gca, 'XLim', [3 5]);

% % Temperature
% figure
% hold on
% % Analytical solution
% hT1 = plot(rsii/d, Tsii(rsii), 'k');
% plot(rio/d, Tio(rio), 'k');
% set(gca, 'ColorOrder', [0 0 0]);
% set(gca, 'LineStyleOrder', {'*', '^', 's', 'o', 'x', '+'});
% % Numerical solution
% [hTs, Sl] = plot_phi_num_data(d, gca, S);
% hold off
% xlabel('r^*');
% ylabel('T^*');
% box on
% legend([hT1, hTs], Sl, 'Location', 'South');
% set(gca, 'XLim', [1 5]);
end
%% Other functions
function [hp, Slegend] = plot_flow_num_data_oldint(d, u_t, hax, S, vref, e_OK)
n = length(S);
hp = zeros(1,n);

Slegend(1) = {'Analytical'};

for l=1:n
    
    load(cell2mat(S(l)));
    
    [Nx, Ny] = size(x);
    
    dx = x(3,1) - x(2,1);
    dy = dx;
    
    j = (Ny/2)+1;
    k = 1;
    for i=(Nx/2):-1:2
        Us(k) = U(i,j);
        Vs(k) = V(i,j);
        j = j+1;
        k = k+1;
    end
    
    rnum = NaN*ones(1,(Nx/2)-1);
    
    rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);
    
    for i=1:(Nx/2-2)
        rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
    end
    
    vnum = (-Us*sind(135) + Vs*cosd(135));
    
    if(e_OK)
        et = abs((vnum - vref(rnum)) ./ vref(rnum)) * 100;
        e = et(rnum/d >= 3.0 & rnum/d <= 5.0);
        
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx) ': max_e '...
            num2str(max(e),'%.2f') ', mean_e ' num2str(mean(e),'%.2f')]};
        
    else
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx)]};
    end
    hp(l) = plot(hax, rnum/d, vnum / u_t);
    
end

end

function [hp, Slegend] = plot_p_num_data_oldint(d, tau_t, hax, S, pref, e_OK)

n = length(S);
hp = zeros(1,n);

Slegend(1) = {'Analytical'};

for l=1:n
    
    load(cell2mat(S(l)));
    
    [Nx, Ny] = size(x);
    
    dx = x(3,1) - x(2,1);
    dy = dx;
    
    j = (Ny/2)+1;
    k = 1;
    for i=(Nx/2):-1:2
        ps(k) = p(i,j);
        j = j+1;
        k = k+1;
    end
    
    rnum = NaN*ones(1,(Nx/2)-1);
    
    rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);
    
    for i=1:(Nx/2-2)
        rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
    end
    
    if(e_OK)
        et = abs((ps - pref(rnum)) ./ pref(rnum)) * 100;
        e = et(rnum/d >= 3.0 & rnum/d <= 5.0);
        
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx) ': max_e '...
            num2str(max(e),'%.2f') ', mean_e ' num2str(mean(e),'%.2f')]};
        
    else
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx)]};
    end
    
    hp(l) = plot(hax, rnum/d, ps/tau_t);
    
end

end

function [hp, Slegend] = plot_flow_num_data(d, u_t, hax, S, vref, e_OK)
n = length(S);
hp = zeros(1,n);

Slegend(1) = {'Analytical'};

for l=1:n
    
    load(cell2mat(S(l)));
    
    [Nx, Ny] = size(nodes_x_cpu_var);
    
    dx = nodes_x_cpu_var(3,1) - nodes_x_cpu_var(2,1);
    dy = dx;
    
    j = (Ny/2)+1;
    k = 1;
    for i=(Nx/2):-1:2
        U(k) = UCenter_cpu_var(i,j);
        V(k) = VCenter_cpu_var(i,j);
        j = j+1;
        k = k+1;
    end
    
    rnum = NaN*ones(1,(Nx/2)-1);
    
    rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);
    
    for i=1:(Nx/2-2)
        rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
    end
    
    vnum = (-U*sind(135) + V*cosd(135));
    
    if(e_OK)
        et = abs((vnum - vref(rnum)) ./ vref(rnum)) * 100;
        e = et(rnum/d >= 3.0 & rnum/d <= 5.0);
        
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx) ': max_e '...
            num2str(max(e),'%.2f') ', mean_e ' num2str(mean(e),'%.2f')]};
        
    else
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx)]};
    end
    hp(l) = plot(hax, rnum/d, vnum / u_t);
    
end

end

function [hp, Slegend] = plot_p_num_data(d, tau_t, hax, S, pref, e_OK)

n = length(S);
hp = zeros(1,n);

Slegend(1) = {'Analytical'};

for l=1:n
    
    load(cell2mat(S(l)));
    
    [Nx, Ny] = size(nodes_x_cpu_var);
    
    dx = nodes_x_cpu_var(3,1) - nodes_x_cpu_var(2,1);
    dy = dx;
    
    j = (Ny/2)+1;
    k = 1;
    for i=(Nx/2):-1:2
        p(k) = p_cpu_var(i,j);
        j = j+1;
        k = k+1;
    end
    
    rnum = NaN*ones(1,(Nx/2)-1);
    
    rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);
    
    for i=1:(Nx/2-2)
        rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
    end
    
    if(e_OK)
        et = abs((p - pref(rnum)) ./ pref(rnum)) * 100;
        e = et(rnum/d >= 3.0 & rnum/d <= 5.0);
        
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx) ': max_e '...
            num2str(max(e),'%.2f') ', mean_e ' num2str(mean(e),'%.2f')]};
        
    else
        Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx)]};
    end
    
    hp(l) = plot(hax, rnum/d, p/tau_t);
    
end

end

function [hp, Slegend] = plot_phi_num_data(d, hax, S)

n = length(S);
hp = zeros(1,n);

Slegend(1) = {'Analytical'};

for l=1:n
    
    load(cell2mat(S(l)));
    
    [Nx, Ny] = size(nodes_x_cpu_var);
    
    dx = nodes_x_cpu_var(3,1) - nodes_x_cpu_var(2,1);
    dy = dx;
    
    j = (Ny/2)+1;
    k = 1;
    for i=(Nx/2):-1:2
        phi(k) = phi_cpu_var(i,j);
        j = j+1;
        k = k+1;
    end
    
    rnum = NaN*ones(1,(Nx/2)-1);
    
    rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);
    
    for i=1:(Nx/2-2)
        rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
    end
    
    Slegend(l+1) = {[num2str(Nx) '\times' num2str(Nx)]};
    hp(l) = plot(hax, rnum/d, phi);
    
end

end
