function Sato
% Validates flow and temperature fields by comparing with analytical
% solutions

% All numerical solution taken below 1e-10

clc
close all
%% Settings
set(groot,'DefaultAxesTickLabelInterpreter','Latex');
set(groot,'DefaultLegendInterpreter','Latex');
%% Graphs
vel_p_OK = 1; % paper

A_vel_OK = 0;
B_vel_OK = 0;

A_p_OK = 0;
B_p_OK = 0;

k_order_OK = 0;
ldx_ldy_OK = 0; % paper
alpha_kappa_OK = 0;

mcolor = 'krbm';
msymbol = 'xosv';
gcolor = 'kbrg';
ksymbol = 'o+svd';
%% Data
pnorm = 2;
S_methods = {'upwind', 'QUICK', 'WAHYD'};
% S_kappas = {'/k_02/', '/k_10/', '/k_20/', '/k_30/', '/k_40/'};
S_kappas = {'/k_02/', '/k_20/', '/k_40/'};
S_grids = {'022', '042', '062', '082', '102', '122'};
% S_grids = {'022', '042', '062', '082'};

% S_methods = {'upwind', 'vanLeer'};
% S_kappas = {'/k_02/'};
% S_grids = {'082'};

rhof = 1.0;
mu = 0.2;
% kf = 10;
% ks = 100;
Ftheta = 122000;
% rsi = 4e-3;
ri = 12e-3;
ro = 20e-3;
rc = (ri+ro)/2;
% Tsi = 1;
% To = 0;
Lx = 0.096;
% Ly = 0.096;
d = 4e-3;
%% Making analytical solution
% rVector
% rVector = linspace(ri, ro, 50);
% vtheta_r
vtheta_r = @(r) ((Ftheta * r) / (2 * mu * (ro^2 - ri^2)) .* ...
    (ro^2 * log(r / ro) + ri^2 * log(ri ./ r) - ...
    (ri * ro./r).^2 * log(ri/ro)));
% tau_{r,theta}
tau_r_theta = @(r) (Ftheta / (2 * (ro^2 - ri^2))) * ....
    (ro^2 - ri^2 + 2 * ro^2 * ri^2 * log(ri / ro) ./ r.^2);

% p
alpha = Ftheta/(2*mu*(ro^2 - ri^2));
beta = (ro^2 - ri^2);
gamma = ri^2 * log(ri) - ro^2 * log(ro);
delta = (ri*ro)^2*log(ro/ri);

C = @(r) -rhof*alpha^2* ( ...
    beta^2* (r.^2 .* log(r) .* log(r)/2 - r.^2 .* log(r)/2 + r.^2 /4) + ...
    beta*gamma*(r.^2 .* log(r) - r.^2/2) + ...
    beta*delta* log(r) .* log(r) + ...
    2*gamma*delta* log(r) + ...
    gamma*gamma* r.^2/2 - delta*delta./(2*r.^2));

p = @(r) rhof*alpha^2* ( ...
    beta^2* (r.^2 .* log(r) .* log(r)/2 - r.^2 .* log(r)/2 + r.^2 /4) + ...
    beta*gamma*(r.^2 .* log(r) - r.^2/2) + ...
    beta*delta* log(r) .* log(r) + ...
    2*gamma*delta* log(r) + ...
    gamma*gamma* r.^2/2 - delta*delta./(2*r.^2)) + C(rc);

% r
% rsii = linspace(rsi, ri, 50);
rio = linspace(ri, ro, 50);
% T
% Tsii = @(r) Tsi + (To-Tsi)*log(r/rsi) / (log(ri/rsi) + (ks/kf)*log(ro/ri));
% Tio = @(r) To - (To-Tsi)*log(ro./r) / ((kf/ks)*log(ri/rsi) + log(ro/ri));

% Reynolds and Prandlt number
u_i = sqrt(tau_r_theta(ri) / rhof);
u_o = sqrt(tau_r_theta(ro) / rhof);
u_t = sqrt( ((ri*u_i)^2 + (ro*u_o)^2) / (2*rc^2) );
tau_t = rhof * u_t ^2;
Re = rhof*u_t*d / mu;
% Pr = cf*mu/kf;
fprintf('Re = %.2f\n', Re);
%% Check cases
% info_cases(pwd, S_methods, S_kappas, S_grids);
%% Errors
for i=1:length(S_methods)
    S_method = cell2mat(S_methods(i));
    
    for j=1:length(S_kappas)
        S_kappa = cell2mat(S_kappas(j));
        
        S_k = ['0.' S_kappa(4:5)];
        
        kappa(j) = str2double(S_k);
        
        for k=1:length(S_grids)
            S_grid = cell2mat(S_grids(k));
            
            S_oldint = [ ...
                S_method ...
                S_kappa 'Sato_oldint_'...
                S_grid];
            
            e_oldint(i,j,k) = ...
                compute_error_UV(Ftheta, mu, ri, ro, u_t, S_oldint, pnorm);
            
            S = [ ...
                S_method ...
                S_kappa 'Sato_'...
                S_grid];
            
            e(i,j,k) = ...
                compute_error_UV(Ftheta, mu, ri, ro, u_t, S, pnorm);
            
        end
    end
end

for k = 1:length(S_grids)
    grid = str2double(cell2mat(S_grids(k)));
    ldx(k) = log(Lx/(grid - 2)); %#ok<*NASGU>
end

lkappa = log(kappa);

for i=1:length(S_methods)
    S_method = cell2mat(S_methods(i));
    
    for j=1:length(S_kappas)
        S_kappa = cell2mat(S_kappas(j));
        
        % Extract grid data for each angle and kappa
        ldy = log(squeeze(e_oldint(i,j,:)));
        % Get the slope of log log representation... this is the method
        % order
        [alpha_oldint(i,j), ~] = compute_lr(ldx,ldy);
        
        % Extract grid data for each angle and kappa
        ldy = log(squeeze(e(i,j,:)));
        % Get the slope of log log representation... this is the method
        % order
        [alpha(i,j), ~] = compute_lr(ldx,ldy);
        
    end
end
%% Graphs
if(vel_p_OK)
    % Velocity
    figure
    hold on
    % Analytical solution
    plot(rio/d, vtheta_r(rio) / u_t, '-k');

    S_legend(1) = {'Analytical'};
    count = 1;
    % upwind
    S_method = 'upwind';
    S_kappa = '/k_02/';
    S_grid = '122';

    S = [ ...
        S_method ...
        S_kappa 'Sato_oldint_'...
        S_grid];

    h = plot_flow_num_data(d, u_t, gca, S, 'rs');
    
    count = count + 1;
    S_legend(count) = {[S_method ': A: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    S = [ ...
        S_method ...
        S_kappa 'Sato_'...
        S_grid];

    h = plot_flow_num_data(d, u_t, gca, S, 'ks');

    count = count + 1;
    S_legend(count) = {[S_method ': B: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
    % QUICK
    S_method = 'QUICK';
    S_kappa = '/k_20/';
    S_grid = '082';

    S = [ ...
        S_method ...
        S_kappa 'Sato_oldint_'...
        S_grid];

    h = plot_flow_num_data(d, u_t, gca, S, 'ro');
    
    count = count + 1;
    S_legend(count) = {[S_method ': A: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    S = [ ...
        S_method ...
        S_kappa 'Sato_'...
        S_grid];

    h = plot_flow_num_data(d, u_t, gca, S, 'ko');

    count = count + 1;
    S_legend(count) = {[S_method ': B: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
    % WAHYD
    S_method = 'WAHYD';
    S_kappa = '/k_02/';
    S_grid = '122';

    S = [ ...
        S_method ...
        S_kappa 'Sato_oldint_'...
        S_grid];

    h = plot_flow_num_data(d, u_t, gca, S, 'r+');
    
    count = count + 1;
    S_legend(count) = {[S_method ': A: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    S = [ ...
        S_method ...
        S_kappa 'Sato_'...
        S_grid];

    h = plot_flow_num_data(d, u_t, gca, S, 'k+');

    count = count + 1;
    S_legend(count) = {[S_method ': B: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    hold off
    xlabel('$r^*$', 'Interpreter', 'Latex');
    ylabel('$v_{\theta} ^*$', 'Interpreter', 'Latex');
    box on
    legend(S_legend, 'Location', 'North', 'Interpreter', 'Latex');
    set(gca, 'XLim', [3 5]);

    % Pressure
    figure
    hold on
    % Analytical solution
    plot(rio/d, p(rio) / tau_t, '-k');

    S_legend(1) = {'Analytical'};
    count = 1;
    % upwind
    S_method = 'upwind';
    S_kappa = '/k_02/';
    S_grid = '122';

    S = [ ...
        S_method ...
        S_kappa 'Sato_oldint_'...
        S_grid];

    h = plot_p_num_data(d, tau_t, gca, S, 'rs');
    
    count = count + 1;
    S_legend(count) = {[S_method ': A: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    S = [ ...
        S_method ...
        S_kappa 'Sato_'...
        S_grid];

    h = plot_p_num_data(d, tau_t, gca, S, 'ks');

    count = count + 1;
    S_legend(count) = {[S_method ': B: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
    % QUICK
    S_method = 'QUICK';
    S_kappa = '/k_20/';
    S_grid = '082';

    S = [ ...
        S_method ...
        S_kappa 'Sato_oldint_'...
        S_grid];

    h = plot_p_num_data(d, tau_t, gca, S, 'ro');
    
    count = count + 1;
    S_legend(count) = {[S_method ': A: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    S = [ ...
        S_method ...
        S_kappa 'Sato_'...
        S_grid];

    h = plot_p_num_data(d, tau_t, gca, S, 'ko');

    count = count + 1;
    S_legend(count) = {[S_method ': B: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
    % WAHYD
    S_method = 'WAHYD';
    S_kappa = '/k_02/';
    S_grid = '122';

    S = [ ...
        S_method ...
        S_kappa 'Sato_oldint_'...
        S_grid];

    h = plot_p_num_data(d, tau_t, gca, S, 'r+');
    
    count = count + 1;
    S_legend(count) = {[S_method ': A: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    S = [ ...
        S_method ...
        S_kappa 'Sato_'...
        S_grid];

    h = plot_p_num_data(d, tau_t, gca, S, 'k+');

    count = count + 1;
    S_legend(count) = {[S_method ': B: $\kappa = 0.' S_kappa(4:5) ...
        ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>

    hold off
    xlabel('$r^*$', 'Interpreter', 'Latex');
    ylabel('$\Delta p ^*$', 'Interpreter', 'Latex');
    box on
    legend(S_legend, 'Location', 'North', 'Interpreter', 'Latex');
    set(gca, 'XLim', [3 5]);
end

% Velocity
if(A_vel_OK)
    figure %#ok<*UNRCH>
    hold on
    % Analytical solution
    plot(rio/d, vtheta_r(rio) / u_t, '-k');
    set(gca, 'ColorOrder', [0 0 0]);
    set(gca, 'LineStyleOrder', {'^', 'o','x', '+'});
    S_legend(1) = {'Analytical'};
    
    % Numerical solution
    count = 2;
    for i=1:length(S_methods)
        S_method = cell2mat(S_methods(i));
        
        for j=1:length(S_kappas)
            S_kappa = cell2mat(S_kappas(j));
            
            for k=1:length(S_grids)
                S_grid = cell2mat(S_grids(k));
                
                S_oldint = [ ...
                    S_method ...
                    S_kappa 'Sato_oldint_'...
                    S_grid];
                
                h = plot_flow_num_data(d, u_t, gca, S_oldint);
                set(h, 'Color', mcolor(i));
                
                S_legend(count) = {[S_method ': $\kappa = 0.' S_kappa(4:5) ...
                    ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
                
                count = count + 1;
            end
        end
    end
    
    hold off
    xlabel('$r^*$', 'Interpreter', 'Latex');
    ylabel('$v_{\theta} ^*$', 'Interpreter', 'Latex');
    box on
    title('oldint');
    legend(S_legend, 'Location', 'North', 'Interpreter', 'Latex');
    set(gca, 'XLim', [3 5]);
    
    clear S_legend
end

if(B_vel_OK)
    figure
    hold on
    % Analytical solution
    plot(rio/d, vtheta_r(rio) / u_t, '-k');
    set(gca, 'ColorOrder', [0 0 0]);
    set(gca, 'LineStyleOrder', {'^', 'o','x', '+'});    
    S_legend(1) = {'Analytical'};
    
    % Numerical solution
    count = 2;
    for i=1:length(S_methods)
        S_method = cell2mat(S_methods(i));
        
        for j=1:length(S_kappas)
            S_kappa = cell2mat(S_kappas(j));
            
            for k=1:length(S_grids)
                S_grid = cell2mat(S_grids(k));
                
                S = [ ...
                    S_method ...
                    S_kappa 'Sato_'...
                    S_grid];
                
                h = plot_flow_num_data(d, u_t, gca, S);
                set(h, 'Color', mcolor(i));
                
                S_legend(count) = {[S_method ': $\kappa = 0.' S_kappa(4:5) ...
                    ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
                
                count = count + 1;
            end
        end
    end
    
    hold off
    xlabel('$r^*$', 'Interpreter', 'Latex');
    ylabel('$v_{\theta} ^*$', 'Interpreter', 'Latex');
    box on
    legend(S_legend, 'Location', 'North', 'Interpreter', 'Latex');
    set(gca, 'XLim', [3 5]);
end

% Pressure
if(A_p_OK)
    figure
    hold on
    % Analytical solution
    plot(rio/d, p(rio) / tau_t, '-k');
    set(gca, 'ColorOrder', [0 0 0]);
    set(gca, 'LineStyleOrder', {'^', 'o','x', '+'});
    S_legend(1) = {'Analytical'};
    
    % Numerical solution
    count = 2;
    for i=1:length(S_methods)
        S_method = cell2mat(S_methods(i));
        
        for j=1:length(S_kappas)
            S_kappa = cell2mat(S_kappas(j));
            
            for k=1:length(S_grids)
                S_grid = cell2mat(S_grids(k));
                
                S_oldint = [ ...
                    S_method ...
                    S_kappa 'Sato_oldint_'...
                    S_grid];
                
                h = plot_p_num_data(d, tau_t, gca, S_oldint);
                set(h, 'Color', mcolor(i));
                
                S_legend(count) = {[S_method ': $\kappa = 0.' S_kappa(4:5) ...
                    ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
                
                count = count + 1;
            end
        end
    end
    
    hold off
    xlabel('$r^*$', 'Interpreter', 'Latex');
    ylabel('$\Delta p ^*$', 'Interpreter', 'Latex');
    box on
    title('oldint');
    legend(S_legend, 'Location', 'NorthWest');
    set(gca, 'XLim', [3 5]);
end

if(B_p_OK)
    figure
    hold on
    % Analytical solution
    plot(rio/d, p(rio) / tau_t, '-k');
    set(gca, 'ColorOrder', [0 0 0]);
    set(gca, 'LineStyleOrder', {'^', 'o','x', '+'});
    mcolor = 'kb';
    S_legend(1) = {'Analytical'};
    
    % Numerical solution
    count = 2;
    for i=1:length(S_methods)
        S_method = cell2mat(S_methods(i));
        
        for j=1:length(S_kappas)
            S_kappa = cell2mat(S_kappas(j));
            
            for k=1:length(S_grids)
                S_grid = cell2mat(S_grids(k));
                
                S = [ ...
                    S_method ...
                    S_kappa 'Sato_'...
                    S_grid];
                
                h = plot_p_num_data(d, tau_t, gca, S);
                set(h, 'Color', mcolor(i));
                
                S_legend(count) = {[S_method ': $\kappa = 0.' S_kappa(4:5) ...
                    ': ' S_grid '\times' S_grid '$']}; %#ok<*AGROW>
                
                count = count + 1;
            end
        end
    end
    
    hold off
    xlabel('$r^*$', 'Interpreter', 'Latex');
    ylabel('$\Delta p ^*$', 'Interpreter', 'Latex');
    box on
    legend(S_legend, 'Location', 'NorthWest');
    set(gca, 'XLim', [3 5]);
end
%% Order graph
if(k_order_OK)
    figure
    hold on
    count = 1;
    
    for k=1:length(S_grids)
        S_grid = cell2mat(S_grids(k));
        
        for i=1:length(S_methods)
            S_method = cell2mat(S_methods(i));
            
            %         plot(lkappa, log( squeeze( e_oldint(i,:,k) ) ), ['-x' gcolor(k)]);
            %         S_legend2(count) = {['A: ' S_method ': $' S_grid '\times' S_grid '$']};
            %         count = count + 1;
            
            plot(lkappa, log( squeeze( e(i,:,k) ) ), ['-' msymbol(i) gcolor(k)]);
            S_legend2(count) = {['B: ' S_method ': $' S_grid '\times' S_grid '$']};
            count = count + 1;
            
        end
    end
    hold off
    xlabel('$\log \kappa$', 'Interpreter', 'Latex');
    ylabel('$\log e_{L2}$', 'Interpreter', 'Latex');
    box on
    legend(S_legend2);
end

if(ldx_ldy_OK)
    
    for i=1:length(S_methods)
        
        S_method = cell2mat(S_methods(i));
        
        count = 1;
        
        figure
        hold on
        
        for j=1:length(S_kappas)
            
            S_kappa = cell2mat(S_kappas(j));
            
            h = plot(ldx, log( squeeze( e_oldint(i,j,:) ) ), '--k');
            set(h, 'Marker', ksymbol(j), 'MarkerFaceColor', 'w');
            
            S_legend(count) = {['A: $\kappa = 0.' S_kappa(4:5) '$']}; %#ok<*AGROW>
            
            count = count + 1;

            h = plot(ldx, log( squeeze( e(i,j,:) ) ), '-k');
            set(h, 'Marker', ksymbol(j), 'MarkerFaceColor', 'w');
            
            S_legend(count) = {['B: $\kappa = 0.' S_kappa(4:5) '$']}; %#ok<*AGROW>
            
            count = count + 1;
            
        end

        % 1st order
        plot([-6.0, -6.8], [0.0, -0.8], '-k');
        % 2nd order
        plot([-5.8, -6.4], [-2.5, -3.7], '-k');

        hold off
        xlabel('$\log dx$', 'Interpreter', 'Latex');
        ylabel('$\log e_{L2} ^{\bf{u}}$', 'Interpreter', 'Latex');
        box on
        axis square
        set(gca, 'YLim', [-3.8 0.2]);
        dar = get(gca, 'DataAspectRatio');
        ht1 = text(-6.4, -0.4, '1st order line', 'VerticalAlignment', 'top');
        set(ht1, 'Rotation', atand(dar(1)/dar(2)), 'Interpreter', 'Latex');
        ht2 = text(-6.1, -3.1, '2nd order line', 'VerticalAlignment', 'top');
        set(ht2, 'Rotation', atand(2*dar(1)/dar(2)), 'Interpreter', 'Latex');
        hl = legend(S_legend, 'Location', 'nw');
        title(hl, S_method, 'Interpreter', 'Latex');
        
        
    end
end

if(alpha_kappa_OK)
    figure
    hold on
    
    count = 1;
    
    for i=1:length(S_methods)
        S_method = cell2mat(S_methods(i));
        
        plot(kappa, squeeze(alpha_oldint(i,:)), ['-x' mcolor(i)]);
        S_legend2(count) = {['A: ' S_method]};
        
        count = count + 1;
        
        plot(kappa, squeeze(alpha(i,:)), ['-o' mcolor(i)]);
        S_legend2(count) = {['B: ' S_method]};
        
        count = count + 1;
    end
    
    hold off
    box on
    xlabel('$\kappa$', 'Interpreter', 'Latex');
    ylabel('$\alpha$', 'Interpreter', 'Latex');
    
    legend(S_legend2, 'Location', 'Best');
    set(gca, 'XLim', [0 0.42]);
end

% % Temperature
% figure
% hold on
% % Analytical solution
% hT1 = plot(rsii/d, Tsii(rsii), 'k');
% plot(rio/d, Tio(rio), 'k');
% set(gca, 'ColorOrder', [0 0 0]);
% set(gca, 'LineStyleOrder', {'*', '^', 's', 'o', 'x', '+'});
% % Numerical solution
% [hTs, Sl] = plot_phi_num_data(d, gca, S);
% hold off
% xlabel('r^*');
% ylabel('T^*');
% box on
% legend([hT1, hTs], Sl, 'Location', 'South');
% set(gca, 'XLim', [1 5]);
end
%% Other functions
function e = compute_error_UV(Ftheta, mu, ri, ro, u_t, S, pnorm)

load(S);

try
    [Nx, Ny] = size(x);
catch
    fprintf('Could not find results (e = NaN) in: %s\n', S);
    e = NaN;
    return;
end

convergence_ok = (sigmas_flow(1) < tol_flow) && ...
    (sigmas_flow(2) < tol_flow) && ...
    (sigmas_flow(4) < tol_flow);

if ~convergence_ok
    fprintf('Not convergence (e = NaN) in: %s\n', S);
    e = NaN;
    return;
end

Lx = x(Nx,1);
Ly = Lx;

xa = x - Lx/2;
ya = y - Ly/2;

% Preallocate
vtheta = zeros(Nx,Ny);

eSS = 0;
count = 0;

% Compute analytic values
for i=1:Nx
    for j=1:Ny
        % Get r, theta from x, y
        r = sqrt(xa(i,j)^2 + ya(i,j)^2);
        theta = atan2(ya(i,j), xa(i,j));
        
        % Velocity
        if ( (r >= ri) && (r <= ro) )
            vtheta(i,j) = (Ftheta*r) / (2*mu*(ro^2 - ri^2)) * ...
                (ro^2*log(r/ro) + ri^2*log(ri/r) - (ri*ro./r)^2*log(ri/ro));
            
            vnum = -U(i,j)*sin(theta) + V(i,j)*cos(theta);
            
            eSS = eSS + abs( (vnum - vtheta(i,j)) / u_t) ^ pnorm;
            count = count + 1;
        
        else            
            vtheta(i,j) = NaN;
        end

    end
end

e = (eSS / count) ^ (1/pnorm);
end
%% plot flow
function h = plot_flow_num_data(d, u_t, hax, S, sgraph)
% Plots at theta = 135

if nargin<5, sgraph = '-'; end

load(S);

[Nx, Ny] = size(x);

dx = x(3,1) - x(2,1);
dy = dx;

j = (Ny/2)+1;
k = 1;
for i=(Nx/2):-1:2
    Us(k) = U(i,j);
    Vs(k) = V(i,j);
    j = j+1;
    k = k+1;
end

rnum = NaN*ones(1,(Nx/2)-1);

rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);

for i=1:(Nx/2-2)
    rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
end

vnum = (-Us*sind(135) + Vs*cosd(135));

h = plot(hax, rnum/d, vnum/u_t, sgraph);

end
%% plot p
function h = plot_p_num_data(d, tau_t, hax, S, sgraph)

if nargin<5, sgraph = '-'; end

load(S);

[Nx, Ny] = size(x);

dx = x(3,1) - x(2,1);
dy = dx;

j = (Ny/2)+1;
k = 1;
for i=(Nx/2):-1:2
    pnum(k) = p(i,j);
    j = j+1;
    k = k+1;
end

rnum = NaN*ones(1,(Nx/2)-1);

rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);

for i=1:(Nx/2-2)
    rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
end

h = plot(hax, rnum/d, pnum/tau_t, sgraph);

end

%% Compute order and R2
function [m, n, rsq] = compute_lr(x, y)

rsq_tol = -0.90;

% 2
p = polyfit(x,y,1);
% Slope
m = p(1);
% Intercept
n = p(2);
% 3
yfit = (polyval(p,x))';
% 4
yresid = y - yfit;
% 5
SSresid = sum(yresid.^2);
% 6
SStotal = (length(y)-1) * var(y);
% 7
rsq = 1 - SSresid/SStotal;

if rsq < rsq_tol
    fprintf('Poor value of rsq = %.2f < %.2f, setting NaN\n', rsq, rsq_tol);
    m = nan;
    n = nan;
end
end

% function [hp, Slegend] = plot_phi_num_data(d, hax, S)
%
% n = length(S);
% hp = zeros(1,n);
%
% Slegend(1) = {'Analytical'};
%
% for l=1:n
%
%     load(cell2mat(S(l)));
%
%     [Nx, Ny] = size(nodes_x_cpu_var);
%
%     dx = nodes_x_cpu_var(3,1) - nodes_x_cpu_var(2,1);
%     dy = dx;
%
%     j = (Ny/2)+1;
%     k = 1;
%     for i=(Nx/2):-1:2
%         phi(k) = phi_cpu_var(i,j);
%         j = j+1;
%         k = k+1;
%     end
%
%     rnum = NaN*ones(1,(Nx/2)-1);
%
%     rnum(1) = sqrt((dx/2)^2 + (dy/2)^2);
%
%     for i=1:(Nx/2-2)
%         rnum(i+1) = rnum(i) + sqrt((dx)^2 + (dy)^2);
%     end
%
%     Slegend(l+1) = {[num2str(Nx) '\times' num2str(Ny)]};
%     hp(l) = plot(hax, rnum/d, phi);
%
% end
%
% end
%% info cases
function info_cases(base_dir, S_methods, S_kappas, S_grids)
% check the convergence info for every case and prints table

fprintf('%30s %5s %8s %8s %8s %8s %8s\n', ...
    'case', 'it', 'su', 'sv', 'sc', 'gu', 'gv');

for i=1:length(S_methods)
    S_method = cell2mat(S_methods(i));
    
    for j=1:length(S_kappas)
        S_kappa = cell2mat(S_kappas(j));
        
        S_k = ['0.' S_kappa(4:5)];
        
        kappa(j) = str2double(S_k);
        
        for k=1:length(S_grids)
            S_grid = cell2mat(S_grids(k));
            
            S_oldint = [ base_dir '/' ...
                S_method ...
                S_kappa 'Sato_oldint_'...
                S_grid];
            
            info_cases_(S_oldint);
            
            S = [ base_dir '/' ...
                S_method ...
                S_kappa 'Sato_'...
                S_grid];

            info_cases_(S);

        end
    end
end
end

function info_cases_(sfile)
%% See if file exists
try
    load(sfile); %#ok<*LOAD>
catch
    fprintf('%30s %5s %8s %8s %8s %8s %8s\n', ...
        sfile, 'file', ' not', ' found', '!!', ' ', ' ');
    return;
end
%% See if convergence was attained
if (( (glob_norm_flow(1) >= tol_flow) || ...
        (glob_norm_flow(2) >= tol_flow) ))
    
    fprintf('%30s %5.2f %8.2e %8.2e %8.2e %8.2e %8.2e\n', ...
        sfile, double(it_flow)/double(max_it_flow), ...
        sigmas_flow(1), sigmas_flow(2), sigmas_flow(4), ...
        glob_norm_flow(1), glob_norm_flow(2));
    
end
end