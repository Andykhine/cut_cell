function [wake_D, CDy, CDPy, p0, ppi] = intFrictionCoeffs(pathtofile, graph_OK, text_OK)
% Get friction coefficients from interpolated stress and pressure over
% cylinder surface
% Assumes flow is from south to north

% INPUT: post_process.m
%% Cleaning
clc
close all
%% Settings
set(groot,'DefaultAxesTickLabelInterpreter','Latex');
set(groot,'DefaultLegendInterpreter','Latex');
%% Process post_process.m file
if text_OK, disp(pathtofile); end
cd(pathtofile)

load('results.mat', 'p')
Pstat = mean(p(2:end-1,1));

% Get wake length
[~, line] = unix("grep -i '% Wake length =' post_process.m");
wake = sscanf(line, "%% Wake length =    %f [m]");

% call test program generated script
post_process

% Set wake length per diameter
if(isempty(wake))
    wake_D = NaN;
else
    wake_D = wake/D;
end
%% Computations
% interpolationType = 0 : pchip
% interpolationType = 1 : spline
interpolationType = 0;

% Puntos periodicos al inicio y final para guiar interpolacion
addPeriodicPoints = true;
nPeriodicPoints = 3;

% flag1: Plot valores de esfuerzo usados en la interpolacion
% flag2: Plot valores usados en interpolacion junto con curva ajustada
% flag3: Plot valores de presión usados en interpolacion y curva de presión
% ajustada en Matlab.
% flag4: figuras 2 y 3 combinadas y normalizadas.

flag1 = false;
flag2 = false;
flag3 = false;
flag4 = false;

if(graph_OK), flag4 = true; end

% Print input values to check for inconsistencies
if(text_OK)
    fprintf("Cylinder coordinates: %10.5f [m] %10.5f [m]\n", ...
        xCyl, yCyl);
    fprintf("rho %10.5f [kg/m^3] mu: %10.5f [Pa s]\n", ...
        rho, mu);
    fprintf("v_inf: %10.5f [m/s] D: %10.5f [m]\n", ...
        v_inf, D);
    fprintf("Re: %10.5f\n", rho*v_inf*D / mu);
    
    if addPeriodicPoints
        fprintf("Interpolación usando puntos periódicos.\n");
    else
        fprintf("Interpolación sin usar puntos periódicos.\n");
    end
end
% Relative coordinates
xRel = cutSegmentCenterVCells(:,2) - xCyl;
yRel = cutSegmentCenterVCells(:,3) - yCyl;
% Compute angle
angle = atan2(yRel, xRel);
type = results1Vy(:, 2);
tau = results0Vy(:, 10);
area = results0Vy(:, 8);

% Sorted quantities
[~, I] = sort(angle);
angleSort = angle(I);
tauSort = tau(I);
areaSort = area(I);

% Select regular cells (type 0)
sel = (type == 0);
selSort = sel(I);
%% Valores de tau disponibles
if flag1
    h1 = figure(1);
    box on
    plot(angleSort(selSort), tauSort(selSort), '.k');
    xlabel('angle')
    ylabel('\tau_{nx} [Pa]')
    title("Valores usados en interpolación");
end

%% Fit
xData = angleSort(selSort);
yData = tauSort(selSort);

% Put
if addPeriodicPoints
    yData = [yData(end-2:end); yData; yData(1:3)];
    xData = [xData(end-2:end)-2*pi; xData; xData(1:3)+2*pi];
end

if interpolationType == 0
    fitTau = pchip(xData, yData);
elseif interpolationType == 1
    fitTau = spline(xData, yData);
end

% Evaluate fitted values
tauFit = ppval(fitTau, angleSort);

if flag2
    h2 = figure(2);
    box on
    hold on
    plot(angleSort, tauFit, '-k')
    plot(angleSort(selSort), tauSort(selSort), '.k');
    if addPeriodicPoints
        plot(xData(1:3), yData(1:3), '.b', ...
            xData(end-2:end), yData(end-2:end), '.b')
    end
    xlabel('angle')
    ylabel('\tau_{nx} [Pa]')
    title("Interpolación")
end

Fy = sum(areaSort .* tauFit);
CDy = Fy / (0.5 * D * rho * v_inf^2);
%% Pressure interpolation
pVal = results0Py(:, 3);
pSort = pVal(I);
selPSort = ~(isnan(pSort));
xData = angleSort(selPSort);
yData = pSort(selPSort);

if addPeriodicPoints
    yData = [yData(end-2:end); yData; yData(1:3)];
    xData = [xData(end-2:end)-2*pi; xData; xData(1:3)+2*pi];
end

if interpolationType == 0
    fitP = pchip(xData, yData);
elseif interpolationType == 1
    fitP = spline(xData, yData);
end

%fitP = pchip(xData(1:end-1), yData(1:end-1));
%fitP = spline(xData(1:end-1), yData(1:end-1));

% Evaluate fitted values
pFit = ppval(fitP, angleSort);

pFactor = results0Py(:,2);
pFactorSort = pFactor(I);
FyP = -sum(pFactorSort .* pFit);
CDPy = FyP / (0.5 * D * rho * v_inf^2);

if flag3
    h4 = figure(4);
    box on
    hold on
    plot(angleSort, pFit, '-k')
    plot(angleSort(selPSort), pSort(selPSort), '.k')
    if addPeriodicPoints
        plot(xData(1:3), yData(1:3), '.b', ...
            xData(end-2:end), yData(end-2:end), '.b')
    end
    xlabel('angle')
    ylabel('P [Pa]')
    title("Interpolación presión")
end

if flag4
    h5 = figure(5);
    angleRotated = angleSort - pi/2;
    angleRotated(angleRotated > pi) = angleRotated(angleRotated > pi) - 2*pi;
    angleRotated(angleRotated < -pi) = angleRotated(angleRotated < -pi) + 2*pi;
    
    [angleRotatedSort, I] = sort(angleRotated);
    pSortRotated = pSort(I);
    selPSortRotated = selPSort(I);
    selSortRotated = selSort(I);
    tauSortRotated = tauSort(I);
    
    box on
    hold on
    plot(angleRotatedSort, pFit(I) / (0.5*rho*v_inf*v_inf*D), '-k')
    p1 = plot(angleRotatedSort(selPSortRotated), pSortRotated(selPSortRotated) / (0.5*rho*v_inf*v_inf*D), '.k');
    %
    plot(angleRotatedSort, tauFit(I) / (0.5*rho*v_inf*v_inf*D), '-r')
    p2 = plot(angleRotatedSort(selSortRotated), tauSortRotated(selSortRotated) / (0.5*rho*v_inf*v_inf*D), '.r');
    %
    xlabel('$\theta$', 'Interpreter', 'Latex');
    legend([p1 p2], ...
        '$p/(0.5 \rho D u_{\infty} ^2)$', '$\tau/(0.5 \rho D u_{\infty} ^2)$', ...
        'Location', 'North');
end

%% Print results
P0 = ppval(fitP, pi/2);
Ppi = ppval(fitP, -pi/2);

p0 = (P0 - Pstat) / (0.5*rho*v_inf*v_inf);
ppi = (Ppi - Pstat) / (0.5*rho*v_inf*v_inf);

if text_OK
    fprintf("%8s %8s\n", "Cf", "Cp");
    fprintf("%8.5f %8.5f\n", CDy, CDPy);
    
    fprintf("%10s %10s %10s\n", "Pstat [Pa]", "P(0) [Pa]", "P(pi) [Pa]");    
    fprintf("%10.3f %10.3f %10.3f\n", Pstat, P0, Ppi);
    
    fprintf("%8s %8s\n", "p0", "ppi");
    fprintf("%8.5f %8.5f\n", p0, ppi);
    
    fprintf("Regular nodes: %i\n", sum(type == 0));
    fprintf("Slave nodes: %i\n", sum(type == 1));
    fprintf("Solid nodes: %i\n", sum(type == 2));
end
end