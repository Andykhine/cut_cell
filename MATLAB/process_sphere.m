function process_sphere
%VORTICITY Computes vorticity for flow past sphere problem
%% Clean
close all
clc
%% Settings
set(groot,'DefaultAxesTickLabelInterpreter','Latex');
set(groot,'DefaultLegendInterpreter','Latex');

%% Radius
R = 0.1;
%% Load data
load('results.mat', 'x', 'y', 'z', 'U', 'V', 'W', 'p');
%% Create grid for MATLAB
xv = squeeze(x(:,2,2));
yv = squeeze(y(2,:,2));
zv = squeeze(z(2,2,:));

[X, Y, Z] = meshgrid(xv,yv,zv);
%% Seek indexes for planes

for i=1:length(xv)
    if(xv(i) >= 3*R)
        i1 = i;
        break
    end
end

for j=1:length(yv)
    if(yv(j) >= R)
        j1 = j;
        break
    end
end

for j=j1:length(yv)
    if(yv(j) >= 2*R)
        j2 = j;
        break
    end
end

for j=j2:length(yv)
    if(yv(j) >= 3*R)
        j3 = j;
        break
    end
end


%% Permute indexes of matrices for MATLAB grid
Up = permute(U, [2 1 3]);
Vp = permute(V, [2 1 3]);
Wp = permute(W, [2 1 3]);
P = permute(p, [2 1 3]);
%% Compute curl and angular velocity
[curlx,curly,curlz,cav] = curl(X, Y, Z, Up, Vp, Wp);
%% Graphs
% figure
% plot(zv,squeeze(U(1,j1,:)), '-b');

Xs = squeeze(X(j1,:,:));
Zs = squeeze(Z(j1,:,:));
Us = squeeze(Up(j1,:,:)); %squeeze(curly(:,i1,:));
Ws = squeeze(Wp(j1,:,:)); %squeeze(curlz(:,i1,:));
cavs = squeeze(cav(j1,:,:));

figure
% subplot(2,2,1)
hold on
contourf(Xs, Zs, cavs);
quiver(Xs, Zs, Us, Ws, 'm');
hold off
xlabel('x');
ylabel('z');
% set(gca, 'XLim', [0*R 4*R]);
% set(gca, 'YLim', [0*R 4*R]);
box on
% axis equal

clear Xp Zp Us Ws cavp
Xs = squeeze(X(j2,:,:));
Zs = squeeze(Z(j2,:,:));
Us = squeeze(Up(j2,:,:)); %squeeze(curly(:,i2,:));
Ws = squeeze(Wp(j2,:,:)); %squeeze(curlz(:,i2,:));
cavs = squeeze(cav(j2,:,:));

figure
% subplot(2,2,2)
hold on
contourf(Xs, Zs, cavs);
quiver(Xs, Zs, Us, Ws, 'm');
hold off
xlabel('x');
ylabel('z');
% set(gca, 'XLim', [0*R 4*R]);
% set(gca, 'YLim', [0*R 4*R]);
box on
% axis equal

clear Xp Zp Us Ws cavp
Ys = squeeze(Y(:,i1,:));
Zs = squeeze(Z(:,i1,:));
Vs = squeeze(Vp(:,i1,:)); %squeeze(curlx(j1,:,:));
Ws = squeeze(Wp(:,i1,:)); %squeeze(curlz(j1,:,:));
cavs = squeeze(cav(:,i1,:));

figure
% subplot(2,2,[3 4])
hold on
contourf(Ys, Zs, cavs);
quiver(Ys, Zs, Vs, Ws, 'm');
hold off
xlabel('x');
ylabel('y');
zlabel('z');
set(gca, 'XLim', [0*R 10*R]);
set(gca, 'YLim', [0*R 8*R]);
box on
% axis equal

[sx, sy, sz] = meshgrid(2*R:0.5*R:6*R, 2.8*R, 2*R:0.5*R:6*R);

figure
hold on
streamline(X, Y, Z, Up, Vp, Wp, sx, sy, sz);
hold off
xlabel('x');
ylabel('y');
zlabel('z');
box on
view(3);
axis tight
end