function draw_data_CUT_CELL
clc
%% Prompt for selection
str = {'Process raw data and save MAT file'; 'Select MAT file'; ...
    'Merge two MAT files'};
[selection, ok] = listdlg('PromptString', 'Select a method:', ...
    'SelectionMode', 'single', ...
    'ListSize', [320 80], ...
    'ListString', str);
%% Choices
if (~ok)
    disp('No valid operation... Terminating');
    return
end

% Graphs flags
mesh_ok = false;
phi_ok = true;
validation_ok = true;

if (selection == 1)
    [x, y, faces_x, faces_y, poly_coeffs, phi, val_data, S_prefix] = ...
        data_process_and_save(validation_ok);
elseif (selection == 2)
    [x, y, faces_x, faces_y, poly_coeffs, phi, val_data, S_prefix] = ...
            draw_data_from_MAT_file;
elseif (selection == 3)
    %     [x, y, DELTA_x, DELTA_y, solid_vars, u_vel, v_vel, pressure, ...
    %         k_turb, epsilon, S_prefix] = ...
    %         draw_data_merge_MAT_files;
    %     merge_ok = true;
end
%% Call drawing function
draw_data(x, y, faces_x, faces_y, poly_coeffs, phi, val_data, S_prefix, ...
    mesh_ok, phi_ok, validation_ok);
end

function [x, y, faces_x, faces_y, poly_coeffs, phi, val_data, S_prefix] = ...
    draw_data_from_MAT_file %#ok<STOUT>
%% Initializing
[filename, pathname] = uigetfile( ...
    {'*.mat','MAT-files (*.mat)'}, ...
    'Choose a file to load results', 'data.mat');
if (isequal(filename, 0) || isequal(pathname, 0))
    error('Need to load a MAT file to proceed with graphics');
end
%% Loading
load([pathname filename]);
end

function [x, y, faces_x, faces_y, poly_coeffs, phi, val_data, S_prefix] = ...
    data_process_and_save(validation_ok)
%% List selection
[selection, ~] = listdlg('PromptString', 'Select results:', ...
    'SelectionMode', 'single', 'ListSize', [320 80], ...
    'ListString', {'Central Processing Unit', 'Graphics Processing Unit'});

if (selection == 1)
    cd CPU
    disp('Processing CPU data');
elseif (selection == 2)
    cd GPU
    disp('Processing GPU data');
end
%% Changing filenames if they exist
if (exist('nodes_x.dat', 'file') && exist('nodes_y.dat', 'file') && ...
        exist('faces_x.dat', 'file') && exist('faces_y.dat', 'file') ...
        && exist('poly_coeffs.dat', 'file') && exist('phi.dat', 'file'))
    !rename .dat _data.m *.dat
else
    error('Missing .DAT files!!');
end
%% Extracting data
nodes_x_data; nodes_y_data; faces_x_data; faces_y_data; 
poly_coeffs_data; phi_data;
gamma_v_data; constant_d_data; phi_b_v_data;
dphidNb_v_data;

!rm *.m

vars_in_workspace = whos;

if ~isempty(strfind(vars_in_workspace(1).name, 'gpu'))
    % GPU case variables
    [Nx, Ny] = size(node_x_gpu_var);
    S = ['GPU_' num2str(Nx) '_' num2str(Ny)];
    x = nodes_x_gpu_var; y = nodes_y_gpu_var;
    faces_x = faces_x_gpu_var; faces_y = faces_y_gpu_var;
    poly_coeffs = poly_coeffs_gpu_var;
    phi = phi_gpu_var;
    gamma_v = gamma_v_gpu_var;
    constant_d = constant_d_gpu_var;
    phi_b_v = phi_d_v_gpu_var;
    dphidNb_v = dphidNb_v_gpu_var;
    
    clear nodes_x_gpu_var nodes_y_gpu_var faces_x_gpu_var faces_y_gpu_var;
    clear poly_coeffs_gpu_var phi_gpu_var;
    clear gamma_v_gpu_var constant_d_gpu_var phi_d_v_gpu_var;
    clear dphidNb_v_gpu_var;
else
    % CPU case variables
    [Nx, Ny] = size(nodes_x_cpu_var);
    S = ['CPU_case_' num2str(Nx) '_' num2str(Ny)];
    x = nodes_x_cpu_var; y = nodes_y_cpu_var;
    faces_x = faces_x_cpu_var; faces_y = faces_y_cpu_var;
    poly_coeffs = poly_coeffs_cpu_var;
    phi = phi_cpu_var;
    gamma_v = gamma_v_cpu_var;
    constant_d = constant_d_cpu_var;
    phi_b_v = phi_b_v_cpu_var;
    dphidNb_v = dphidNb_v_cpu_var;
    
    clear nodes_x_cpu_var nodes_y_cpu_var faces_x_cpu_var faces_y_cpu_var;
    clear poly_coeffs_cpu_var phi_cpu_var;
    clear gamma_v_cpu_var constant_d_cpu_var phi_b_v_cpu_var;
    clear dphidNb_v_cpu_var;
end
disp(S);
%% Preparing validation data
if(validation_ok)
    
    rsi = sqrt((-1)*constant_d(3));
    ri = sqrt((-1)*constant_d(2));
    ro = sqrt((-1)*constant_d(1));
    
%     phisi = phi_b_v(3);
    phio = phi_b_v(1);
    
    gamma_f = gamma_v(1);
    gamma_s = gamma_v(3);
    
    C1_val = (-1)*gamma_s*dphidNb_v(3)*rsi;
    %% Analytical solution
    % radius
    r = linspace(rsi, ro, 512);
    val_data.r_delta_val = r/rsi;
    n_data = length(r);
    % phi
    for i=1:(n_data/2)
        val_data.phi.val(i) = phio + ...
            (C1_val/gamma_f)*log(ro/ri) + (C1_val/gamma_s)*log(ri/r(i));
    end
    
    for i=(n_data/2)+1:n_data
        val_data.phi.val(i) = phio + (C1_val/gamma_f)*log(ro/r(i));
    end
    % qr*r = C1
    val_data.C1 = C1_val;
    %% Numerical solution
    for i=(Nx/2)+1:Nx
        j = Nx/2;
        val_data.phi.N(i-j) = 0.5*(phi(j,i) + phi(j+1,i));
        val_data.phi.E(i-j) = 0.5*(phi(i,j) + phi(i,j+1));
        val_data.phi.NE(i-j) = phi(i,i);
    end
    
    Delta_x = (x(2,1) - x(1,1))*2;
    
    % Radii
    val_data.r_delta_NE(1) = (Delta_x/rsi)*sqrt(0.5);
    for i=2:(Nx/2)-1
        val_data.r_delta_NE(i) = val_data.r_delta_NE(i-1) + ...
            (Delta_x/rsi)*sqrt(2);
    end
    val_data.r_delta_NE(Nx/2) = val_data.r_delta_NE((Nx/2)-1) + ...
        (Delta_x/rsi)*sqrt(0.5);
    
    val_data.r_delta_E_N(1) = (Delta_x/rsi)/2;
    for i=2:(Nx/2)-1
        val_data.r_delta_E_N(i) = val_data.r_delta_E_N(i-1) + ...
            (Delta_x/rsi);
    end
    val_data.r_delta_E_N(Nx/2) = val_data.r_delta_E_N((Nx/2)-1) + ...
        (Delta_x/rsi)/2;
else
    val_data = [];
end

%% Saving data to MAT file
S_prefix = [S(1:3) ' ' num2str(Nx) '\times' num2str(Ny) ': '];
save(S);

cd ..
end

function draw_data(x, y, faces_x, faces_y, poly_coeffs, phi, val_data, S_prefix, ...
    mesh_ok, phi_ok, validation_ok)
[Nx, Ny] = size(x);
%% Mesh graph
if(mesh_ok)
    [n_curves, ~] = size(poly_coeffs);
    s = zeros(n_curves, Nx, Ny);
    for i=1:n_curves
        s(i,:,:) = poly_coeffs(i,1)*x.^2 + poly_coeffs(i,2)*y.^2 + poly_coeffs(i,3)*0^2 + ...
            poly_coeffs(i,4)*x.*y + poly_coeffs(i,5)*x.*0 + poly_coeffs(i,6)*y.*0 + ...
            poly_coeffs(i,7)*x + poly_coeffs(i,8)*y + poly_coeffs(i,9)*0 + ...
            poly_coeffs(i,10);
    end
    
%     [n_lines, ~] = size(line_points);
%     n_lines = n_lines/2;
        
    figure;
    plot(x, y, 'ok', 'MarkerFaceColor', 'k', 'MarkerSize', 1.0);
    xlabel('x'); ylabel('y');
    hold on
    for I=1:(Nx-1)
        for J=1:(Ny-1)
            plot([x(I,J) x(I+1,J)], [faces_y(I,J) faces_y(I+1,J)], '--b');
        end
    end
    for I=1:(Nx-1)
        for J=1:(Ny-1)
            plot([faces_x(I,J) faces_x(I,J+1)], [y(I,J) y(I,J+1)], '--b');
        end
    end
    for i=1:n_curves
        contour(x, y, squeeze(s(i,:,:)),'LevelList', 0, 'Color', 'r');
    end
%     for count=1:n_lines
%         i = 2*count-1;
%         plot(squeeze(line_points(i:i+1,1)), ... 
%             squeeze(line_points(i:i+1,2)), '--k');
%     end
    hold off
    title([S_prefix 'Mesh']);
    axis equal
    axis tight
    Graph_Format;
end
if(phi_ok)
    figure;
    hs = mesh(x, y, phi);
    xlabel('x'); ylabel('y'); zlabel('\phi');
    ht = title([S_prefix 'Mesh']);
    set(gcf, 'Color', 'k');
    set(ht, 'Color', 'w');
    set(gca, 'Color', 'k', 'XColor', 'w', 'YColor', 'w', 'ZColor', 'w');
    set(hs, 'FaceColor', 'none');
%     Graph_Format;
end

if(validation_ok)
    figure;
    plot(val_data.r_delta_val, val_data.phi.val, '-r');
    hold on
    plot(val_data.r_delta_E_N, val_data.phi.N, 'xk');
    plot(val_data.r_delta_E_N, val_data.phi.E, '+m');
    plot(val_data.r_delta_NE, val_data.phi.NE, 'ob');
    hold off
    xlabel('r/\delta'); ylabel('\phi');
    set(gca, 'XLim', [min(val_data.r_delta_val), ...
        max(val_data.r_delta_val)]);
    h_l = legend('Analytical solution', 'N', 'E', 'NE');
    set(h_l, 'Location', 'southwest');
    title([S_prefix 'Validation']);
    Graph_Format;
end
end
