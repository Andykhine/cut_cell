#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/poll.h>
#include <mgl2/mgl_cf.h>
#include <mgl2/fltk.h>
#include <signal.h>
#include <pthread.h>
#include <omp.h>
#include <unistd.h>

/* MACROS */
#define MAX(a,b) ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a > _b ? _a : _b; })
#define MIN(a,b) ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a < _b ? _a : _b; })

/* CONSTANTS */
#define DEBUG_ME 0

#define TMINS 4
#define DETACHED_ANIM 0

#define COLOR_R "\033[;0;91m"
#define COLOR_G "\033[;0;92m"
#define COLOR_Y "\033[;0;93m"
#define COLOR_B "\033[;0;94m"
#define COLOR_M "\033[;0;95m"
#define COLOR_C "\033[;0;96m"

#define COLOR_B_R "\033[;1;31m"
#define COLOR_B_G "\033[;1;32m"
#define COLOR_B_Y "\033[;1;33m"
#define COLOR_B_B "\033[;1;34m"
#define COLOR_B_M "\033[;1;35m"
#define COLOR_B_C "\033[;1;36m"

#define COLOR_END "\033[0m"

/* STRUCT */
typedef struct {

  int Nx;
  int Ny;

  int flow_done_OK;
  int turb_OK;

  double *U;
  double *V;
  double *p;
  double *k_turb;
  double *eps_turb;
  double *mu_turb;
  double *phi;
  double *gL;
  
  HMGL gr;

  HMDT x_ih;
  HMDT y_ih;
  
  HMDT U_ih;
  HMDT V_ih;
  HMDT p_ih;

  HMDT k_ih;
  HMDT eps_ih;
  HMDT mu_ih;

  HMDT phi_ih;
  HMDT gL_ih;
} Data_mgl;

/* GLOBAL VARIABLES */
sig_atomic_t gr_flag = 0;
pthread_cond_t gr_flag_cv;

pthread_mutex_t gr_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t data_mutex = PTHREAD_MUTEX_INITIALIZER;

/* FUNCTION PROTOTYPES */
void poll_read_size_from_server(int *Nx, int *Ny, 
				int *flow_done_OK, int *turb_OK,
				const int sockfd, 
				struct pollfd *fds, 
				const double timeout);
void poll_read_array_from_server(double *array, 
				 const int Nx, const int Ny, 
				 const char *msg, const int sockfd, 
				 struct pollfd *fds, 
				 const double timeout);

void read_size_from_server(int *Nx, int *Ny, 
			   int *flow_done_OK, int *turb_OK,
			   const int sockfd);
void read_array_from_server(double *array, 
			    const int Nx, const int Ny, 
			    const int sockfd);

void print_array(const double *A_, 
		 const int Nx, const int Ny, const char msg[]);
double get_max(const double *A_, const int Nx, const int Ny);
double get_min(const double *A_, const int Nx, const int Ny);
void error(const char *msg);

void set_gr_anims(Data_mgl *data);
void set_mem_anims(Data_mgl *data);
void free_mem_anims(Data_mgl *data);
void update_anims(Data_mgl *data);

void *anim(void *data_void);
void anim_data(void *data_void);
int anim_draw(HMGL gr, void *data_void);

void anim_flow_data(void *data_void);
void anim_phi_data(void *data_void);

void initialize_flag(void);
void set_thread_flag(int flag_value);

/* MAIN */
int main(int argc, char *argv[]){

  Data_mgl data;

  struct sockaddr_in serv_addr;
  struct hostent *server;
  struct pollfd fds;

  pthread_t thr_id, flow_thr_id, phi_thr_id;
  pthread_attr_t attr;

  int sockfd, port, n;

  int i, j, index_;
  int Nx = 10, Ny = 10;
  int flow_done_OK;
  int turb_OK;

  const int detached_OK = DETACHED_ANIM;
  
  const double timeout = (TMINS * 60 * 1000);

  /* Seed values, will change when read from server */
  data.flow_done_OK = 0;
  data.turb_OK = 0;

  data.Nx = Nx;
  data.Ny = Ny;

  data.U = NULL;
  data.V = NULL;
  data.p = NULL;
  data.k_turb = NULL;
  data.eps_turb = NULL;
  data.mu_turb = NULL;
  data.phi = NULL;
  data.gL = NULL;

  data.gr = NULL;
  
  data.x_ih = mgl_create_data();
  data.y_ih = mgl_create_data();
  data.U_ih = mgl_create_data();
  data.V_ih = mgl_create_data();
  data.p_ih = mgl_create_data();
  data.k_ih = mgl_create_data();
  data.eps_ih = mgl_create_data();
  data.mu_ih = mgl_create_data();
  data.phi_ih = mgl_create_data();
  data.gL_ih = mgl_create_data();

  port = 4223;
  server = gethostbyname("localhost");
  
  if(argc < 3){
    printf("Usage: %s hostname port\n", argv[0]);
    printf("Not enough arguments passed, completing using localhost:4223\n");
  }
  else{
    port = atoi(argv[2]);
    server = gethostbyname(argv[1]);
  }

  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if(sockfd < 0){
    error("ERROR opening socket");
  }
  
  if(server == NULL) {
    error("ERROR no such server");
  }

  bzero((char *)&serv_addr, sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;

  bcopy((char *)server->h_addr, 
	(char *)&serv_addr.sin_addr.s_addr,
	server->h_length);

  serv_addr.sin_port = htons(port);

  if (connect(sockfd, 
	      (struct sockaddr *)&serv_addr, 
	      sizeof(serv_addr)) < 0){
    error("ERROR connecting");
  }

  /* Initialize the pollfd structure */
  memset((void *) &fds, 0 , sizeof(fds));
  /* Set up the file descriptor */
  fds.fd = sockfd;
  /* Event indicating that there is data to read */
  fds.events = POLLIN;

  /* Get sizes and flags from server */
  poll_read_size_from_server(&(data.Nx), &(data.Ny), 
			     &(data.flow_done_OK), &(data.turb_OK),
			     sockfd, &fds, timeout);
  Nx = data.Nx;
  Ny = data.Ny;

  flow_done_OK = data.flow_done_OK;
  turb_OK = data.turb_OK;

  if(Nx <= 0 || Ny <= 0){
    error("ERROR reading sizes");
  }
  
  printf("(Nx, Ny) = (%d, %d), flow_done_OK = %d, turb_OK = %d\n", 
	 Nx, Ny, flow_done_OK, turb_OK);

  /* Get memory */
  set_mem_anims(&data);
  
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

  /* Launching single thread multiple anims */
  if(detached_OK){
    if(pthread_create(&thr_id, &attr, &anim, (void *) &data) == 0){
      printf("Animation detached thread launched\n");
    }
    else{
      error("ERROR Anim thread");
    }
  }
  else{
    if(pthread_create(&thr_id, NULL, &anim, (void *) &data) == 0){
      printf("Animation thread launched\n");
    }
    else{
      error("ERROR Anim thread");
    }
  }
  
  pthread_cond_wait(&gr_flag_cv, &gr_mutex);  
  /* Here put condition variable as restriction to go on */
  pthread_attr_destroy(&attr);
  
  /* Loop for reading the updated data */
  /* Needs to wait for the server app */
  /* perhaps monitor with poll the sockfd and block the process */
  
  while(1){

    pthread_mutex_lock(&data_mutex);

    // Get sizes and flags from server
    poll_read_size_from_server(&(data.Nx), &(data.Ny), 
			       &(data.flow_done_OK), &(data.turb_OK),
			       sockfd, &fds, timeout);
    Nx = data.Nx;
    Ny = data.Ny;
    
    flow_done_OK = data.flow_done_OK;
    turb_OK = data.turb_OK;
    
    if(!flow_done_OK){

      poll_read_array_from_server(data.U, Nx, Ny, "U", sockfd, &fds, timeout);
      poll_read_array_from_server(data.V, Nx, Ny, "V", sockfd, &fds, timeout);
      poll_read_array_from_server(data.p, Nx, Ny, "p", sockfd, &fds, timeout);

      if(turb_OK){

	poll_read_array_from_server(data.k_turb, Nx, Ny, "k_turb", 
				    sockfd, &fds, timeout);
	poll_read_array_from_server(data.eps_turb, Nx, Ny, "eps_turb", 
				    sockfd, &fds, timeout);
	poll_read_array_from_server(data.mu_turb, Nx, Ny, "mu_turb", 
				    sockfd, &fds, timeout);

      }      

    }
    else{

      poll_read_array_from_server(data.phi, Nx, Ny, "phi", 
				  sockfd, &fds, timeout);
      poll_read_array_from_server(data.gL, Nx, Ny, "gL", 
				  sockfd, &fds, timeout);

    }
    
    /* Here compute velocity at main cells
       normally a 2 step procedure:
       1) center_vel_components.
       2) compute_vel_at_main_cells.
       It will only be the second step just as in the server */

    pthread_mutex_unlock(&data_mutex);
    
    /* Updating the anims */
    update_anims(&data);
    
  } /* end while(1) */

  free_mem_anims(&data);

  return 0;
}

/* POLL_READ_SIZE_FROM_SERVER */
void poll_read_size_from_server(int *Nx, int *Ny, 
				int *flow_done_OK, int *turb_OK,
				const int sockfd, 
				struct pollfd *fds, 
				const double timeout){

  int n;

  /* Call poll() and wait for it to complete */
  printf("Waiting on poll() to read size:\n");
  n = poll(fds, 1, timeout);

  /* Check to see if the poll call failed */
  if (n < 0){
    perror("poll() failed");
  }

  /* Check to see if the time out expired */
  if (n == 0){
    printf("poll() timed out. End program.\n");
  }
  
  read_size_from_server(Nx, Ny, flow_done_OK, turb_OK, sockfd);

  return;
}

/* READ_SIZE_FROM_SERVER */
void read_size_from_server(int *Nx, int *Ny, 
			   int *flow_done_OK, int *turb_OK,
			   const int sockfd){
  
  char buffer[28];
  char *tokenPtr;
  
  int n;

  const int buff_size = 28;

  /* Reading from socket the Nx, Ny
     n: number of Bytes read */
  n = read(sockfd, buffer, buff_size-1);
  if (n <= 0){
    error("ERROR reading from socket");
  }

#if DEBUG_ME
  printf("buffer (%d char): %s\n", n, buffer);
#endif
  
  tokenPtr = strtok(buffer, " ");
  sscanf(tokenPtr, "%d", Nx);
  
  tokenPtr = strtok(NULL, " ");
  sscanf(tokenPtr, "%d", Ny);

  tokenPtr = strtok(NULL, " ");
  sscanf(tokenPtr, "%d", flow_done_OK);

  tokenPtr = strtok(NULL, " ");
  sscanf(tokenPtr, "%d", turb_OK);
  
  return;
}

/* POLL_READ_ARRAY_FROM_SERVER */
void poll_read_array_from_server(double *array, const int Nx, const int Ny, 
				 const char *msg, const int sockfd, 
				 struct pollfd *fds, const double timeout){

  int n;

  /* Call poll() and wait for it to complete */
  printf("Waiting on poll() to read %s:\n", msg);
  n = poll(fds, 1, timeout);

  /* Check to see if the poll call failed */
  if (n < 0){
    perror("poll() failed");
  }

  /* Check to see if the time out expired */
  if (n == 0){
    printf("poll() timed out. End program.\n");
  }
  
  read_array_from_server(array, Nx, Ny, sockfd);
  
  return;
}

/* READ_ARRAY_FROM_SERVER */
void read_array_from_server(double *array, const int Nx, const int Ny, 
			    const int sockfd){
  
  char *buffer = NULL;
  char *buffer_aux = NULL;
  char *tokenPtr;

  int i, j, index_;
  int buff_size;
  int n, n_read;
  int total_char_read = 0;
  int total_double_read = 0;
  int que_char;

  double x;
  
  buff_size = 11*Nx+1;
  /* Allocating the memory */
  buffer = (char *) calloc(sizeof(char), buff_size);
  buffer_aux = (char *) calloc(sizeof(char), buff_size);
  
  if (buffer == NULL){
    error("ERROR could not allocate buffer");
  }
      
  for(j=0; j<Ny; j++){

    bzero(buffer, buff_size);
    /* Reading from socket
       n: number of Bytes read */
    n = read(sockfd, buffer, buff_size - 1);
      
    if (n <= 0){
      error("ERROR reading from socket");
    }

    if(n < buff_size-1){
      // se leyeron menos chars de los debidos
      n_read = n;
      que_char = buff_size - 1 - n_read;
      printf("%sRead %d char, Expected %d chars, Missing %d char%s\n", 
	     COLOR_Y, n_read, buff_size-1, que_char, COLOR_END);

#if DEBUG_ME
      if(n_read >= 4){
	printf("Last 4 char from buffer: %c%c%c%c\n", 
	       buffer[n_read-4], buffer[n_read-3], 
	       buffer[n_read-2], buffer[n_read-1]);
      }
#endif
      /*
	We cannot leave yet until we get the full line 
	for tokens to work properly.
	Call poll to wait for file descriptor here 
	would be the correct approach assuming 
	the socket would be ready
      */
      
      n = read(sockfd, buffer_aux, que_char);
      if (n != que_char){
	error("ERROR reading que_char from socket for the buffer_aux");
      }
      
#if DEBUG_ME
      printf("Now the first 4 char from buffer_aux: %c%c%c%c\n",
	     buffer_aux[0], buffer_aux[1], buffer_aux[2], buffer_aux[3]);
#endif
      
      // fix the line here
      for(i = 0; i<que_char; i++){
	buffer[n_read + i] = buffer_aux[i];
      }
      // Now the char read are
      n = n_read + n;
      printf("%sFixed to %d char read per line%s\n", 
	     COLOR_Y, n, COLOR_END);
    }

    // Ending string for token
    buffer[buff_size-1] = '\0';

    total_char_read = total_char_read + n;

#if DEBUG_ME
    printf("buffer (%d char): %s\n", n, buffer);
#endif

    tokenPtr = strtok(buffer, " ");

    i = 0;

    while (tokenPtr != NULL){
      index_ = j*Nx+i;
      
      n = sscanf(tokenPtr, "%10lf ", &x);
      array[index_] = x;

      total_double_read = total_double_read + n;

#if DEBUG_ME
      printf("The token is %10s, i = %2d, x = %10f, array = %10f\n",
	     tokenPtr, i, x, array[index_]);
#endif

      tokenPtr = strtok(NULL, " ");
      i++;
    }
  }

  printf("Total read: %d char, %d doubles\n", 
	 total_char_read, total_double_read);

  if(total_double_read != Nx*Ny){
    error("Client read not completed or faulty");
  }
    
  free(buffer);
  free(buffer_aux);
  
  return ;
}

/* INITIALIZE_FLAG */
void initialize_flag (void){
  /* Initialize the mutex and condition variable */
  pthread_mutex_init (&gr_mutex, NULL);
  pthread_cond_init (&gr_flag_cv, NULL);
  /* Initialize the flag value. */
  gr_flag = 0;
  
  return;
}

/* SET_THREAD_FLAG */
void set_thread_flag (int flag_value){
  /* Lock the mutex before accessing the flag value. */
  pthread_mutex_lock(&gr_mutex);
  /* Set the flag value, and then signal in case thread_function is
     blocked, waiting for the flag to become set. However,
     thread_function can’t actually check the flag until the mutex is
     unlocked. */

  gr_flag = flag_value;

  pthread_cond_signal(&gr_flag_cv);

  /* Unlock the mutex. */
  pthread_mutex_unlock(&gr_mutex);

  return;
}

/* ANIM */
void *anim(void *data_void){
	
  Data_mgl *data = (Data_mgl *) data_void;

  /* anim_data should not be called within anim_draw */
  data->gr = mgl_create_graph_fltk(anim_draw, "Results", data, anim_data);
  /* Set and send variable gr_flag and its signal to main */
  set_thread_flag(1);

  /* Starting event handling */
  mgl_fltk_run();
  
  return NULL;
}

/* ANIM_DATA */
void anim_data(void *data_void){
	
  anim_flow_data(data_void);
  anim_phi_data(data_void);

  return;
}

/* ANIM_DRAW */
int anim_draw(HMGL gr, void *data_void){
  
  const char *msg = NULL;
    
  Data_mgl *data = (Data_mgl *) data_void;

  HMDT U_ih = data->U_ih;
  HMDT V_ih = data->V_ih;
  HMDT p_ih = data->p_ih;

  HMDT k_ih = data->k_ih;
  HMDT eps_ih = data->eps_ih;
  HMDT mu_ih = data->mu_ih;
  
  HMDT phi_ih = data->phi_ih;
  HMDT gL_ih = data->gL_ih;
  
  mreal min, max;

  int ret_value = 0;
  
  const int flow_done_OK = data->flow_done_OK;
  const int turb_OK = data->turb_OK;
  
  min = MIN( mgl_data_min(U_ih), mgl_data_min(V_ih) );
  max = MAX( mgl_data_max(U_ih), mgl_data_max(V_ih) );
  
  // mgl_clf_chr(gr, 'k');
  // mgl_set_transp_type(gr, 2);
  mgl_set_font_size(gr, 4);

  if(!flow_done_OK){
      
    if(turb_OK == 1){
      msg = "Abe, Kondoh and Nagano model";
    }
    else if(turb_OK == 2){
      msg = "Cho and Goldstein model";
    }
      
    if(turb_OK){
        
      mgl_puts(gr, 0.5, 0.05, 0, msg, "A", -1);
        
      mgl_subplot(gr, 5, 1, 0, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', min, max);
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_vect_2d(gr, U_ih, V_ih, "BbcyrR", "meshnum 20");
      mgl_puts(gr, 0.5, 1.02, 0, "U, V (m/s)", "", -2);
    
      mgl_subplot(gr, 5, 1, 1, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(p_ih), mgl_data_max(p_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, p_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "p (Pa)", "", -2);
      
      mgl_subplot(gr, 5, 1, 2, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(k_ih), mgl_data_max(k_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, k_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "k (m^2/s^2)", "", -2);

      mgl_subplot(gr, 5, 1, 3, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(eps_ih), mgl_data_max(eps_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, eps_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "\\varepsilon (m^2/s^3)", "", -2);

      mgl_subplot(gr, 5, 1, 4, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(mu_ih), mgl_data_max(mu_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, mu_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "\\mu _t (kg/ms)", "", -2);
    
    }
    else{
      mgl_subplot(gr, 2, 1, 0, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', min, max);
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_vect_2d(gr, U_ih, V_ih, "BbcyrR", "meshnum 20");
      mgl_puts(gr, 0.5, 1.02, 0, "U, V (m/s)", "", -2);
      
      mgl_subplot(gr, 2, 1, 1, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(p_ih), mgl_data_max(p_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, p_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "p (Pa)", "", -2);
    }
  }
  else{
      
    min = mgl_data_min(phi_ih);
    max = mgl_data_max(phi_ih);
  
    mgl_subplot(gr, 2, 1, 0, "LAUR");
    mgl_set_range_val(gr, 'x', 0, 1);
    mgl_set_range_val(gr, 'y', 0, 1);
    mgl_set_range_val(gr, 'c', min, max);
    mgl_axis(gr, "xyz", "", "");
    mgl_box(gr);
    mgl_label(gr, 'x', "x/L_x", 0, "");
    mgl_label(gr, 'y', "y/L_y", 0, "");
    mgl_colorbar(gr, ">");
    mgl_cont(gr, phi_ih, "BbcyrR_", "value 20");
    mgl_puts(gr, 0.5, 1.02, 0, "\\phi (K)", "", -2);
    
    mgl_subplot(gr, 2, 1, 1, "LAUR");
    mgl_set_range_val(gr, 'x', 0, 1);
    mgl_set_range_val(gr, 'y', 0, 1);
    mgl_set_range_val(gr, 'c', mgl_data_min(gL_ih), mgl_data_max(gL_ih));
    mgl_axis(gr, "xyz", "", "");
    mgl_box(gr);
    mgl_label(gr, 'x', "x/L_x", 0, "");
    mgl_label(gr, 'y', "y/L_y", 0, "");
    mgl_colorbar(gr, ">");
    mgl_cont(gr, gL_ih, "BbcyrR_", "value 4");
    mgl_puts(gr, 0.5, 1.02, 0, "gL", "", -2);
    
  }

  return ret_value;
}

/* ANIM_FLOW_DATA */
void anim_flow_data(void *data_void){

  Data_mgl *data = (Data_mgl *) data_void;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  double *U = data->U;
  double *V = data->V;

  const double *p = data->p;

  const double *k_turb = data->k_turb;
  const double *eps_turb = data->eps_turb;
  const double *mu_turb = data->mu_turb;
  
  HMDT U_ih = data->U_ih;
  HMDT V_ih = data->V_ih;
  HMDT p_ih = data->p_ih;

  HMDT k_ih = data->k_ih;
  HMDT eps_ih = data->eps_ih;
  HMDT mu_ih = data->mu_ih;
  
  mgl_data_set_double(U_ih, U, Nx, Ny, 1);
  mgl_data_set_double(V_ih, V, Nx, Ny, 1);
  mgl_data_set_double(p_ih, p, Nx, Ny, 1);

  mgl_data_set_double(k_ih, k_turb, Nx, Ny, 1);
  mgl_data_set_double(eps_ih, eps_turb, Nx, Ny, 1);
  mgl_data_set_double(mu_ih, mu_turb, Nx, Ny, 1);

  return;
}
/* ANIM_PHI_DATA */
void anim_phi_data(void *data_void){
    
  Data_mgl *data = (Data_mgl *) data_void;
     
  double *phi = data->phi;
  double *gL = data->gL;

  HMDT phi_ih = data->phi_ih;
  HMDT gL_ih = data->gL_ih;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  mgl_data_set_double(phi_ih, phi, Nx, Ny, 1);
  mgl_data_set_double(gL_ih, gL, Nx, Ny, 1);

  return;
}

/* SET_MEM_ANIMS */
void set_mem_anims(Data_mgl *data){

  int Nx = data->Nx;
  int Ny = data->Ny;
  
  data->U = (double *) realloc((void *) data->U, Nx*Ny*sizeof(double));
  data->V = (double *) realloc((void *) data->V, Nx*Ny*sizeof(double));
  data->p = (double *) realloc((void *) data->p, Nx*Ny*sizeof(double));

  data->k_turb = (double *) realloc((void *) data->k_turb, 
				    Nx*Ny*sizeof(double));
  data->eps_turb = (double *) realloc((void *) data->eps_turb, 
				      Nx*Ny*sizeof(double));
  data->mu_turb = (double *) realloc((void *) data->mu_turb, 
				     Nx*Ny*sizeof(double));

  data->phi = (double *) realloc((void *) data->phi, Nx*Ny*sizeof(double));
  data->gL = (double *) realloc((void *) data->gL, Nx*Ny*sizeof(double));
  
  /* Creating structure and memory for anim setup */
  mgl_data_create(data->x_ih, Nx, 1, 1);
  mgl_data_create(data->y_ih, Ny, 1, 1);

  mgl_data_create(data->U_ih, Nx, Ny, 1);
  mgl_data_create(data->V_ih, Nx, Ny, 1);
  mgl_data_create(data->p_ih, Nx, Ny, 1);

  mgl_data_create(data->k_ih, Nx, Ny, 1);
  mgl_data_create(data->eps_ih, Nx, Ny, 1);
  mgl_data_create(data->mu_ih, Nx, Ny, 1);

  mgl_data_create(data->phi_ih, Nx, Ny, 1);
  mgl_data_create(data->gL_ih, Nx, Ny, 1);
  
  /* Making positions between 0 and 1 */
  mgl_data_fill(data->x_ih, 0, 1, 'x');
  mgl_data_fill(data->y_ih, 0, 1, 'x');

  return;
}

/* FREE_MEM_ANIMS */
void free_mem_anims(Data_mgl *data){

  mgl_delete_graph(data->gr);

  mgl_delete_data(data->U_ih);
  mgl_delete_data(data->V_ih);
  mgl_delete_data(data->p_ih);

  mgl_delete_data(data->k_ih);
  mgl_delete_data(data->eps_ih);
  mgl_delete_data(data->mu_ih);

  mgl_delete_data(data->phi_ih);
  mgl_delete_data(data->gL_ih);

  free(data->U);
  free(data->V);
  free(data->p);

  free(data->k_turb);
  free(data->eps_turb);
  free(data->mu_turb);

  free(data->phi);
  free(data->gL);
  
  return;
}

/* UPDATE_ANIMS */
void update_anims(Data_mgl *data){

  if(data->gr != NULL){
    /* This should be the correct */
    /*    mgl_wnd_update(data->gr); */
    
    /* Less efficient but updates correctly */
    mgl_wnd_reload(data->gr);
    
  }
  
  return;
}

/* GET_MAX */
double get_max(const double *A_, const int Nx, const int Ny){
  int i, j, index_, found_value_OK = 0;
  double max;

  for(i=0; i<Nx; i++){
    for(j=0; j<Ny; j++){
      index_ = j*Nx+i;
      
      if(!(isnan(A_[index_])) && (!found_value_OK)){
	max = A_[index_];
	found_value_OK = 1;
      }

      if(!(isnan(A_[index_]))){      
	max = MAX(max, A_[index_]);
      }
      
    }
  }

  return max;
}

/* GET_MIN */
double get_min(const double *A_, const int Nx, const int Ny){
  int i, j, index_, found_value_OK = 0;
  double min;

  for(i=0; i<Nx; i++){
    for(j=0; j<Ny; j++){
    
      index_ = j*Nx+i;
      
      if(!(isnan(A_[index_])) && (!found_value_OK)){
	min = A_[index_];
	found_value_OK = 1;
      }
      
      if(!(isnan(A_[index_]))){      
	min = MIN(min, A_[index_]);
      }
    }
  }

  return min;
}

/* PRINT ARRAY */
void print_array(const double *A_, const int Nx, const int Ny, const char msg[]){
  int i, j, index_;

  printf("%s = \n", msg);
  for(i=0; i<Nx; i++){
    for(j=0; j<Ny; j++){
      
      index_ = j*Nx + i;
      printf("%+14.3e ", A_[index_]);

    }
    printf("\n");
  }

  return;
}

/* ERROR */
void error(const char *msg){
  perror(msg);
  exit(0);
}

