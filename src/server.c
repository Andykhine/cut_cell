#include "include/some_defs.h"
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 

#define MAX_DOUBLES_PER_ARRAY 2000
    
#define TRUE 1
#define FALSE 0
// Signaling server thread launch to main
extern sig_atomic_t sgr_flag;
extern pthread_cond_t sgr_flag_cv;
extern pthread_mutex_t sgr_mutex;

extern sig_atomic_t update_clients_OK;
extern pthread_cond_t update_cv;
extern pthread_mutex_t update_mutex;

extern pthread_mutex_t sdata_mutex;

/* SET_UPDATE_CLIENTS_FLAG */
void set_update_clients_flag (int flag_value){
  /* Lock the mutex before accessing the flag value. */
  pthread_mutex_lock(&update_mutex);
  /* This flag is checked by the server thread */
  update_clients_OK = flag_value;
  /* Only is needed this signaling process */
  //  pthread_cond_signal(&sgr_flag_cv);
  /* Unlock the mutex. */
  pthread_mutex_unlock(&update_mutex);
  return;
}

/* SET_STHREAD_FLAG */
void set_sthread_flag (int sflag_value){
  /* Lock the mutex before accessing the flag value. */
  pthread_mutex_lock(&sgr_mutex);
  /* This flag is unnecesary */
  sgr_flag = sflag_value;
  /* Only is needed this signaling process */
  pthread_cond_signal(&sgr_flag_cv);
  /* Unlock the mutex. */
  pthread_mutex_unlock(&sgr_mutex);
  return;
}

/* SERVER_THREAD */
void * server_thr_function(void *data_null){
  
  Data_Mem *data = (Data_Mem *) data_null;
  Server_Data *sdata = (Server_Data *) (&data->sdata);
  Wins *wins = &(data->wins);
  
  char buffer[1025];  //data buffer of 1K 
  char msg[SIZE];
  
  int opt = TRUE;  
  int master_socket, addrlen, new_socket;
  int activity, i, sd;
  int max_sd;

  const int max_clients = sdata->max_clients;

  const int flow_done_OK = data->flow_done_OK;
  const int turb_OK = data->turb_model;

  int *client_socket = sdata->client_socket;
  
  struct sockaddr_in address;  
        
  //set of socket descriptors 
  fd_set readfds;

  // Copying to sdata struct
  sdata->readfds = &readfds;

  //initialise all client_socket[] to 0 so not checked 
  for (i = 0; i < max_clients; i++){  
    client_socket[i] = 0;  
  }  
  
  //create a master socket 
  if((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0){
    perror("socket failed");
    exit(EXIT_FAILURE);
  }
  
  //set master socket to allow multiple connections , 
  //this is just a good habit, it will work without this 
  if(setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, 
		sizeof(opt)) < 0){  
    perror("setsockopt");  
    exit(EXIT_FAILURE);  
  }  
    
  //type of socket created 
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(sdata->port);
        
  //bind the socket to localhost port sdata->port 
  if (bind(master_socket, (struct sockaddr *)&address,
	   sizeof(address)) < 0){
    perror("bind failed");
    exit(EXIT_FAILURE);
  }
  
  //try to specify maximum of 2 pending connections for the master socket
  if (listen(master_socket, 2) < 0){  
    perror("listen");  
    exit(EXIT_FAILURE);  
  }  
  
  //accept the incoming connection 
  addrlen = sizeof(address);  
  sprintf(msg, "Server %s:%d launched", 
	  sdata->server_name, sdata->port);
  serv_msg(wins, msg);
  
  /* Set and send variable sgr_flag and its signal to main */
  set_sthread_flag(1);
        
  while(TRUE){  
    //clear the socket set 
    FD_ZERO(&readfds);  
    
    //add master socket to set 
    FD_SET(master_socket, &readfds);  
    max_sd = master_socket;  
    
    //add child sockets to set 
    for (i = 0; i < max_clients; i++){
      //socket descriptor 
      sd = client_socket[i];
      
      //if valid socket descriptor then add to read list 
      if(sd > 0){  
	FD_SET(sd, &readfds);
      }
      
      //highest file descriptor number, need it for the select function 
      if(sd > max_sd){
	max_sd = sd;
      }
    } // end for (i = 0; i < max_clients; i++){
    
      //wait for an activity on one of the sockets , timeout is NULL , 
      //so wait indefinitely 
    activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
    
    if ((activity < 0) && (errno!=EINTR)){
      serv_msg(wins, "select error");
    }
            
    //If something happened on the master socket, 
    //then its an incoming connection 
    if (FD_ISSET(master_socket, &readfds)){
      if ((new_socket = accept(master_socket, 
			       (struct sockaddr *)&address, 
			       (socklen_t*)&addrlen)) < 0){
	perror("accept");
	exit(EXIT_FAILURE);
      }
	
      //inform user of socket number - used in send and receive commands 
      sprintf(msg, "New connection, socket fd %d, ip %s, port %d", 
	      new_socket, inet_ntoa(address.sin_addr), 
	      ntohs(address.sin_port));
      serv_msg(wins, msg);
      
      //send data size
      send_size_to_client(new_socket, 
			  sdata->size_x, sdata->size_y,
			  flow_done_OK, turb_OK);
	
      //add new socket to array of sockets
      for (i = 0; i < max_clients; i++){  
	//if position is empty 
	if(client_socket[i] == 0){
	  client_socket[i] = new_socket;
	  break;
	} // end if
      } // end for
    } // end if (FD_ISSET(master_socket, &readfds))
            
    // Global variable to update client list (unnecessary?)
    if(update_clients_OK){
      
      //else its some IO operation on some other socket
      for (i = 0; i < max_clients; i++){
	sd = client_socket[i];
	  
	if (FD_ISSET(sd, &readfds)){
	  //Check if it was for closing, and also read the 
	  //incoming message
	  if ((read(sd, buffer, 1024)) == 0){
	    //Somebody disconnected, get his details and print 
	    getpeername(sd, (struct sockaddr*)&address, 
			(socklen_t*)&addrlen);

	    sprintf(msg, "Client disconnected, ip %s, port %d",
		    inet_ntoa(address.sin_addr), 
		    ntohs(address.sin_port));
	    serv_msg(wins, msg);
	    
	    //Close the socket and mark as 0 in list for reuse 
	    close(sd);  
	    client_socket[i] = 0;
	  }

	  /*	      
	  //Echo back the message that came in 
	  else{  
	  //set the string terminating NULL byte on the end 
	  //of the data read 
	  buffer[valread] = '\0';  
	  send(sd, buffer, strlen(buffer), 0);
	  }
	  */

	} // end if (FD_ISSET(sd, &readfds)){
      } // end for (i = 0; i < max_clients; i++){
    } // end if(update_clients_OK)
  } // end while(TRUE){
  
  return NULL;
} 

/* SEND_DATA_TO_CLIENT */
void send_data_to_client(int sock, Data_Mem *data){

  /* Simulating some data */
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int size_x = data->sdata.size_x;
  const int size_y = data->sdata.size_y;
  const int bans = data->sdata.bans;

  const int flow_done_OK = data->flow_done_OK;
  const int turb_OK = data->turb_model;
  
  const double *U = data->sdata.U;
  const double *V = data->sdata.V;
  const double *p = data->p;

  const double *k_turb = data->k_turb;
  const double *eps_turb = data->eps_turb;
  const double *mu_turb = data->mu_turb;

  const double *phi = data->phi;
  const double *gL = data->gL;

  send_size_to_client(sock, 
		      size_x, size_y,
		      flow_done_OK, turb_OK);

  if(!flow_done_OK){

    send_array_to_client(sock, U, Nx, Ny, size_x, bans);
    send_array_to_client(sock, V, Nx, Ny, size_x, bans);
    send_array_to_client(sock, p, Nx, Ny, size_x, bans);

    if(turb_OK){

      send_array_to_client(sock, k_turb, Nx, Ny, size_x, bans);
      send_array_to_client(sock, eps_turb, Nx, Ny, size_x, bans);
      send_array_to_client(sock, mu_turb, Nx, Ny, size_x, bans);

    }
  }
  else{

    send_array_to_client(sock, phi, Nx, Ny, size_x, bans);
    send_array_to_client(sock, gL, Nx, Ny, size_x, bans);

  }
  
  return;
}

/* SEND_ARRAY_TO_CLIENT */
void send_array_to_client(int sock, const double *array, const int Nx, const int Ny, 
			  const int size_x, const int bans){

  char buffer_short[12];
  char *buffer = NULL;

  int I, J, index_;
  int n;
  int total_char = 0;
  int total_doubles = 0;

  const int buff_size = 11*size_x+1;
  const int stride = pos_int_pow(2, bans);

  /* Initializing buffer */
  buffer = (char *) malloc(buff_size);
  
  for(J=0; J<Ny; J = J + stride){
    
    bzero(buffer, buff_size);

    for (I=0; I<Nx; I = I + stride){

      index_ = J*Nx+I;
      // snprintf() write at most size bytes (including the terminating null byte ('\0')) to str.
      n = snprintf(buffer_short, 12, "%+10.3e ", array[index_]);
      if (n < 11){
	perror("ERROR buffer_short");
	exit(EXIT_FAILURE);
      }

      buffer = strncat(buffer, buffer_short, 11);

      total_doubles++;
    }

    /* Writing to socket
       n: number of Bytes written */
    n = write(sock, buffer, strlen(buffer));
    if (n < 0){
      perror("ERROR writing to socket");
      exit(EXIT_FAILURE);
    }
    
    total_char = total_char + n;
  }

  free(buffer);

  return;
}

/* SEND_SIZE_TO_CLIENT */
void send_size_to_client(int sock, const int Nx, const int Ny, 
			 const int flow_done_OK, const int turb_OK){

  char buffer[28];

  int n;

  const int buff_size = 28;

  bzero(buffer, buff_size);
  /* Here we write to buffer at most strlen(buffer)-1 char
     n: number of char printed EXCLUDING null byte */
  n = snprintf(buffer, strlen(buffer)-1, "%10d %10d %2d %2d", 
	       Nx, Ny, flow_done_OK, turb_OK);

  if (n <= 0){
    perror("Nothing printed on buffer");
    exit(EXIT_FAILURE);
  }

  /* Writing to socket
     n: number of Bytes written */
  n = write(sock, buffer, strlen(buffer));
  if (n < 0){
    perror("ERROR writing to socket");
    exit(EXIT_FAILURE);
  }
  
  return;
}

/* UPDATE_SERVER */
void update_server(Data_Mem *data){

  Server_Data *sdata = &(data->sdata);

  int i, sd;
  int update_vel_OK = 1;

  const int max_clients = sdata->max_clients;

  /* This function runs in the main thread.
     It needs to coordinate with the server thread */

  /* Stop server activity on clients (unnecessary?)
     because we are writing to their sockets */
  
  set_update_clients_flag(0);
  pthread_mutex_lock(&sdata_mutex);

  // Here is safe to read sdata

  for(i=0; i<max_clients; i++){
    // look for clients
    sd = sdata->client_socket[i];
    if(sd > 0){

      if(update_vel_OK){
	// Set U and V for server
	center_vel_for_server(data);
	/* Since it is a extensive operation we only want to 
	   perform it just the necessary */
	update_vel_OK = 0;
      }
      //      printf("socket %d, i = %d\n", sd, i);
      send_data_to_client(sd, data);
      // Here to check if client received 
      // the entirety of the data its paramount
      // recieve ok, next client
      // recieve not ok, send again (clear socket)
    }
  }

  pthread_mutex_unlock(&sdata_mutex);
  // Resume (server and main)
  set_update_clients_flag(1);
  
  return;

}

/* DATA_RESIZE_FOR_SERVER */
void data_resize_for_server(Data_Mem *data){

  Server_Data *sdata = &(data->sdata);

  char msg[SIZE];
  
  int div, rem;
  int counter = 0;
  int Nx = data->Nx;
  int Ny = data->Ny;

  const int size_max = MAX_DOUBLES_PER_ARRAY;
  
  while(Nx*Ny >= size_max){

    div = Nx / 2;
    rem = Nx % 2;

    if(rem != 0){
      div = div + 1;
    }

    Nx = div;

    div = Ny / 2;
    rem = Ny % 2;

    if(rem != 0){
      div = div + 1;
    }

    Ny = div;

    counter++;
  }

  sdata->size_x = Nx;
  sdata->size_y = Ny;
  sdata->size_max = size_max;
  sdata->bans = counter;

  if(counter > 0){
    sprintf(msg, 
	    "Data for server set at (Nx, Ny) = (%d, %d), %d reductions", 
	    Nx, Ny, counter);
  }

  return;
}

/* POS_INT_POW */
int pos_int_pow(int base, int exp){
  int result = 1;
  while (exp){
    result = base*result;
    exp = exp-1;
  }
  return result;
}
