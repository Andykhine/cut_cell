#include "include/some_defs.h"

#ifdef GRAPHS_OK

#include <mgl2/config.h>
#undef MGL_HAVE_C99_COMPLEX
#define MGL_HAVE_C99_COMPLEX 0

#include <mgl2/mgl_cf.h>
#include <mgl2/fltk.h>
#include <mgl2/qt.h>

#define TEXT_SIZE 3

extern sig_atomic_t gr_flag;
extern pthread_cond_t gr_flag_cv;
extern pthread_mutex_t gr_mutex;

typedef struct{
  
  int Nx;
  int Ny;
  int Nz;
  
  int flow_steady_state_OK;
  int steady_state_OK;
  int solve_phase_change_OK;
  int turb_OK;

  int solve_3D_OK;

  int *flow_done_OK;
  
  const int *P_solve_phase_OK;
  const int *domain_matrix;

  /* Buffers to copy (graphs) or point (anim) to data */
  double *x;
  double *y;
  double *z;

  double *U;
  double *V;
  double *W;
  double *p;
  double *k_turb;
  double *eps_turb;
  double *mu_turb;

  double *phi;
  double *gL;  
  
  const double *tflow;
  const double *tphi;

  Wins *wins;

  /* Data in-house in mathgl format ready to be draw */
  HMDT x_ih;
  HMDT y_ih;
  HMDT z_ih;
  
  HMDT U_ih;
  HMDT V_ih;
  HMDT W_ih;
  HMDT p_ih;
  HMDT k_ih;
  HMDT eps_ih;
  HMDT mu_ih;

  HMDT phi_ih;
  HMDT gL_ih;

  HMDT vel_mag_ih;

  mreal mag_max = 0;

} Data_mgl;

/* FUNCTION PROTOTYPES */
void data_flow(void *data_mgl_void);
void data_phi(void *data_mgl_void);

int draw_flow(HMGL gr, void *data_mgl_void);
int draw_phi(HMGL gr, void *data_mgl_void);

void graphs_data(void *data_mgl_void);
int graphs_phi_draw(HMGL gr, void *data_mgl_void);
int graphs_flow_draw(HMGL gr, void *data_mgl_void);

void anims_data(void *data_void);
int anims_draw(HMGL gr, void *data_void);

/* DATA_FLOW */
void data_flow(void *data_mgl_void){
  /*****************************************************************************/
  /*
    FUNCTION: data_flow
    Sets data for the flow variables.

    RETURNS: void.
    
    MODIFIED: 27-09-2019
  */
  /*****************************************************************************/
  
  Data_mgl *data_mgl = (Data_mgl *) data_mgl_void;

  const int Nx = data_mgl->Nx;
  const int Ny = data_mgl->Ny;
  const int Nz = data_mgl->Nz;

  const int solve_3D_OK = data_mgl->solve_3D_OK;

  const int *domain_matrix = data_mgl->domain_matrix;

  double *U = data_mgl->U;
  double *V = data_mgl->V;
  double *W = data_mgl->W;
  
  const double *p = data_mgl->p;
  const double *k_turb = data_mgl->k_turb;
  const double *eps_turb = data_mgl->eps_turb;
  const double *mu_turb = data_mgl->mu_turb;

  HMDT U_ih = data_mgl->U_ih;
  HMDT V_ih = data_mgl->V_ih;
  HMDT W_ih = data_mgl->W_ih;
  HMDT p_ih = data_mgl->p_ih;

  HMDT k_ih = data_mgl->k_ih;
  HMDT eps_ih = data_mgl->eps_ih;
  HMDT mu_ih = data_mgl->mu_ih;

  HMDT vel_mag_ih = data_mgl->vel_mag_ih;

  mreal mag_max = 0;

  /* This should set NaNs within solids */
  set_nans_domain(U, domain_matrix, 0, Nx, Ny, Nz);
  set_nans_domain(V, domain_matrix, 0, Nx, Ny, Nz);
  set_nans_domain(W, domain_matrix, 0, Nx, Ny, Nz);
  
  /* Setting the data onto the HMDT arrays */
  mgl_data_set_double(U_ih, U, Nx, Ny, Nz);
  mgl_data_set_double(V_ih, V, Nx, Ny, Nz);
  mgl_data_set_double(W_ih, W, Nx, Ny, Nz);
  mgl_data_set_double(p_ih, p, Nx, Ny, Nz);
  
  mgl_data_set_double(k_ih, k_turb, Nx, Ny, Nz);
  mgl_data_set_double(eps_ih, eps_turb, Nx, Ny, Nz);
  mgl_data_set_double(mu_ih, mu_turb, Nx, Ny, Nz);

  /* This is for computing velocity magnitude */
  mgl_data_set_double(vel_mag_ih, U, Nx, Ny, Nz);
  
  /* Computing velocity magnitude */
  /* vel_mag_ih is set to U_ih in data_flow function */
  if(solve_3D_OK){
    mgl_data_modify_vw(vel_mag_ih, "sqrt(u*u + v*v + w*w)", V_ih, W_ih);
  }
  else{
    mgl_data_modify_vw(vel_mag_ih, "sqrt(u*u + v*v)", V_ih, NULL);
  }
  
  /* Get max velocity magnitude value and pass it to the struct for its use in draw_flow */
  mag_max = mgl_data_max(vel_mag_ih);
  data_mgl->mag_max = mag_max;
  
  /* Normalize components */
  mgl_data_div_num(U_ih, mag_max);
  mgl_data_div_num(V_ih, mag_max);
  if(solve_3D_OK){
    mgl_data_div_num(W_ih, mag_max);
  }
  
  return;

}

/* DATA_PHI */
void data_phi(void *data_mgl_void){
  /*****************************************************************************/
  /*
    FUNCTION: data_phi
    Sets data for the phi and gL variables.
    
    RETURNS: void.
    
    MODIFIED: 18-08-2019
  */
  /*****************************************************************************/
    
  Data_mgl *data_mgl = (Data_mgl *) data_mgl_void;
  
  const int Nx = data_mgl->Nx;
  const int Ny = data_mgl->Ny;
  const int Nz = data_mgl->Nz;
    
  const double *phi = data_mgl->phi;
  const double *gL = data_mgl->gL;

  HMDT phi_ih = data_mgl->phi_ih;
  HMDT gL_ih = data_mgl->gL_ih;

  mgl_data_set_double(phi_ih, phi, Nx, Ny, Nz);
  mgl_data_set_double(gL_ih, gL, Nx, Ny, Nz);
    
  return;
}

/* DRAW_FLOW */
int draw_flow(HMGL gr, void *data_mgl_void){
  /*****************************************************************************/
  /*
    FUNCTION: draw_flow
    Draws the flow data (U,V,W,p,mu_t,k,e) for animation, peek and graphic mode.
    
    RETURNS: 0.
    
    MODIFIED: 27-09-2019
  */
  /*****************************************************************************/

  const char *msg = NULL;
  char msg2[SIZE];
  
  Data_mgl *data_mgl = (Data_mgl *) data_mgl_void;

  const int flow_steady_state_OK = data_mgl->flow_steady_state_OK;
    
  int ret_value = 0;

  const int solve_3D_OK = data_mgl->solve_3D_OK;
  
  const int turb_OK = data_mgl->turb_OK;

  HMDT U_ih = data_mgl->U_ih;
  HMDT V_ih = data_mgl->V_ih;
  HMDT W_ih = data_mgl->W_ih;
  HMDT p_ih = data_mgl->p_ih;

  HMDT k_ih = data_mgl->k_ih;
  HMDT eps_ih = data_mgl->eps_ih;
  HMDT mu_ih = data_mgl->mu_ih;

  HMDT vel_mag_ih = data_mgl->vel_mag_ih;
  mreal mag_max = data_mgl->mag_max;

  const double *time = data_mgl->tflow;

  if(turb_OK == 1){
    msg = "Abe, Kondoh and Nagano model";
  }
  else if(turb_OK == 2){
    msg = "Cho and Goldstein model";
  }

  /* Dark theme
     mgl_clf_chr(gr, 'k');
     mgl_set_transp_type(gr, 2);
  */
  
  mgl_set_font_size(gr, 4);

  if(!solve_3D_OK){
    
    if(turb_OK){
      
      mgl_puts(gr, 0.5, 0.05, 0, msg, "Ak", -1);
      
      mgl_subplot(gr, 5, 1, 0, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', 0, mag_max);
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_vect_2d(gr, U_ih, V_ih, "BbcyrR", "meshnum 50");
      mgl_puts(gr, 0.5, 1.02, 0, "U, V (m/s)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 1, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(p_ih), mgl_data_max(p_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, p_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "p (Pa)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 2, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(k_ih), mgl_data_max(k_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, k_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "k (m^2/s^2)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 3, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(eps_ih), mgl_data_max(eps_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, eps_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "\\varepsilon (m^2/s^3)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 4, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(mu_ih), mgl_data_max(mu_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, mu_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "\\mu _t (kg/ms)", "k", -2);
      
    }
    else{
      mgl_subplot(gr, 2, 1, 0, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', 0, mag_max);
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_vect_2d(gr, U_ih, V_ih, "BbcyrR", "meshnum 50");
      mgl_puts(gr, 0.5, 1.02, 0, "U, V (m/s)", "k", -2);
      
      mgl_subplot(gr, 2, 1, 1, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(p_ih), mgl_data_max(p_ih));
      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, p_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "p (Pa)", "k", -2);
    }
  }

  else{
    /* Poner aqui código para gráficos en 3D para el flow */
    if(turb_OK){
      
      mgl_puts(gr, 0.5, 0.05, 0, msg, "Ak", -1);
      
      mgl_subplot(gr, 5, 1, 0, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      //      mgl_set_light(gr, 1);
      mgl_set_alpha(gr, 1);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', 0, mgl_data_max(vel_mag_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_vect_3d(gr, U_ih, V_ih, W_ih, "BbcyrR", "meshnum 50");
      mgl_puts(gr, 0.5, 1.02, 0, "U, V, W (m/s)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 1, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(p_ih), mgl_data_max(p_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, p_ih, "BbcyrR_", "10");
      mgl_puts(gr, 0.5, 1.02, 0, "p (Pa)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 2, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(k_ih), mgl_data_max(k_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, k_ih, "BbcyrR_", "5");
      mgl_puts(gr, 0.5, 1.02, 0, "k (m^2/s^2)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 3, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(eps_ih), mgl_data_max(eps_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, eps_ih, "BbcyrR_", "5");
      mgl_puts(gr, 0.5, 1.02, 0, "\\varepsilon (m^2/s^3)", "k", -2);
      
      mgl_subplot(gr, 5, 1, 4, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(mu_ih), mgl_data_max(mu_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, mu_ih, "BbcyrR_", "value 40");
      mgl_puts(gr, 0.5, 1.02, 0, "\\mu _t (kg/ms)", "k", -2);
      
    }
    else{
      mgl_subplot(gr, 2, 1, 0, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      //      mgl_set_light(gr, 1);
      mgl_set_alpha(gr, 1);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', 0, mgl_data_max(vel_mag_ih));;
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_vect_3d(gr, U_ih, V_ih, W_ih, "BbcyrR", "meshnum 50");
      mgl_puts(gr, 0.5, 1.3, 1.1, "U, V, W (m/s)", "k", -1.5);
      
      mgl_subplot(gr, 2, 1, 1, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(p_ih), mgl_data_max(p_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, p_ih, "BbcyrR_", "10");
      mgl_puts(gr, 0.5, 1.3, 1.1, "p (Pa)", "k", -1.5);
    }
  }

  if(!flow_steady_state_OK){
    sprintf(msg2, "t = %10.5g (s)", *time);
    mgl_puts(gr, 0.5, 0.95, 0, msg2, "Ak", -1);
  }
  
  return ret_value;
}

/* DRAW_PHI */
int draw_phi(HMGL gr, void *data_mgl_void){
  /*****************************************************************************/
  /*
    FUNCTION: draw_phi
    Draws the scalar data (phi, gL) for animation, peek and graphic mode.
    
    RETURNS: 0.
    
    MODIFIED: 18-08-2019
  */
  /*****************************************************************************/
  
  Data_mgl *data_mgl = (Data_mgl *) data_mgl_void;
  
  char msg[SIZE];
    
  int ret_value = 0;

  const int solve_3D_OK = data_mgl->solve_3D_OK;
   
  const int solve_phase_change_OK = data_mgl->solve_phase_change_OK;
  const int steady_state_OK = data_mgl->steady_state_OK;

  const double *time = data_mgl->tphi;

  HMDT phi_ih = data_mgl->phi_ih;
  HMDT gL_ih = data_mgl->gL_ih;

  /* Dark theme
     mgl_clf_chr(gr, 'k');
     mgl_set_transp_type(gr, 2);
  */
  
  mgl_set_font_size(gr, 4);

  if(!solve_3D_OK){
  
    if(solve_phase_change_OK){
      mgl_subplot(gr, 2, 1, 0, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(phi_ih), mgl_data_max(phi_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, phi_ih, "BbcyrR_", "value 20");
      mgl_puts(gr, 0.5, 1.02, 0, "\\phi (K)", "k", -1.5);
    
      mgl_subplot(gr, 2, 1, 1, "LAUR");
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(gL_ih), mgl_data_max(gL_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, gL_ih, "BbcyrR_", "value 4");
      mgl_puts(gr, 0.5, 1.02, 0, "gL", "k", -1.5);
    }
    else{
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(phi_ih), mgl_data_max(phi_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_colorbar(gr, ">");
      mgl_cont(gr, phi_ih, "BbcyrR_", "value 20");
      mgl_puts(gr, 0.5, 1.02, 0, "\\phi (K)", "k", -1.5);
    }
  }

  else{
    /* Aqui poner código para gráficos 3D de phi y gL */
    if(solve_phase_change_OK){
      mgl_subplot(gr, 2, 1, 0, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      //   mgl_set_light(gr, 1);
      mgl_set_alpha(gr, 1);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(phi_ih), mgl_data_max(phi_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, phi_ih, "BbcyrR_", "5");
      mgl_puts(gr, 0.5, 1.3, 1.1, "\\phi (K)", "k", -1.5);
    
      mgl_subplot(gr, 2, 1, 1, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(gL_ih), mgl_data_max(gL_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, gL_ih, "BbcyrR_", "5");
      mgl_puts(gr, 0.5, 1.3, 1.1, "gL", "k", -1.5);
    }
    else{
      mgl_subplot(gr, 2, 1, 0, "LAUR");
      mgl_rotate(gr, 40, 60, 0);
      mgl_set_light(gr, 1);
      mgl_set_alpha(gr, 1);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(phi_ih), mgl_data_max(phi_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, phi_ih, "BbcyrR_", "8");
      mgl_puts(gr, 0.5, 1.3, 1.1, "\\phi (K)", "k", -1.5);

      mgl_subplot(gr, 2, 1, 1, "LAUR");
      mgl_rotate(gr, 40, 60, 180);
      mgl_set_light(gr, 1);
      mgl_set_alpha(gr, 1);
      mgl_set_range_val(gr, 'x', 0, 1);
      mgl_set_range_val(gr, 'y', 0, 1);
      mgl_set_range_val(gr, 'z', 0, 1);
      mgl_set_range_val(gr, 'c', mgl_data_min(phi_ih), mgl_data_max(phi_ih));
      //      mgl_axis(gr, "xyz", "", "");
      mgl_box(gr);
      mgl_label(gr, 'x', "x/L_x", 0, "");
      mgl_label(gr, 'y', "y/L_y", 0, "");
      mgl_label(gr, 'z', "z/L_z", 0, "");
      mgl_colorbar(gr, ">");
      mgl_surf3(gr, phi_ih, "BbcyrR_", "8");
      mgl_puts(gr, 0.5, -0.3, -1.1, "\\phi (K)", "k", -1.5);
    }
  }
  
  if(!steady_state_OK){
    sprintf(msg, "t = %10.5g (s)", *time);
    mgl_puts(gr, 0.5, 0.95, 0, msg, "Ak", -1);
  }
  
  return ret_value;
}

/* ANIMS */
void *anims(void *data_void){
  /*****************************************************************************/
  /*
    FUNCTION: anims
    This is the entry point to this file from main when the animation 
    option is selected. Set the (pointer to) structure Data_mgl * for 
    making flow and/or phi animations by calling function anims_draw. 
    Once set all variables, including a pointer to a HMGL gr element that is 
    made available to the outside by transforming it to void *, 
    the function calls set_thread_flag to change the condition variable 
    and allow main to continue.

    RETURNS: void *, since it is a pthreads procedure.
    
    MODIFIED: 18-08-2019
  */
  /*****************************************************************************/

  HMGL gr;	
  Data_Mem *data = (Data_Mem *) data_void;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  /* Creating structure and memory for anims setup */
  Data_mgl data_mgl;

  HMDT U_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT V_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT W_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT p_ih = mgl_create_data_size(Nx, Ny, Nz);

  HMDT k_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT eps_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT mu_ih = mgl_create_data_size(Nx, Ny, Nz);

  HMDT phi_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT gL_ih = mgl_create_data_size(Nx, Ny, Nz);

  HMDT vel_mag_ih = mgl_create_data_size(Nx, Ny, Nz);
  
  /* Setting up drawing structure */
  /* Flags */
  data_mgl.flow_done_OK = &(data->flow_done_OK);
  data_mgl.turb_OK = data->turb_model;
  data_mgl.flow_steady_state_OK = data->flow_steady_state_OK;
  data_mgl.steady_state_OK = data->steady_state_OK;
  data_mgl.solve_phase_change_OK = data->solve_phase_change_OK;

  data_mgl.solve_3D_OK = data->solve_3D_OK;
  /* Pointers */
  data_mgl.U = data->U;
  data_mgl.V = data->V;
  data_mgl.W = data->W;
  data_mgl.p = data->p;
  
  data_mgl.k_turb = data->k_turb;
  data_mgl.eps_turb = data->eps_turb;
  data_mgl.mu_turb = data->mu_turb;
  
  data_mgl.phi = data->phi;
  data_mgl.gL = data->gL;
  /* HMDTs */
  data_mgl.U_ih = U_ih;
  data_mgl.V_ih = V_ih;
  data_mgl.W_ih = W_ih;
  data_mgl.p_ih = p_ih;
  data_mgl.k_ih = k_ih;
  data_mgl.eps_ih = eps_ih;
  data_mgl.mu_ih = mu_ih;
  data_mgl.phi_ih = phi_ih;
  data_mgl.gL_ih = gL_ih;
  data_mgl.vel_mag_ih = vel_mag_ih;
  /* Others */
  data_mgl.Nx = Nx;
  data_mgl.Ny = Ny;
  data_mgl.Nz = Nz;
  data_mgl.tflow = &(data->dt[0]);
  data_mgl.tphi = &(data->dt[6]);
  data_mgl.P_solve_phase_OK = data->P_solve_phase_OK;
  data_mgl.domain_matrix = data->domain_matrix;
  data_mgl.wins = &(data->wins);

  gr = mgl_create_graph_fltk(anims_draw, "Results", (void *) &data_mgl,
			     anims_data);
  /* Make gr element available to outside this file in an anonymous way */
  data->gr_void = (void *)gr;
  /* Set and send variable gr_flag and its signal to main */
  set_thread_flag(1);

  /* Starting event handling */
  mgl_fltk_run();

  /* Freeing memory used for anims */
  mgl_delete_graph(gr);

  mgl_delete_data(U_ih);
  mgl_delete_data(V_ih);
  mgl_delete_data(W_ih);
  mgl_delete_data(p_ih);

  mgl_delete_data(k_ih);
  mgl_delete_data(eps_ih);
  mgl_delete_data(mu_ih);

  mgl_delete_data(phi_ih);
  mgl_delete_data(gL_ih);
  mgl_delete_data(vel_mag_ih);
  
  return NULL;
}

/* ANIMS_DATA */
void anims_data(void *data_mgl_void){

  Data_mgl *data_mgl = (Data_mgl *) data_mgl_void;

  const int flow_done_OK = *(data_mgl->flow_done_OK);

  if(!flow_done_OK){
    data_flow(data_mgl_void);
  }
  else{
    data_phi(data_mgl_void);
  }

  return;
}

/* ANIMS_DRAW */
int anims_draw(HMGL gr, void *data_mgl_void){
  
  Data_mgl *data_mgl = (Data_mgl *) data_mgl_void;
  
  const int flow_done_OK = *(data_mgl->flow_done_OK);

  mgl_clf_chr(gr, 'k');
  mgl_set_transp_type(gr, 2);
  
  mgl_set_font_size(gr, 4);
  
  if(!flow_done_OK){
    draw_flow(gr, data_mgl_void);
  }
  else{
    draw_phi(gr, data_mgl_void);
  }

  return 0;
}

/* INITIALIZE_FLAG */
void initialize_flag (void){
  /* Initialize the mutex and condition variable */
  pthread_mutex_init (&gr_mutex, NULL);
  pthread_cond_init (&gr_flag_cv, NULL);
  /* Initialize the flag value */
  gr_flag = 0;
  
  return;
}

/* SET_THREAD_FLAG */
void set_thread_flag (int flag_value){
  /* Lock the mutex before accessing the flag value */
  pthread_mutex_lock(&gr_mutex);
  /* Set the flag value, and then signal in case thread_function is
     blocked, waiting for the flag to become set. However,
     thread_function can’t actually check the flag until the mutex is
     unlocked. */

  gr_flag = flag_value;
  pthread_cond_signal(&gr_flag_cv);
  /* Unlock the mutex. */
  pthread_mutex_unlock(&gr_mutex);
  return;
}

/* UPDATE_ANIMS */
void update_anims(void *data_void){

  Data_Mem *data = (Data_Mem *) data_void;

  if((HMGL)data->gr_void == NULL){
    printf("NULL pointer!!\n\n");
  }

  /* 2 updates */
  mgl_wnd_reload((HMGL)data->gr_void);
  /* 1 Update but unstable */
  // mgl_wnd_update((HMGL)data->gr_void);

  return;
}

/* GRAPHS */
void *graphs(void *data_void){
    
  HMGL gr = NULL;
  Data_mgl data_mgl;
  Data_Mem *data = (Data_Mem *) data_void;
  Wins *wins = &(data->wins);

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int graphs_flow_OK = data->graphs_flow_OK;
  const int graphs_phi_OK = data->graphs_phi_OK;

  const int peek_flow_OK = data->peek_flow_OK;
  const int peek_phi_OK = data->peek_phi_OK;

  const int solve_3D_OK = data->solve_3D_OK;
  
  const double *U = data->U;
  const double *V = data->V;
  const double *W = data->W;
  const double *p = data->p;
  
  const double *k_turb = data->k_turb;
  const double *eps_turb = data->eps_turb;
  const double *mu_turb = data->mu_turb;
  
  const double *phi = data->phi;
  const double *gL = data->gL;

  const double *x = data->nodes_x;
  const double *y = data->nodes_y;
  const double *z = data->nodes_z;

  HMDT x_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT y_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT z_ih = mgl_create_data_size(Nx, Ny, Nz);
  
  HMDT U_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT V_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT W_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT p_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT k_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT eps_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT mu_ih = mgl_create_data_size(Nx, Ny, Nz);

  HMDT phi_ih = mgl_create_data_size(Nx, Ny, Nz);
  HMDT gL_ih = mgl_create_data_size(Nx, Ny, Nz);

  HMDT vel_mag_ih = mgl_create_data_size(Nx, Ny, Nz);

  /* FILLING THE MGL STRUCTURE */
  data_mgl.Nx = Nx;
  data_mgl.Ny = Ny;
  data_mgl.Nz = Nz;
  
  data_mgl.flow_done_OK = &(data->flow_done_OK);
  data_mgl.turb_OK = data->turb_model;
  data_mgl.flow_steady_state_OK = data->flow_steady_state_OK;
  data_mgl.steady_state_OK = data->steady_state_OK;
  data_mgl.tflow = &(data->dt[0]);
  data_mgl.tphi = &(data->dt[6]);
  data_mgl.solve_phase_change_OK = data->solve_phase_change_OK;

  data_mgl.solve_3D_OK = solve_3D_OK;
  
  data_mgl.U_ih = U_ih;
  data_mgl.V_ih = V_ih;
  data_mgl.W_ih = W_ih;
  data_mgl.p_ih = p_ih;
  data_mgl.k_ih = k_ih;
  data_mgl.eps_ih = eps_ih;
  data_mgl.mu_ih = mu_ih;
  
  data_mgl.phi_ih = phi_ih;
  data_mgl.gL_ih = gL_ih;

  data_mgl.vel_mag_ih = vel_mag_ih;
  
  /* Referencing to data struct */  
  data_mgl.wins = wins;
    
  data_mgl.x_ih = x_ih;
  data_mgl.y_ih = y_ih;
  data_mgl.z_ih = z_ih;
  
  data_mgl.P_solve_phase_OK = data->P_solve_phase_OK;
  data_mgl.domain_matrix = data->domain_matrix;

  /* Memory allocations */
  data_mgl.x = create_array(Nx, Ny, Nz, 0);
  data_mgl.y = create_array(Nx, Ny, Nz, 0);
  data_mgl.z = create_array(Nx, Ny, Nz, 0);
  
  data_mgl.U = create_array(Nx, Ny, Nz, 0);
  data_mgl.V = create_array(Nx, Ny, Nz, 0);
  data_mgl.W = create_array(Nx, Ny, Nz, 0);
  data_mgl.p = create_array(Nx, Ny, Nz, 0);
  
  data_mgl.k_turb = create_array(Nx, Ny, Nz, 0);
  data_mgl.eps_turb = create_array(Nx, Ny, Nz, 0);
  data_mgl.mu_turb = create_array(Nx, Ny, Nz, 0);
  
  data_mgl.phi = create_array(Nx, Ny, Nz, 0);
  data_mgl.gL = create_array(Nx, Ny, Nz, 0);
  
  if(peek_flow_OK){
    center_vel_components(data);
    compute_vel_at_main_cells(data);
  }
  
  /* Copy data to buffers */
  copy_array(U, data_mgl.U, Nx, Ny, Nz);
  copy_array(V, data_mgl.V, Nx, Ny, Nz);
  copy_array(W, data_mgl.W, Nx, Ny, Nz);
  copy_array(p, data_mgl.p, Nx, Ny, Nz);
  
  copy_array(k_turb, data_mgl.k_turb, Nx, Ny, Nz);
  copy_array(eps_turb, data_mgl.eps_turb, Nx, Ny, Nz);
  copy_array(mu_turb, data_mgl.mu_turb, Nx, Ny, Nz);
  copy_array(x, data_mgl.x, Nx, Ny, Nz);
  copy_array(y, data_mgl.y, Nx, Ny, Nz);
  copy_array(z, data_mgl.z, Nx, Ny, Nz);
  
  copy_array(phi, data_mgl.phi, Nx, Ny, Nz);
  copy_array(gL, data_mgl.gL, Nx, Ny, Nz);
  
  /* GENERATE DATA */
  graphs_data((void *) &data_mgl);

  /* CALLING THE QT */
  if(graphs_flow_OK || peek_flow_OK){
    gr = mgl_create_graph_qt(graphs_flow_draw,
			     "Results for flow field",
			     (void *) &data_mgl, NULL);
    mgl_qt_run();
  }
  
  if(graphs_phi_OK || peek_phi_OK){
    gr = mgl_create_graph_qt(graphs_phi_draw, "Results for phi", 
			     (void *) &data_mgl, NULL);
    mgl_qt_run();
  }
  
  /* FREEING MEMORY */
  mgl_delete_graph(gr);

  free(data_mgl.x);
  free(data_mgl.y);
  free(data_mgl.z);

  free(data_mgl.U);
  free(data_mgl.V);
  free(data_mgl.W);
  free(data_mgl.p);
  
  free(data_mgl.k_turb);
  free(data_mgl.eps_turb);
  free(data_mgl.mu_turb);
  
  free(data_mgl.phi);
  free(data_mgl.gL);
  
  mgl_delete_data(x_ih);
  mgl_delete_data(y_ih);
  mgl_delete_data(z_ih);
  
  mgl_delete_data(U_ih);
  mgl_delete_data(V_ih);
  mgl_delete_data(W_ih);
  mgl_delete_data(p_ih);
  mgl_delete_data(k_ih);
  mgl_delete_data(eps_ih);
  mgl_delete_data(mu_ih);
  mgl_delete_data(phi_ih);
  mgl_delete_data(gL_ih);
  mgl_delete_data(vel_mag_ih);
  
  return NULL;
}

/* GRAPHS_FLOW_DRAW */
int graphs_flow_draw(HMGL gr, void *data_mgl_void){

  draw_flow(gr, data_mgl_void);

  return 0;
}

/* GRAPHS_PHI_DRAW */
int graphs_phi_draw(HMGL gr, void *data_mgl_void){

  draw_phi(gr, data_mgl_void);
  
  return 0;
}

/* GRAPHS_DATA */
void graphs_data(void *data_mgl_void){
    
  Data_mgl *data_mgl = (Data_mgl *) data_mgl_void;
  
  const int Nx = data_mgl->Nx;
  const int Ny = data_mgl->Ny;
  const int Nz = data_mgl->Nz;
  
  // const int *P_solve_phase_OK = data_mgl->P_solve_phase_OK;
  
  // double *phi = data_mgl->phi;

  double *x = data_mgl->x;
  double *y = data_mgl->y;
  double *z = data_mgl->z;
  
  /* PREPARING DATA */
  /* Here place code for NAN in non phi solvable phases */
  //  set_nans_domain(phi, P_solve_phase_OK, 1, Nx, Ny, Nz);
  /* Set nans in corners */
  // set_nans_corners(phi, Nx, Ny, Nz);

  /* This need to be checked
     set_bc_value(phi, NAN, 'N', Nx, Ny);
     set_bc_value(phi, NAN, 'S', Nx, Ny);
     set_bc_value(phi, NAN, 'E', Nx, Ny);
     set_bc_value(phi, NAN, 'W', Nx, Ny);
  */
  
  /* NORMALIZATION */
  normalize_self(x, Nx, Ny, Nz);
  normalize_self(y, Nx, Ny, Nz);
  normalize_self(z, Nx, Ny, Nz);
  
  /* COPYING DATA TO HMDT ARRAYS */
  /* in-house */
  data_flow(data_mgl_void);
  data_phi(data_mgl_void);
  
  return;
}
#else

void *anims(void *data_void){

  return NULL;
}

void update_anims(void *ptr){
  
  return;
}

void *graphs(void *ptr){
  //  alert_msg(wins, "No graphics will be displayed");
  return NULL;
}
#endif
