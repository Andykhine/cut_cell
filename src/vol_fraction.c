#include "include/some_defs.h"

/* COMPUTE_SOLID_VOL_FRACTIONS */
/*****************************************************************************/
/**

  Sets:

  Array (int, NxNy) v_fraction: solid volume fraction at cell center.

  Array (int, NxNy) v_fraction_x: solid volume fraction midway between fluid 
  cell center and non-fluid neighbour found in x direction.

  Array (int, NxNy) v_fraction_y: solid volume fraction midway between fluid
  cell center and non-fluid neighbour found in y direction.
  
  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 05-02-2019
*/
int compute_solid_vol_fractions(Data_Mem *data){

  int ret_value = 0;

  int I, J, index_, curv_ind;

  int curve_flag = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int curves = data->curves;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;
  const int *dom_matrix = data->domain_matrix;

  double pc_temp[16];
  /* Puntos de los vertices de la celda intermedia */
  double c1[2] = {0};
  double c2[2] = {0};

  /* Solid fractions for solve_phi 
     which are always in the fluid phase and main grid */
  double *v_fraction = data->v_fraction;
  double *v_fraction_x = data->v_fraction_x;
  double *v_fraction_y = data->v_fraction_y;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *poly_coeffs = data->poly_coeffs;
  
  if(curves > 0){
    
    for (curv_ind=0; curv_ind<curves; curv_ind++){
      
      for (I=0; I<16; I++){
	pc_temp[I] = poly_coeffs[curv_ind*16 + I];
      }
      
      curve_flag = curve_is_solid_OK[curv_ind];
      
#pragma omp parallel for default(none) firstprivate(c1, c2)		\
  private(I, J, index_)							\
  shared(nodes_x, nodes_y,						\
	 Delta_x, Delta_y, Delta_xE, Delta_xW, Delta_yS, Delta_yN,	\
	 pc_temp, dom_matrix, v_fraction, v_fraction_x, v_fraction_y,	\
	 curve_flag, curv_ind)

      for (J=1; J<Ny-1; J++){
	for (I=1; I<Nx-1; I++){
	  
	  index_ = J*Nx + I;
	  
	  /* CENTER */
	  if((dom_matrix[index_ + 1] == curv_ind + 1) ||
	     (dom_matrix[index_ - 1] == curv_ind + 1) ||
	     (dom_matrix[index_ + Nx] == curv_ind + 1) ||
	     (dom_matrix[index_ - Nx] == curv_ind + 1)){
	    c1[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	    c1[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	    c2[0] = nodes_x[index_] + 0.5*Delta_x[index_];
	    c2[1] = nodes_y[index_] + 0.5*Delta_y[index_];
	    v_fraction[index_] = vol_fraction_pol(c1, c2, pc_temp, curve_flag);
	  }
	  /* EAST */
	  if ((dom_matrix[index_] == 0) && 
	      (dom_matrix[index_ + 1] == curv_ind + 1)){
	    c1[0] = nodes_x[index_];
	    c1[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	    c2[0] = nodes_x[index_] + Delta_xE[index_];
	    c2[1] = nodes_y[index_] + 0.5*Delta_y[index_];
	    v_fraction_x[index_] =
	      vol_fraction_pol(c1, c2, pc_temp, curve_flag);
	  }
	  /* WEST */
	  if ((dom_matrix[index_] == 0) && 
	      (dom_matrix[index_ - 1] == curv_ind + 1)){
	    c1[0] = nodes_x[index_] - Delta_xW[index_];
	    c1[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	    c2[0] = nodes_x[index_];
	    c2[1] = nodes_y[index_] + 0.5*Delta_y[index_];
	    v_fraction_x[index_] = 
	      vol_fraction_pol(c1, c2, pc_temp, curve_flag);
	  }
	  /* NORTH */
	  if ((dom_matrix[index_] == 0) && 
	      (dom_matrix[index_ + Nx] == curv_ind + 1)){
	    c1[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	    c1[1] = nodes_y[index_];
	    c2[0] = nodes_x[index_] + 0.5*Delta_x[index_];
	    c2[1] = nodes_y[index_] + Delta_yN[index_];
	    v_fraction_y[index_] = 
	      vol_fraction_pol(c1, c2, pc_temp, curve_flag);
	  }
	  /* SOUTH */
	  if ((dom_matrix[index_] == 0) && 
	      (dom_matrix[index_ - Nx] == curv_ind + 1)){
	    c1[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	    c1[1] = nodes_y[index_] - Delta_yS[index_];
	    c2[0] = nodes_x[index_] + 0.5*Delta_x[index_];
	    c2[1] = nodes_y[index_];
	    v_fraction_y[index_] = 
	      vol_fraction_pol(c1, c2, pc_temp, curve_flag);
	  }
	}
      } 
    }

  } //   if(curves > 0){
  
  return ret_value;
  
}

/* VOL_FRACTION_POL */
/*****************************************************************************/
/**

   Compute cell solid volume fraction for cell defined by vertexes c1 and c2.
  
   @param c1, c2, pc, curve_is_solid_OK

   @return ((double) count) / (dNx * dNy)
  
   MODIFIED: 11-12-2019
*/
double vol_fraction_pol(double *c1, double *c2, const double *pc,
			const int curve_is_solid_OK){

  int i, j;

  int count = 0;
    
  const int dNx = 20; 
  const int dNy = dNx;
  
  double delta_x, delta_y;
  double x_test[3] = {0};
  
  const double curve_factor = pow(-1, 1 + curve_is_solid_OK);

  /* Cell subdivision */
  delta_x = (c2[0] - c1[0]) / dNx;
  delta_y = (c2[1] - c1[1]) / dNy;

  for (i=0; i<dNx; i++){
    for (j=0; j<dNy; j++){
      x_test[0] = c1[0] + (i + 0.5)*delta_x;
      x_test[1] = c1[1] + (j + 0.5)*delta_y;
      if (polyn(x_test, pc) * curve_factor < 0){
	count += 1;
      }
    }
  }
  return ((double) count) / (dNx * dNy);
}

/* F_TEST_LINE */
double f_test_line(double x, double y, double pend, double inters){
  return y - (inters + pend*x);	
}

/* COMPUTE_SOLID_VOL_FRACTIONS_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, NxNyNz) v_fraction: solid volume fraction at cell center.

  Array (int, NxNyNz) v_fraction_x: solid volume fraction midway between fluid 
  cell center and non-fluid neighbour found in x direction.

  Array (int, NxNyNz) v_fraction_y: solid volume fraction midway between fluid
  cell center and non-fluid neighbour found in y direction.

  Array (int, NxNyNz) v_fraction_z: solid volume fraction midway between fluid 
  cell center and non-fluid neighbour found in z direction.    

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 13-07-2019
*/
int compute_solid_vol_fractions_3D(Data_Mem *data){

  int ret_value = 0;

  int I, J, K, index_, curv_ind;

  int curve_flag = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int curves = data->curves;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;
  const int *dom_matrix = data->domain_matrix;

  double pc_temp[16];
  /* Puntos de los vertices de la celda intermedia */
  double c1[3] = {0};
  double c2[3] = {0};

  /* Solid fractions for solve_phi 
     which are always in the fluid phase and main grid */
  double *v_fraction = data->v_fraction;
  double *v_fraction_x = data->v_fraction_x;
  double *v_fraction_y = data->v_fraction_y;
  double *v_fraction_z = data->v_fraction_z;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;  
  const double *poly_coeffs = data->poly_coeffs;
  
  if(curves > 0){
    
    for (curv_ind=0; curv_ind<curves; curv_ind++){
      
      for (I=0; I<16; I++){
	pc_temp[I] = poly_coeffs[curv_ind*16 + I];
      }
      
      curve_flag = curve_is_solid_OK[curv_ind];
      
#pragma omp parallel for default(none) firstprivate(c1, c2)		\
  private(I, J, K, index_)						\
  shared(nodes_x, nodes_y, nodes_z,					\
	 vertex_x, vertex_y, vertex_z,					\
	 pc_temp, dom_matrix, v_fraction, v_fraction_x, v_fraction_y,	\
	 v_fraction_z, curve_flag, curv_ind)

      for (K=1; K<Nz-1; K++){
	for (I=1; I<Nx-1; I++){
	  for (J=1; J<Ny-1; J++){
	  
	    index_ = K*Nx*Ny + J*Nx + I;
	  
	    /* CENTER */
	    if((dom_matrix[index_ + 1] == curv_ind + 1) ||
	       (dom_matrix[index_ - 1] == curv_ind + 1) ||
	       (dom_matrix[index_ + Nx] == curv_ind + 1) ||
	       (dom_matrix[index_ - Nx] == curv_ind + 1) ||
	       (dom_matrix[index_ + Nx*Ny] == curv_ind + 1) ||
	       (dom_matrix[index_ - Nx*Ny] == curv_ind + 1)){
		 
	      c1[0] = vertex_x[I-1];
	      c1[1] = vertex_y[J-1];
	      c1[2] = vertex_z[K-1];
	      c2[0] = vertex_x[I];
	      c2[1] = vertex_y[J];
	      c2[2] = vertex_z[K];
	      v_fraction[index_] = vol_fraction_pol_3D(c1, c2, pc_temp, curve_flag);
	    }
	       
	    /* EAST */
	    if ((dom_matrix[index_] == 0) && 
		(dom_matrix[index_ + 1] == curv_ind + 1)){
	      c1[0] = nodes_x[index_];
	      c1[1] = vertex_y[J-1];
	      c1[2] = vertex_z[K-1];
	      c2[0] = nodes_x[index_+1];
	      c2[1] = vertex_y[J];
	      c2[2] = vertex_z[K];
	      v_fraction_x[index_] =
		vol_fraction_pol_3D(c1, c2, pc_temp, curve_flag);
	    }
	       
	    /* WEST */
	    if ((dom_matrix[index_] == 0) && 
		(dom_matrix[index_ - 1] == curv_ind + 1)){
	      c1[0] = nodes_x[index_-1];
	      c1[1] = vertex_y[J-1];
	      c1[2] = vertex_z[K-1];
	      c2[0] = nodes_x[index_];
	      c2[1] = vertex_y[J];
	      c2[2] = vertex_z[K];
	      v_fraction_x[index_] = 
		vol_fraction_pol_3D(c1, c2, pc_temp, curve_flag);
	    }
	       
	    /* NORTH */
	    if ((dom_matrix[index_] == 0) && 
		(dom_matrix[index_ + Nx] == curv_ind + 1)){
	      c1[0] = vertex_x[I-1];
	      c1[1] = nodes_y[index_];
	      c1[2] = vertex_z[K-1];
	      c2[0] = vertex_x[I];
	      c2[1] = nodes_y[index_+Nx];
	      c2[2] = vertex_z[K];
	      v_fraction_y[index_] = 
		vol_fraction_pol_3D(c1, c2, pc_temp, curve_flag);
	    }
	       
	    /* SOUTH */
	    if ((dom_matrix[index_] == 0) && 
		(dom_matrix[index_ - Nx] == curv_ind + 1)){
	      c1[0] = vertex_x[I-1];
	      c1[1] = nodes_y[index_-Nx];
	      c1[2] = vertex_z[K-1];
	      c2[0] = vertex_x[I];
	      c2[1] = nodes_y[index_];
	      c2[2] = vertex_z[K];
	      v_fraction_y[index_] = 
		vol_fraction_pol_3D(c1, c2, pc_temp, curve_flag);
	    }

	    /* TOP */
	    if ((dom_matrix[index_] == 0) &&
		(dom_matrix[index_ + Nx*Ny] ==  curv_ind + 1)){
	      c1[0] = vertex_x[I-1];
	      c1[1] = vertex_y[J-1];
	      c1[2] = nodes_z[index_];
	      c2[0] = vertex_x[I];
	      c2[1] = vertex_y[J];
	      c2[2] = nodes_z[index_+Nx*Ny];
	      v_fraction_z[index_] =
		vol_fraction_pol_3D(c1, c2, pc_temp, curve_flag);
	    }

	    /* BOTTOM */
	    if ((dom_matrix[index_] == 0) &&
		(dom_matrix[index_ - Nx*Ny] == curv_ind + 1)){
	      c1[0] = vertex_x[I-1];
	      c1[1] = vertex_y[J-1];
	      c1[2] = nodes_z[index_-Nx*Ny];
	      c2[0] = vertex_x[I];
	      c2[1] = vertex_y[J];
	      c2[2] = nodes_z[index_];
	      v_fraction_z[index_] =
		vol_fraction_pol_3D(c1, c2, pc_temp, curve_flag);
	    }
	       
	  }
	}
      }
    } //     for (curv_ind=0; curv_ind<curves; curv_ind++)

  } //   if(curves > 0)
  
  return ret_value;
  
}

/* VOL_FRACTION_POL_3D */
/**

   Compute cell solid volume fraction for cell defined by vertexes c1 and c2.
  
   @param c1, c2, pc, curve_is_solid_OK

   @return ((double) count) / (dNx * dNy)
  
   MODIFIED: 11-12-2019
*/
double vol_fraction_pol_3D(double *c1, double *c2, const double *pc,
			const int curve_is_solid_OK){

  int i, j, k;

  int count = 0;
    
  const int dNx = 20; 
  const int dNy = dNx;
  const int dNz = dNx;
  
  double delta_x, delta_y, delta_z;
  double x_test[3] = {0};
  
  const double curve_factor = pow(-1, 1 + curve_is_solid_OK);

  /* Cell subdivision */
  delta_x = (c2[0] - c1[0]) / dNx;
  delta_y = (c2[1] - c1[1]) / dNy;
  delta_z = (c2[2] - c1[2]) / dNz;

  for (k=0; k<dNz; k++){
    for (j=0; j<dNy; j++){
      for (i=0; i<dNx; i++){

	x_test[0] = c1[0] + (i + 0.5) * delta_x;
	x_test[1] = c1[1] + (j + 0.5) * delta_y;
	x_test[2] = c1[2] + (k + 0.5) * delta_z;
	if (polyn(x_test, pc) * curve_factor < 0){
	  count += 1;
	}
      }
    }
  }

	     
  return ((double) count) / (dNx * dNy * dNz);
}

/* AREA_FRACTION_POL */
/*****************************************************************************/
/**
  
  Determines the fraction of the face that is located on the fluid domain:
  fluid_area / total_area, for the face defined between the vertex c1 and c2.

  normal_direction = 0, face normal to x axis.

  normal_direction = 1, face normal to y axis.

  normal_direction = 2, face normal to z axis.

  @param c1, c2, pc, curve_is_solid_OK, normal_direction

  @return ((double) count) / (dNx * dNy)
  
  MODIFIED: 31-07-2019
*/
double area_fraction_pol(const double *c1, const double *c2, const double *pc,
			 const int curve_is_solid_OK, const int normal_direction){

  int i, j, k;

  int count = 0;
    
  const int dNx = 20; 
  const int dNy = dNx;
  const int dNz = dNx;
  
  double delta_x, delta_y, delta_z;
  double x_test[3];
  
  const double curve_factor = pow(-1, 1 + curve_is_solid_OK);

  /* Face perpendicular to x axis */
  if(normal_direction == 0){
    x_test[0] = c1[0];
    delta_y = (c2[1] - c1[1]) / dNy;
    delta_z = (c2[2] - c1[2]) / dNz;
    
    for (k=0; k<dNz; k++){
      for (j=0; j<dNy; j++){
	x_test[1] = c1[1] + (j + 0.5)*delta_y;
	x_test[2] = c1[2] + (k + 0.5)*delta_z;
	if (polyn(x_test, pc) * curve_factor > 0){
	  count += 1;
	}
      }
    }
  }
  /* Face perpendicular to y axis */
  else if(normal_direction == 1){
    x_test[1] = c1[1];
    delta_x = (c2[0] - c1[0]) / dNx;
    delta_z = (c2[2] - c1[2]) / dNz;

    for (k=0; k<dNz; k++){
      for (i=0; i<dNx; i++){
	x_test[0] = c1[0] + (i + 0.5)*delta_x;
	x_test[2] = c1[2] + (k + 0.5)*delta_z;
	if (polyn(x_test, pc) * curve_factor > 0){
	  count += 1;
	}
      }
    }    
  }
  /* Face perpendicular to z axis */
  else if(normal_direction == 2){
    x_test[2] = c1[2];
    delta_x = (c2[0] - c1[0]) / dNx;
    delta_y = (c2[1] - c1[1]) / dNy;

    for (j=0; j<dNy; j++){
      for (i=0; i<dNx; i++){
	x_test[0] = c1[0] + (i + 0.5)*delta_x;
	x_test[1] = c1[1] + (j + 0.5)*delta_y;
	if (polyn(x_test, pc) * curve_factor > 0){
	  count += 1;
	}
      }
    }
  }
  
  return ((double) count) / (dNx * dNy);
}

/* FACE_CENTROID */
/*****************************************************************************/
/**

  Determines the face centroid, for the face defined between the vertex c0 and c1, 
  and stores the values on x_centroid array.

  normal_direction = 0, face normal to x axis.

  normal_direction = 1, face normal to y axis.

  normal_direction = 2, face normal to z axis.

  @param x_centroid, c0, c1, pc, curve_is_solid_OK, normal_direction

  @return ret_value not implemented.
  
  MODIFIED: 31-07-2019
*/
void face_centroid(double *x_centroid, const double *c0, const double *c1, const double *pc,
		   const int curve_is_solid_OK, const int normal_direction){
  
  int i, j, k;
  int count = 0;

  const int dNx = 40;
  const int dNy = dNx;
  const int dNz = dNx;

  double delta_x, delta_y, delta_z;
  double x_sum = 0;
  double y_sum = 0;
  double z_sum = 0;

  double x_test[3];

  const double curve_factor = pow(-1, 1 + curve_is_solid_OK);

  /* Face perpendicular to x axis */
  if(normal_direction == 0){
    x_test[0] = c0[0];
    delta_y = (c1[1] - c0[1]) / dNy;
    delta_z = (c1[2] - c0[2]) / dNz;
    
    for(k=0; k<dNz; k++){
      x_test[2] = c0[2] + (k + 0.5) * delta_z;
      for(j=0; j<dNy; j++){
	x_test[1] = c0[1] + (j + 0.5) * delta_y;
	if(polyn(x_test, pc) * curve_factor > 0){
	  count += 1;
	  y_sum += x_test[1];
	  z_sum += x_test[2];
	}
      }
    }
  }
  /* Face perpendicular to y axis */
  else if(normal_direction == 1){
    x_test[1] = c0[1];
    delta_x = (c1[0] - c0[0]) / dNx;
    delta_z = (c1[2] - c0[2]) / dNz;

    for(k=0; k<dNz; k++){
      x_test[2] = c0[2] + (k + 0.5) * delta_z;
      for(i=0; i<dNx; i++){
	x_test[0] = c0[0] + (i + 0.5) * delta_x;
	if(polyn(x_test, pc) *curve_factor > 0){
	  count += 1;
	  x_sum += x_test[0];
	  z_sum += x_test[2];
	}
      }
    }
   
  }
  /* Face perpendicular to z axis */
  else if(normal_direction == 2){
    x_test[2] = c0[2];
    delta_x = (c1[0] - c0[0]) / dNx;
    delta_y = (c1[1] - c0[1]) / dNy;

    for(j=0; j<dNy; j++){
      x_test[1] = c0[1] + (j + 0.5) * delta_y;
      for(i=0; i<dNx; i++){
	x_test[0] = c0[0] + (i + 0.5) * delta_x;
	if(polyn(x_test, pc) *curve_factor > 0){
	  count += 1;
	  x_sum += x_test[0];
	  y_sum += x_test[1];
	}
      }
    }
  }
      
  if(count == 0){
    x_centroid[0] = 0.5 * (c0[0] + c1[0]);
    x_centroid[1] = 0.5 * (c0[1] + c1[1]);
    x_centroid[2] = 0.5 * (c0[2] + c1[2]);
  }
  else if(normal_direction == 0){
    x_centroid[0] = c0[0];
    x_centroid[1] = y_sum / ((double) count);
    x_centroid[2] = z_sum / ((double) count);
  }
  else if(normal_direction == 1){
    x_centroid[0] = x_sum / ((double) count);
    x_centroid[1] = c0[1];
    x_centroid[2] = z_sum / ((double) count);
  }
  else if(normal_direction == 2){
    x_centroid[0] = x_sum / ((double) count);
    x_centroid[1] = y_sum / ((double) count);
    x_centroid[2] = c0[2];
  }
    
  return;
}
