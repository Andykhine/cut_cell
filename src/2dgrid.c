#include "include/some_defs.h"

/* SET_BASE_SEGMENTS */
int set_base_segments(Grid_Info *gi, const double *d_segments, const int kseg){

  int count = 0;
  int I, k;
  int start, end;
  int N0;
  int sum;

  const int segs = gi->seg;

  double *d = gi->deltas;
  
  sum = 0;
  for(k=0; k<segs; k++){
    
    N0 = gi->N[k] - 2;

    if(k >= kseg){
      start = sum + 1;
      end = start + N0 - 1;
      
      for(I=start; I<=end; I++){
	d[I] = d_segments[k];
      }
      count++;
    }
    
    sum = sum + N0;
  }
  
  return count;
}

/* COUNT_NODES_IN_GI */
int count_nodes_in_gi(const Grid_Info *gi){
  
  int i, N = 0;

  const int segs = gi->seg;
  
  for(i = 0; i < segs; i++){
    N += gi->N[i] - 2;
  }
  N += 2;

  return N;
}

/* SMOOTH_GRID */
int smooth_grid(Data_Mem *data){

  Grid_Info *x_side = &(data->x_side);
  Grid_Info *y_side = &(data->y_side);
  Grid_Info *z_side = &(data->z_side);
  
  int ret_value = 0;

  int Nx, Ny, Nz;

  const int solve_3D_OK = data->solve_3D_OK;

  const double Lx = data->Lx;
  const double Ly = data->Ly;
  const double Lz = data->Lz;

  Nx = smooth_grid_dim(x_side, Lx);
  Ny = smooth_grid_dim(y_side, Ly);
  
  if(solve_3D_OK){
    Nz = smooth_grid_dim(z_side, Lz);
  }
  else{
    /* Ignore 3D grid information */
    data->Nz = 1;
  }
  
  if(solve_3D_OK){
    if(data->x_side.smooth_OK || data->y_side.smooth_OK || data->z_side.smooth_OK){
      printf("Former size %d x %d x %d, Current size %d x %d x %d\n",
	     data->Nx, data->Ny, data->Nz, 
	     Nx, Ny, Nz);
      
      data->Nx = Nx;
      data->Ny = Ny;
      data->Nz = Nz;
      
    }
  }
  else{
    if(data->x_side.smooth_OK || data->y_side.smooth_OK){
      printf("Former size %d x %d, Current size %d x %d\n",
	     data->Nx, data->Ny,
	     Nx, Ny);
      
      data->Nx = Nx;
      data->Ny = Ny;
      
    }
  }
  
  return ret_value;
}

/* SMOOTH_GRID_DIM */
int smooth_grid_dim(Grid_Info *gi, const double L){

  /* This function sets deltas values for the construction of the grid */
  /* Inputs:
     L total length

     Return value:
     N < 0 in case of error,
     > 0 (new) grid size dimension 
  */

  int i = 0, k = 0;
  int N = 0;
  int head = 0, tail = 0;
  
  int sum = 0;

  int N0 = 0, N1 = 0, Ntilde = 0;

  int *Nheads = NULL;
  int *Ntails = NULL;
  int *Nremnants = NULL;
  
  const int segs = gi->seg;
  const int rs = segs-1;

  const int smooth_OK = gi->smooth_OK;
  
  double r = 1;
  double m = 0;

  double d0 = 0, d1 = 0, d_1 = 0, dlocal = 0;
  double L0 = 0, Lcovered = 0;
  
  double *ds = NULL;
  double *Ls = NULL;
  double *Lremnants = NULL;
  double *mheads = NULL;
  double *mtails = NULL;
  double *dummy = NULL;
  double *d_segments = NULL;

  /* There are some memory struct here that are not needed
     only ds are needed I believe */
  N = count_nodes_in_gi(gi);
  
  ds = create_array(N, 1, 1, 0);
  gi->deltas = ds;

  Ls = create_array(segs, 1, 1, 0);
  gi->L = Ls;
  
  /* internal use */
  d_segments = create_array(segs, 1, 1, 0);
  Lremnants = create_array(segs, 1, 1, 0);
  mheads = create_array(segs, 1, 1, 0);
  mtails = create_array(segs, 1, 1, 0);
  Nheads = create_array_int(segs, 1, 1, 0);
  Ntails = create_array_int(segs, 1, 1, 0);
  Nremnants = create_array_int(segs, 1, 1, 0);
  
  if(segs > 1){
    /* Initial deltas */
    for(k=0; k<segs; k++){
      N0 = gi->N[k] - 2;
      Ls[k] = gi->per[k] * L;
      
      d_segments[k] = Ls[k] / N0;
    }

    /* Create base d.
       This is the case without smooth treatment.
    */
    set_base_segments(gi, d_segments, 0);
    
    if(!smooth_OK){
      /* Free memory */
      free(d_segments);
      free(Lremnants);
      free(mheads);
      free(mtails);
      free(Nheads);
      free(Ntails);
      free(Nremnants);
      
      return N;
    }

    /* This only executes if smooth_OK is set */
    /* Building Ntilde, ms */
    for(k=0; k<rs; k++){
      d0 = d_segments[k];
      d1 = d_segments[k+1];
      
      N0 = gi->N[k] - 2;
      N1 = gi->N[k+1] - 2;
      
      r = d1 / d0;
      
      if(r < 0.8){
	/* 1 ==> Head of k segment */
	Ntilde = N0 / 3;
	Nheads[k] = Ntilde;
	Ntails[k + 1] = 0;

	m = (d1-d0) / (Ntilde-1);

	mheads[k] = m;
      }
      else if (r > 1.2){
	/* 0 ==> Tail of k+1 segment */
	Ntilde = N1 / 3;
	Nheads[k] = 0;
	Ntails[k + 1] = Ntilde;

	m = (d1-d0) / (Ntilde-1);
	
	mtails[k + 1] = m;
      }
      else{
	Nheads[k] = 0;
	Ntails[k + 1] = 0;
      }
    }

    /* Building Lremnant, Nremnant */
    for(k=0; k<segs; k++){

      d0 = d_segments[k];
      
      if(k > 0){
	d_1 = d_segments[k-1];
      }
      
      N0 = gi->N[k] - 2;

      L0 = Ls[k];

      head = Nheads[k];
      tail = Ntails[k];
      
      Lcovered = 0;
          
      if(head){
	/* 1 Head */
	m = mheads[k];
	
	for (i = 1; i <= head; i++){
	  dlocal = m * (i-1) + d0;
	  Lcovered = Lcovered + dlocal;
	}

      }
      
      if(tail){
	/* 0 Tail */
	m = mtails[k];
	
	for (i = 1; i <= tail; i++){
	  dlocal = m * (i-1) + d_1;
	  Lcovered = Lcovered + dlocal;
	}
      }

      Lremnants[k] = L0 - Lcovered;

      Nremnants[k] =  N0 - (Ntails[k] + Nheads[k]);
      dlocal = Lremnants[k] / Nremnants[k];

      while(dlocal >= d0){
	Nremnants[k]++;
	dlocal = Lremnants[k] / Nremnants[k];
      }
      Nremnants[k]--;
      
      gi->N[k] = Ntails[k] + Nremnants[k] + Nheads[k] + 2;
      
      /*
	printf("k = %2d, N0 = %2d, sumNs = %2d, Ntail = %2d, Nremnant = %2d, Nhead = %2d, mtail = %5g, mhead = %5g\n",
	k, N0, Ntails[k] + Nremnants[k] + Nheads[k],
	Ntails[k], Nremnants[k], Nheads[k],
	mtails[k], mheads[k]);
      */
      
    } // end     for(k=0; k<segs; k++){

    /* New number of nodes */
    N = count_nodes_in_gi(gi);

    dummy = (double *)realloc((void *)ds, N * sizeof(double));
    
    if(dummy == NULL){
      exit(EXIT_FAILURE);
    }
    
    ds = dummy;

    /* Need to do over all deltas */
    sum = 0;
    for(k=0; k<segs; k++){

      d0 = d_segments[k];

      if(k > 0){
	d_1 = d_segments[k-1];
      }

      N0 = gi->N[k] - 2;

      head = Nheads[k];
      tail = Ntails[k];

      for(i=1; i<=N0; i++){

	if(tail && i <= tail){
	  m = mtails[k];
	  dlocal = m * (i-1) + d_1; // here it corresponds to the previous block..... check the equation
	}
	else if(head && i > N0 - head){
	  m = mheads[k];
	  dlocal = m * (i - N0 + head - 1) + d0;
	}
	else{
	  dlocal = Lremnants[k] / Nremnants[k];
	}

	ds[i + sum] = dlocal;

      }

      sum = sum + N0;

    }
    
  } // end if(segs > 1)
  else{
    
    d0 = L / (N - 2);
    
    for(i=1; i<N-1; i++){
      ds[i] = d0;
    }
    
  }
  
  /* Free memory */
  free(d_segments);
  free(Lremnants);
  free(mheads);
  free(mtails);
  free(Nheads);
  free(Ntails);
  free(Nremnants);
  
  return N;

}

/* GRID_MAKER */
void grid_maker(Data_Mem *data){

  int i, j, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  double delta_x, delta_y;
    
  const double Lx = data->Lx;
  const double Ly = data->Ly;
  
  double *nodes_x = data->nodes_x;
  double *nodes_y = data->nodes_y;
  double *Delta_x = data->Delta_x;
  double *Delta_y = data->Delta_y;  
  double *Delta_xE = data->Delta_xE;
  double *Delta_xW = data->Delta_xW;
  double *Delta_yN = data->Delta_yN;
  double *Delta_yS = data->Delta_yS;
  double *Delta_xe = data->Delta_xe;
  double *Delta_xw = data->Delta_xw;
  double *Delta_yn = data->Delta_yn;
  double *Delta_ys = data->Delta_ys;

  double *nodes_x_u = data->nodes_x_u;
  double *nodes_y_u = data->nodes_y_u;
  double *Delta_x_u = data->Delta_x_u;
  double *Delta_y_u = data->Delta_y_u;  
  double *Delta_xE_u = data->Delta_xE_u;
  double *Delta_xW_u = data->Delta_xW_u;
  double *Delta_yN_u = data->Delta_yN_u;
  double *Delta_yS_u = data->Delta_yS_u;
  double *Delta_xe_u = data->Delta_xe_u;
  double *Delta_xw_u = data->Delta_xw_u;
  double *Delta_yn_u = data->Delta_yn_u;
  double *Delta_ys_u = data->Delta_ys_u;

  double *nodes_x_v = data->nodes_x_v;
  double *nodes_y_v = data->nodes_y_v;
  double *Delta_x_v = data->Delta_x_v;
  double *Delta_y_v = data->Delta_y_v;  
  double *Delta_xE_v = data->Delta_xE_v;
  double *Delta_xW_v = data->Delta_xW_v;
  double *Delta_yN_v = data->Delta_yN_v;
  double *Delta_yS_v = data->Delta_yS_v;
  double *Delta_xe_v = data->Delta_xe_v;
  double *Delta_xw_v = data->Delta_xw_v;
  double *Delta_yn_v = data->Delta_yn_v;
  double *Delta_ys_v = data->Delta_ys_v;

  /* SEGMENTATION OF SPACE */
  delta_x = segmentation(0, Lx, Nx);
  delta_y = segmentation(0, Ly, Ny);
    
  for(j = 0; j < Ny; j++)
    {
      for(i = 0; i < Nx; i++)
        {
	  index_ = Nx*j + i;
	  Delta_x[index_] = delta_x;
	  Delta_y[index_] = delta_y;
	  if(i == 0 || i == Nx - 1)
            {
	      Delta_x[index_] = 0;
            }
	  if(j == 0 || j == Ny - 1)
            {
	      Delta_y[index_] = 0;
            }
        }
    }
    
  /* FACES */
  //  faces_pos(data, delta_x, delta_y);

  /* DELTAS */
  for(j = 0; j < Ny; j++)
    {
      for(i = 1; i < Nx; i++)
        {
	  index_ = Nx*j + i;
	  Delta_xW[index_] = (Delta_x[index_] + Delta_x[index_ - 1])/2;
	  Delta_xw[index_] = (Delta_x[index_])/2;
        }
    }
        
  for(j = 0; j < Ny; j++)
    {
      for(i = 0; i < Nx - 1; i++)
        {
	  index_ = Nx*j + i;
	  Delta_xE[index_] = (Delta_x[index_] + Delta_x[index_ + 1])/2;
	  Delta_xe[index_] = (Delta_x[index_])/2;
        }
    }
    
  for(j = 1; j < Ny; j++)
    {
      for(i = 0; i < Nx; i++)
        {
	  index_ = Nx*j + i;
	  Delta_yS[index_] = (Delta_y[index_] + Delta_y[index_ - Nx])/2;
	  Delta_ys[index_] = (Delta_y[index_])/2; 
        }
    }

  for(j = 0; j < Ny - 1; j++)
    {
      for(i = 0; i < Nx; i++)
        {
	  index_ = Nx*j + i;
	  Delta_yN[index_] = (Delta_y[index_] + Delta_y[index_ + Nx])/2;
	  Delta_yn[index_] = (Delta_y[index_])/2;
        }
    }

  /* NODES */
  for(j = 0; j < Ny; j++)
    {
      for(i = 1; i < Nx; i++)
        {
	  index_ = Nx*j + i;
	  nodes_x[index_] = nodes_x[index_ - 1] + Delta_xE[index_ - 1];
        }
    }
  for(j = 1; j < Ny; j++)
    {
      for(i = 0; i < Nx; i++)
        {
	  index_ = Nx*j + i;
	  nodes_y[index_] = nodes_y[index_ - Nx] + Delta_yN[index_ - Nx];
        }
    }

  /* U GRID */
  for(j = 0; j < Ny; j++)
    {
      for(i = 0; i < nx; i++)
        {
	  index_ = nx*j + i;
	  Delta_x_u[index_] = delta_x;
	  Delta_y_u[index_] = delta_y;
	  if(i == 0 || i == nx - 1)
            {
	      Delta_x_u[index_] = 0.5*delta_x;
            }
	  if(j == 0 || j == Ny - 1)
            {
	      Delta_y_u[index_] = 0;
            }
        }
    }
    
  /* FACES */
  /*faces_pos(data, delta_x, delta_y);*/

  /* DELTAS */
  for(j = 0; j < Ny; j++)
    {
      for(i = 1; i < nx; i++)
        {
	  index_ = nx*j + i;
	  Delta_xW_u[index_] = Delta_x_u[index_];
	  Delta_xw_u[index_] = (Delta_x_u[index_])/2;
	  if(i == nx - 1){
	    Delta_xW_u[index_] = 2*Delta_x_u[index_];
	    Delta_xw_u[index_] = Delta_x_u[index_];
	  }
        }
    }
        
  for(j = 0; j < Ny; j++)
    {
      for(i = 0; i < nx - 1; i++)
        {
	  index_ = nx*j + i;
	  Delta_xE_u[index_] = Delta_x_u[index_];
	  Delta_xe_u[index_] = (Delta_x_u[index_])/2;
	  if(i == 0){
	    Delta_xE_u[index_] = 2*Delta_x_u[index_];
	    Delta_xe_u[index_] = Delta_x_u[index_];
	  }
        }
    }
    
  for(j = 1; j < Ny; j++)
    {
      for(i = 0; i < nx; i++)
        {
	  index_ = nx*j + i;
	  Delta_yS_u[index_] = (Delta_y_u[index_] + Delta_y_u[index_ - nx])/2;
	  Delta_ys_u[index_] = (Delta_y_u[index_])/2; 
        }
    }

  for(j = 0; j < Ny - 1; j++)
    {
      for(i = 0; i < nx; i++)
        {
	  index_ = nx*j + i;
	  Delta_yN_u[index_] = (Delta_y_u[index_] + Delta_y_u[index_ + nx])/2;
	  Delta_yn_u[index_] = (Delta_y_u[index_])/2;
        }
    }

  /* NODES */
  for(j = 0; j < Ny; j++)
    {
      for(i = 1; i < nx; i++)
        {
	  index_ = nx*j + i;
	  nodes_x_u[index_] = nodes_x_u[index_ - 1] + Delta_xE_u[index_ - 1];
        }
    }
  for(j = 1; j < Ny; j++)
    {
      for(i = 0; i < nx; i++)
        {
	  index_ = nx*j + i;
	  nodes_y_u[index_] = nodes_y_u[index_ - nx] + Delta_yN_u[index_ - nx];
        }
    }

  /* V GRID */
  for(j = 0; j < ny; j++){
    for(i = 0; i < Nx; i++){
      index_ = Nx*j + i;
      Delta_x_v[index_] = delta_x;
      Delta_y_v[index_] = delta_y;
      if(j == 0 || j == ny-1){
	Delta_y_v[index_] = 0.5*delta_y;
      }
      if(i == 0 || i == Nx-1){
	Delta_x_v[index_] = 0;
      }
    }
  }
    
  /* DELTAS*/
  for(j = 1; j < ny; j++){
    for(i = 0; i< Nx; i++){
      index_ = Nx*j + i;
      Delta_yS_v[index_] = Delta_y_v[index_];
      Delta_ys_v[index_] = (Delta_y_v[index_])/2;
      if(j == ny-1){
	Delta_yS_v[index_] = 2*Delta_y_v[index_];
	Delta_ys_v[index_] = Delta_y_v[index_];
      }
    }
  }
     
        
  for(j = 0; j < ny-1; j++){
    for(i = 0; i < Nx; i++){
      index_ = Nx*j + i;
      Delta_yN_v[index_] = Delta_y_v[index_];
      Delta_yn_v[index_] = (Delta_y_v[index_])/2;
      if(j == 0){
	Delta_yN_v[index_] = 2*Delta_y_v[index_];
	Delta_yn_v[index_] = Delta_y_v[index_];
      }
    }
  }
     
    
  for(j = 0; j < ny; j++){
    for(i = 1; i < Nx; i++){
      index_ = Nx*j + i;
      Delta_xW_v[index_] = (Delta_x_v[index_] + Delta_x_v[index_-1])/2;
      Delta_xw_v[index_] = (Delta_x_v[index_])/2;
    }
  }

  for(j = 0; j < ny; j++){
    for(i = 0; i < Nx-1; i++){
      index_ = Nx*j + i;
      Delta_xE_v[index_] = (Delta_x_v[index_] + Delta_x_v[index_+1])/2;
      Delta_xe_v[index_] = (Delta_x_v[index_])/2;
    }
  }

  /* NODES */
  for(j = 0; j < ny; j++){
    for(i = 1; i < Nx; i++){
      index_ = Nx*j + i;
      nodes_x_v[index_] = nodes_x_v[index_-1] + Delta_xE_v[index_-1];
    }
  }

  for(j = 1; j < ny; j++){
    for(i = 0; i < Nx; i++){
      index_ = Nx*j + i;
      nodes_y_v[index_] = nodes_y_v[index_-Nx] + Delta_yN_v[index_-Nx];
    }
  }

  return;
}

/* SEGMENTATION */
double segmentation(const double start, const double end, const int nodes){
  double delta;
  delta = (double) (end - start)/(nodes - 2);
  return delta;
}

/* MAKE_SURFACE */
void make_surface(Data_Mem *data,
		  const double angle_x, const double angle_y, const double angle_z,
		  const double lambda_1, const double lambda_2, const double lambda_3,
		  const double constant_d, 
		  const double tcompx, const double tcompy, const double tcompz,
		  double *poly_coeffs, const int count){
  int i, j, index_;

  double *intermediate = NULL;
  double *matrix_c = data->matrix_c;
  double *R_x = data->R_x;
  double *R_y = data->R_y;
  double *R_z = data->R_z;
  double *Rot_ = data->Rot_;
  double *Rot_T = data->Rot_T;
  double *matrix_A = data->matrix_A;
  double *Trans_ = data->Trans_;

  intermediate = create_array(3, 3, 1, 0);

  for(j = 0; j < 3; j++)
    {
      for(i = 0; i < 3; i++)
        {
	  index_ = 3*j + i;
	  if(i == 0 && j == 0)
            {
	      matrix_c[index_] = lambda_1;
	      R_x[index_] = 1;
	      R_y[index_] = cos(angle_y);
	      R_z[index_] = cos(angle_z);
	      Trans_[1*j + i] = tcompx;
            }
	  if(i == 0 && j == 1)
            {
	      R_x[index_] = 0;
	      R_y[index_] = 0;
	      R_z[index_] = sin(angle_z);
            }
	  if(i == 0 && j == 2)
            {
	      R_x[index_] = 0;
	      R_y[index_] = -sin(angle_y);
	      R_z[index_] = 0;
            }
	  if(i == 1 && j == 0)
            {
	      R_x[index_] = 0;
	      R_y[index_] = 0;
	      R_z[index_] = -sin(angle_z);
	      Trans_[1*j + i] = tcompy;
            }
	  if(i == 1 && j == 1)
            {
	      matrix_c[index_] = lambda_2;
	      R_x[index_] = cos(angle_x);
	      R_y[index_] = 1;
	      R_z[index_] = cos(angle_z);
            }
	  if(i == 1 && j == 2)
            {
	      R_x[index_] = sin(angle_x);
	      R_y[index_] = 0;
	      R_z[index_] = 0;
            }
	  if(i == 2 && j == 0)
            {
	      R_x[index_] = 0;
	      R_y[index_] = sin(angle_y);
	      R_z[index_] = 0;
	      Trans_[1*j + i] = tcompz;
            }
	  if(i == 2 && j == 1)
            {
	      R_x[index_] = -sin(angle_x);
	      R_y[index_] = 0;
	      R_z[index_] = 0;
            }
	  if(i == 2 && j == 2)
            {
	      matrix_c[index_] = lambda_3;
	      R_x[index_] = cos(angle_x);
	      R_y[index_] = cos(angle_y);
	      R_z[index_] = 1;
            }
        }
    }
  
  /* ROTATION MATRIX */
  matrix_product(R_y, R_x, intermediate, 3, 3, 3, 3);
  matrix_product(R_z, intermediate, Rot_, 3, 3, 3, 3);
  transpose_matrix(Rot_, Rot_T, 3, 3);
  matrix_product(Rot_T, matrix_c, intermediate, 3, 3, 3, 3);
  matrix_product(intermediate, Rot_, matrix_A, 3, 3, 3, 3);
  
  data->vector_a[0] = -(2*matrix_A[3*0+0]*Trans_[1*0] +
			matrix_A[3*0+1]*Trans_[1*1] + 
			matrix_A[3*0+2]*Trans_[1*2] +
			matrix_A[3*1+0]*Trans_[1*1] + 
			matrix_A[3*2+0]*Trans_[1*2]);
     
  data->vector_a[1] = -(matrix_A[3*0+1]*Trans_[1*1] +
			2*matrix_A[3*1+1]*Trans_[1*1] + 
			matrix_A[3*1+2]*Trans_[1*2] +
			matrix_A[3*1+0]*Trans_[1*0] + 
			matrix_A[3*2+1]*Trans_[1*2]);
  
  data->vector_a[2] = -(matrix_A[3*2+0]*Trans_[1*0] +
			matrix_A[3*2+1]*Trans_[1*1] + 
			2*matrix_A[3*2+2]*Trans_[1*2] +
			matrix_A[3*0+2]*Trans_[1*1] + 
			matrix_A[3*1+2]*Trans_[1*1]);
  
  data->constant_a = matrix_A[3*0+0]*Trans_[1*0]*Trans_[1*0] + 
    matrix_A[3*0+1]*Trans_[1*0]*Trans_[1*1] + 
    matrix_A[3*0+2]*Trans_[1*0]*Trans_[1*2] +
    matrix_A[3*1+0]*Trans_[1*0]*Trans_[1*1] +
    matrix_A[3*1+1]*Trans_[1*1]*Trans_[1*1] +
    matrix_A[3*1+2]*Trans_[1*2]*Trans_[1*1] +
    matrix_A[3*2+0]*Trans_[1*0]*Trans_[1*2] +
    matrix_A[3*2+1]*Trans_[1*1]*Trans_[1*2] +
    matrix_A[3*2+2]*Trans_[1*2]*Trans_[1*2] + constant_d;

  /* SAVING POLYNOMIAL COEFFICIENTES, SEE EQUATION (38) IN KIRKPATRICK ET AL */
  /* THE FACTOR OF TWO IS INCLUDED WITHIN THE COEFFICIENTS */
  i = 0; j = 0;
  index_ = 3*j + i;
  poly_coeffs[count*16 + 0] = matrix_A[index_];

  i = 1; j = 1;
  index_ = 3*j + i;
  poly_coeffs[count*16 + 1] = matrix_A[index_];

  i = 2; j = 2;
  index_ = 3*j + i;
  poly_coeffs[count*16 + 2] = matrix_A[index_];

  i = 0; j = 1;
  index_ = 3*j + i;
  poly_coeffs[count*16 + 3] = 2*matrix_A[index_];

  i = 0; j = 2;
  index_ = 3*j + i;
  poly_coeffs[count*16 + 4] = 2*matrix_A[index_];

  i = 1; j = 2;
  index_ = 3*j + i;
  poly_coeffs[count*16 + 5] = 2*matrix_A[index_];

  poly_coeffs[count*16 + 6] = data->vector_a[0];
  poly_coeffs[count*16 + 7] = data->vector_a[1];
  poly_coeffs[count*16 + 8] = data->vector_a[2];

  poly_coeffs[count*16 + 9] = data->constant_a;

  free(intermediate);

  return;
}

/* MATRIX_PRODUCT */
void matrix_product(const double *A_matrix,const double *B_matrix, double *C_matrix, 
		    const int A_ROW, const int A_COL, const int B_ROW, const int B_COL)
{
  int i, j, k;
  double sum;
  
  if(A_COL != B_ROW){
    //      error_msg("The number of columns in the first matrix must be equal to "
    //		"the number of rows in the second matrix");
    exit(EXIT_FAILURE);
    return;
  }
    
  for(i = 0; i < A_ROW; i++){
    for(j = 0; j < B_COL; j++){
      sum = 0;
      for(k = 0; k < A_COL; k++){
	sum += A_matrix[A_ROW*k + i] * B_matrix[B_ROW*j + k];
      }
      C_matrix[A_ROW*j + i] = sum;
    }
  }
  return;
}

/* TRANSPOSE_MATRIX */
void transpose_matrix(const double *matrix, double *transpose,
		      const int ROW, const int COL){
  int i, j;
  for(j = 0; j < COL ; j++)
    {
      for(i = 0; i < ROW; i++)
        {
	  transpose[COL*i + j] = matrix[ROW*j + i];
        }
    }
  return;
}

/* MATRIX_ADDITION*/
void matrix_addition(const double *A_matrix, const double *B_matrix, double *C_matrix,
		     const int A_ROW, const int A_COL, const int B_ROW,
		     const int B_COL, const int C_ROW, const int C_COL,
		     const int option){
 
  /* A for first matrix and B for the second and C for the result option = 1 
     for add and option = 0 for subtract*/
  
  int i, j, index_;
  if(A_COL != B_COL || A_ROW != B_ROW){
    //    error_msg("Can't add these matrices. The size must be the same");
    exit(EXIT_FAILURE);
  }
  
  if(C_COL != A_COL || C_ROW != A_ROW){
    //      error_msg("The container matrix must have the same size");
    exit(EXIT_FAILURE);
  }
  
  if(option){
    for(j = 0; j < B_COL; j++){
      for(i = 0; i < A_ROW; i++){
	index_ = A_ROW*j + i;
	C_matrix[index_] = A_matrix[index_] + B_matrix[index_];
      }
    }
  }
  else{
    for(j = 0; j < B_COL; j++){
      for(i = 0; i < A_ROW; i++){
	index_ = A_ROW*j + i;
	C_matrix[index_] = A_matrix[index_] - B_matrix[index_];
      }
    }
  }
  
  return;
}

/* GRID_MAKER NON UNIFORM */
/*****************************************************************************/
/**

  Sets:

  Grid description arrays for p, u, v and w grids.

  @param data
	  
  @return ret_value not implemented.
  
  MODIFIED: 20-03-2020
*/
void grid_maker_non_uni(Data_Mem *data){

  Wins *wins = &(data->wins);

  int i, j, k, I, J, K, index_face, index_;
  int index_phi;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int solve_3D_OK = data->solve_3D_OK;
  
  double *Delta_x = data->Delta_x;
  double *Delta_y = data->Delta_y;
  double *Delta_z = data->Delta_z;
  double *Delta_xE = data->Delta_xE;
  double *Delta_xW = data->Delta_xW;
  double *Delta_yN = data->Delta_yN;
  double *Delta_yS = data->Delta_yS;
  double *Delta_zT = data->Delta_zT;
  double *Delta_zB = data->Delta_zB;
  double *Delta_xe = data->Delta_xe;
  double *Delta_xw = data->Delta_xw;
  double *Delta_yn = data->Delta_yn;
  double *Delta_ys = data->Delta_ys;
  double *Delta_zt = data->Delta_zt;
  double *Delta_zb = data->Delta_zb;
  double *nodes_x = data->nodes_x;
  double *nodes_y = data->nodes_y;
  double *nodes_z = data->nodes_z;

  double *nodes_x_u = data->nodes_x_u;
  double *nodes_y_u = data->nodes_y_u;
  double *nodes_z_u = data->nodes_z_u;
  double *Delta_x_u = data->Delta_x_u;
  double *Delta_y_u = data->Delta_y_u;
  double *Delta_z_u = data->Delta_z_u;
  double *Delta_xE_u = data->Delta_xE_u;
  double *Delta_xW_u = data->Delta_xW_u;
  double *Delta_yN_u = data->Delta_yN_u;
  double *Delta_yS_u = data->Delta_yS_u;
  double *Delta_zT_u = data->Delta_zT_u;
  double *Delta_zB_u = data->Delta_zB_u;
  double *Delta_xe_u = data->Delta_xe_u;
  double *Delta_xw_u = data->Delta_xw_u;
  double *Delta_yn_u = data->Delta_yn_u;
  double *Delta_ys_u = data->Delta_ys_u;
  double *Delta_zt_u = data->Delta_zt_u;
  double *Delta_zb_u = data->Delta_zb_u;

  double *nodes_x_v = data->nodes_x_v;
  double *nodes_y_v = data->nodes_y_v;
  double *nodes_z_v = data->nodes_z_v;
  double *Delta_x_v = data->Delta_x_v;
  double *Delta_y_v = data->Delta_y_v;
  double *Delta_z_v = data->Delta_z_v;
  double *Delta_xE_v = data->Delta_xE_v;
  double *Delta_xW_v = data->Delta_xW_v;
  double *Delta_yN_v = data->Delta_yN_v;
  double *Delta_yS_v = data->Delta_yS_v;
  double *Delta_zT_v = data->Delta_zT_v;
  double *Delta_zB_v = data->Delta_zB_v;
  double *Delta_xe_v = data->Delta_xe_v;
  double *Delta_xw_v = data->Delta_xw_v;
  double *Delta_yn_v = data->Delta_yn_v;
  double *Delta_ys_v = data->Delta_ys_v;
  double *Delta_zt_v = data->Delta_zt_v;
  double *Delta_zb_v = data->Delta_zb_v;

  double *nodes_x_w = data->nodes_x_w;
  double *nodes_y_w = data->nodes_y_w;
  double *nodes_z_w = data->nodes_z_w;
  double *Delta_x_w = data->Delta_x_w;
  double *Delta_y_w = data->Delta_y_w;
  double *Delta_z_w = data->Delta_z_w;
  double *Delta_xE_w = data->Delta_xE_w;
  double *Delta_xW_w = data->Delta_xW_w;
  double *Delta_yN_w = data->Delta_yN_w;
  double *Delta_yS_w = data->Delta_yS_w;
  double *Delta_zT_w = data->Delta_zT_w;
  double *Delta_zB_w = data->Delta_zB_w;
  double *Delta_xe_w = data->Delta_xe_w;
  double *Delta_xw_w = data->Delta_xw_w;
  double *Delta_yn_w = data->Delta_yn_w;
  double *Delta_ys_w = data->Delta_ys_w;
  double *Delta_zt_w = data->Delta_zt_w;
  double *Delta_zb_w = data->Delta_zb_w;
  double *vertex_x = data->vertex_x;
  double *vertex_y = data->vertex_y;
  double *vertex_z = data->vertex_z;
  double *vertex_x_u = data->vertex_x_u;
  double *vertex_y_v = data->vertex_y_v;
  double *vertex_z_w = data->vertex_z_w;

  double *faces_x = data->faces_x;
  double *faces_y = data->faces_y;
  double *faces_z = data->faces_z;

  const double *dx = data->x_side.deltas;
  const double *dy = data->y_side.deltas;
  const double *dz = data->z_side.deltas;

  /* Deltas */
  progress_msg(wins, "base grid", 0.0); 
#pragma omp parallel for default(none) private(i, j, k, index_)	\
  shared(dx, Delta_x)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	Delta_x[index_] = dx[i];
      }
    }
  }
  
#pragma omp parallel for default(none) private(i, j, k, index_)	\
  shared(dy, Delta_y)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	Delta_y[index_] = dy[j];
      }
    }
  }  

  if(solve_3D_OK){
  
#pragma omp parallel for default(none) private(i, j, k, index_) \
  shared(dz, Delta_z)

    for(k = 0; k < Nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  Delta_z[index_] = dz[k];
	}
      }
    }
  }

  progress_msg(wins, "base grid", 10.0);
  
  /* DELTAS */
#pragma omp parallel for default(none) private(i, j, k, index_)	\
  shared(Delta_xW, Delta_xw, Delta_x)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 1; i < Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	Delta_xW[index_] = 0.5 * (Delta_x[index_] + Delta_x[index_ - 1]);
	Delta_xw[index_] = 0.5 * (Delta_x[index_]);
      }
    }
  }
        
#pragma omp parallel for default(none) private(i, j, k, index_)	\
  shared(Delta_xE, Delta_xe, Delta_x)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 0; i < Nx - 1; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	Delta_xE[index_] = 0.5 * (Delta_x[index_] + Delta_x[index_ + 1]);
	Delta_xe[index_] = 0.5 * (Delta_x[index_]);
      }
    }
  }
    
#pragma omp parallel for default(none) private(i, j, k, index_)	\
  shared(Delta_yS, Delta_ys, Delta_y)

  for(k = 0; k < Nz; k++){
    for(j = 1; j < Ny; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	Delta_yS[index_] = 0.5 * (Delta_y[index_] + Delta_y[index_ - Nx]);
	Delta_ys[index_] = 0.5 * (Delta_y[index_]); 
      }
    }
  }

#pragma omp parallel for default(none) private(i, j, k, index_)	\
  shared(Delta_yN, Delta_yn, Delta_y)

  for(k = 0; k < Nz; k++){  
    for(j = 0; j < Ny - 1; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	Delta_yN[index_] = 0.5 * (Delta_y[index_] + Delta_y[index_ + Nx]);
	Delta_yn[index_] = 0.5 * (Delta_y[index_]);
      }
    }
  }

  if(solve_3D_OK){

#pragma omp parallel for default(none) private(i, j, k, index_) \
  shared(Delta_zB, Delta_zb, Delta_z)
    
    for(k = 1; k < Nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  Delta_zB[index_] = 0.5 * (Delta_z[index_] + Delta_z[index_ - Nx*Ny]);
	  Delta_zb[index_] = 0.5 * (Delta_z[index_]);
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_) \
  shared(Delta_zT, Delta_zt, Delta_z)
   
    for(k = 0; k < Nz - 1; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  Delta_zT[index_] = 0.5 * (Delta_z[index_] + Delta_z[index_ + Nx*Ny]);
	  Delta_zt[index_] = 0.5 * (Delta_z[index_]);
	}
      }
    }
    
  }  // if(solve_3D_OK)

  progress_msg(wins, "base grid", 20.0);

  /* NODES */

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 1; i < Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	nodes_x[index_] = nodes_x[index_ - 1] + Delta_xE[index_ - 1];
      }
    }
  }

  for(k = 0; k < Nz; k++){
    for(j = 1; j < Ny; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	nodes_y[index_] = nodes_y[index_ - Nx] + Delta_yN[index_ - Nx];
      }
    }
  }

  if(solve_3D_OK){
    
    for(k = 1; k < Nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  nodes_z[index_] = nodes_z[index_ - Nx*Ny] + Delta_zT[index_ - Nx*Ny];
	}
      }
    }
    
  }

  progress_msg(wins, "base grid", 30.0);

  /* VERTEX */

  for(i=1; i<nx; i++){
    vertex_x[i] = vertex_x[i-1] + dx[i];
  }

  for(j=1; j<ny; j++){
    vertex_y[j] = vertex_y[j-1] + dy[j];
  }

  if(solve_3D_OK){
    for(k=1; k<nz; k++){
      vertex_z[k] = vertex_z[k-1] + dz[k];
    }
  }

  /* U GRID */

  /* DELTAS */
  
#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_x_u, Delta_y_u, Delta_x, Delta_y)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 0; i < nx; i++){
	index_ = k*nx*Ny + j*nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_x_u[index_] = 0.5 * (Delta_x[index_phi] + Delta_x[index_phi + 1]);
	Delta_y_u[index_] = Delta_y[index_phi];
      }
    }
  }

  if(solve_3D_OK){

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_z, Delta_z_u)
    
    for(k = 0; k < Nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < nx; i++){
	  index_ = k*nx*Ny + j*nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_z_u[index_] = Delta_z[index_phi];
	}
      }
    }
  }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_xW_u, Delta_xw_u, Delta_x, Delta_x_u)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 1; i < nx; i++){
	index_ = k*nx*Ny + j*nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_xW_u[index_] = Delta_x[index_phi];
	Delta_xw_u[index_] = 0.5 * Delta_x[index_phi];
      }
    }
  }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_xE_u, Delta_xe_u, Delta_x, Delta_x_u)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 0; i < nx - 1; i++){
	index_ = k*nx*Ny + j*nx + i;
	index_phi = k*Nx*Ny + j*Nx + i + 1;
	Delta_xE_u[index_] = Delta_x[index_phi];
	Delta_xe_u[index_] = 0.5 * Delta_x[index_phi];
      }
    }
  }
   
#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_yS_u, Delta_ys_u, Delta_yS, Delta_ys)

  for(k = 0; k < Nz; k++){
    for(j = 1; j < Ny; j++){
      for(i = 0; i < nx; i++){
	index_ = k*nx*Ny + j*nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_yS_u[index_] = Delta_yS[index_phi];
	Delta_ys_u[index_] = Delta_ys[index_phi]; 
      }
    }
  }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_yN_u, Delta_yn_u, Delta_yN, Delta_yn)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny - 1; j++){
      for(i = 0; i < nx; i++){
	index_ = k*nx*Ny + j*nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_yN_u[index_] = Delta_yN[index_phi];
	Delta_yn_u[index_] = Delta_yn[index_phi];
      }
    }
  }


  if(solve_3D_OK){

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_zT, Delta_zt, Delta_zT_u, Delta_zt_u)
    
    for(k = 0; k < Nz - 1; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < nx; i++){
	  index_ = k*nx*Ny + j*nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_zT_u[index_] = Delta_zT[index_phi];
	  Delta_zt_u[index_] = Delta_zt[index_phi];
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_zB, Delta_zb, Delta_zB_u, Delta_zb_u)
    
    for(k = 1; k < Nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < nx; i++){
	  index_ = k*nx*Ny + j*nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_zB_u[index_] = Delta_zB[index_phi];
	  Delta_zb_u[index_] = Delta_zb[index_phi];
	}
      }
    }
    
  } //   if(solve_3D_OK)

  /* NODES */

  for(k = 0; k < Nz; k++){
    for(j = 0; j < Ny; j++){
      for(i = 1; i < nx; i++){
	index_ = k*nx*Ny + j*nx + i;
	nodes_x_u[index_] = nodes_x_u[index_-1] + Delta_xE_u[index_-1];
      }
    }
  }

  for(k = 0; k < Nz; k++){
    for(j = 1; j < Ny; j++){
      for(i = 0; i < nx; i++){
	index_ = k*nx*Ny + j*nx + i;
	nodes_y_u[index_] = nodes_y_u[index_-nx] + Delta_yN_u[index_-nx];
      }
    }
  }

  if(solve_3D_OK){
    
    for(k = 1; k < Nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < nx; i++){
	  index_ = k*nx*Ny + j*nx + i;
	  nodes_z_u[index_] = nodes_z_u[index_-nx*Ny] + Delta_zT_u[index_-nx*Ny];
	}
      }
    }
    
  }

  /* VERTEX */

  for(i = 0; i < nx; i++){
    vertex_x_u[i+1] = vertex_x_u[i] + Delta_x_u[i];
  }

  /* V GRID */

  /* DELTAS*/
  
#pragma omp parallel for default(none) private(i, j, k, index_, index_phi)	\
  shared(Delta_x_v, Delta_y_v, Delta_x, Delta_y)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < ny; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*ny + j*Nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_x_v[index_] = Delta_x[index_phi];
	Delta_y_v[index_] = 0.5 * (Delta_y[index_phi] + Delta_y[index_phi + Nx]);
      }
    }
  }

  progress_msg(wins, "base grid", 50.0);

  if(solve_3D_OK){

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_z, Delta_z_v)
    
    for(k = 0; k < Nz; k++){
      for(j = 0; j < ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_z_v[index_] = Delta_z[index_phi];
	}
      }
    }
    
  }
  
#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_yS_v, Delta_ys_v, Delta_y, Delta_y_v)

  for(k = 0; k < Nz; k++){
    for(j = 1; j < ny; j++){
      for(i = 0; i< Nx; i++){
	index_ = k*Nx*ny + j*Nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_yS_v[index_] = Delta_y[index_phi];
	Delta_ys_v[index_] = 0.5 * Delta_y[index_phi];
      }
    }
  }
     
#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_yN_v, Delta_yn_v, Delta_y, Delta_y_v)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < ny-1; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*ny + j*Nx + i;
	index_phi = k*Nx*Ny + (j + 1)*Nx + i;
	Delta_yN_v[index_] = Delta_y[index_phi];
	Delta_yn_v[index_] = 0.5 * Delta_y[index_phi];
      }
    }
  }
      
#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_xW_v, Delta_xw_v, Delta_xW, Delta_xw)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < ny; j++){
      for(i = 1; i < Nx; i++){
	index_ = k*Nx*ny + j*Nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_xW_v[index_] = Delta_xW[index_phi];
	Delta_xw_v[index_] = Delta_xw[index_phi];
      }
    }
  }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_xE_v, Delta_xe_v, Delta_xE, Delta_xe)

  for(k = 0; k < Nz; k++){
    for(j = 0; j < ny; j++){
      for(i = 0; i < Nx-1; i++){
	index_ = k*Nx*ny + j*Nx + i;
	index_phi = k*Nx*Ny + j*Nx + i;
	Delta_xE_v[index_] = Delta_xE[index_phi];
	Delta_xe_v[index_] = Delta_xe[index_phi];
      }
    }
  }

  if(solve_3D_OK){

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_zT, Delta_zt, Delta_zT_v, Delta_zt_v)
    
    for(k = 0; k < Nz-1; k++){
      for(j = 0; j < ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_zT_v[index_] = Delta_zT[index_phi];
	  Delta_zt_v[index_] = Delta_zt[index_phi];
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_zB, Delta_zb, Delta_zB_v, Delta_zb_v)
    
    for(k = 1; k < Nz; k++){
      for(j = 0; j < ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_zB_v[index_] = Delta_zB[index_phi];
	  Delta_zb_v[index_] = Delta_zb[index_phi];
	}
      }
    }
    
  } // if(solve_3D_OK)

  /* NODES*/

  for(k = 0; k < Nz; k++){
    for(j = 0; j < ny; j++){
      for(i = 1; i < Nx; i++){
	index_ = k*Nx*ny + j*Nx + i;
	nodes_x_v[index_] = nodes_x_v[index_-1] + Delta_xE_v[index_-1];
      }
    }
  }

  for(k = 0; k < Nz; k++){
    for(j = 1; j < ny; j++){
      for(i = 0; i < Nx; i++){
	index_ = k*Nx*ny + j*Nx + i;
	nodes_y_v[index_] = nodes_y_v[index_-Nx] + Delta_yN_v[index_-Nx];
      }
    }
  }

  if(solve_3D_OK){

    for(k = 1; k < Nz; k++){
      for(j = 0; j < ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*ny + j*Nx + i;
	  nodes_z_v[index_] = nodes_z_v[index_-Nx*ny] + Delta_zT_v[index_-Nx*ny];
	}
      }
    }
    
  }

  /* VERTEX */

  for(j = 0; j < ny; j++){
    vertex_y_v[j+1] = vertex_y_v[j] + Delta_y_v[j*Nx];
  }

  progress_msg(wins, "base grid", 70.0);

  
  /* W GRID */

  /* DELTAS */

  if(solve_3D_OK){

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi)	\
  shared(Delta_x, Delta_y, Delta_z, Delta_x_w, Delta_y_w, Delta_z_w)
    
    for(k = 0; k < nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_x_w[index_] = Delta_x[index_phi];
	  Delta_y_w[index_] = Delta_y[index_phi];
	  Delta_z_w[index_] = 0.5 * (Delta_z[index_phi] + Delta_z[index_phi + Nx*Ny]);
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_xW, Delta_xw, Delta_xW_w, Delta_xw_w)

    for(k = 0; k < nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 1; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_xW_w[index_] = Delta_xW[index_phi];
	  Delta_xw_w[index_] = Delta_xw[index_phi];
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_xE, Delta_xe, Delta_xE_w, Delta_xe_w)
    
    for(k = 0; k < nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx-1; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_xE_w[index_] = Delta_xE[index_phi];
	  Delta_xe_w[index_] = Delta_xe[index_phi];
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_yS, Delta_ys, Delta_yS_w, Delta_ys_w)
    
    for(k = 0; k < nz; k++){
      for(j = 1; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_yS_w[index_] = Delta_yS[index_phi];
	  Delta_ys_w[index_] = Delta_ys[index_phi];
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_yN, Delta_yn, Delta_yN_w, Delta_yn_w)
    
    for(k = 0; k < nz; k++){
      for(j = 0; j < Ny-1; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_yN_w[index_] = Delta_yN[index_phi];
	  Delta_yn_w[index_] = Delta_yn[index_phi];
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_z, Delta_zB_w, Delta_zb_w)
    
    for(k = 1; k < nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  index_phi = k*Nx*Ny + j*Nx + i;
	  Delta_zB_w[index_] = Delta_z[index_phi];
	  Delta_zb_w[index_] = 0.5 * Delta_z[index_phi];
	}
      }
    }

#pragma omp parallel for default(none) private(i, j, k, index_, index_phi) \
  shared(Delta_z, Delta_zT_w, Delta_zt_w)
    
    for(k = 0; k < nz-1; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  index_phi = (k+1)*Nx*Ny + j*Nx + i;
	  Delta_zT_w[index_] = Delta_z[index_phi];
	  Delta_zt_w[index_] = 0.5 * Delta_z[index_phi];
	}
      }
    }
    
    
    /* NODES */

    for(k = 0; k < nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 1; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  nodes_x_w[index_] = nodes_x_w[index_-1] + Delta_xE_w[index_-1];
	}
      }
    }

    for(k = 0; k < nz; k++){
      for(j = 1; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  nodes_y_w[index_] = nodes_y_w[index_-Nx] + Delta_yN_w[index_-Nx];
	}
      }
    }

    for(k = 1; k < nz; k++){
      for(j = 0; j < Ny; j++){
	for(i = 0; i < Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  nodes_z_w[index_] = nodes_z_w[index_-Nx*Ny] + Delta_zT_w[index_-Nx*Ny];
	}
      }
    }

    /* VERTEX */

    for(k = 0; k < nz; k++){
      vertex_z_w[k+1] = vertex_z_w[k] + Delta_z_w[k*Nx*Ny];
    }

    
  } //   if(solve_3D_OK)

  /* phi grid faces */

#pragma omp parallel for default(none) private(I, J, K, i, index_, index_face) \
  shared(faces_x, Delta_y, Delta_z)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	I = i + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_face = K*nx*Ny + J*nx + i;
	faces_x[index_face] = Delta_y[index_] * Delta_z[index_];
      }
    }
  }

#pragma omp parallel for default(none) private(I, J, K, j, index_, index_face) \
  shared(faces_y, Delta_x, Delta_z)
  
  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	J = j + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_face = K*Nx*ny + j*Nx + I;
	faces_y[index_face] = Delta_x[index_] * Delta_z[index_];
      }
    }
  }

  if(solve_3D_OK){
  
#pragma omp parallel for default(none) private(I, J, K, k, index_, index_face) \
  shared(faces_z, Delta_x, Delta_y)
  
    for(k=0; k<nz; k++){
      for(J=0; J<Ny; J++){
	for(I=0; I<Nx; I++){
	  K = k + 1;
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_face = k*Nx*Ny + J*Nx + I;
	  faces_z[index_face] = Delta_x[index_] * Delta_y[index_];
	}
      }
    }

  }

  progress_msg(wins, "base grid", 80.0);
  
  /* Building interpolation factors */
  compute_fnsew(data);

  progress_msg(wins, "base grid", 100.0);
  
  return;
}

/* COMPUTE_FNSEW */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) fe = 1 - Delta_xe / Delta_xE

  Array (double, NxNyNz) fw = 1 - Delta_xw / Delta_xW

  Array (double, NxNyNz) fn = 1 - Delta_yn / Delta_yN

  Array (double, NxNyNz) fs = 1 - Delta_ys / Delta_yS

  Array (double, NxNyNz) ft = 1 - Delta_zt / Delta_zT

  Array (double, NxNyNz) fb = 1 - Delta_zb / Delta_zB

  @param data
  
  @return void
  
  MODIFIED: 08-07-2019
*/
void compute_fnsew(Data_Mem *data){
  
  int I, J, K;
  int index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int solve_3D_OK = data->solve_3D_OK;

  double *fe = data->fe;
  double *fw = data->fw;
  double *fn = data->fn;
  double *fs = data->fs;
  double *ft = data->ft;
  double *fb = data->fb;

  const double *Delta_xe = data->Delta_xe;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_yn = data->Delta_yn;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_zt = data->Delta_zt;
  const double *Delta_zb = data->Delta_zb;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zT = data->Delta_zT;
  const double *Delta_zB = data->Delta_zB;

#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(fe, fw, fn, fs,						\
	 Delta_xe, Delta_xw, Delta_yn, Delta_ys,			\
	 Delta_xE, Delta_xW, Delta_yN, Delta_yS) 
 for(J=0; J<Ny; J++){
    for(I=0; I<Nx; I++){
      
      index_ = J*Nx + I;
      
      fe[index_] = 1 - Delta_xe[index_] / Delta_xE[index_];
      fw[index_] = 1 - Delta_xw[index_] / Delta_xW[index_];
      fn[index_] = 1 - Delta_yn[index_] / Delta_yN[index_];
      fs[index_] = 1 - Delta_ys[index_] / Delta_yS[index_];
      
    }

 }
  
  if(solve_3D_OK){
#pragma omp parallel for default(none) private(I, J, K, index_) \
  shared(fe, fw, fn, fs, ft, fb,				\
	 Delta_xe, Delta_xw, Delta_yn, Delta_ys,		\
	 Delta_xE, Delta_xW, Delta_yN, Delta_yS,		\
	 Delta_zt, Delta_zb, Delta_zT, Delta_zB)

    for(K=0; K<Nz; K++){
      for(J=0; J<Ny; J++){
	for(I=0; I<Nx; I++){
	  
	  index_ = K*Nx*Ny + J*Nx + I;

	  fe[index_] = 1 - Delta_xe[index_] / Delta_xE[index_];
	  fw[index_] = 1 - Delta_xw[index_] / Delta_xW[index_];
	  fn[index_] = 1 - Delta_yn[index_] / Delta_yN[index_];
	  fs[index_] = 1 - Delta_ys[index_] / Delta_yS[index_];
	  ft[index_] = 1 - Delta_zt[index_] / Delta_zT[index_];
	  fb[index_] = 1 - Delta_zb[index_] / Delta_zB[index_];
	}
      }
    }
  }
  
  return;

}
