#include "include/some_defs.h"

extern sig_atomic_t action_on_signal_OK;
extern int timer_flag_OK;

/* SOLVE_FLOW_SIMPLES */
/*****************************************************************************/
/**
   
   Solve flow field for 2D case.

   @param data

   @return void
 */
void solve_flow_SIMPLES(Data_Mem *data){
  
  char msg[SIZE];
  
  const char *flow_solver_name = data->flow_solver_name;
  
  /* Auxiliary iteration counter for ADI loop */  
  int iter;
  /* Main loop counter */  
  int outer_iter = 0;
  int convergence_OK = 0;
  int convergence_turb_OK = 0;  

  /* Number of uvp iterations on each main iteration */  
  int iter_uvp;
  int convergence_uvp_OK;

  /* Number of ke iterations on each main iteration */
  int iter_ke;
  int convergence_ke_OK;

  /* Use pseudo-velocities, SIMPLER */  
  int use_ps_velocities_OK;
  
  /* These can be adjusted at interruption signal */
  int it_for_update_coeffs = data->it_for_update_coeffs;
  int it_for_update_k_coeffs = data->it_for_update_k_coeffs;
  int it_for_update_eps_coeffs = data->it_for_update_eps_coeffs;
  int it_upd_coeff_phi = data->it_upd_coeff_phi;
  int max_iter_flow = data->max_iter_flow;
  int max_iter_ADI = data->max_iter_ADI;
  int max_iter_uvp = data->max_iter_uvp;
  int max_iter_ke = data->max_iter_ke;
  int flow_scheme = data->flow_scheme;
  int it_to_start_coupled_physics = data->it_to_start_coupled_physics;

  const int coupled_physics_OK = data->coupled_physics_OK;
  /* Used for correct ncurses display information */
  const int rstride = coupled_physics_OK ? 5 : 2;

  /* Iterations after which a reference residual is calculated
     for uvwp or ke sub-loop */  
  const int uvp_ref_it = 2;
  const int ke_ref_it = 2;

  /* Iterations for reference residual calculation, main loop */  
  const int outer_ref_it = 5;
  /* Iterations for reference residual calculation, ADI loop */  
  const int ref_it = 5;
  const int it_to_start_turb = data->it_to_start_turb;
  const int turb_model = data->turb_model;
  const int it_to_update_y_plus = data->it_to_update_y_plus;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  
  const int animation_flow_OK = data->animation_flow_OK;
  const int animation_flow_update_it = 
    data->animation_flow_update_it;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int res_sigma_iter = data->res_sigma_iter;

  u_int64_t time_i, time_f, timer;

  double res_u = 1, res_v = 1, res_p = 1;
  double res_pcorr = 1, res_c = 1, res_k = 1, res_eps = 1;
  /* Reference residuals at ADI loop level */
  double ref_res_u = 1, ref_res_v = 1, ref_res_p = 1;
  double ref_res_pcorr = 1, ref_res_k = 1, ref_res_eps = 1;
  /* Reference residuals at main loop level */
  double outer_ref_res_u = 1;
  double outer_ref_res_v = 1;
  double outer_ref_res_c = 1;
  double outer_ref_res_k = 1;
  double outer_ref_res_eps = 1;

  /* Reference residuals at sub-loop level */  
  double uvp_ref_res_u = 1, uvp_ref_res_v = 1, uvp_ref_res_c = 1;
  double ke_ref_res_k = 1, ke_ref_res_eps = 1;

  /* Sigmas at sub-loop level */
  double sub_sigmas_v[6];  

  /* These can be adjusted at interruption signal */
  double tol_flow = data->tol_flow;
  double tol_ADI = data->tol_ADI;

  double *sigmas_v = data->sigmas.values;
  double *glob_norm = data->sigmas.glob_norm;

  /**** BEGIN: Coupled physics insert *****/
  /* Pointer to phi coeffs function */
  void (*coeffs_phi)(Data_Mem *);
  
  int convergence_phi_OK = 0, Tol_OK = 0;
  int it_ref_norm_phi;
  int gL_convergence_OK;

  /* These are for a sub-sub cycle for phi (if needed set max_phi_sub_it > 1) */
  int phi_gL_it;
  int max_iter_phi_gL = data->max_sub_it; /* Extracted from phi section in CONFIG file */
  int convergence_phi_gL_OK = 0;

  const int phi_flow_scheme = data->phi_flow_scheme;
  
  const int read_tmp_file_OK = data->read_tmp_file_OK;

  const int interface_flux_update_it =
    data->interface_flux_update_it;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  
  double norm_phi = 1, norm_phi_ref = 1;
  double gL_diff = 1, gL_diff_ref = 1;

  double phi_gL_inner_glob_norm[2] = {0};
  
  const double tol_phi = data->tol_phi;
  const double gL_tol = data->gL_tol;
  
  double *gL = data->gL;
  double *gL_calc = data->gL_calc;
  double *gL_guess = data->gL_guess;
  double *refs = data->sigmas.refs;
  /**** END: Coupled physics insert *****/

  void (*ADI_solver)(double *, Coeffs *, 
		     const int, const int, const int, 
		     ADI_Vars *) = data->ADI_solver;
  
  void (*coeffs_flow_u)(Data_Mem *) = NULL;
  void (*coeffs_flow_v)(Data_Mem *) = NULL;

  Sigmas *sigmas = &(data->sigmas);
  Wins *wins = &(data->wins);

  sigmas->time_f = &(time_f);

  /* UPWIND scheme */
  if(flow_scheme == 0){
    coeffs_flow_u = coeffs_flow_u_upwind;
    coeffs_flow_v = coeffs_flow_v_upwind;
  }
  /* QUICK scheme */
  else if(flow_scheme == 1){
    coeffs_flow_u = coeffs_flow_u_quick;
    coeffs_flow_v = coeffs_flow_v_quick;
  }
  /* van Leer scheme */
  else if(flow_scheme == 2){
    coeffs_flow_u = coeffs_flow_u_TVD;
    coeffs_flow_v = coeffs_flow_v_TVD;
  }

  /**** BEGIN: Coupled physics insert *****/
  if(coupled_physics_OK){
    if(phi_flow_scheme == 1){
      coeffs_phi = coeffs_phi_power_law;
    }
    else if(phi_flow_scheme == 2){
      coeffs_phi = coeffs_phi_TVD;
    }
    else{
      coeffs_phi = coeffs_phi_power_law;
    }
  }
  /**** END: Coupled physics insert *****/
  
  action_on_signal_OK = 0;

  /* SIMPLER */
  if(pv_coupling_algorithm == 2){
    (*coeffs_flow_u)(data);
    (*coeffs_flow_v)(data);
  }

  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  solve_flow_pre_calc_jobs(data);
  
  /**** BEGIN: Coupled physics insert *****/
  if(coupled_physics_OK){
    solve_phi_pre_calc_jobs(data);

    if(read_tmp_file_OK){
      it_ref_norm_phi = -1;
      norm_phi_ref = refs[7];
      gL_diff_ref = refs[8];
    }
    else{
      it_ref_norm_phi = data->it_ref_norm_phi;
    }

    if(solve_phase_change_OK){
      /* gL_diff Max difference between guess and 
	 calculated value of gL */
      gL_diff = 1;
      gL_convergence_OK = 0;
    }
    else{
      gL_convergence_OK = 1;
    }
  }
  /**** END: Coupled physics insert *****/

  /* This is the main (outer) loop */
  while(outer_iter < max_iter_flow && !convergence_OK){
    
    set_mu(data, outer_iter);
    
    /* Setting iteration counter and flag for uvp sub-loop */
    iter_uvp = 0;
    convergence_uvp_OK = 0;

    while(iter_uvp < max_iter_uvp && !convergence_uvp_OK){
      
      set_bc_flow(data);
      
      if(pv_coupling_algorithm == 2){
	use_ps_velocities_OK = 1;
	/* SIMPLER: COMPUTE PSEUDO-VELOCITIES */
	compute_pseudo_velocities(data);
	
	/* SIMPLER: SOLVE PRESSURE EQUATION */
	coeffs_flow_p(data, use_ps_velocities_OK);
	
	for(iter = 0; iter < ref_it; iter++){
	  (*ADI_solver)(data->p,
			&(data->coeffs_p),
			Nx, Ny, 0,
			&(data->ADI_vars));
	}
	ref_res_p = compute_residues(data->p, &(data->coeffs_p), Nx, Ny);
	
	res_p = 1;
	iter = 1;
	while((iter < max_iter_ADI) &&
	      ((res_p / ref_res_p) > tol_ADI)){
	  (*ADI_solver)(data->p,
			&(data->coeffs_p),
			Nx, Ny, 0,
			&(data->ADI_vars));
	  res_p = compute_residues(data->p, &(data->coeffs_p), Nx, Ny);
	  iter++;
	}
      }
      
      /* 1: SOLVE MOMENTUM EQUATIONS */
      
      // u loop
      (*coeffs_flow_u)(data);
      
      for(iter = 0; iter < ref_it; iter++){
	(*ADI_solver)(data->u,
		      &(data->coeffs_u), 
		      nx, Ny, 0, 
		      &(data->ADI_vars));
      }
      ref_res_u = compute_residues(data->u, &(data->coeffs_u), 
				   nx, Ny);
      res_u = 1;
      iter = 1;
      while((iter < max_iter_ADI) && 
	    ((res_u / ref_res_u) > tol_ADI)){
	
	if(iter % it_for_update_coeffs == 0){
	  (*coeffs_flow_u)(data);
	}
	
	(*ADI_solver)(data->u, 
		      &(data->coeffs_u), 
		      nx, Ny, 0, 
		      &(data->ADI_vars));
	res_u = compute_residues(data->u, &(data->coeffs_u), nx, Ny);
	iter++;
      }
      
      // v loop
      (*coeffs_flow_v)(data);
      
      for(iter = 0; iter < ref_it; iter++){
	(*ADI_solver)(data->v, 
		      &(data->coeffs_v), 
		      Nx, ny, 0, 
		      &(data->ADI_vars));
      }
      ref_res_v = compute_residues(data->v, &(data->coeffs_v), 
				   Nx, ny);
      res_v = 1;
      iter = 1;
      while((iter < max_iter_ADI) && 
	    ((res_v / ref_res_v) > tol_ADI)){
	
	if(iter % it_for_update_coeffs == 0){
	  (*coeffs_flow_v)(data);
	}
	
	(*ADI_solver)(data->v, 
		      &(data->coeffs_v), 
		      Nx, ny, 0, 
		      &(data->ADI_vars));
	res_v = compute_residues(data->v, &(data->coeffs_v), Nx, ny);
	iter++;
      }
      
      /* 2: SOLVE PRESSURE CORRECTION EQUATION */
      use_ps_velocities_OK = 0;

      /* To set pressure correction values to zero was found to improve convergence */  
      if(pv_coupling_algorithm != 2){
	fill_array(data->p_corr, Nx, Ny, Nz, 0);
      }

      coeffs_flow_p(data, use_ps_velocities_OK);
      
      for(iter = 0; iter < ref_it; iter++){
	(*ADI_solver)(data->p_corr, 
		      &(data->coeffs_p), 
		      Nx, Ny, 0, 
		      &(data->ADI_vars));
      }
      ref_res_pcorr = compute_residues(data->p_corr, 
				       &(data->coeffs_p), Nx, Ny);
      res_pcorr = 1;
      iter = 1;
      while((iter < max_iter_ADI) && 
	    ((res_pcorr / ref_res_pcorr) > tol_ADI)){
	(*ADI_solver)(data->p_corr, 
		      &(data->coeffs_p), 
		      Nx, Ny, 0, 
		      &(data->ADI_vars));
	res_pcorr = compute_residues(data->p_corr, 
				     &(data->coeffs_p), Nx, Ny);
	iter++;
      }
      
      /* 3: CORRECT PRESSURE AND VELOCITIES */
      update_field(data);

      /* Store values to use with periodic boundary condition, 
	 because p_corr values are set to zero at every iteration */
      store_bnd_p_values(data);

      res_c = continuity_residual(data);
      
      if(iter_uvp == uvp_ref_it){
	uvp_ref_res_u = res_u;
	uvp_ref_res_v = res_v;
	uvp_ref_res_c = res_c;
      }

      /* Store sigmas for sub loop */      
      sub_sigmas_v[0] = res_u / uvp_ref_res_u;
      sub_sigmas_v[1] = res_v / uvp_ref_res_v;
      sub_sigmas_v[3] = res_c / uvp_ref_res_c;

      convergence_uvp_OK = sub_sigmas_v[0] < tol_flow
	&& sub_sigmas_v[1] < tol_flow && sub_sigmas_v[3] < tol_flow
	&& (iter_uvp >= uvp_ref_it);
      
      iter_uvp++;
      
    } //   while(iter_uvp < max_iter_uvp && !convergence_uvp_OK)

    /* 4: SOLVE k-eps EQUATIONS */
    // k-eps loop
    if(turb_model && (outer_iter >= it_to_start_turb)){

      /* Set iteration counter and convergence flag for ke sub-loop */      
      iter_ke = 0;
      convergence_ke_OK = 0;
      
      while(iter_ke < max_iter_ke && !convergence_ke_OK){
	
	/* Filter nodes with low y_plus out of the calculation */
	if(outer_iter % it_to_update_y_plus == 0){
	  filter_y_plus(data);
	}
			
	// k loop
	compute_turb_aux_fields(data);	
	set_coeffs_k(data);
	set_bc_turb_k(data);
	
	for(iter=0; iter<ref_it; iter++){
	  (*ADI_solver)(data->k_turb, &(data->coeffs_k),
			Nx, Ny, 0, &(data->ADI_vars));
	}
	ref_res_k = compute_residues(data->k_turb,
				     &(data->coeffs_k), Nx, Ny);
	res_k = 1;
	iter = 1;
	while((iter < max_iter_ADI) &&
	      ((res_k / ref_res_k) > tol_ADI)){
	  
	  if(iter % it_for_update_k_coeffs == 0){
	    compute_turb_aux_fields(data);
	    set_coeffs_k(data);
	  }
	  
	  (*ADI_solver)(data->k_turb, &(data->coeffs_k),
			Nx, Ny, 0, &(data->ADI_vars));
	  res_k = compute_residues(data->k_turb,
				   &(data->coeffs_k), Nx, Ny);
	  iter++;
	}
	
	filter_small_values_omp(data->k_turb, Nx, Ny, Nz, SMALL_VALUE, 1);
	
	// eps loop
	compute_turb_aux_fields(data);	
	set_coeffs_eps(data);
	set_bc_turb_eps(data);
	
	for(iter=0; iter<ref_it; iter++){
	  (*ADI_solver)(data->eps_turb, &(data->coeffs_eps),
			Nx, Ny, 0, &(data->ADI_vars));
	}
	ref_res_eps = compute_residues(data->eps_turb,
				       &(data->coeffs_eps), Nx, Ny);
	res_eps = 1;
	iter = 1;
	while((iter < max_iter_ADI) &&
	      ((res_eps / ref_res_eps) > tol_ADI)){
	  
	  if(iter % it_for_update_eps_coeffs == 0){
	    compute_turb_aux_fields(data);
	    set_coeffs_eps(data);
	  }
	  
	  (*ADI_solver)(data->eps_turb, &(data->coeffs_eps),
			Nx, Ny, 0, &(data->ADI_vars));
	  res_eps = compute_residues(data->eps_turb,
				     &(data->coeffs_eps), Nx, Ny);
	  iter++;
	}
       
	filter_small_values_omp(data->eps_turb, Nx, Ny, Nz, SMALL_VALUE, 1);
			
	if(iter_ke == ke_ref_it){
	  ke_ref_res_k = res_k;
	  ke_ref_res_eps = res_eps;
	}	
	
	sub_sigmas_v[4] = res_k / ke_ref_res_k;
	sub_sigmas_v[5] = res_eps / ke_ref_res_eps;
	
	convergence_ke_OK = sub_sigmas_v[4] < tol_flow
	  && sub_sigmas_v[5] < tol_flow && (iter_ke > ke_ref_it);
	
	iter_ke++;
      } //     while(iter_ke < max_iter_ke && !convergence_ke_OK){

      /* Compute mu_turb_at velocity nodes
	 for next iteration of the u, v, p cycle */
      compute_mu_turb_at_vel_nodes(data);
      
    } // end if(turb_model && (outer_iter >= it_to_start_turb))

    /* Use coeffs with the latest values of fields to evaluate outer loop convergence */
    (*coeffs_flow_u)(data);
    (*coeffs_flow_v)(data);
    
    res_u = compute_residues(data->u, &(data->coeffs_u), nx, Ny);
    res_v = compute_residues(data->v, &(data->coeffs_v), Nx, ny);
    /* res_c hasn't changed from the one calculated inside uvp sub-loop
       because velocity values are the same */      
      
    if(outer_iter == outer_ref_it){
      outer_ref_res_u = res_u;
      outer_ref_res_v = res_v;
      outer_ref_res_c = res_c;
    }
      
    sigmas_v[0] = res_u / outer_ref_res_u;
    sigmas_v[1] = res_v / outer_ref_res_v;
    sigmas_v[3] = res_c / outer_ref_res_c;

    if(turb_model && (outer_iter >= it_to_start_turb)){

      /* Use coeffs with the latest values of fields to evaluate outer loop convergence */
      compute_turb_aux_fields(data);
      set_coeffs_k(data);
      set_coeffs_eps(data);

      res_k = compute_residues(data->k_turb, &(data->coeffs_k), Nx, Ny);
      res_eps = compute_residues(data->eps_turb, &(data->coeffs_eps), Nx, Ny);      
      
      if(outer_iter == outer_ref_it + it_to_start_turb){
	outer_ref_res_k = res_k;
	outer_ref_res_eps = res_eps;
      }
      
      sigmas_v[4] = res_k / outer_ref_res_k;
      sigmas_v[5] = res_eps / outer_ref_res_eps;    
    
      convergence_turb_OK = sigmas_v[4] < tol_flow
	&& sigmas_v[5] < tol_flow && (outer_iter >= outer_ref_it + it_to_start_turb);
    }
    else{
      convergence_turb_OK = 1;
    }



    /* 5: SOLVE phi-gL EQUATIONS */
    /**** BEGIN: Coupled physics insert *****/

    if(coupled_physics_OK && (outer_iter >= it_to_start_coupled_physics)){

      phi_gL_it = 0;
      convergence_phi_gL_OK = 0;
      
      while(phi_gL_it < max_iter_phi_gL && !convergence_phi_gL_OK){
	
      /* Centering velocities and computing in main nodes 
	 (in turbulence these are not fully performed so it is needed here) */
      center_vel_components(data);
      compute_vel_at_main_cells(data);

      if(outer_iter % it_upd_coeff_phi == 0){
	coeffs_phi(data);
      }
      
      /* Computation of flow at the interfaces 
	 for the conjugate case must be accompanied with
	 an update of coefficients */
      if(outer_iter % interface_flux_update_it == 0){
	coeffs_phi(data);
	compute_phi_SC_at_interface(data);
      }
     
      if(solve_phase_change_OK){
	/* Update phase average properties */
	avg_phys_prop(data);
	/* For steady state the source term is due to phase change is zero
	   compute_enthalpy_diff(data); */
	
	norm_phi = solve_phi_jobs(data);
	
	compute_gL_value(data, gL_calc);
	gL_diff = max_abs_matrix_diff(gL_calc,
				      gL_guess, Nx, Ny);
	update_gL_guess(data);

	if(outer_iter == it_ref_norm_phi){
	  if(gL_diff > 10e-9){
	    gL_diff_ref = gL_diff;
	  }
	  /* Else gL_diff_ref remains equal to 1 and gL_convergence_OK 
	     evaluates to true */
	  else{
	    gL_diff_ref = 1;
	  }

	  refs[8] = gL_diff_ref;
	}

	sigmas_v[8] = gL_diff / gL_diff_ref;
	
	if(sigmas_v[8] < gL_tol){
	  gL_convergence_OK = 1;
	}
	
	if(outer_iter <= it_ref_norm_phi){
	  gL_convergence_OK = 0;
	}

      }
      else{
	norm_phi = solve_phi_jobs(data);
      }
	    
      /* Para adimensionalizar los residuos y poder 
	 compararlos con tol_phi apropiadamente */
      if(outer_iter == it_ref_norm_phi){
	norm_phi_ref = norm_phi;
	refs[7] = norm_phi_ref;
      }
      
      sigmas_v[7] = norm_phi / norm_phi_ref;
      Tol_OK = (sigmas_v[7] <= tol_phi);

      /* Comparison with tolerances must be with 
	 normalized residuals */
      if(outer_iter <= it_ref_norm_phi){
	Tol_OK = 0;
      }
	
      /* Replaced with flow iteration variables criteria */
      if((Tol_OK && gL_convergence_OK) || (outer_iter >= max_iter_flow)){
	convergence_phi_OK = 1;
      }
      
      /* Update gL value */
      if(solve_phase_change_OK){
	copy_array(gL_guess, gL, Nx, Ny, Nz);
      }
      
      /* Setting criteria for exiting the sub (independent) cycle phi_gL */
      phi_gL_inner_glob_norm[0] = compute_norm_glob_res(data->phi, &(data->coeffs_phi), Nx, Ny);
      convergence_phi_gL_OK = phi_gL_inner_glob_norm[0] < tol_phi;
      
      if(solve_phase_change_OK){
	
	phi_gL_inner_glob_norm[1] = compute_avg_gL_diff(data);
	
	convergence_phi_gL_OK = convergence_phi_gL_OK && phi_gL_inner_glob_norm[1] < gL_tol;
      }
      
      phi_gL_it++;
      
      } // end while(phi_gL_it < max_iter_phi_gL && !convergence_phi_gL_OK)
    } // end if coupled_physics_OK
    
    /**** END: Coupled physics insert *****/


    
    /* Main convergence flag */
    convergence_OK =  sigmas_v[0] < tol_flow
      && sigmas_v[1] < tol_flow && sigmas_v[3] < tol_flow
      && convergence_turb_OK && (outer_iter > outer_ref_it);

    /**** BEGIN: Coupled physics insert *****/
    if(coupled_physics_OK && (outer_iter >= it_to_start_coupled_physics)){
      convergence_OK = convergence_OK && convergence_phi_OK;
    }
    /**** END: Coupled physics insert *****/
    
    
    /* Here process action upon signal raise */
    if(action_on_signal_OK){
      process_action_on_signal(data, &convergence_OK, &convergence_OK);

      flow_scheme = data->flow_scheme;
      /* UPWIND scheme */
      if(flow_scheme == 0){
	coeffs_flow_u = coeffs_flow_u_upwind;
	coeffs_flow_v = coeffs_flow_v_upwind;
      }
      /* QUICK scheme */
      else if(flow_scheme == 1){
	coeffs_flow_u = coeffs_flow_u_quick;
	coeffs_flow_v = coeffs_flow_v_quick;
      }
      /* van Leer scheme */
      else if(flow_scheme == 2){
	coeffs_flow_u = coeffs_flow_u_TVD;
	coeffs_flow_v = coeffs_flow_v_TVD;
      }
      
      /* Update in case of modification the new values */
      max_iter_flow = data->max_iter_flow;
      max_iter_ADI = data->max_iter_ADI;
      it_for_update_coeffs = data->it_for_update_coeffs;
      max_iter_uvp = data->max_iter_uvp;

      it_for_update_k_coeffs = data->it_for_update_k_coeffs;
      it_for_update_eps_coeffs = data->it_for_update_eps_coeffs;
      max_iter_ke = data->max_iter_ke;

      it_upd_coeff_phi = data->it_upd_coeff_phi;
      max_iter_phi_gL = data->max_sub_it;
      
      it_to_start_coupled_physics = data->it_to_start_coupled_physics;
      
      action_on_signal_OK = 0;
    }

    time_i = time_f;		
    time_f = time_f + elapsed_time(&timer);
    
    if(outer_iter % res_sigma_iter == 0){

      /* Leave outside this IF clause if you need it for convergence criteria
	 Otherwise leave here for optimization purposes */
      sigmas_v[6] = mass_io_balance(data);
      
      sigmas->it = outer_iter;
      sigmas->sub_it = outer_iter;

      glob_norm[0] = compute_norm_glob_res(data->u, &(data->coeffs_u), nx, Ny);
      glob_norm[1] = compute_norm_glob_res(data->v, &(data->coeffs_v), Nx, ny);
      glob_norm[2] = NAN;
      
      if(turb_model && (outer_iter >= it_to_start_turb)){
	glob_norm[4] = compute_norm_glob_res(data->k_turb, &(data->coeffs_k), Nx, Ny);
	glob_norm[5] = compute_norm_glob_res(data->eps_turb, &(data->coeffs_eps), Nx, Ny);
      }
      
      print_flow_it_info(msg, SIZE, rstride,
			 flow_solver_name, outer_iter, max_iter_flow,
			 time_i, time_f, turb_model, sigmas,
			 wins);

      /**** BEGIN: Coupled physics insert *****/
      if(coupled_physics_OK){
	table_header_phi_it(wins);
	print_phi_it_info(outer_iter, max_iter_flow,
			  time_i, time_f,
			  sigmas,
			  wins);
      }      
      /**** END: Coupled physics insert *****/
      
      update_graph_window(data);
      
      write_sfile(data);
      
#ifdef AVGIV_OK
      write_in_vel(data);
#endif
    }
    
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);

      memset(msg, '\0', sizeof(msg));
      snprintf(msg, SIZE, 
	       "flow: it = %7d; "
	       "su = %9.3e; sv = %9.3e, sw = %9.3e; sc = %9.3e; sk = %9.3e; se = %9.3e; sMB = %9.3e", 
	       outer_iter,
	       sigmas_v[0], sigmas_v[1], sigmas_v[2],
	       sigmas_v[3], sigmas_v[4], sigmas_v[5], sigmas_v[6]);

      sigmas->it = outer_iter;
      write_to_log_file(data, msg);

      /**** BEGIN: Coupled physics insert *****/
      if(coupled_physics_OK){
	memset(msg, '\0', sizeof(msg));
	snprintf(msg, SIZE, 
		 "phi: it = "
		 "%7d; sphi = %9.3e; sgL = %9.3e", 
		 outer_iter, sigmas_v[7], sigmas_v[8]);

	sigmas->it = outer_iter;
	write_to_log_file(data, msg);
      }
      /**** END: Coupled physics insert *****/
      
      timer_flag_OK = 0;
    }
    
    /* Updating the server */
    if((launch_server_OK) && 
       (outer_iter % server_update_it == 0)){
      update_server(data);
    }
    
    /* Updating the animation */
    if((animation_flow_OK) && 
       (outer_iter % animation_flow_update_it == 0)){
      /* Computing U, V with velocities centered at uncut faces */
      center_vel_components(data);
      compute_vel_at_main_cells(data);
      update_anims((void *) data);
    }
    
    outer_iter++;
    
  } // end while(outer_iter < max_iter_flow && !convergence_OK){

  /* Save iteration */
  sigmas->flow_it = outer_iter - 1;
  /**** BEGIN: Coupled physics insert *****/
  if(coupled_physics_OK){
    sigmas->phi_it = outer_iter - 1;
  }
  /**** END: Coupled physics insert *****/

#ifdef AVGIV_OK
  close_in_vel(data);
#endif

  solve_flow_post_calc_jobs(data);
  /**** BEGIN: Coupled physics insert *****/
  if(coupled_physics_OK){
    solve_phi_post_calc_jobs(data);
    close_sfile(data); // Chequear si está obsoleto esto
  }
  /**** END: Coupled physics insert *****/

  /* Print normalized global residuals */
  
  write_gnres_sfile(data);
  
  return;

}

/* COMPUTE_PSEUDO_VELOCITES */
/*****************************************************************************/
/**
  
  Sets:

  Array (nx, Ny) Data_Mem::u_ps 

  Array (Nx, ny) Data_Mem::v_ps

  @param data
  
  @return ret_value not implemented.
  
  MODIFIED: 30-05-2019
*/
void compute_pseudo_velocities(Data_Mem *data){

  int I, J, i, j;
  int index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  
  double *u_ps = data->u_ps;
  double *v_ps = data->v_ps;
  
  /* U */
  const double *u = data->u;
  const double *aP_u = data->coeffs_u.aP;
  const double *aW_u = data->coeffs_u.aW;
  const double *aE_u = data->coeffs_u.aE;
  const double *aS_u = data->coeffs_u.aS;
  const double *aN_u = data->coeffs_u.aN;
  const double *b_u = data->coeffs_u.b;

  /* V */
  const double *v = data->v;
  const double *aP_v = data->coeffs_v.aP;
  const double *aW_v = data->coeffs_v.aW;
  const double *aE_v = data->coeffs_v.aE;
  const double *aS_v = data->coeffs_v.aS;
  const double *aN_v = data->coeffs_v.aN;
  const double *b_v = data->coeffs_v.b;
  
  /* U */
#pragma omp parallel for default(none) private(i, J, index_u)	\
  shared(u, u_ps, aE_u, aW_u, aN_u, aS_u, b_u, aP_u)
  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){
      index_u = J*nx + i;
      u_ps[index_u] = (aE_u[index_u] * u[index_u+1] + 
		       aW_u[index_u] * u[index_u-1] +
		       aN_u[index_u] * u[index_u+nx] + 
		       aS_u[index_u] * u[index_u-nx] +
		       b_u[index_u]) / aP_u[index_u];
    }
  }

  /* V */
#pragma omp parallel for default(none) private(I, j, index_v)	\
  shared(v, v_ps, aE_v, aW_v, aN_v, aS_v, b_v, aP_v)
  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){
      index_v = j*Nx + I;
      v_ps[index_v] = (aE_v[index_v] * v[index_v+1] + 
		       aW_v[index_v] * v[index_v-1] +
		       aN_v[index_v] * v[index_v+Nx] + 
		       aS_v[index_v] * v[index_v-Nx] +
		       b_v[index_v]) / aP_v[index_v];
    }
  }

  return;
}

/* COEFFS_FLOW_P */
/*****************************************************************************/
/**

  Sets:

  Structure coeffs_p: coefficients of pressure or pressure correction equation.

  @param data

  @param use_ps_velocities_OK

  @return ret_value not implemented.
  
  MODIFIED: 30-05-2019
*/
void coeffs_flow_p(Data_Mem *data, const int use_ps_velocities_OK){

  int i, j, I, J;
  int index_, index_u, index_v;
  int use_p_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int set_ref_p_node = data->set_ref_p_node;
  const int ref_p_node_index = data->ref_p_node_index;

  const int *p_dom_matrix = data->p_dom_matrix;

  const double rho = data->rho_v[0];
  const double alpha_p_coeff = data->alpha_p_coeff;
  const double ref_p_value = data->ref_p_value;

  double *p_aP = data->coeffs_p.aP;
  double *p_aW = data->coeffs_p.aW;
  double *p_aE = data->coeffs_p.aE;
  double *p_aS = data->coeffs_p.aS;
  double *p_aN = data->coeffs_p.aN;
  double *p_b = data->coeffs_p.b;
  double *u_aP_SIMPLEC = data->u_aP_SIMPLEC;
  double *v_aP_SIMPLEC = data->v_aP_SIMPLEC;

  const double *u_aP = data->coeffs_u.aP;
  const double *u_aE = data->coeffs_u.aE;
  const double *u_aW = data->coeffs_u.aW;
  const double *u_aN = data->coeffs_u.aN;
  const double *u_aS = data->coeffs_u.aS;

  const double *v_aP = data->coeffs_v.aP;
  const double *v_aE = data->coeffs_v.aE;
  const double *v_aW = data->coeffs_v.aW;
  const double *v_aN = data->coeffs_v.aN;
  const double *v_aS = data->coeffs_v.aS;

  const double *u = data->u;
  const double *v = data->v;

  const double *u_ps = data->u_ps;
  const double *v_ps = data->v_ps;

  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;

  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;

  const double *p_W_non_uni = data->p_W_non_uni;
  const double *p_E_non_uni = data->p_E_non_uni;
  const double *p_S_non_uni = data->p_S_non_uni;
  const double *p_N_non_uni = data->p_N_non_uni;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);

  /* SIMPLEC */
  if(pv_coupling_algorithm == 3){

#pragma omp parallel for default(none) private(i, J, index_u)	\
  shared(u_aP_SIMPLEC, u_aP, u_aE, u_aW, u_aN, u_aS)
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	
	index_u = J*nx + i;

	u_aP_SIMPLEC[index_u] = u_aP[index_u] - 
	  (u_aE[index_u] + u_aW[index_u] + 
	   u_aN[index_u] + u_aS[index_u]);
      }
    }
    
#pragma omp parallel for default(none) private(I, j, index_v)	\
  shared(v_aP_SIMPLEC, v_aP, v_aE, v_aW, v_aN, v_aS)
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	
	index_v = j*Nx + I;
	
	v_aP_SIMPLEC[index_v] = v_aP[index_v] - 
	  (v_aE[index_v] + v_aW[index_v] + 
	   v_aN[index_v] + v_aS[index_v]);
	
      }
    }
    
    /* Switching the pointer to the modified coefficients */
    u_aP = u_aP_SIMPLEC;
    v_aP = v_aP_SIMPLEC;

  }

  /* SIMPLER */
  if(use_ps_velocities_OK){

#pragma omp parallel for default(none) private(I, J, i, j, index_, index_u, index_v) \
  shared(p_dom_matrix, p_b, p_faces_x, p_faces_y, u_ps, v_ps)
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = J*Nx + I;

	if(p_dom_matrix[index_] == 0){

	  i = I-1;
	  j = J-1;
	  
	  index_u = J*nx + i;
	  index_v = j*Nx + I;	
	  p_b[index_] = 
	    rho * (p_faces_x[index_u] * u_ps[index_u] - 
		   p_faces_x[index_u+1] * u_ps[index_u+1] + 
		   p_faces_y[index_v] * v_ps[index_v] - 
		   p_faces_y[index_v+Nx] * v_ps[index_v+Nx]);
	  
	}

      }
    }
	  
  }
  else{
    /* SIMPLE */

#pragma omp parallel for default(none) private(I, J, i, j, index_, index_u, index_v) \
  shared(p_dom_matrix, p_b, p_faces_x, p_faces_y, u, v)
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = J*Nx + I;
	
	if(p_dom_matrix[index_] == 0){

	  i = I-1;
	  j = J-1;

	  index_u = J*nx + i;
	  index_v = j*Nx + I;

	  p_b[index_] = 
	    rho * (p_faces_x[index_u] * u[index_u] - 
		   p_faces_x[index_u+1] * u[index_u+1] +
		   p_faces_y[index_v] * v[index_v] - 
		   p_faces_y[index_v+Nx] * v[index_v+Nx]);

	}

      }
    }
    
  }

  /* COMMON */
#pragma omp parallel for default(none) private(I, J, i, j, index_, index_u, index_v) \
  shared(p_dom_matrix, p_aE, p_aW, p_aN, p_aS, p_b, p_aP, p_faces_x, p_faces_y, \
	 u_faces_x, v_faces_y, u_aP, v_aP)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      
      if(p_dom_matrix[index_] == 0){

	i = I-1;
	j = J-1;

	index_u = J*nx + i;
	index_v = j*Nx + I;	
		
	p_aE[index_] = p_faces_x[index_u+1] * rho * 
	  u_faces_x[index_+1] / u_aP[index_u+1];
	p_aW[index_] = p_faces_x[index_u] * rho * 
	  u_faces_x[index_-1] / u_aP[index_u];
	
	p_aN[index_] = p_faces_y[index_v+Nx] * rho * 
	  v_faces_y[index_+Nx] / v_aP[index_v+Nx];
	p_aS[index_] = p_faces_y[index_v] * rho * 
	  v_faces_y[index_-Nx] / v_aP[index_v];

	p_aP[index_] = 
	  p_faces_x[index_u+1] * rho * 
	  u_faces_x[index_] / u_aP[index_u+1] +
	  p_faces_x[index_u] * rho * 
	  u_faces_x[index_] / u_aP[index_u] +
	  p_faces_y[index_v+Nx] * rho * 
	  v_faces_y[index_] / v_aP[index_v+Nx] +
	  p_faces_y[index_v] * rho * 
	  v_faces_y[index_] / v_aP[index_v];
	
      }
      else{
		  
	p_aP[index_] = 1;
	p_b[index_] = 0;
	
      }
      
    }
  }

  /*
  // Slave nodes

  This correction consider u_aP, v_aP from SIMPLE, can't be applied because for
  slave cells u_aP, v_aP = 1 from coeffs_flow function.

  for(k=0; k<u_slave_count; k++){

  i = u_slave_i[k];
  J = u_slave_J[k];
  I = i+1;

  index_ = J*Nx+I;
  index_u = u_slave_nodes[k];

  // Pressure node is at west position relative to slave node
  if(u_slave_at_E_of_p_OK[k]){
  p_aP[index_-1] = p_aP[index_-1] - 
  p_faces_x[index_u] * rho * 
  u_faces_x[index_-1]/u_aP[index_u];
  p_aE[index_-1] = 0;
  }
  // Pressure node is at east position relative to slave node
  else{
  p_aP[index_] = p_aP[index_] - p_faces_x[index_u] * rho * 
  u_faces_x[index_]/u_aP[index_u];
  p_aW[index_] = 0;
  }
  }

  for(k=0; k<v_slave_count; k++){

  I = v_slave_I[k];
  j = v_slave_j[k];
  J = j+1;

  index_ = J*Nx+I;
  index_v = v_slave_nodes[k];

  // Pressure node is at south position relative to slave node
  if(v_slave_at_N_of_p_OK[k]){
  p_aP[index_-Nx] = p_aP[index_-Nx] - 
  p_faces_y[index_v] * rho * 
  v_faces_y[index_-Nx]/v_aP[index_v];
  p_aN[index_-Nx] = 0;
  }
  // Pressure node is at north position relative to slave node
  else{
  p_aP[index_] = p_aP[index_] - p_faces_y[index_v] * rho * 
  v_faces_y[index_]/v_aP[index_v];
  p_aS[index_] = 0;
  }
  }

  */

  /* Here boundaries per se */
#pragma omp parallel sections default(none) private(I, J, index_)	\
  shared(p_dom_matrix, p_aP, p_aW, p_aE, p_aS, p_aN, p_b)
 
  {

#pragma omp section 
    {
      /* Nodos sur */
      J = 0;

      for(I=1; I<Nx-1; I++){
	index_ = J*Nx+I;
	p_aP[index_] = 1;
	if(p_dom_matrix[index_+Nx] == 0){
	  p_aN[index_] = 1;
	}
	else{
	  p_b[index_] = 0;
	}
      }
    }

#pragma omp section 
    {
      /* Nodos norte */
      J = Ny-1;

      for(I=1; I<Nx-1; I++){
	index_ = J*Nx+I;
	p_aP[index_] = 1;
	if(p_dom_matrix[index_-Nx] == 0){
	  p_aS[index_] = 1;
	}
	else{
	  p_b[index_] = 0;
	}
      }
    }

#pragma omp section 
    {
      /* Nodos oeste */
      I = 0;

      for(J=1; J<Ny-1; J++){
	index_ = J*Nx+I;
	p_aP[index_] = 1;
	if(p_dom_matrix[index_+1] == 0){
	  p_aE[index_] = 1;
	}
	else{
	  p_b[index_] = 0;
	}
      }
    }

#pragma omp section 
    {
      /* Nodos este */
      I = Nx-1;

      for(J=1; J<Ny-1; J++){
	index_ = J*Nx + I;
	p_aP[index_] = 1;
	if(p_dom_matrix[index_-1] == 0){
	  p_aW[index_] = 1;
	}
	else{
	  p_b[index_] = 0;
	}
      }
    }
  } // sections

  /* Nodos adyacentes a la frontera sur */
  J = 1;
  j = J-1;

 
  /* Constant pressure boundary condition */
  if(bc_flow_south->type == 4){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){
	/* SIMPLER */
	if(pv_coupling_algorithm == 2){
	  p_b[index_] = bc_flow_south->p_value;
	}
	/* SIMPLE, SIMPLE_CANG, SIMPLEC */
	else{
	  p_b[index_] = 0;
	}
      }
      /* Pressure node is not solved */
      else{
	p_b[index_] = 0;
      }
    }
  }
  /* Periodic boundary */
  else if(bc_flow_south->type == 5){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){
	p_b[index_] = p_S_non_uni[I];
      }
      // Pressure node is not solved 
      else{
	p_b[index_] = 0;
      }
    }
  }
  else{
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;

      if(p_dom_matrix[index_] == 0){
	index_v = j*Nx + I;
	p_aP[index_] = p_aP[index_] - 
	  p_faces_y[index_v] * rho * 
	  v_faces_y[index_] / v_aP[index_v];
	p_aS[index_] = 0;
      }
      /* Pressure node is not solved */
      else{
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_b[index_] = 0;
      }
    }
  }
  
  /* Nodos adyacentes a la frontera norte */
  J = Ny-2;
  j = J-1;

  if(bc_flow_north->type == 4){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){

	if(pv_coupling_algorithm == 2){
	  p_b[index_] = bc_flow_north->p_value;
	}
	else{
	  p_b[index_] = 0;
	}
      }
      else{
	p_b[index_] = 0;
      }
    }
  }
  else if(bc_flow_north->type == 5){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){
	p_b[index_] = p_N_non_uni[I];
      }
      else{
	p_b[index_] = 0;
      }
    }
  }
  else{
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;

      if(p_dom_matrix[index_] == 0){
	index_v = j*Nx + I;
	p_aP[index_] = p_aP[index_] - 
	  p_faces_y[index_v+Nx] * rho * 
	  v_faces_y[index_] / v_aP[index_v+Nx];
	p_aN[index_] = 0;
      }
      else{
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_b[index_] = 0;
      }
    }
  }

  /* Nodos adyacentes a la frontera oeste */
  I = 1;
  i = I-1;

  if(bc_flow_west->type == 4){
    for(J=1; J<Ny-1; J++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){

	if(pv_coupling_algorithm == 2){
	  p_b[index_] = bc_flow_west->p_value; 
	}
	else{
	  p_b[index_] = 0;
	}
      }
      else{
	p_b[index_] = 0;
      }
    }
  }
  else if(bc_flow_west->type == 5){
    for(J=1; J<Ny-1; J++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){
	p_b[index_] = p_W_non_uni[J];
      }
      else{
	p_b[index_] = 0;
      }
    }
  }
  else{
    for(J=1; J<Ny-1; J++){

      index_ = J*Nx + I;

      if(p_dom_matrix[index_] == 0){
	index_u = J*nx + i;
	p_aP[index_] = p_aP[index_] - 
	  p_faces_x[index_u] * rho * 
	  u_faces_x[index_] / u_aP[index_u];
	p_aW[index_] = 0;
      }
      else{
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_b[index_] = 0;
      }
    }
  }

  /* Nodos adyacentes a la frontera este */
  I = Nx-2;
  i = I-1;

  if(bc_flow_east->type == 4){
    for(J=1; J<Ny-1; J++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){

	if(pv_coupling_algorithm == 2){
	  p_b[index_] = bc_flow_east->p_value;
	}
	else{
	  p_b[index_] = 0;
	}
      }
      else{
	p_b[index_] = 0;
      }      
    }
  }
  else if(bc_flow_east->type == 5){
    for(J=1; J<Ny-1; J++){

      index_ = J*Nx + I;
      p_aP[index_] = 1;
      p_aW[index_] = 0;
      p_aE[index_] = 0;
      p_aS[index_] = 0;
      p_aN[index_] = 0;

      if(p_dom_matrix[index_] == 0){
	p_b[index_] = p_E_non_uni[J];
      }
      else{
	p_b[index_] = 0;
      }
    }
  }
  else{
    for(J=1; J<Ny-1; J++){

      index_ = J*Nx + I;

      if(p_dom_matrix[index_] == 0){
	index_u = J*nx + i;
	p_aP[index_] = p_aP[index_] - 
	  p_faces_x[index_u+1] * rho * 
	  u_faces_x[index_] / u_aP[index_u+1];
	p_aE[index_] = 0;
      }
      else{
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_b[index_] = 0;
      }
    }
  }


  /* Check for a reference pressure node */
  /* index of the reference pressure node identified on set_p_ref_node function */

  if(set_ref_p_node){
    p_aP[ref_p_node_index] = 1;
    p_aW[ref_p_node_index] = 0;
    p_aE[ref_p_node_index] = 0;
    p_aS[ref_p_node_index] = 0;
    p_aN[ref_p_node_index] = 0;
    /* SIMPLER */
    if(pv_coupling_algorithm == 2){
      p_b[ref_p_node_index] = ref_p_value;
    }
    /* SIMPLE, SIMPLE_CANG, SIMPLEC */
    else{
      p_b[ref_p_node_index] = 0;
    }
  }

  /* Here we relax coefficients with alpha_p in the same way
     that with other coefficients */
  
  if(alpha_p_coeff != 1){

    if(pv_coupling_algorithm == 1){
      /* We are using SIMPLE_cang
	 set relaxation for pcorr */
      use_p_OK = 0;
    }
    else if(pv_coupling_algorithm == 2){
      /* We are using SIMPLER
	 set relaxation for p or pcorr alternatively
	 by using flag use_ps_velocities_OK */
      use_p_OK = use_ps_velocities_OK;
    }

    if(use_p_OK){
      relax_coeffs(data->p, p_dom_matrix, 0, data->coeffs_p, Nx, Ny, data->alpha_p_coeff);
    }
    else{
      relax_coeffs(data->p_corr, p_dom_matrix, 0, data->coeffs_p, Nx, Ny, data->alpha_p_coeff);
    }
  }

  return;
  
}

/* UPDATE_FIELD */
/*****************************************************************************/
/**
  
  Sets:

  Array (nx, Ny) Data_Mem::u: update values using pressure correction.

  Array (Nx, ny) Data_Mem::v: update values using pressure correction.

  Array (Nx, Ny) Data_Mem::p: update values using pressure correction.

  @param data
  
  @return ret_value not implemented.
  
  MODIFIED: 21-11-2019
*/  
void update_field(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;

  int set_corner_null_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;

  const int u_slave_count = data->u_slave_count;
  const int v_slave_count = data->v_slave_count;
  const int bc_west_type = data->bc_flow_west.type;
  const int bc_east_type = data->bc_flow_east.type;
  const int bc_north_type = data->bc_flow_north.type;
  const int bc_south_type = data->bc_flow_south.type;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *v_slave_nodes = data->v_slave_nodes;

  double alpha_u;
  double alpha_v;

  const double alpha_p_corr = data->alpha_p_corr;

  double *u = data->u;
  double *v = data->v;
  double *p = data->p;

  const double *p_corr = data->p_corr;

  const double *u_aP = data->coeffs_u.aP;
  const double *v_aP = data->coeffs_v.aP;

  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *fn_u = data->fn_u;
  const double *fe_v = data->fe_v;

  /* pv_coupling_algorithm == 0, SIMPLE */
  if(pv_coupling_algorithm == 0){
    alpha_u = data->alpha_u;
    alpha_v = data->alpha_v;
  }
  else{
    alpha_u = 1;
    alpha_v = 1;
  }

  /* U */
#pragma omp parallel for default(none) private(I, J, i, index_, index_u) \
  shared(u_dom_matrix, u, p_corr, u_faces_x, u_aP, alpha_u)
  
  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      index_u = J*nx + i;

      if(u_dom_matrix[index_u] == 0){

	I = i+1;
	index_ = J*Nx + I;

	u[index_u] += alpha_u * (p_corr[index_-1] * u_faces_x[index_-1] - 
		   p_corr[index_] * u_faces_x[index_]) / u_aP[index_u];
      }
    }
  }

  /* V */
#pragma omp parallel for default(none) private(I, J, j, index_, index_v) \
  shared(v_dom_matrix, v, p_corr, v_faces_y, v_aP, alpha_v)
  
  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){

      index_v = j*Nx + I;

      if(v_dom_matrix[index_v] == 0){

	J = j+1;
	index_ = J*Nx + I;

	v[index_v] += alpha_v * (p_corr[index_-Nx] * v_faces_y[index_-Nx] - 
		   p_corr[index_] * v_faces_y[index_]) / v_aP[index_v];
      }
    }
  }

  /* Boundary nodes */

#pragma omp parallel sections default(none) private(I, J, i, j, index_u, index_v) \
  shared(u, v, fn_u, fe_v)

  {

#pragma omp section
    {
      /* South */
      /* Needed to maintain equality of velocity values imposed by boundary conditions,
	 the previous loop only updated interior values */
      if((bc_south_type == 2) || (bc_south_type == 0) 
	 || (bc_south_type == 4) || (bc_south_type == 5)){
	J = 0;
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  u[index_u] = u[index_u+nx];
	}
	if(bc_south_type == 0){
	  j = 0;
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    v[index_v] = v[index_v+Nx];
	  }
	}
	if(bc_south_type == 5){
	  j = 0;
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    v[index_v] = v[(ny-2)*Nx + I];
	  }

	  J = 0;
	  for(i=1; i<nx-1; i++){
	    index_u = J*nx + i;
	    u[index_u] = u[(Ny-3)*nx + i] * fn_u[(Ny-3)*nx + i] +
	      u[(Ny-2)*nx + i] * (1 - fn_u[(Ny-3)*nx + i]);
	  }
	}
      }
    } /* South section */ 

#pragma omp section 
    {
      /* North */
      if((bc_north_type == 2) || (bc_north_type == 0)
	 || (bc_north_type == 4) || (bc_north_type == 5)){
	J = Ny-1;
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  u[index_u] = u[index_u-nx];
	}
	if(bc_north_type == 0){
	  j = ny-1;
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    v[index_v] = v[index_v-Nx];
	  }
	}
	if(bc_north_type == 5){
	  j = ny - 1;
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    v[index_v] = v[Nx + I];
	  }

	  J = Ny - 1;
	  for(i=1; i<nx-1; i++){
	    index_u = J*nx + i;
	    u[index_u] = u[nx + i] * fn_u[nx + i] +
	      u[2*nx + i] * (1 - fn_u[nx + i]);
	  }
	}
      }
    } /* North section */

#pragma omp section
    {
      /* West */
      if((bc_west_type == 2) || (bc_west_type == 0)
	 || (bc_west_type == 4) || (bc_west_type == 5)){
	I = 0;
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  v[index_v] = v[index_v+1];
	}
	if(bc_west_type == 0){
	  i = 0;
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    u[index_u] = u[index_u+1];
	  }
	}
	if(bc_west_type == 5){
	  i = 0;
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    u[index_u] = u[J*nx + nx-2];
	  }

	  I = 0;
	  for(j=1; j<ny-1; j++){
	    index_v = j*Nx + I;
	    v[index_v] = v[j*Nx + Nx-3] * fe_v[j*Nx + Nx-3] + 
	      v[j*Nx + Nx-2] * (1 - fe_v[j*Nx + Nx-3]);
	  }
	}
      }
    } /* West section */

#pragma omp section
    {
      /* East */
      if((bc_east_type == 2) || (bc_east_type == 0)
	 || (bc_east_type == 4) || (bc_east_type == 5)){
	I = Nx-1;
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  v[index_v] = v[index_v-1];
	}
	if(bc_east_type == 0){
	  i = nx - 1;
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    u[index_u] = u[index_u-1];
	  }
	}
	if(bc_east_type == 5){
	  i = nx - 1;
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    u[index_u] = u[J*nx + 1];
	  }

	  I = Nx - 1;
	  for(j=1; j<ny-1; j++){
	    index_v = j*Nx + I;
	    v[index_v] = v[j*Nx + 1] * fe_v[j*Nx + 1] + 
	      v[j*Nx + 2] * (1 - fe_v[j*Nx + 1]);
	  }
	}
      }
    } /* East section */
  } /* sections */

  /* Update slave nodes */
  for(i=0; i<u_slave_count; i++){
    u[ u_slave_nodes[i] ] = 0;
  }
  for(j=0; j<v_slave_count; j++){
    v[ v_slave_nodes[j] ] = 0;
  }

  /* Arbitrary assignment of corner values, necessary to avoid problems
     when computing derivatives in calculating turbulence */
  
  /* West-South */
  set_corner_null_OK =
    bc_west_type == 3 || bc_south_type == 3;

  if(set_corner_null_OK){
    u[0] = 0;
    v[0] = 0;
  }
  else{
    u[0] = 0.5 * (u[1] + u[nx]);
    v[0] = 0.5 * (v[1] + v[Nx]);
  }

  /* West-North */
  set_corner_null_OK =
    bc_west_type == 3 || bc_north_type == 3;

  if(set_corner_null_OK){
    u[(Ny-1) * nx] = 0;
    v[(ny-1) * Nx] = 0;
  }
  else{
    u[(Ny-1) * nx] = 0.5 * (u[(Ny-1) * nx + 1] + u[(Ny-2) * nx]);
    v[(ny-1) * Nx] = 0.5 * (v[(ny-1) * Nx + 1] + v[(ny-2) * Nx]);
  }

  /* East-South */
  set_corner_null_OK =
    bc_east_type == 3 || bc_south_type == 3;

  if(set_corner_null_OK){
    u[nx-1] = 0;
    v[Nx-1] = 0;
  }
  else{
    u[nx-1] = 0.5 * (u[nx-2] + u[2*nx - 1]);
    v[Nx-1] = 0.5 * (v[Nx-2] + v[2*Nx - 1]);
  }

  /* East-North */
  set_corner_null_OK =
    bc_east_type == 3 || bc_north_type == 3;

  if(set_corner_null_OK){
    u[nx*Ny - 1] = 0;
    v[Nx*ny - 1] = 0;
  }
  else{
    u[nx*Ny - 1] = 0.5 * (u[nx*Ny - 2] + u[nx*Ny - nx - 1]);
    v[Nx*ny - 1] = 0.5 * (v[Nx*ny - 2] + v[Nx*ny - Nx - 1]);
  }

  /* UPDATE PRESSURE ONLY FOR SIMPLE, SIMPLE_CANG and SIMPLEC */
  /* P */
  if(pv_coupling_algorithm != 2){
    
#pragma omp parallel for default(none)		\
  private(I, J, index_) shared(p, p_corr)
    
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_ = J*Nx + I;
	
	p[index_] += alpha_p_corr * p_corr[index_];
      }
    }
  }
  
  return;
}

/* SET_BC_FLOW */
/*****************************************************************************/
/**
   Sets:

   Structure coeffs_u: Set values at boundary according to specified boundary condition.

   Structure coeffs_v: Set values at boundary according to specified boundary condition.
   
   @param data
   @return ret_value not implemented.
  
   MODIFIED: 15-02-2019
*/
void set_bc_flow(Data_Mem *data){

  int I, J, i, j;
  int index_u, index_v;
 
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
 
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  
  double *aP_u = data->coeffs_u.aP;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *b_u = data->coeffs_u.b;

  double *aP_v = data->coeffs_v.aP;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *b_v = data->coeffs_v.b;

  const double *u_E_non_uni = data->u_E_non_uni;
  const double *u_W_non_uni = data->u_W_non_uni;
  const double *v_N_non_uni = data->v_N_non_uni;
  const double *v_S_non_uni = data->v_S_non_uni;
  const double *u = data->u;
  const double *v = data->v;
  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;
  const double *fn_u = data->fn_u;
  const double *fe_v = data->fe_v;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);

  /* U */

#pragma omp parallel sections default(none) private(I, J, i, j, index_u, index_v) \
  shared(bc_flow_west, bc_flow_east, bc_flow_south, bc_flow_north, u_dom_matrix, u_W_non_uni, \
	 u_E_non_uni, u, v, p_faces_x, p_faces_y, aP_u, aW_u, aE_u, aS_u, aN_u, b_u, fn_u)
  {
#pragma omp section 
    {
      /* West */
      i = 0;

      /* Type = 1, Fixed value */
      if(bc_flow_west->type == 1){

	/* Fixed value, parabolic */
	if(bc_flow_west->parabolize_profile_OK){
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = u_W_non_uni[J];
	  }
	} 

	else{
	  /* Make piston profile */
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    aP_u[index_u] = 1;
	
	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }
	    else{
	      b_u[index_u] = bc_flow_west->v_value[0];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */
      else if((bc_flow_west->type == 2) || (bc_flow_west->type == 3)){
	for(J=1; J<Ny-1; J++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  b_u[index_u] = 0;
	}
      }
      /* Type = 0, Zero gradient */
      else if(bc_flow_west->type == 0){
	for(J=1; J<Ny-1; J++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){		  
	    b_u[index_u] = 0;		  
	  }
	  else{	  
	    aE_u[index_u] = 1;
	  }	  
	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_flow_west->type == 4){
	for(J=1; J<Ny-1; J++){
	  index_u = J * nx + i;
	  aP_u[index_u] = 1;

	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;
	  }
	  else{
	    I = i + 1;
	    j = J - 1;
	    index_v = j * Nx + I;
	    /* Ec. 9.29 Veersteg */
	    b_u[index_u] = (u[index_u+1] * p_faces_x[index_u+1] + v[index_v+Nx] * p_faces_y[index_v+Nx] -
			    v[index_v]  * p_faces_y[index_v]) / p_faces_x[index_u];
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(bc_flow_west->type == 5){
	for(J=1; J<Ny-1; J++){
	  index_u = J * nx + i;
	  aP_u[index_u] = 1;

	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;
	  }
	  else{
	    /* Ec. 9.31 Veersteg */
	    b_u[index_u] = u[J * nx + nx - 2];
	  }
	}
      }      
    } /* West section */

#pragma omp section 
    {
      /* East */
      i = nx-1;

      /* Type = 1, Fixed value */      
      if(bc_flow_east->type == 1){

	/* Fixed value, parabolic */	
	if(bc_flow_east->parabolize_profile_OK){
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = u_E_non_uni[J];
	  }
	}

	else{
	  /* Make piston profile */	  
	  for(J=1; J<Ny-1; J++){
	    index_u = J*nx + i;
	    aP_u[index_u] = 1;
	  
	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }	  
	    else{
	      b_u[index_u] = bc_flow_east->v_value[0];	  
	    }	
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */      
      else if((bc_flow_east->type == 2) || (bc_flow_east->type == 3)){
	for(J=1; J<Ny-1; J++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  b_u[index_u] = 0;
	}
      }
      /* Type = 0, Zero gradient */      
      else if(bc_flow_east->type == 0){
	for(J=1; J<Ny-1; J++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){	  
	    b_u[index_u] = 0;		  
	  }
	  
	  else{
	    aW_u[index_u] = 1;	  
	  }
	}
      }
      /* Type = 4, Constant pressure */      
      else if(bc_flow_east->type == 4){
	for(J=1; J<Ny-1; J++){
	  index_u = J * nx + i;
	  aP_u[index_u] = 1;

	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;
	  }
	  else{
	    j = J - 1;
	    index_v = j * Nx + i;
	    /* Ec. 9.29 Veersteg */	    
	    b_u[index_u] = (u[index_u-1] * p_faces_x[index_u-1] + v[index_v] * p_faces_y[index_v] -
			    v[index_v+Nx] * p_faces_y[index_v+Nx]) / p_faces_x[index_u];
	  }
	}
      }
      /* Type = 5, Periodic boundary */      
      else if(bc_flow_east->type == 5){
	for(J=1; J<Ny-1; J++){
	  index_u = J * nx + i;
	  aP_u[index_u] = 1;

	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;
	  }
	  else{
	    /* Ec. 9.31 Veersteg */	    
	    b_u[index_u] = u[J * nx + 1];
	  }
	}
      }
    } /* East section */

#pragma omp section 
    {
      /* South */
      J = 0;
      
      /* Type = 1, Fixed value */
      if(bc_flow_south->type == 1){
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;		  
	  }	  
	  else{
	    b_u[index_u] = bc_flow_south->v_value[0];	  
	  }
	}
      }
      /* Type = 3, Wall */
      else if(bc_flow_south->type == 3){
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  b_u[index_u] = 0;
	}
      }
      /* Type = 5, Periodic */
      else if(bc_flow_south->type == 5){
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;		  
	  }	  
	  else{
	    b_u[index_u] = u[(Ny-3)*nx + i] * fn_u[(Ny-3)*nx + i] + 
	      u[(Ny-2)*nx + i] * (1 - fn_u[(Ny-3)*nx + i]);	  
	  }
	}
      }
      /* Type = 0, Zero gradient. Type = 2, Symmetry. Type = 4, Constant pressure */
      else{
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;		  
	  }	  
	  else{
	    aN_u[index_u] = 1;	  
	  }
	}
      }
    } /* South section */

#pragma omp section 
    {
      /* North */
      J = Ny - 1;

      /* Type = 1, Fixed value */      
      if(bc_flow_north->type == 1){
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){		  
	    b_u[index_u] = 0;		  
	  }	  
	  else{		  
	    b_u[index_u] = bc_flow_north->v_value[0];	  
	  }
	}
      }
      /* Type = 3, Wall */      
      else if(bc_flow_north->type == 3){
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  b_u[index_u] = 0;
	}
      }
      /* Type = 5, Periodic */      
      else if(bc_flow_north->type == 5){
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;		  
	  }	  
	  else{		  
	    b_u[index_u] = u[nx + i] * fn_u[nx + i] + 
	      u[2*nx + i] * (1 - fn_u[nx + i]);	  
	  }
	}
      }
      /* Type = 0, Zero gradient. Type = 2, Symmetry. Type = 4, Constant pressure */      
      else{
	for(i=1; i<nx-1; i++){
	  index_u = J*nx + i;
	  aP_u[index_u] = 1;
	  
	  if(u_dom_matrix[index_u] != 0){
	    b_u[index_u] = 0;		  
	  }
	  
	  else{		  
	    aS_u[index_u] = 1;	  
	  }
	}
      }
    } /* North section */
    
  } /* sections */

  /* V */

#pragma omp parallel sections default(none) private(I, J, i, j, index_u, index_v) \
  shared(bc_flow_west, bc_flow_east, bc_flow_south, bc_flow_north, v_dom_matrix, v_S_non_uni, \
	 v_N_non_uni, u, v, p_faces_x, p_faces_y, aP_v, aW_v, aE_v, aS_v, aN_v, b_v, fe_v)
  {
#pragma omp section 
    {
      /* West */
      I = 0;

      /* Type = 1, Fixed value */            
      if(bc_flow_west->type == 1){
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  
	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;		  
	  } 
	  else{
	    b_v[index_v] = bc_flow_west->v_value[1];	  
	  }
	}
      }
      /* Type = 3, Wall */            
      else if(bc_flow_west->type == 3){
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  b_v[index_v] = 0;
	}
      }
      /* Type = 5, Periodic */            
      else if(bc_flow_west->type == 5){
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;

	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;
	  }
	  else{
	    b_v[index_v] = v[j*Nx + Nx-3] * fe_v[j*Nx + Nx-3] +
	      v[j*Nx + Nx-2] * (1 - fe_v[j*Nx + Nx-3]);
	  }
	}
      }
      /* Type = 0, Zero gradient. Type = 2, Symmetry. Type = 4, Constant pressure */            
      else{
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  
	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;		  
	  }	  
	  else{		  
	    aE_v[index_v] = 1;	  
	  }
	}
      }
    } /* West section */

#pragma omp section 
    {
      /* East */
      I = Nx - 1;

      /* Type = 1, Fixed value */      
      if(bc_flow_east->type == 1){
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  
	  if(v_dom_matrix[index_v] != 0){		 
	    b_v[index_v] = 0;		  
	  }	  
	  else{
	    b_v[index_v] = bc_flow_east->v_value[1];	  
	  }
	}
      }
      /* Type = 3, Wall */      
      else if(bc_flow_east->type == 3){
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  b_v[index_v] = 0;
	}
      }
      /* Type = 5, Periodic */      
      else if(bc_flow_east->type == 5){
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;

	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;
	  }
	  else{
	    b_v[index_v] = v[j*Nx + 1] * fe_v[j*Nx + 1] +
	      v[j*Nx + 2] * (1 - fe_v[j*Nx + 1]);
	  }
	}
      }
      /* Type = 0, Zero gradient. Type = 2, Symmetry. Type = 4, Constant pressure */      
      else{
	for(j=1; j<ny-1; j++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  
	  if(v_dom_matrix[index_v] != 0){		 
	    b_v[index_v] = 0;		 
	  }	  
	  else{
	    aW_v[index_v] = 1;	  
	  }
	}
      }
    } /* East section */   

#pragma omp section 
    {
      /* South */
      j = 0;

      /* Type = 1, Fixed value */      
      if(bc_flow_south->type == 1){

	/* Fixed value, parabolic */	
	if(bc_flow_south->parabolize_profile_OK){
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = v_S_non_uni[I];
	  }
	}
	/* Make piston profile */
	else{
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    aP_v[index_v] = 1;
	  
	    if(v_dom_matrix[index_v] != 0){	  
	      b_v[index_v] = 0;
	    }
	    else{	  
	      b_v[index_v] = bc_flow_south->v_value[1];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */      
      else if((bc_flow_south->type == 2) || (bc_flow_south->type == 3)){
	for(I=1; I<Nx-1; I++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  b_v[index_v] = 0;
	}
      }
      /* Type = 0, Zero gradient */      
      else if(bc_flow_south->type == 0){
	for(I=1; I<Nx-1; I++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  
	  if(v_dom_matrix[index_v] != 0){		  
	    b_v[index_v] = 0;		  
	  }
	  
	  else{		  
	    aN_v[index_v] = 1;	  
	  }
	}
      }
      /* Type = 4, Constant pressure */      
      else if(bc_flow_south->type == 4){
	for(I=1; I<Nx-1; I++){
	  index_v = j * Nx + I;
	  aP_v[index_v] = 1;
	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;
	  }
	  else{
	    J = j + 1;
	    i = I - 1;
	    index_u = J * nx + i;
	    /* Ec. 9.29 Veersteg */	    
	    b_v[index_v] = (v[index_v+Nx] * p_faces_y[index_v+Nx] + u[index_u+1] * p_faces_x[index_u+1] -
			    u[index_u] * p_faces_x[index_u]) / p_faces_y[index_v];
	  }
	}
      }
      /* Type = 5, Periodic boundary */      
      else if(bc_flow_south->type == 5){
	for(I=1; I<Nx-1; I++){
	  index_v = j * Nx + I;
	  aP_v[index_v] = 1;

	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;
	  }
	  else{
	    /* Ec. 9.31 Veersteg */	    
	    b_v[index_v] = v[(ny-2) * Nx + I];
	  }
	}
      }
    } /* South section*/

#pragma omp section 
    {
      /* North */
      j = ny-1;

      /* Type = 1, Fixed value */           
      if(bc_flow_north->type == 1){

	/* Fixed value, parabolic */		
	if(bc_flow_north->parabolize_profile_OK){
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = v_N_non_uni[I];
	  }
	}
	else{
	  /* Make piston profile */	  
	  for(I=1; I<Nx-1; I++){
	    index_v = j*Nx + I;
	    aP_v[index_v] = 1;
	  
	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      b_v[index_v] = bc_flow_north->v_value[1];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */            
      else if((bc_flow_north->type == 2) || (bc_flow_north->type == 3)){
	for(I=1; I<Nx-1; I++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  b_v[index_v] = 0;
	}
      }
      /* Type = 0, Zero gradient */            
      else if(bc_flow_north->type == 0){
	for(I=1; I<Nx-1; I++){
	  index_v = j*Nx + I;
	  aP_v[index_v] = 1;
	  
	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;		  
	  }
	  else{		  
	    aS_v[index_v] = 1;	  
	  }
	}
      }
      /* Type = 4, Constant pressure */            
      else if(bc_flow_north->type == 4){
	for(I=1; I<Nx-1; I++){
	  index_v = j * Nx + I;
	  aP_v[index_v] = 1;
	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;
	  }
	  else{
	    i = I - 1;
	    index_u = j * nx + i;
	    /* Ec. 9.29 Veersteg */	    	    
	    b_v[index_v] = (v[index_v-Nx] * p_faces_y[index_v-Nx] + u[index_u] * p_faces_x[index_u] -
			    u[index_u+1] * p_faces_x[index_u+1]) / p_faces_y[index_v];
	  }
	}
      }
      /* Type = 5, Periodic boundary */            
      else if(bc_flow_north->type == 5){
	for(I=1; I<Nx-1; I++){
	  index_v = j * Nx + I;
	  aP_v[index_v] = 1;
	  
	  if(v_dom_matrix[index_v] != 0){
	    b_v[index_v] = 0;
	  }
	  else{
	    /* Ec. 9.31 Veersteg */	    	    
	    b_v[index_v] = v[1*Nx + I];
	  }
	}
      }

    } /* North section */
    
  } /* sections */

  return;
}

/* CONTINUITY_RESIDUAL */
/*****************************************************************************/
/**
  
  Sets:

  (double, 1) res: maximum value of the continuity equation residual.

  @param data

  @return double res.
  
  MODIFIED: 25-02-2019
*/
double continuity_residual(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;

  const int *p_dom_matrix = data->p_dom_matrix;

  double res = 0;
  double res_temp = 0;
  
  const double *v = data->v;
  const double *u = data->u;
  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;
 
#pragma omp parallel for default(none)				\
  private(I, J, i, j, index_, index_u, index_v, res_temp)	\
  shared(p_faces_x, p_faces_y, u, v, p_dom_matrix)		\
  reduction(max: res)

  for(I=1; I<Nx-1; I++){
    for(J=1; J<Ny-1; J++){
      
      i = I-1;
      j = J-1;

      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;

      if(p_dom_matrix[index_] == 0){
	
	res_temp = fabs(p_faces_y[index_v] * v[index_v] - 
			p_faces_y[index_v+Nx] * v[index_v+Nx] +
			p_faces_x[index_u] * u[index_u] - 
			p_faces_x[index_u+1] * u[index_u+1]);
	
	if (res_temp > res){
	  res = res_temp;
	}
      }
    }
  }
  
  return res;
}

/* SET_D_u */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) Du_x: Diffusive parameter for u momentum equation at x_faces.

  Array (double, nxny) Du_y: Diffusive parameter for u momentum equation at y_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 11-06-2019
*/
void set_D_u(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face;
  int index_u;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int diff_u_face_x_count = data->diff_u_face_x_count;

  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;

  double mu_eff;
  double mu_eff_0, mu_eff_1;

  const double mu = data->mu;

  double *Du_x = data->Du_x;
  double *Du_y = data->Du_y;
  
  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  
  const double *Delta_xW_u = data->Delta_xW_u;
  const double *Delta_yN_u = data->Delta_yN_u;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  const double *fw_u = data->fw_u;
  const double *fn_u = data->fn_u;
  const double *diff_u_face_x_Delta = data->diff_u_face_x_Delta;
  
#pragma omp parallel for default(none) private(I, J, i, index_, index_u, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Du_x, u_faces_x, mu_turb_at_u_nodes, Delta_xW_u, fw_u)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      i = I-1;

      index_ = J*Nx+I;
      index_u = J*nx+i;

      mu_eff_0 = mu + mu_turb_at_u_nodes[index_u];
      mu_eff_1 = mu + mu_turb_at_u_nodes[index_u+1];
      mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * (1 - fw_u[index_u+1]) + mu_eff_1 * fw_u[index_u+1]);
      Du_x[index_] = u_faces_x[index_] * 
	mu_eff / Delta_xW_u[index_u+1];

    }
  }

  /* Correction for cut faces */

  for(z=0; z<diff_u_face_x_count; z++){
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    mu_eff_0 = mu + mu_turb_at_u_nodes[index_u];
    mu_eff_1 = mu + mu_turb_at_u_nodes[index_u+1];
    mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * (1 - fw_u[index_u+1]) + mu_eff_1 * fw_u[index_u+1]);
    Du_x[index_] = u_faces_x[index_] * 
      mu_eff / diff_u_face_x_Delta[z];
  }

#pragma omp parallel for default(none) private(I, J, i, j, index_face, index_u, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Du_y, u_faces_y, Delta_yN_u, mu_turb_at_u_nodes, fn_u)

  for(j=0; j<ny; j++){
    for(i=1; i<nx-1; i++){

      J = j+1;
      I = i + 1;

      index_face = j*nx+i;
      index_u = J*nx+i;
       
      mu_eff_0 = mu + mu_turb_at_u_nodes[index_u-nx];
      mu_eff_1 = mu + mu_turb_at_u_nodes[index_u];
      mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_1 * (1 - fn_u[index_u-nx]) + mu_eff_0 * fn_u[index_u-nx]);
      Du_y[index_face] = u_faces_y[index_face] * 
	mu_eff / Delta_yN_u[index_u-nx];
    }
  }
  
  return;
}

/* SET_D_v */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxny) Dv_x: Diffusive parameter for v momentum equation at x_faces.

  Array (double, NxNy) Dv_y: Diffusive parameter for v momentum equation at y_faces.

  @param data
  
  @return ret_value not fully implemented.
  
  MODIFIED: 11-06-2019
*/
void set_D_v(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face;
  int index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int diff_v_face_y_count = data->diff_v_face_y_count;

  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;

  double mu_eff;
  double mu_eff_0, mu_eff_1;

  const double mu = data->mu;

  double *Dv_x = data->Dv_x;
  double *Dv_y = data->Dv_y;

  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;

  const double *Delta_xE_v = data->Delta_xE_v;
  const double *Delta_yS_v = data->Delta_yS_v;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  const double *fe_v = data->fe_v;
  const double *fs_v = data->fs_v;
  const double *diff_v_face_y_Delta = data->diff_v_face_y_Delta;

#pragma omp parallel for default(none) private(I, J, i, j, index_face, index_v, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dv_x, v_faces_x, Delta_xE_v, mu_turb_at_v_nodes, fe_v)

  for(j=1; j<ny-1; j++){
    for(i=0; i<nx; i++){

      I = i+1;
      J = j+1;

      index_face = j*nx + i;
      index_v = j*Nx + I;

      mu_eff_0 = mu + mu_turb_at_v_nodes[index_v-1];
      mu_eff_1 = mu + mu_turb_at_v_nodes[index_v];
      mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_1 * (1 - fe_v[index_v-1]) + mu_eff_0 * fe_v[index_v-1]); 
      Dv_x[index_face] = v_faces_x[index_face] * 
	mu_eff / Delta_xE_v[index_v-1];
    }
  }

#pragma omp parallel for default(none) private(I, J, j, index_, index_v, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dv_y, v_faces_y, Delta_yS_v, mu_turb_at_v_nodes, fs_v)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      j = J-1;

      index_ = J*Nx + I;
      index_v = j*Nx + I;

      mu_eff_0 = mu + mu_turb_at_v_nodes[index_v];
      mu_eff_1 = mu + mu_turb_at_v_nodes[index_v+Nx];
      mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * (1 - fs_v[index_v+Nx]) + mu_eff_1 * fs_v[index_v+Nx]);
      Dv_y[index_] = v_faces_y[index_] * 
	mu_eff / Delta_yS_v[index_v+Nx];

    }
  }

  /* Correction for cut faces */

  for(z=0; z<diff_v_face_y_count; z++){
    index_ = diff_v_face_y_index[z];
    index_v = diff_v_face_y_index_v[z];
    mu_eff_0 = mu + mu_turb_at_v_nodes[index_v];
    mu_eff_1 = mu + mu_turb_at_v_nodes[index_v+Nx];
    mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * (1 - fs_v[index_v+Nx]) + mu_eff_1 * fs_v[index_v+Nx]);
    Dv_y[index_] = v_faces_y[index_] * 
      mu_eff / diff_v_face_y_Delta[z];
    
  }

  return;
}

/* SET_F_u */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) Fu_x: Convective parameter for u momentum equation at x_faces.

  Array (double, nxny) Fu_y: Convective parameter for u momentum equation at y_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 11-06-2019
*/
void set_F_u(Data_Mem *data){

  int I, J, i, j, z;
  int index_face, index_;
  int index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int u_cut_face_Fy_count = data->u_cut_face_Fy_count;

  const int *u_cut_face_Fy_index = data->u_cut_face_Fy_index;
  const int *u_cut_face_Fy_index_v = data->u_cut_face_Fy_index_v;

  const double rho = data->rho_v[0];

  double *Fu_x = data->Fu_x;
  double *Fu_y = data->Fu_y;
  
  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  const double *fe_v = data->fe_v;
  const double *u_cut_face_Fy_W = data->u_cut_face_Fy_W;
  const double *u_cut_face_Fy_E = data->u_cut_face_Fy_E;
  const double *u = data->u;
  const double *v = data->v;
  const double *u_cut_fw = data->u_cut_fw;
  const double *u_cut_fe = data->u_cut_fe;

# pragma omp parallel for default(none) private(I, J, i, index_, index_u) \
  shared(Fu_x, u_faces_x, u, u_cut_fw, u_cut_fe)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      i = I-1;

      index_ = J*Nx+I;
      index_u = J*nx+i;
      /* Not at center of cut face, corrected with alpha later */
      Fu_x[index_] = u_faces_x[index_] * rho *
	(u[index_u] * u_cut_fw[index_] + u[index_u+1] * u_cut_fe[index_]);
      
    }
  }

# pragma omp parallel for default(none) private(i, j, index_v, index_face) \
  shared(Fu_y, u_faces_y, v, fe_v)

  for(j=0; j<ny; j++){
    for(i=1; i<nx-1; i++){

      index_v = j*Nx+i;
      index_face = j*nx+i;

      /* Computed at center of cut face */      
      Fu_y[index_face] = u_faces_y[index_face] * rho * 
	(v[index_v] * fe_v[index_v] + (1 - fe_v[index_v]) * v[index_v+1]);
    }
  }


  for(z=0; z<u_cut_face_Fy_count; z++){

    index_face = u_cut_face_Fy_index[z];
    index_v = u_cut_face_Fy_index_v[z];
    Fu_y[index_face] = rho * u_faces_y[index_face] * 
      (v[index_v] * u_cut_face_Fy_W[z] + v[index_v+1] * u_cut_face_Fy_E[z]);
  }

  return;
}

/* SET_F_v */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxny) Fv_x: Convective parameter for v momentum equation at x_faces.

  Array (double, NxNy) Fv_y: Convective parameter for v momentum equation at y_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 11-06-2019
*/
void set_F_v(Data_Mem *data){

  int I, J, i, j, z;
  int index_face, index_;
  int index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int v_cut_face_Fx_count = data->v_cut_face_Fx_count;

  const int *v_cut_face_Fx_index = data->v_cut_face_Fx_index;
  const int *v_cut_face_Fx_index_u = data->v_cut_face_Fx_index_u;

  const double rho = data->rho_v[0];

  double *Fv_x = data->Fv_x;
  double *Fv_y = data->Fv_y;

  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *u = data->u;
  const double *v = data->v;
  const double *fn_u = data->fn_u;
  const double *v_cut_face_Fx_S = data->v_cut_face_Fx_S;
  const double *v_cut_face_Fx_N = data->v_cut_face_Fx_N;
  const double *v_cut_fs = data->v_cut_fs;
  const double *v_cut_fn = data->v_cut_fn;

# pragma omp parallel for default(none) private(i, j, index_u, index_face) \
  shared(Fv_x, v_faces_x, u, fn_u)

  for(j=1; j<ny-1; j++){
    for(i=0; i<nx; i++){

      index_u = j*nx+i;
      index_face = j*nx+i;

      /* Computed at center of cut face */      	
      Fv_x[index_face] = v_faces_x[index_face] * rho * 
	(u[index_u] * fn_u[index_u] + (1 - fn_u[index_u]) * u[index_u+nx]);
      
    }
  }

  for(z=0; z<v_cut_face_Fx_count; z++){

    index_face = v_cut_face_Fx_index[z];
    index_u = v_cut_face_Fx_index_u[z];
    Fv_x[index_face] = v_faces_x[index_face] * rho *
      (u[index_u] * v_cut_face_Fx_S[z] + u[index_u+nx] * v_cut_face_Fx_N[z]);
  }

# pragma omp parallel for default(none) private(I, J, j, index_, index_v) \
  shared(Fv_y, v_faces_y, v, v_cut_fs, v_cut_fn)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      j = J-1;
      
      index_ = J*Nx+I;
      index_v = j*Nx+I;
      
      /* Not at center of cut face, corrected with alpha later */
      Fv_y[index_] = v_faces_y[index_] * rho *
	(v[index_v] * v_cut_fs[index_] + v[index_v+Nx] * v_cut_fn[index_]);
    }
  }

  return;
}

/* OPEN_BND_INDX */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Ny) w_J: J indexes on west boundary that correspond to fluid. 
  Only the first w_count values are meaningful.

  Array (int, Ny) e_J: J indexes on east boundary that correspond to fluid.
  Only the first e_count values are meaningful.

  Array (int, Nx) s_I: I indexes on south boundary that correspond to fluid.
  Only the first s_count values are meaningful.

  Array (int, Nx) n_I: I indexes on north boundary that correspond to fluid.
  Only the first n_count values are meaningful.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 05-03-2019
*/
void open_bnd_indx(Data_Mem *data){

  int i, j, I, J, index_u, index_v;

  int w_count = 0;
  int e_count = 0;
  int s_count = 0;
  int n_count = 0;

  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  int *w_J = data->w_J;
  int *e_J = data->e_J;
  int *s_I = data->s_I;
  int *n_I = data->n_I;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);
  
#pragma omp parallel sections default(none) private(I, J, i, j, index_u, index_v) \
  shared(bc_flow_west, bc_flow_east, bc_flow_north, bc_flow_south, u_dom_matrix, v_dom_matrix, \
	 n_count, s_count, e_count, w_count, n_I, s_I, e_J, w_J)
  {

#pragma omp section 
    {
      /* West boundary */
      if(bc_flow_west->type == 1){
	if(bc_flow_west->parabolize_profile_OK == 1){
	  /* Finding open fluid zones */
	  i = 0;
	  for(J=0; J<Ny; J++){
	    index_u = J*nx + i;
	    if(u_dom_matrix[index_u] == 0){
	      /* Stores all Js from which there is fluid */
	      w_J[w_count] = J;
	      w_count++;
	    }
	  }
	}
      }
    } /* West section */

#pragma omp section
    {
      /* East boundary */
      if(bc_flow_east->type == 1){
	if(bc_flow_east->parabolize_profile_OK == 1){
	  i = nx - 1;
	  for(J=0; J<Ny; J++){
	    index_u = J*nx + i;
	    if(u_dom_matrix[index_u] == 0){
	      e_J[e_count] = J;
	      e_count++;
	    }
	  }
	}
      }
    } /* East section */

#pragma omp section 
    {
      /* South boundary */
      if(bc_flow_south->type == 1){
	if(bc_flow_south->parabolize_profile_OK == 1){
	  j = 0;
	  for(I=0; I<Nx; I++){
	    index_v = j*Nx + I;
	    if(v_dom_matrix[index_v] == 0){
	      s_I[s_count] = I;
	      s_count++;
	    }
	  }
	}
      }
    } /* South section */

#pragma omp section 
    { 
      /* North boundary */
      if(bc_flow_north->type == 1){
	if(bc_flow_north->parabolize_profile_OK == 1){
	  j = ny - 1;
	  for(I=0; I<Nx; I++){
	    index_v = j*Nx + I;
	    if(v_dom_matrix[index_v] == 0){
	      n_I[n_count] = I;
	      n_count++;
	    }
	  }
	}
      }
    } /* North section */
  } /* sections */

  data->w_count = w_count;
  data->e_count = e_count;
  data->s_count = s_count;
  data->n_count = n_count;

  return;
}

/* PROCESS_ACTION_ON_SIGNAL */
void process_action_on_signal(Data_Mem *data,
			      int *convergence_OK,
			      int *sub_convergence_OK){

  /* Attemp to make a single function for flow and phi */

  const int coupled_physics_OK = data->coupled_physics_OK;
  const int flow_done_OK = data->flow_done_OK;
  const int turb_model = data->turb_model;

  switch(action_on_signal_OK){
    
  case 0: /* Continue */
    printf("\n");
    break;
    
  case 1: /* Save tmp data */
    write_to_tmp_file(data);
    break;
    
  case 2:  /* Save data in .dat files */
    checkCall(save_data_in_MAT_file(data), &(data->wins));
    break;
    
  case 3:  /* Exit */
    destroy_windows(&(data->wins));
    free_memory(data);
    printf("\n");
    exit(EXIT_SUCCESS);
    break;
    
  case 4: /* Exit and save tmp data */
    write_to_tmp_file(data);
    destroy_windows(&(data->wins));
    free_memory(data);
    printf("\n");
    exit(EXIT_SUCCESS);
    break;
    
  case 5: /* Declare convergence */
    *convergence_OK = 1;
    *sub_convergence_OK = 1;
    break;
    
  case 6: /* Change alphas */
    if(!flow_done_OK){      
      change_alphas_flow(data);
      
      if(coupled_physics_OK){
	change_alphas_phi(data);
      }
    }
    
    if(flow_done_OK && !coupled_physics_OK){
      change_alphas_phi(data);
    }

    break;
    
  case 7: /* Change iteration values */
    if(!flow_done_OK){
      
      printf("Enter flow iteration values, current are: \n"
	     "max_iter_flow(%d) max_sub_iter_flow(%d) max_iter_ADI(%d)\n"
	     "it_for_update_coeffs(%d) max_iter_uvp(%d) = ",
	     data->max_iter_flow,
	     data->max_sub_iter_flow,
	     data->max_iter_ADI,
	     data->it_for_update_coeffs,
	     data->max_iter_uvp);
      
      scanf("%d %d %d %d %d", 
	    &(data->max_iter_flow),
	    &(data->max_sub_iter_flow),
	    &(data->max_iter_ADI),
	    &(data->it_for_update_coeffs),
	    &(data->max_iter_uvp));

      if(turb_model){
	printf("Enter turbulence iteration values, current are: \n"
	       "it_for_update_k_coeffs(%d) it_for_update_eps_coeffs(%d) max_iter_ke(%d) = ",
	       data->it_for_update_k_coeffs,
	       data->it_for_update_eps_coeffs,
	       data->max_iter_ke);
	
	scanf("%d %d %d", 
	      &(data->it_for_update_k_coeffs),
	      &(data->it_for_update_eps_coeffs),
	      &(data->max_iter_ke));
	
      }
      
      if(coupled_physics_OK){
	      printf("Enter coupled physics iteration values, current are: \n"
	     "it_update_coeff_phi(%d) max_phi_gL_iter(%d) = ",
	     data->it_upd_coeff_phi,
	     data->max_sub_it);
      
      scanf("%d %d", 
	    &(data->it_upd_coeff_phi),
	    &(data->max_sub_it));
      }
   
    } // end if(!flow_done_OK)
    
    if(flow_done_OK && !coupled_physics_OK){
      
      printf("Enter new solve_phi iteration values, current are: "
	     "max_it(%d) max_sub_it(%d) it_update_coeff_phi(%d) = ",
	     data->max_it,
	     data->max_sub_it,
	     data->it_upd_coeff_phi);
      
      scanf("%d %d %d",
	    &(data->max_it),
	    &(data->max_sub_it),
	    &(data->it_upd_coeff_phi));
    }
    
    break;
    
  case 8: /* Compute Courant number */
    checkCall(compute_courant_number(data), &(data->wins));
    break;

  case 9: /* Peek data */
    data->peek_flow_OK = 0;
    data->peek_phi_OK = 0;
    
    if(!flow_done_OK){
      /* Flow computations are running */
      data->peek_flow_OK = 1;
      
      if(coupled_physics_OK){
	/* Flow and phi coupled computations running */
	data->peek_phi_OK = 1;

      }      
    }
    
    if(flow_done_OK && !coupled_physics_OK){
      /* Only phi computations running */
      data->peek_flow_OK = 0;
      data->peek_phi_OK = 1;      
    }

    graphs((void *) data);
    
    data->peek_flow_OK = 0;
    data->peek_phi_OK = 0;
    
    break;
    
  case 10: /* Change flow_scheme */
    if(!flow_done_OK){
      printf("Enter new flow_scheme (current value is %d), "
	     "(0) upwind, (1) QUICK, (2) TVD: ", data->flow_scheme);
      scanf("%d", &(data->flow_scheme));
    }
    else{
      printf("Flow already computed... Nothing to be done here.\n");
    }
    break;
    
  case 11: /* Start coupled physics mode now */
    if(!coupled_physics_OK){
      printf("Coupled physics mode needs to be enabled from the respective CONFIG file\n");
      printf("No action taken\n");      
    }
    else{
      data->it_to_start_coupled_physics = 1;
    }
    break;
    
  default:
    printf("\n");
    break;
  }

#ifdef DISPLAY_OK
  /* Back to ncurses */
  refresh();
#endif
  
  return;
}

/* MASS_IO_BALANCE */
/*****************************************************************************/
/**

  Calculates relative global mass imbalance.

  mass_in: Mass flow at West-South boundaries.

  mass_out: Mass flow at East-North boundaries.

  @param data

  @return fabs((mass_in - mass_out) / mass_in);
  
  MODIFIED: 27-11-2019
*/  
double mass_io_balance(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;

  double mass_in = 0;
  double mass_out = 0;

  const double *u = data->u;
  const double *v = data->v;
  const double *rho_m = data->rho_m;

  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;

#pragma omp parallel sections default(none) private(I, J, i, j, index_u, index_v, index_) \
  shared(u, v, rho_m, u_faces_x, v_faces_y) reduction(+:mass_in, mass_out)
  {

#pragma omp section 
    {
      /* East */
      I = Nx-1;
      i = I-1;

      for(J=1; J<Ny-1; J++){

	index_ = J*Nx+I;
	index_u = J*nx+i;

	mass_out += rho_m[index_]*u[index_u]*u_faces_x[index_];
      
      }
    }

#pragma omp section 
    {
      /* West */
      I = 0;
      i = 0;

      for(J=1; J<Ny-1; J++){
      
	index_ = J*Nx+I;
	index_u = J*nx+i;
      
	mass_in += rho_m[index_]*u[index_u]*u_faces_x[index_];
      
      }
    }

#pragma omp section 
    {
      /* North */
      J = Ny-1;
      j = J-1;

      for(I=1; I<Nx-1; I++){
      
	index_ = J*Nx+I;
	index_v = j*Nx+I;
      
	mass_out += rho_m[index_]*v[index_v]*v_faces_y[index_];
      }
    }

#pragma omp section 
    {
      /* South */
      J = 0;
      j = 0;

      for(I=1; I<Nx-1; I++){
      
	index_ = J*Nx+I;
	index_v = j*Nx+I;
      
	mass_in += rho_m[index_]*v[index_v]*v_faces_y[index_];
      }
    }

  } // sections
  
  if(mass_in != 0){
    return fabs((mass_in - mass_out) / mass_in);
  }
  else{
    return NAN;
  }
}

/* COMPUTE PARABOLIC INLET */
/*****************************************************************************/
/**
  
  On function make_parabolic_profile:

  Sets:

  Array (double, Ny) u_*_non_uni: value of u to apply at boundary if 
  parabolize_profile_OK is set as true.

  Array (double, Nx) v_*_non_uni: value of v to apply at boundary if 
  parabolize_profile_OK is set as true.

  Array (double, Nx|Ny) k_*_non_uni: value of k to apply at boundary if 
  parabolize_profile_OK is set as true.

  Array (double, Nx|Ny) eps_*_non_uni: value of eps to apply at boundary if 
  parabolize_profile_OK is set as true.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 06-02-2019
*/
void compute_parabolic_inlet(Data_Mem *data){

  int i, j;

  const int w_count = data->w_count;
  const int e_count = data->e_count;
  const int n_count = data->n_count;
  const int s_count = data->s_count;
  int l_, u_;
 
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  int *e_J = data->e_J;
  int *w_J = data->w_J;
  int *n_I = data->n_I;
  int *s_I = data->s_I;
 
  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);

  /* U */
  /* West */
  i = 0;
  if(bc_flow_west->type == 1){

    if(bc_flow_west->parabolize_profile_OK == 1){

      if(w_count == Ny){
	/* The whole boundary is fluid */
	l_ = 0;
	u_ = Ny-1;
	/* Make parabolic profile */
	make_parabolic_profile(data, l_, u_, 'W');
      }
      else{
	/* There exist at least one segment of fluid < Ly */
	/* Need to get their Js */
	l_ = w_J[0];

	for(i=1; i<w_count; i++){
	  if(w_J[i] != w_J[i-1] + 1){
	    u_ = w_J[i-1];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'W');
	    /* Search for another fluid segment */
	    l_ = w_J[i];
	  }
	  else if(i == w_count-1){
	    u_ = w_J[i];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'W');
	  }
	} /* for(i=1; i<w_count; i++) */
      } /* else */
    } /* if(bc_flow_west->parabolize_profile_OK == 1){ */
  }

  /* East */
  i = nx-1;
  if(bc_flow_east->type == 1){
    if(bc_flow_east->parabolize_profile_OK == 1){

      if(e_count == Ny){
	/* The whole boundary is fluid */
	l_ = 0;
	u_ = Ny-1;
	/* Make parabolic profile */
	make_parabolic_profile(data, l_, u_, 'E');
      }
      else{
	/* There exist at least one segment of fluid < Ly */
	/* Need to get their Js */
	l_ = e_J[0];

	for(i=1; i<e_count; i++){
	  if(e_J[i] != e_J[i-1] + 1){
	    u_ = e_J[i-1];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'E');
	    /* Search for another fluid segment */
	    l_ = e_J[i];
	  }
	  else if(i == e_count-1){
	    u_ = e_J[i];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'E');
	  }
	} /* for(i=1; i<e_count; i++) */
      } /* else */
    } /* if(bc_flow_east->parabolize_profile_OK == 1){ */
  }

  /* South */
  j = 0;
  if(bc_flow_south->type == 1){

    if(bc_flow_south->parabolize_profile_OK == 1){

      if(s_count == Nx){
	/* The whole boundary is fluid */
	l_ = 0;
	u_ = Nx-1;
	/* Make parabolic profile */
	make_parabolic_profile(data, l_, u_, 'S');
      }
      else{
	/* There exist at least one segment of fluid < Lx */
	/* Need to get their Is */
	l_ = s_I[0];
	
	for(j=1; j<s_count; j++){
	  if(s_I[j] != s_I[j-1] + 1){
	    u_ = s_I[j-1];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'S');
	    /* Search for another fluid segment */
	    l_ = s_I[j];
	  }
	  else if(j == s_count-1){
	    u_ = s_I[j];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'S');
	  }
	} /* for(j=1; j<s_count; j++) */
      } /* else */
    } /* if(bc_flow_south->parabolize_profile_OK == 1){ */
  }

  /* North */
  j = ny-1;
  if(bc_flow_north->type == 1){

    if(bc_flow_north->parabolize_profile_OK == 1){

      if(n_count == Nx){
	/* The whole boundary is fluid */
	l_ = 0;
	u_ = Nx-1;
	/* Make parabolic profile */
	make_parabolic_profile(data, l_, u_, 'N');
      }
      else{
	/* There exist at least one segment of fluid < Lx */
	/* Need to get their Is */
	l_ = n_I[0];

	for(j=1; j<n_count; j++){
	  if(n_I[j] != n_I[j-1] + 1){
	    u_ = n_I[j-1];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'N');
	    /* Search for another fluid segment */
	    l_ = n_I[j];
	  }
	  else if(j == n_count-1){
	    u_ = n_I[j];
	    /* Make parabolic profile */
	    make_parabolic_profile(data, l_, u_, 'N');
	  }
	} /* for(j=1; j<n_count; j++) */
      } /* else */
    } /* if(bc_flow_south->parabolize_profile_OK == 1){ */
  }

  return;
}

/* SET_P_REF_NODE */
/*****************************************************************************/
/**
  
  Sets:

  (int) set_ref_p_node: set to true if a reference pressure node is set.

  (int) ref_p_node_index: index of reference pressure node.

  (double) ref_p_value: reference pressure value.

  @param data

  @return ret_value.

  MODIFIED: 06-02-2019
*/
int set_p_ref_node(Data_Mem *data){

  int I, J, index_;
  int ref_p_index_found = 0;
  int ret_value = 0;

  int *set_ref_p_node = &(data->set_ref_p_node);
  int *ref_p_node_index = &(data->ref_p_node_index);

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int w_ref_p_node_OK = data->bc_flow_west.ref_p_node_OK;
  const int e_ref_p_node_OK = data->bc_flow_east.ref_p_node_OK;
  const int s_ref_p_node_OK = data->bc_flow_south.ref_p_node_OK;
  const int n_ref_p_node_OK = data->bc_flow_north.ref_p_node_OK;

  const int *p_dom_matrix = data->p_dom_matrix;

  double *ref_p_value = &(data->ref_p_value);

  const double w_p_value = data->bc_flow_west.p_value;
  const double e_p_value = data->bc_flow_east.p_value;
  const double s_p_value = data->bc_flow_south.p_value;
  const double n_p_value = data->bc_flow_north.p_value;

  double *p = data->p;
  
  if(w_ref_p_node_OK + e_ref_p_node_OK + s_ref_p_node_OK + n_ref_p_node_OK > 1){
    ret_value = 1;
    return ret_value;
  }

  /* Initialization */
  *set_ref_p_node = 0;

  if(w_ref_p_node_OK){
    I = 1;
    /* J between 2 and Ny-3 to influence only west boundary */
    J = 2;
    while((J < Ny-2) && (!ref_p_index_found)){
      index_ = J * Nx + I;
      /* Search for some available pressure node */
      if(p_dom_matrix[index_] == 0){
	*set_ref_p_node = 1;
	*ref_p_node_index = index_;
	ref_p_index_found = 1;
	/* Store reference pressure value to apply in coeffs_p function */
	*ref_p_value = w_p_value;
	p[index_] = w_p_value;
	/* Needed for SIMPLE, because the iterative section will maintain equality of pressure correction  only */
	p[index_-1] = p[index_];
      }
      J++;
    }
  }

  if(e_ref_p_node_OK){
    I = Nx - 2;
    J = 2;
    while((J < Ny-2) && (!ref_p_index_found)){
      index_ = J * Nx + I;
      if(p_dom_matrix[index_] == 0){
	*set_ref_p_node = 1;
	*ref_p_node_index = index_;
	ref_p_index_found = 1;
	*ref_p_value = e_p_value;
	p[index_] = e_p_value;
	p[index_+1] = p[index_];
      }
      J++;
    }
  }

  if(s_ref_p_node_OK){
    J = 1;
    I = 2;
    while((I < Nx-2) && (!ref_p_index_found)){
      index_ = J * Nx + I;
      if(p_dom_matrix[index_] == 0){
	*set_ref_p_node = 1;
	*ref_p_node_index = index_;
	ref_p_index_found = 1;
	*ref_p_value = s_p_value;
	p[index_] = s_p_value;
	p[index_-Nx] = p[index_];
      }
      I++;
    }
  }

  if(n_ref_p_node_OK){
    J = Ny - 2;
    I = 2;
    while((I < Nx-2) && (!ref_p_index_found)){
      index_ = J * Nx + I;
      if(p_dom_matrix[index_] == 0){
	*set_ref_p_node = 1;
	*ref_p_node_index = index_;
	ref_p_index_found = 1;
	*ref_p_value = n_p_value;
	p[index_] = n_p_value;
	p[index_+Nx] = p[index_];
      }
      I++;
    }
  }

  return ret_value;
}

/* INIT_P_AT_BOUNDARY */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) p: initialize boundary values.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 06-02-2019
*/
void init_p_at_boundary(Data_Mem *data){

  int I, J, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *p_dom_matrix = data->p_dom_matrix;

  double *p = data->p;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);


  /* Needed for SIMPLE, SIMPLE_CANG, SIMPLEC, because boundary condition will 
     be applied to pressure correction only */


#pragma omp parallel sections default(none) private(I, J, index_)	\
  shared(bc_flow_west, bc_flow_east, bc_flow_south, bc_flow_north, p_dom_matrix, p)
  {

#pragma omp section
    {
      /* West */
      I = 1;
      if(bc_flow_west->type == 4){
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = bc_flow_west->p_value;
	    p[index_-1] = p[index_];
	  }
	}
      }
      else if(bc_flow_west->type == 5){
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = 0.5 * (p[J*Nx + Nx-3] + p[J*Nx + 2]);
	    p[index_-1] = p[index_];
	  }
	}
      }
      else{
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_-1] = p[index_];
	  }
	}
      }
    }

#pragma omp section
    {
      /* East */
      I = Nx - 2;
      if(bc_flow_east->type == 4){
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = bc_flow_east->p_value;
	    p[index_+1] = p[index_];
	  }
	}
      }
      else if(bc_flow_east->type == 5){
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = 0.5 * (p[J*Nx + Nx-3] + p[J*Nx + 2]);
	    p[index_+1] = p[index_];
	  }
	}
      }
      else{
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_+1] = p[index_];
	  }
	}
      }
    }

#pragma omp section 
    {
      /* South */
      J = 1;
      if(bc_flow_south->type == 4){
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = bc_flow_south->p_value;
	    p[index_-Nx] = p[index_];
	  }
	}
      }
      else if(bc_flow_south->type == 5){
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = 0.5 * (p[(Ny-3)*Nx + I] + p[2*Nx + I]);
	    p[index_-Nx] = p[index_];
	  }
	}
      }
      else{
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_-Nx] = p[index_];
	  }
	}
      }
    }

#pragma omp section 
    {
      /* North */
      J = Ny - 2;
      if(bc_flow_north->type == 4){
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = bc_flow_north->p_value;
	    p[index_+Nx] = p[index_];
	  }
	}
      }
      else if(bc_flow_north->type == 5){
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] = 0.5 * (p[(Ny-3)*Nx + I] + p[2*Nx + I]);
	    p[index_+Nx] = p[index_];
	  }
	}
      }
      else{
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_+Nx] = p[index_];
	  }
	}
      }
    }

  } // sections
  
  return;
}

/* ADD_PERIODIC_SOURCE_TO_P */
/*****************************************************************************/
/**
   Sets:
   Array (double, NxNy) p: add source term to pressure profile after calculation.
   
   @param data
    
   @return ret_value not implemented.
       
   MODIFIED: 03-06-2019
*/
void add_periodic_source_to_p(Data_Mem *data){

  int I, J, index_; 

  const int p_source_orientation = data->p_source_orientation;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *p_dom_matrix = data->p_dom_matrix;
  
  const double periodic_source = data->periodic_source;

  double *p = data->p;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;

  /* w-e periodic flow */
  if(p_source_orientation == 1){

#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(p, nodes_x, p_dom_matrix)

    for(J=1; J<Ny-1; J++){
      for(I=0; I<Nx; I++){
	index_ = J*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_x[index_];
	}
      }
    }

#pragma omp parallel sections default(none) private(I, J, index_)	\
  shared(p, nodes_x, p_dom_matrix)
    {
#pragma omp section
      {
	J = 0;
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] -= periodic_source * nodes_x[index_];
	  }
	}
      }

#pragma omp section
      {
	J = Ny-1;
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] -= periodic_source * nodes_x[index_];
	  }
	}
      }
    }
  }

  /* s-n periodic flow */
  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(p, nodes_y, p_dom_matrix)
    
    for(J=0; J<Ny; J++){
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_y[index_];
	}
      }
    }

#pragma omp parallel sections default(none) private(I, J, index_)	\
  shared(p, nodes_y, p_dom_matrix)
    {
#pragma omp section 
      {
	I = 0;
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] -= periodic_source * nodes_y[index_];
	  }
	}
      }

#pragma omp section 
      {
	I = Nx - 1;
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] -= periodic_source * nodes_y[index_];
	  }
	}
      }
    }
  }

  return;
}


/* STORE_BND_P_VALUES */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, Nx|Ny) p_*_non_uni: values of pressure to use on next iteration
  for periodic boundary condition.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 06-02-2019
*/
void store_bnd_p_values(Data_Mem *data){

  int I, J;

  const int w_type = data->bc_flow_west.type;
  const int e_type = data->bc_flow_east.type;
  const int s_type = data->bc_flow_south.type;
  const int n_type = data->bc_flow_north.type;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  double *p_W_non_uni = data->p_W_non_uni;
  double *p_E_non_uni = data->p_E_non_uni;
  double *p_S_non_uni = data->p_S_non_uni;
  double *p_N_non_uni = data->p_N_non_uni;

  const double *p_field = NULL;

  /* SIMPLER */
  if(pv_coupling_algorithm == 2){
    p_field = data->p;    
  }
  else{
    p_field = data->p_corr;    
  }
  

#pragma omp parallel sections default(none) private(I, J)		\
  shared(p_W_non_uni, p_E_non_uni, p_S_non_uni, p_N_non_uni, p_field)
  {

    /* w section */
#pragma omp section 
    {
      if(w_type == 5){

	for(J=1; J<Ny-1; J++){
	  p_W_non_uni[J] = 0.5 * (p_field[J*Nx + Nx-3] + p_field[J*Nx + 2]);
	}
      }
    }

    /* e section */    
#pragma omp section 
    {
      if(e_type == 5){

	for(J=1; J<Ny-1; J++){
	  p_E_non_uni[J] = 0.5 * (p_field[J*Nx + Nx-3] + p_field[J*Nx + 2]);
	}
      }
    } 

    /* s section */
#pragma omp section 
    {
      if(s_type == 5){

	for(I=1; I<Nx-1; I++){
	  p_S_non_uni[I] = 0.5 * (p_field[(Ny-3)*Nx + I] + p_field[2*Nx + I]);
	}
      }
    } 

    /* n section */    
#pragma omp section 
    {
      if(n_type == 5){

	for(I=1; I<Nx-1; I++){
	  p_N_non_uni[I] = 0.5 * (p_field[(Ny-3)*Nx + I] + p_field[2*Nx + I]);
	}
      }
    } 

  } // sections
  
  return;  
}

/* SOLVE_FLOW_SIMPLES_TRANSIENT */
/*****************************************************************************/
/**

   Solve flow field for 2D case, transient.

   @param data

   @return void
 */
void solve_flow_SIMPLES_transient(Data_Mem *data){
  
  char msg[SIZE];
  char filename[SIZE];

  const char *results_dir = data->results_dir;  
  const char *flow_solver_name = data->flow_solver_name;

  FILE *fp = NULL;

  /* Auxiliary counter for saving transient data */
  int transient_counter = 1;
  /* Auxiliary iteration counter for ADI loop */    
  int iter;
  /* Main loop counter */  
  int outer_iter = 0;
  int convergence_OK = 0;
  int convergence_turb_OK = 0;

  /* Number of uvp iterations on each sub iteration */
  int iter_uvp;
  int convergence_uvp_OK;

  /* Number of ke iterations on each sub iteration */
  int iter_ke;
  int convergence_ke_OK;
    
  /* Sub iter counter */
  int sub_iter;
  int sub_iter_convergence_OK = 0;

  /* Use pseudo-velocities, SIMPLER */  
  int use_ps_velocities_OK;

  /* These can be adjusted at interruption signal */
  int it_to_start_coupled_physics = data->it_to_start_coupled_physics;
  int it_for_update_coeffs = data->it_for_update_coeffs;
  int it_for_update_k_coeffs = data->it_for_update_k_coeffs;
  int it_for_update_eps_coeffs = data->it_for_update_eps_coeffs;
  int it_upd_coeff_phi = data->it_upd_coeff_phi;
  int max_iter_flow = data->max_iter_flow;
  int max_iter_ADI = data->max_iter_ADI;
  int max_iter_uvp = data->max_iter_uvp;
  int max_iter_ke = data->max_iter_ke;
  int max_sub_iter = data->max_sub_iter_flow;
  int flow_scheme = data->flow_scheme;

  /* Number of sub iterations */
  const int it_to_start_turb = data->it_to_start_turb;
  const int turb_model = data->turb_model;
  const int it_to_update_y_plus = data->it_to_update_y_plus;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;

  const int coupled_physics_OK = data->coupled_physics_OK;
  /* Used for correct ncurses display information */
  const int rstride = coupled_physics_OK ? 5 : 2;
  
  const int animation_flow_OK = data->animation_flow_OK;
  const int animation_flow_update_it = data->animation_flow_update_it;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int res_sigma_iter = data->res_sigma_iter;
  const int write_flow_transient_data_OK = data->write_flow_transient_data_OK;

  u_int64_t time_i, time_f, timer;

  double res_u = 1, res_v = 1, res_p = 1;
  double res_pcorr = 1, res_c = 1, res_k = 1, res_eps = 1;

  /* These can be adjusted at interruption signal */
  double tol_flow = data->tol_flow;
  double tol_ADI = data->tol_ADI;

  /* Residuals for inner uvp - ke cycles */
  double inner_glob_norm[6] = {0};
  
  double *glob_norm = data->sigmas.glob_norm;

  const double time_step = data->dt[3];
  const double transient_save = data->write_flow_transient_data_interval;

  /**** BEGIN: Coupled physics insert *****/
  /* Pointer to phi coeffs function */
  void (*coeffs_phi)(Data_Mem *);
  
  const TEG *teg = &(data->teg);

  FILE *teg_fp = NULL;
  FILE *current_fp = NULL;
  FILE *glob_res_fp = NULL;

  char phi_filename[SIZE];
  char teg_filename[SIZE];
  char current_filename[SIZE];

  const char *glob_res_filename = data->glob_res_filename;

  int sub_convergence_OK = 0, Tol_OK = 0;
  int gL_convergence_OK;

  /* These are for a sub-sub cycle for phi (if needed set max_phi_sub_it > 1) */
  int phi_gL_it;
  int max_iter_phi_gL = data->max_sub_it; /* Extracted from phi section in CONFIG file */
  int convergence_phi_gL_OK = 0;
  int phi_transient_counter = 1;

  const int phi_flow_scheme = data->phi_flow_scheme;
  
  const int write_transient_data_OK = data->write_transient_data_OK;
  const int TEG_OK_summary = data->TEG_OK_summary;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int interface_flux_update_it =
    data->interface_flux_update_it;

  const int it_ref_norm_phi = data->it_ref_norm_phi;

  double norm_phi = 1, norm_phi_ref = 1;
  double gL_diff = 1, gL_diff_ref = 1;

  double phi_gL_inner_glob_norm[2] = {0};

  const double tol_phi = data->tol_phi;
  const double gL_tol = data->gL_tol;
  const double phi_transient_save = data->write_transient_data_interval * 60;
  
  double *phi = data->phi;
  double *phi0 = data->phi0;
  double *gL = data->gL;
  double *gL_calc = data->gL_calc;
  double *gL_guess = data->gL_guess;
  double *dt = data->dt;
  double *sigmas_v= data->sigmas.values;
  /**** END: Coupled physics insert *****/

  void (*ADI_solver)(double *, Coeffs *, 
		     const int, const int, const int, 
		     ADI_Vars *) = data->ADI_solver;
  
  void (*coeffs_flow_u)(Data_Mem *) = NULL;
  void (*coeffs_flow_v)(Data_Mem *) = NULL;

  Sigmas *sigmas = &(data->sigmas);
  Wins *wins = &(data->wins);

  sigmas->time_f = &(time_f);

  /* UPWIND scheme */
  if(flow_scheme == 0){
    coeffs_flow_u = coeffs_flow_u_upwind;
    coeffs_flow_v = coeffs_flow_v_upwind;
  }
  /* QUICK scheme */
  else if(flow_scheme == 1){
    coeffs_flow_u = coeffs_flow_u_quick;
    coeffs_flow_v = coeffs_flow_v_quick;
  }
  /* van Leer scheme */
  else if(flow_scheme == 2){
    coeffs_flow_u = coeffs_flow_u_TVD;
    coeffs_flow_v = coeffs_flow_v_TVD;
  }

  /**** BEGIN: Coupled physics insert *****/
  if(coupled_physics_OK){
    if(phi_flow_scheme == 1){
      coeffs_phi = coeffs_phi_power_law;
    }
    else if(phi_flow_scheme == 2){
      coeffs_phi = coeffs_phi_TVD;
    }
    else{
      coeffs_phi = coeffs_phi_power_law;
    }
  }
  /**** END: Coupled physics insert *****/

  action_on_signal_OK = 0;

  /* SIMPLER */
  if(pv_coupling_algorithm == 2){
    (*coeffs_flow_u)(data);
    (*coeffs_flow_v)(data);
  }

  /* Set sim_time file name and heading */
  if(write_flow_transient_data_OK){
    set_transient_sim_time_filename(filename, "sim_time_flow", 0, results_dir);

    open_check_file(filename, &fp, wins, 0);
    fprintf(fp, "%% Time in seconds\n");
    fprintf(fp, "sim_time_flow_var = [\n");
    fclose(fp);
  }

  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  solve_flow_pre_calc_jobs(data);

  /**** BEGIN: Coupled physics insert *****/
  if(coupled_physics_OK){
    
    solve_phi_pre_calc_jobs(data);

    if(write_transient_data_OK){
      set_transient_sim_time_filename(phi_filename, "sim_time",
				      0, results_dir);
      
      open_check_file(phi_filename, &fp, wins, 0);
      fprintf(fp, "sim_time_var = [\n");          
      fclose(fp);
      
      if(TEG_OK_summary){
	set_transient_sim_time_filename(teg_filename, "teg_vars",
					0, results_dir);
	
	open_check_file(teg_filename, &teg_fp, wins, 0);
	fprintf(teg_fp, "%% %8s %8s %8s %8s %8s %8s ...\n",
		"t (min)", "I (mA)", "QLi (W)", "Q0i (W)", "TLi (K)", "T0i (K)");
	fprintf(teg_fp, "teg_var = [\n");	
	fclose(teg_fp);
	
	set_transient_sim_time_filename(current_filename, "IVP", 
					0, results_dir);
	
	open_check_file(current_filename, &current_fp, wins, 0);
	fprintf(current_fp, "IVP_var = [\n");
	fclose(current_fp);
	
      }
    }
    
    if(!data->read_tmp_file_OK){
      dt[6] = 0;
    }    
  }
  /**** END: Coupled physics insert *****/


  /* This is the main (outer) loop */
  do{
    
    set_mu(data, outer_iter);

    /* Update *0 fields */
    store_flow_fields(data);

    /* Setting iteration counters and flags for the inner loops */
    sub_iter = 0;
    sub_iter_convergence_OK = 0;
    
    /**** BEGIN: Coupled physics insert *****/
    if(coupled_physics_OK){

      sub_convergence_OK = 0;
      
      norm_phi_ref = 1;
      
      if(solve_phase_change_OK){
	/* gL_diff Max difference between guess and 
	   calculated value of gL */
	gL_diff = 1;
	gL_diff_ref = 1;
	gL_convergence_OK = 0;
      }
      else{
	gL_convergence_OK = 1;
      }
      
      /* Store phi values */
      copy_array(phi, phi0, Nx, Ny, Nz);
      
      /* Update coeffs before starting next outer iteration */
      if(solve_phase_change_OK){
	/* gL must be updated before average physical properties calculation */
	update_gL_guess(data);
	avg_phys_prop(data);
	compute_enthalpy_diff(data);
      }
      
      /* Update coefficients and interface flow */
      coeffs_phi(data);
      compute_phi_SC_at_interface(data);
    }
    /**** END: Coupled physics insert *****/

    
    
    /* Fully implicit scheme */
    do{

      /* Set iteration counter for uvp inner loop */
      iter_uvp =0;
      convergence_uvp_OK = 0;

      while(iter_uvp < max_iter_uvp && !convergence_uvp_OK){
      
	set_bc_flow(data);

	if(pv_coupling_algorithm == 2){
	  use_ps_velocities_OK = 1;
	  /* SIMPLER: COMPUTE PSEUDO-VELOCITIES */
	  compute_pseudo_velocities(data);

	  /* SIMPLER: SOLVE PRESSURE EQUATION */
	  coeffs_flow_p(data, use_ps_velocities_OK);

	  res_p = 1;
	  iter = 1;

	  while((iter < max_iter_ADI) && (res_p > tol_ADI)){
	    (*ADI_solver)(data->p,
			  &(data->coeffs_p),
			  Nx, Ny, 0,
			  &(data->ADI_vars));
	  
	    res_p = compute_norm_glob_res(data->p, &(data->coeffs_p), Nx, Ny);
	    iter++;
	  }
	}
      
	/* 1: SOLVE MOMENTUM EQUATIONS */
	
	// u loop
	(*coeffs_flow_u)(data);
      
	res_u = 1;
	iter = 1;
      
	while((iter < max_iter_ADI) && (res_u > tol_ADI)){
	
	  if(iter % it_for_update_coeffs == 0){
	    (*coeffs_flow_u)(data);
	  }
	
	  (*ADI_solver)(data->u, 
			&(data->coeffs_u), 
			nx, Ny, 0, 
			&(data->ADI_vars));
	
	  res_u = compute_norm_glob_res(data->u, &(data->coeffs_u), nx, Ny);
	  iter++;
	}
      
	// v loop
	(*coeffs_flow_v)(data);
      
	res_v = 1;
	iter = 1;
      
	while((iter < max_iter_ADI) && (res_v > tol_ADI)){
	
	  if(iter % it_for_update_coeffs == 0){
	    (*coeffs_flow_v)(data);
	  }
	
	  (*ADI_solver)(data->v, 
			&(data->coeffs_v), 
			Nx, ny, 0, 
			&(data->ADI_vars));
	
	  res_v = compute_norm_glob_res(data->v, &(data->coeffs_v), Nx, ny);
	  iter++;
	}
      
	/* 2: SOLVE PRESSURE CORRECTION EQUATION */
	use_ps_velocities_OK = 0;

	/* To set pressure correction values to zero was found to improve convergence */
	if(pv_coupling_algorithm != 2){
	  fill_array(data->p_corr, Nx, Ny, Nz, 0);
	}

	coeffs_flow_p(data, use_ps_velocities_OK);
      
	res_pcorr = 1;
	iter = 1;
      
	while((iter < max_iter_ADI) && (res_pcorr > tol_ADI)){
	  (*ADI_solver)(data->p_corr, 
			&(data->coeffs_p), 
			Nx, Ny, 0, 
			&(data->ADI_vars));
	
	  res_pcorr = compute_norm_glob_res(data->p_corr, 
					    &(data->coeffs_p), Nx, Ny);
	  iter++;
	}
      
	/* 3: CORRECT PRESSURE AND VELOCITIES */
	update_field(data);

	/* Store values to use with periodic boundary condition, 
	   because p_corr values are set to zero at every iteration */
	store_bnd_p_values(data);

	res_c = continuity_residual(data);
      
	/* Store residuals for inner loop */

	inner_glob_norm[0] = res_u;
	inner_glob_norm[1] = res_v;
	inner_glob_norm[3] = res_c;

	convergence_uvp_OK = inner_glob_norm[0] < tol_flow &&
	  inner_glob_norm[1] < tol_flow && inner_glob_norm[3] < tol_flow;

	iter_uvp++;
      }
      
      /* 4: SOLVE k-eps EQUATIONS */
      // k-eps loop
      if(turb_model && (outer_iter >= it_to_start_turb)){

	/* Set iteration counter for ke inner loop */
	iter_ke = 0;
	convergence_ke_OK = 0;

	while(iter_ke < max_iter_ke && !convergence_ke_OK){
	
	  /* Filter nodes with low y_plus out of the calculation */
	  if(outer_iter % it_to_update_y_plus == 0){
	    filter_y_plus(data);
	  }

	  // k loop
	  compute_turb_aux_fields(data);	       
	  set_coeffs_k(data);
	  set_bc_turb_k(data);
	
	  res_k = 1;
	  iter = 1;
	  
	  while((iter < max_iter_ADI) && (res_k > tol_ADI)){
	  
	    if(iter % it_for_update_k_coeffs == 0){
	      compute_turb_aux_fields(data);
	      set_coeffs_k(data);
	    }
	  
	    (*ADI_solver)(data->k_turb, &(data->coeffs_k),
			  Nx, Ny, 0, &(data->ADI_vars));
	    
	    res_k = compute_norm_glob_res(data->k_turb,
					  &(data->coeffs_k), Nx, Ny);
	    iter++;
	  }
	
	  filter_small_values_omp(data->k_turb, Nx, Ny, Nz, SMALL_VALUE, 1);
	
	  // eps loop
	  compute_turb_aux_fields(data);
	  set_coeffs_eps(data);
	  set_bc_turb_eps(data);
	
	  res_eps = 1;
	  iter = 1;
	  
	  while((iter < max_iter_ADI) && (res_eps > tol_ADI)){
	  
	    if(iter % it_for_update_eps_coeffs == 0){
	      compute_turb_aux_fields(data);
	      set_coeffs_eps(data);
	    }
	  
	    (*ADI_solver)(data->eps_turb, &(data->coeffs_eps),
			  Nx, Ny, 0, &(data->ADI_vars));
	    
	    res_eps = compute_norm_glob_res(data->eps_turb,
					    &(data->coeffs_eps), Nx, Ny);
	    iter++;
	  }
       
	  filter_small_values_omp(data->eps_turb, Nx, Ny, Nz, SMALL_VALUE, 1);	
	
	  inner_glob_norm[4] = res_k;
	  inner_glob_norm[5] = res_eps;
	
	  convergence_ke_OK = inner_glob_norm[4] < tol_flow &&
	    inner_glob_norm[5] < tol_flow;

	  iter_ke++;
	}

	/* Compute mu_turb_at velocity nodes 
	   for next iteration of the u, v, p cycle */
	compute_mu_turb_at_vel_nodes(data);
      
      } // end if(turb_model && (outer_iter >= it_to_start_turb)){

      /* Use coeffs with the latest values of fields to evaluate sub-iter convergence */
      (*coeffs_flow_u)(data);
      (*coeffs_flow_v)(data);

      glob_norm[0] = compute_norm_glob_res(data->u, &(data->coeffs_u), nx, Ny);
      glob_norm[1] = compute_norm_glob_res(data->v, &(data->coeffs_v), Nx, ny);
      /* res_c hasn't changed from the one calculated inside uvp inner loop
	 because velocity values are the same */
      glob_norm[3] = inner_glob_norm[3];
		
      if(turb_model && (outer_iter >= it_to_start_turb)){

	/* Use coeffs with the latest values of fields to evaluate outer loop convergence */
	compute_turb_aux_fields(data);
	set_coeffs_k(data);
	set_coeffs_eps(data);

	glob_norm[4] = compute_norm_glob_res(data->k_turb, &(data->coeffs_k), Nx, Ny);
	glob_norm[5] = compute_norm_glob_res(data->eps_turb, &(data->coeffs_eps), Nx, Ny);

	convergence_turb_OK = glob_norm[4] < tol_flow
	  && glob_norm[5] < tol_flow;
      }
      else{
	convergence_turb_OK = 1;
      }


      /* 5: SOLVE phi-gL EQUATIONS */
      /**** BEGIN: Coupled physics insert *****/
      if(coupled_physics_OK && (outer_iter >= it_to_start_coupled_physics)){

	phi_gL_it = 0;
	convergence_phi_gL_OK = 0;

	while(phi_gL_it < max_iter_phi_gL && !convergence_phi_gL_OK){
	
	  /* Centering velocities and computing in main nodes 
	     (in turbulence these are not fully performed so it is needed here) */
	  center_vel_components(data);
	  compute_vel_at_main_cells(data);
	
	  /* Call solve_flow_jobs with proper update flag */
	  if(sub_iter % it_upd_coeff_phi == 0){
	  
	    /* gL must be updated before average physical properties calculation */
	    if(solve_phase_change_OK){
	      update_gL_guess(data);
	      avg_phys_prop(data);
	      compute_enthalpy_diff(data);
	    }
	  
	    /* Update coeffs */
	    coeffs_phi(data);
	  
	    /* Computation of flow at interfaces must be after an update
	       of the coefficients */
	    if(sub_iter % interface_flux_update_it == 0){
	      compute_phi_SC_at_interface(data);
	    }
	  }
	
	  /* Solve for phi */
	  norm_phi = solve_phi_jobs(data);
	
	  if(solve_phase_change_OK){
	    /* Update phase average properties */
	    compute_gL_value(data, gL_calc);
	    gL_diff = max_abs_matrix_diff(gL_calc, gL_guess, Nx, Ny);
	  
	    if(sub_iter == it_ref_norm_phi){
	       gL_diff_ref = (gL_diff > 1e-9) ? gL_diff : 1;
	    }
	  
	    sigmas_v[8] = gL_diff / gL_diff_ref;
	  
	    if(sigmas_v[8] < gL_tol){
	      gL_convergence_OK = 1;
	    }
	  
	    if(sub_iter <= it_ref_norm_phi){
	      gL_convergence_OK = 0;
	    }
	  
	  }
	
	  /* Comparison with tolerances must be with 
	     normalized residuals */
	  if(sub_iter == it_ref_norm_phi){
	    norm_phi_ref = norm_phi;        
	  }
	
	  sigmas_v[7] = norm_phi / norm_phi_ref;
	  Tol_OK = (sigmas_v[7] <= tol_phi) && gL_convergence_OK;
	
	  if(sub_iter <= it_ref_norm_phi){
	    Tol_OK = 0;
	  }
	
	  if(Tol_OK || (sub_iter >= max_sub_iter)){
	    sub_convergence_OK = 1;
	  }

	  /* Setting criteria for exiting the sub (independent) cycle phi_gL */
	  phi_gL_inner_glob_norm[0] = compute_norm_glob_res(data->phi, &(data->coeffs_phi), Nx, Ny);
	  convergence_phi_gL_OK = phi_gL_inner_glob_norm[0] < tol_phi;
	  
	  if(solve_phase_change_OK){
	  
	    phi_gL_inner_glob_norm[1] = compute_avg_gL_diff(data);
	    
	    convergence_phi_gL_OK = convergence_phi_gL_OK && phi_gL_inner_glob_norm[1] < gL_tol;
	  }
	  
	  phi_gL_it++;
	  
	} // end while(phi_gL_it < max_iter_phi_gL && !convergence_phi_gL_OK)
      }
      /**** END: Coupled physics insert *****/
      
      sub_iter_convergence_OK = glob_norm[0] < tol_flow
	&& glob_norm[1] < tol_flow && glob_norm[3] < tol_flow
	&& convergence_turb_OK;

      /**** BEGIN: Coupled physics insert *****/
      if(coupled_physics_OK && (outer_iter >= it_to_start_coupled_physics)){

	sub_iter_convergence_OK = sub_iter_convergence_OK && sub_convergence_OK;
	
      }
      /**** END: Coupled physics insert *****/
      
      sub_iter++;

    } while(sub_iter < max_sub_iter && !sub_iter_convergence_OK); // End sub_iter

    dt[0] += time_step;
    /* For internal phi data functions */
    dt[6] = dt[0];

    /**** BEGIN: Coupled physics insert *****/
    if(coupled_physics_OK && (outer_iter >= it_to_start_coupled_physics)){
      /* Update gL value */
      if(solve_phase_change_OK){
	copy_array(gL_guess, gL, Nx, Ny, Nz);
      }

      /* Compute normalized global residues */
      if(outer_iter % res_sigma_iter == 0){
	glob_norm[7] =
	  compute_norm_glob_res(data->phi, &(data->coeffs_phi), Nx, Ny);
	glob_norm[8] = compute_avg_gL_diff(data);
      }
    }
    /**** END: Coupled physics insert *****/

    
    /* Here process action upon signal raise */
    if(action_on_signal_OK){
      process_action_on_signal(data, &convergence_OK, &sub_iter_convergence_OK);
      
      flow_scheme = data->flow_scheme;
      /* UPWIND scheme */
      if(flow_scheme == 0){
	coeffs_flow_u = coeffs_flow_u_upwind;
	coeffs_flow_v = coeffs_flow_v_upwind;
      }
      /* QUICK scheme */
      else if(flow_scheme == 1){
	coeffs_flow_u = coeffs_flow_u_quick;
	coeffs_flow_v = coeffs_flow_v_quick;
      }
      /* van Leer scheme */
      else if(flow_scheme == 2){
	coeffs_flow_u = coeffs_flow_u_TVD;
	coeffs_flow_v = coeffs_flow_v_TVD;
      }
      
      /* Update in case of modification the new values */
      max_iter_flow = data->max_iter_flow;
      max_sub_iter = data->max_sub_iter_flow;
      max_iter_ADI = data->max_iter_ADI;
      it_for_update_coeffs = data->it_for_update_coeffs;
      max_iter_uvp = data->max_iter_uvp;

      it_for_update_k_coeffs = data->it_for_update_k_coeffs;
      it_for_update_eps_coeffs = data->it_for_update_eps_coeffs;
      max_iter_ke = data->max_iter_ke;

      it_upd_coeff_phi = data->it_upd_coeff_phi;
      max_iter_phi_gL = data->max_sub_it;
      
      it_to_start_coupled_physics = data->it_to_start_coupled_physics;
      
      action_on_signal_OK = 0;
    }

    time_i = time_f;		
    time_f = time_f + elapsed_time(&timer);
    
    if(outer_iter % res_sigma_iter == 0){

      /* Leave outside this IF clause if you need it for convergence criteria
	 Otherwise leave here for optimization purposes */
      glob_norm[6] = mass_io_balance(data);
      
      sigmas->it = outer_iter;
      sigmas->sub_it = sub_iter;

      /* Pass glob_norm values as sigmas to see some graphs */
      sigmas_v[0] = glob_norm[0]; // gu
      sigmas_v[1] = glob_norm[1]; // gv
      sigmas_v[3] = glob_norm[3]; // gc
      
      if(turb_model){
	sigmas_v[4] = glob_norm[4];
	sigmas_v[5] = glob_norm[5];
      }

      sigmas_v[6] = glob_norm[6]; // sMB
		  
      print_flow_it_info(msg, SIZE, rstride,
			 flow_solver_name, outer_iter, max_iter_flow,
			 time_i, time_f, turb_model, sigmas,
			 wins);
      
      /**** BEGIN: Coupled physics insert *****/
      if(coupled_physics_OK){
	
	table_header_phi_it(wins);
	print_phi_it_info(outer_iter, max_iter_flow,
			  time_i, time_f,
			  sigmas,
			  wins);
      }
      /**** END: Coupled physics insert *****/

      update_graph_window(data);

      write_sfile(data);
      
      write_norm_glob_res(data);

#ifdef AVGIV_OK
      write_in_vel(data);
#endif      
    }
    
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);
      memset(msg, '\0', sizeof(msg));
      snprintf(msg, SIZE, 
	       "flow: it = %7d; "
	       "su = %9.3e; sv = %9.3e, sw = %9.3e; sc = %9.3e; sk = %9.3e; se = %9.3e; sMB = %9.3e", 
	       outer_iter,
	       sigmas_v[0], sigmas_v[1], sigmas_v[2],
	       sigmas_v[3], sigmas_v[4], sigmas_v[5], sigmas_v[6]);

      sigmas->it = outer_iter;
      write_to_log_file(data, msg);
      
      /**** BEGIN: Coupled physics insert *****/
      if(coupled_physics_OK){
	memset(msg, '\0', sizeof(msg));
	snprintf(msg, SIZE, 
		 "phi: it = "
		 "%7d; sphi = %9.3e; sgL = %9.3e", 
		 outer_iter, sigmas_v[7], sigmas_v[8]);

	sigmas->it = outer_iter;
	write_to_log_file(data, msg);
      }
      /**** END: Coupled physics insert *****/
      
      timer_flag_OK = 0;
    }
    
    /* Updating the server */
    if((launch_server_OK) && 
       (outer_iter % server_update_it == 0)){
      update_server(data);
    }
    
    /* Updating the animation */
    if((animation_flow_OK) && 
       (outer_iter % animation_flow_update_it == 0)){
      /* Computing U, V with cut cells as is, i.e., not centered at uncut faces */
      center_vel_components(data);
      compute_vel_at_main_cells(data);
      update_anims((void *) data);
      
    }
   
    /* Saving transient data */
    if( (write_flow_transient_data_OK) &&
	(dt[0] >= data->tsaves[0]) &&
	(dt[0] <= data->tsaves[1]) ){
      
      if( dt[0] >= data->tsaves[0] + transient_save * (transient_counter - 1) ){
	
	save_flow_transient_data(data, filename);
	
	transient_counter++;
	
      }
    }

    /**** BEGIN: Coupled physics insert *****/
    if(coupled_physics_OK){
      
      if( (write_transient_data_OK) &&
	  (dt[0] >= data->tsaves[2]) &&
	  (dt[0] <= data->tsaves[3]) ){
	
	if( dt[0] >= data->tsaves[2] + phi_transient_save * (phi_transient_counter - 1) ){
	  
	  save_transient_data(data, phi_filename);
	
	  if(TEG_OK_summary){
	    /* Here compute electric current */ 
	    solve_current(data, current_filename);
	    save_transient_teg_data(data, teg_filename);
	  }

	  phi_transient_counter++;
	  
	}
	
      }
      
    }
    /**** END: Coupled physics insert *****/
    
    outer_iter++;
    
  } while(outer_iter < max_iter_flow && !convergence_OK);

  /* Save iteration */
  sigmas->flow_it = outer_iter - 1;

  if(write_flow_transient_data_OK){
    
    open_check_file(filename, &fp, wins, 1);
    fprintf(fp, "];\n");
    fclose(fp);
      
  }

#ifdef AVGIV_OK
  close_in_vel(data);
#endif
  
  solve_flow_post_calc_jobs(data);


  /**** BEGIN: Coupled physics insert *****/
  if(coupled_physics_OK){
    /* Save iteration */
    sigmas->phi_it = sigmas->flow_it;
    
    solve_phi_post_calc_jobs(data);
  
    if(write_transient_data_OK){

      open_check_file(phi_filename, &fp, wins, 1);
      fprintf(fp, "];\n");
      fclose(fp);
      
      if(TEG_OK_summary){

	open_check_file(teg_filename, &teg_fp, wins, 1);
	fprintf(teg_fp, "];\n");
	fclose(teg_fp);

	open_check_file(current_filename, &current_fp, wins, 1);
	fprintf(current_fp, "];\n");
	fclose(current_fp);
      }
      
    }
  
    close_sfile(data);

    open_check_file(glob_res_filename, &glob_res_fp, wins, 1);
    fprintf(glob_res_fp, "];\n");
    fclose(glob_res_fp);   
  }
  /**** END: Coupled physics insert *****/


#ifdef DISPLAY_OK
  wprintw(wins->disp, "Simulation time (s) = %g\n", dt[0]);
  wrefresh(wins->disp);
#endif
  
  return;

}

/* ADD_U_MOMENTUM_SOURCE */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNy) coeffs_u.b: add custom source term.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 13-06-2019
*/
void add_u_momentum_source(Data_Mem *data){

  int J, i, index_u;

  const int Ny = data->Ny;
  const int nx = data->nx;

  const int *u_dom_matrix = data->u_dom_matrix;

  double r_sqr, x_rel, y_rel;

  const double force_mag = 122000;
  const double Lx = data->Lx;
  const double Ly = data->Ly;

  double *b_u = data->coeffs_u.b;
  
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *vol_x = data->vol_x;

  /* u momentum equation */
#pragma omp parallel for default(none) private(i, J, index_u, r_sqr, x_rel, y_rel) \
  shared(u_dom_matrix, nodes_x_u, nodes_y_u, b_u, vol_x)

  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      index_u = J*nx + i;
      
      if(u_dom_matrix[index_u] == 0){
	x_rel = 0.5 * Lx - nodes_x_u[index_u];
	y_rel = 0.5 * Ly - nodes_y_u[index_u];
	r_sqr = x_rel * x_rel + y_rel * y_rel;
	// 	v_theta = (-u[index_u] * y_rel + v_at_faces_x[index_u] * x_rel) / r_val;
	b_u[index_u] += (-force_mag * y_rel) * vol_x[index_u] / r_sqr;
	//     b_u[index_u] += (-force_mag * y_rel  - rho * v_theta * v_theta * x_rel) * vol_x[index_u] / r_sqr;
      }
    }
  }

  return;
}

/* ADD_V_MOMENTUM_SOURCE */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, Nxny) coeffs_v.b: add custom source term.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 13-06-2019
*/
void add_v_momentum_source(Data_Mem *data){

  int I, j, index_v;

  const int Nx = data->Nx;
  const int ny = data->ny;

  const int *v_dom_matrix = data->v_dom_matrix;

  double r_sqr, x_rel, y_rel;

  const double force_mag = 122000;
  const double Lx = data->Lx;
  const double Ly = data->Ly;

  double *b_v = data->coeffs_v.b;
  
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *vol_y = data->vol_y;

  /* v momentum equation */
#pragma omp parallel for default(none) private(I, j, index_v, r_sqr, x_rel, y_rel) \
  shared(v_dom_matrix, nodes_x_v, nodes_y_v, b_v, vol_y)

  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){

      index_v = j*Nx + I;
      
      if(v_dom_matrix[index_v] == 0){
	x_rel = 0.5 * Lx - nodes_x_v[index_v];
	y_rel = 0.5 * Ly - nodes_y_v[index_v];
	r_sqr = x_rel * x_rel + y_rel * y_rel;
	// 	v_theta = (-u_at_faces_y[index_v] * y_rel + v[index_v] * x_rel) / r_val;
	b_v[index_v] += (force_mag * x_rel) * vol_y[index_v] / r_sqr;
	//     b_v[index_v] += (force_mag * x_rel - rho * v_theta * v_theta * y_rel) * vol_y[index_v] / r_sqr;
      }
    }
  }

  return;
}
