/* HEADERS */
#ifdef DISPLAY_OK
#include <ncurses.h>
#else
#include <stdio.h>
#define WINDOW int
#define chtype long int
#endif
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/time.h>
#include <omp.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <matio.h>

#ifdef NAN
/* NAN is supported */
#endif
#ifdef INFINITY
/* INFINITY is supported */
#endif

/* PARAMETERS */
#define SMALL_VALUE 1e-10
#define SIZE 256
#define MAX_CLIENTS 30

/* DEFINES */
/* Simulation data for residues */
#define DSIZE 400
#define CUT_VALUE 1
#define START_VALUE 1e-2
/* chtype for the graphs */
#define SUCHTYPE COLOR_PAIR(5) | A_BOLD | ACS_DEGREE
#define SVCHTYPE COLOR_PAIR(2) | A_BOLD | ACS_DEGREE
#define SWCHTYPE COLOR_PAIR(4) | A_BOLD | ACS_DEGREE
#define SCCHTYPE COLOR_PAIR(3) | ACS_DIAMOND
#define SKCHTYPE COLOR_PAIR(1) | A_BOLD | '+'
#define SECHTYPE COLOR_PAIR(4) | A_BOLD | '+'
#define SMBCHTYPE COLOR_PAIR(1) | ACS_DIAMOND
#define SPHICHTYPE COLOR_PAIR(2) | A_BOLD | '+'
#define SGLCHTYPE COLOR_PAIR(5) | A_BOLD | '+'
/* y-axis exponent ranges 
   for enable scale adaptation in linear scheme */
#define MPOW_H 10
#define MPOW_L -20
/* y-axis exponent ranges in semilog y graph */
#define XMAX 3
#define XMIN -7
/* To format ETA string*/
#define ETA_MSG 27

/* STRUCTS */
typedef struct {

  mat_t *matfp_tr = NULL;
  
  matvar_t *U = NULL;
  matvar_t *V = NULL;
  matvar_t *W = NULL;
  matvar_t *p = NULL;
  matvar_t *k = NULL;
  matvar_t *e = NULL;

  matvar_t *tflow = NULL;

  matvar_t *phi = NULL;
  matvar_t *gL = NULL;

  matvar_t *tphi = NULL;

  matvar_t *current = NULL;
  matvar_t *voltage = NULL;
  matvar_t *power = NULL;
  
} MATIOS;

typedef struct {

  int curves;
  int ntm;
  
  int *TE_OK;
  int *ntppm;
  
  double I;
  double V;
  double P;
  
  double *QL;
  double *Q0;
  double *TfH;
  double *TfC;
  double *hL;
  double *h0;
  double *TL;
  double *T0;
  double *ALPHA;
  double *K;
  double *Re;
  double *RHOe;
  double *k;
  double *A;
  double *L;
  
} TEG;

typedef struct {

  WINDOW *warns;
  WINDOW *prog;
  WINDOW *msgs;
  WINDOW *upds;
  WINDOW *graphs;
  WINDOW *disp;
  
  int Nx, Ny, Nz;
  
  int semilog_OK;
  
  int g_height;
  int g_width;

  int res_sigma_iter;

  int it_i, it_f;
  int inter;
  int set_i;
  int count_range;

  int offset_i;
  int offset_j;
  
  int mpow;
  int mpow_new;

  int axes[6];

  int *flow_done_OK;
  int *turb_model;

  int *iter;
  int *stride;

  double dy;
  double smax;

  double *svalues;
  
  chtype *ch_m;
  chtype *ch_v;
  
} Wins;

/* STRUCTS */
/**
   Iterations and residual information.
 */
typedef struct {
  int it = 0;
  int sub_it = 0;
  int flow_it = 0;
  int phi_it = 0;

  u_int64_t *time_f;
  
  double refs[9] = {1, 1, 1, 1, 1, 1, 1, 1};
  double values[9] = {NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN};
  double glob_norm[9] = {NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN};
  double flow_values[7] = {NAN, NAN, NAN, NAN, NAN, NAN, NAN};
  double flow_glob_norm[7] = {NAN, NAN, NAN, NAN, NAN, NAN, NAN};
  double phi_values[2] = {NAN, NAN};
  double phi_glob_norm[2] = {NAN, NAN};
} Sigmas;

typedef struct {
  const char *server_name;
  int port;
  int client_socket[MAX_CLIENTS];
  int max_clients;
  int size_x;
  int size_y;
  int size_max;
  int bans;
  fd_set *readfds;
  double *u;
  double *v;
  double *w;
  double *U;
  double *V;
  double *W;
} Server_Data;

typedef struct{
  double *a_T;
  double *b_T;
  double *c_T;
  double *d_T;
  double *c_p;
  double *d_p;
} ADI_Vars;

typedef struct{
  double *aP;
  double *b;
  double *aE;
  double *aW;
  double *aN;
  double *aS;
  double *aT;
  double *aB;
} Coeffs;

typedef struct{
  int parts;
  int Nperx;
  int Npery;
  int Nperz;
  int *alpha;
  double *per;
  double *perx;
  double *pery;
  double *perz;
  double *beta;
  double *gamma;
  double *phi_b;
} Boundary_Info;

typedef struct{
  int smooth_OK;
  int seg;
  int *N;
  double *L;
  double *per;
  double *deltas;
} Grid_Info;

/** Boundary condition for flow variables.
 */
typedef struct{
  int type;
  int parabolize_profile_OK;
  double v_value[3];
  double k_value;
  double eps_value;
  int ref_p_node_OK;
  double p_value;
} b_cond_flow;

/**
   Structure that is passed to the majority of functions.
 */
typedef struct{
  void *gr_void;

  const char *log_filename;
  const char *tmp_filename;
  const char *results_dir;
  const char *geom_filename;
  const char *flow_solver_name; /**< One from SIMPLE, SIMPLE_cang, SIMPLER, SIMPLEC. */
  const char *sigmas_filename;
  const char *glob_res_filename;
  const char *post_process_filename;
  const char *avg_iv_filename; /**< Average inlet velocity file for periodic flow simulations. */
  const char *res_filename;
  const char *res_tr_filename;
  
  int Nx; /**< Nodes in x direction, p grid. */
  int Ny; /**< Nodes in y direction, p grid. */
  int Nz; /**< Nodes in z direction, p grid. */

  int nx; /**< nx = Data_Mem::Nx - 1 */
  int ny; /**< ny = Data_Mem::Ny - 1 */
  int nz; /**< nz = Data_Mem::Nz - 1 */
  
  int curves;
  int coupled_physics_OK;
  int lines;
  int solve_flow_OK;
  int read_tmp_file_OK;
  int read_MAT_file_OK;
  int read_y_wall_OK;
  int read_cut_cell_data_OK;
  int max_it;
  int max_sub_it;
  int steady_state_OK;
  int write_transient_data_OK;
  int it_ref_norm_phi;
  int it_upd_coeff_phi;
  int write_data_on_file_OK;
  int write_cut_cell_data_on_file_OK;
  int write_coeffs_OK;
  int num_omp_threads;
  int graphs_flow_OK;
  int graphs_phi_OK;
  int animation_phi_OK;
  int animation_phi_update_it;
  int animation_flow_OK;
  int animation_flow_update_it;
  int peek_flow_OK;
  int peek_phi_OK;
  int launch_server_OK;
  int server_update_it;
  int interface_flux_update_it;
  int interface_molecular_flux_model;
  int pv_coupling_algorithm; /**< Pressure velocity coupling algorithm, 0: SIMPLE, 1: SIMPLE_cang, 2: SIMPLER, 3: SIMPLEC. */
  int solid_boundary_cell_count; /**< Number of cells on array Data_Mem::sf_boundary_solid_indexs. */
  int Dirichlet_boundary_cell_count;
  int Isoflux_boundary_cell_count;
  int Conjugate_boundary_cell_count;
  int consider_wall_shear_OK;
  int it_for_update_coeffs; /**< Recalculation of coefficients inside ADI loop, for u, v, w (not p nor p_corr). */
  int it_for_update_k_coeffs; /**< Recalculation of coefficients inside ADI loop, for Data_Mem::k_turb. */
  int it_for_update_eps_coeffs; /**< Recalculation of coefficients inside ADI loop, for Data_Mem::eps_turb. */
  int mu_initial_iter_flow;
  int mu_final_iter_flow;
  int res_sigma_iter; /**< Iterations between print of sigmas and global residues to screen, 
		       and sigmas print to file. */
  int post_process_OK;
  int w_count;
  int e_count;
  int s_count;
  int n_count;
  int b_count;
  int t_count;
  int turb_model; /**< 0: Laminar, 1: AKN, 2: CG. */
  int flow_done_OK; /**< True if flow was already solved. */
  int centered_vel_comp_OK;  /**< 1 if velocity values are at face center. */

  int u_slave_count;
  int v_slave_count;
  int w_slave_count;
  /*
  int u_master_size;
  int v_master_size;
  */
  int max_iter_flow; /**< Main loop iterations for flow variables. */
  int max_iter_ADI; /**< ADI loop iterations for flow solver. */
  int max_iter_uvp; /**< Iterations for uvp sub-loop. */
  int max_iter_ke; /**< Iterations for ke sub-loop. */
  int display_interval;
  int solve_phase_change_OK; /**< Solve phase change variables. */
  int u_sol_factor_count; /**< Number of u cells for which source terms are added due to the presence of a solid surface inside the cell. Is equal to the number of cells cut as indicated in Data_Mem::u_cut_matrix array. */
  int v_sol_factor_count; /**< Idem to Data_Mem::u_sol_factor_count. */
  int w_sol_factor_count; /**< Idem to Data_Mem::v_sol_factor_count. */
  int turb_boundary_cell_count; /**< Number of cells on Data_Mem::turb_boundary_indexs array. */
  int it_to_start_turb; /**< Iterations to start turbulent calculations. */
  int it_to_start_coupled_physics; /**< Iterations to start solving for phi in a coupled physics mode. */
  int use_y_wall_OK; /**< If true scale initial Data_Mem::k_turb value by Data_Mem::y_wall/max_y_wall. */

  int it_to_update_y_plus; /**< Iterations between recalculation of Data_Mem::y_plus values. */
  int set_ref_p_node;
  int ref_p_node_index;
  int p_gradient_direction;
  int p_source_orientation; /**< Set to 0 for no periodic source, 1 for w->e periodic
			       flow, 2 for s->n periodic flow, 3 for b->t periodic flow. */
  int flow_scheme; /**< Convective term scheme, 0: Upwind, 1: QUICK. */
  int add_p_source_when_finish_OK;

  int u_cut_face_Fy_count; /**< Number of faces_y on u cell where v is interpolated using a point
			      on the solid surface. */
  int u_cut_face_Fz_count; /**< Number of faces_z on u cell where w is interpolated using a point
			      on the solid surface. */
  int v_cut_face_Fx_count; /**< Number of faces_x on v cell where u is interpolated using a point
			      on the solid surface. */
  int v_cut_face_Fz_count; /**< Number of faces_z on v cell where w is interpolated using a point
			      on the solid surface. */
  int w_cut_face_Fx_count; /**< Number of faces_x on w cell where u is interpolated using a point
			      on the solid surface. */
  int w_cut_face_Fy_count; /**< Number of faces_y on w cell where v is interpolated using a point
			      on the solid surface. */

  int p_nodes_in_solid_count;
  int flow_steady_state_OK; /**< If true, solves steady state flow equations. */
  int write_flow_transient_data_OK; /**< If true, write transient flow simulation results. */
  int max_sub_iter_flow; /**< Maximum number of sub iterations for transient flow simulation. */

  int diff_u_face_x_count;
  int diff_v_face_y_count;
  int diff_w_face_z_count;

  int add_momentum_source_OK;

  int phi_flow_scheme;
  int phi_dF_OK;

  int Dirichlet_Isoflux_convective_boundary_OK;

  int solve_3D_OK; /**< Solve fields for third dimension. */

  int TEG_OK_summary; /**< 0 if no TEM module is present, 1 otherwise. */

  int *quadric_OK; /**< Flasg true for computing quadric or false if direct polynomial assignment. */
  int *curve_is_solid_OK;
  int *solve_this_phase_OK;
  int *Isoflux_OK;
  int *sf_boundary_solid_indexs; /**< Non-fluid cells that are solved (based on Data_Mem::P_solve_phase_OK) that have a fluid neighbour. */
  int *I_bm;
  int *J_bm;
  int *K_bm;
  int *Dirichlet_boundary_indexs;
  int *I_Dirichlet_v;
  int *J_Dirichlet_v;
  int *K_Dirichlet_v;
  int *Isoflux_boundary_indexs;
  int *I_Isoflux_v;
  int *J_Isoflux_v;
  int *K_Isoflux_v;
  int *Conjugate_boundary_indexs;
  int *I_Conjugate_v;
  int *J_Conjugate_v;
  int *K_Conjugate_v;
  int *cut_matrix; /**< Curve index if cell is cut, 0 otherwise. */
  int *domain_matrix; /**< Curve index if cell is inside curve, 0 otherwise. */
  
  int *P_solve_phase_OK;
  int *E_solve_phase_OK;
  int *W_solve_phase_OK;
  int *N_solve_phase_OK;
  int *S_solve_phase_OK;
  int *T_solve_phase_OK;
  int *B_solve_phase_OK;

  int *E_is_fluid_OK; /**< True if neighbour at East position is fluid.*/
  int *W_is_fluid_OK; /**< True if neighbour at West position is fluid.*/
  int *N_is_fluid_OK; /**< True if neighbour at North position is fluid.*/
  int *S_is_fluid_OK; /**< True if neighbour at South position is fluid.*/
  int *B_is_fluid_OK; /**< True if neighbour at Bottom position is fluid.*/
  int *T_is_fluid_OK; /**< True if neighbour at Top position is fluid.*/
  
  int *p_cut_face_x; /**< If p cell face perpendicular to x axis is cut, curve number, 0 otherwise (nxNyNz). */
  int *p_cut_face_y; /**< If p cell face perpendicular to y axis is cut, curve number, 0 otherwise (NxnyNz). */
  int *p_cut_face_z; /**< If p cell face perpendicular to z axis is cut, curve number, 0 otherwise (NxNynz). */

  char *case_s_type_x; /**< Classifies solid cells adjacent to interface if they have: (a) > 3 inward solid neighborgs x-wise, (b) 2, (c) 1 (d) 0 (NxNyNz). */
  char *case_s_type_y;
  char *case_s_type_z;
  
  int *e_J;
  int *e_K;
  int *w_J;
  int *w_K;
  int *n_I;
  int *n_K;
  int *s_I;
  int *s_K;
  int *b_I;
  int *b_J;
  int *t_I;
  int *t_J;

  int *u_dom_matrix; /**< Domain matrix, u nodes (nxNyNz). */ 
  int *v_dom_matrix; /**< Domain matrix, v nodes (NxnyNz). */
  int *w_dom_matrix; /**< Domain matrix, w nodes (NxNynz). */
  int *u_slave_nodes;
  int *v_slave_nodes;
  int *w_slave_nodes;
  int *u_slave_i;
  int *u_slave_J;
  int *u_slave_K;
  int *v_slave_I;
  int *v_slave_j;
  int *v_slave_K;
  int *w_slave_I;
  int *w_slave_J;
  int *w_slave_k;
  /*
  int *u_master_nodes;
  int *v_master_nodes;
  */
  int *p_dom_matrix; /**< Domain matrix, p nodes (NxNyNz). */
  int *u_cut_matrix; /**< Cut matrix, u nodes (nxNyNz). */
  int *v_cut_matrix; /**< Cut matrix, v nodes (NxnyNz). */
  int *w_cut_matrix; /**< Cut matrix, w nodes (NxNyNz). */
  int *u_slave_at_E_of_p_OK;
  int *v_slave_at_N_of_p_OK;
  int *w_slave_at_T_of_p_OK;
  int *curve_phase_change_OK; /**< True if phase change is to be solved on corresponding curve (curves). */
  int *u_sol_factor_index_u;
  int *u_sol_factor_index;
  int *u_sol_factor_i;
  int *u_sol_factor_J;
  int *u_sol_factor_K;
  int *v_sol_factor_index_v;
  int *v_sol_factor_index;
  int *v_sol_factor_I;
  int *v_sol_factor_j;
  int *v_sol_factor_K;
  int *w_sol_factor_index_w;
  int *w_sol_factor_index;
  int *w_sol_factor_I;
  int *w_sol_factor_J;
  int *w_sol_factor_k;
  int *turb_boundary_indexs; /**< Fluid p cells that have a non fluid neighbour. */
  int *turb_boundary_I;
  int *turb_boundary_J;
  int *turb_boundary_K;
  int *turb_bnd_excl_indexs;
  int *u_sol_type; /**< Clasiffies near solid u boundary cells 0:regular fluid node, 1:slave, 2: solid.*/
  int *v_sol_type; /**< Clasiffies near solid v boundary cells 0:regular fluid node, 1:slave, 2: solid.*/
  int *w_sol_type; /**< Clasiffies near solid w boundary cells 0:regular fluid node, 1:slave, 2: solid.*/

  int *u_cut_face_Fy_index; /**< face_y on u cell where Data_Mem::Fu_y is interpolated near solid. */
  int *u_cut_face_Fz_index; /**< face_z on u cell where Data_Mem::Fu_z is interpolated near solid. */
  int *v_cut_face_Fx_index; /**< face_x on v cell where Data_Mem::Fv_x is interpolated near solid. */
  int *v_cut_face_Fz_index; /**< face_z on v cell where Data_Mem::Fv_z is interpolated near solid. */
  int *w_cut_face_Fx_index; /**< face_x on w cell where Data_Mem::Fw_x is interpolated near solid. */
  int *w_cut_face_Fy_index; /**< face_y on w_cell where Data_Mem::Fw_y is interpolated near solid. */
  int *u_cut_face_Fy_index_v; /**< v cell nodes used for interpolation of Data_Mem::Fu_y near solid. */
  int *u_cut_face_Fz_index_w; /**< w cell nodes used for interpolation of Data_Mem::Fu_z near solid. */
  int *v_cut_face_Fx_index_u; /**< u cell nodes used for interpolation of Data_Mem::Fv_x near solid. */
  int *v_cut_face_Fz_index_w; /**< w cell nodes used for interpolation of Data_Mem::Fv_z near solid. */
  int *w_cut_face_Fx_index_u; /**< u cell nodes used for interpolation of Data_Mem::Fw_x near solid. */
  int *w_cut_face_Fy_index_v; /**< v cell nodes used for interpolation of Data_Mem::Fw_y near solid. */

  int *u_cut_face_x; /**< If u cell face perpendicular to x axis is cut, curve number, 0 otherwise. */
  int *u_cut_face_y; /**< If u cell face perpendicular to y axis is cut, curve number, 0 otherwise. */
  int *u_cut_face_z; /**< If u cell face perpendicular to z axis is cut, curve number, 0 otherwise. */
  int *v_cut_face_y; /**< If v cell face perpendicular to x axis is cut, curve number, 0 otherwise. */
  int *v_cut_face_x; /**< If v cell face perpendicular to y axis is cut, curve number, 0 otherwise. */
  int *v_cut_face_z; /**< If v cell face perpendicular to z axis is cut, curve number, 0 otherwise. */
  int *w_cut_face_x; /**< If w cell face perpendicular to x axis is cut, curve number, 0 otherwise. */
  int *w_cut_face_y; /**< If w cell face perpendicular to y axis is cut, curve number, 0 otherwise. */
  int *w_cut_face_z; /**< If w cell face perpendicular to z axis is cut, curve number, 0 otherwise. */

  int *p_nodes_in_solid_indx;

  int *diff_u_face_x_index;
  int *diff_u_face_x_index_u;
  int *diff_u_face_x_I;
  int *diff_v_face_y_index;
  int *diff_v_face_y_index_v;
  int *diff_v_face_y_J;
  int *diff_w_face_z_index;
  int *diff_w_face_z_index_w;
  int *diff_w_face_z_K;

  int *phase_change_matrix; /**< Set to 1 if phase change source term must be calculated for this cell (NxNyNz). True for cells that have a True value for both Data_Mem::solve_this_phase_OK and Data_Mem::curve_phase_change_OK arrays.*/

  int *W_coeff;
  int *E_coeff;
  int *N_coeff;
  int *S_coeff;
  int *T_coeff;
  int *B_coeff;
  
  double Lx;
  double Ly;
  double Lz;
  double tol_phi;
  double alpha_phi;
  double alpha_SC_phi;
  double constant_a;
  double u0_v;
  double v0_v;
  double w0_v;
  double p0;
  double pL;
  double mu;
  double mu_initial;
  double mu_final;
  double rise_to;
  /* 0: tflow, 1: min flow, 2: max flow  */
  /* 3: dt */
  /* 4: min phi, 5: max phi, 6: tphi */
  double dt[7];
  /* flow: 0 min, 1 max */
  /* phi: 2 min, 3 max */
  double tsaves[4];

  double C_1;
  double C_2;
  double C_mu;
  double C_sigma_k;
  double C_sigma_eps;
  double gL_tol;

  double cell_size;
  double alpha_u;
  double alpha_v;
  double alpha_w;
  double alpha_p_corr;
  double alpha_p_coeff;
  double pol_tol;
  double tol_flow; /**< Tolerance for convergence of flow variables, main loop. */
  double tol_ADI; /**< Tolerance for convergence of flow variables, ADI loop. */
  double alpha_gL;

  double mu_eff0;
  double max_mix_l;
  double l0;
  double cbc;
  double alpha_k;
  double alpha_eps;
  double alpha_P_k;
  double alpha_mu_turb;
  double alpha_S_CG_eps;

  double alpha_SC_k;
  double alpha_SC_eps;
  double y_plus_limit; /**< Nodes with values of Data_Mem::y_plus below this value are 
			  not calculated. */
  double eps_dist_tol;
  double kappa;
  double ref_p_value;
  double periodic_source; /**< Value of periodic source term. */
  double write_flow_transient_data_interval;
  double write_transient_data_interval;

  double cputime_flow_solver = 0;
  double cputime_phi_solver = 0;
  
  double *c_x;
  double *c_y;

  double *vol;
  double *vol_x;
  double *vol_y;
  double *vol_z;
  double *vol_uncut;

  double *vf_x; /**< Solid fraction at u cells. */
  double *vf_y; /**< Solid fraction at v cells. */
  double *vf_z; /**< Solid fraction at w cells. */

  double *nodes_x; /**< x coordinates of nodes for p grid. */
  double *nodes_y; /**< y coordinates of nodes for p grid. */
  double *nodes_z; /**< z coordinates of nodes for p grid. */
  double *Delta_x;
  double *Delta_y;
  double *Delta_z;
  
  double *Delta_xE; /**< Distance from node to East neighbour, p nodes. */
  double *Delta_xW; /**< Distance from node to West neighbour, p nodes. */
  double *Delta_yN; /**< Distance from node to North neighbour, p nodes. */
  double *Delta_yS; /**< Distance from node to South neighbour, p nodes. */
  double *Delta_zT; /**< Distance from node to Top neighbour, p nodes. */
  double *Delta_zB; /**< Distance from node to Bottom neigbour, p nodes. */
  double *Delta_xe; /**< Distance from node to East face, p nodes. */
  double *Delta_xw; /**< Distance from node to West face, p nodes. */
  double *Delta_yn; /**< Distance from node to North face, p nodes. */
  double *Delta_ys; /**< Distance from node to South face, p nodes. */
  double *Delta_zt; /**< Distance from node to Top face, p nodes. */
  double *Delta_zb; /**< Distance from node to Bottom face, p nodes. */

  double *fe;
  double *fw;
  double *fn;
  double *fs;
  double *ft;
  double *fb;

  double *nodes_x_u; /**< x coordinates of nodes for u grid (nxNyNz). */
  double *nodes_y_u; /**< y coordinates of nodes for u grid (nxNyNz). */
  double *nodes_z_u; /**< z coordinates of nodes for u grid (nxNyNz). */
  double *Delta_x_u;
  double *Delta_y_u;
  double *Delta_z_u;
  double *Delta_xE_u; /**< Distance from node to East neighbour, u nodes. */
  double *Delta_xW_u; /**< Distance from node to West neighbour, u nodes. */
  double *Delta_yN_u; /**< Distance from node to North neighbour, u nodes. */
  double *Delta_yS_u; /**< Distance from node to South neighbour, u nodes. */
  double *Delta_zt_u; /**< Distance from node to Top neighbour, u nodes. */
  double *Delta_zb_u; /**< Distance from node to Bottom neigbour, u nodes. */
  double *Delta_xe_u; /**< Distance from node to East face, u nodes. */
  double *Delta_xw_u; /**< Distance from node to West face, u nodes. */
  double *Delta_yn_u; /**< Distance from node to North face, u nodes. */
  double *Delta_ys_u; /**< Distance from node to South face, u nodes. */
  double *Delta_zT_u; /**< Distance from node to Top face, u nodes. */
  double *Delta_zB_u; /**< Distance from node to Bottom face, u nodes. */

  double *nodes_x_v; /**< x coordinates of nodes for v grid (NxnyNz). */
  double *nodes_y_v; /**< y coordinates of nodes for v grid (NxnyNz). */
  double *nodes_z_v; /**< z coordinates of nodes for v grid (NxnyNz). */
  double *Delta_x_v;
  double *Delta_y_v;
  double *Delta_z_v;
  double *Delta_xE_v; /**< Distance from node to East neighbour, v nodes. */
  double *Delta_xW_v; /**< Distance from node to West neighbour, v nodes. */
  double *Delta_yN_v; /**< Distance from node to North neighbour, v nodes. */
  double *Delta_yS_v; /**< Distance from node to South neighbour, v nodes. */
  double *Delta_zT_v; /**< Distance from node to Top neighbour, v nodes. */
  double *Delta_zB_v; /**< Distance from node to Bottom neigbour, v nodes. */
  double *Delta_xe_v; /**< Distance from node to East face, v nodes. */
  double *Delta_xw_v; /**< Distance from node to West face, v nodes. */
  double *Delta_yn_v; /**< Distance from node to North face, v nodes. */
  double *Delta_ys_v; /**< Distance from node to South face, v nodes. */
  double *Delta_zt_v; /**< Distance from node to Top face, v nodes. */
  double *Delta_zb_v; /**< Distance from node to Bottom face, v nodes. */

  double *nodes_x_w; /**< x coordinates of nodes for w grid (NxNynz). */
  double *nodes_y_w; /**< y coordinates of nodes for w grid (NxNynz). */
  double *nodes_z_w; /**< z coordinates of nodes for w grid (NxNynz). */
  double *Delta_x_w;
  double *Delta_y_w;
  double *Delta_z_w;
  double *Delta_xE_w; /**< Distance from node to East neighbour, w nodes. */
  double *Delta_xW_w; /**< Distance from node to West neighbour, w nodes. */
  double *Delta_yN_w; /**< Distance from node to North neighbour, w nodes. */
  double *Delta_yS_w; /**< Distance from node to South neighbour, w nodes. */
  double *Delta_zT_w; /**< Distance from node to Top neighbour, w nodes. */
  double *Delta_zB_w; /**< Distance from node to Bottom neigbour, w nodes. */
  double *Delta_xe_w; /**< Distance from node to East face, w nodes. */
  double *Delta_xw_w; /**< Distance from node to West face, w nodes. */
  double *Delta_yn_w; /**< Distance from node to North face, w nodes. */
  double *Delta_ys_w; /**< Distance from node to South face, w nodes. */
  double *Delta_zt_w; /**< Distance from node to Top face, w nodes. */
  double *Delta_zb_w; /**< Distance from node to Bottom face, w nodes. */
  
  double *epsilon_x; /**< Normalized distance between fluid cell center and 
			interface in x direction. */
  double *epsilon_y;
  double *epsilon_z;
  double *epsilon_x_sol; /**< Normalized distance between solid cell center and
			interface in x direction. */
  double *epsilon_y_sol;
  double *epsilon_z_sol;
  double *v_fraction;
  double *v_fraction_x;
  double *v_fraction_y;
  double *v_fraction_z;

  double *norm_vec_x_x;
  double *norm_vec_x_y;
  double *norm_vec_x_z;
  double *norm_vec_y_x;
  double *norm_vec_y_y;
  double *norm_vec_y_z;
  double *norm_vec_z_x;
  double *norm_vec_z_y;
  double *norm_vec_z_z;
  double *par_vec_x_x;
  double *par_vec_x_y;
  double *par_vec_x_z;
  double *par_vec_y_x;
  double *par_vec_y_y;
  double *par_vec_y_z;
  double *par_vec_z_x;
  double *par_vec_z_y;
  double *par_vec_z_z;
  double *par_vec_x_x_2;
  double *par_vec_x_y_2;
  double *par_vec_x_z_2;
  double *par_vec_y_x_2;
  double *par_vec_y_y_2;
  double *par_vec_y_z_2;
  double *par_vec_z_x_2;
  double *par_vec_z_y_2;
  double *par_vec_z_z_2;  
  double *nxnx_x;
  double *nxny_x;
  double *nyny_y;
  double *nxny_y;
  
  double *norm_x;
  double *norm_y;
  double *norm_z;
  double *par_x;
  double *par_y;
  double *par_z;
  double *par_x_2;
  double *par_y_2;
  double *par_z_2;  
  
  double *rho_m; /**< Density values at main grid nodes. */
  double *rho_v;
  double *rho_l;
  double *cp_m; /**< Specific heat values at main grid nodes. */
  double *cp_v;
  double *cp_l;
  double *gamma_m; /**< Conductivity values at main grid nodes. */
  double *gamma_v;
  double *gamma_l;
  double *SC_vol_m_0;
  double *SC_vol_m;
  
  double *matrix_c; /**< C matrix. */
  double *constant_d;
  double *R_x; /**< Rotation in x. */
  double *R_y; /**< Rotation in y. */
  double *R_z; /**< Rotation in z. */
  double *Rot_; /**< Total rotation matrix. */
  double *Rot_T; /**< Total rotation matrix transpose. */
  double *Trans_; /**< Translation vector. */
  double *Trans_T;
  double *matrix_A; /* A matrix; A = R_T*C*R_ */
  double *vector_a;
  
  double *lambda_1;
  double *lambda_2;
  double *lambda_3;
  double *angle_x;
  double *angle_y;
  double *angle_z;
  double *tcompx;
  double *tcompy;
  double *tcompz;
  double *poly_coeffs;
  double *dphidNb_v;
  double *phi_b_v;
  double *phi0_v; /**< Initial values for phi, indexes correspond to domain matrix. */
  double *line_points;
  
  double *dphidxb; /**< Isoflux values. */
  double *dphidyb; /**< Isoflux values. */
  double *dphidzb; /**< Isoflux values. */

  double *dphidxs; /**< Derivatives up to two levels within solid for Conjugate. */
  double *dphidys; /**< Derivatives up to two levels within solid for Conjugate. */
  double *dphidzs; /**< Derivatives up to two levels within solid for Conjugate. */

  double *J_x;
  double *J_y;
  double *J_z;
  
  double *J_norm;
  double *J_par;
  double *J_par_2;
  
  double *phi;
  double *phi0;
  double *u;
  /**< Values of x component velocity at displaced positions. */
  double *u0;
  /**< Values of Data_Mem::u from previous iteration for transient flow solver.*/

  double *v;
  /**< Values of y component velocity at displaced positions. */
  double *v0;
  /**< Values of Data_Mem::v from previous iteration for transient flow solver.*/

  double *w;
    /**< Values of z component velocity at displaced positions. */
  double *w0;
  /**< Values of Data_Mem::w from previous iteration for transient flow solver.*/

  double *u_ps; /**< Pseudo u velocities for SIMPLER algorithm. */
  double *v_ps; /**< Pseudo v velocities for SIMPLER algorithm. */
  double *w_ps; /**< Pseudo w velocities for SIMPLER algorithm. */
  
  double *U; /**< U velocity values at p nodes. */
  double *V; /**< V velocity values at p nodes. */
  double *W; /**< W velocity values at p nodes. */
  
  double *p; /**< Pressure field values. */
  double *p_corr;

  double *u_aP_SIMPLEC;
  double *v_aP_SIMPLEC;
  double *w_aP_SIMPLEC;

  double *faces_x;
  double *faces_y;
  double *faces_z;
  double *p_faces_x; /**< Area of faces perpendicular to x axis, p cells (nxNyNz). */
  double *p_faces_y; /**< Area of faces perpendicular to y axis, p cells (NxnyNz). */
  double *p_faces_z; /**< Area of faces perpendicular to z axis, p cells (NxNynz). */
  double *u_faces_x; /**< Area of faces perpendicular to x axis, u cells (NxNyNz). */  
  double *u_faces_y; /**< Area of faces perpendicular to y axis, u cells (nxnyNz). */
  double *u_faces_z; /**< Area of faces perpendicular to z axis, u cells (nxNynz). */
  double *v_faces_x; /**< Area of faces perpendicular to x axis, v cells (nxnyNz). */
  double *v_faces_y; /**< Area of faces perpendicular to y axis, v cells (NxNyNz). */
  double *v_faces_z; /**< Area of faces perpendicular to z axis, v cells (Nxnynz). */
  double *w_faces_x; /**< Area of faces perpendicular to x axis, w cells (nxNynz). */
  double *w_faces_y; /**< Area of faces perpendicular to y axis, w cells (Nxnynz). */
  double *w_faces_z; /**< Area of faces perpendicular to z axis, w cells (NxNyNz). */

  double *u_at_faces; /**< u at center of face perpendicular to x axis, on p cell. */
  double *v_at_faces; /**< v at center of face perpendicular to y axis, on p cell. */
  double *w_at_faces; /**< w at center of face perpendicular to z axis, on p cell. */

  double *u_at_faces_y; /**< u at center of face perpendicular to y axis, on p cell. */
  double *u_at_faces_z; /**< u at center of face perpendicular to z axis, on p cell. */
    
  double *v_at_faces_x; /**< v at center of face perpendicular to x axis, on p cell. */
  double *v_at_faces_z; /**< v at center of face perpendicular to z axis, on p cell. */

  double *w_at_faces_x; /**< w at center of face perpendicular to x axis, on p cell. */
  double *w_at_faces_y; /**< w at center of face perpendicular to y axis, on p cell. */

  double *u_at_corners;
  double *v_at_corners;
  double *w_at_corners;

  double *alpha_u_x; /**< Correction factor for u_e and u_w (convective transport) in momentum
			balance for u cell (NxNyNz). */
  double *alpha_u_y; /**< Correction factor for u_n and u_s (convective transport) in momentum
			balance for u cell (nxnyNz). */
  double *alpha_u_z; /**< Correction factor for u_t and u_b (convective transport) in momentum
			balance for u cell (nxNynz). */
  double *alpha_v_x; /**< Correction factor for v_e and v_w (convective transport) in momentum
			balance for v cell (nxnyNz). */
  double *alpha_v_y; /**< Correction factor for v_n and v_s (convective transport) in momentum
			balance for v cell (NxNyNz). */
  double *alpha_v_z; /**< Correction factor for v_t and v_b (convective transport) in momentum
			balance for v cell (Nxnynz). */
  double *alpha_w_x; /**< Correction factor for w_e and w_w (convective transport) in momentum 
			balance for w cell (nxNynz). */
  double *alpha_w_y; /**< Correction factor for w_n and w_s (convective transport) in momentum
			balance for w cell (Nxnynz). */
  double *alpha_w_z; /**< Correction factor for w_t and w_b (convective transport) in momentum
			balance for w cell (NxNyNz). */
  double *diff_factor_u_face_x; /**< Correction factor for diffusive momentum flux at w and e faces
				   in momentum balance for u cell (Data_Mem::diff_u_face_x_count). */
  double *diff_factor_v_face_y; /**< Correction factor for diffusive momentum flux at s and n faces
				   in momentum balance for v cell (Data_Mem::diff_v_face_y_count). */
  double *diff_factor_w_face_z; /**< Correction factor for diffusive momentum flux at b and t faces
				   in momentum balance for w cell (Data_Mem::diff_w_face_z_count). */

  double *Du_x;
  double *Du_y;
  double *Du_z;
  double *Dv_x;
  double *Dv_y;
  double *Dv_z;
  double *Dw_x;
  double *Dw_y;
  double *Dw_z;
  double *Dk_x; /**< Diffusive x coefficient for Data_Mem::k_turb (nxNyNz). */
  double *Dk_y; /**< Diffusive y coefficient for Data_Mem::k_turb (NxnyNz). */
  double *Dk_z; /**< Diffusive z coefficient for Data_Mem::k_turb (NxNynz). */
  double *Deps_x; /**< Diffusive x coefficient for Data_Mem::eps_turb (nxNyNz). */
  double *Deps_y; /**< Diffusive y coefficient for Data_Mem::eps_turb (NxnyNz). */
  double *Deps_z; /**< Diffusive z coefficient for Data_Mem::eps_turb (NxNynz). */
  double *Dphi_x; /**< Diffusive x coefficient for phi ~ (nxNyNz). */
  double *Dphi_y; /**< Diffusive y coefficient for phi ~ (NxnyNz). */
  double *Dphi_z; /**< Diffusive z coefficient for phi ~ (NxNynz). */

  double *Fu_x;
  double *Fu_y;
  double *Fu_z;
  double *Fv_x;
  double *Fv_y;
  double *Fv_z;
  double *Fw_x;
  double *Fw_y;
  double *Fw_z;
  double *Fk_x;
  double *Fk_y;
  double *Fk_z;
  double *Feps_x;
  double *Feps_y;
  double *Feps_z;
  double *Fphi_x; /**< Convective x coefficient for phi ~ (nxNyNz). */
  double *Fphi_y; /**< Convective y coefficient for phi ~ (NxnyNz). */
  double *Fphi_z; /**< Convective z coefficient for phi ~ (NxNynz). */

  double *T_phase_change; /**< Temperature of phase change (curves). */
  double *dT_phase_change; /**< Temperature interval over which phase change occurs (curves). */
  double *gL;
  double *gL_guess; /**< Guessed value of gL (NxNyNz). */
  double *gL_calc; /**< Value of gL calculated with current phi values (NxNyNz). */
  double *dH_S_L;
  double *enthalpy_diff;
  double *T_ref;
  double *phase_change_vol; /**< Solid volume of solved solid cells that experience phase change.*/
  double *u_sol_factor;
  double *u_p_factor;
  double *v_sol_factor;
  double *v_p_factor;
  double *w_sol_factor;
  double *w_p_factor;
  double *u_sol_factor_x;
  double *u_sol_factor_y;
  double *u_sol_factor_z;
  double *u_sol_factor_u;
  double *u_sol_factor_F;
  double *u_sol_factor_tau;
  double *u_sol_factor_p;
  double *u_sol_factor_F_p;
  double *v_sol_factor_x;
  double *v_sol_factor_y;
  double *v_sol_factor_z;
  double *v_sol_factor_v;
  double *v_sol_factor_F;
  double *v_sol_factor_tau;
  double *v_sol_factor_p;
  double *v_sol_factor_F_p;
  double *w_sol_factor_x;
  double *w_sol_factor_y;
  double *w_sol_factor_z;
  double *w_sol_factor_w;
  double *w_sol_factor_F;
  double *w_sol_factor_tau;
  double *w_sol_factor_p;
  double *w_sol_factor_F_p;
  double *y_wall; /**< Distance to nearest wall at p nodes. */
  double *nabla_u; /**< Equal to du_i/dx_j, u_i: u, v, w. x_j: x, y, z. */
  double *def_tens; /**< Deformation tensor. */
  double *tau_turb; /**< Turbulent stress tensor. */
  double *P_k; /**< Turbulent kinetic energy production. */
  double *P_k_lag; /**< Values of Data_Mem::P_k from previous iteration. */
  double *mu_turb;
  double *mu_turb_lag; /**< Values of Data_Mem::mu_turb from previous iteration. */
  double *f_1;
  double *f_2;
  double *f_mu;
  double *k_turb; /**< Turbulent kinetic energy. */
  double *k_turb0;
  /**< Values of Data_Mem::k_turb from previous iteration for transient flow solver.*/
  double *eps_turb; /**< Rate of dissipation of Data_Mem::k_turb. */
  double *eps_turb0;
  /**< Values of Data_Mem::eps_turb from previous iteration for transient flow solver.*/
  double *gamma;
  double *S_CG_eps;
  double *S_CG_eps_lag;
  double *d2Uidxjdxk2;
  double *depsdxb;
  double *depsdyb;
  double *depsdzb;
  double *SC_vol_k_turb; /**< Source term due to solid boundary for Data_Mem::k_turb equation. */
  double *SC_vol_eps_turb; /**< Source term due to solid boundary for Data_Mem::eps_turb equation. */
  double *SC_vol_k_turb_lag; /**< Values of Data_Mem::SC_vol_k_turb from previous iteration. */
  double *SC_vol_eps_turb_lag; /**< Values of Data_Mem::SC_vol_eps_turb from previous iteration. */  
  double *eps_bnd_x;
  double *eps_bnd_y;
  double *eps_bnd_z;

  double *y_plus; /**< Values of y_plus for p cells on Data_Mem::turb_boundary_indexs array. */
  
  double *fe_u;
  double *fw_u;
  double *fn_u;
  double *fs_u;
  double *ft_u;
  double *fb_u;
  
  double *fe_v;
  double *fw_v;
  double *fn_v;
  double *fs_v;
  double *ft_v;
  double *fb_v;

  double *fe_w;
  double *fw_w;
  double *fn_w;
  double *fs_w;
  double *ft_w;
  double *fb_w;
  
  double *u_E_non_uni;
  double *u_W_non_uni;
  double *v_N_non_uni;
  double *v_S_non_uni;
  double *w_B_non_uni;
  double *w_T_non_uni;
  double *k_E_non_uni;
  double *k_W_non_uni;
  double *k_N_non_uni;
  double *k_S_non_uni;
  double *k_B_non_uni;
  double *k_T_non_uni;
  double *eps_E_non_uni;
  double *eps_W_non_uni;
  double *eps_N_non_uni;
  double *eps_S_non_uni;
  double *eps_B_non_uni;
  double *eps_T_non_uni;
  double *p_E_non_uni;
  double *p_W_non_uni;
  double *p_S_non_uni;
  double *p_N_non_uni;
  double *p_B_non_uni;
  double *p_T_non_uni;
  double *u_factor_delta_h;
  double *v_factor_delta_h;
  double *w_factor_delta_h;
  double *u_factor_area;
  double *v_factor_area;
  double *w_factor_area;
  double *u_factor_inters_x;
  double *u_factor_inters_y;
  double *u_factor_inters_z;
  double *v_factor_inters_x;
  double *v_factor_inters_y;
  double *v_factor_inters_z;
  double *w_factor_inters_x;
  double *w_factor_inters_y;
  double *w_factor_inters_z;
  double *u_cut_fw;
  double *u_cut_fe;
  double *v_cut_fs;
  double *v_cut_fn;
  double *w_cut_fb;
  double *w_cut_ft;

  double *cdE; /**< Cut P-cell distance to E or solid. NAN within solid body */
  double *cdW; /**< Cut P-cell distance to W or solid. NAN within solid body */
  double *cdN; /**< Cut P-cell distance to N or solid. NAN within solid body */
  double *cdS; /**< Cut P-cell distance to S or solid. NAN within solid body */
  double *cdT; /**< Cut P-cell distance to T or solid. NAN within solid body */
  double *cdB; /**< Cut P-cell distance to B or solid. NAN within solid body */
  
  double *cudE; /**< Cut u-cell distance to E or solid. NAN within solid body */
  double *cudW; /**< Cut u-cell distance to W or solid. NAN within solid body */
  double *cudN; /**< Cut u-cell distance to N or solid. NAN within solid body */
  double *cudS; /**< Cut u-cell distance to S or solid. NAN within solid body */
  double *cudT; /**< Cut u-cell distance to T or solid. NAN within solid body */
  double *cudB; /**< Cut u-cell distance to B or solid. NAN within solid body */

  double *cvdE; /**< Cut v-cell distance to E or solid. NAN within solid body */
  double *cvdW; /**< Cut v-cell distance to W or solid. NAN within solid body */
  double *cvdN; /**< Cut v-cell distance to N or solid. NAN within solid body */
  double *cvdS; /**< Cut v-cell distance to S or solid. NAN within solid body */
  double *cvdT; /**< Cut v-cell distance to T or solid. NAN within solid body */
  double *cvdB; /**< Cut v-cell distance to B or solid. NAN within solid body */

  double *cwdE; /**< Cut w-cell distance to E or solid. NAN within solid body */
  double *cwdW; /**< Cut w-cell distance to W or solid. NAN within solid body */
  double *cwdN; /**< Cut w-cell distance to N or solid. NAN within solid body */
  double *cwdS; /**< Cut w-cell distance to S or solid. NAN within solid body */
  double *cwdT; /**< Cut w-cell distance to T or solid. NAN within solid body */
  double *cwdB; /**< Cut w-cell distance to B or solid. NAN within solid body */

  double *u_cut_face_Fy_W; /**< Weight used for interpolation of Data_Mem::Fu_y near solid. */
  double *u_cut_face_Fy_E; /**< Weight used for interpolation of Data_Mem::Fu_y near solid. */
  double *u_cut_face_Fz_W; /**< Weight used for interpolation of Data_Mem::Fu_z near solid. */
  double *u_cut_face_Fz_E; /**< Weight used for interpolation of Data_Mem::Fu_z near solid. */
  double *v_cut_face_Fx_S; /**< Weight used for interpolation of Data_Mem::Fv_x near solid. */
  double *v_cut_face_Fx_N; /**< Weight used for interpolation of Data_Mem::Fv_x near solid. */
  double *v_cut_face_Fz_S; /**< Weight used for interpolation of Data_Mem::Fv_z near solid. */
  double *v_cut_face_Fz_N; /**< Weight used for interpolation of Data_Mem::Fv_z near solid. */
  double *w_cut_face_Fx_B; /**< Weight used for interpolation of Data_Mem::Fw_x near solid. */
  double *w_cut_face_Fx_T; /**< Weight used for interpolation of Data_Mem::Fw_x near solid. */
  double *w_cut_face_Fy_B; /**< Weight used for interpolation of Data_Mem::Fw_y near solid. */
  double *w_cut_face_Fy_T; /**< Weight used for interpolation of Data_Mem::Fw_y near solid. */  
  
  double *mu_turb_at_u_nodes; /**< Values of Data_Mem::mu_turb interpolated at faces of p cells on x direction. */
  double *mu_turb_at_v_nodes; /**< Values of Data_Mem::mu_turb interpolated at faces of p cells on y direction. */
  double *mu_turb_at_w_nodes; /**< Values of Data_Mem::mu_turb interpolated at faces of p cells on z direction. */

  double *u_p_factor_W;
  double *v_p_factor_S;
  double *w_p_factor_B;

  double *diff_u_face_x_Delta;
  double *diff_v_face_y_Delta;
  double *diff_w_face_z_Delta;

  double *vertex_x;
  double *vertex_y;
  double *vertex_z;

  double *vertex_x_u;
  double *vertex_y_v;
  double *vertex_z_w;
  
  Boundary_Info east;
  Boundary_Info west;
  Boundary_Info north;
  Boundary_Info south;
  Boundary_Info top;
  Boundary_Info bottom;
  
  Coeffs coeffs_phi;
  Coeffs coeffs_u;
  Coeffs coeffs_v;
  Coeffs coeffs_w;
  Coeffs coeffs_p;
  Coeffs coeffs_k;
  Coeffs coeffs_eps;
  
  ADI_Vars ADI_vars;
  
  Server_Data sdata;

  Sigmas sigmas;

  Wins wins;

  TEG teg;

  MATIOS matios;

  Grid_Info x_side;
  Grid_Info y_side;
  Grid_Info z_side;

  b_cond_flow bc_flow_east;
  b_cond_flow bc_flow_west;
  b_cond_flow bc_flow_south;
  b_cond_flow bc_flow_north;
  b_cond_flow bc_flow_bottom;
  b_cond_flow bc_flow_top;

  void (*ADI_solver)(double *, Coeffs *, const int, const int, const int, ADI_Vars *);
  
} Data_Mem;

/* FUNCTIONS PROTOTYPES */

/* 2DGRID.C*/

int set_base_segments(Grid_Info *gi, const double *d_segments, const int kseg);
int count_nodes_in_gi(const Grid_Info *gi);
int smooth_grid(Data_Mem *data);
int smooth_grid_dim(Grid_Info *gi, const double L);
void grid_maker(Data_Mem *Ptr);
double segmentation(const double start, const double end, const int nodes);
void make_surface(Data_Mem *Ptr, const double angle_x, const double angle_y, const double angle_z, 
		  const double lambda_1, const double lambda_2, const double lambda_3,
		  const double constant_d, 
		  const double tcompx, const double tcompy, const double tcompz, 
		  double *poly_coeffs, const int count);
void matrix_product(const double *matrix_A,const double *matrix_B, double *matrix_C, const int A_ROW, 
		    const int A_COL, const int B_ROW, const int B_COL);
void transpose_matrix(const double *matrix, double *transpose, const int ROW, const int COL);
void matrix_addition(const double *A_matrix, const double *B_matrix, double *C_matrix, 
		     const int A_ROW, const int A_COL, const int B_ROW, const int B_COL, const int C_ROW, 
		     const int C_COL, const int option);
void grid_maker_non_uni(Data_Mem *data);
void compute_fnsew(Data_Mem * data);

/* COEFFS_FLOW_QUICK.c */

void coeffs_flow_u_quick(Data_Mem *data);
void coeffs_flow_v_quick(Data_Mem *data);
void coeffs_flow_u_quick_3D(Data_Mem *data);
void coeffs_flow_v_quick_3D(Data_Mem *data);
void coeffs_flow_w_quick_3D(Data_Mem *data);

/* GRAPHS.C */

void * anims(void *ptr);
void initialize_flag (void);
void set_thread_flag (int flag_value);
void update_anims(void *ptr);
void * graphs(void *ptr);
void * init_anim_graph_struct(Data_Mem *data);

/* SOLVE_FLOW.C */

void solve_flow_SIMPLES(Data_Mem *data);
void compute_pseudo_velocities(Data_Mem *data);
void coeffs_flow_p(Data_Mem *data, const int use_ps_velocities_OK);
void update_field(Data_Mem *data);
void set_bc_flow(Data_Mem *data);
double continuity_residual(Data_Mem *data);
void set_D_u(Data_Mem *data);
void set_D_v(Data_Mem *data);
void set_F_u(Data_Mem *data);
void set_F_v(Data_Mem *data);
void open_bnd_indx(Data_Mem *data);
void process_action_on_signal(Data_Mem *data,
			      int *convergence_OK,
			      int *sub_convergence_OK);
double mass_io_balance(Data_Mem *data);
void compute_parabolic_inlet(Data_Mem *data);
int set_p_ref_node(Data_Mem *data);
void init_p_at_boundary(Data_Mem *data);
void add_periodic_source_to_p(Data_Mem *data);
void store_bnd_p_values(Data_Mem *data);
void solve_flow_SIMPLES_transient(Data_Mem *data);
void add_u_momentum_source(Data_Mem *data);
void add_v_momentum_source(Data_Mem *data);

/* SOLVE_FLOW_3D.C */

int set_periodic_flow_source(Data_Mem *data);
void solve_flow_pre_calc_jobs(Data_Mem *data);
void solve_flow_post_calc_jobs(Data_Mem *data);
void set_mu(Data_Mem *data, const int outer_iter);
void store_flow_fields(Data_Mem *data);
void add_u_momentum_source_3D(Data_Mem *data);
void add_v_momentum_source_3D(Data_Mem *data); /* <== not defined */
void add_w_momentum_source_3D(Data_Mem *data); /* <== not defined */
void open_bnd_indx_3D(Data_Mem *data);
void solve_flow_SIMPLES_3D(Data_Mem *data);
void set_D_w(Data_Mem *data);
void set_F_w(Data_Mem *data);
void add_w_momentum_source(Data_Mem *data);
void set_F_u_3D(Data_Mem *data);
void set_F_v_3D(Data_Mem *data);
void set_D_u_3D(Data_Mem *data);
void set_D_v_3D(Data_Mem *data);
void init_p_at_boundary_3D(Data_Mem *data);
int set_p_ref_node_3D(Data_Mem *data);
void store_bnd_p_values_3D(Data_Mem *data);
void set_bc_flow_3D(Data_Mem *data);
void compute_pseudo_velocities_3D(Data_Mem *data);
void update_field_3D(Data_Mem *data);
void coeffs_flow_p_3D(Data_Mem *data, const int use_ps_velocities_OK);
double continuity_residual_3D(Data_Mem *data);
double mass_io_balance_3D(Data_Mem *data);
void add_periodic_source_to_p_3D(Data_Mem *data);
void solve_flow_SIMPLES_3D_transient(Data_Mem *data);

/* COEFFS_FLOW_UPWIND.C */
void coeffs_flow_u_upwind(Data_Mem *data);
void coeffs_flow_v_upwind(Data_Mem *data);
void coeffs_flow_u_upwind_3D(Data_Mem *data);
void coeffs_flow_v_upwind_3D(Data_Mem *data);
void coeffs_flow_w_upwind_3D(Data_Mem *data);

/* TUI.C */
int set_y_labels(Wins *wins);
void print_phi_it_info(const int it, const int max_it,
		       const u_int64_t time_i, const u_int64_t time_f,
		       Sigmas *sigmas, Wins *wins);
void table_header_phi_it(Wins *wins);
void print_flow_it_info(char *msg, const int msg_size, const int rstride,
			const char *solver_name,
			const int outer_iter, const int max_iter,
			const u_int64_t time_i, const u_int64_t time_f,
			const int consider_turbulence_OK, Sigmas *sigmas, Wins *wins);
void table_header_flow_it(char *msg, const int msg_size, Wins *wins);
void error_msg(Wins *wins, const char *text);
void alert_msg(Wins *wins, const char *text);
void info_msg(Wins *wins, const char *text);
void progress_msg(Wins *wins, const char *text, const double per);
void serv_msg(Wins *wins, const char *text);
int set_it_graph_sizes(Wins *wins);
int init_ncurses_processes(Wins *wins, const char *config_filename);
int init_upds_window_info(Wins *wins);
int update_upds_window(Wins *wins, const int line);
int update_graph_window(Data_Mem *data);
WINDOW *create_newwin(const int height, const int width,
		      const int starty, const int startx);
void destroy_windows(Wins *wins);
void add_to_the_right(Wins *wins);
void clear_canvas(chtype *ch_m, const int g_width, const int g_height);
void clear_vector(chtype *ch_v, const int g_height);
void draw_canvas(Wins *wins);
int set_values_in_column(Wins *wins);
void set_title(Wins *wins);
double get_smax(const double *svalues);
int get_range_for_graph(Wins *wins);
int get_scale_for_graph(Wins *wins);
void dump_config_file(const char * filename, Wins *wins);
void interface_cell_count(Data_Mem *data);
void cut_cell_count(Data_Mem *data);

/* TURB_PROPERTIES.C */

void compute_turb_prop(Data_Mem *data);
void compute_P_k(Data_Mem *data);
void compute_tau_turb(Data_Mem *data);
void compute_def_tens(Data_Mem *data);
void compute_nabla_u(Data_Mem *data);
void vel_at_crossed_faces(Data_Mem *data);
void compute_S_CG_eps(Data_Mem *data);
void compute_d2Uidxjdxk2(Data_Mem *data);
void compute_vel_at_centers_full_from_faces(Data_Mem *data);
void vel_at_corners(Data_Mem *data);
void vel_at_crossed_faces_3D(Data_Mem *data);
void get_xi(double *xi, const double *xM, const double *poly_coeffs,
	    const int *phase,
	    const double *x_, const double *y_, const double *z_,
	    const double cell_size,
	    const int curves, const int idx);
void compute_nabla_u_3D(Data_Mem *data);
void compute_def_tens_3D(Data_Mem *data);
void compute_tau_turb_3D(Data_Mem *data);
void compute_P_k_3D(Data_Mem *data);
void compute_d2Uidxjdxk2_3D(Data_Mem *data);
void vel_at_corners_3D(Data_Mem *data);
inline double _cross_der(const int ic,
			 const int offset1, const int offset2, const int offset3,
			 const double *u, const double dxdy);
void compute_S_CG_eps_3D(Data_Mem *data);
void compute_turb_prop_3D(Data_Mem *data);

/* BOUNDARY_FLUX.C */

void compute_near_boundary_molecular_flux_experimental(Data_Mem *data);
void compute_near_boundary_molecular_flux_Tsutsumi(Data_Mem *data);
void compute_near_boundary_molecular_flux_Tsutsumi_def(Data_Mem *data);
void compute_near_boundary_molecular_flux_Sato(Data_Mem *data);
void compute_near_boundary_molecular_flux_Sato_at_face(Data_Mem *data);
void compute_near_boundary_convective_flux(Data_Mem *data);
void compute_solid_der_at_interface(Data_Mem *data);
void compute_near_boundary_molecular_flux_experimental_SI(Data_Mem *data);

/* PERP_DIST.C */

int perp_dist(double *poly_coeffs, double *x, const double cell_size,
	      const int curve_is_solid_OK, const char *results_dir);
double polyn(const double *x, const double *pc);
double polyn_l(const double *x, const double *pc);
double polyn_q(const double *x, const double *pc);
inline double __add_interval_dist(const double x,
				  const double q, const double xmin, const double xmax);
inline double __add_interval_dist2(const double x,
				   const double q, const double xmin, const double xmax);
int normal(double *norm_vec, const double *x, const double *pc);
int normal_l(double *norm_vec, const double *x, const double *pc);
int normal_q(double *norm_vec, const double *x, const double *pc);
int Normal_q(double *norm_vec, const double *x, const double *pc);
double calc_dist(const double *x, const double *pc, const double cell_size);
double calc_dist_l(const double *x, const double *pc, const double cell_size);
double calc_dist_q(const double *x, const double *pc, const double cell_size);
int int_curv(double *x_int, const double *x, const double *norm_v,
	     const double *pc, const double cell_size);
int int_curv_l(double *x_int, const double *x, const double *norm_v,
	       const double *pc, const double cell_size);
int int_curv_q(double *x_int, const double *x, const double *norm_v,
	       const double *pc, const double cell_size);
int int_curv_cf(double *x_int, const double *x, const double *norm_v,
		const double *pc, const double cell_size, const double cf);
int int_curv_l_cf(double *x_int, const double *x, const double *norm_v,
		  const double *pc, const double cell_size, const double cf);
int int_curv_q_cf(double *x_int, const double *x, const double *norm_v,
		  const double *pc, const double cell_size, const double cf);
inline int __checks_bnd(const double *x);
int calc_xint(double *xint,
	      const double *x, const double *pc, const double cell_size);
int calc_xint_l(double *xint,
		const double *x, const double *pc, const double cell_size);
int calc_xint_q(double *xint,
		const double *x, const double *pc, const double cell_size);

double calc_dist_far(const double *x, const double *pc, const double cell_size);
void calc_dist_norm(const double *x, const double *pc,
		    double *d, double *norm_v, const double cell_size);
double max_abs_d(const double *x1, const double *x2);
double vec_dist(const double *x1, const double *x2);
void subs_scalar(double *x, const double scalar);
void adds_vec_sca(double *x1, const double *x2, const double scalar);
void copy_vec(double *x1, const double *x2);
void avg_vec(double *x_result, const double *x0, const double *x1);
void bisection(double *x_int, const double *x0, const double *x1, const double *pc);
void find_solid_area(double *norm_v, double *solid_area, const double area_x, const double area_y,
		     const double area_z, const double max_area);
double vec_mag(const double *vec);
void normalize_vec(double *vec);
void diff_vec(double *x0, const double *x1, const double *x2);
void scale_vec(double *x, const double value);

/* SOLVE_PHI.C */

double solve_phi_jobs(Data_Mem *data);
void compute_phi_SC_at_interface_Isoflux(Data_Mem *data);
void compute_phi_SC_at_interface_Isoflux_legacy(Data_Mem *data);
void compute_phi_SC_at_interface_Dirichlet(Data_Mem *data);
void compute_phi_SC_at_interface(Data_Mem *data);
void set_bc(Data_Mem *data);
void coeffs_phi_power_law(Data_Mem *data);
void set_D_phi(Data_Mem *data);
void set_F_phi(Data_Mem *data);
void solve_phi_transient(Data_Mem *data);
void solve_phi_steady(Data_Mem *data);
void solve_phi_pre_calc_jobs(Data_Mem *data);
void solve_phi_post_calc_jobs(Data_Mem *data);

/* TURB_COFFS.C */

void set_D_k(Data_Mem *data);
void set_D_eps(Data_Mem *data);
void set_F_k(Data_Mem *data);
void set_F_eps(Data_Mem *data);
void set_coeffs_k(Data_Mem *data);
void set_coeffs_eps(Data_Mem *data);
void set_bc_turb_k(Data_Mem *data);
void set_bc_turb_eps(Data_Mem *data);
int turb_boundary_nodes(Data_Mem *data);
void compute_k_SC_at_interface(Data_Mem *data);
void compute_eps_SC_at_interface(Data_Mem *data);
void filter_neg_k_eps(Data_Mem *data);
void change_bnd_turb_to_default(Data_Mem *data);
void set_turb_init_values(Data_Mem *data);
void relax_mu_turb(Data_Mem *data);
void init_turb_lag_vars(Data_Mem *data);
void compute_eps_bnd(Data_Mem *data);
void filter_y_plus(Data_Mem *data);
void compute_mu_turb_at_vel_nodes(Data_Mem *data);
void interp_mu_turb_to_p_nodes_in_solid(Data_Mem *data);
void compute_turb_aux_fields(Data_Mem *data);
int turb_boundary_nodes_3D(Data_Mem *data);
void filter_y_plus_3D(Data_Mem *data);
void compute_turb_aux_fields_3D(Data_Mem *data);
void relax_mu_turb_3D(Data_Mem *data);
void set_D_k_3D(Data_Mem *data);
void set_F_k_3D(Data_Mem *data);
void set_D_eps_3D(Data_Mem *data);
void set_F_eps_3D(Data_Mem *data);
void set_coeffs_k_3D(Data_Mem *data);
void compute_k_SC_at_interface_3D(Data_Mem *data);
void set_coeffs_eps_3D(Data_Mem *data);
void compute_eps_SC_at_interface_3D(Data_Mem *data);
void set_bc_turb_k_3D(Data_Mem *data);
void set_bc_turb_eps_3D(Data_Mem *data);
void compute_eps_bnd_3D(Data_Mem *data);
void compute_mu_turb_at_vel_nodes_3D(Data_Mem *data);
void set_turb_init_values_3D(Data_Mem *data);
inline double _get_der_sqrt_k_b(double e_, const double e_tol,
				const double phi_c, const double phi_f,
				const double Delta, const double dir_fact);
double __get_b_derivative_value_Dirichlet(const double e_, const double e_tol,
					  const double phi_b, const double phi_c,
					  const double phi_f, const double Dic,
					  const double Dcf, const int dir_fact);

/* UTILITIES.C */
void set_eta_array(char *eta, const int size, const u_int64_t delta);
void make_bl_profile(Data_Mem *data);
void _make_bl_profile(double *phi, const double phib,
		      const char bc_face, const char solid_at_face,
		      const double wref,
		      const double *x, const double *y, const double *z,
		      const int *dM, const int dM_value,
		      const double d,
		      const int Nx, const int Ny, const int Nz,
		      const double xmin, const double xmax,
		      const double ymin, const double ymax,
		      const double zmin, const double zmax);
void set_array_profile_for_debug(double *phi, Data_Mem *data, const int radial_ok);
void set_uvw_profile_for_debug(Data_Mem *data, const int radial_ok);
void mem_count(Wins *wins);
mat_t * get_matfp(const char *filename, const int v5_OK, Wins *wins);
int write_to_log_file(Data_Mem *data, const char *log_entry);
int read_int_array_from_MAT_file(int *array, const char *var_name,
				 const unsigned int Nx, const unsigned int Ny,
				 const unsigned int Nz, mat_t *matfp);
int read_array_from_MAT_file(double *array, const char *var_name,
			     const unsigned int Nx, const unsigned int Ny,
			     const unsigned int Nz, mat_t *matfp);
int write_char_array_in_MAT_file(char *array, const char *var_name,
				 const int Nx, const int Ny, const int Nz, mat_t *matfp);
int write_int_array_in_MAT_file(int *array, const char *var_name,
				const int Nx, const int Ny, const int Nz, mat_t *matfp);
int write_array_in_MAT_file(double *array, const char *var_name,
			    const int Nx, const int Ny, const int Nz, mat_t *matfp);
int save_data_in_MAT_file(Data_Mem *data);
double compute_volumetric_average_phase_phi(Data_Mem *data, const int phase);
double compute_surface_average_phase_phi(Data_Mem *data, const int phase);
int compute_courant_number(Data_Mem *data);
void close_sfile(Data_Mem *data);
void write_sfile(Data_Mem *data);
void write_gnres_sfile(Data_Mem *data);
void process_sfile(Data_Mem *data);
void write_to_geom_file(Data_Mem *data);
void construct_quadrics(Data_Mem *data);
void set_nsew_to_value(double *A_, const int Nx, const int Ny, const double value);
void set_transient_sim_time_filename(char *filename,
				     const char *msg,
				     const int flag,
				     const char *results_dir);
void save_transient_data(Data_Mem *data, const char *tr_filename);
void save_flow_transient_data(Data_Mem * data, const char* tr_filename);
int save_transient_array(const double *A_,
			 const int Nx, const int Ny,
			 const int count,
			 const char *msg, const int flag,
			 const char *results_dir);
void filter_small_values_omp(double *A_, const int Nx, const int Ny, const int Nz,
			     const double small_number, const int filter_to_zero_OK);
void sum_relax_array(const double *A_, const double *B_, double *C_,
		     const double alpha, const int Nx, const int Ny, const int Nz);
void print_tensor_at_node(const double *tensor, const int I, const int J,
			  const int Nx, const int Ny, const char *msg);
void make_parabolic_profile(Data_Mem *data, const int l_, const int u_,
			    const char face);
void Dirichlet_boundary_indexs_finder(Data_Mem *data);
void Isoflux_boundary_indexs_finder(Data_Mem *data);
void Conjugate_boundary_indexs_finder(Data_Mem *data);
void initialize_phi_in_domain(Data_Mem *data);
void relax_coeffs(const double *field, const int *sel_mat, const int sel_val, 
		  Coeffs coeffs_, const int Nx, const int Ny, const double alpha);
double compute_residues(double *phi, Coeffs *coeffs, const int Nx, const int Ny);
void scale_velocity_self(double *U, double *V, double *W,
			 const int Nx, const int Ny, const int Nz);
int make_p_init_profile(Data_Mem *data);
void get_memory(Data_Mem *data);
void sf_boundary_solid_indexs_finder(Data_Mem *data);
void read_from_MAT_file(Data_Mem *data);
void read_flow_field_from_tmp_file(Data_Mem *data);
void read_from_tmp_file(Data_Mem *data);
void write_to_tmp_file(Data_Mem *data);
void change_alphas_phi(Data_Mem *data);
void change_alphas_flow(Data_Mem *data);
void write_data(Data_Mem *data);
void mask_domain(int *A_, const int Nx, const int Ny);
void copy_array(const double *A_, double *B_, const int Nx, const int Ny, const int Nz);
void copy_array_int(const int *A_, int *B_, const int Nx, const int Ny, const int Nz);
void set_nans_domain(double *phi, const int *filter, const int value,
		     const int Nx, const int Ny, const int Nz);
void set_no_nans_domain(double *phi, const double value,
			const int Nx, const int Ny, const int Nz);
void normalize_self(double *phi, const int Nx, const int Ny, const int Nz);
void normalize(double *phi, const double min, const double max,
	       const int Nx, const int Ny, const int Nz);
void set_bc_value(double *phi, const double value, const char face,
		  const int Nx, const int Ny);
void set_bc_mirror(double *phi, const char face, const int Nx, const int Ny);
double get_max(const double *A_, const int Nx, const int Ny, const int Nz);
double get_min(const double *A_, const int Nx, const int Ny, const int Nz);
void set_nans_corners(double *A_, const int Nx, const int Ny, const int Nz, const int solve_3D_OK);
int write_int_to_file(const int *A_, const int Nx, const int Ny,
		      const char *msg, const int flag, const char *results_dir);
void verify_alloc_int(const int *ptr);
void fill_array_int(int *A_, const int Nx, const int Ny, const int Nz,
		    const int number);
void fill_array_char(char *A_, const int Nx, const int Ny, const int Nz, const char d);
int * create_array_int(const int Nx, const int Ny, const int Nz, const int number);
char * create_array_char(const int Nx, const int Ny, const int Nz, const char d);
u_int64_t elapsed_time(u_int64_t *et);
int write_double_to_file(const double *A_, const int Nx, const int Ny, const char *msg, 
			 const int flag, const char *results_dir);
void free_memory(Data_Mem *data);
void free_grid_info(Grid_Info *bi, const int solve_3D_OK);
void free_coeffs(Coeffs *bi);
void free_bc_info(Boundary_Info *bi);
double * create_array(const int Nx, const int Ny, const int Nz, const double number);
void verify_alloc(const double *ptr);
void fill_array(double *A_, const int Nx, const int Ny, const int Nz,
		const double number);
void print_array(const double *struct_Ptr, const int Nx,
		 const int Ny, const char msg[]);
void fill_phys_prop(Data_Mem *data);
void write_cut_cell_data(Data_Mem *data);
void write_coeffs(Data_Mem *data);
void center_vel_components(Data_Mem *data);
void _center_vel_components(Data_Mem *data);
void _center_vel_components_3D(Data_Mem *data);
void compute_vel_at_main_cells(Data_Mem *data);
void _compute_vel_at_main_cells(Data_Mem *data);
void _compute_vel_at_main_cells_3D(Data_Mem *data);
void compute_gL_value(Data_Mem *data, double *gL_to_calculate);
double gL_function(const double T, const double TPC, const double dTPC);
void avg_phys_prop(Data_Mem *data);
void avg_phys_prop_3D(Data_Mem *data);
void compute_enthalpy_diff(Data_Mem *data);
double max_abs_matrix_diff(const double *A, const double *B,
			   const int Nx, const int Ny);
double max_abs_matrix_diff_3D(const double *A, const double *B,
			      const int Nx, const int Ny, const int Nz);
void update_gL_guess(Data_Mem *data);
void phase_change_cells_vol(Data_Mem *data);
void center_vel_for_server(Data_Mem *data);
void add_gamma_turb(Data_Mem *data);
void add_gamma_turb_3D(Data_Mem *data);
void row_addition(double *matrix, const int row_A, const int row_B,
		  const double alpha, const int mat_size);
int flip_row(double *matrix, const int row_A, const int row_B, const int mat_size);
void calc_inv_matrix(double *inv_matrix, const double *matrix,
		     double *aux_matrix, const int mat_size);
void compute_force_on_solid(Data_Mem *data);
void interpolate_slave_velocities(Data_Mem *data);
void reorder_matrix_elements(double *matrix, const int sub_rows,
			     const int sub_cols, const int rows, const int cols);
void interp_near_v_node(Data_Mem *data, const int index_v, const int z,
			double *int_coeffs, 
			const int *offset_I, const int *offset_J);
void interp_near_u_node(Data_Mem *data, const int index_u, const int z,
			double *int_coeffs, 
			const int *offset_I, const int *offset_J);
double compute_norm_glob_res(const double *field, Coeffs *coeffs_,
			     const int Nx, const int Ny);
void set_variable_arrays_default_size(Data_Mem *data);
void write_in_vel(Data_Mem *data);
void close_in_vel(Data_Mem *data);
void send_email(const char *email, const char *file);
void compute_normal_parallel_products(Data_Mem *data);
void open_check_file(const char file_to_open[SIZE], FILE **fp, Wins *wins,
		     const int mode);
void write_norm_glob_res(Data_Mem *data);
void compute_neighbour_coeffs_factors(Data_Mem *data);
double compute_avg_gL_diff(Data_Mem *data);
void sf_boundary_solid_indexs_finder_3D(Data_Mem *data);
void Dirichlet_boundary_indexs_finder_3D(Data_Mem *data);
void Isoflux_boundary_indexs_finder_3D(Data_Mem *data);
void Conjugate_boundary_indexs_finder_3D(Data_Mem *data);
void relax_coeffs_3D(const double *field, const int *sel_mat, const int sel_val, 
		     Coeffs coeffs_, const int Nx, const int Ny, const int Nz, const double alpha);
double compute_residues_3D(double *phi, Coeffs *coeffs, const int Nx, const int Ny, const int Nz);
double compute_norm_glob_res_3D(const double *field, Coeffs *coeffs_,
				const int Nx, const int Ny, const int Nz);
int make_p_init_profile_3D(Data_Mem *data);
void compute_force_on_solid_3D(Data_Mem *data);
void write_in_vel_3D(Data_Mem *data);
int read_from_geom_file(Data_Mem *data);
void compute_gL_value_3D(Data_Mem *data, double *gL_to_calculate);
void update_gL_guess_3D(Data_Mem *data);
void compute_enthalpy_diff_3D(Data_Mem *data);
double compute_avg_gL_diff_3D(Data_Mem *data);

/* VOL_FRACTION.C */

int compute_solid_vol_fractions(Data_Mem *data);
double vol_fraction_pol(double *c1, double *c2, const double *pc,
			const int curve_is_solid_OK);
double f_test_line(double x, double y, double pend, double inters);
double vol_fraction_pol_3D(double *c1, double *c2, const double *pc,
			   const int curve_is_solid_OK);
int compute_solid_vol_fractions_3D(Data_Mem *data);
double area_fraction_pol(const double *c1, const double *c2, const double *pc,
			 const int curve_is_solid_OK, const int normal_direction);
void face_centroid(double *x_centroid, const double *c0, const double *c1, const double *pc,
		   const int curve_is_solid_OK, const int normal_direction);

/* ADI_SOLVERS.C */

void ADI_omp_3D(double *phi, Coeffs *coeffs,
		const int Nx, const int Ny, const int Nz, ADI_Vars *ADI_vars);
void ADI_sequential_3D(double *phi, Coeffs *coeffs,
		       const int Nx, const int Ny, const int Nz, ADI_Vars *ADI_vars);
void ADI_omp_2D(double *phi, Coeffs *coeffs, const int Nx, const int Ny,
		const int dummy, ADI_Vars *ADI_vars);
void ADI_sequential_2D(double *phi, Coeffs *coeffs, const int Nx, const int Ny,
		       const int dummy, ADI_Vars *ADI_vars);

/* CLASSIFY_CELLS.C */

void classify_cells(Data_Mem *data);
int compute_y_wall(Data_Mem *data);
int compute_volumes(Data_Mem *data);
int compute_cdist_phi(Data_Mem *data);
int set_eps_eps_s_norm_par_legacy(Data_Mem *data);
int set_eps_eps_s_norm_par(Data_Mem *data);
int set_phase_flags(Data_Mem *data);
int compute_lines(Data_Mem *data);
int set_cut_matrix(Data_Mem *data);
int set_dom_matrix(Data_Mem *data);
int set_phase_change_matrix(Data_Mem *data);
int set_cut_matrix_3D(Data_Mem *data);
int set_dom_matrix_3D(Data_Mem *data);
int set_eps_eps_s_norm_par_legacy_3D(Data_Mem *data);
int compute_y_wall_3D(Data_Mem *data);
void compute_par_vec_1(double *par_vec, const double *norm_vec);
void compute_par_vec_2(double *par_vec, const double *norm_vec);
int all_pos_neg_octant(const double *vec);
void compute_par_vec_1_2(double *par_vec, const double *norm_vec);
void compute_par_vec_2_2(double *par_vec, const double *norm_vec);

/* CUT_CELL_FUNCTIONS.C */

void cut_cell_jobs(Data_Mem *data);
void available_vel_nodes(Data_Mem *data);
void add_boundary_p_nodes(Data_Mem *data);
int face_area(Data_Mem *data);
void displace_vel_nodes(Data_Mem *data);
void find_small_pressure_cells(Data_Mem *data);
void set_slave_nodes(Data_Mem *data);
void compute_alphas(Data_Mem *data);
int compute_solid_factors(Data_Mem *data);
void set_p_nodes_in_solid_to_zero(Data_Mem *data);
int compute_fnsew_u_v(Data_Mem *data);
int fluid_fraction_v_cells(Data_Mem *data);
int compute_fnsew_u_v_cut_cells(Data_Mem *dat);
void p_nodes_in_solid(Data_Mem *data);
int set_cut_matrix_v_cells(Data_Mem *data);
int face_area_3D(Data_Mem *data);
int face_area_p_cells(Data_Mem *data);
int set_cut_faces_p_cells(Data_Mem *data);
void find_small_pressure_cells_3D(Data_Mem *data);
void set_slave_nodes_3D(Data_Mem *data);
int fluid_fraction_v_cells_3D(Data_Mem *data);
void p_nodes_in_solid_3D(Data_Mem *data);
int compute_solid_factors_3D(Data_Mem *data);
void compute_alphas_3D(Data_Mem *data);
int compute_fnsewtb_u_v_w_cut_cells(Data_Mem *dat);
int compute_cdist(Data_Mem *data);
int __compute_cdist(double *cdE, double *cdW, double *cdN, double *cdS,
		    const int Nx, const int Ny, const int curves,
		    const int* dom, const int *curve_is_solid_OK,
		    const double *x, const double *y,
		    const double *poly_coeffs, const double cell_size);
int __compute_cdist_3D(double *cdE, double *cdW, double *cdN, double *cdS, double *cdT, double *cdB,
		       const int Nx, const int Ny, const int Nz, const int curves,
		       const int* dom, const int *curve_is_solid_OK,
		       const double *x, const double *y, const double *z,
		       const double *poly_coeffs, const double cell_size);
void set_sol_factor_type_cells(Data_Mem *data);
void _set_sol_factor_type_cells(int *_sol_type,
				const int *_slave_nodes, const int *_sol_factor_index_,
				const int *_dom_matrix,
				const int _slave_count, const int _sol_factor_count);
void print_cut_cell_info(Data_Mem *data);

/* MAIN.C */

void signal_handler(int);
void timer_handler(const int signum);
void print_usage(FILE* stream, const int exit_code, const char* program_name);
void print_pendings(Wins *wins);

/* SERVER.C */

void set_update_clients_flag (int flag_value);
void * server_thr_function(void *sdata);
void send_data_to_client(int sock, Data_Mem *data);
void send_array_to_client(int sock, const double *array,
			  const int Nx, const int Ny, const int size_x, const int bans);
void send_size_to_client(int sock, const int Nx, const int Ny,
			 const int flow_done_OK, const int turb_OK);
void update_server(Data_Mem *data);
void data_resize_for_server(Data_Mem *data);
int pos_int_pow(int base, int exp);

/* TEXT_PARSER.C */

int text_parser(Data_Mem *Ptr, const char *config_filename);
int get_bc_info_from_file(Boundary_Info *bi, FILE *fPtr, const int solve_3D_OK);
void get_grid_info_from_file(Grid_Info *gi, FILE *fPtr);
int get_bc_flow_info_from_file(b_cond_flow *bc, FILE *fPtr);
int get_per_parts_bc(double *perx, FILE *fPtr);

/* TEG.C */

int ymy_teg_phase(const TEG *teg);
void save_transient_teg_data(Data_Mem *data, const char *tr_filename);
int set_TEM_properties(TEG *teg);
int solve_current(Data_Mem *data, char *current_filename);

/* SOLVE_PHI_3D.C */

void compute_near_boundary_molecular_experimental_3D(Data_Mem *data);
void compute_near_boundary_molecular_flux_Tsutsumi_def_3D(Data_Mem *data);
void compute_near_boundary_molecular_flux_Tsutsumi_3D(Data_Mem *data);
void compute_near_boundary_molecular_flux_Sato_at_face_3D(Data_Mem *data);
void compute_solid_der_at_interface_at_face_3D(Data_Mem *data);
void debug_phi(Data_Mem *data, char c);
void compute_near_boundary_convective_flux_3D(Data_Mem *data);
void compute_near_boundary_molecular_flux_Sato_3D(Data_Mem *data);
void compute_solid_der_at_interface_3D(Data_Mem *data);
void compute_phi_SC_at_interface_Isoflux_3D(Data_Mem *data);
void solve_phi_transient_3D(Data_Mem *data);
void solve_phi_steady_3D(Data_Mem *data);
void solve_phi_pre_calc_jobs_3D(Data_Mem *data);
double solve_phi_jobs_3D(Data_Mem *data);
void set_bc_3D(Data_Mem *data);
void compute_phi_SC_at_interface_Dirichlet_3D(Data_Mem *data);
void compute_phi_SC_at_interface_3D(Data_Mem *data);
void coeffs_phi_power_law_3D(Data_Mem *data);
void set_F_phi_3D(Data_Mem *data);
void set_D_phi_3D(Data_Mem *data);
void __get_b_values_Dirichlet(double *der_at_b, double *phi_m,
			      const double e_, const double e_tol,
			      const double phi_b, const double phi_c,
			      const double phi_f,
			      const double Dic, const double Dcf,
			      const int dir_fact);

/* COEFFS_FLOW_TVD.C */
double __van_Leer(double r, double R);
double __wahyd(double r, double R);
double __QUICK(double r, double R);
double __QUICK_TVD(double r, double R);
double __upwind(double r, double R);
void coeffs_flow_u_TVD(Data_Mem *data);
void coeffs_flow_v_TVD(Data_Mem *data);
void coeffs_flow_u_TVD_3D(Data_Mem *data);
void coeffs_flow_v_TVD_3D(Data_Mem *data);
void coeffs_flow_w_TVD_3D(Data_Mem *data);

/* COEFFS_PHI_TVD.C */
void coeffs_phi_TVD(Data_Mem *data);
void coeffs_phi_TVD_3D(Data_Mem *data);

/* MACROS */
#define MAX(a,b) ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a > _b ? _a : _b; })
#define MIN(a,b) ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a < _b ? _a : _b; })

#define checkCall(err,wins)  __checkCall((err), (wins), __FILE__, __LINE__)

inline void __checkCall(const int err, Wins *wins, const char *file, const int line){
  char msg[SIZE];
  
  if(err != 0) {
    
    snprintf(msg, SIZE, "%s(%d): checkCall() error == %d",
	     file, line, err);
    alert_msg(wins, msg);
  }
  return;
}

/* __CENTRAL_B_DER */
/*****************************************************************************/
/** 
    Eq 25 Sato

    @param e: Normalized distance to boundary.

    @param dphib: Derivative at boundary.

    @param phif: Value outside boundary with greater coordinate value.

    @param phii: Value outside boundary with lesser coordinate value.

    @param Db: Distance from phif to phi inside boundary.

    @param Dif: Distance from phif to phii.
    
*/
inline double __central_b_der(const double e, const double dphib,
			      const double phif, const double phii,
			      const double Db, const double Dif){

  double der = 0;

  const double m = (0.5*Dif) / (e*Db);

  const double derf = (phif - phii) / Dif;

  der = (1/(1 + m)) * (m*dphib + derf);

  der = (e > 0) ? der : dphib;
  
  return der;
}

/* __LINEAR_EXTRAP_TO_B */
/*****************************************************************************/
/** 
    Eq 28 Sato

    @param di: Derivative (or value) outside and nearest to boundary.

    @param df: Derivative (or value) outside and fartest to boundary.
    
    @param e: Normalized distance to boundary.

    @param D0i: Distance between di and 0 across phase.

    @param Dif: Distance between di and df.

    @return db: Derivative (or value) at boundary.
    
*/
inline double __linear_extrap_to_b(const double di, const double df,
				   const double e,
				   const double D0i, const double Dif){

  double db = 0;

  const double Da = e*D0i + Dif;

  const double m = e*D0i / Da;

  db = di / (1 - m) - m * df / (1 - m);
  
  return db;
}

/* __ASYMM_B_DER */
/*****************************************************************************/
/**
   Compute asymmetric derivative evaluated at the boundary, 
   considering variable distances between points.
   This function is supossed to be used only for the Dirichlet cases.
   
   @param phi_b: value at b, where the derivative will be computed.
   @param phi_c: value at P.
   @param phi_f: value at far end of boundary.
   @param Dbc: distance between phi_b and phi_c.
   @param Dcf: distance between phi_c and phi_f.
   @param dir_fact: direction factor, set to +1 for ENT_is_solid_OK == true.
  
   @return value of derivative at b
   
   MODIFIED: 13-07-2020

*/
inline double __asymm_b_der(const double phi_b, const double phi_c, const double phi_f,
		     const double Dbc, const double Dcf,
		     const int dir_fact){
  
  double a, b, c;
  double Da, Den;
  
  double der = 0;

  Da = Dbc + Dcf;

  Den =  (Dbc*Dbc) - Dbc*Da;

  /* Programming for dir_fact == -1 case (ENT)_is_solid_OK == true */
  b = - Da / Den;
  c = 1 / Da + Dbc / Den;

  if(dir_fact == +1){
    /* Now is the case for dir_fact == +1 case (WSB)_is_solid_OK == true */
    b = -b;
    c = -c;
  }
  
  a = - b - c;

  der = a*phi_b + b*phi_c + c*phi_f;

  return der;
}

/* __CENTRAL_DER */
/*****************************************************************************/
/**
   Compute central derivative considering variable distances between points.
   This function is supossed to be used only for the Conjugated cases, 
   levels [1] and [2] within solid body.
   
   @param phiE: value at E (N|T).
   @param phiP: value at P where the derivative will be computed.
   @param phiW: value at W (S|B).
   @param DE: distance between phiE and phiP.
   @param DW: distance between phiP and phiW.
  
   @return value of central derivative at P
   
   MODIFIED: 11-07-2020
*/
inline double __central_der(const double phiE, const double phiP, const double phiW,
			    const double DE, const double DW){
  
  double a, b, c;
  double Da;

  double der = 0;

  Da = DE + DW;

  if(DE == DW){
    
    /* Necessary for round-off errors */
    der = (phiE - phiW) / Da;

  }
  else{

    a = (DW/DE) * (1/Da);
    c = (1/Da) - 1/DW;
    b = -c-a;

    der = a*phiE + b*phiP + c*phiW;
  }
  
  return der;
}

/* __ASYMM_DER */
/*****************************************************************************/
/**
   Compute asymmetric derivative considering variable distances between points.
   
   @param phii: value at P, where the derivative will be computed.
   @param phim: value at W|S|B for dir == +1. if dir == -1 then value is E|N|T.
   @param phif: value at WW|SS|BB for dir == +1. if dir == -1 then value is EE|NN|TT.
   @param Dim: distance between phii and phim.
   @param Dmf: distance between phim and phif.
   @param dir: direction factor.
  
   @return value of derivative at i
   
   MODIFIED: 10-07-2020
*/
inline double __asymm_der(const double phii, const double phim, const double phif,
			  const double Dim, const double Dmf, const int dir){
  
  double a, b, c;
  double Da;

  double der = 0;

  if(Dmf > 0){
     
    Da = Dim + Dmf;

    b = (Da/Dim) * (1 / (Dim - Da));
    c = 1/(Da-Dim) - 1/Da;

    if(dir == -1){
      b = -b;
      c = -c;
    }
  
    a = -b-c;

    der = a*phii + b*phim + c*phif;
  }
  else if(Dmf <= 0){

    der = dir * (phii - phim) / Dim;

  }
    
  return der;
}
