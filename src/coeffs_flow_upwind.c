#include "include/some_defs.h"

extern sig_atomic_t action_on_signal_OK;
extern int timer_flag_OK;

/* COEFFS_FLOW_u_UPWIND */
/*****************************************************************************/
/**
  
  Sets:

  Structure coeffs_u: coefficients of u momentum equations for internal nodes.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 11-06-2019
*/
void coeffs_flow_u_upwind(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face, index_u;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int p_source_orientation = data->p_source_orientation;
  const int u_slave_count = data->u_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_u_face_x_count = data->diff_u_face_x_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
  const int *diff_u_face_x_I = data->diff_u_face_x_I;

  double dpA;
  double mu_eff;
  double De, Dw, Dn, Ds;
  double Fe, Fw, Fn, Fs;
  double aP0, S_diff;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 2; // For 2D case
  const double periodic_source = data->periodic_source;
 
  double *aP_u = data->coeffs_u.aP;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *b_u = data->coeffs_u.b;

  const double *u = data->u;
  const double *p = data->p;

  const double *u_faces_x = data->u_faces_x;

  const double *Du_x = data->Du_x;
  const double *Du_y = data->Du_y;
  const double *Fu_x = data->Fu_x;
  const double *Fu_y = data->Fu_y;
  const double *u_sol_factor = data->u_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  const double *vol_x = data->vol_x;
  const double *alpha_u_x = data->alpha_u_x;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *u0 = data->u0;
  const double *diff_factor_u_face_x = data->diff_factor_u_face_x;
  const double *u_cut_fw = data->u_cut_fw;
  const double *u_cut_fe = data->u_cut_fe;
  
  set_D_u(data);
  set_F_u(data);

#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_u, index_face, De, Dw, Dn, Ds, Fe, Fw, Fn, Fs, dpA) \
  shared(u_dom_matrix, aE_u, aW_u, aS_u, aN_u, aP_u, b_u, u, p, u_faces_x, \
	 Du_x, Du_y, Fu_x, Fu_y, mu_turb_at_u_nodes, alpha_u_x)

  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      index_u = J*nx + i;

      if(u_dom_matrix[index_u] == 0){

	I = i+1;      
	j = J-1;
      
	index_ = J*Nx + I;
	index_face = j*nx + i;

	/* Diffusive */
	De = Du_x[index_];
	Dw = Du_x[index_-1];
	Dn = Du_y[index_face+nx];
	Ds = Du_y[index_face];

	/* Convective */
	Fe = Fu_x[index_] * alpha_u_x[index_];
	Fw = Fu_x[index_-1] * alpha_u_x[index_-1];
	Fn = Fu_y[index_face+nx];
	Fs = Fu_y[index_face];

	aE_u[index_u] = De + MAX(-Fe, 0);
	aW_u[index_u] = Dw + MAX(Fw, 0);
	aN_u[index_u] = Dn + MAX(-Fn, 0);
	aS_u[index_u] = Ds + MAX(Fs, 0);

	aP_u[index_u] = aE_u[index_u] + aW_u[index_u] + aN_u[index_u] + aS_u[index_u];
	  //	  De + Dw + Dn + Ds + MAX(Fe, 0) + MAX(-Fw, 0) + MAX(Fn, 0) + MAX(-Fs, 0);

	dpA = u_faces_x[index_] * p[index_] - 
	  u_faces_x[index_-1] * p[index_-1];

	b_u[index_u] = -dpA;
	// - 2*rho*(k_turb[index_] - k_turb[index_-1]) / (3 * Delta_xW[index_]);
      }
      else{
	aP_u[index_u] = 1;
	aW_u[index_u] = 0;
	aE_u[index_u] = 0;
	aS_u[index_u] = 0;
	aN_u[index_u] = 0;
	b_u[index_u] = 0;
      }
    }
  }

  /* Diffusive factor */  
  for(z=0; z<diff_u_face_x_count; z++){
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    I = diff_u_face_x_I[z];
    mu_eff = mu + 0.5 * (mu_turb_at_u_nodes[index_u] + mu_turb_at_u_nodes[index_u+1]);
    S_diff = mu_eff * diff_factor_u_face_x[z] * (u[index_u] * u_cut_fw[index_] + u[index_u+1] * u_cut_fe[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */
    if((u_dom_matrix[index_u] == 0) && (I > 1)){
      b_u[index_u] -= S_diff;
    }
    if((u_dom_matrix[index_u+1] == 0) && (I < Nx-2)){
      b_u[index_u+1] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<u_sol_factor_count; z++){
      index_u = u_sol_factor_index_u[z];
      
      if(u_dom_matrix[index_u] == 0){
	index_ = u_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_u_nodes[index_u];
	aP_u[index_u] += mu_eff * u_sol_factor[z];
	b_u[index_u] += u_p_factor[z] * (u_p_factor_W[z] * p[index_-1]
					 + (1 - u_p_factor_W[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 1){

#pragma omp parallel for default(none) private(i, J, index_u)	\
  shared(u_dom_matrix, b_u, vol_x)

    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = J*nx + i;
	if(u_dom_matrix[index_u] == 0){
	  b_u[index_u] += periodic_source * vol_x[index_u];
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(i, J, index_u, aP0)	\
  shared(u_dom_matrix, aP_u, b_u, vol_x, u0)

    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = J*nx + i;
	aP0 = rho * vol_x[index_u] / dt;
	if(u_dom_matrix[index_u] == 0){
	  aP_u[index_u] += aP0;
	  b_u[index_u] += aP0 * u0[index_u];
	}
      }
    }
  }

  /* Add custom source term */
  if(add_momentum_source_OK){
    vel_at_crossed_faces(data);
    add_u_momentum_source(data);
  }

  /* Relax coefficients */
  if(pv_coupling_algorithm){
    relax_coeffs(u, u_dom_matrix, 0, data->coeffs_u, nx, Ny, data->alpha_u);
  }

  /* Set coefficients for slave cells */
  for(i=0; i<u_slave_count; i++){
    index_u = u_slave_nodes[i];
    aP_u[index_u] = 1;
    aW_u[index_u] = 0;
    aE_u[index_u] = 0;
    aS_u[index_u] = 0;
    aN_u[index_u] = 0;
    b_u[index_u] = 0;
  }
  
  return;
}

/* COEFFS_FLOW_v_UPWIND */
/*****************************************************************************/
/**
  
  Sets:

  Structure coeffs_v: coefficients of v momentum equation for internal nodes.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 11-06-2019
*/
void coeffs_flow_v_upwind(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int v_slave_count = data->v_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int p_source_orientation = data->p_source_orientation;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_v_face_y_count = data->diff_v_face_y_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *v_dom_matrix = data->v_dom_matrix;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
  const int *diff_v_face_y_J = data->diff_v_face_y_J;

  double mu_eff;
  double De, Dw, Dn, Ds;
  double Fe, Fw, Fn, Fs;
  double dpA, aP0, S_diff;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 2; // For 2D case
  const double periodic_source = data->periodic_source;
 
  double *aP_v = data->coeffs_v.aP;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *b_v = data->coeffs_v.b;

  const double *v = data->v;
  const double *p = data->p;

  const double *v_faces_y = data->v_faces_y;
  const double *Dv_x = data->Dv_x;
  const double *Dv_y = data->Dv_y;
  const double *Fv_x = data->Fv_x;
  const double *Fv_y = data->Fv_y;
  const double *v_sol_factor = data->v_sol_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  const double *vol_y = data->vol_y;
  const double *alpha_v_y = data->alpha_v_y;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *v0 = data->v0;
  const double *v_cut_fs = data->v_cut_fs;
  const double *v_cut_fn = data->v_cut_fn;
  const double *diff_factor_v_face_y = data->diff_factor_v_face_y;

  set_D_v(data);
  set_F_v(data);
  
  /* V */
#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_v, index_face, De, Dw, Dn, Ds, Fe, Fw, Fn, Fs, dpA) \
  shared(v_dom_matrix, aE_v, aW_v, aS_v, aN_v, aP_v, b_v, v, p, v_faces_y, \
	 Dv_x, Dv_y, Fv_x,						\
	 Fv_y, mu_turb_at_v_nodes, alpha_v_y)

  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){

      index_v = j*Nx + I;

      if(v_dom_matrix[index_v] == 0){

	i = I-1;
	J = j+1;
      
	index_ = J*Nx + I;
	index_face = j*nx + i;
	
	/* Diffusive */
	De = Dv_x[index_face+1];
	Dw = Dv_x[index_face];
	Dn = Dv_y[index_];
	Ds = Dv_y[index_-Nx];

	/* Convective */
	Fe = Fv_x[index_face+1];
	Fw = Fv_x[index_face];
	Fn = Fv_y[index_] * alpha_v_y[index_];
	Fs = Fv_y[index_-Nx] * alpha_v_y[index_-Nx];

	aE_v[index_v] = De + MAX(-Fe, 0);
	aW_v[index_v] = Dw + MAX(Fw, 0);
	aN_v[index_v] = Dn + MAX(-Fn, 0);
	aS_v[index_v] = Ds + MAX(Fs, 0);

	aP_v[index_v] = aE_v[index_v] + aW_v[index_v] + aN_v[index_v] + aS_v[index_v];
	  //	  De + Dw + Dn + Ds + MAX(Fe, 0) + MAX(-Fw, 0) + MAX(Fn, 0) + MAX(-Fs, 0); 

	dpA = v_faces_y[index_] * p[index_] - 
	  v_faces_y[index_-Nx] * p[index_-Nx];

	b_v[index_v] = -dpA;
	//- 2*rho*(k_turb[index_] - k_turb[index_-Nx]) / (3*Delta_yS[index_]);
      }
      else{
	aP_v[index_v] = 1;
	aW_v[index_v] = 0;
	aE_v[index_v] = 0;
	aS_v[index_v] = 0;
	aN_v[index_v] = 0;
	b_v[index_v] = 0;
      }
    }
  }

  /* Diffusive factor */
  for(z=0; z<diff_v_face_y_count; z++){
    index_v = diff_v_face_y_index_v[z];
    index_ = diff_v_face_y_index[z];
    J = diff_v_face_y_J[z];
    mu_eff = mu + 0.5 * (mu_turb_at_v_nodes[index_v] + mu_turb_at_v_nodes[index_v+Nx]);
    S_diff = mu_eff * diff_factor_v_face_y[z] * (v[index_v] * v_cut_fs[index_] + v[index_v+Nx] * v_cut_fn[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((v_dom_matrix[index_v] == 0) && (J > 1)){
      b_v[index_v] -= S_diff;
    }
    if((v_dom_matrix[index_v+Nx] == 0) && (J < Ny-2)){
      b_v[index_v+Nx] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<v_sol_factor_count; z++){
      
      index_v = v_sol_factor_index_v[z];
      
      if(v_dom_matrix[index_v] == 0){
	index_ = v_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_v_nodes[index_v];
	aP_v[index_v] += mu_eff * v_sol_factor[z];
	b_v[index_v] += v_p_factor[z] * (v_p_factor_S[z] * p[index_-Nx]
					 + (1 - v_p_factor_S[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, j, index_v)	\
  shared(v_dom_matrix, b_v, vol_y)

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = j*Nx + I;
	if(v_dom_matrix[index_v] == 0){
	  b_v[index_v] += periodic_source * vol_y[index_v];
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, j, index_v, aP0)	\
  shared(v_dom_matrix, aP_v, b_v, vol_y, v0)

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = j*Nx + I;
	if(v_dom_matrix[index_v] == 0){
	  aP0 = rho * vol_y[index_v] / dt;
	  aP_v[index_v] += aP0;
	  b_v[index_v] += aP0 * v0[index_v];
	}
      }
    }
  }

  /* Add custom source term */
  if(add_momentum_source_OK){
    vel_at_crossed_faces(data);
    add_v_momentum_source(data);
  }

  /* Relax coefficients */
  if(pv_coupling_algorithm){
    relax_coeffs(v, v_dom_matrix, 0, data->coeffs_v, Nx, ny, data->alpha_v);
  }

  for(i=0; i<v_slave_count; i++){
    index_v = v_slave_nodes[i];
    aP_v[index_v] = 1;
    aW_v[index_v] = 0;
    aE_v[index_v] = 0;
    aS_v[index_v] = 0;
    aN_v[index_v] = 0;
    b_v[index_v] = 0;
  }

  return;
}

/* COEFFS_FLOW_U_UPWIND_3D */
/*****************************************************************************/
/**
  
  Sets:

  Structure coeffs_u: coefficients of u momentum equations for internal nodes.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 03-10-2019
*/
void coeffs_flow_u_upwind_3D(Data_Mem *data){

  int I, J, K, i, j, k, z, index_;
  int index_face_x, index_face_y, index_face_z, index_u;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;  
  const int nx = data->nx;
  const int ny = data->ny;
  const int p_source_orientation = data->p_source_orientation;
  const int u_slave_count = data->u_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_u_face_x_count = data->diff_u_face_x_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
  const int *diff_u_face_x_I = data->diff_u_face_x_I;

  double dpA;
  double mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double aP0, S_diff;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_u = data->coeffs_u.aP;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *aB_u = data->coeffs_u.aB;
  double *aT_u = data->coeffs_u.aT;
  double *b_u = data->coeffs_u.b;

  const double *u = data->u;
  const double *p = data->p;
  const double *u_faces_x = data->u_faces_x;
  const double *Du_x = data->Du_x;
  const double *Du_y = data->Du_y;
  const double *Du_z = data->Du_z;
  const double *Fu_x = data->Fu_x;
  const double *Fu_y = data->Fu_y;
  const double *Fu_z = data->Fu_z;
  const double *u_sol_factor = data->u_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  const double *vol_x = data->vol_x;
  const double *alpha_u_x = data->alpha_u_x;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *u0 = data->u0;
  const double *diff_factor_u_face_x = data->diff_factor_u_face_x;
  const double *u_cut_fw = data->u_cut_fw;
  const double *u_cut_fe = data->u_cut_fe;
  
  set_D_u_3D(data);
  set_F_u_3D(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_u, index_face_x, index_face_y, index_face_z, De, Dw, \
	  Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb, dpA)			\
  shared(u_dom_matrix, aE_u, aW_u, aS_u, aN_u, aT_u, aB_u, aP_u, b_u, u, p, u_faces_x, \
	 Du_x, Du_y, Du_z, Fu_x, Fu_y, Fu_z, mu_turb_at_u_nodes, alpha_u_x)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){

	index_u = K*nx*Ny + J*nx + i;

	if(u_dom_matrix[index_u] == 0){

	  I = i+1;      
	  j = J-1;
	  k = K-1;
      
	  index_face_x = K*Nx*Ny + J*Nx + I - 1;
	  index_face_y = K*nx*ny + j*nx + i;
	  index_face_z = k*nx*Ny + J*nx + i;

	  /* Diffusive */
	  De = Du_x[index_face_x+1];
	  Dw = Du_x[index_face_x];
	  Dn = Du_y[index_face_y+nx];
	  Ds = Du_y[index_face_y];
	  Dt = Du_z[index_face_z+nx*Ny];
	  Db = Du_z[index_face_z];

	  /* Convective */
	  Fe = Fu_x[index_face_x+1] * alpha_u_x[index_face_x+1];
	  Fw = Fu_x[index_face_x] * alpha_u_x[index_face_x];
	  Fn = Fu_y[index_face_y+nx];
	  Fs = Fu_y[index_face_y];
	  Ft = Fu_z[index_face_z+nx*Ny];
	  Fb = Fu_z[index_face_z];

	  aE_u[index_u] = De + MAX(-Fe, 0);
	  aW_u[index_u] = Dw + MAX(Fw, 0);
	  aN_u[index_u] = Dn + MAX(-Fn, 0);
	  aS_u[index_u] = Ds + MAX(Fs, 0);
	  aT_u[index_u] = Dt + MAX(-Ft, 0);
	  aB_u[index_u] = Db + MAX(Fb, 0);

	  aP_u[index_u] = aE_u[index_u] + aW_u[index_u] + aN_u[index_u] + aS_u[index_u] + aT_u[index_u] + aB_u[index_u];
	    //	    De + Dw + Dn + Ds + Dt + Db + MAX(Fe, 0) + MAX(-Fw, 0) + MAX(Fn, 0) + MAX(-Fs, 0) + MAX(Ft, 0) + MAX(-Fb, 0);
	  
	  dpA = u_faces_x[index_face_x+1] * p[index_face_x+1] - 
	    u_faces_x[index_face_x] * p[index_face_x];

	  b_u[index_u] = -dpA;
	  // - 2*rho*(k_turb[index_] - k_turb[index_-1]) / (3 * Delta_xW[index_]);
	}
	else{
	  aP_u[index_u] = 1;
	  aW_u[index_u] = 0;
	  aE_u[index_u] = 0;
	  aS_u[index_u] = 0;
	  aN_u[index_u] = 0;
	  aB_u[index_u] = 0;
	  aT_u[index_u] = 0;
	  b_u[index_u] = 0;
	}
      }
    }
  }

  
  /* Diffusive factor */
  
  for(z=0; z<diff_u_face_x_count; z++){
    
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    I = diff_u_face_x_I[z];
    mu_eff = mu + 0.5 * (mu_turb_at_u_nodes[index_u] + mu_turb_at_u_nodes[index_u+1]);
    S_diff = mu_eff * diff_factor_u_face_x[z] * (u[index_u] * u_cut_fw[index_] + u[index_u+1] * u_cut_fe[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((u_dom_matrix[index_u] == 0) && (I > 1)){
      b_u[index_u] -= S_diff;
    }
    if((u_dom_matrix[index_u+1] == 0) && (I < Nx-2)){
      b_u[index_u+1] += S_diff;
    }
  }


  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){

#pragma omp parallel for default(none) private(z, index_u, index_, mu_eff) \
  shared(u_sol_factor_index_u, u_sol_factor_index, u_dom_matrix, mu_turb_at_u_nodes, \
	 u_sol_factor, u_p_factor, u_p_factor_W, p, aP_u, b_u)
    
    for(z=0; z<u_sol_factor_count; z++){
      
      index_u = u_sol_factor_index_u[z];
      
      if(u_dom_matrix[index_u] == 0){
	index_ = u_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_u_nodes[index_u];
	aP_u[index_u] += mu_eff * u_sol_factor[z];
	b_u[index_u] += u_p_factor[z] * (u_p_factor_W[z] * p[index_-1]
					 + (1 - u_p_factor_W[z]) * p[index_]);
      }
    }
  }


  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 1){

#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(u_dom_matrix, b_u, vol_x)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	  index_u = K*nx*Ny + J*nx + i;
	  if(u_dom_matrix[index_u] == 0){
	    b_u[index_u] += periodic_source * vol_x[index_u];
	  }
	}
      }
    }
  }


  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(i, J, K, index_u, aP0)	\
  shared(u_dom_matrix, aP_u, b_u, vol_x, u0)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	  index_u = K*nx*Ny + J*nx + i;
	  aP0 = rho * vol_x[index_u] / dt;
	  if(u_dom_matrix[index_u] == 0){
	    aP_u[index_u] += aP0;
	    b_u[index_u] += aP0 * u0[index_u];
	  }
	}
      }
    }
  }

  /* Add custom source term */
  if(add_momentum_source_OK){
    vel_at_crossed_faces(data);
    add_u_momentum_source(data);
  }

  /* Relax coefficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(u, u_dom_matrix, 0, data->coeffs_u, nx, Ny, Nz, data->alpha_u);
  }

  /* Set coefficients for slave cells */

#pragma omp parallel for default(none) private(i, index_u)		\
  shared(u_slave_nodes, aP_u, aW_u, aE_u, aS_u, aN_u, aB_u, aT_u, b_u)
  
  for(i=0; i<u_slave_count; i++){
    index_u = u_slave_nodes[i];
    aP_u[index_u] = 1;
    aW_u[index_u] = 0;
    aE_u[index_u] = 0;
    aS_u[index_u] = 0;
    aN_u[index_u] = 0;
    aB_u[index_u] = 0;
    aT_u[index_u] = 0;
    b_u[index_u] = 0;
  }
  
  return;
}

/* COEFFS_FLOW_V_UPWIND_3D */
/*****************************************************************************/
/**

  Sets:

  Structure coeffs_v: coefficients of v momentum equation for internal nodes.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 10-10-2019
*/
void coeffs_flow_v_upwind_3D(Data_Mem *data){

  int I, J, K, i, j, k, z, index_;
  int index_face_x, index_face_y, index_face_z, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;  
  const int nx = data->nx;
  const int ny = data->ny;
  const int p_source_orientation = data->p_source_orientation;
  const int v_slave_count = data->v_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_v_face_y_count = data->diff_v_face_y_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *v_dom_matrix = data->v_dom_matrix;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
  const int *diff_v_face_y_J = data->diff_v_face_y_J;

  double dpA;
  double mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double aP0, S_diff;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_v = data->coeffs_v.aP;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *aB_v = data->coeffs_v.aB;
  double *aT_v = data->coeffs_v.aT;
  double *b_v = data->coeffs_v.b;

  const double *v = data->v;
  const double *p = data->p;
  const double *v_faces_y = data->v_faces_y;
  const double *Dv_x = data->Dv_x;
  const double *Dv_y = data->Dv_y;
  const double *Dv_z = data->Dv_z;
  const double *Fv_x = data->Fv_x;
  const double *Fv_y = data->Fv_y;
  const double *Fv_z = data->Fv_z;
  const double *v_sol_factor = data->v_sol_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  const double *vol_y = data->vol_y;
  const double *alpha_v_y = data->alpha_v_y;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *v0 = data->v0;
  const double *diff_factor_v_face_y = data->diff_factor_v_face_y;
  const double *v_cut_fs = data->v_cut_fs;
  const double *v_cut_fn = data->v_cut_fn;
  
  set_D_v_3D(data);
  set_F_v_3D(data);
  
  /* V */
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_v, index_face_x, index_face_y, index_face_z, De, Dw, \
	  Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb, dpA)			\
  shared(v_dom_matrix, aE_v, aW_v, aS_v, aN_v, aT_v, aB_v, aP_v, b_v, v, p, v_faces_y, \
	 Dv_x, Dv_y, Dv_z, Fv_x, Fv_y, Fv_z, mu_turb_at_v_nodes, alpha_v_y)

  for(K=1; K<Nz-1; K++){
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){

	index_v = K*Nx*ny + j*Nx + I;

	if(v_dom_matrix[index_v] == 0){

	  i = I-1;
	  J = j+1;
	  k = K-1;
      
	  index_face_x = K*nx*ny + j*nx + i;
	  index_face_y = K*Nx*Ny + (J-1)*Nx + I;
	  index_face_z = k*Nx*ny + j*Nx + I;
	
	  /* Diffusive */
	  De = Dv_x[index_face_x+1];
	  Dw = Dv_x[index_face_x];
	  Dn = Dv_y[index_face_y+Nx];
	  Ds = Dv_y[index_face_y];
	  Dt = Dv_z[index_face_z+Nx*ny];
	  Db = Dv_z[index_face_z];

	  /* Convective */
	  Fe = Fv_x[index_face_x+1];
	  Fw = Fv_x[index_face_x];
	  Fn = Fv_y[index_face_y+Nx] * alpha_v_y[index_face_y+Nx];
	  Fs = Fv_y[index_face_y] * alpha_v_y[index_face_y];
	  Ft = Fv_z[index_face_z+Nx*ny];
	  Fb = Fv_z[index_face_z];

	  aE_v[index_v] = De + MAX(-Fe, 0);
	  aW_v[index_v] = Dw + MAX(Fw, 0);
	  aN_v[index_v] = Dn + MAX(-Fn, 0);
	  aS_v[index_v] = Ds + MAX(Fs, 0);
	  aT_v[index_v] = Dt + MAX(-Ft, 0);
	  aB_v[index_v] = Db + MAX(Fb, 0);

	  aP_v[index_v] = aE_v[index_v] + aW_v[index_v] + aN_v[index_v] + aS_v[index_v] + aT_v[index_v] + aB_v[index_v];
	    //	    De + Dw + Dn + Ds + Dt + Db + MAX(Fe, 0) + MAX(-Fw, 0) + MAX(Fn, 0) + MAX(-Fs, 0) + MAX(Ft, 0) + MAX(-Fb, 0); 

	  dpA = v_faces_y[index_face_y+Nx] * p[index_face_y+Nx] - 
	    v_faces_y[index_face_y] * p[index_face_y];

	  b_v[index_v] = -dpA;
	  //- 2*rho*(k_turb[index_] - k_turb[index_-Nx]) / (3*Delta_yS[index_]);
	}
	else{
	  aP_v[index_v] = 1;
	  aW_v[index_v] = 0;
	  aE_v[index_v] = 0;
	  aS_v[index_v] = 0;
	  aN_v[index_v] = 0;
	  aB_v[index_v] = 0;
	  aT_v[index_v] = 0;
	  b_v[index_v] = 0;
	}
      }
    }
  }

  /* Diffusive factor */
  
  for(z=0; z<diff_v_face_y_count; z++){

    index_ = diff_v_face_y_index[z];    
    index_v = diff_v_face_y_index_v[z];
    J = diff_v_face_y_J[z];
    mu_eff = mu + 0.5 * (mu_turb_at_v_nodes[index_v] + mu_turb_at_v_nodes[index_v+Nx]);
    S_diff = mu_eff * diff_factor_v_face_y[z] * (v[index_v] * v_cut_fs[index_] + v[index_v+Nx] * v_cut_fn[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((v_dom_matrix[index_v] == 0) && (J > 1)){
      b_v[index_v] -= S_diff;
    }
    if((v_dom_matrix[index_v+Nx] == 0) && (J < Ny-2)){
      b_v[index_v+Nx] += S_diff;
    }
  }

  /* Solid factor */
  
  if((curves > 0) && (consider_wall_shear_OK)){

#pragma omp parallel for default(none) private(z, index_v, index_, mu_eff) \
  shared(v_sol_factor_index_v, v_sol_factor_index, v_dom_matrix, mu_turb_at_v_nodes, \
	 v_sol_factor, v_p_factor, v_p_factor_S, p, aP_v, b_v)
    
    for(z=0; z<v_sol_factor_count; z++){
      
      index_v = v_sol_factor_index_v[z];
      
      if(v_dom_matrix[index_v] == 0){
	index_ = v_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_v_nodes[index_v];
	aP_v[index_v] += mu_eff * v_sol_factor[z];
	b_v[index_v] += v_p_factor[z] * (v_p_factor_S[z] * p[index_-Nx]
					 + (1 - v_p_factor_S[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(v_dom_matrix, b_v, vol_y)

    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	  index_v = K*Nx*ny + j*Nx + I;
	  if(v_dom_matrix[index_v] == 0){
	    b_v[index_v] += periodic_source * vol_y[index_v];
	  }
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, j, K, index_v, aP0)	\
  shared(v_dom_matrix, aP_v, b_v, vol_y, v0)

    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	  index_v = K*Nx*ny + j*Nx + I;
	  if(v_dom_matrix[index_v] == 0){
	    aP0 = rho * vol_y[index_v] / dt;
	    aP_v[index_v] += aP0;
	    b_v[index_v] += aP0 * v0[index_v];
	  }
	}
      }
    }
  }

  /* Add custom source term */
  if(add_momentum_source_OK){
    vel_at_crossed_faces(data);
    add_v_momentum_source(data);
  }

  /* Relax coefficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(v, v_dom_matrix, 0, data->coeffs_v, Nx, ny, Nz, data->alpha_v);
  }

  /* Set coefficients for slave cells */

#pragma omp parallel for default(none) private(i, index_v) \
  shared(v_slave_nodes, aP_v, aW_v, aE_v, aS_v, aN_v, aB_v, aT_v, b_v)
  
  for(i=0; i<v_slave_count; i++){
    index_v = v_slave_nodes[i];
    aP_v[index_v] = 1;
    aW_v[index_v] = 0;
    aE_v[index_v] = 0;
    aS_v[index_v] = 0;
    aN_v[index_v] = 0;
    aB_v[index_v] = 0;
    aT_v[index_v] = 0;
    b_v[index_v] = 0;
  }
  
  return;  
}

/* COEFFS_FLOW_W_UPWIND_3D */
/*****************************************************************************/
/**
  
  Sets:

  Structure coeffs_v: coefficients of w momentum equation for internal nodes.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 10-10-2019
*/
void coeffs_flow_w_upwind_3D(Data_Mem *data){

  int I, J, K, i, j, k, z, index_;
  int index_face_x, index_face_y, index_face_z, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int p_source_orientation = data->p_source_orientation;
  const int w_slave_count = data->w_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int w_sol_factor_count = data->w_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_w_face_z_count = data->diff_w_face_z_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *w_dom_matrix = data->w_dom_matrix;
  const int *w_slave_nodes = data->w_slave_nodes;
  const int *w_sol_factor_index_w = data->w_sol_factor_index_w;
  const int *w_sol_factor_index = data->w_sol_factor_index;
  const int *diff_w_face_z_index = data->diff_w_face_z_index;
  const int *diff_w_face_z_index_w = data->diff_w_face_z_index_w;
  const int *diff_w_face_z_K = data->diff_w_face_z_K;

  double dpA;
  double mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double aP0, S_diff;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;

  double *aP_w = data->coeffs_w.aP;
  double *aW_w = data->coeffs_w.aW;
  double *aE_w = data->coeffs_w.aE;
  double *aS_w = data->coeffs_w.aS;
  double *aN_w = data->coeffs_w.aN;
  double *aB_w = data->coeffs_w.aB;
  double *aT_w = data->coeffs_w.aT;
  double *b_w = data->coeffs_w.b;

  const double *w = data->w;
  const double *p = data->p;
  const double *w_faces_z = data->w_faces_z;
  const double *Dw_x = data->Dw_x;
  const double *Dw_y = data->Dw_y;
  const double *Dw_z = data->Dw_z;
  const double *Fw_x = data->Fw_x;
  const double *Fw_y = data->Fw_y;
  const double *Fw_z = data->Fw_z;
  const double *w_sol_factor = data->w_sol_factor;
  const double *w_p_factor = data->w_p_factor;
  const double *mu_turb_at_w_nodes = data->mu_turb_at_w_nodes;
  const double *vol_z = data->vol_z;
  const double *alpha_w_z = data->alpha_w_z;
  const double *w_p_factor_B = data->w_p_factor_B;
  const double *w0 = data->w0;
  const double *diff_factor_w_face_z = data->diff_factor_w_face_z;
  const double *w_cut_fb = data->w_cut_fb;
  const double *w_cut_ft = data->w_cut_ft;

  set_D_w(data);
  set_F_w(data);

  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_w = k*Nx*Ny + J*Nx + I;

	if(w_dom_matrix[index_w] == 0){

	  i = I - 1;
	  j = J - 1;
	  K = k + 1;

	  index_face_x = k*nx*Ny + J*nx + i;
	  index_face_y = k*Nx*ny + j*Nx + I;
	  index_face_z = (K-1)*Nx*Ny + J*Nx + I;

	  /* Diffusive */
	  De = Dw_x[index_face_x+1];
	  Dw = Dw_x[index_face_x];
	  Dn = Dw_y[index_face_y+Nx];
	  Ds = Dw_y[index_face_y];
	  Dt = Dw_z[index_face_z+Nx*Ny];
	  Db = Dw_z[index_face_z];

	  /* Convective */
	  Fe = Fw_x[index_face_x+1];
	  Fw = Fw_x[index_face_x];
	  Fn = Fw_y[index_face_y+Nx];
	  Fs = Fw_y[index_face_y];
	  Ft = Fw_z[index_face_z+Nx*Ny] * alpha_w_z[index_face_z+Nx*Ny];
	  Fb = Fw_z[index_face_z] * alpha_w_z[index_face_z];

	  aE_w[index_w] = De + MAX(-Fe, 0);
	  aW_w[index_w] = Dw + MAX(Fw, 0);
	  aN_w[index_w] = Dn + MAX(-Fn, 0);
	  aS_w[index_w] = Ds + MAX(Fs, 0);
	  aT_w[index_w] = Dt + MAX(-Ft, 0);
	  aB_w[index_w] = Db + MAX(Fb, 0);

	  aP_w[index_w] = aE_w[index_w] + aW_w[index_w] + aN_w[index_w] + aS_w[index_w] + aT_w[index_w] + aB_w[index_w];
	    //	    De + Dw + Dn + Ds + Dt + Db + MAX(Fe, 0) + MAX(-Fw, 0) + MAX(Fn, 0) + MAX(-Fs, 0) + MAX(Ft, 0) + MAX(-Fb, 0);
	  
	  dpA = w_faces_z[index_face_z+Nx*Ny] * p[index_face_z+Nx*Ny] -
	    w_faces_z[index_face_z] * p[index_face_z];

	  b_w[index_w] = -dpA;
	  
	}
	else{
	  aP_w[index_w] = 1;
	  aE_w[index_w] = 0;
	  aW_w[index_w] = 0;
	  aN_w[index_w] = 0;
	  aS_w[index_w] = 0;
	  aT_w[index_w] = 0;
	  aB_w[index_w] = 0;
	  b_w[index_w] = 0;
	}
      }
    }
  }

  /* Diffusive factor */
  
  for(z=0; z<diff_w_face_z_count; z++){

    index_ = diff_w_face_z_index[z];    
    index_w = diff_w_face_z_index_w[z];
    K = diff_w_face_z_K[z];
    mu_eff = mu + 0.5 * (mu_turb_at_w_nodes[index_w] + mu_turb_at_w_nodes[index_w+Nx*Ny]);
    S_diff = mu_eff * diff_factor_w_face_z[z] * (w[index_w] * w_cut_fb[index_] + w[index_w+Nx*Ny] * w_cut_ft[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((w_dom_matrix[index_w] == 0) && (K > 1)){
      b_w[index_w] -= S_diff;
    }
    if((w_dom_matrix[index_w+Nx*Ny] == 0) && (K < Nz-2)){
      b_w[index_w+Nx*Ny] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){

#pragma omp parallel for default(none) private(z, index_w, index_, mu_eff) \
  shared(w_sol_factor_index_w, w_sol_factor_index, w_dom_matrix, mu_turb_at_w_nodes, \
	 w_sol_factor, w_p_factor, w_p_factor_B, p, aP_w, b_w)
    
    for(z=0; z<w_sol_factor_count; z++){
      
      index_w = w_sol_factor_index_w[z];
      
      if(w_dom_matrix[index_w] == 0){
  	index_ = w_sol_factor_index[z];
  	mu_eff = mu + mu_turb_at_w_nodes[index_w];
  	aP_w[index_w] += mu_eff * w_sol_factor[z];
  	b_w[index_w] += w_p_factor[z] * (w_p_factor_B[z] * p[index_-Nx*Ny]
  					 + (1 - w_p_factor_B[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 3){

# pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(w_dom_matrix, b_w, vol_z)

    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_w = k*Nx*Ny + J*Nx + I;
	  if(w_dom_matrix[index_w] == 0){
	    b_w[index_w] += periodic_source * vol_z[index_w];
	  }
	}
      }
    }
  }

/* Transient contribution */

if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, J, k, index_w, aP0)	\
  shared(w_dom_matrix, aP_w, b_w, vol_z, w0)
  
  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_w = k*Nx*Ny + J*Nx + I;
	if(w_dom_matrix[index_w] == 0){
	  aP0 = rho * vol_z[index_w] / dt;
	  aP_w[index_w] += aP0;
	  b_w[index_w] += aP0 * w0[index_w];
	}
      }
    }
  }
 }

  /* Add custom source term */
  if(add_momentum_source_OK){
    vel_at_crossed_faces(data);
    add_w_momentum_source(data);
  }

  /* Relax coefficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(w, w_dom_matrix, 0, data->coeffs_w, Nx, Ny, nz, data->alpha_w);
  }

  /* Set coefficients for slave cells */

#pragma omp parallel for default(none) private(i, index_w)		\
  shared(w_slave_nodes, aP_w, aW_w, aE_w, aS_w, aN_w, aB_w, aT_w, b_w)
  
  for(i=0; i<w_slave_count; i++){
    index_w = w_slave_nodes[i];
    aP_w[index_w] = 1;
    aW_w[index_w] = 0;
    aE_w[index_w] = 0;
    aS_w[index_w] = 0;
    aN_w[index_w] = 0;
    aB_w[index_w] = 0;
    aT_w[index_w] = 0;
    b_w[index_w] = 0;    
  }
  
  return;
}
