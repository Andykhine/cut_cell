#include "include/some_defs.h"

extern sig_atomic_t action_on_signal_OK;
extern int timer_flag_OK;

/* SET_PERIODIC_FLOW_SOURCE */
/*****************************************************************************/
/**
  
  Sets:

  (int) Data_Mem::p_source_orientation: set to 0 for no periodic source, 1 for w->e periodic
  flow, 2 for s->n periodic flow, 3 for b->t periodic flow.

  (double) Data_Mem::periodic_source: value of periodic source term.

  @param data

  @return
  - 0 : No error found.

  - 1 : Periodic condition set in more than one direction.

  - 2 : Inconsistent specification.
  
  MODIFIED: 03-10-2019
*/
int set_periodic_flow_source(Data_Mem *data){

  int w_periodic_OK, e_periodic_OK;
  int s_periodic_OK, n_periodic_OK;
  int b_periodic_OK, t_periodic_OK;
  int periodic_WE = 0;
  int periodic_SN = 0;
  int periodic_BT = 0;
  int ret_value = 0;

  const int solve_3D_OK = data->solve_3D_OK;
  
  int *p_source_orientation = &(data->p_source_orientation);
  
  double *periodic_source = &(data->periodic_source);

  const double Lx = data->Lx;
  const double Ly = data->Ly;
  const double Lz = data->Lz;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);
  const b_cond_flow *bc_flow_bottom = &(data->bc_flow_bottom);
  const b_cond_flow *bc_flow_top = &(data->bc_flow_top);

  w_periodic_OK = (bc_flow_west->type == 5);
  e_periodic_OK = (bc_flow_east->type == 5);
  s_periodic_OK = (bc_flow_south->type == 5);
  n_periodic_OK = (bc_flow_north->type == 5);
  b_periodic_OK = (bc_flow_bottom->type == 5);
  t_periodic_OK = (bc_flow_top->type == 5);

  *p_source_orientation = 0;
  *periodic_source = 0;

  periodic_WE = (w_periodic_OK || e_periodic_OK);
  periodic_SN = (s_periodic_OK || n_periodic_OK);
  
  if(solve_3D_OK){
    periodic_BT = (b_periodic_OK || t_periodic_OK);
  }

  if(periodic_WE + periodic_SN + periodic_BT > 1){
    ret_value = 1;
    return ret_value;
  }
  else{
    if(periodic_WE){
      if((!w_periodic_OK) || (!e_periodic_OK)){
	ret_value = 2;
	return ret_value;
      }
      else{
	*periodic_source = (bc_flow_west->p_value - bc_flow_east->p_value) / Lx;
	*p_source_orientation = 1;
      }
    }

    if(periodic_SN){
      if((!s_periodic_OK) || (!n_periodic_OK)){
	ret_value = 2;
	return ret_value;
      }
      else{
	*periodic_source = (bc_flow_south->p_value - bc_flow_north->p_value) / Ly;
	*p_source_orientation = 2;
      }
    }

    /* solve_3D_OK = 0 already considered at periodic_BT definition */
    if(periodic_BT){
      if((!b_periodic_OK) || (!t_periodic_OK)){
	ret_value = 2;
	return ret_value;
      }
      else{
	*periodic_source = (bc_flow_bottom->p_value - bc_flow_top->p_value) / Lz;
	*p_source_orientation = 3;
      }
    }
  }

  return ret_value;
}

/* SOLVE_FLOW_PRE_CALC_JOBS */
/*****************************************************************************/
/**
  @param data

  @return void

  @see change_bnd_turb_to_default, compute_parabolic_inlet, init_p_at_boundary, set_p_ref_node

  @see set_periodic_flow_source, store_bnd_p_values
  
  MODIFIED: 08-05-2020
*/
void solve_flow_pre_calc_jobs(Data_Mem *data){

  MATIOS *matios = &(data->matios);

  int ret_value;
  const int turb_model = data->turb_model;
  const int solve_3D_OK = data->solve_3D_OK;
  const int solve_flow_OK = data->solve_flow_OK;
  
  Sigmas *sigmas = &(data->sigmas);

  Wins *wins = &(data->wins);
  
  size_t dim[2], dims[4];

  const int fss_OK = data->flow_steady_state_OK;
  const int wftd_OK = data->write_flow_transient_data_OK;

  if(wftd_OK && !fss_OK){
    dim[0] = 1;
    dim[1] = 1;
    matios->tflow = Mat_VarCreate("tflow", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->dt[0]),
				  MAT_F_DONT_COPY_DATA);

    /* We are about to compute cut_cell velocities
       so the values of velocity will be located at centroid 
       of cut faces */
    
    /*
      data->centered_vel_comp_OK = 0;
      
      
      dims[0] = data->nx;
      dims[1] = data->Ny;
      dims[2] = data->Nz;
      dims[3] = 1;
      
      matios->u = Mat_VarCreate("u_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims, data->u,
      MAT_F_DONT_COPY_DATA);
      
      dims[0] = data->Nx;
      dims[1] = data->ny;
      dims[2] = data->Nz;
      dims[3] = 1;
      
      matios->v = Mat_VarCreate("v_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims, data->v,
      MAT_F_DONT_COPY_DATA);
      
      dims[0] = data->Nx;
      dims[1] = data->Ny;
      dims[2] = data->nz;
      dims[3] = 1;
      
      matios->w = Mat_VarCreate("w_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims, data->w,
      MAT_F_DONT_COPY_DATA);
    */
    
    dims[0] = data->Nx;
    dims[1] = data->Ny;
    dims[2] = data->Nz;
    dims[3] = 1;

    matios->U = Mat_VarCreate("U_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims, data->U,
			      MAT_F_DONT_COPY_DATA);
    matios->V = Mat_VarCreate("V_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims, data->V,
			      MAT_F_DONT_COPY_DATA);
    matios->W = Mat_VarCreate("W_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims, data->W,
			      MAT_F_DONT_COPY_DATA);
  
    matios->p = Mat_VarCreate("p_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims, data->p,
			      MAT_F_DONT_COPY_DATA);
  
    if(data->turb_model){
      matios->k = Mat_VarCreate("k_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims,
				data->k_turb,
				MAT_F_DONT_COPY_DATA);
      matios->e = Mat_VarCreate("e_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims,
				data->eps_turb,
				MAT_F_DONT_COPY_DATA);
    }

  }
  
  /* Initialize sigmas structure */
  sigmas->values[0] = NAN;  
  sigmas->values[1] = NAN;
  sigmas->values[2] = NAN;
  sigmas->values[3] = NAN;
  sigmas->values[4] = NAN;
  sigmas->values[5] = NAN;
  sigmas->values[6] = NAN;
  sigmas->values[7] = NAN;

  /** Sets default values for inlet turbulent variables if necessary. */
  /* Sets: 
     - bc_flow_west->k_value
     - bc_flow_west->eps_value
     - bc_flow_east->k_value
     - bc_flow_east->eps_value
     - bc_flow_south->k_value
     - bc_flow_south->eps_value
     - bc_flow_north->k_value
     - bc_flow_north->eps_value

  */
  if(turb_model){
    change_bnd_turb_to_default(data);
  }

  /** Compute values of velocity to apply at the boundary in case
      a boundary has the option b_cond_flow::parabolize_profile_OK set as true. */
  /* Sets:
     - u_E_non_uni
     - k_E_non_uni
     - eps_non_uni
     - u_W_non_uni
     - k_W_non_uni
     - eps_W_non_uni
     - v_N_non_uni
     - k_N_non_uni
     - eps_N_non_uni
     - v_S_non_uni
     - k_S_non_uni
     - eps_S_non_uni
  */
  compute_parabolic_inlet(data);

  /** Initialize pressure nodes to the correct value 
      at boundaries. */
  if(solve_3D_OK){
    init_p_at_boundary_3D(data);    
  }
  else{
    init_p_at_boundary(data);
  }

  /** Looks for a suitable pressure node along the boundary 
      to specify a pressure value. */
  /* Sets:
     - set_ref_p_node
     - ref_p_node_index
     - ref_p_value
  */
  if(solve_3D_OK){
    if(set_p_ref_node_3D(data)){
      alert_msg(wins, "More than one reference pressure node set");
    }
  }
  else{
    if(set_p_ref_node(data)){
      alert_msg(wins, "More than one reference pressure node set");
    }
  }

  /** Check if a periodic boundary condition was used and initializes 
      the corresponding source term accordingly. */
  /* Sets:
     - periodic_source
     - p_source_orientation
  */
  ret_value = set_periodic_flow_source(data);
  if(ret_value == 1){
    alert_msg(wins, "Periodic condition set in more than one orientation");
    exit(EXIT_FAILURE);
  }
  else if(ret_value == 2){
    alert_msg(wins,
	      "Inconsistent specification of periodic flow (just one boundary set as periodic)");
    exit(EXIT_FAILURE);
  }
  /** Store values to use with periodic boundary condition. */
  /* Sets:
     - p_W_non_uni
     - p_E_non_uni
     - p_S_non_uni
     - p_N_non_uni
     - p_B_non_uni
     - p_T_non_uni
  */
  /* solve_flow_OK = 1, computes flow field from tmp.mat
     _non_uni arrays are initialized on read_flow_field_from_tmp_file function. */
  if(solve_flow_OK != 1){
    
    if(solve_3D_OK){
      store_bnd_p_values_3D(data);
    }
    else{
      store_bnd_p_values(data);
    }
  } 

  /** Write header of sigmas.dat file */

  write_sfile(data);

  mem_count(wins);

  return;

}

/* SOLVE_FLOW_POST_CALC_JOBS */
void solve_flow_post_calc_jobs(Data_Mem *data){

  const int write_cut_cell_data_on_file_OK = data->write_cut_cell_data_on_file_OK;
  const int post_process_OK = data->post_process_OK;
  const int add_p_source_when_finish_OK = data->add_p_source_when_finish_OK;
  const int solve_3D_OK = data->solve_3D_OK;
  const int curves = data->curves;

  Sigmas *sigmas = &(data->sigmas);
  Wins *wins = &(data->wins);

  /* Close sigmas file */
  close_sfile(data);

  /* Post processing use u and v values before interpolation to face centers */
  if(post_process_OK){
    if(curves == 1){
      if(solve_3D_OK){
	compute_force_on_solid_3D(data);
      }
      else{
	compute_force_on_solid(data);
      }
    }
    else{
      alert_msg(wins, "Post processing: curves must be equal to 1");
    }
  }

  /* Center velocity components and compute at grid nodes */
  center_vel_components(data);
  compute_vel_at_main_cells(data);
  
  /* Write u and v fields */
  if(write_cut_cell_data_on_file_OK){
    write_cut_cell_data(data);
  }

  /* Add average pressure drop */
  if(add_p_source_when_finish_OK){
    if(solve_3D_OK){
      add_periodic_source_to_p_3D(data);
    }
    else{
      add_periodic_source_to_p(data);
    }
  }

  /* Copy sigmas values */
  sigmas->flow_values[0] = sigmas->values[0]; // u
  sigmas->flow_values[1] = sigmas->values[1]; // v
  sigmas->flow_values[2] = sigmas->values[2]; // w
  sigmas->flow_values[3] = sigmas->values[3]; // c
  sigmas->flow_values[4] = sigmas->values[4]; // k
  sigmas->flow_values[5] = sigmas->values[5]; // e
  sigmas->flow_values[6] = sigmas->values[6]; // MB

  sigmas->flow_glob_norm[0] = sigmas->glob_norm[0]; // u
  sigmas->flow_glob_norm[1] = sigmas->glob_norm[1]; // v
  sigmas->flow_glob_norm[2] = sigmas->glob_norm[2]; // w
  sigmas->flow_glob_norm[3] = sigmas->glob_norm[3]; // c
  sigmas->flow_glob_norm[4] = sigmas->glob_norm[4]; // k
  sigmas->flow_glob_norm[5] = sigmas->glob_norm[5]; // e
  sigmas->flow_glob_norm[6] = sigmas->glob_norm[6]; // MB
  
  return;
}

/* SET_MU */
/*****************************************************************************/
/**
  
  Sets:

  double Data_mem::mu: applies linear progression of mu values if relevant.

  @param data, outer_iter

  @return void
  
  MODIFIED: 30-05-2019
*/
void set_mu(Data_Mem *data, const int outer_iter){

  const int mu_initial_iter_flow = data->mu_initial_iter_flow;
  const int mu_final_iter_flow = data->mu_final_iter_flow;

  const double mu_final = data->mu_final;
  const double mu_initial = data->mu_initial;


  if(mu_final_iter_flow > 0){
    
    if((outer_iter >= mu_initial_iter_flow) &&
       (outer_iter <= mu_final_iter_flow)){
      data->mu = ((mu_final - mu_initial) * (outer_iter - mu_initial_iter_flow))
	/ (mu_final_iter_flow - mu_initial_iter_flow) + mu_initial;
    }
    else if (outer_iter <= mu_initial_iter_flow){
      data->mu = mu_initial;
    }
    else{
      data->mu = mu_final;
    }
  }

  return;
}


/* STORE_FLOW_FIELDS */
/*****************************************************************************/
/**
  
   Store values of flow field on transient calculations.

   Sets:

   Array (double, nxNyNz) Data_Mem::u0

   Array (double, NxnyNz) Data_Mem::v0

   Array (double, NxNynz) Data_Mem::w0

   Array (double, NxNyNz) Data_Mem::k_turb0

   Array (double, NxNyNz) Data_Mem::eps_turb0

   @param data

   @return ret_value not implemented.
  
   MODIFIED: 07-07-2019
*/
void store_flow_fields(Data_Mem *data){

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int turb_model = data->turb_model;
  const int solve_3D_OK = data->solve_3D_OK;
 
  copy_array(data->u, data->u0, nx, Ny, Nz);
  copy_array(data->v, data->v0, Nx, ny, Nz);

  if(solve_3D_OK){
    copy_array(data->w, data->w0, Nx, Ny, nz);
  }

  if(turb_model){
    copy_array(data->k_turb, data->k_turb0, Nx, Ny, Nz);
    copy_array(data->eps_turb, data->eps_turb0, Nx, Ny, Nz);
  }

  return;
}

/* ADD_U_MOMENTUM_SOURCE_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNyNx) coeffs_u.b: add custom source term.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 03-10-2019
*/
void add_u_momentum_source_3D(Data_Mem *data){

  int K, J, i, index_u;

  const int Ny = data->Ny;
  const int nx = data->nx;
  const int Nz = data->Nz;

  const int *u_dom_matrix = data->u_dom_matrix;

  double r_sqr, x_rel, y_rel, z_rel;

  const double force_mag = 122000;
  const double Lx = data->Lx;
  const double Ly = data->Ly;
  const double Lz = data->Lz;

  double *b_u = data->coeffs_u.b;
  
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;
  const double *vol_x = data->vol_x;

  /* u momentum equation */
#pragma omp parallel for default(none) private(i, J, K, index_u, r_sqr, x_rel, y_rel, z_rel) \
  shared(u_dom_matrix, nodes_x_u, nodes_y_u, nodes_z_u, b_u, vol_x)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){

	index_u = K*nx*Ny + J*nx + i;
      
	if(u_dom_matrix[index_u] == 0){
	  x_rel = 0.5 * Lx - nodes_x_u[index_u];
	  y_rel = 0.5 * Ly - nodes_y_u[index_u];
	  z_rel = 0.5 * Lz - nodes_z_u[index_u];
	  r_sqr = x_rel * x_rel + y_rel * y_rel;
	  // 	v_theta = (-u[index_u] * y_rel + v_at_faces_x[index_u] * x_rel) / r_val;
	  b_u[index_u] += (-force_mag * y_rel) * vol_x[index_u] / r_sqr;
	  //     b_u[index_u] += (-force_mag * y_rel  - rho * v_theta * v_theta * x_rel) * vol_x[index_u] / r_sqr;
	}
      }
    }
  }

  return;
}

/* OPEN_BND_INDX_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, NyNz) w_J: J indexes on west boundary that correspond to fluid. 

  Array (int, NyNz) w_K: K indexes on west boundary that correspond to fluid.

  For w_J and w_K only the first w_count values are meaningful.

  Array (int, NyNz) e_J: J indexes on east boundary that correspond to fluid.  

  Array (int, NyNz) e_K: K indexes on east boundary that correspond to fluid.

  For e_J and e_K only the first e_count values are meaningful.

  Array (int, NxNz) s_I: I indexes on south boundary that correspond to fluid.

  Array (int, NxNz) s_K: K indexes on south boundary that correspond to fluid.

  For s_I and s_K only the first s_count values are meaningful.

  Array (int, NxNz) n_I: I indexes on north boundary that correspond to fluid.

  Array (int, NxNz) n_K: K indexes on north boundary that correspond to fluid.

  For n_I and n_K only the first n_count values are meaningful.

  Array (int, NxNy) b_I: I indexes on bottom boundary that correspond to fluid.

  Array (int, NxNy) b_J: J indexes on bottom boundary that correspond to fluid.

  For b_I and b_J only the first b_count values are meaningful.

  Array (int, NxNy) t_I: I indexes on top boundary that correspond to fluid.

  Array (int, NxNy) t_J: J indexes on top boundary that correspond to fluid.

  For t_I and t_J only the first t_count values are meaningful.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 08-08-2019
*/
void open_bnd_indx_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_u, index_v, index_w;

  int w_count = 0;
  int e_count = 0;
  int s_count = 0;
  int n_count = 0;
  int b_count = 0;
  int t_count = 0;

  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  int *w_J = data->w_J;
  int *w_K = data->w_K;
  int *e_J = data->e_J;
  int *e_K = data->e_K;
  int *s_I = data->s_I;
  int *s_K = data->s_K;
  int *n_I = data->n_I;
  int *n_K = data->n_K;
  int *b_I = data->b_I;
  int *b_J = data->b_J;
  int *t_I = data->t_I;
  int *t_J = data->t_J;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);
  const b_cond_flow *bc_flow_bottom = &(data->bc_flow_bottom);
  const b_cond_flow *bc_flow_top = &(data->bc_flow_top);
  
#pragma omp parallel sections default(none) private(I, J, K, i, j, k, index_u, index_v, index_w) \
  shared(bc_flow_west, bc_flow_east, bc_flow_north, bc_flow_south, bc_flow_top, bc_flow_bottom, \
	 u_dom_matrix, v_dom_matrix, w_dom_matrix, n_count, s_count, e_count, w_count, \
	 t_count, b_count, n_I, n_K, s_I, s_K, e_J, e_K, w_J, w_K, t_I, t_J, b_I, b_J)
  {

#pragma omp section 
    {
      /* West boundary */
      if(bc_flow_west->type == 1){
	if(bc_flow_west->parabolize_profile_OK == 1){
	  /* Finding open fluid zones */
	  i = 0;
	  for(K=0; K<Nz; K++){
	    for(J=0; J<Ny; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      if(u_dom_matrix[index_u] == 0){
		/* Stores all Js and Ks from which there is fluid */
		w_J[w_count] = J;
		w_K[w_count] = K;
		w_count++;
	      }
	    }
	  }
	}
      }
    } /* West section */

#pragma omp section
    {
      /* East boundary */
      if(bc_flow_east->type == 1){
	if(bc_flow_east->parabolize_profile_OK == 1){
	  i = nx - 1;
	  for(K=0; K<Nz; K++)
	    for(J=0; J<Ny; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      if(u_dom_matrix[index_u] == 0){
		e_J[e_count] = J;
		e_K[e_count] = K;
		e_count++;
	      }
	    }
	}
      }
    } /* East section */

#pragma omp section 
    {
      /* South boundary */
      if(bc_flow_south->type == 1){
	if(bc_flow_south->parabolize_profile_OK == 1){
	  j = 0;
	  for(K=0; K<Nz; K++){
	    for(I=0; I<Nx; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      if(v_dom_matrix[index_v] == 0){
		s_I[s_count] = I;
		s_K[s_count] = K;
		s_count++;
	      }
	    }
	  }
	}
      }
    } /* South section */

#pragma omp section 
    { 
      /* North boundary */
      if(bc_flow_north->type == 1){
	if(bc_flow_north->parabolize_profile_OK == 1){
	  j = ny - 1;
	  for(K=0; K<Nz; K++){
	    for(I=0; I<Nx; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      if(v_dom_matrix[index_v] == 0){
		n_I[n_count] = I;
		n_K[n_count] = K;
		n_count++;
	      }
	    }
	  }
	}
      }
    } /* North section */

#pragma omp section
    {
      /* Bottom boundary */
      if(bc_flow_bottom->type == 1){
	if(bc_flow_bottom->parabolize_profile_OK == 1){
	  k = 0;
	  for(J=0; J<Ny; J++){
	    for(I=0; I<Nx; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      if(w_dom_matrix[index_w] == 0){
		b_I[b_count] = I;
		b_J[b_count] = J;
		b_count++;
	      }
	    }
	  }
	}
      }
    }

#pragma omp section
    {
      /* Top boundary */
      if(bc_flow_top->type == 1){
	if(bc_flow_top->parabolize_profile_OK == 1){
	  k = nz-1;
	  for(J=0; J<Ny; J++){
	    for(I=0; I<Nx; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      if(w_dom_matrix[index_w] == 0){
		t_I[t_count] = I;
		t_J[t_count] = J;
		t_count++;
	      }
	    }
	  }
	}
      }
    }
    
  } /* sections */

  data->w_count = w_count;
  data->e_count = e_count;
  data->s_count = s_count;
  data->n_count = n_count;
  data->b_count = b_count;
  data->t_count = t_count;

  return;
}

/* SOLVE_FLOW_SIMPLES_3D */
/*****************************************************************************/
/**
   
   Solve flow field for 3D case.

   @param data

   @return void
 */
void solve_flow_SIMPLES_3D(Data_Mem *data){
  
  char msg[SIZE];
  
  const char *flow_solver_name = data->flow_solver_name;
  
  /* Auxiliary iteration counter for ADI loop */
  int iter;
  /* Main loop counter */
  int outer_iter = 0;
  int convergence_OK = 0;
  int convergence_turb_OK = 0;  

  /* Number of uvw iterations on each main iteration */
  int iter_uvwp;
  int convergence_uvwp_OK;
  /* Number of ke iterations on each main iteration */
  int iter_ke;
  int convergence_ke_OK;
  /* Use pseudo-velocities, SIMPLER */
  int use_ps_velocities_OK;
  
  /* These can be adjusted at interruption signal */
  int it_for_update_coeffs = data->it_for_update_coeffs;
  int max_iter_flow = data->max_iter_flow;
  int max_iter_ADI = data->max_iter_ADI;
  int flow_scheme = data->flow_scheme;
  
  const int it_for_update_k_coeffs = data->it_for_update_k_coeffs;
  const int it_for_update_eps_coeffs = data->it_for_update_eps_coeffs;

  const int coupled_physics_OK = data->coupled_physics_OK;
  /* Used for correct ncurses display information */
  const int rstride = coupled_physics_OK ? 5 : 2;

  /* Iterations after which a reference residual is calculated
     for uvwp or ke sub-loop */
  const int uvwp_ref_it = 2;
  const int ke_ref_it = 2;
  
  const int max_iter_uvwp = data->max_iter_uvp;
  const int max_iter_ke = data->max_iter_ke;

  /* Iterations for reference residual calculation, main loop */
  const int outer_ref_it = 5;
  /* Iterations for reference residual calculation, ADI loop */
  const int ref_it = 5;  
  const int it_to_start_turb = data->it_to_start_turb;
  const int turb_model = data->turb_model;
  const int it_to_update_y_plus = data->it_to_update_y_plus;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  
  const int animation_flow_OK = data->animation_flow_OK;
  const int animation_flow_update_it = 
    data->animation_flow_update_it;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int res_sigma_iter = data->res_sigma_iter;

  u_int64_t time_i, time_f, timer;

  double res_u = 1, res_v = 1, res_w = 1, res_p = 1;
  double res_pcorr = 1, res_c = 1, res_k = 1, res_eps = 1;
  /* Reference residuals at ADI loop level */
  double ref_res_u = 1, ref_res_v = 1, ref_res_w = 1, ref_res_p = 1;
  double ref_res_pcorr = 1, ref_res_k = 1, ref_res_eps = 1;
  /* Reference residuals at main loop level */
  double outer_ref_res_u = 1;
  double outer_ref_res_v = 1;
  double outer_ref_res_w = 1;
  double outer_ref_res_c = 1;
  double outer_ref_res_k = 1;
  double outer_ref_res_eps = 1;

  /* Reference residuals at sub-loop level */
  double uvwp_ref_res_u = 1, uvwp_ref_res_v = 1, uvwp_ref_res_w = 1, uvwp_ref_res_c = 1;
  double ke_ref_res_k = 1, ke_ref_res_eps = 1;

  /* Sigmas at sub-loop level */
  double sub_sigmas_v[6];

  /* These can be adjusted at interruption signal */
  double tol_flow = data->tol_flow;
  double tol_ADI = data->tol_ADI;

  double *sigmas_v = data->sigmas.values;
  double *glob_norm = data->sigmas.glob_norm;

  void (*ADI_solver)(double *, Coeffs *, 
		     const int, const int, const int, 
		     ADI_Vars *) = data->ADI_solver;
  
  void (*coeffs_flow_u)(Data_Mem *) = NULL;
  void (*coeffs_flow_v)(Data_Mem *) = NULL;
  void (*coeffs_flow_w)(Data_Mem *) = NULL;

  Sigmas *sigmas = &(data->sigmas);
  Wins *wins = &(data->wins);

  sigmas->time_f = &(time_f);

  /* UPWIND scheme */
  if(flow_scheme == 0){
    coeffs_flow_u = coeffs_flow_u_upwind_3D;
    coeffs_flow_v = coeffs_flow_v_upwind_3D;
    coeffs_flow_w = coeffs_flow_w_upwind_3D;
  } 
  else if(flow_scheme == 1){
    /* QUICK scheme */    
    coeffs_flow_u = coeffs_flow_u_quick_3D;
    coeffs_flow_v = coeffs_flow_v_quick_3D;
    coeffs_flow_w = coeffs_flow_w_quick_3D;
  }
  /* van Leer scheme */
  else if(flow_scheme == 2){
    coeffs_flow_u = coeffs_flow_u_TVD_3D;
    coeffs_flow_v = coeffs_flow_v_TVD_3D;
    coeffs_flow_w = coeffs_flow_w_TVD_3D;
  }

  /* SIMPLER */
  if(pv_coupling_algorithm == 2){
    (*coeffs_flow_u)(data);
    (*coeffs_flow_v)(data);
    (*coeffs_flow_w)(data);
  }

  action_on_signal_OK = 0;  

  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  solve_flow_pre_calc_jobs(data);

  /* This is the main (outer) loop */
  while(outer_iter < max_iter_flow && !convergence_OK){
    
    set_mu(data, outer_iter);
    
    /* Set iteration counter and convergence flag for uvwp sub-loop */
    iter_uvwp = 0;
    convergence_uvwp_OK = 0;

    while(iter_uvwp < max_iter_uvwp && !convergence_uvwp_OK){
      
      set_bc_flow_3D(data);
      
      if(pv_coupling_algorithm == 2){
	use_ps_velocities_OK = 1;
	/* SIMPLER: COMPUTE PSEUDO-VELOCITIES */
	compute_pseudo_velocities_3D(data);
	
	/* SIMPLER: SOLVE PRESSURE EQUATION */
	coeffs_flow_p_3D(data, use_ps_velocities_OK);
	
	for(iter = 0; iter < ref_it; iter++){
	  (*ADI_solver)(data->p,
			&(data->coeffs_p),
			Nx, Ny, Nz,
			&(data->ADI_vars));
	}
	ref_res_p = compute_residues_3D(data->p, &(data->coeffs_p), Nx, Ny, Nz);
	
	res_p = 1;
	iter = 1;
	while((iter < max_iter_ADI) &&
	      ((res_p / ref_res_p) > tol_ADI)){
	  (*ADI_solver)(data->p,
			&(data->coeffs_p),
			Nx, Ny, Nz,
			&(data->ADI_vars));
	  res_p = compute_residues_3D(data->p, &(data->coeffs_p), Nx, Ny, Nz);
	  iter++;
	}
      }
      
      /* 1: SOLVE MOMENTUM EQUATIONS */
      
      /* u loop */
      (*coeffs_flow_u)(data);
      
      for(iter = 0; iter < ref_it; iter++){
	(*ADI_solver)(data->u,
		      &(data->coeffs_u), 
		      nx, Ny, Nz, 
		      &(data->ADI_vars));
      }
      ref_res_u = compute_residues_3D(data->u, &(data->coeffs_u), 
				   nx, Ny, Nz);
      res_u = 1;
      iter = 1;
      while((iter < max_iter_ADI) && 
	    ((res_u / ref_res_u) > tol_ADI)){
	
	if(iter % it_for_update_coeffs == 0){
	  (*coeffs_flow_u)(data);
	}
	
	(*ADI_solver)(data->u, 
		      &(data->coeffs_u), 
		      nx, Ny, Nz, 
		      &(data->ADI_vars));
	res_u = compute_residues_3D(data->u, &(data->coeffs_u), nx, Ny, Nz);
	iter++;
      }
      
      /* v loop */
      (*coeffs_flow_v)(data);
      
      for(iter = 0; iter < ref_it; iter++){
	(*ADI_solver)(data->v, 
		      &(data->coeffs_v), 
		      Nx, ny, Nz, 
		      &(data->ADI_vars));
      }
      ref_res_v = compute_residues_3D(data->v, &(data->coeffs_v), 
				   Nx, ny, Nz);
      res_v = 1;
      iter = 1;
      while((iter < max_iter_ADI) && 
	    ((res_v / ref_res_v) > tol_ADI)){
	
	if(iter % it_for_update_coeffs == 0){
	  (*coeffs_flow_v)(data);
	}
	
	(*ADI_solver)(data->v, 
		      &(data->coeffs_v), 
		      Nx, ny, Nz, 
		      &(data->ADI_vars));
	res_v = compute_residues_3D(data->v, &(data->coeffs_v), Nx, ny, Nz);
	iter++;
      }

      /* w loop */
      (*coeffs_flow_w)(data);
      
      for(iter = 0; iter < ref_it; iter++){
	(*ADI_solver)(data->w,
		      &(data->coeffs_w),
		      Nx, Ny, nz,
		      &(data->ADI_vars));
      }
      ref_res_w = compute_residues_3D(data->w, &(data->coeffs_w),
				   Nx, Ny, nz);      
      res_w = 1;
      iter = 1;
      while((iter < max_iter_ADI) &&
	    ((res_w / ref_res_w) > tol_ADI)){
	
	if(iter % it_for_update_coeffs == 0){
	  (*coeffs_flow_w)(data);
	}

	(*ADI_solver)(data->w,
		      &(data->coeffs_w),
		      Nx, Ny, nz,
		      &(data->ADI_vars));
	res_w = compute_residues_3D(data->w, &(data->coeffs_w), Nx, Ny, nz);
	iter++;
      }
      
      /* 2: SOLVE PRESSURE CORRECTION EQUATION */
      use_ps_velocities_OK = 0;

      /* To set pressure correction values to zero was found to improve convergence */
      if(pv_coupling_algorithm != 2){
	fill_array(data->p_corr, Nx, Ny, Nz, 0);
      }

      coeffs_flow_p_3D(data, use_ps_velocities_OK);
      
      for(iter = 0; iter < ref_it; iter++){
	(*ADI_solver)(data->p_corr, 
		      &(data->coeffs_p), 
		      Nx, Ny, Nz, 
		      &(data->ADI_vars));
      }
      ref_res_pcorr = compute_residues_3D(data->p_corr, &(data->coeffs_p),
					  Nx, Ny, Nz);
      res_pcorr = 1;
      iter = 1;
      while((iter < max_iter_ADI) && 
	    ((res_pcorr / ref_res_pcorr) > tol_ADI)){
	(*ADI_solver)(data->p_corr, 
		      &(data->coeffs_p), 
		      Nx, Ny, Nz, 
		      &(data->ADI_vars));
	res_pcorr = compute_residues_3D(data->p_corr, &(data->coeffs_p),
					Nx, Ny, Nz);
	iter++;
      }
      
      /* 3: CORRECT PRESSURE AND VELOCITIES */
      update_field_3D(data);

      /* Store values to use with periodic boundary condition, 
	 because p_corr values are set to zero at every iteration */
      store_bnd_p_values_3D(data);

      res_c = continuity_residual_3D(data);
      
      if(iter_uvwp == uvwp_ref_it){
	uvwp_ref_res_u = res_u;
	uvwp_ref_res_v = res_v;
	uvwp_ref_res_w = res_w;
	uvwp_ref_res_c = res_c;
      }

      /* Store sigmas for sub loop */
      sub_sigmas_v[0] = res_u / uvwp_ref_res_u;
      sub_sigmas_v[1] = res_v / uvwp_ref_res_v;
      sub_sigmas_v[2] = res_w / uvwp_ref_res_w;
      sub_sigmas_v[3] = res_c / uvwp_ref_res_c;

      convergence_uvwp_OK = sub_sigmas_v[0] < tol_flow
	&& sub_sigmas_v[1] < tol_flow && sub_sigmas_v[2] < tol_flow
	&& sub_sigmas_v[3] < tol_flow && (iter_uvwp >= uvwp_ref_it);
      
      iter_uvwp++;
      
    } /*   while(iter_uvwp < max_iter_uvwp && !convergence_uvwp_OK) */

    /* 4: SOLVE k-eps EQUATIONS */
    /* k-eps loop */
    if(turb_model && (outer_iter >= it_to_start_turb)){

      /* Set iteration counter and convergence flag for ke sub-loop */
      iter_ke = 0;
      convergence_ke_OK = 0;
      
      while(iter_ke < max_iter_ke && !convergence_ke_OK){
	
	/* Filter nodes with low y_plus out of the calculation */
	if(outer_iter % it_to_update_y_plus == 0){
	  filter_y_plus_3D(data);
	}

	/* k loop */	
	compute_turb_aux_fields_3D(data);	       
	set_coeffs_k_3D(data);
	set_bc_turb_k_3D(data);
	
	for(iter=0; iter<ref_it; iter++){
	  (*ADI_solver)(data->k_turb, &(data->coeffs_k),
			Nx, Ny, Nz, &(data->ADI_vars));
	}
	ref_res_k = compute_residues_3D(data->k_turb,
					&(data->coeffs_k), Nx, Ny, Nz);
	res_k = 1;
	iter = 1;
	while((iter < max_iter_ADI) &&
	      ((res_k / ref_res_k) > tol_ADI)){
	  
	  if(iter % it_for_update_k_coeffs == 0){
	    compute_turb_aux_fields_3D(data);
	    set_coeffs_k_3D(data);
	  }
	  
	  (*ADI_solver)(data->k_turb, &(data->coeffs_k),
			Nx, Ny, Nz, &(data->ADI_vars));
	  res_k = compute_residues_3D(data->k_turb,
				      &(data->coeffs_k), Nx, Ny, Nz);
	  iter++;
	}
	
	filter_small_values_omp(data->k_turb, Nx, Ny, Nz, SMALL_VALUE, 1);
	
	/* eps loop */
	compute_turb_aux_fields_3D(data);	       	
	set_coeffs_eps_3D(data);
	set_bc_turb_eps_3D(data);
	
	for(iter=0; iter<ref_it; iter++){
	  (*ADI_solver)(data->eps_turb, &(data->coeffs_eps),
			Nx, Ny, Nz, &(data->ADI_vars));
	}
	ref_res_eps = compute_residues_3D(data->eps_turb,
					  &(data->coeffs_eps), Nx, Ny, Nz);
	res_eps = 1;
	iter = 1;
	while((iter < max_iter_ADI) &&
	      ((res_eps / ref_res_eps) > tol_ADI)){
	  
	  if(iter % it_for_update_eps_coeffs == 0){
	    compute_turb_aux_fields_3D(data);
	    set_coeffs_eps_3D(data);
	  }
	  
	  (*ADI_solver)(data->eps_turb, &(data->coeffs_eps),
			Nx, Ny, Nz, &(data->ADI_vars));
	  res_eps = compute_residues_3D(data->eps_turb,
					&(data->coeffs_eps), Nx, Ny, Nz);
	  iter++;
	}
       
	filter_small_values_omp(data->eps_turb, Nx, Ny, Nz, SMALL_VALUE, 1);       
		
	if(iter_ke == ke_ref_it){
	  ke_ref_res_k = res_k;
	  ke_ref_res_eps = res_eps;
	}
	
	sub_sigmas_v[4] = res_k / ke_ref_res_k;
	sub_sigmas_v[5] = res_eps / ke_ref_res_eps;
	
	convergence_ke_OK = sub_sigmas_v[4] < tol_flow
	  && sub_sigmas_v[5] < tol_flow && (iter_ke >= ke_ref_it);
	
	iter_ke++;
      } /* while(iter_ke < max_iter_ke && !convergence_ke_OK) */

      /* Compute mu_turb_at velocity nodes
	 for next iteration of the u, v, w, p cycle */
      compute_mu_turb_at_vel_nodes_3D(data);
      
    } /* end if(turb_model && (outer_iter >= it_to_start_turb)) */

    /* Use coeffs with the latest values of fields to evaluate outer loop convergence */
    (*coeffs_flow_u)(data);
    (*coeffs_flow_v)(data);
    (*coeffs_flow_w)(data);
    
    res_u = compute_residues_3D(data->u, &(data->coeffs_u), nx, Ny, Nz);
    res_v = compute_residues_3D(data->v, &(data->coeffs_v), Nx, ny, Nz);
    res_w = compute_residues_3D(data->w, &(data->coeffs_w), Nx, Ny, nz);
    /* res_c hasn't changed from the one calculated inside uvwp sub-loop
       because velocity values are the same */      
      
    if(outer_iter == outer_ref_it){
      outer_ref_res_u = res_u;
      outer_ref_res_v = res_v;
      outer_ref_res_w = res_w;
      outer_ref_res_c = res_c;
    }
      
    sigmas_v[0] = res_u / outer_ref_res_u;
    sigmas_v[1] = res_v / outer_ref_res_v;
    sigmas_v[2] = res_w / outer_ref_res_w;
    sigmas_v[3] = res_c / outer_ref_res_c;    

    if(turb_model  && (outer_iter >= it_to_start_turb)){

      /* Use coeffs with the latest values of fields to evaluate outer loop convergence */
      compute_turb_aux_fields_3D(data);
      set_coeffs_k_3D(data);
      set_coeffs_eps_3D(data);

      res_k = compute_residues_3D(data->k_turb,
				  &(data->coeffs_k), Nx, Ny, Nz);
      res_eps = compute_residues_3D(data->eps_turb,
				    &(data->coeffs_eps), Nx, Ny, Nz);      
      
      if(outer_iter == outer_ref_it + it_to_start_turb){
	outer_ref_res_k = res_k;
	outer_ref_res_eps = res_eps;
      }
      
      sigmas_v[4] = res_k / outer_ref_res_k;
      sigmas_v[5] = res_eps / outer_ref_res_eps;    
    
      convergence_turb_OK = sigmas_v[4] < tol_flow
	&& sigmas_v[5] < tol_flow && (outer_iter >= outer_ref_it + it_to_start_turb);
    }
    else{
      convergence_turb_OK = 1;
    }

    convergence_OK =  sigmas_v[0] < tol_flow
      && sigmas_v[1] < tol_flow && sigmas_v[2] < tol_flow
      && sigmas_v[3] < tol_flow && convergence_turb_OK
      && (outer_iter > outer_ref_it);
    
    /* Here process action upon signal raise */
    if(action_on_signal_OK){
      
      process_action_on_signal(data, &convergence_OK, &convergence_OK);

      flow_scheme = data->flow_scheme;
      /* UPWIND scheme */
      if(flow_scheme == 0){
	coeffs_flow_u = coeffs_flow_u_upwind_3D;
	coeffs_flow_v = coeffs_flow_v_upwind_3D;
	coeffs_flow_w = coeffs_flow_w_upwind_3D;
      } 
      else if(flow_scheme == 1){
	/* QUICK scheme */    
	coeffs_flow_u = coeffs_flow_u_quick_3D;
	coeffs_flow_v = coeffs_flow_v_quick_3D;
	coeffs_flow_w = coeffs_flow_w_quick_3D;
      }
      /* van Leer scheme */
      else if(flow_scheme == 2){
	coeffs_flow_u = coeffs_flow_u_TVD_3D;
	coeffs_flow_v = coeffs_flow_v_TVD_3D;
	coeffs_flow_w = coeffs_flow_w_TVD_3D;
      }

      /* Update in case of modification the new values */
      it_for_update_coeffs = data->it_for_update_coeffs;
      max_iter_flow = data->max_iter_flow;
      max_iter_ADI = data->max_iter_ADI;
      tol_flow = data->tol_flow;
      tol_ADI = data->tol_ADI;
      
      action_on_signal_OK = 0;
    }

    time_i = time_f;           
    time_f = time_f + elapsed_time(&timer);
    
    if(outer_iter % res_sigma_iter == 0){

      /* Leave outside this IF clause if you need it for convergence criteria
	 Otherwise leave here for optimization purposes */
      sigmas_v[6] = mass_io_balance_3D(data);
      
      sigmas->it = outer_iter;

      glob_norm[0] = compute_norm_glob_res_3D(data->u, &(data->coeffs_u), nx, Ny, Nz);
      glob_norm[1] = compute_norm_glob_res_3D(data->v, &(data->coeffs_v), Nx, ny, Nz);
      glob_norm[2] = compute_norm_glob_res_3D(data->w, &(data->coeffs_w), Nx, Ny, nz);

      if(turb_model && (outer_iter >= it_to_start_turb)){
       glob_norm[4] = compute_norm_glob_res_3D(data->k_turb, &(data->coeffs_k), Nx, Ny, Nz);
       glob_norm[5] = compute_norm_glob_res_3D(data->eps_turb, &(data->coeffs_eps), Nx, Ny, Nz);
      }
      
      print_flow_it_info(msg, SIZE, rstride,
			 flow_solver_name, outer_iter, max_iter_flow,
			 time_i, time_f, turb_model, sigmas,
			 wins);
      
      update_graph_window(data);
      
      write_sfile(data);
      
#ifdef AVGIV_OK
      write_in_vel_3D(data);
#endif
      
    }
    
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);
      sigmas->it = outer_iter;
      write_to_log_file(data, msg);
      timer_flag_OK = 0;
    }
    
    /* Updating the server */
    if((launch_server_OK) && 
       (outer_iter % server_update_it == 0)){
      update_server(data);
    }
    
    /* Updating the animation */
    if((animation_flow_OK) && 
       (outer_iter % animation_flow_update_it == 0)){

      center_vel_components(data);
      compute_vel_at_main_cells(data);
      
      update_anims((void *) data);
    }
    
    outer_iter++;
    
  } // end while(outer_iter < max_iter_flow && !convergence_OK){

  /* Save iteration */
  sigmas->flow_it = outer_iter - 1;

#ifdef AVGIV_OK
  close_in_vel(data);
#endif
  
  solve_flow_post_calc_jobs(data);
  
  /* Print normalized global residuals */
  
  write_gnres_sfile(data);
  
  return;

}

/* SET_D_W */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNynz) Dw_x: Diffusive parameter for w momentum equation at x_faces.

  Array (double, Nxnynz) Dw_y: Diffusive parameter for w momentum equation at y_faces.

  Array (double, NxNyNz) Dw_z: Diffusive parameter for w momentum equation at z_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 18-10-2019
*/  
void set_D_w(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_face;
  int index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int diff_w_face_z_count = data->diff_w_face_z_count;

  const int *diff_w_face_z_index = data->diff_w_face_z_index;
  const int *diff_w_face_z_index_w = data->diff_w_face_z_index_w;

  double mu_eff;
  double mu_eff_0, mu_eff_1;

  const double mu = data->mu;

  double *Dw_x = data->Dw_x;
  double *Dw_y = data->Dw_y;
  double *Dw_z = data->Dw_z;

  const double *w_faces_x = data->w_faces_x;
  const double *w_faces_y = data->w_faces_y;
  const double *w_faces_z = data->w_faces_z;
  const double *Delta_xE_w = data->Delta_xE_w;
  const double *Delta_yN_w = data->Delta_yN_w;
  const double *Delta_zT_w = data->Delta_zT_w;
  const double *mu_turb_at_w_nodes = data->mu_turb_at_w_nodes;
  const double *fw_w = data->fw_w;
  const double *fe_w = data->fe_w;
  const double *fn_w = data->fn_w;
  const double *fs_w = data->fs_w;
  const double *ft_w = data->ft_w;
  const double *fb_w = data->fb_w;
  const double *diff_w_face_z_Delta = data->diff_w_face_z_Delta;

#pragma omp parallel for default(none) private(I, J, K, k, index_, index_w, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dw_z, w_faces_z, mu_turb_at_w_nodes, Delta_zT_w, ft_w, fb_w)
  
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	k = K - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	mu_eff_0 = mu + mu_turb_at_w_nodes[index_w];
	mu_eff_1 = mu + mu_turb_at_w_nodes[index_w+Nx*Ny];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * ft_w[index_w] + mu_eff_1 * fb_w[index_w+Nx*Ny]);
	Dw_z[index_] = w_faces_z[index_] *
	  mu_eff / Delta_zT_w[index_w];
      }
    }
  }

  /* Correction for cut faces */

  for(z=0; z<diff_w_face_z_count; z++){

    index_ = diff_w_face_z_index[z];
    index_w = diff_w_face_z_index_w[z];
    mu_eff_0 = mu + mu_turb_at_w_nodes[index_w];
    mu_eff_1 = mu + mu_turb_at_w_nodes[index_w+Nx*Ny];
    mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * ft_w[index_w] + mu_eff_1 * fb_w[index_w+Nx*Ny]);
    Dw_z[index_] = w_faces_z[index_] *
      mu_eff / diff_w_face_z_Delta[z];
  }

#pragma omp parallel for default(none) private(I, J, i, k, index_face, index_w, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dw_x, w_faces_x, Delta_xE_w, mu_turb_at_w_nodes, fe_w, fw_w)
  
  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){

	I = i + 1;
	index_face = k*nx*Ny + J*nx + i;
	index_w = k*Nx*Ny + J*Nx + I;

	mu_eff_0 = mu + mu_turb_at_w_nodes[index_w-1];
	mu_eff_1 = mu + mu_turb_at_w_nodes[index_w];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fe_w[index_w-1] + mu_eff_1 * fw_w[index_w]);
	Dw_x[index_face] = w_faces_x[index_face] *
	  mu_eff / Delta_xE_w[index_w-1];
      }
    }
  }

#pragma omp parallel for default(none) private(I, J, j, k, index_face, index_w, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dw_y, w_faces_y, Delta_yN_w, mu_turb_at_w_nodes, fn_w, fs_w)
  
  for(k=1; k<nz-1; k++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){

	J = j + 1;
	index_face = k*Nx*ny + j*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	mu_eff_0 = mu + mu_turb_at_w_nodes[index_w-Nx];
	mu_eff_1 = mu + mu_turb_at_w_nodes[index_w];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fn_w[index_w-Nx] + mu_eff_1 * fs_w[index_w]);
	Dw_y[index_face] = w_faces_y[index_face] *
	  mu_eff / Delta_yN_w[index_w-Nx];
      }
    }
  }
    
  
  return;
}

/* SET_F_W */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNynz) Fw_x: Convective parameter for w momentum equation at x_faces.

  Array (double, Nxnynz) Fw_y: Convective parameter for w momentum equation at y_faces.

  Array (double, NxNyNz) Fw_z: Convective parameter for w momentum equation at z_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 17-10-2019
*/
void set_F_w(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_face;
  int index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int w_cut_face_Fx_count = data->w_cut_face_Fx_count;
  const int w_cut_face_Fy_count = data->w_cut_face_Fy_count;

  const int *w_cut_face_Fx_index = data->w_cut_face_Fx_index;
  const int *w_cut_face_Fx_index_u = data->w_cut_face_Fx_index_u;
  const int *w_cut_face_Fy_index = data->w_cut_face_Fy_index;
  const int *w_cut_face_Fy_index_v = data->w_cut_face_Fy_index_v;

  const double rho = data->rho_v[0];

  double *Fw_x = data->Fw_x;
  double *Fw_y = data->Fw_y;
  double *Fw_z = data->Fw_z;

  const double *w_faces_x = data->w_faces_x;
  const double *w_faces_y = data->w_faces_y;
  const double *w_faces_z = data->w_faces_z;
  const double *ft_u = data->ft_u;
  const double *fb_u = data->fb_u;
  const double *ft_v = data->ft_v;
  const double *fb_v = data->fb_v;
  const double *w_cut_face_Fx_B = data->w_cut_face_Fx_B;
  const double *w_cut_face_Fx_T = data->w_cut_face_Fx_T;
  const double *w_cut_face_Fy_B = data->w_cut_face_Fy_B;
  const double *w_cut_face_Fy_T = data->w_cut_face_Fy_T;
  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *w_cut_fb = data->w_cut_fb;
  const double *w_cut_ft = data->w_cut_ft;

# pragma omp parallel for default(none) private(I, J, K, k, index_face, index_w) \
  shared(Fw_z, w_faces_z, w, w_cut_fb, w_cut_ft)
  
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	k = K - 1;
	index_face = K*Nx*Ny + J*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	/* Not at center of cut face, corrected with alpha later */
	Fw_z[index_face] = w_faces_z[index_face] * rho *
	  (w[index_w] * w_cut_fb[index_face] + w[index_w+Nx*Ny] * w_cut_ft[index_face]);
      }
    }
  }

#pragma omp parallel for default(none) private(J, K, i, j, index_u, index_face) \
  shared(Fw_x, w_faces_x, u, ft_u, fb_u)
  
  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){

	K = k + 1;
	index_u = (K - 1)*nx*Ny + J*nx + i;
	index_face = k*nx*Ny + J*nx + i;

	/* Computed at center of cut face */
	Fw_x[index_face] = w_faces_x[index_face] * rho *
	  (u[index_u] * ft_u[index_u] + u[index_u+nx*Ny] * fb_u[index_u+nx*Ny]);
      }
    }
  }

#pragma omp parallel for default(none) private(I, K, j, k, index_v, index_face) \
  shared(Fw_y, w_faces_y, v, ft_v, fb_v)
  
  for(k=1; k<nz-1; k++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){

	K = k + 1;
	index_v = (K - 1)*Nx*ny + j*Nx + I;
	index_face = k*Nx*ny + j*Nx + I;

	/* Computed at center of cut face */
	Fw_y[index_face] = w_faces_y[index_face] * rho *
	  (v[index_v] * ft_v[index_v] + v[index_v+Nx*ny] * fb_v[index_v+Nx*ny]);
      }
    }
  }

  /* Corrections near solid surface */

  for(z=0; z<w_cut_face_Fx_count; z++){

    index_face = w_cut_face_Fx_index[z];
    index_u = w_cut_face_Fx_index_u[z];
    Fw_x[index_face] = w_faces_x[index_face] * rho *
      (u[index_u] * w_cut_face_Fx_B[z] + u[index_u+nx*Ny] * w_cut_face_Fx_T[z]);
  }

  for(z=0; z<w_cut_face_Fy_count; z++){

    index_face = w_cut_face_Fy_index[z];
    index_v = w_cut_face_Fy_index_v[z];
    Fw_y[index_face] = w_faces_y[index_face] * rho *
      (v[index_v] * w_cut_face_Fy_B[z] + v[index_v+Nx*ny] * w_cut_face_Fy_T[z]);
  }

  return;
}

/* SET_F_u_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) Fu_x: Convective parameter for u momentum equation at x_faces.

  Array (double, nxnyNz) Fu_y: Convective parameter for u momentum equation at y_faces.

  Array (double, nxNynz) Fu_z: Convective parameter for u momentum equation at z_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 14-10-2019
*/
void set_F_u_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_face;
  int index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int u_cut_face_Fy_count = data->u_cut_face_Fy_count;
  const int u_cut_face_Fz_count = data->u_cut_face_Fz_count;

  const int *u_cut_face_Fy_index = data->u_cut_face_Fy_index;
  const int *u_cut_face_Fy_index_v = data->u_cut_face_Fy_index_v;
  const int *u_cut_face_Fz_index = data->u_cut_face_Fz_index;  
  const int *u_cut_face_Fz_index_w = data->u_cut_face_Fz_index_w;

  const double rho = data->rho_v[0];

  double *Fu_x = data->Fu_x;
  double *Fu_y = data->Fu_y;
  double *Fu_z = data->Fu_z;
  
  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  const double *u_faces_z = data->u_faces_z;
  const double *fe_v = data->fe_v;
  const double *fw_v = data->fw_v;
  const double *fe_w = data->fe_w;
  const double *fw_w = data->fw_w;
  const double *u_cut_face_Fy_W = data->u_cut_face_Fy_W;
  const double *u_cut_face_Fy_E = data->u_cut_face_Fy_E;
  const double *u_cut_face_Fz_W = data->u_cut_face_Fz_W;
  const double *u_cut_face_Fz_E = data->u_cut_face_Fz_E;
  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *u_cut_fw = data->u_cut_fw;
  const double *u_cut_fe = data->u_cut_fe;

# pragma omp parallel for default(none) private(I, J, K, i, index_face, index_u) \
  shared(Fu_x, u_faces_x, u, u_cut_fw, u_cut_fe)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	index_face = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;
	/* Not at center of cut face, corrected with alpha later */
	Fu_x[index_face] = u_faces_x[index_face] * rho *
	  (u[index_u] * u_cut_fw[index_face] + u[index_u+1] * u_cut_fe[index_face]);
      
      }
    }
  }

# pragma omp parallel for default(none) private(I, K, i, j, index_v, index_face) \
  shared(Fu_y, u_faces_y, v, fe_v, fw_v)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(i=1; i<nx-1; i++){

	I = i + 1;
	index_v = K*Nx*ny + j*Nx + I - 1;
	index_face = K*nx*ny + j*nx + i;

	/* Computed at center of cut face */      
	Fu_y[index_face] = u_faces_y[index_face] * rho * 
	  (v[index_v] * fe_v[index_v] + v[index_v+1] * fw_v[index_v+1]);
      }
    }
  }

# pragma omp parallel for default(none) private(I, J, i, k, index_w, index_face) \
  shared(Fu_z, u_faces_z, w, fe_w, fw_w)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){

	I = i + 1;
	index_w = k*Nx*Ny + J*Nx + I - 1;
	index_face = k*nx*Ny + J*nx + i;

	/* Computed at center of cut face */
	Fu_z[index_face] = u_faces_z[index_face] * rho *
	  (w[index_w] * fe_w[index_w] + w[index_w+1] * fw_w[index_w+1]);
      }
    }
  }
  
  /* Corrections near solid surface */
  
  for(z=0; z<u_cut_face_Fy_count; z++){

    index_face = u_cut_face_Fy_index[z];
    index_v = u_cut_face_Fy_index_v[z];
    Fu_y[index_face] = u_faces_y[index_face] * rho *
      (v[index_v] * u_cut_face_Fy_W[z] + v[index_v+1] * u_cut_face_Fy_E[z]);
  }

  for(z=0; z<u_cut_face_Fz_count; z++){

    index_face = u_cut_face_Fz_index[z];
    index_w = u_cut_face_Fz_index_w[z];
    Fu_z[index_face] = u_faces_z[index_face] * rho *
      (w[index_w] * u_cut_face_Fz_W[z] + w[index_w+1] * u_cut_face_Fz_E[z]);
  }

  return;
}

/* SET_F_v_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxnyNz) Fv_x: Convective parameter for v momentum equation at x_faces.

  Array (double, NxNyNz) Fv_y: Convective parameter for v momentum equation at y_faces.

  Array (double, Nxnynz) Fv_z: Convective parameter for v momentum equation at z_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 17-10-2019
*/
void set_F_v_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_face;
  int index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int v_cut_face_Fx_count = data->v_cut_face_Fx_count;
  const int v_cut_face_Fz_count = data->v_cut_face_Fz_count;

  const int *v_cut_face_Fx_index = data->v_cut_face_Fx_index;
  const int *v_cut_face_Fx_index_u = data->v_cut_face_Fx_index_u;
  const int *v_cut_face_Fz_index = data->v_cut_face_Fz_index;
  const int *v_cut_face_Fz_index_w = data->v_cut_face_Fz_index_w;

  const double rho = data->rho_v[0];

  double *Fv_x = data->Fv_x;
  double *Fv_y = data->Fv_y;
  double *Fv_z = data->Fv_z;

  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *v_faces_z = data->v_faces_z;
  const double *fn_u = data->fn_u;
  const double *fs_u = data->fs_u;
  const double *fn_w = data->fn_w;
  const double *fs_w = data->fs_w;
  const double *v_cut_face_Fx_S = data->v_cut_face_Fx_S;
  const double *v_cut_face_Fx_N = data->v_cut_face_Fx_N;
  const double *v_cut_face_Fz_S = data->v_cut_face_Fz_S;
  const double *v_cut_face_Fz_N = data->v_cut_face_Fz_N;
  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *v_cut_fs = data->v_cut_fs;
  const double *v_cut_fn = data->v_cut_fn;

# pragma omp parallel for default(none) private(I, J, K, j, index_face, index_v) \
  shared(Fv_y, v_faces_y, v, v_cut_fs, v_cut_fn)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	j = J - 1;      
	index_face = K*Nx*Ny + J*Nx + I;
	index_v = K*Nx*ny + j*Nx + I;
      
	/* Not at center of cut face, corrected with alpha later */
	Fv_y[index_face] = v_faces_y[index_face] * rho *
	  (v[index_v] * v_cut_fs[index_face] + v[index_v+Nx] * v_cut_fn[index_face]);
      }
    }
  }
  

# pragma omp parallel for default(none) private(J, K, i, j, index_u, index_face) \
  shared(Fv_x, v_faces_x, u, fn_u, fs_u)

  for(K=1; K<Nz-1; K++){
    for(j=1; j<ny-1; j++){
      for(i=0; i<nx; i++){

	J = j + 1;
	index_u = K*nx*Ny + (J - 1)*nx + i;
	index_face = K*nx*ny + j*nx + i;
	
	/* Computed at center of cut face */      	
	Fv_x[index_face] = v_faces_x[index_face] * rho * 
	  (u[index_u] * fn_u[index_u] + u[index_u+nx] * fs_u[index_u+nx]);
      
      }
    }
  }

#pragma omp parallel for default(none) private(I, J, j, k, index_w, index_face) \
  shared(Fv_z, v_faces_z, w, fn_w, fs_w)
  
  for(k=0; k<nz; k++){
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){

	J = j + 1;
	index_w = k*Nx*Ny + (J - 1)*Nx + I;
	index_face = k*Nx*ny + j*Nx + I;

	/* Computed at center of cut face */
	Fv_z[index_face] = v_faces_z[index_face] * rho *
	  (w[index_w] * fn_w[index_w] + w[index_w+Nx] * fs_w[index_w+Nx]); 
      }
    }
  }

  /* Corrections near solid surface */  

  for(z=0; z<v_cut_face_Fx_count; z++){

    index_face = v_cut_face_Fx_index[z];
    index_u = v_cut_face_Fx_index_u[z];
    Fv_x[index_face] = v_faces_x[index_face] * rho *
      (u[index_u] * v_cut_face_Fx_S[z] + u[index_u+nx] * v_cut_face_Fx_N[z]);
  }

  for(z=0; z<v_cut_face_Fz_count; z++){
    
    index_face = v_cut_face_Fz_index[z];
    index_w = v_cut_face_Fz_index_w[z];
    Fv_z[index_face] = v_faces_z[index_face] * rho *
      (w[index_w] * v_cut_face_Fz_S[z] + w[index_w+Nx] * v_cut_face_Fz_N[z]);
  }

  return;
}

/* SET_D_u_3D */
/*****************************************************************************/
/**

  Sets:

  Array (double, NxNyNz) Du_x: Diffusive parameter for u momentum equation at x_faces.

  Array (double, nxnyNz) Du_y: Diffusive parameter for u momentum equation at y_faces.

  Array (double, nxNynz) Du_z: Diffusive parameter for u momentum equation at z_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 18-10-2019
*/
void set_D_u_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_face;
  int index_u;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int diff_u_face_x_count = data->diff_u_face_x_count;

  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;

  double mu_eff;
  double mu_eff_0, mu_eff_1;

  const double mu = data->mu;

  double *Du_x = data->Du_x;
  double *Du_y = data->Du_y;
  double *Du_z = data->Du_z;
  
  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  const double *u_faces_z = data->u_faces_z;  
  const double *Delta_xE_u = data->Delta_xE_u;
  const double *Delta_yN_u = data->Delta_yN_u;
  const double *Delta_zT_u = data->Delta_zT_u;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  const double *fw_u = data->fw_u;
  const double *fe_u = data->fe_u;
  const double *fn_u = data->fn_u;
  const double *fs_u = data->fs_u;
  const double *ft_u = data->ft_u;
  const double *fb_u = data->fb_u;
  const double *diff_u_face_x_Delta = data->diff_u_face_x_Delta;
  
#pragma omp parallel for default(none) private(I, J, K, i, index_, index_u, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Du_x, u_faces_x, mu_turb_at_u_nodes, Delta_xE_u, fw_u, fe_u)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;

	mu_eff_0 = mu + mu_turb_at_u_nodes[index_u];
	mu_eff_1 = mu + mu_turb_at_u_nodes[index_u+1];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fe_u[index_u] + mu_eff_1 * fw_u[index_u+1]);
	Du_x[index_] = u_faces_x[index_] * 
	  mu_eff / Delta_xE_u[index_u];

      }
    }
  }

  /* Correction for cut faces */

  for(z=0; z<diff_u_face_x_count; z++){
    
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    mu_eff_0 = mu + mu_turb_at_u_nodes[index_u];
    mu_eff_1 = mu + mu_turb_at_u_nodes[index_u+1];
    mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fe_u[index_u] + mu_eff_1 * fw_u[index_u+1]);
    Du_x[index_] = u_faces_x[index_] * 
      mu_eff / diff_u_face_x_Delta[z];
  }

  
#pragma omp parallel for default(none) private(J, K, i, j, index_face, index_u, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Du_y, u_faces_y, Delta_yN_u, mu_turb_at_u_nodes, fn_u, fs_u)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(i=1; i<nx-1; i++){

	J = j + 1;
	index_face = K*nx*ny + j*nx + i;
	index_u = K*nx*Ny + J*nx + i;
       
	mu_eff_0 = mu + mu_turb_at_u_nodes[index_u-nx];
	mu_eff_1 = mu + mu_turb_at_u_nodes[index_u];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fn_u[index_u-nx] + mu_eff_1 * fs_u[index_u]);
	Du_y[index_face] = u_faces_y[index_face] * 
	  mu_eff / Delta_yN_u[index_u-nx];
      }
    }
  }

#pragma omp parallel for default(none) private(J, K, i, k, index_face, index_u, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Du_z, u_faces_z, Delta_zT_u, mu_turb_at_u_nodes, ft_u, fb_u)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){

	K = k + 1;
	index_face = k*nx*Ny + J*nx + i;
	index_u = K*nx*Ny + J*nx + i;

	mu_eff_0 = mu + mu_turb_at_u_nodes[index_u-nx*Ny];
	mu_eff_1 = mu + mu_turb_at_u_nodes[index_u];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * ft_u[index_u-nx*Ny] + mu_eff_1 * fb_u[index_u]);
	Du_z[index_face] = u_faces_z[index_face] *
	  mu_eff / Delta_zT_u[index_u-nx*Ny];
      }
    }
  }
  
  return;
}

/* SET_D_v */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxnyNz) Dv_x: Diffusive parameter for v momentum equation at x_faces.

  Array (double, NxNyNz) Dv_y: Diffusive parameter for v momentum equation at y_faces.

  Array (double, Nxnynz) Dv_z: Diffusive parameter for v momentum equation at z_faces.

  @param data

  @return ret_value not fully implemented.
  
  MODIFIED: 11-06-2019
*/
void set_D_v_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_face;
  int index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int diff_v_face_y_count = data->diff_v_face_y_count;

  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;

  double mu_eff;
  double mu_eff_0, mu_eff_1;

  const double mu = data->mu;

  double *Dv_x = data->Dv_x;
  double *Dv_y = data->Dv_y;
  double *Dv_z = data->Dv_z;

  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *v_faces_z = data->v_faces_z;
  const double *Delta_xE_v = data->Delta_xE_v;
  const double *Delta_yN_v = data->Delta_yN_v;
  const double *Delta_zT_v = data->Delta_zT_v;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  const double *fw_v = data->fw_v;
  const double *fe_v = data->fe_v;
  const double *fs_v = data->fs_v;
  const double *fn_v = data->fn_v;
  const double *fb_v = data->fb_v;
  const double *ft_v = data->ft_v;
  const double *diff_v_face_y_Delta = data->diff_v_face_y_Delta;

#pragma omp parallel for default(none) private(I, J, K, j, index_, index_v, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dv_y, v_faces_y, Delta_yN_v, mu_turb_at_v_nodes, fs_v, fn_v)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	j = J - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_v = K*Nx*ny + j*Nx + I;

	mu_eff_0 = mu + mu_turb_at_v_nodes[index_v];
	mu_eff_1 = mu + mu_turb_at_v_nodes[index_v+Nx];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fn_v[index_v] + mu_eff_1 * fs_v[index_v+Nx]);
	Dv_y[index_] = v_faces_y[index_] * 
	  mu_eff / Delta_yN_v[index_v];

      }
    }
  }

  /* Correction for cut faces */

  for(z=0; z<diff_v_face_y_count; z++){
    
    index_ = diff_v_face_y_index[z];
    index_v = diff_v_face_y_index_v[z];
    mu_eff_0 = mu + mu_turb_at_v_nodes[index_v];
    mu_eff_1 = mu + mu_turb_at_v_nodes[index_v+Nx];
    mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fn_v[index_v] + mu_eff_1 * fs_v[index_v+Nx]);
    Dv_y[index_] = v_faces_y[index_] * 
      mu_eff / diff_v_face_y_Delta[z];
    
  }

#pragma omp parallel for default(none) private(I, J, i, j, index_face, index_v, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dv_x, v_faces_x, Delta_xE_v, mu_turb_at_v_nodes, fe_v, fw_v)

  for(K=1; K<Nz-1; K++){
    for(j=1; j<ny-1; j++){
      for(i=0; i<nx; i++){

	I = i + 1;
	index_face = K*nx*ny + j*nx + i;
	index_v = K*Nx*ny + j*Nx + I;

	mu_eff_0 = mu + mu_turb_at_v_nodes[index_v-1];
	mu_eff_1 = mu + mu_turb_at_v_nodes[index_v];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * fe_v[index_v-1] + mu_eff_1 * fw_v[index_v]); 
	Dv_x[index_face] = v_faces_x[index_face] * 
	  mu_eff / Delta_xE_v[index_v-1];
      }
    }
  }

#pragma omp parallel for default(none) private(j, k, I, K, index_face, index_v, mu_eff, mu_eff_0, mu_eff_1) \
  shared(Dv_z, v_faces_z, Delta_zT_v, mu_turb_at_v_nodes, ft_v, fb_v)
  
  for(k=0; k<nz; k++){
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){

	K = k + 1;
	index_face = k*Nx*ny + j*Nx + I;
	index_v = K*Nx*ny + j*Nx + I;

	mu_eff_0 = mu + mu_turb_at_v_nodes[index_v-Nx*ny];
	mu_eff_1 = mu + mu_turb_at_v_nodes[index_v];
	mu_eff = mu_eff_0 * mu_eff_1 / (mu_eff_0 * ft_v[index_v-Nx*ny] + mu_eff_1 * fb_v[index_v]);
	Dv_z[index_face] = v_faces_z[index_face] *
	  mu_eff / Delta_zT_v[index_v-Nx*ny];
      }
    }
  }
  
  return;
}

/* INIT_P_AT_BOUNDARY_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNx) p: initialize boundary values.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 21-10-2019
*/
void init_p_at_boundary_3D(Data_Mem *data){

  int I, J, K, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *p_dom_matrix = data->p_dom_matrix;

  double *p = data->p;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);
  const b_cond_flow *bc_flow_bottom = &(data->bc_flow_bottom);
  const b_cond_flow *bc_flow_top = &(data->bc_flow_top);


  /* Needed for SIMPLE, SIMPLE_CANG, SIMPLEC, because boundary condition will 
     be applied to pressure correction only */

#pragma omp parallel sections default(none) private(I, J, K, index_)	\
  shared(bc_flow_west, bc_flow_east, bc_flow_south, bc_flow_north, \
	 bc_flow_bottom, bc_flow_top, p_dom_matrix, p)
  {

#pragma omp section
    {
      /* West */
      I = 1;
      
      if(bc_flow_west->type == 4){

	/* Constant pressure */
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = bc_flow_west->p_value;
	      p[index_-1] = p[index_];
	    }
	  }
	}	
      }
      else if(bc_flow_west->type == 5){

	/* Periodic boundary */
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = 0.5 * (p[K*Nx*Ny + J*Nx + Nx-3] + p[K*Nx*Ny + J*Nx + 2]);
	      p[index_-1] = p[index_];
	    }
	  }
	}
      }
      else{
	
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_-1] = p[index_];
	    }
	  }
	}
      }
    }

#pragma omp section
    {
      /* East */
      I = Nx - 2;
      
      if(bc_flow_east->type == 4){

	/* Constant pressure */
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = bc_flow_east->p_value;
	      p[index_+1] = p[index_];
	    }
	  }
	}
      }
      else if(bc_flow_east->type == 5){

	/* Periodic boundary */
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = 0.5 * (p[K*Nx*Ny + J*Nx + Nx-3] + p[K*Nx*Ny + J*Nx + 2]);
	      p[index_+1] = p[index_];
	    }
	  }
	}
      }
      else{
	
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_+1] = p[index_];
	    }
	  }
	}
      }
    }

#pragma omp section 
    {
      /* South */
      J = 1;
      
      if(bc_flow_south->type == 4){

	/* Constant pressure */
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = bc_flow_south->p_value;
	      p[index_-Nx] = p[index_];
	    }
	  }
	}
      }
      else if(bc_flow_south->type == 5){

	/* Periodic boundary */
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = 0.5 * (p[K*Nx*Ny + (Ny-3)*Nx + I] + p[K*Nx*Ny + 2*Nx + I]);
	      p[index_-Nx] = p[index_];
	    }
	  }
	}
      }
      else{
	
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_-Nx] = p[index_];
	    }
	  }
	}
      }
    }

#pragma omp section 
    {
      /* North */
      J = Ny - 2;
      
      if(bc_flow_north->type == 4){

	/* Constant pressure */
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = bc_flow_north->p_value;
	      p[index_+Nx] = p[index_];
	    }
	  }
	}
      }
      else if(bc_flow_north->type == 5){

	/* Periodic boundary */
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = 0.5 * (p[K*Nx*Ny + (Ny-3)*Nx + I] + p[K*Nx*Ny + 2*Nx + I]);
	      p[index_+Nx] = p[index_];
	    }
	  }
	}
      }
      else{
	
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_+Nx] = p[index_];
	    }
	  }
	}
      }
    }

#pragma omp section
    {
      /* Bottom */

      K = 1;

      if(bc_flow_bottom->type == 4){

	/* Constant pressure */
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = bc_flow_bottom->p_value;	    
	      p[index_-Nx*Ny] = p[index_];
	    }
	  }
	}	
      }
      else if(bc_flow_bottom->type == 5){

	/* Periodic boundary */
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = 0.5 * (p[(Ny-3)*Nx*Ny + J*Nx + I] + p[2*Nx*Ny + J*Nx + I]);
	      p[index_-Nx*Ny] = p[index_];
	    }
	  }
	}
      }
      else{

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_-Nx*Ny] = p[index_];
	    }
	  }
	}
      }
             
    }

#pragma omp section
    {
      /* Top */
      K = Nz - 2;

      if(bc_flow_top->type == 4){

	/* Constant pressure */
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = bc_flow_top->p_value;
	      p[index_+Nx*Ny] = p[index_];
	    }
	  }
	}
      }
      else if(bc_flow_top->type == 5){

	/* Periodic boundary */
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_] = 0.5 * (p[(Nz-3)*Nx*Ny + J*Nx + I] + p[2*Nx*Ny + J*Nx + I]);
	      p[index_+Nx*Ny] = p[index_];
	    }
	  }
	}
      }
      else{

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;
	    if(p_dom_matrix[index_] == 0){
	      p[index_+Nx*Ny] = p[index_];
	    }
	  }
	}
      }
      
    }

  } // sections
  
  return;
}

/* SET_P_REF_NODE_3D */
/*****************************************************************************/
/**
  
  Sets:

  (int) set_ref_p_node: set to true if a reference pressure node is set.

  (int) ref_p_node_index: index of reference pressure node.

  (double) ref_p_value: reference pressure value.

  @param data

  @return ret_value.

  MODIFIED: 26-10-2019
*/
int set_p_ref_node_3D(Data_Mem *data){

  int I, J, K, index_;
  int ref_p_index_found = 0;
  int ret_value = 0;

  int *set_ref_p_node = &(data->set_ref_p_node);
  int *ref_p_node_index = &(data->ref_p_node_index);

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int w_ref_p_node_OK = data->bc_flow_west.ref_p_node_OK;
  const int e_ref_p_node_OK = data->bc_flow_east.ref_p_node_OK;
  const int s_ref_p_node_OK = data->bc_flow_south.ref_p_node_OK;
  const int n_ref_p_node_OK = data->bc_flow_north.ref_p_node_OK;
  const int b_ref_p_node_OK = data->bc_flow_bottom.ref_p_node_OK;
  const int t_ref_p_node_OK = data->bc_flow_top.ref_p_node_OK;
  
  const int *p_dom_matrix = data->p_dom_matrix;

  double *ref_p_value = &(data->ref_p_value);

  const double w_p_value = data->bc_flow_west.p_value;
  const double e_p_value = data->bc_flow_east.p_value;
  const double s_p_value = data->bc_flow_south.p_value;
  const double n_p_value = data->bc_flow_north.p_value;
  const double b_p_value = data->bc_flow_bottom.p_value;
  const double t_p_value = data->bc_flow_top.p_value;  

  double *p = data->p;
  
  if(w_ref_p_node_OK + e_ref_p_node_OK + s_ref_p_node_OK +
     n_ref_p_node_OK + b_ref_p_node_OK + t_ref_p_node_OK > 1){
    ret_value = 1;
    return ret_value;
  }

  /* Initialization */
  *set_ref_p_node = 0;

  /* West boundary */
  if(w_ref_p_node_OK){
    I = 1;

    K = 2; 
    while((K < Nz-2) && (!ref_p_index_found)){
      /* J between 2 and Ny-3 to influence only west boundary */
      J = 2;
      
      while((J < Ny-2) && (!ref_p_index_found)){
	
	index_ = K*Nx*Ny + J*Nx + I;
	/* Search for some available pressure node */
	if(p_dom_matrix[index_] == 0){
	  *set_ref_p_node = 1;
	  *ref_p_node_index = index_;
	  ref_p_index_found = 1;
	  /* Store reference pressure value to apply in coeffs_p function */
	  *ref_p_value = w_p_value;
	  p[index_] = w_p_value;
	  /* Needed for SIMPLE, because the iterative section will maintain equality of pressure correction  only */
	  p[index_-1] = p[index_];
	}
	J++;
      }
      K++;
    }
  }

  /* East boundary */
  if(e_ref_p_node_OK){
    I = Nx - 2;

    K = 2;
    while((K < Nz-2) && (!ref_p_index_found)){
      
      J = 2;
      while((J < Ny-2) && (!ref_p_index_found)){
      
	index_ = K*Nx*Ny + J*Nx + I;
	/* Search for some available pressure node */
	if(p_dom_matrix[index_] == 0){
	  *set_ref_p_node = 1;
	  *ref_p_node_index = index_;
	  ref_p_index_found = 1;
	  /* Store reference pressure to apply in coeffs_p function */
	  *ref_p_value = e_p_value;	  
	  p[index_] = e_p_value;
	  p[index_+1] = p[index_];
	}
	J++;
      }
      K++;
    }
  }

  /* South boundary */
  if(s_ref_p_node_OK){
    J = 1;

    K = 2;
    while((K < Nz-2) && (!ref_p_index_found)){
      
      I = 2;
      while((I < Nx-2) && (!ref_p_index_found)){
	
	index_ = K*Nx*Ny + J*Nx + I;
	/* Search for some available pressure node */
	if(p_dom_matrix[index_] == 0){
	  *set_ref_p_node = 1;
	  *ref_p_node_index = index_;
	  ref_p_index_found = 1;
	  /* Store reference pressure to apply in coeffs_p function */
	  *ref_p_value = s_p_value;
	  p[index_] = s_p_value;
	  p[index_-Nx] = p[index_];
	}
	I++;
      }
      K++;
    }
  }

  /* North boundary */
  if(n_ref_p_node_OK){
    
    J = Ny - 2;

    K = 2;
    while((K < Nz-2) && (!ref_p_index_found)){
      
      I = 2;
      while((I < Nx-2) && (!ref_p_index_found)){
	
	index_ = K*Nx*Ny + J*Nx + I;
	/* Search for some available pressure node */
	if(p_dom_matrix[index_] == 0){
	  *set_ref_p_node = 1;
	  *ref_p_node_index = index_;
	  ref_p_index_found = 1;
	  /* Store reference pressure node to apply in coeffs_p function */
	  *ref_p_value = n_p_value;
	  p[index_] = n_p_value;
	  p[index_+Nx] = p[index_];
	}
	I++;
      }
      K++;
    }
  }

  /* Bottom boundary */
  if(b_ref_p_node_OK){
    K = 1;

    J = 2;
    while((J < Ny-2) && (!ref_p_index_found)){

      I = 2;
      while((I < Nx-2) && (!ref_p_index_found)){

	index_ = K*Nx*Ny + J*Nx + I;
	/* Search for some available pressure node */
	if(p_dom_matrix[index_] == 0){
	  *set_ref_p_node = 1;
	  *ref_p_node_index = index_;
	  ref_p_index_found = 1;
	  /* Store reference pressure to apply in coeffs_p function */
	  *ref_p_value = b_p_value;
	  p[index_] = b_p_value;
	  p[index_-Nx*Ny] = p[index_];
	}
	I++;
      }
      J++;
    }
  }

  /* Top boundary */
  if(t_ref_p_node_OK){
    K = Nz-2;

    J = 2;
    while((J < Ny-2) && (!ref_p_index_found)){

      I = 2;
      while((I < Nx-2) && (!ref_p_index_found)){

	index_ = K*Nx*Ny + J*Nx + I;
	/* Search for some available pressure node */
	if(p_dom_matrix[index_] == 0){
	  *set_ref_p_node = 1;
	  *ref_p_node_index = index_;
	  ref_p_index_found = 1;
	  /* Store reference pressure to apply in coeffs_p function */
	  *ref_p_value = t_p_value;
	  p[index_] = t_p_value;
	  p[index_+Nx*Ny] = p[index_];
	}
	I++;
      }
      J++;
    }
  }  
  
  return ret_value;
}

/* STORE_BND_P_VALUES_3D */
/*****************************************************************************/
/**
    
   Sets:

   Array (double, NxNy|NyNz|NxNz) p_*_non_uni: values of pressure to use on next iteration
   for periodic boundary condition.

   @param data

   @return ret_value not implemented.
  
   MODIFIED: 04-10-2019
*/
void store_bnd_p_values_3D(Data_Mem *data){

  int I, J, K;
  int index_l, index_u;

  const int w_type = data->bc_flow_west.type;
  const int e_type = data->bc_flow_east.type;
  const int s_type = data->bc_flow_south.type;
  const int n_type = data->bc_flow_north.type;
  const int b_type = data->bc_flow_bottom.type;
  const int t_type = data->bc_flow_top.type;
  
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  double *p_W_non_uni = data->p_W_non_uni;
  double *p_E_non_uni = data->p_E_non_uni;
  double *p_S_non_uni = data->p_S_non_uni;
  double *p_N_non_uni = data->p_N_non_uni;
  double *p_B_non_uni = data->p_B_non_uni;
  double *p_T_non_uni = data->p_T_non_uni;

  const double *p_field = NULL;

  /* SIMPLER */
  if(pv_coupling_algorithm == 2){
    p_field = data->p;
  }
  else{
    p_field = data->p_corr;
  }
  

#pragma omp parallel sections default(none) private(I, J, K, index_l, index_u)	\
  shared(p_W_non_uni, p_E_non_uni, p_S_non_uni, p_N_non_uni, p_B_non_uni, p_T_non_uni, p_field)
  {

#pragma omp section 
    {
      /* Type = 5, Periodic boundary */
      if(w_type == 5){

	/* SIMPLER */
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_l = K*Nx*Ny + J*Nx + 2;
	    index_u = K*Nx*Ny + J*Nx + Nx-3;
	    p_W_non_uni[K*Ny + J] = 0.5 * (p_field[index_l] + p_field[index_u]);
	  }
	}
      }
    } // w section

#pragma omp section 
    {
      /* Type = 5, Periodic boundary */
      if(e_type == 5){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_l = K*Nx*Ny + J*Nx + 2;
	    index_u = K*Nx*Ny + J*Nx + Nx-3;
	    p_E_non_uni[K*Ny + J] = 0.5 * (p_field[index_l] + p_field[index_u]);
	  }
	}
      }
    } // e section

#pragma omp section 
    {
      /* Type = 5, Periodic boundary */
      if(s_type == 5){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_l = K*Nx*Ny + 2*Nx + I;
	    index_u = K*Nx*Ny + (Ny-3)*Nx + I;
	    p_S_non_uni[K*Nx + I] = 0.5 * (p_field[index_l] + p_field[index_u]);
	  }
	}
      }
    } // s section
  
#pragma omp section 
    {
      /* Type = 5, Periodic boundary */
      if(n_type == 5){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_l = K*Nx*Ny + 2*Nx + I;
	    index_u = K*Nx*Ny + (Ny-3)*Nx + I;
	    p_N_non_uni[K*Nx + I] = 0.5 * (p_field[index_l] + p_field[index_u]);
	  }
	}
      }
    } // n section

#pragma omp section
    {
      /* Type = 5, Periodic boundary */
      if(b_type == 5){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_l = 2*Nx*Ny + J*Nx + I;
	    index_u = (Nz-3)*Nx*Ny + J*Nx + I;
	    p_B_non_uni[J*Nx + I] = 0.5 * (p_field[index_l] + p_field[index_u]);
	  }
	}      
      }
    } // b section

#pragma omp section
    {
      /* Type = 5, Periodic boundary */
      if(t_type == 5){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_l = 2*Nx*Ny + J*Nx + I;
	    index_u = (Nz-3)*Nx*Ny + J*Nx + I;
	    p_T_non_uni[J*Nx + I] = 0.5 * (p_field[index_l] + p_field[index_u]);
	  }
	}
      }
    }

  } // sections
  
  return;  
}

/* SET_BC_FLOW_3D */
/*****************************************************************************/
/**

  Sets:

  Structure coeffs_u: Set values at boundary according to specified boundary condition.

  Structure coeffs_v: Set values at boundary according to specified boundary condition.

  Structure coeffs_w: Set values at boudnary according to specified boundary condition.

  @param data
  
  @return ret_value not implemented.
  
  MODIFIED: 15-11-2019
*/
void set_bc_flow_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_u, index_v, index_w;
  int index_aux;
 
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int w_type = data->bc_flow_west.type;
  const int e_type = data->bc_flow_east.type;
  const int s_type = data->bc_flow_south.type;
  const int n_type = data->bc_flow_north.type;
  const int b_type = data->bc_flow_bottom.type;
  const int t_type = data->bc_flow_top.type;
 
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;
  
  double *aP_u = data->coeffs_u.aP;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *aB_u = data->coeffs_u.aB;
  double *aT_u = data->coeffs_u.aT;
  double *b_u = data->coeffs_u.b;

  double *aP_v = data->coeffs_v.aP;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *aB_v = data->coeffs_v.aB;
  double *aT_v = data->coeffs_v.aT;
  double *b_v = data->coeffs_v.b;

  double *aP_w = data->coeffs_w.aP;
  double *aS_w = data->coeffs_w.aS;
  double *aN_w = data->coeffs_w.aN;
  double *aW_w = data->coeffs_w.aW;
  double *aE_w = data->coeffs_w.aE;
  double *aB_w = data->coeffs_w.aB;
  double *aT_w = data->coeffs_w.aT;
  double *b_w = data->coeffs_w.b;

  const double *u_E_non_uni = data->u_E_non_uni;
  const double *u_W_non_uni = data->u_W_non_uni;
  const double *v_N_non_uni = data->v_N_non_uni;
  const double *v_S_non_uni = data->v_S_non_uni;
  const double *w_T_non_uni = data->w_T_non_uni;
  const double *w_B_non_uni = data->w_B_non_uni;
  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;
  const double *p_faces_z = data->p_faces_z;
  const double *fn_u = data->fn_u;
  const double *ft_u = data->ft_u;
  const double *fe_v = data->fe_v;
  const double *ft_v = data->ft_v;
  const double *fe_w = data->fe_w;
  const double *fn_w = data->fn_w;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);
  const b_cond_flow *bc_flow_bottom = &(data->bc_flow_bottom);
  const b_cond_flow *bc_flow_top = &(data->bc_flow_top);

  /* U */

#pragma omp parallel sections default(none)				\
  private(I, J, K, i, j, k, index_u, index_v, index_w, index_aux)	\
  shared(bc_flow_west, bc_flow_east,					\
	 bc_flow_south, bc_flow_north, bc_flow_bottom,			\
	 bc_flow_top, u_dom_matrix, u_W_non_uni,			\
	 u_E_non_uni, u, v, w, p_faces_x, p_faces_y, p_faces_z,		\
	 aP_u, aW_u, aE_u, aS_u, aN_u, aB_u, aT_u, b_u, fn_u, ft_u)
  {
#pragma omp section 
    {
      /* West */
      i = 0;

      /* Type = 1, Fixed value */
      if(w_type == 1){

	/* Fixed value, parabolic */
	if(bc_flow_west->parabolize_profile_OK){
	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      aP_u[index_u] = 1;
	      b_u[index_u] = u_W_non_uni[K*Ny + J];
	    }
	  }
	}
	/* Make piston profile */	
	else{
	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      aP_u[index_u] = 1;
	      b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : bc_flow_west->v_value[0];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */
      else if((w_type == 2) || (w_type == 3)){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = 0;
	  }
	}
      }
      /* Type = 0, Zero gradient */
      else if(w_type == 0){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;    
	  
	    if(u_dom_matrix[index_u] != 0){		  
	      b_u[index_u] = 0;		  
	    }
	    else{	  
	      aE_u[index_u] = 1;
	    }	  
	  }
	}
      }
      /* Type = 4, Constant pressure */
      else if(w_type == 4){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;

	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }
	    else{
	      I = i + 1;
	      j = J - 1;
	      k = K - 1;
	      index_v = K*Nx*ny + j*Nx + I;
	      index_w = k*Nx*Ny + J*Nx + I;
	      /* Ec. 9.29 Veersteg */

	      b_u[index_u] =
		(u[index_u+1] * p_faces_x[index_u+1]
		 + v[index_v+Nx] * p_faces_y[index_v+Nx]
		 + w[index_w+Nx*Ny] * p_faces_z[index_w+Nx*Ny]
		 - v[index_v]  * p_faces_y[index_v]
		 - w[index_w] * p_faces_z[index_w]) / p_faces_x[index_u];
	    }
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(w_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;

	    /* Ec. 9.31 Veersteg */
	    b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : u[K*nx*Ny + J*nx + nx-2];
	  }
	}
      }      
    } /* West section */
  
#pragma omp section 
    {
      /* East */
      i = nx - 1;

      /* Type = 1, Fixed value */
      if(e_type == 1){

	/* Fixed value, parabolic */
	if(bc_flow_east->parabolize_profile_OK){
	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      aP_u[index_u] = 1;
	      b_u[index_u] = u_E_non_uni[K*Ny + J];
	    }
	  }
	}
	/* Make piston profile */
	else{
	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      aP_u[index_u] = 1;
	      b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : bc_flow_east->v_value[0];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */      
      else if((e_type == 2) || (e_type == 3)){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = 0;
	  }
	}
      }
      /* Type = 0, Zero gradient */      
      else if(e_type == 0){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	  
	    if(u_dom_matrix[index_u] != 0){	  
	      b_u[index_u] = 0;		  
	    }	  
	    else{
	      aW_u[index_u] = 1;	  
	    }
	  }
	}
      }
      /* Type = 4, Constant pressure */      
      else if(e_type == 4){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;

	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }
	    else{
	      I = i + 1;
	      j = J - 1;
	      k = K - 1;
	      index_v = K*Nx*ny + j*Nx + (I-1);
	      index_w = k*Nx*Ny + J*Nx + (I-1);
	      b_u[index_u] =
		(u[index_u-1] * p_faces_x[index_u-1]
		 + v[index_v] * p_faces_y[index_v]
		 + w[index_w] * p_faces_z[index_w]
		 - v[index_v+Nx] * p_faces_y[index_v+Nx]
		 - w[index_w+Nx*Ny] * p_faces_z[index_w+Nx*Ny]) / p_faces_x[index_u];
	    }
	  }
	}
      }
      /* Type = 5, Periodic boundary */      
      else if(e_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : u[K*nx*Ny + J*nx + 1];
	  }
	}
      }
    } /* East section */

#pragma omp section 
    {
      /* South */
      J = 0;
      
      /* Type = 1, Fixed value */
      if(s_type == 1){
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : bc_flow_south->v_value[0];	  
	  }
	}
      }
      /* Type = 3, Wall */
      else if(s_type == 3){
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(s_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	  
	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;		  
	    }	  
	    else{
	      index_aux = K*nx*Ny + (Ny-3)*nx + i;
	      b_u[index_u] = u[index_aux] * fn_u[index_aux] + 
		u[index_aux+nx] * (1 - fn_u[index_aux]);	  
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	  
	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;		  
	    }	  
	    else{
	      aN_u[index_u] = 1;	  
	    }
	  }
	}
      }
    } /* South section */

#pragma omp section 
    {
      /* North */
      J = Ny - 1;

      /* Type = 1, Fixed value */      
      if(n_type == 1){
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : bc_flow_north->v_value[0];	  
	  }
	}
      }
      /* Type = 3, Wall */      
      else if(n_type == 3){
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */      
      else if(n_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	  
	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;		  
	    }	  
	    else{
	      index_aux = K*nx*Ny + nx + i;
	      b_u[index_u] = u[index_aux] * fn_u[index_aux] + 
		u[index_aux+nx] * (1 - fn_u[index_aux]);	  
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/      
      else{
	for(K=1; K<Nz-1; K++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	  
	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;		  
	    }
	  
	    else{		  
	      aS_u[index_u] = 1;	  
	    }
	  }
	}
      }
    } /* North section */

#pragma omp section
    {
      /* Bottom */
      K = 0;

      /* Type = 1, Fixed value */
      if(b_type == 1){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : bc_flow_bottom->v_value[0];
	  }
	}
      }
      /* Type = 3, Wall */
      else if(b_type == 3){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(b_type == 5){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;

	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }
	    else{
	      index_aux = (Nz-3)*nx*Ny + J*nx + i;
	      b_u[index_u] = u[index_aux] * ft_u[index_aux] +
		u[index_aux+nx*Ny] * (1 - ft_u[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;

	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }
	    else{
	      aT_u[index_u] = 1;
	    }
	  }
	}
      }      
    } /* Bottom section */

#pragma omp section
    {
      /* Top */
      K = Nz - 1;

      /* Type = 1, Fixed value */
      if(t_type == 1){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = (u_dom_matrix[index_u] != 0)? 0 : bc_flow_top->v_value[0];
	  }
	}
      }
      /* Type = 3, Wall */
      else if(t_type == 3){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;
	    b_u[index_u] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(t_type == 5){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;

	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }
	    else{
	      index_aux = nx*Ny + J*nx + i;
	      b_u[index_u] = u[index_aux] * ft_u[index_aux] +
		u[index_u+nx*Ny] * (1 - ft_u[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	    index_u = K*nx*Ny + J*nx + i;
	    aP_u[index_u] = 1;

	    if(u_dom_matrix[index_u] != 0){
	      b_u[index_u] = 0;
	    }
	    else{
	      aB_u[index_u] = 1;
	    }
	  }
	}
      }
    } /* Top section */
    
  } /* sections */


  /* V */

#pragma omp parallel sections default(none) private(I, J, K, i, j, k, index_u, index_v, index_w, index_aux) \
  shared(bc_flow_west, bc_flow_east, bc_flow_south, bc_flow_north, bc_flow_bottom, bc_flow_top, v_dom_matrix, v_S_non_uni, \
	 v_N_non_uni, u, v, w, p_faces_x, p_faces_y, p_faces_z, aP_v, aW_v, aE_v, aS_v, aN_v, aT_v, aB_v, b_v, fe_v, ft_v)
  {
#pragma omp section 
    {
      /* West */
      I = 0;

      /* Type = 1, Fixed value */      
      if(w_type == 1){
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : bc_flow_west->v_value[1];	  
	  }
	}
      }
      /* Type = 3, Wall */      
      else if(w_type == 3){
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */      
      else if(w_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;

	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      index_aux = K*Nx*ny + j*Nx + Nx-3;
	      b_v[index_v] = v[index_aux] * fe_v[index_aux] +
		v[index_aux+1] * (1 - fe_v[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/      
      else{
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	  
	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;		  
	    }	  
	    else{		  
	      aE_v[index_v] = 1;	  
	    }
	  }
	}
      }
    } /* West section */

#pragma omp section 
    {
      /* East */
      I = Nx - 1;

      /* Type = 1, Fixed value */            
      if(e_type == 1){
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : bc_flow_east->v_value[1];
	  }
	}
      }
      /* Type = 3, Wall */            
      else if(e_type == 3){
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */            
      else if(e_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;

	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      index_aux = K*Nx*ny + j*Nx + 1;
	      b_v[index_v] = v[index_aux] * fe_v[index_aux] +
		v[index_aux+1] * (1 - fe_v[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/            
      else{
	for(K=1; K<Nz-1; K++){
	  for(j=1; j<ny-1; j++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	  
	    if(v_dom_matrix[index_v] != 0){		 
	      b_v[index_v] = 0;		 
	    }	  
	    else{
	      aW_v[index_v] = 1;	  
	    }
	  }
	}
      }
    } /* East section */   

#pragma omp section 
    {
      /* South */
      j = 0;

      /* Type = 1, Fixed value */      
      if(s_type == 1){

	if(bc_flow_south->parabolize_profile_OK){
	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      aP_v[index_v] = 1;
	      b_v[index_v] = v_S_non_uni[K*Nx + I];
	    }
	  }
	}
	/* Make piston profile */
	else{
	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      aP_v[index_v] = 1;
	      b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : bc_flow_south->v_value[1];	   
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */      
      else if((s_type == 2) || (s_type == 3)){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = 0;
	  }
	}
      }
      /* Type = 0, Zero gradient */      
      else if(s_type == 0){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	  
	    if(v_dom_matrix[index_v] != 0){		  
	      b_v[index_v] = 0;		  
	    }
	  
	    else{		  
	      aN_v[index_v] = 1;	  
	    }
	  }
	}
      }
      /* Type = 4, Constant pressure */      
      else if(s_type == 4){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      i = I - 1;
	      J = j + 1;	      
	      k = K - 1;
	      index_u = K*nx*Ny + J*nx + i;
	      index_w = k*Nx*Ny + J*Nx + I;
	      b_v[index_v] =
		(u[index_u+1] * p_faces_x[index_u+1]
		 + v[index_v+Nx] * p_faces_y[index_v+Nx]
		 + w[index_w+Nx*Ny] * p_faces_z[index_w+Nx*Ny]
		 - u[index_u] * p_faces_x[index_u]
		 - w[index_w] * p_faces_z[index_w]) / p_faces_y[index_v];
	    }
	  }
	}
      }
      /* Type = 5, Periodic boundary */      
      else if(s_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : v[K*Nx*ny + (ny-2) * Nx + I];
	  }
	}
      }
    } /* South section*/

#pragma omp section 
    {
      /* North */
      j = ny-1;

      /* Type = 1, Fixed value */      
      if(n_type == 1){

	/* Fixed value, parabolic */
	if(bc_flow_north->parabolize_profile_OK){
	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      aP_v[index_v] = 1;
	      b_v[index_v] = v_N_non_uni[K*Nx + I];
	    }
	  }
	}
	/* Make piston profile */
	else{
	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      aP_v[index_v] = 1;
	      b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : bc_flow_north->v_value[1];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */            
      else if((n_type == 2) || (n_type == 3)){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = 0;
	  }
	}
      }
      /* Type = 0, Zero gradient */            
      else if(n_type == 0){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	  
	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;		  
	    }
	    else{		  
	      aS_v[index_v] = 1;	  
	    }
	  }
	}
      }
      /* Type = 4, Constant pressure */            
      else if(n_type == 4){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      i = I - 1;
	      J = j + 1;
	      k = K - 1;
	      index_u = K*nx*Ny + (J-1)*nx + i;
	      index_w = k*Nx*Ny + (J-1)*Nx + I;
	      b_v[index_v] =
		(u[index_u] * p_faces_x[index_u]
		 + v[index_v-Nx] * p_faces_y[index_v-Nx]
		 + w[index_w] * p_faces_z[index_w]
		 - u[index_u+1] * p_faces_x[index_u+1]
		 - w[index_w+Nx*Ny] * p_faces_z[index_w+Nx*Ny]) / p_faces_y[index_v];
	    }
	  }
	}
      }
      /* Type = 5, Periodic boundary */            
      else if(n_type == 5){
	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : v[K*Nx*ny + 1*Nx + I];
	  }
	}
      }
    } /* North section */

#pragma omp section
    {
      /* Bottom */
      K = 0;

      /* Type = 1, Fixed value */
      if(b_type == 1){
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : bc_flow_bottom->v_value[1];
	  }
	}
      }
      /* Type = 3, Wall */
      else if(b_type == 3){
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(b_type == 5){
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    
	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      index_aux = (Nz-3)*Nx*ny + j*Nx + I;
	      b_v[index_v] = v[index_aux] * ft_v[index_aux] +
		v[index_aux+Nx*ny] * (1 - ft_v[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;

	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      aT_v[index_v] = 1;
	    }
	  }
	}
      }
    } /* Bottom section */

#pragma omp section
    {
      /* Top */
      K = Nz - 1;

      /* Type = 1, Fixed value */
      if(t_type == 1){
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = (v_dom_matrix[index_v] != 0)? 0 : bc_flow_top->v_value[1];
	  }
	}
      }
      /* Type = 3, Wall */
      else if(t_type == 3){
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;
	    b_v[index_v] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(t_type == 5){
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;

	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      index_aux = Nx*ny + j*Nx + I;
	      b_v[index_v] = v[index_aux] * ft_v[index_aux] +
		v[index_aux+Nx*ny] * (1 - ft_v[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(j=1; j<ny-1; j++){
	  for(I=1; I<Nx-1; I++){
	    index_v = K*Nx*ny + j*Nx + I;
	    aP_v[index_v] = 1;

	    if(v_dom_matrix[index_v] != 0){
	      b_v[index_v] = 0;
	    }
	    else{
	      aB_v[index_v] = 1;
	    }
	  }
	}
      }
    } /* Top section */    
    
  } /* sections */

  /* W */

#pragma omp parallel sections default(none) private(I, J, K, i, j, k, index_u, index_v, index_w, index_aux) \
  shared(bc_flow_west, bc_flow_east, bc_flow_south, bc_flow_north, bc_flow_bottom, bc_flow_top, w_dom_matrix, w_B_non_uni, \
	 w_T_non_uni, u, v, w, p_faces_x, p_faces_y, p_faces_z, aP_w, aW_w, aE_w, aS_w, aN_w, aT_w, aB_w, b_w, fe_w, fn_w)

  {

#pragma omp section
    {
      /* West */
      I = 0;

      /* Type = 1, Fixed value */
      if(w_type == 1){
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : bc_flow_west->v_value[2];
	  }
	}
      }
      /* Type = 3, Wall */
      else if(w_type == 3){
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(w_type == 5){
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      index_aux = k*Nx*Ny + J*Nx + Nx-3;
	      b_w[index_w] = w[index_aux] * fe_w[index_aux] +
		w[index_aux+1] * (1 - fe_w[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      aE_w[index_w] = 1;
	    }
	  }
	}
      }      
    } /* West section */

#pragma omp section
    {
      /* East */
      I = Nx - 1;

      /* Type = 1, Fixed value */
      if(e_type == 1){
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : bc_flow_east->v_value[2];
	  }
	}
      }
       /* Type = 3, Wall */
      else if(e_type == 3){
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(e_type == 5){
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      index_aux = k*Nx*Ny + J*Nx + 1;
	      b_w[index_w] = w[index_aux] * fe_w[index_aux] +
		w[index_aux+1] * (1 - fe_w[index_aux]);
	    }	    
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(k=1; k<nz-1; k++){
	  for(J=1; J<Ny-1; J++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      aW_w[index_w] = 1;
	    }
	  }
	}
      }      
    } /* East section */

#pragma omp section
    {

      /* South */
      J = 0;

      /* Type = 1, Fixed value */
      if(s_type == 1){
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : bc_flow_south->v_value[2];
	  }
	}
      }
      /* Type = 3, Wall */
      else if(s_type == 3){
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(s_type == 5){
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      index_aux = k*Nx*Ny + (Ny-3)*Nx + I;
	      b_w[index_w] = w[index_aux] * fn_w[index_aux] +
		w[index_aux+Nx] * (1 - fn_w[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      aN_w[index_w] = 1;
	    }
	  }
	}
      }      
    } /* South section */

#pragma omp section
    {
      /* North */
      J = Ny - 1;

      /* Type = 1, Fixed value */
      if(n_type == 1){
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : bc_flow_north->v_value[2];
	  }
	}
      }
      /* Type = 3, Wall */
      else if(n_type == 3){
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(n_type == 5){
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      index_aux = k*Nx*Ny + Nx + I;
	      b_w[index_w] = w[index_aux] * fn_w[index_aux] +
		w[index_aux+Nx] * (1 - fn_w[index_aux]);
	    }
	  }
	}
      }
      /* Type = 0 OR 2 OR 4*/
      else{
	for(k=1; k<nz-1; k++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      aS_w[index_w] = 1;
	    }
	  }	  
	}
      }      
    } /* North section */

#pragma omp section
    {
      /* Bottom */
      k = 0;

      /* Type = 1, Fixed value */
      if(b_type == 1){

	/* Fixed value, parabolic */
	if(bc_flow_bottom->parabolize_profile_OK){
	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      aP_w[index_w] = 1;
	      b_w[index_w] = w_B_non_uni[J*Nx + I];
	    }
	  }
	}
	/* Make piston profile */		
	else{
	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      aP_w[index_w] = 1;
	      b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : bc_flow_bottom->v_value[2];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */
      else if((b_type == 2) || (b_type == 3)){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = 0;
	  }
	}
      }
      /* Type = 0, Zero gradient */
      else if(b_type == 0){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      aT_w[index_w] = 1;
	    }
	  }
	}
      }
      /* Type = 4, Constant pressure */
      else if(b_type == 4){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      i = I - 1;
	      j = J - 1;
	      K = k + 1;
	      index_u = K*nx*Ny + J*nx + i;
	      index_v = K*Nx*ny + j*Nx + I;

	      b_w[index_w] =
		(u[index_u+1] * p_faces_x[index_u+1]
		 + v[index_v+Nx] * p_faces_y[index_v+Nx]
		 + w[index_w+Nx*Ny] * p_faces_z[index_w+Nx*Ny]
		 - u[index_u] * p_faces_x[index_u]
		 - v[index_v] * p_faces_y[index_v]) / p_faces_z[index_w];
	    }
	  }
	}	  
      }
      /* Type = 5, Periodic boundary */
      else if(b_type == 5){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : w[(nz-2)*Nx*Ny + J*Nx + I];
	  }
	}
      }     
    } /* Bottom section */

#pragma omp section
    {
      /* Top */
      k = nz - 1;

      /* Type = 1, Fixed value */
      if(t_type == 1){

	/* Fixed value, parabolic */
	if(bc_flow_top->parabolize_profile_OK){
	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      aP_w[index_w] = 1;
	      b_w[index_w] = w_T_non_uni[J*Nx + I];
	    }
	  }
	}
	/* Make piston profile */
	else{
	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      aP_w[index_w] = 1;
	      b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : bc_flow_top->v_value[2];
	    }
	  }
	}
      }
      /* Type = 2, Symmetry. Type = 3, Wall. */
      else if((t_type == 2) || (t_type == 3)){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = 0;
	  }
	}
      }
      /* Type = 0, Zero gradient */
      else if(t_type == 0){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;
	    }
	    else{
	      aB_w[index_w] = 1;
	    }
	  }
	}
      }
      /* Type = 4, Constant pressure */
      else if(t_type == 4){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;

	    if(w_dom_matrix[index_w] != 0){
	      b_w[index_w] = 0;	      
	    }
	    else{
	      i = I - 1;
	      j = J - 1;
	      K = k + 1;
	      index_u = (K-1)*nx*Ny + J*nx + i;
	      index_v = (K-1)*Nx*ny + j*Nx + I;
	      b_w[index_w] =
		(u[index_u] * p_faces_x[index_u]
		 + v[index_v] * p_faces_y[index_v]
		 + w[index_w-Nx*Ny] * p_faces_z[index_w-Nx*Ny]
		 - u[index_u+1] * p_faces_x[index_u+1]
		 - v[index_v+Nx] * p_faces_y[index_v+Nx]) / p_faces_z[index_w];
	    }
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(t_type == 5){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_w = k*Nx*Ny + J*Nx + I;
	    aP_w[index_w] = 1;
	    b_w[index_w] = (w_dom_matrix[index_w] != 0)? 0 : w[Nx*Ny + J*Nx + I];
	  }
	}
      }      
    } /* Top section */
    
  } /* sections */

  
    /* Hack para BL condition */
  /* Type = -1, BL */
  if(w_type == -1 || e_type == -1){
    make_bl_profile(data);
  }
    
  return;
}

/* COEFFS_FLOW_P_3D */
/*****************************************************************************/
/**

  Sets:

  Structure coeffs_p: coefficients of pressure or pressure correction equation.

  @param data, use_ps_velocities_OK

  @return ret_value not implemented.
  
  MODIFIED: 02-01-2020
*/
void coeffs_flow_p_3D(Data_Mem *data, const int use_ps_velocities_OK){

  int i, j, k, I, J, K;
  int index_, index_u, index_v, index_w;
  int use_p_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int set_ref_p_node = data->set_ref_p_node;
  const int ref_p_node_index = data->ref_p_node_index;
  const int w_type = data->bc_flow_west.type;
  const int e_type = data->bc_flow_east.type;
  const int s_type = data->bc_flow_south.type;
  const int n_type = data->bc_flow_north.type;
  const int b_type = data->bc_flow_bottom.type;
  const int t_type = data->bc_flow_top.type;

  const int *p_dom_matrix = data->p_dom_matrix;

  double w_p_value, e_p_value, s_p_value;
  double n_p_value, b_p_value, t_p_value; 

  const double rho = data->rho_v[0];
  const double alpha_p_coeff = data->alpha_p_coeff;
  const double ref_p_value = data->ref_p_value;  

  double *p_aP = data->coeffs_p.aP;
  double *p_aW = data->coeffs_p.aW;
  double *p_aE = data->coeffs_p.aE;
  double *p_aS = data->coeffs_p.aS;
  double *p_aN = data->coeffs_p.aN;
  double *p_aB = data->coeffs_p.aB;
  double *p_aT = data->coeffs_p.aT;
  double *p_b = data->coeffs_p.b;
  double *u_aP_SIMPLEC = data->u_aP_SIMPLEC;
  double *v_aP_SIMPLEC = data->v_aP_SIMPLEC;
  double *w_aP_SIMPLEC = data->w_aP_SIMPLEC;

  const double *u_aP = data->coeffs_u.aP;
  const double *u_aE = data->coeffs_u.aE;
  const double *u_aW = data->coeffs_u.aW;
  const double *u_aN = data->coeffs_u.aN;
  const double *u_aS = data->coeffs_u.aS;
  const double *u_aT = data->coeffs_u.aT;
  const double *u_aB = data->coeffs_u.aB;  

  const double *v_aP = data->coeffs_v.aP;
  const double *v_aE = data->coeffs_v.aE;
  const double *v_aW = data->coeffs_v.aW;
  const double *v_aN = data->coeffs_v.aN;
  const double *v_aS = data->coeffs_v.aS;
  const double *v_aT = data->coeffs_v.aT;
  const double *v_aB = data->coeffs_v.aB;

  const double *w_aP = data->coeffs_w.aP;
  const double *w_aE = data->coeffs_w.aE;
  const double *w_aW = data->coeffs_w.aW;
  const double *w_aN = data->coeffs_w.aN;
  const double *w_aS = data->coeffs_w.aS;
  const double *w_aT = data->coeffs_w.aT;
  const double *w_aB = data->coeffs_w.aB;

  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;  

  const double *u_ps = data->u_ps;
  const double *v_ps = data->v_ps;
  const double *w_ps = data->w_ps;  

  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;
  const double *p_faces_z = data->p_faces_z;  

  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *w_faces_z = data->w_faces_z;  

  const double *p_W_non_uni = data->p_W_non_uni;
  const double *p_E_non_uni = data->p_E_non_uni;
  const double *p_S_non_uni = data->p_S_non_uni;
  const double *p_N_non_uni = data->p_N_non_uni;
  const double *p_B_non_uni = data->p_B_non_uni;
  const double *p_T_non_uni = data->p_T_non_uni;  

  /* SIMPLER: p_value. SIMPLE, SIMPLE_CANG, SIMPLEC: correction equal to zero */    

  if(pv_coupling_algorithm == 2){
  
    w_p_value = data->bc_flow_west.p_value;
    e_p_value = data->bc_flow_east.p_value;
    s_p_value = data->bc_flow_south.p_value;
    n_p_value = data->bc_flow_north.p_value;
    b_p_value = data->bc_flow_bottom.p_value;
    t_p_value = data->bc_flow_top.p_value;
  }
  else{
    w_p_value = 0;
    e_p_value = 0;
    s_p_value = 0;
    n_p_value = 0;
    b_p_value = 0;
    t_p_value = 0;    
  }

  /* SIMPLEC */
  if(pv_coupling_algorithm == 3){

#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(u_aP_SIMPLEC, u_aP, u_aE, u_aW, u_aN, u_aS, u_aT, u_aB)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	
	  index_u = K*nx*Ny + J*nx + i;

	  u_aP_SIMPLEC[index_u] = u_aP[index_u] - 
	    (u_aE[index_u] + u_aW[index_u] + 
	     u_aN[index_u] + u_aS[index_u] +
	     u_aT[index_u] + u_aB[index_u]);
	}
      }
    }
    
#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(v_aP_SIMPLEC, v_aP, v_aE, v_aW, v_aN, v_aS, v_aT, v_aB)

    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	
	  index_v = K*Nx*ny + j*Nx + I;
	
	  v_aP_SIMPLEC[index_v] = v_aP[index_v] - 
	    (v_aE[index_v] + v_aW[index_v] + 
	     v_aN[index_v] + v_aS[index_v] +
	     v_aT[index_v] + v_aB[index_v]);
	
	}
      }
    }

#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(w_aP_SIMPLEC, w_aP, w_aE, w_aW, w_aN, w_aS, w_aT, w_aB)

    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_w = k*Nx*Ny + J*Nx + I;

	  w_aP_SIMPLEC[index_w] = w_aP[index_w] -
	    (w_aE[index_w] + w_aW[index_w] +
	     w_aN[index_w] + w_aS[index_w] +
	     w_aT[index_w] + w_aB[index_w]);
	}
      }
    }
    
    /* Switching the pointer to the modified coefficients */
    u_aP = u_aP_SIMPLEC;
    v_aP = v_aP_SIMPLEC;
    w_aP = w_aP_SIMPLEC;

  }

  /* Calculating source term for SIMPLER or SIMPLE algorithm */

  /* SIMPLER */
  if(use_ps_velocities_OK){

#pragma omp parallel for default(none) private(I, J, K, i, j, k, index_, index_u, index_v, index_w) \
  shared(p_dom_matrix, p_b, p_faces_x, p_faces_y, p_faces_z, u_ps, v_ps, w_ps)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  if(p_dom_matrix[index_] == 0){

	    i = I - 1;
	    j = J - 1;
	    k = K - 1;	  
	    index_u = K*nx*Ny + J*nx + i;
	    index_v = K*Nx*ny + j*Nx + I;
	    index_w = k*Nx*Ny + J*Nx + I;
	    
	    p_b[index_] = 
	      rho * (p_faces_x[index_u] * u_ps[index_u] - 
		     p_faces_x[index_u+1] * u_ps[index_u+1] + 
		     p_faces_y[index_v] * v_ps[index_v] - 
		     p_faces_y[index_v+Nx] * v_ps[index_v+Nx] +
		     p_faces_z[index_w] * w_ps[index_w] -
		     p_faces_z[index_w+Nx*Ny] * w_ps[index_w+Nx*Ny]);
	  
	  }
	}
      }
    }
	  
  }
  else{
    /* SIMPLE */

#pragma omp parallel for default(none) private(I, J, K, i, j, k, index_, index_u, index_v, index_w) \
  shared(p_dom_matrix, p_b, p_faces_x, p_faces_y, p_faces_z, u, v, w)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;
	
	  if(p_dom_matrix[index_] == 0){

	    i = I - 1;
	    j = J - 1;
	    k = K - 1;
	    index_u = K*nx*Ny + J*nx + i;
	    index_v = K*Nx*ny + j*Nx + I;
	    index_w = k*Nx*Ny + J*Nx + I;

	    p_b[index_] = 
	      rho * (p_faces_x[index_u] * u[index_u] - 
		     p_faces_x[index_u+1] * u[index_u+1] +
		     p_faces_y[index_v] * v[index_v] - 
		     p_faces_y[index_v+Nx] * v[index_v+Nx] +
		     p_faces_z[index_w] * w[index_w] -
		     p_faces_z[index_w+Nx*Ny] * w[index_w+Nx*Ny]);

	  }
	}
      }
    }
    
  }

  /* COMMON */
  
#pragma omp parallel for default(none) private(I, J, K, i, j, k, index_, index_u, index_v, index_w) \
  shared(p_dom_matrix, p_aE, p_aW, p_aN, p_aS, p_aT, p_aB, p_b, p_aP, p_faces_x, p_faces_y, p_faces_z, \
	 u_faces_x, v_faces_y, w_faces_z, u_aP, v_aP, w_aP)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
      
	if(p_dom_matrix[index_] == 0){

	  i = I - 1;
	  j = J - 1;
	  k = K - 1;

	  index_u = K*nx*Ny + J*nx + i;
	  index_v = K*Nx*ny + j*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
		
	  p_aE[index_] = p_faces_x[index_u+1] * rho * 
	    u_faces_x[index_+1] / u_aP[index_u+1];
	  p_aW[index_] = p_faces_x[index_u] * rho * 
	    u_faces_x[index_-1] / u_aP[index_u];
	
	  p_aN[index_] = p_faces_y[index_v+Nx] * rho * 
	    v_faces_y[index_+Nx] / v_aP[index_v+Nx];
	  p_aS[index_] = p_faces_y[index_v] * rho * 
	    v_faces_y[index_-Nx] / v_aP[index_v];

	  p_aT[index_] = p_faces_z[index_w+Nx*Ny] * rho *
	    w_faces_z[index_+Nx*Ny] / w_aP[index_w+Nx*Ny];
	  p_aB[index_] = p_faces_z[index_w] * rho *
	    w_faces_z[index_-Nx*Ny] / w_aP[index_w];

	  p_aP[index_] = 
	    p_faces_x[index_u+1] * rho * u_faces_x[index_] / u_aP[index_u+1] +
	    p_faces_x[index_u] * rho * u_faces_x[index_] / u_aP[index_u] +
	    p_faces_y[index_v+Nx] * rho * v_faces_y[index_] / v_aP[index_v+Nx] +
	    p_faces_y[index_v] * rho * v_faces_y[index_] / v_aP[index_v] +
	    p_faces_z[index_w+Nx*Ny] * rho * w_faces_z[index_] / w_aP[index_w+Nx*Ny] +
	    p_faces_z[index_w] * rho * w_faces_z[index_] / w_aP[index_w];
	  	
	}
	else{		  
	  p_aP[index_] = 1;
	  p_b[index_] = 0;	
	}      
      }
    }
  }

  /* Here boundaries per se */
#pragma omp parallel sections default(none) private(I, J, K, index_)	\
  shared(p_dom_matrix, p_aP, p_aW, p_aE, p_aS, p_aN, p_aB, p_aT, p_b)
 
  {

#pragma omp section 
    {
      /* West */
      I = 0;

      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  p_aP[index_] = 1;
	  if(p_dom_matrix[index_+1] == 0){
	    p_aE[index_] = 1;
	  }
	  else{
	    p_b[index_] = 0;
	  }
	}
      }
    }

#pragma omp section 
    {
      /* East */
      I = Nx - 1;

      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  p_aP[index_] = 1;
	  if(p_dom_matrix[index_-1] == 0){
	    p_aW[index_] = 1;
	  }
	  else{
	    p_b[index_] = 0;
	  }
	}
      }
    }    

#pragma omp section 
    {
      /* South */
      J = 0;

      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  p_aP[index_] = 1;	
	  if(p_dom_matrix[index_+Nx] == 0){
	    p_aN[index_] = 1;
	  }
	  else{
	    p_b[index_] = 0;
	  }
	}
      }
    }

#pragma omp section 
    {
      /* North */
      J = Ny - 1;

      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  p_aP[index_] = 1;
	  if(p_dom_matrix[index_-Nx] == 0){
	    p_aS[index_] = 1;
	  }
	  else{
	    p_b[index_] = 0;
	  }
	}
      }
    }

#pragma omp section
    {
      /* Bottom */
      K = 0;

      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  p_aP[index_] = 1;
	  if(p_dom_matrix[index_+Nx*Ny] == 0){
	    p_aT[index_] = 1;
	  }
	  else{
	    p_b[index_] = 0;
	  }
	}
      }
    }

#pragma omp section
    {
      /* Top */
      K = Nz - 1;

      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  p_aP[index_] = 1;
	  if(p_dom_matrix[index_-Nx*Ny] == 0){
	    p_aB[index_] = 1;	    
	  }
	  else{
	    p_b[index_] = 0;
	  }
	}
      }
    }
    
  } /* sections */


  /* Nodes next to boundaries: First solve faces with boundary
     conditions for velocity values only */
  
  /* Nodes next to West boundary */
  I = 1;
  i = I - 1;

  if(w_type < 4){
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(p_dom_matrix[index_] == 0){
	  index_u = K*nx*Ny + J*nx + i;
	  p_aP[index_] -= p_faces_x[index_u] * rho * 
	    u_faces_x[index_] / u_aP[index_u];
	  p_aW[index_] = 0;
	}
	else{
	  p_aP[index_] = 1;
	  p_aW[index_] = 0;
	  p_aE[index_] = 0;
	  p_aS[index_] = 0;
	  p_aN[index_] = 0;
	  p_aB[index_] = 0;
	  p_aT[index_] = 0;
	  p_b[index_] = 0;
	}
      }
    }
  }

  /* Nodes next to East boundary */
  I = Nx - 2;
  i = I - 1;

  if(e_type < 4){
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(p_dom_matrix[index_] == 0){
	  index_u = K*nx*Ny + J*nx + i;
	  p_aP[index_] -= p_faces_x[index_u+1] * rho * 
	    u_faces_x[index_] / u_aP[index_u+1];
	  p_aE[index_] = 0;
	}
	else{
	  p_aP[index_] = 1;
	  p_aW[index_] = 0;
	  p_aE[index_] = 0;
	  p_aS[index_] = 0;
	  p_aN[index_] = 0;
	  p_aB[index_] = 0;
	  p_aT[index_] = 0;
	  p_b[index_] = 0;
	}
      }
    }
  }

  
  /* Nodes nexto to South boundary */
  J = 1;
  j = J - 1;
  
  if(s_type < 4){
    
    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(p_dom_matrix[index_] == 0){
	  index_v = K*Nx*ny + j*Nx + I;
	  p_aP[index_] -= p_faces_y[index_v] * rho * 
	    v_faces_y[index_] / v_aP[index_v];
	  p_aS[index_] = 0;
	}
	/* Pressure node is not solved */
	else{
	  p_aP[index_] = 1;
	  p_aW[index_] = 0;
	  p_aE[index_] = 0;
	  p_aS[index_] = 0;
	  p_aN[index_] = 0;
	  p_aB[index_] = 0;
	  p_aT[index_] = 0;
	  p_b[index_] = 0;
	}
      }
    }
  }
  
  /* Nodes next to North boundary */
  J = Ny - 2;
  j = J - 1;


  if(n_type < 4){
    
    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(p_dom_matrix[index_] == 0){
	  index_v = K*Nx*ny + j*Nx + I;
	  p_aP[index_] -= p_faces_y[index_v+Nx] * rho * 
	    v_faces_y[index_] / v_aP[index_v+Nx];
	  p_aN[index_] = 0;
	}
	else{
	  p_aP[index_] = 1;
	  p_aW[index_] = 0;
	  p_aE[index_] = 0;
	  p_aS[index_] = 0;
	  p_aN[index_] = 0;
	  p_aB[index_] = 0;
	  p_aT[index_] = 0;
	  p_b[index_] = 0;
	}
      }
    }
  }


  /* Node next to Bottom boundary */
  K = 1;
  k = K - 1;

  if(b_type < 4){
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(p_dom_matrix[index_] == 0){
	  index_w = k*Nx*Ny + J*Nx + I;
	  p_aP[index_] -= p_faces_z[index_w] * rho *
	    w_faces_z[index_] / w_aP[index_w];
	  p_aB[index_] = 0;
	}
	/* Pressure node is not solved */
	else{
	  p_aP[index_] = 1;
	  p_aW[index_] = 0;
	  p_aE[index_] = 0;
	  p_aS[index_] = 0;
	  p_aN[index_] = 0;
	  p_aB[index_] = 0;
	  p_aT[index_] = 0;
	  p_b[index_] = 0;
	}
      }
    }
  }

  /* Nodes next to Top boundary */
  K = Nz - 2;
  k = K - 1;

  if(t_type < 4){
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(p_dom_matrix[index_] == 0){
	  
	  index_w = k*Nx*Ny + J*Nx + I;
	  p_aP[index_] -= p_faces_z[index_w+Nx*Ny] * rho *
	    w_faces_z[index_] / w_aP[index_w+Nx*Ny];
	  p_aT[index_] = 0;
	}
	else{
	  p_aP[index_] = 1;
	  p_aW[index_] = 0;
	  p_aE[index_] = 0;
	  p_aS[index_] = 0;
	  p_aN[index_] = 0;
	  p_aB[index_] = 0;
	  p_aT[index_] = 0;
	  p_b[index_] = 0;
	}
      }	
    }    
  }

  /* Nodes next to boundaries: solve faces with boundary
     conditions for pressure, these override possible modifications 
     to p_aP on edges due to neighboring faces */

  /* Nodes next to West boundary */
  I = 1;
  i = I - 1;

  /* Constant pressure boundary condition */
  if(w_type == 4){
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? w_p_value : 0;
      }
    }
  }
  /* Periodic boundary */  
  else if(w_type == 5){
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? p_W_non_uni[K*Ny + J] : 0;
      }
    }
  }

  /* Nodes next to East boundary */
  I = Nx - 2;
  i = I - 1;

  /* Constant pressure boundary condition */    
  if(e_type == 4){
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? e_p_value : 0;     
      }
    }
  }
  /* Periodic boundary */    
  else if(e_type == 5){
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? p_E_non_uni[K*Ny + J] : 0;
      }
    }
  }

  /* Nodes nexto to South boundary */
  J = 1;
  j = J - 1;

   /* Constant pressure boundary condition */
  if(s_type == 4){
  
    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? s_p_value : 0;
      }
    }
  }
  /* Periodic boundary */
  else if(s_type == 5){
    
    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? p_S_non_uni[K*Nx + I] : 0;
      }
    }
  } 

  /* Nodes next to North boundary */
  J = Ny - 2;
  j = J - 1;

    /* Constant pressure boundary condition */  
  if(n_type == 4){

    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? n_p_value : 0;
      }
    }
  }
  /* Periodic boundary */  
  else if(n_type == 5){
    
    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? p_N_non_uni[K*Nx + I] : 0;
      }
    }
  }

  /* Node next to Bottom boundary */
  K = 1;
  k = K - 1;

    /* Constant pressure boundary condition */
  if(b_type == 4){
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? b_p_value : 0;
      }
    }
  }
  /* Periodic boundary */
  else if(b_type == 5){
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? p_B_non_uni[J*Nx + I] : 0;
      }
    }    
  }

  /* Nodes next to Top boundary */
  K = Nz - 2;
  k = K - 1;

  /* Constant pressure boundary condition */
  if(t_type == 4){

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? t_p_value : 0;
      }
    }
  }
  /* Periodic boundary */
  else if(t_type == 5){
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p_aP[index_] = 1;
	p_aW[index_] = 0;
	p_aE[index_] = 0;
	p_aS[index_] = 0;
	p_aN[index_] = 0;
	p_aB[index_] = 0;
	p_aT[index_] = 0;
	p_b[index_] = (p_dom_matrix[index_] == 0)? p_T_non_uni[J*Nx + I] : 0;
      }
    }
  }  

  /* Check for a reference pressure node 
     index of the reference pressure node identified on set_p_ref_node function */

  if(set_ref_p_node){
    p_aP[ref_p_node_index] = 1;
    p_aW[ref_p_node_index] = 0;
    p_aE[ref_p_node_index] = 0;
    p_aS[ref_p_node_index] = 0;
    p_aN[ref_p_node_index] = 0;
    p_aB[ref_p_node_index] = 0;
    p_aT[ref_p_node_index] = 0;
    /* SIMPLER */
    if(pv_coupling_algorithm == 2){
      p_b[ref_p_node_index] = ref_p_value;
    }
    /* SIMPLE, SIMPLE_CANG, SIMPLEC */
    else{
      p_b[ref_p_node_index] = 0;
    }
  }

  /* Here we relax coefficients with alpha_p in the same way
     that with other coefficients */
  
  if(alpha_p_coeff != 1){

    if(pv_coupling_algorithm == 1){
      /* We are using SIMPLE_cang
	 set relaxation for pcorr */
      use_p_OK = 0;
    }
    else if(pv_coupling_algorithm == 2){
      /* We are using SIMPLER
	 set relaxation for p or pcorr alternatively
	 by using flag use_ps_velocities_OK */
      use_p_OK = use_ps_velocities_OK;
    }

    if(use_p_OK){
      relax_coeffs_3D(data->p, p_dom_matrix, 0, data->coeffs_p, Nx, Ny, Nz, data->alpha_p_coeff);
    }
    else{
      relax_coeffs_3D(data->p_corr, p_dom_matrix, 0, data->coeffs_p, Nx, Ny, Nz, data->alpha_p_coeff);
    }
  }

  return;
  
}

/* COMPUTE_PSEUDO_VELOCITES_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (nx, Ny, Nz) u_ps: pseudo u velocities for SIMPLER algorithm.

  Array (Nx, ny, Nz) v_ps: pseudo v velocities for SIMPLER algorithm.

  Array (Nx, Ny, nz) w_ps: pseudo w velocities for SIMPLER algorithm.

  @param data
  
  @return ret_value not implemented.
  
  MODIFIED: 21-11-2019
*/
void compute_pseudo_velocities_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  
  double *u_ps = data->u_ps;
  double *v_ps = data->v_ps;
  double *w_ps = data->w_ps;
  
  /* U */
  const double *u = data->u;
  const double *aP_u = data->coeffs_u.aP;
  const double *aW_u = data->coeffs_u.aW;
  const double *aE_u = data->coeffs_u.aE;
  const double *aS_u = data->coeffs_u.aS;
  const double *aN_u = data->coeffs_u.aN;
  const double *aB_u = data->coeffs_u.aB;
  const double *aT_u = data->coeffs_u.aT;
  const double *b_u = data->coeffs_u.b;

  /* V */
  const double *v = data->v;
  const double *aP_v = data->coeffs_v.aP;
  const double *aW_v = data->coeffs_v.aW;
  const double *aE_v = data->coeffs_v.aE;
  const double *aS_v = data->coeffs_v.aS;
  const double *aN_v = data->coeffs_v.aN;
  const double *aB_v = data->coeffs_v.aB;
  const double *aT_v = data->coeffs_v.aT;
  const double *b_v = data->coeffs_v.b;

  /* W */

  const double *w = data->w;
  const double *aP_w = data->coeffs_w.aP;
  const double *aW_w = data->coeffs_w.aW;
  const double *aE_w = data->coeffs_w.aE;
  const double *aS_w = data->coeffs_w.aS;
  const double *aN_w = data->coeffs_w.aN;
  const double *aB_w = data->coeffs_w.aB;
  const double *aT_w = data->coeffs_w.aT;
  const double *b_w = data->coeffs_w.b;  
  
  /* U */
#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(u, u_ps, aE_u, aW_u, aN_u, aS_u, aT_u, aB_u, b_u, aP_u)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = K*nx*Ny + J*nx + i;
	u_ps[index_u] = (aE_u[index_u] * u[index_u+1] + 
			 aW_u[index_u] * u[index_u-1] +
			 aN_u[index_u] * u[index_u+nx] + 
			 aS_u[index_u] * u[index_u-nx] +
			 aT_u[index_u] * u[index_u+nx*Ny] +
			 aB_u[index_u] * u[index_u-nx*Ny] +
			 b_u[index_u]) / aP_u[index_u];
      }
    }
  }

  /* V */
#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(v, v_ps, aE_v, aW_v, aN_v, aS_v, aT_v, aB_v, b_v, aP_v)

  for(K=1; K<Nz-1; K++){
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = K*Nx*ny + j*Nx + I;
	v_ps[index_v] = (aE_v[index_v] * v[index_v+1] + 
			 aW_v[index_v] * v[index_v-1] +
			 aN_v[index_v] * v[index_v+Nx] + 
			 aS_v[index_v] * v[index_v-Nx] +
			 aT_v[index_v] * v[index_v+Nx*ny] +
			 aB_v[index_v] * v[index_v+Nx*ny] +
			 b_v[index_v]) / aP_v[index_v];
      }
    }
  }

  /* W */
#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(w, w_ps, aE_w, aW_w, aN_w, aS_w, aT_w, aB_w, b_w, aP_w)

  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_w = k*Nx*Ny + J*Nx + I;
	w_ps[index_w] = (aE_w[index_w] * w[index_w+1] + 
			 aW_w[index_w] * w[index_w-1] +
			 aN_w[index_w] * w[index_w+Nx] + 
			 aS_w[index_w] * w[index_w-Nx] +
			 aT_w[index_w] * w[index_w+Nx*Ny] +
			 aB_w[index_w] * w[index_w+Nx*Ny] +
			 b_w[index_w]) / aP_w[index_w];
      }
    }
  }
  
  return;
}

/* UPDATE_FIELD_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (nx, Ny, Nz) u: update values using pressure correction.

  Array (Nx, ny, Nz) v: update values using pressure correction.

  Array (Nx, Ny, nz) w: update values using pressure correction.

  Array (Nx, Ny, Nz) p: update values using pressure correction.

  @param data
  
  @return ret_value not implemented.
  
  MODIFIED: 21-11-2019
*/    
void update_field_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  int index_aux;

  int set_corner_null_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;

  const int u_slave_count = data->u_slave_count;
  const int v_slave_count = data->v_slave_count;
  const int w_slave_count = data->w_slave_count;
  const int w_type = data->bc_flow_west.type;
  const int e_type = data->bc_flow_east.type;
  const int n_type = data->bc_flow_north.type;
  const int s_type = data->bc_flow_south.type;
  const int t_type = data->bc_flow_top.type;
  const int b_type = data->bc_flow_bottom.type;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *w_slave_nodes = data->w_slave_nodes;

  double alpha_u;
  double alpha_v;
  double alpha_w;

  const double alpha_p_corr = data->alpha_p_corr;

  double *u = data->u;
  double *v = data->v;
  double *w = data->w;
  double *p = data->p;

  const double *p_corr = data->p_corr;

  const double *u_aP = data->coeffs_u.aP;
  const double *v_aP = data->coeffs_v.aP;
  const double *w_aP = data->coeffs_w.aP;

  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *w_faces_z = data->w_faces_z;
  const double *fn_u = data->fn_u;
  const double *fn_w = data->fn_w;
  const double *fe_v = data->fe_v;
  const double *fe_w = data->fe_w;
  const double *ft_u = data->ft_u;
  const double *ft_v = data->ft_v;


  if(pv_coupling_algorithm == 0){
    alpha_u = data->alpha_u;
    alpha_v = data->alpha_v;
    alpha_w = data->alpha_w;
  }
  else{
    alpha_u = 1;
    alpha_v = 1;
    alpha_w = 1;
  }

  /* U */
#pragma omp parallel for default(none) private(I, J, K, i, index_, index_u) \
  shared(u_dom_matrix, u, p_corr, u_faces_x, u_aP, alpha_u)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){

	index_u = K*nx*Ny + J*nx + i;

	if(u_dom_matrix[index_u] == 0){

	  I = i + 1;
	  index_ = K*Nx*Ny + J*Nx + I;

	  u[index_u] += alpha_u * (p_corr[index_-1] * u_faces_x[index_-1] - 
				   p_corr[index_] * u_faces_x[index_]) / u_aP[index_u];
	}
      }
    }
  }

  /* V */
#pragma omp parallel for default(none) private(I, J, K, j, index_, index_v) \
  shared(v_dom_matrix, v, p_corr, v_faces_y, v_aP, alpha_v)

  for(K=1; K<Nz-1; K++){
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){

	index_v = K*Nx*ny + j*Nx + I;

	if(v_dom_matrix[index_v] == 0){

	  J = j + 1;
	  index_ = K*Nx*Ny + J*Nx + I;

	  v[index_v] += alpha_v * (p_corr[index_-Nx] * v_faces_y[index_-Nx] - 
				   p_corr[index_] * v_faces_y[index_]) / v_aP[index_v];
	}
      }
    }
  }

  /* W */
#pragma omp parallel for default(none) private(I, J, K, k, index_, index_w) \
  shared(w_dom_matrix, w, p_corr, w_faces_z, w_aP, alpha_w)

  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_w = k*Nx*Ny + J*Nx + I;

	if(w_dom_matrix[index_w] == 0){

	  K = k + 1;
	  index_ = K*Nx*Ny + J*Nx + I;

	  w[index_w] += alpha_w * (p_corr[index_-Nx*Ny] * w_faces_z[index_-Nx*Ny] -
				   p_corr[index_] * w_faces_z[index_]) / w_aP[index_w];
	}
      }
    }
  }

  /* Boundary nodes */
  /* Needed to maintain equality of velocity values imposed by boundary conditions,
     the previous loop only updated interior values */  

#pragma omp parallel sections default(none) private(I, J, K, i, j, k, index_u, index_v, index_w, index_aux) \
  shared(u, v, w, fn_u, fn_w, fe_v, fe_w, ft_u, ft_v)

  {

#pragma omp section
    {
      /* South */

      /* Type: 0, Zero gradient. Type: 2, Symmetry. Type: 4, Constant pressure.
	 Type: 5, Periodic boundary */
      
      if((s_type == 2) || (s_type == 0) 
	 || (s_type == 4) || (s_type == 5)){

	/* U */

	/* Type: 5, Periodic boundary */
	if(s_type == 5){
	  J = 0;
	  
	  for(K=1; K<Nz-1; K++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      index_aux = K*nx*Ny + (Ny-3)*nx + i;
	      u[index_u] = u[index_aux] * fn_u[index_aux] +
		u[index_aux+nx] * (1 - fn_u[index_aux]);
	    }
	  }
	}
	else{
	  J = 0;
	  
	  for(K=1; K<Nz-1; K++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[index_u+nx];
	    }
	  }
	}

	/* V */

	/* Type: 0, Zero gradient */
	if(s_type == 0){
	  j = 0;

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_v+Nx];
	    }
	  }
	}
	/* Type: 5, Periodic boundary */
	if(s_type == 5){
	  j = 0;

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[K*Nx*ny + (ny-2)*Nx + I];
	    }
	  }
	}

	/* W */
	
	/* Type: 5, Periodic boundary */
	if(s_type == 5){
	  J = 0;

	  for(k=1; k<nz-1; k++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      index_aux = k*Nx*Ny + (Ny-3)*Nx + I;
	      w[index_w] = w[index_aux] * fn_w[index_aux] +
		w[index_aux+Nx] * (1 - fn_w[index_aux]);
	    }
	  }
	}
	else{
	  J = 0;

	  for(k=1; k<nz-1; k++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[index_w+Nx];
	    }
	  }
	}
	
      } /* if((s_type == 2) || (s_type == 0) || (s_type == 4) || (s_type == 5)) */  
            
    } /* South section */ 

#pragma omp section 
    {
      /* North */


      /* Type: 0, Zero gradient. Type: 2, Symmetry. Type: 4, Constant pressure.
	 Type: 5, Periodic boundary */
      
      if((n_type == 2) || (n_type == 0)
	 || (n_type == 4) || (n_type == 5)){

	/* U */

	/* Type: 5, Periodic boundary */
	if(n_type == 5){
	  J = Ny - 1;

	  for(K=1; K<Nz-1; K++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      index_aux = K*nx*Ny + 1*nx + i;
	      u[index_u] = u[index_aux] * fn_u[index_aux] +
		u[index_aux+nx] * (1 - fn_u[index_aux]);
	    }
	  }
	}
	else{
	  J = Ny - 1;

	  for(K=1; K<Nz-1; K++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[index_u-nx];
	    }
	  }
	}


	/* V */

	/* Type: 0, Zero gradient */
	if(n_type == 0){
	  j = ny - 1;

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_v-Nx];
	    }
	  }
	}
	/* Type: 5, Periodic boundary */
	if(n_type == 5){
	  j = ny - 1;

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[K*Nx*ny + 1*Nx + I];
	    }
	  }
	}

	/* W */

	/* Type: 5, Periodic boundary */
	if(n_type == 5){
	  J = Ny - 1;

	  for(k=1; k<nz-1; k++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      index_aux = k*Nx*Ny + 1*Nx + I;
	      w[index_w] = w[index_aux] * fn_w[index_aux] +
		w[index_aux+Nx] * (1 - fn_w[index_aux]);
	    }
	  }
	}
	else{
	  J = Ny - 1;

	  for(k=1; k<nz-1; k++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[index_w-Nx];
	    }
	  }
	}
	
      } /* if((n_type == 2) || (n_type == 0) || (n_type == 4) || (n_type == 5)) */
    } /* North section */

#pragma omp section
    {
      /* West */

      /* Type: 0, Zero gradient. Type: 2, Symmetry. Type: 4, Constant pressure.
	 Type: 5, Periodic boundary */
      
      if((w_type == 2) || (w_type == 0)
	 || (w_type == 4) || (w_type == 5)){

	/* U */

	/* Type: 0, Zero gradient */
	if(w_type == 0){
	  i = 0;

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[index_u+1];
	    }
	  }
	}
	/* Type: 5, Periodic boundary */
	if(w_type == 5){
	  i = 0;

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[K*nx*Ny + J*nx + nx-2];
	    }
	  }
	}	

	/* V */	

	/* Type: 5, Periodic boundary */
	if(w_type == 5){
	  I = 0;

	  for(K=1; K<Nz-1; K++){
	    for(j=1; j<ny-1; j++){
	      index_v = K*Nx*ny + j*Nx + I;
	      index_aux = K*Nx*ny + j*Nx + Nx-3;
	      v[index_v] = v[index_aux] * fe_v[index_aux] + 
		v[index_aux+1] * (1 - fe_v[index_aux]);
	    }
	  }
	}
	else{
	  I = 0;

	  for(K=1; K<Nz-1; K++){
	    for(j=1; j<ny-1; j++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_v+1];
	    }
	  }
	}

	/* W */

	/* Type: 5, Periodic boundary */
	if(w_type == 5){
	  I = 0;

	  for(k=1; k<nz-1; k++){
	    for(J=1; J<Ny-1; J++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      index_aux = k*Nx*Ny + J*Nx + Nx-3;
	      w[index_w] = w[index_aux] * fe_w[index_aux] +
		w[index_aux+1] * (1 - fe_w[index_aux]);
	    }
	  }
	}
	else{
	  I = 0;

	  for(k=1; k<nz-1; k++){
	    for(J=1; J<Ny-1; J++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[index_w+1];
	    }
	  } 
	}
	
      }
    } /* West section */

#pragma omp section
    {
      /* East */

      /* Type: 0, Zero gradient. Type: 2, Symmetry. Type: 4, Constant pressure.
	 Type: 5, Periodic boundary */

      if((e_type == 2) || (e_type == 0)
	 || (e_type == 4) || (e_type == 5)){

	/* U */

	/* Type: 0, Zero gradient */
	if(e_type == 0){
	  i = nx - 1;

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[index_u-1];
	    }
	  }
	}
	/* Type: 5, Periodic boundary */
	if(e_type == 5){
	  i = nx - 1;

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[K*nx*Ny + J*nx + 1];
	    }
	  }
	}	
	
	/* V */

	/* Type: 5, Periodic boundary */
	if(e_type == 5){
	  I = Nx - 1;

	  for(K=1; K<Nz-1; K++){
	    for(j=1; j<ny-1; j++){
	      index_v = K*Nx*ny + j*Nx + I;
	      index_aux = K*Nx*ny + j*Nx + 1;
	      v[index_v] = v[index_aux] * fe_v[index_aux] + 
		v[index_aux+1] * (1 - fe_v[index_aux]);
	    }
	  }
	}
	else{
	  I = Nx - 1;

	  for(K=1; K<Nz-1; K++){
	    for(j=1; j<ny-1; j++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_v-1];
	    }
	  }
	}

	/* W */

	/* Type: 5, Periodic boundary */
	if(e_type == 5){
	  I = Nx - 1;

	  for(k=1; k<nz-1; k++){
	    for(J=1; J<Ny-1; J++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      index_aux = k*Nx*Ny + J*Nx + 1;
	      w[index_w] = w[index_aux] * fe_w[index_aux] +
		w[index_aux+1] * (1 - fe_w[index_aux]);
	    }
	  }
	}
	else{
	  I = Nx - 1;

	  for(k=1; k<nz-1; k++){
	    for(J=1; J<Ny-1; J++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[index_w-1];
	    }
	  }
	}
      }           
    } /* East section */

#pragma omp section
    {
      /* Bottom */

      /* Type: 0, Zero gradient. Type: 2, Symmetry. Type: 4, Constant pressure.
	 Type: 5, Periodic boundary */

      if((b_type == 2) || (b_type == 0)
	 || (b_type == 4) || (b_type == 5)){

	/* U */

	/* Type: 5, Periodic boundary */

	if(b_type == 5){
	  K = 0;

	  for(J=1; J<Ny-1; J++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      index_aux = (Nz-3)*nx*Ny + J*nx + i;
	      u[index_u] = u[index_aux] * ft_u[index_aux] +
		u[index_aux+nx*Ny] * (1 - ft_u[index_aux]);
	    }
	  }
	}
	else{
	  K = 0;

	  for(J=1; J<Ny-1; J++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[index_u+nx*Ny];
	    }
	  }
	}

	/* V */

	if(b_type == 5){
	  K = 0;

	  for(j=1; j<ny-1; j++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      index_aux = (Nz-3)*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_aux] * ft_v[index_aux] +
		v[index_aux+Nx*ny] * (1 - ft_v[index_aux]);		
	    }
	  }
	}
	else{
	  K = 0;

	  for(j=1; j<ny-1; j++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_v+Nx*ny];
	    }
	  }
	}

	/* W */

	if(b_type == 0){
	  k = 0;

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[index_w+Nx*Ny];
	    }
	  }
	}
	if(b_type == 5){
	  k = 0;

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[(nz-2)*Nx*Ny + J*Nx + I];
	    }
	  } 
	}	
      }            
    } /* Bottom section */

#pragma omp section
    {
      /* Top */

      /* Type: 0, Zero gradient. Type: 2, Symmetry. Type: 4, Constant pressure
       Type: 5, Periodic boundary */

      if((t_type == 2) || (t_type == 0)
	 || (t_type == 4) || (t_type == 5)){

	/* U */

	if(t_type == 5){
	  K = Nz - 1;

	  for(J=1; J<Ny-1; J++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      index_aux = 1*nx*Ny + J*nx + i;
	      u[index_u] = u[index_aux] * ft_u[index_aux] +
		u[index_aux+nx*Ny] * (1 - ft_u[index_aux]);
	    }
	  }
	}
	else{
	  K = Nz - 1;

	  for(J=1; J<Ny-1; J++){
	    for(i=1; i<nx-1; i++){
	      index_u = K*nx*Ny + J*nx + i;
	      u[index_u] = u[index_u-nx*Ny];
	    }
	  }
	}

	/* V */

	if(t_type == 5){
	  K = Nz - 1;
	  
	  for(j=1; j<ny-1; j++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      index_aux = 1*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_aux] * ft_v[index_aux] +
		v[index_aux+Nx*ny] * (1 -ft_v[index_aux]);	      
	    }
	  }
	}
	else{
	  K = Nz - 1;

	  for(j=1; j<ny-1; j++){
	    for(I=1; I<Nx-1; I++){
	      index_v = K*Nx*ny + j*Nx + I;
	      v[index_v] = v[index_v-Nx*ny];
	    }
	  }
	}

	/* W */

	if(t_type == 0){
	  k = Nz - 1;

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[index_w-Nx*Ny];
	    }
	  }
	}
	if(t_type == 5){
	  k = Nz - 1;

	  for(K=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_w = k*Nx*Ny + J*Nx + I;
	      w[index_w] = w[1*Nx*Ny + J*Nx + I];
	    }
	  }	  
	}
      }
    }
    
  } /* sections */

  /* Update slave nodes */
  for(i=0; i<u_slave_count; i++){
    u[ u_slave_nodes[i] ] = 0;
  }
  for(j=0; j<v_slave_count; j++){
    v[ v_slave_nodes[j] ] = 0;
  }

  for(k=0; k<w_slave_count; k++){
    w[ w_slave_nodes[k] ] = 0;
  }

  /* Arbitrary assignment of corner values, necessary to avoid problems
     when computing derivatives in calculating turbulence */
  
  /* Bottom-South-West */
  set_corner_null_OK =
    b_type == 3 || s_type == 3 || w_type == 3;

  if(set_corner_null_OK){
    u[0] = 0;
    v[0] = 0;
    w[0] = 0;
  }
  else{
    index_u = 0;
    index_v = 0;
    index_w = 0;
    u[index_u] = (u[index_u + nx*Ny] + u[index_u + nx] + u[index_u + 1]) / 3;
    v[index_v] = (v[index_u + Nx*ny] + v[index_v + Nx] + v[index_v + 1]) / 3;
    w[index_w] = (w[index_w + Nx*Ny] + w[index_w + Nx] + w[index_w + 1]) / 3;
  }

  /* Bottom-North-West */
  set_corner_null_OK =
    b_type == 3 || n_type == 3 || w_type == 3;

  if(set_corner_null_OK){
    u[(Ny-1) * nx] = 0;
    v[(ny-1) * Nx] = 0;
    w[(Ny-1) * Nx] = 0;
  }
  else{
    index_u = (Ny-1) * nx;
    index_v = (ny-1) * Nx;
    index_w = (Ny-1) * Nx;
    u[index_u] = (u[index_u + nx*Ny] + u[index_u - nx] + u[index_u + 1]) / 3;
    v[index_v] = (v[index_v + Nx*ny] + v[index_v - Nx] + v[index_v + 1]) / 3;
    w[index_w] = (w[index_w + Nx*Ny] + w[index_w - Nx] + w[index_w + 1]) / 3;
  }

  /* Bottom-South-East */
  set_corner_null_OK =
    b_type == 3 || s_type == 3 || e_type == 3;

  if(set_corner_null_OK){
    u[nx-1] = 0;
    v[Nx-1] = 0;
    w[Nx-1] = 0;
  }
  else{
    index_u = nx-1;
    index_v = Nx-1;
    index_w = Nx-1;
    u[index_u] = (u[index_u + nx*Ny] + u[index_u + nx] + u[index_u - 1]) / 3;
    v[index_v] = (v[index_v + Nx*ny] + v[index_v + Nx] + v[index_v - 1]) / 3;
    w[index_w] = (w[index_w + Nx*Ny] + w[index_w + Nx] + w[index_w - 1]) / 3;
  }

  /* Bottom-North-East */
  set_corner_null_OK =
    b_type == 3 || n_type == 3 || e_type == 3;

  if(set_corner_null_OK){
    u[(Ny-1)*nx + nx-1] = 0;
    v[(ny-1)*Nx + Nx-1] = 0;
    w[(Ny-1)*Nx + Nx-1] = 0;
  }
  else{
    index_u = (Ny-1)*nx + nx-1;
    index_v = (ny-1)*Nx + Nx-1;
    index_w = (Ny-1)*Nx + Nx-1;
    u[index_u] = (u[index_u + nx*Ny] + u[index_u - nx] + u[index_u - 1]) / 3;
    v[index_v] = (v[index_v + Nx*ny] + v[index_v - Nx] + v[index_v - 1]) / 3;
    w[index_w] = (w[index_w + Nx*Ny] + w[index_w - Nx] + w[index_w - 1]) / 3;   
  }

  /* Top-South-West */
  set_corner_null_OK =
    t_type == 3 || s_type == 3 || w_type == 3;

  if(set_corner_null_OK){
    u[(Nz-1)*nx*Ny] = 0;
    v[(Nz-1)*Nx*ny] = 0;
    w[(nz-1)*Nx*Ny] = 0;
  }
  else{
    index_u = (Nz-1)*nx*Ny;
    index_v = (Nz-1)*Nx*ny;
    index_w = (nz-1)*Nx*Ny;
    u[index_u] = (u[index_u - nx*Ny] + u[index_u + nx] + u[index_u + 1]) / 3;
    v[index_v] = (v[index_u - Nx*ny] + v[index_v + Nx] + v[index_v + 1]) / 3;
    w[index_w] = (w[index_w - Nx*Ny] + w[index_w + Nx] + w[index_w + 1]) / 3;
  }

  /* Top-North-West */
  set_corner_null_OK =
    t_type == 3 || n_type == 3 || w_type == 3;

  if(set_corner_null_OK){
    u[(Nz-1)*nx*Ny + (Ny-1) * nx] = 0;
    v[(Nz-1)*Nx*ny + (ny-1) * Nx] = 0;
    w[(nz-1)*Nx*Ny + (Ny-1) * Nx] = 0;
  }
  else{
    index_u = (Nz-1)*nx*Ny + (Ny-1) * nx;
    index_v = (Nz-1)*Nx*ny + (ny-1) * Nx;
    index_w = (nz-1)*Nx*Ny + (Ny-1) * Nx;
    u[index_u] = (u[index_u - nx*Ny] + u[index_u - nx] + u[index_u + 1]) / 3;
    v[index_v] = (v[index_v - Nx*ny] + v[index_v - Nx] + v[index_v + 1]) / 3;
    w[index_w] = (w[index_w - Nx*Ny] + w[index_w - Nx] + w[index_w + 1]) / 3;
  }

  /* Bottom-South-East */
  set_corner_null_OK =
    t_type == 3 || s_type == 3 || e_type == 3;

  if(set_corner_null_OK){
    u[(Nz-1)*nx*Ny + nx-1] = 0;
    v[(Nz-1)*Nx*ny + Nx-1] = 0;
    w[(nz-1)*Nx*Ny + Nx-1] = 0;
  }
  else{
    index_u = (Nz-1)*nx*Ny + nx-1;
    index_v = (Nz-1)*Nx*ny + Nx-1;
    index_w = (nz-1)*Nx*Ny + Nx-1;
    u[index_u] = (u[index_u - nx*Ny] + u[index_u + nx] + u[index_u - 1]) / 3;
    v[index_v] = (v[index_v - Nx*ny] + v[index_v + Nx] + v[index_v - 1]) / 3;
    w[index_w] = (w[index_w - Nx*Ny] + w[index_w + Nx] + w[index_w - 1]) / 3;
  }

  /* Bottom-North-East */
  set_corner_null_OK =
    t_type == 3 || n_type == 3 || e_type == 3;

  if(set_corner_null_OK){
    u[(Nz-1)*nx*Ny + (Ny-1)*nx + nx-1] = 0;
    v[(Nz-1)*Nx*ny + (ny-1)*Nx + Nx-1] = 0;
    w[(nz-1)*Nx*Ny + (Ny-1)*Nx + Nx-1] = 0;
  }
  else{
    index_u = (Nz-1)*nx*Ny + (Ny-1)*nx + nx-1;
    index_v = (Nz-1)*Nx*ny + (ny-1)*Nx + Nx-1;
    index_w = (nz-1)*Nx*Ny + (Ny-1)*Nx + Nx-1;
    u[index_u] = (u[index_u - nx*Ny] + u[index_u - nx] + u[index_u - 1]) / 3;
    v[index_v] = (v[index_v - Nx*ny] + v[index_v - Nx] + v[index_v - 1]) / 3;
    w[index_w] = (w[index_w - Nx*Ny] + w[index_w - Nx] + w[index_w - 1]) / 3;   
  }


  /* Bottom - South */

  set_corner_null_OK = b_type == 3 || s_type == 3;
  
  if(set_corner_null_OK){
  
    for(i=1; i<nx-1; i++){
      u[i] = 0;
    }
    
    for(I=1; I<Nx-1; I++){
      v[I] = 0;
      w[I] = 0;
    }
  }
  else{

    for(i=1; i<nx-1; i++){
      index_u = i;
      u[index_u] = 0.5 * (u[index_u+nx] + u[index_u+nx*Ny]);
    }
    
    for(I=1; I<Nx-1; I++){
      index_v = I;
      index_w = I;
      v[index_v] = 0.5 * (v[index_v+Nx] + v[index_v+Nx*ny]);
      w[index_w] = 0.5 * (w[index_w+Nx] + w[index_w+Nx*Ny]);    
    }
  }

  /* Bottom - North */

  set_corner_null_OK = b_type == 3 || n_type == 3;

  if(set_corner_null_OK){

    for(i=1; i<nx-1; i++){
      u[(Ny-1)*nx+i] = 0;
    }
    
    for(I=1; I<Nx-1; I++){
      v[(ny-1)*Nx+I] = 0;
      w[(Ny-1)*Nx+I] = 0;
    }

  }
  else{

    for(i=1; i<nx-1; i++){
      index_u = (Ny-1)*nx + i;
      u[index_u] = 0.5 * (u[index_u-nx] + u[index_u+nx*Ny]);
    }
    
    for(I=1; I<Nx-1; I++){
      index_v = (ny-1)*Nx + I;
      index_w = (Ny-1)*Nx + I;
      v[index_v] = 0.5 * (v[index_v-Nx] + v[index_v+Nx*ny]);
      w[index_w] = 0.5 * (w[index_w-Nx] + w[index_w+Nx*Ny]);
    }
  }

  /* Bottom - West */

  set_corner_null_OK = b_type == 3 || w_type == 3;

  if(set_corner_null_OK){

    for(j=1; j<ny-1; j++){
      v[j*Nx] = 0;
    }

    for(J=1; J<Ny-1; J++){
      u[J*nx] = 0;
      w[J*Nx] = 0;
    }
  }
  else{

    for(j=1; j<ny-1; j++){
      index_v = j*Nx;
      v[index_v] = 0.5 * (v[index_v+1] + v[index_v+Nx*ny]);
    }

    for(J=1; J<Ny-1; J++){
      index_u = J*nx;
      index_w = J*Nx;
      u[index_u] = 0.5 * (u[index_u+1] + u[index_u+nx*Ny]);
      w[index_w] = 0.5 * (w[index_w+1] + w[index_w+Nx*Ny]);
    }
  }

  /* Bottom - East */

  set_corner_null_OK = b_type == 3 || e_type == 3;

  if(set_corner_null_OK){

    for(j=1; j<ny-1; j++){
      v[j*Nx+Nx-1] = 0;      
    }

    for(J=1; J<Ny-1; J++){
      u[J*nx+nx-1] = 0;
      w[J*Nx+Nx-1] = 0;
    }
  }
  else{

    for(j=1; j<ny-1; j++){
      index_v = j*Nx + Nx-1;
      v[index_v] = 0.5 * (v[index_v-1] + v[index_v+Nx*ny]);
    }

    for(J=1; J<Ny-1; J++){
      index_u = J*nx + nx-1;
      index_w = J*Nx + Nx-1;
      u[index_u] = 0.5 * (u[index_u-1] + u[index_u+nx*Ny]);
      w[index_w] = 0.5 * (w[index_w-1] + w[index_w+Nx*Ny]);
    }
  }

  /* Top - South */

  set_corner_null_OK = t_type == 3 || s_type == 3;

  if(set_corner_null_OK){

    for(i=1; i<nx-1; i++){
      u[(Nz-1)*nx*Ny+i] = 0;
    }

    for(I=1; I<Nx-1; I++){
      v[(Nz-1)*Nx*ny+I] = 0;
      w[(nz-1)*Nx*Ny+I] = 0;
    }
  }
  else{

    for(i=1; i<nx-1; i++){
      index_u = (Nz-1)*nx*Ny + i;
      u[index_u] = 0.5 * (u[index_u+nx] + u[index_u-nx*Ny]);
    }

    for(I=1; I<Nx-1; I++){
      index_v = (Nz-1)*Nx*ny + I;
      index_w = (nz-1)*Nx*Ny + I;
      v[index_v] = 0.5 * (v[index_v+Nx] + v[index_v-Nx*ny]);
      w[index_w] = 0.5 * (w[index_w+Nx] + w[index_w-Nx*Ny]);
    }
  }

  /* Top - North */

  set_corner_null_OK = t_type == 3 || n_type == 3;

  if(set_corner_null_OK){

    for(i=1; i<nx-1; i++){
      u[(Nz-1)*nx*Ny+(Ny-1)*nx+i] = 0;
    }

    for(I=1; I<Nx-1; I++){
      v[(Nz-1)*Nx*ny+(ny-1)*Nx+I] = 0;
      w[(nz-1)*Nx*Ny+(Ny-1)*Nx+I] = 0;
    }
  }
  else{

    for(i=1; i<nx-1; i++){
      index_u = (Nz-1)*nx*Ny + (Ny-1)*nx + i;
      u[index_u] = 0.5 * (u[index_u-nx] + u[index_u-nx*Ny]);
    }

    for(I=1; I<Nx-1; I++){
      index_v = (Nz-1)*Nx*ny + (ny-1)*Nx + I;
      index_w = (nz-1)*Nx*Ny + (Ny-1)*Nx + I;
      v[index_v] = 0.5 * (v[index_v-Nx] + v[index_v-Nx*ny]);
      w[index_w] = 0.5 * (w[index_w-Nx] + w[index_w-Nx*Ny]);	
    }
  }

  /* Top - West */

  set_corner_null_OK = t_type == 3 || w_type == 3;

  if(set_corner_null_OK){

    for(j=1; j<ny-1; j++){
      v[(Nz-1)*Nx*ny+j*Nx] = 0;
    }

    for(J=1; J<Ny-1; J++){
      u[(Nz-1)*nx*Ny+J*nx] = 0;
      w[(nz-1)*Nx*Ny+J*Nx] = 0;
    }
  }
  else{

    for(j=1; j<ny-1; j++){
      index_v = (Nz-1)*Nx*ny + j*Nx;
      v[index_v] = 0.5 * (v[index_v+1] + v[index_v-Nx*ny]);
    }

    for(J=1; J<Ny-1; J++){
      index_u = (Nz-1)*nx*Ny + J*nx;
      index_w = (nz-1)*Nx*Ny + J*Nx;
      u[index_u] = 0.5 * (u[index_u+1] + u[index_u-nx*Ny]);
      w[index_w] = 0.5 * (w[index_w+1] + w[index_w-Nx*Ny]);
    }
  }

  /* Top - East */

  set_corner_null_OK = t_type == 3 || e_type == 3;

  if(set_corner_null_OK){

    for(j=1; j<ny-1; j++){
      v[(Nz-1)*Nx*ny+j*Nx+Nx-1] = 0;
    }

    for(J=1; J<Ny-1; J++){
      u[(Nz-1)*nx*Ny+J*nx+nx-1] = 0;
      w[(nz-1)*Nx*Ny+J*Nx+Nx-1] = 0;
    }
  }
  else{

    for(j=1; j<ny-1; j++){
      index_v = (Nz-1)*Nx*ny + j*Nx + Nx-1;
      v[index_v] = 0.5 * (v[index_v-1] + v[index_v-Nx*ny]);
    }

    for(J=1; J<Ny-1; J++){
      index_u = (Nz-1)*nx*Ny + J*nx + nx-1;
      index_w = (nz-1)*Nx*Ny + J*Nx + Nx-1;
      u[index_u] = 0.5 * (u[index_u-1] + u[index_u-nx*Ny]);
      w[index_w] = 0.5 * (w[index_w-1] + w[index_w-Nx*Ny]);
    }
  }

  /* South - West */

  set_corner_null_OK = s_type == 3 || w_type == 3;


  if(set_corner_null_OK){
    
    for(k=1; k<nz-1; k++){
      w[k*Nx*Ny] = 0;
    }

    for(K=1; K<Nz-1; K++){
      u[K*nx*Ny] = 0;
      v[K*Nx*ny] = 0;
    }
  }
  else{

    for(k=1; k<nz-1; k++){
      index_w = k*Nx*Ny;
      w[index_w] = 0.5 * (w[index_w+1] + w[index_w+Nx]);
    }

    for(K=1; K<Nz-1; K++){
      index_u = K*nx*Ny;
      index_v = K*Nx*ny;
      u[index_u] = 0.5 * (u[index_u+1] + u[index_u+nx]);
      v[index_v] = 0.5 * (v[index_v+1] + v[index_v+Nx]);
    }
  }


  /* South - East */

  set_corner_null_OK = s_type == 3 || e_type == 3;

  if(set_corner_null_OK){

    for(k=1; k<nz-1; k++){
      w[k*Nx*Ny+Nx-1] = 0;
    }

    for(K=1; K<Nz-1; K++){
      u[K*nx*Ny+nx-1] = 0;
      v[K*Nx*ny+Nx-1] = 0;
    }
  }
  else{

    for(k=1; k<nz-1; k++){
      index_w = k*Nx*Ny + Nx-1;
      w[index_w] = 0.5 * (w[index_w-1] + w[index_w+Nx]);
    }

    for(K=1; K<Nz-1; K++){
      index_u = K*nx*Ny + nx-1;
      index_v = K*Nx*ny + Nx-1;
      u[index_u] = 0.5 * (u[index_u-1] + u[index_u+nx]);
      v[index_v] = 0.5 * (v[index_v-1] + v[index_v+Nx]);
    }
  }

  /* North - West */

  set_corner_null_OK = n_type == 3 || w_type == 3;

  if(set_corner_null_OK){

    for(k=1; k<nz-1; k++){
      w[k*Nx*Ny+(Ny-1)*Nx] = 0;
    }

    for(K=1; K<Nz-1; K++){
      u[K*nx*Ny+(Ny-1)*nx] = 0;
      v[K*Nx*ny+(ny-1)*Nx] = 0;
    }
  }
  else{

    for(k=1; k<nz-1; k++){
      index_w = k*Nx*Ny + (Ny-1)*Nx;
      w[index_w] = 0.5 * (w[index_w+1] + w[index_w-Nx]);
    }

    for(K=1; K<Nz-1; K++){
      index_u = K*nx*Ny + (Ny-1)*nx;
      index_v = K*Nx*ny + (ny-1)*Nx;
      u[index_u] = 0.5 * (u[index_u+1] + u[index_u-nx]);
      v[index_v] = 0.5 * (v[index_v+1] + v[index_v-Nx]);
    }
  }

  /* North - East */

  set_corner_null_OK = n_type == 3 || e_type == 3;

  if(set_corner_null_OK){

    for(k=1; k<nz-1; k++){
      w[k*Nx*Ny+(Ny-1)*Nx+Nx-1] = 0;
    }

    for(K=1; K<Nz-1; K++){
      u[K*nx*Ny+(Ny-1)*nx+nx-1] = 0;
      v[K*Nx*ny+(ny-1)*Nx+Nx-1] = 0;
    }
  }
  else{

    for(k=1; k<nz-1; k++){
      index_w = k*Nx*Ny + (Ny-1)*Nx + Nx-1;
      w[index_w] = 0.5 * (w[index_w-1] + w[index_w-Nx]);
    }

    for(K=1; K<Nz-1; K++){
      index_u = K*nx*Ny + (Ny-1)*nx + nx-1;
      index_v = K*Nx*ny + (ny-1)*Nx + Nx-1;
      u[index_u] = 0.5 * (u[index_u-1] + u[index_u-nx]);
      v[index_v] = 0.5 * (v[index_v-1] + v[index_v-Nx]);
    }
  }
  
  /* UPDATE PRESSURE ONLY FOR SIMPLE, SIMPLE_CANG and SIMPLEC */
  /* P */
  
  if(pv_coupling_algorithm != 2){
    
#pragma omp parallel for default(none)		\
  private(I, J, K, index_) shared(p, p_corr)

    for(K=0; K<Nz; K++){
      for(J=0; J<Ny; J++){
	for(I=0; I<Nx; I++){
	
	  index_ = K*Nx*Ny + J*Nx + I;	
	  p[index_] += alpha_p_corr * p_corr[index_];
	}
      }
    }
  }
  
  return;
}

/* CONTINUITY_RESIDUAL_3D */
/*****************************************************************************/
/**
  
  Calculates (double, 1) res: maximum value of the continuity equation residual
  for 3D case.

  @param data

  @return double res
  
  MODIFIED: 27-11-2019
*/
double continuity_residual_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;

  const int *p_dom_matrix = data->p_dom_matrix;

  double res = 0;
  double res_temp = 0;
  
  const double *v = data->v;
  const double *u = data->u;
  const double *w = data->w;
  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;
  const double *p_faces_z = data->p_faces_z;
 
#pragma omp parallel for default(none)				\
  private(I, J, K, i, j, k, index_, index_u, index_v, index_w, res_temp)	\
  shared(p_faces_x, p_faces_y, p_faces_z, u, v, w, p_dom_matrix)	\
  reduction(max: res)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
      
	i = I - 1;
	j = J - 1;
	k = K - 1;

	index_ = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;
	index_v = K*Nx*ny + j*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	if(p_dom_matrix[index_] == 0){
	
	  res_temp = fabs(p_faces_y[index_v] * v[index_v] - 
			  p_faces_y[index_v+Nx] * v[index_v+Nx] +
			  p_faces_x[index_u] * u[index_u] - 
			  p_faces_x[index_u+1] * u[index_u+1] +
			  p_faces_z[index_w] * w[index_w] -
			  p_faces_z[index_w+Nx*Ny] * w[index_w+Nx*Ny]);
	
	  if (res_temp > res){
	    res = res_temp;
	  }
	}
      }
    }
  }
  
  return res;
}

/* MASS_IO_BALANCE_3D */
/*****************************************************************************/  
/**
   \brief Calculates relative global mass imbalance.

   mass_in: Mass flow at West-South boundaries.

   mass_out: Mass flow at East-North boundaries.

   @param data

   @return fabs((mass_in - mass_out) / mass_in);
  
   MODIFIED: 27-11-2019
*/  

double mass_io_balance_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;

  double mass_in = 0;
  double mass_out = 0;

  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *rho_m = data->rho_m;

  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *w_faces_z = data->w_faces_z;

#pragma omp parallel sections default(none) private(I, J, K, i, j, k, index_u, index_v, index_w, index_) \
  shared(u, v, w, rho_m, u_faces_x, v_faces_y, w_faces_z) reduction(+:mass_in, mass_out)
  {

#pragma omp section 
    {
      /* East */
      I = Nx - 1;
      i = I - 1;

      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){

	  index_ = K*Nx*Ny + J*Nx + I;
	  index_u = K*nx*Ny + J*nx + i;
	  mass_out += rho_m[index_] * u[index_u] * u_faces_x[index_];
      
	}
      }
    }

#pragma omp section 
    {
      /* West */
      I = 0;
      i = 0;

      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
      
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_u = K*nx*Ny + J*nx + i;      
	  mass_in += rho_m[index_] * u[index_u] * u_faces_x[index_];
      
	}
      }
    }

#pragma omp section 
    {
      /* North */
      J = Ny - 1;
      j = J - 1;

      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){
      
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_v = K*Nx*ny + j*Nx+I;       
	  mass_out += rho_m[index_] * v[index_v] * v_faces_y[index_];
	}
      }
    }

#pragma omp section 
    {
      /* South */
      J = 0;
      j = 0;

      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){
      
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_v = K*Nx*ny + j*Nx + I;      
	  mass_in += rho_m[index_] * v[index_v] * v_faces_y[index_];
	}
      }
    }

#pragma omp section
    {
      /* Top */
      K = Nz - 1;
      k = K - 1;

      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
	  mass_out += rho_m[index_] * w[index_w] * w_faces_z[index_];
	}
      }
    }

#pragma omp section
    {
      /* Bottom */
      K = 0;
      k = 0;

      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
	  mass_in += rho_m[index_] * w[index_w] * w_faces_z[index_];
	}
      }
    }
    
  } /* sections */
  
  if(mass_in != 0){
    return fabs((mass_in - mass_out) / mass_in);
  }
  else{
    return NAN;
  }
}

/* ADD_PERIODIC_SOURCE_TO_P_3D */
/*****************************************************************************/
/**
   Sets:
   Array (double, NxNyNz) p: add source term to pressure profile after calculation

   of flow using periodic boundary condition.
   
   @param data
    
   @return ret_value not implemented.
       
   MODIFIED: 29-11-2019
*/
void add_periodic_source_to_p_3D(Data_Mem *data){

  int I, J, K, index_; 

  const int p_source_orientation = data->p_source_orientation;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *p_dom_matrix = data->p_dom_matrix;
  
  const double periodic_source = data->periodic_source;

  double *p = data->p;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;

  /* w-e periodic flow */
  if(p_source_orientation == 1){
        
#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(p, nodes_x, p_dom_matrix)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=0; I<Nx; I++){
	
	  index_ = K*Nx*Ny + J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] -= periodic_source * nodes_x[index_];
	  }
	}
      }
    }

    /* South - North faces */   

#pragma omp parallel for default(none) private(I, K, index_)	\
  shared(p_dom_matrix, p, nodes_x)
    
    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	/* South */
	index_ = K*Nx*Ny + 0*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_x[index_];
	}
	/* North */
	index_ = K*Nx*Ny + (Ny-1)*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_x[index_];
	}
      }
    }

    /* Bottom - Top faces */

#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(p_dom_matrix, p, nodes_x)
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	/* Bottom */
	index_ = 0*Nx*Ny + J*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_x[index_];
	}
	/* Top */
	index_ = (Nz-1)*Nx*Ny + J*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_x[index_];
	}
      }
    }
          
  }

  /* s-n periodic flow */
  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(p, nodes_y, p_dom_matrix)

    for(K=1; K<Nz-1; K++){
      for(J=0; J<Ny; J++){
	for(I=1; I<Nx-1; I++){
	  
	  index_ = K*Nx*Ny + J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] -= periodic_source * nodes_y[index_];
	  }
	}
      }
    }


    /* West - East faces */    

#pragma omp parallel for default(none) private(J, K, index_)	\
  shared(p_dom_matrix, p, nodes_y)
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	/* West */
	index_ = K*Nx*Ny + J*Nx + 0;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_y[index_];
	}
	/* East */
	index_ = K*Nx*Ny + J*Nx + Nx-1;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_y[index_];
	}
      }
    }
      
    /* Bottom - Top faces */

#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(p_dom_matrix, p, nodes_y)
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	/* Bottom */
	index_ = 0*Nx*Ny + J*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_y[index_];
	}
	/* Top */
	index_ = (Nz-1)*Nx*Ny + J*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_y[index_];
	}
      }
    }
      
  }

  /* b-t periodic flow */
  if(p_source_orientation == 3){

#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(p, nodes_z, p_dom_matrix)
    
    for(K=0; K<Nz; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;
	  if(p_dom_matrix[index_] == 0){
	    p[index_] -= periodic_source * nodes_z[index_];
	  }
	}
      }
    }

    /* West - East faces */

#pragma omp parallel for default(none) private(J, K, index_)	\
  shared(p_dom_matrix, p, nodes_z)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){

	/* West */
	index_ = K*Nx*Ny + J*Nx + 0;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_z[index_];
	}
	/* East */
	index_ = K*Nx*Ny + J*Nx + Nx-1;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_z[index_];
	}
      }
    }

    /* South - North faces */

#pragma omp parallel for default(none) private(I, K, index_)	\
  shared(p_dom_matrix, p, nodes_z)

    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){

	/* South */
	index_ = K*Nx*Ny + 0*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_z[index_];
	}
	/* North */
	index_ = K*Nx*Ny + (Ny-1)*Nx + I;
	if(p_dom_matrix[index_] == 0){
	  p[index_] -= periodic_source * nodes_z[index_];
	}
      }
    }
    
  }

  return;
}

/* Bogus */
void add_w_momentum_source(Data_Mem *data){

   return;
 }

void add_v_momentum_source_3D(Data_Mem *data){
  alert_msg(&(data->wins), "There is nothing here!!");
}

void add_w_momentum_source_3D(Data_Mem *data){
  alert_msg(&(data->wins), "There is nothing here!!");
}

/* SOLVE_FLOW_SIMPLES_3D_TRANSIENT */
/*****************************************************************************/
/**

   Solve flow field for 3D case, transient.

   @param data

   @return void
 */
void solve_flow_SIMPLES_3D_transient(Data_Mem *data){
  
  char msg[SIZE];
  char filename[SIZE];

  const char *results_dir = data->results_dir;  
  const char *flow_solver_name = data->flow_solver_name;

  FILE *fp = NULL;

  /* Auxiliary counter for saving transient data */
  int transient_counter = 1;
  /* Auxiliary iteration counter for ADI loop */    
  int iter;
  /* Main loop counter */  
  int outer_iter = 0;
  int convergence_OK = 0;
  int convergence_turb_OK = 0;

  /* Number of uvwp iterations on each sub iteration */
  int iter_uvwp;
  int convergence_uvwp_OK;

  /* Number of ke iterations on each sub iteration */
  int iter_ke;
  int convergence_ke_OK;
    
  /* Sub iter counter */
  int sub_iter;
  int sub_iter_convergence_OK = 0;

  /* Use pseudo-velocities, SIMPLER */  
  int use_ps_velocities_OK;

  /* These can be adjusted at interruption signal */
  int it_for_update_coeffs = data->it_for_update_coeffs;
  int max_iter_flow = data->max_iter_flow;
  int max_iter_ADI = data->max_iter_ADI;
  int flow_scheme = data->flow_scheme;

  /* Number of sub iterations */
  const int max_sub_iter = data->max_sub_iter_flow;
  const int it_for_update_k_coeffs = data->it_for_update_k_coeffs;
  const int it_for_update_eps_coeffs = data->it_for_update_eps_coeffs;
  const int max_iter_uvwp = data->max_iter_uvp;
  const int max_iter_ke = data->max_iter_ke;
  const int it_to_start_turb = data->it_to_start_turb;
  const int turb_model = data->turb_model;
  const int it_to_update_y_plus = data->it_to_update_y_plus;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int coupled_physics_OK = data->coupled_physics_OK;
  /* Used for correct ncurses display information */
  const int rstride = coupled_physics_OK ? 5 : 2;
  
  const int animation_flow_OK = data->animation_flow_OK;
  const int animation_flow_update_it = data->animation_flow_update_it;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int res_sigma_iter = data->res_sigma_iter;
  const int write_flow_transient_data_OK = data->write_flow_transient_data_OK;

  u_int64_t time_i, time_f, timer;

  double res_u = 1, res_v = 1, res_w = 1, res_p = 1;
  double res_pcorr = 1, res_c = 1, res_k = 1, res_eps = 1;

  /* These can be adjusted at interruption signal */
  double tol_flow = data->tol_flow;
  double tol_ADI = data->tol_ADI;

  /* Residuals for inner uvwp - ke cycles */
  double inner_glob_norm[6] = {0};

  /* Will use glob_norm values in sigmas_v for transient scheme */
  double *sigmas_v = data->sigmas.values;
  double *glob_norm = data->sigmas.glob_norm;

  const double time_step = data->dt[3];
  const double transient_save = data->write_flow_transient_data_interval;

  void (*ADI_solver)(double *, Coeffs *, 
		     const int, const int, const int, 
		     ADI_Vars *) = data->ADI_solver;
  
  void (*coeffs_flow_u)(Data_Mem *) = NULL;
  void (*coeffs_flow_v)(Data_Mem *) = NULL;
  void (*coeffs_flow_w)(Data_Mem *) = NULL;

  Sigmas *sigmas = &(data->sigmas);
  Wins *wins = &(data->wins);

  sigmas->time_f = &(time_f);

  /* UPWIND scheme */
  if(flow_scheme == 0){
    coeffs_flow_u = coeffs_flow_u_upwind_3D;
    coeffs_flow_v = coeffs_flow_v_upwind_3D;
    coeffs_flow_w = coeffs_flow_w_upwind_3D;
  }
  /* QUICK scheme */
  else if(flow_scheme == 1){
    coeffs_flow_u = coeffs_flow_u_quick_3D;
    coeffs_flow_v = coeffs_flow_v_quick_3D;
    coeffs_flow_w = coeffs_flow_w_quick_3D;
  }
  /* van Leer scheme */
  else if(flow_scheme == 2){
    coeffs_flow_u = coeffs_flow_u_TVD_3D;
    coeffs_flow_v = coeffs_flow_v_TVD_3D;
    coeffs_flow_w = coeffs_flow_w_TVD_3D;
  }

  action_on_signal_OK = 0;

  /* SIMPLER */
  if(pv_coupling_algorithm == 2){
    (*coeffs_flow_u)(data);
    (*coeffs_flow_v)(data);
    (*coeffs_flow_w)(data);
  }

  /* Set sim_time file name and heading */
  if(write_flow_transient_data_OK){
    set_transient_sim_time_filename(filename, "sim_time_flow", 0, results_dir);

    open_check_file(filename, &fp, wins, 0);
    fprintf(fp, "%% Time in seconds\n");
    fprintf(fp, "sim_time_flow_var = [\n");
    fclose(fp);
  }

  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  /* 2D/3D */
  solve_flow_pre_calc_jobs(data);

  /* This is the main (outer) loop */
  do{
    /* 2D/3D */    
    set_mu(data, outer_iter);

    /* Update *0 fields 2D/3D */
    store_flow_fields(data);

    /* Setting iteration counters and flags for the inner loops */
    sub_iter = 0;
    sub_iter_convergence_OK = 0;

    /* Fully implicit scheme */
    do{

      /* Set iteration counter for uvwp inner loop */
      iter_uvwp =0;
      convergence_uvwp_OK = 0;

      while(iter_uvwp < max_iter_uvwp && !convergence_uvwp_OK){
      
	set_bc_flow_3D(data);

	if(pv_coupling_algorithm == 2){
	  use_ps_velocities_OK = 1;
	  /* SIMPLER: COMPUTE PSEUDO-VELOCITIES */
	  compute_pseudo_velocities_3D(data);

	  /* SIMPLER: SOLVE PRESSURE EQUATION */
	  coeffs_flow_p_3D(data, use_ps_velocities_OK);

	  res_p = 1;
	  iter = 1;

	  while((iter < max_iter_ADI) && (res_p > tol_ADI)){
	    (*ADI_solver)(data->p,
			  &(data->coeffs_p),
			  Nx, Ny, Nz,
			  &(data->ADI_vars));
	  
	    res_p = compute_norm_glob_res_3D(data->p, &(data->coeffs_p), Nx, Ny, Nz);
	    iter++;
	  }
	}
      
	/* 1: SOLVE MOMENTUM EQUATIONS */
	
	// u loop
	(*coeffs_flow_u)(data);
      
	res_u = 1;
	iter = 1;
      
	while((iter < max_iter_ADI) && (res_u > tol_ADI)){
	
	  if(iter % it_for_update_coeffs == 0){
	    (*coeffs_flow_u)(data);
	  }
	
	  (*ADI_solver)(data->u, 
			&(data->coeffs_u), 
			nx, Ny, Nz, 
			&(data->ADI_vars));
	
	  res_u = compute_norm_glob_res_3D(data->u, &(data->coeffs_u), nx, Ny, Nz);
	  iter++;
	}
      
	// v loop
	(*coeffs_flow_v)(data);
      
	res_v = 1;
	iter = 1;
      
	while((iter < max_iter_ADI) && (res_v > tol_ADI)){
	
	  if(iter % it_for_update_coeffs == 0){
	    (*coeffs_flow_v)(data);
	  }
	
	  (*ADI_solver)(data->v, 
			&(data->coeffs_v), 
			Nx, ny, Nz, 
			&(data->ADI_vars));
	
	  res_v = compute_norm_glob_res_3D(data->v, &(data->coeffs_v), Nx, ny, Nz);
	  iter++;
	}

	// w loop
	(*coeffs_flow_w)(data);
      
	res_w = 1;
	iter = 1;
      
	while((iter < max_iter_ADI) && (res_w > tol_ADI)){
	
	  if(iter % it_for_update_coeffs == 0){
	    (*coeffs_flow_w)(data);
	  }
	
	  (*ADI_solver)(data->w, 
			&(data->coeffs_w), 
			Nx, Ny, nz, 
			&(data->ADI_vars));
	
	  res_w = compute_norm_glob_res_3D(data->w, &(data->coeffs_w), Nx, Ny, nz);
	  iter++;
	}
      
	/* 2: SOLVE PRESSURE CORRECTION EQUATION */
	use_ps_velocities_OK = 0;

	/* To set pressure correction values to zero was found to improve convergence */
	if(pv_coupling_algorithm != 2){
	  fill_array(data->p_corr, Nx, Ny, Nz, 0);
	}

	coeffs_flow_p_3D(data, use_ps_velocities_OK);
      
	res_pcorr = 1;
	iter = 1;
      
	while((iter < max_iter_ADI) && (res_pcorr > tol_ADI)){
	  (*ADI_solver)(data->p_corr, 
			&(data->coeffs_p), 
			Nx, Ny, Nz, 
			&(data->ADI_vars));
	
	  res_pcorr = compute_norm_glob_res_3D(data->p_corr, 
					       &(data->coeffs_p), Nx, Ny, Nz);
	  iter++;
	}
      
	/* 3: CORRECT PRESSURE AND VELOCITIES */
	update_field_3D(data);

	/* Store values to use with periodic boundary condition, 
	   because p_corr values are set to zero at every iteration */
	store_bnd_p_values_3D(data);

	res_c = continuity_residual_3D(data);
      
	/* Store residuals for inner loop */
	inner_glob_norm[0] = res_u;
	inner_glob_norm[1] = res_v;
	inner_glob_norm[2] = res_w;
	inner_glob_norm[3] = res_c;

	convergence_uvwp_OK = inner_glob_norm[0] < tol_flow &&
	  inner_glob_norm[1] < tol_flow &&
	  inner_glob_norm[2] < tol_flow &&
	  inner_glob_norm[3] < tol_flow;

	iter_uvwp++;
      }
      
      /* 4: SOLVE k-eps EQUATIONS */
      // k-eps loop
      if(turb_model && (outer_iter >= it_to_start_turb)){

	/* Set iteration counter for ke inner loop */
	iter_ke = 0;
	convergence_ke_OK = 0;

	while(iter_ke < max_iter_ke && !convergence_ke_OK){
	
	  /* Filter nodes with low y_plus out of the calculation */
	  if(outer_iter % it_to_update_y_plus == 0){
	    filter_y_plus_3D(data);
	  }

	  // k loop
	  compute_turb_aux_fields_3D(data);	       
	  set_coeffs_k_3D(data);
	  set_bc_turb_k_3D(data);
	
	  res_k = 1;
	  iter = 1;
	  
	  while((iter < max_iter_ADI) && (res_k > tol_ADI)){
	  
	    if(iter % it_for_update_k_coeffs == 0){
	      compute_turb_aux_fields_3D(data);
	      set_coeffs_k_3D(data);
	    }
	  
	    (*ADI_solver)(data->k_turb, &(data->coeffs_k),
			  Nx, Ny, Nz, &(data->ADI_vars));
	    
	    res_k = compute_norm_glob_res_3D(data->k_turb,
					     &(data->coeffs_k), Nx, Ny, Nz);
	    iter++;
	  }
	
	  filter_small_values_omp(data->k_turb, Nx, Ny, Nz, SMALL_VALUE, 1);
	
	  // eps loop
	  compute_turb_aux_fields_3D(data);
	  set_coeffs_eps_3D(data);
	  set_bc_turb_eps_3D(data);
	
	  res_eps = 1;
	  iter = 1;
	  
	  while((iter < max_iter_ADI) && (res_eps > tol_ADI)){
	  
	    if(iter % it_for_update_eps_coeffs == 0){
	      compute_turb_aux_fields_3D(data);
	      set_coeffs_eps_3D(data);
	    }
	  
	    (*ADI_solver)(data->eps_turb, &(data->coeffs_eps),
			  Nx, Ny, Nz, &(data->ADI_vars));
	    
	    res_eps = compute_norm_glob_res_3D(data->eps_turb,
					       &(data->coeffs_eps), Nx, Ny, Nz);
	    iter++;
	  }
       
	  filter_small_values_omp(data->eps_turb, Nx, Ny, Nz, SMALL_VALUE, 1);
	
	  inner_glob_norm[4] = res_k;
	  inner_glob_norm[5] = res_eps;
	
	  convergence_ke_OK = inner_glob_norm[4] < tol_flow &&
	    inner_glob_norm[5] < tol_flow;

	  iter_ke++;
	}

	/* Compute mu_turb_at velocity nodes 
	   for next iteration of the u, v, w, p cycle */
	compute_mu_turb_at_vel_nodes_3D(data);
      
      } // end if(turb_model && (outer_iter >= it_to_start_turb)){

      /* Use coeffs with the latest values of fields to evaluate sub-iter convergence */
      (*coeffs_flow_u)(data);
      (*coeffs_flow_v)(data);
      (*coeffs_flow_w)(data);

      glob_norm[0] = compute_norm_glob_res_3D(data->u, &(data->coeffs_u), nx, Ny, Nz);
      glob_norm[1] = compute_norm_glob_res_3D(data->v, &(data->coeffs_v), Nx, ny, Nz);
      glob_norm[2] = compute_norm_glob_res_3D(data->w, &(data->coeffs_w), Nx, Ny, nz);
      /* res_c hasn't changed from the one calculated inside uvwp inner loop
	 because velocity values are the same */
      glob_norm[3] = inner_glob_norm[3];
      
      if(turb_model && (outer_iter >= it_to_start_turb)){

	/* Use coeffs with the latest values of fields to evaluate outer loop convergence */
	compute_turb_aux_fields_3D(data);
	set_coeffs_k_3D(data);
	set_coeffs_eps_3D(data);

	glob_norm[4] = compute_norm_glob_res_3D(data->k_turb, &(data->coeffs_k), Nx, Ny, Nz);
	glob_norm[5] = compute_norm_glob_res_3D(data->eps_turb, &(data->coeffs_eps), Nx, Ny, Nz);

	convergence_turb_OK = glob_norm[4] < tol_flow
	  && glob_norm[5] < tol_flow;
      }
      else{
	convergence_turb_OK = 1;
      }
      
      sub_iter_convergence_OK = glob_norm[0] < tol_flow	&&
	glob_norm[1] < tol_flow &&
	glob_norm[2] < tol_flow &&
	glob_norm[3] < tol_flow
	&& convergence_turb_OK;

      sub_iter++;

    } while(sub_iter < max_sub_iter && !sub_iter_convergence_OK); // End sub_iter

    data->dt[0] += time_step;
   
    /* Here process action upon signal raise */
    if(action_on_signal_OK){
      process_action_on_signal(data, &convergence_OK, &sub_iter_convergence_OK);

      flow_scheme = data->flow_scheme;
      /* UPWIND scheme */
      if(flow_scheme == 0){
	coeffs_flow_u = coeffs_flow_u_upwind_3D;
	coeffs_flow_v = coeffs_flow_v_upwind_3D;
	coeffs_flow_w = coeffs_flow_w_upwind_3D;
      } 
      else if(flow_scheme == 1){
	/* QUICK scheme */    
	coeffs_flow_u = coeffs_flow_u_quick_3D;
	coeffs_flow_v = coeffs_flow_v_quick_3D;
	coeffs_flow_w = coeffs_flow_w_quick_3D;
      }
      /* van Leer scheme */
      else if(flow_scheme == 2){
	coeffs_flow_u = coeffs_flow_u_TVD_3D;
	coeffs_flow_v = coeffs_flow_v_TVD_3D;
	coeffs_flow_w = coeffs_flow_w_TVD_3D;
      }
      
      /* Update in case of modification the new values */
      it_for_update_coeffs = data->it_for_update_coeffs;
      max_iter_flow = data->max_iter_flow;
      max_iter_ADI = data->max_iter_ADI;
      tol_flow = data->tol_flow;
      tol_ADI = data->tol_ADI;
      
      action_on_signal_OK = 0;
    }

    time_i = time_f;		
    time_f = time_f + elapsed_time(&timer);
    
    if(outer_iter % res_sigma_iter == 0){

      /* Leave outside this IF clause if you need it for convergence criteria
	 Otherwise leave here for optimization purposes */
      glob_norm[6] = mass_io_balance_3D(data);
      
      sigmas->it = outer_iter;

      /* Using glob_norm for transient scheme FIX THIS */
      sigmas_v[0] = glob_norm[0];
      sigmas_v[1] = glob_norm[1];
      sigmas_v[2] = glob_norm[2];
      sigmas_v[3] = glob_norm[3];
      sigmas_v[4] = glob_norm[4];
      sigmas_v[5] = glob_norm[5];
      sigmas_v[6] = glob_norm[6];
      
      print_flow_it_info(msg, SIZE, rstride,
			 flow_solver_name, outer_iter, max_iter_flow,
			 time_i, time_f, turb_model, sigmas,
			 wins);
      
      update_graph_window(data);
      
      write_norm_glob_res(data);
      
    }

#ifdef AVGIV_OK
    write_in_vel_3D(data);
#endif
    
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);
      sigmas->it = outer_iter;
      write_to_log_file(data, msg);
      timer_flag_OK = 0;
    }
    
    /* Updating the server */
    if((launch_server_OK) && 
       (outer_iter % server_update_it == 0)){
      update_server(data);
    }
    
    /* Updating the animation */
    if((animation_flow_OK) && 
       (outer_iter % animation_flow_update_it == 0)){

      center_vel_components(data);
      compute_vel_at_main_cells(data);
      
      update_anims((void *) data);
    }

    /* Saving transient data */
    if( (write_flow_transient_data_OK) &&
	(data->dt[0] >= data->tsaves[0]) &&
	(data->dt[0] <= data->tsaves[1]) ){
      
      if( data->dt[0] >= data->tsaves[0] + transient_save * (transient_counter - 1) ){
	
	save_flow_transient_data(data, filename);
	transient_counter++;
	
      }
    }
 
    outer_iter++;
    
  } while(outer_iter < max_iter_flow && !convergence_OK);

  /* Save iteration */
  sigmas->flow_it = outer_iter - 1;

  if(write_flow_transient_data_OK){
    
    open_check_file(filename, &fp, wins, 1);
    fprintf(fp, "];\n");
    fclose(fp);
      
  }

#ifdef DISPLAY_OK
  wprintw(wins->disp, "Flow simulation time (s) = %g\n", data->dt[0]);
  wrefresh(wins->disp);
#endif

#ifdef AVGIV_OK
  close_in_vel(data);
#endif
  
  solve_flow_post_calc_jobs(data);
  
  return;

}
