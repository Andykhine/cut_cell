#include "include/some_defs.h"

#define NCOLS_MSGS 70

#ifdef DISPLAY_OK

/* SET_Y_LABELS */
int set_y_labels(Wins *wins){

  int i, n;
  int x;
  int ret_value = 0;
  
  const int semilog_OK = wins->semilog_OK;
  const int g_height = wins->g_height;
  const int xmax = XMAX; // wins->xmax; // Max exponent to consider
  const int xmin = XMIN; // wins->xmin; // Min exponent to consider

  float alpha, beta;

  if(semilog_OK){

    if(g_height == 101){

      alpha = -10; beta = 31;
    
      for(x=xmin; x<=xmax; x++){

	n = (int)round(alpha*x+beta);
      
	mvwprintw(wins->graphs, n, 0, "%+3d\n", x);
      
      }
    }
  
    else if(g_height == 51){
    
      alpha = -5; beta = 16;
    
      for(x=xmin; x<=xmax; x++){

	n = (int)round(alpha*x+beta);
      
	mvwprintw(wins->graphs, n, 0, "%+3d\n", x);
      
      }
    }
  
    else{
      // put here odd labels only
      alpha = -2.5; beta = 8.5;
    
      for(x=xmin; x<=xmax; x=x+2){
      
	n = (int)round(alpha*x+beta);
      
	mvwprintw(wins->graphs, n, 0, "%+3d\n", x);
      
      }

    }

  }
  else{
    /* Linear code here */
    // Calculate the row for 0, 25, 50, 75, 100 in absolute window coordinates
    wins->axes[0] = 0 + wins->offset_j;
    for(i=1; i<6; i++){
      wins->axes[i] = wins->axes[i-1] + (wins->inter + 1);
    }

    /* y-axis labels */
    mvwprintw(wins->graphs, wins->axes[0], 0, "1.0\n");
    mvwprintw(wins->graphs, wins->axes[1], 0, "0.8\n");
    mvwprintw(wins->graphs, wins->axes[2], 0, "0.6\n");
    mvwprintw(wins->graphs, wins->axes[3], 0, "0.4\n");
    mvwprintw(wins->graphs, wins->axes[4], 0, "0.2\n");
    mvwprintw(wins->graphs, wins->axes[5], 0, "0.0\n");
    
  }
  
  return ret_value;
  
}

/* PRINT_PHI_IT_INFO */
void print_phi_it_info(const int it, const int max_it,
		       const u_int64_t time_i, const u_int64_t time_f,
		       Sigmas *sigmas, Wins *wins){

  char msg[SIZE];
  char eta[ETA_MSG] = "";
  
  int row, col;

  const double fraction =  (it + 1.0) / (max_it + 1.0);
  
  const u_int64_t elapsed = time_f;
  const u_int64_t estimated_total = elapsed / fraction;
  const u_int64_t delta = estimated_total - elapsed;

  const double *sigmas_v = sigmas->values;
  const double *glob_norm = sigmas->glob_norm;

  memset(eta, '\0', sizeof(char) * ETA_MSG);

  set_eta_array(eta, ETA_MSG, delta);
  
  snprintf(msg, SIZE, "%11s: %7d %13.3e %13.3e %11.3e %11.3e\n"
	   "%12s %7d %*s %11.3e %11.3e",
	   "solve_phi", it, (time_f - time_i) / 1000000.0, time_f/(60 * 1000000.0),
	   sigmas_v[7], sigmas_v[8],
	   "", sigmas->sub_it,
	   ETA_MSG, eta, 
	   glob_norm[7], glob_norm[8]);
  
  getyx(wins->disp, row, col);
  wmove(wins->disp, row-2, col);
  wdeleteln(wins->disp);
  wprintw(wins->disp, "%s\n", msg);
  wrefresh(wins->disp);
  
  return;
}

/* TABLE_HEADER_PHI_IT */
void table_header_phi_it(Wins *wins){

  char msg[SIZE];

  snprintf(msg, SIZE,
	   "%20s %13s %13s %11s %11s\n"
	   "%32s %11s\n",
	   "it/sit", "time_it (s)", "elapsed (min)",
	   "sphi/gphi", "sgL/ggL",
	   "(s)", "(min)");

  wattron(wins->disp, A_BOLD);
  wprintw(wins->disp, "%s\n", msg);
  wattroff(wins->disp, A_BOLD);
  
  return;
}

/* PRINT_FLOW_IT_INFO */
void print_flow_it_info(char *msg, const int msg_size, const int rstride,
			const char *solver_name,
			const int outer_iter, const int max_iter,
			const u_int64_t time_i, const u_int64_t time_f,
			const int turb_model, Sigmas *sigmas,
			Wins *wins){

  char msg2[SIZE];
  char eta[ETA_MSG] = "";

  int row, col;

  const double fraction =  (outer_iter + 1.0) / (max_iter + 1.0);

  const u_int64_t elapsed = time_f;
  const u_int64_t estimated_total = elapsed / fraction;
  const u_int64_t delta = estimated_total - elapsed;

  const double *glob_norm = sigmas->glob_norm;
  
  memset(msg, '\0', sizeof(char) * msg_size);
  memset(eta, '\0', sizeof(char) * ETA_MSG);

  set_eta_array(eta, ETA_MSG, delta);
  
  if(turb_model){
    snprintf(msg, msg_size,
	     "%11s: %7d %13.3e %13.3e %11.3e %11.3e %11.3e %11.3e %11.3e %11.3e %11.3e", 
	     solver_name, outer_iter,
	     (time_f - time_i) / 1000000.0, time_f/(60 * 1000000.0),
	     sigmas->values[0], sigmas->values[1], sigmas->values[2], /* u v w */
	     sigmas->values[3], /* c */
	     sigmas->values[4], sigmas->values[5], /* k e */
	     sigmas->values[6]); /* MB */

    snprintf(msg2, SIZE,
	     "%12s %7d %*s %11.3e %11.3e %11.3e %11s %11.3e %11.3e %11.3e", 
	     "", sigmas->sub_it,
	     ETA_MSG, eta, 
	     glob_norm[0], glob_norm[1], glob_norm[2], /* u v w */
	     "--", /* c */
	     glob_norm[4], glob_norm[5], /* k e */
	     glob_norm[6]); /* MB */
	
  }
  else{
    snprintf(msg, msg_size,
	     "%11s: %7d %13.3e %13.3e %11.3e %11.3e %11.3e %11.3e %11s %11s %11.3e",
	     solver_name, outer_iter,
	     (time_f - time_i) / 1000000.0, time_f/(60 * 1000000.0),
	     sigmas->values[0], sigmas->values[1], sigmas->values[2], /* u v w */
	     sigmas->values[3], /* c */	     
	     "--", "--", /* k e */
	     sigmas->values[6]); /* MB */

    snprintf(msg2, SIZE,
	     "%12s %7d %*s %11.3e %11.3e %11.3e %11s %11s %11s %11.3e",
	     "", sigmas->sub_it,
	     ETA_MSG, eta, 
	     glob_norm[0], glob_norm[1], glob_norm[2], /* u v w */
	     "--", /* c */	     
	     "--", "--", /* k e */
	     glob_norm[6]); /* MB */
  }
  
  getyx(wins->disp, row, col);
  wmove(wins->disp, row-rstride, col);
  wdeleteln(wins->disp);
  wprintw(wins->disp, "%s\n", msg);
  wprintw(wins->disp, "%s\n", msg2);
  wrefresh(wins->disp);
  
  return;
  
}

/* TABLE_HEADER_FLOW_IT */
void table_header_flow_it(char *msg, const int msg_size, Wins *wins){

  const char *flow_model = "Laminar";
  
  const int turb_model = *(wins->turb_model);

  memset(msg, '\0', sizeof(char)*msg_size);

  if(turb_model == 1){
    flow_model = "AKN";
  }
  else if (turb_model == 2){
    flow_model = "CG";
  }
  
  snprintf(msg, msg_size,
	   "%12s %7s %13s %13s %11s %11s %11s %11s %11s %11s %11s\n"
	   "%10s %21s %11s\n",
	   "Model Solver",
	   "it/sit", "time_it (s)", "elapsed (min)",
	   "su/gu", "sv/gv", "sw/gw", "sc/gc", "sk/gk", "se/ge", "sMB/sMB",
	   flow_model,
	   "(s)", "(min)");

  wattron(wins->disp, A_BOLD);
  wprintw(wins->disp, "%s\n", msg);
  wattroff(wins->disp, A_BOLD);
  
  return;
}

/* ERROR_MSG */
void error_msg(Wins *wins, const char *text){
  wattron(wins->disp, COLOR_PAIR(1) | A_BOLD);
  wprintw(wins->disp, "%s%s\n", "ERROR: ", text);
  wattroff(wins->disp, COLOR_PAIR(1) | A_BOLD);
  wrefresh(wins->disp);
  napms(2000);
  destroy_windows(wins);
  fprintf(stderr, "%s%s\n", "ERROR: ", text);
  exit(EXIT_FAILURE);
  return;
}

/* ALERT_MSG */
void alert_msg(Wins *wins, const char *text){
  wprintw(wins->warns, "%s\n", text);
  wrefresh(wins->warns);
  return;
}

/* INFO_MSG */
void info_msg(Wins *wins, const char *text){
  wprintw(wins->msgs, "%s\n", text);
  wrefresh(wins->msgs);
  return;
}

/* PROGRESS_MSG */
void progress_msg(Wins *wins, const char *text, const double per){

  static int row = 0;
  int col = 0;

  if(per > 0.0){
    getyx(wins->prog, row, col);

    row = row-1;
    
    wmove(wins->prog, row, 0);
    wdeleteln(wins->prog);
  }

  wattron(wins->prog, COLOR_PAIR(2) | A_BOLD);  
  mvwprintw(wins->prog, row, 0, "%-*s%5.1f\%\n", NCOLS_MSGS-7, text, per);
  wattroff(wins->prog, COLOR_PAIR(2) | A_BOLD);
  
  getyx(wins->prog, row, col);
  
  wrefresh(wins->prog);
  
  return;
}

/* SERV_MSG */
void serv_msg(Wins *wins, const char *text){
  wattron(wins->msgs, COLOR_PAIR(2) | A_BOLD);
  wprintw(wins->msgs, "%s\n", text);
  wattroff(wins->msgs, COLOR_PAIR(2) | A_BOLD);
  wrefresh(wins->msgs);
  return;
}

/* SET_IT_GRAPH_SIZES */
int set_it_graph_sizes(Wins *wins){

  int ret_value = 0;
  
  const int res_sigma_iter = wins->res_sigma_iter;
  const int semilog_OK = wins->semilog_OK;

  /* Editing it window */
  /* Iteration ranges */
  wins->it_i = 0;
  wins->it_f = (wins->g_width-1)*res_sigma_iter;

  wins->set_i = 0;
  wins->count_range = 0;

  if(!semilog_OK){
    wins->mpow = 0;
    wins->mpow_new = 0;
    
    wins->dy = ((0.2 - 0.0)/(wins->inter + 1)) * pow(10, wins->mpow);
  }
  
  wins->offset_i = 3;
  wins->offset_j = 1;
  
  wins->smax = 1;

  /* Set y labels */
  set_y_labels(wins);
  
  clear_canvas(wins->ch_m, wins->g_width, wins->g_height);
  
  set_title(wins);
  
  draw_canvas(wins);
  
  return ret_value;

}

/* INIT_NCURSES_PROCESSES */
int init_ncurses_processes(Wins *wins, const char *config_filename){

  int ret_value = 0;
  int width, height;
  int g_width, g_height;
  int i, j, index_ch;
  
  /* Initialization of ncurses */
  /*
    if(has_colors() == FALSE){
    printf("There are no color capabilities in  this terminal\n");
    printf("Exit\n");
    exit(EXIT_FAILURE);
    }
  */
    
  initscr();
  start_color();
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_WHITE, COLOR_BLACK);
  init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
  init_pair(4, COLOR_GREEN, COLOR_BLACK);
  init_pair(5, COLOR_YELLOW, COLOR_BLACK);
  
  cbreak(); // To get SIGINT
  
  noecho();
  keypad(stdscr, TRUE);

  /* Dimensions of graphs window */
  height = 27;
  width = COLS-NCOLS_MSGS; // COLS-50; // 117; //103;

  if(LINES-height < 10 || width < 117){
    endwin();
    printf("Not appropiate terminal resolution (%d,%d)\n", LINES, COLS);
    printf("Should be at least (37,167)\n");
    printf("Try reducing font and/or increasing window size\n");
    exit(EXIT_FAILURE);
  }

  if(LINES-102 > 11){
    height = 102;
  }
  else if(LINES-52 > 11){
    height = 52;
  }

  /* Creating windows */
  wins->warns = create_newwin(10, COLS-width, 0, 0);
  wins->prog = create_newwin(5, COLS-width, 10, 0);
  wins->msgs = create_newwin(LINES-25, COLS-width, 15, 0);
  wins->upds = create_newwin(10, COLS-width, LINES-15, 0);

  wins->graphs = create_newwin(height, width, 0, COLS-width);
  wins->disp = create_newwin(LINES-height, width, height, COLS-width);
  
  /* Graph effective sizes */
  g_height = height - 1;
  wins->g_height = g_height;

  g_width = width - 3; // 100;
  wins->g_width = g_width;

  wins->inter = (g_height-6) / 5;
  
  /* These are relative window coordinates */
  wins->ch_m = (chtype *)calloc(g_height*g_width, sizeof(chtype));
  wins->ch_v = (chtype *)calloc(g_height, sizeof(chtype));

  /* Set graph sizes */
  set_it_graph_sizes(wins);
  
  /* Set some data */
  for(j=0; j<g_height; j++){

    wins->ch_v[j] = ' ';
    
    for(i=0; i<g_width; i++){
      
      index_ch = j*g_width+i;

      wins->ch_m[index_ch] = ' ';
      
    }
  }

  refresh();

  scrollok(wins->warns, TRUE);
  /* Editing params window */
  //  mvwprintw(wins->msgs, 0, 0, "Parameters of: %s\n", config_filename);
  scrollok(wins->msgs, TRUE);

  /* Editing msgs window */
  //  mvwprintw(wins->disp, 0, 0, "Messages:\n");
  scrollok(wins->disp, TRUE);
  
  /* Read config file into ncurses param window */
  //  dump_config_file(config_filename, wins);

  init_upds_window_info(wins);

  wbkgd(wins->warns, COLOR_PAIR(1) | A_BOLD);
  //  wbkgd(wins->msgs, COLOR_PAIR(2));
  wbkgd(wins->upds, COLOR_PAIR(4));
  //  wbkgd(wins->graphs, COLOR_PAIR(2));
  //  wbkgd(wins->disp, COLOR_PAIR(2));

  /* Refreshing windows */
  wrefresh(wins->warns);
  wrefresh(wins->msgs);
  wrefresh(wins->upds);
  wrefresh(wins->graphs);
  wrefresh(wins->disp);
  
  return ret_value;
}

/* INIT_UPDS_WINDOW_INFO */
int init_upds_window_info(Wins *wins){

  char msg[SIZE];

  const int Nx = wins->Nx;
  const int Ny = wins->Ny;
  const int Nz = wins->Nz;

  snprintf(msg, SIZE, "%18s(%8d, %8d, %8d)\n%18s%30d",
	   "(Nx, Ny, Nz) = ", Nx, Ny, Nz,
	   "Grid = ", Nx*Ny*Nz);
  wprintw(wins->upds, "%s\n", msg);

  wprintw(wins->upds, "%18s\n", "MEM ALLOCS = ");
  wprintw(wins->upds, "%18s%30s\n", "LOG : ", "--");
  wprintw(wins->upds, "%18s%30s\n", "RESULTS : ", "--");
  wprintw(wins->upds, "%18s%30s\n", "TMP : ", "--");
  wprintw(wins->upds, "%18s%30s\n", "TRANSIENT : ", "--");
  
  wrefresh(wins->upds);

  return 0;
}

/* UPDATE_UPDS_WINDOW */
int update_upds_window(Wins *wins, const int line){

  char msg[40];
  
  struct timeval tv;
  // This pointer should not be freed!!
  struct tm *ptm = NULL;

  /* Obtain the time of day, and convert it to a tm struct. */
  gettimeofday(&tv, NULL);
  ptm = localtime(&tv.tv_sec);
  /* Format the date and time, down to a single second. */
  strftime(msg, sizeof(msg),
	   "%d-%m-%Y %H:%M:%S", ptm);

  wmove(wins->upds, line, 20);
  wclrtoeol(wins->upds);
  wprintw(wins->upds, "%s\n", msg);
  wrefresh(wins->upds);

  return 0;

}

/* UPDATE_GRAPH_WINDOW */
int update_graph_window(Data_Mem *data){

  Wins *wins = &(data->wins);

  const int semilog_OK = wins->semilog_OK;
  
  const double *svalues = wins->svalues;

  if(!semilog_OK){
    /* Get maximum value */
    wins->smax = get_smax(svalues);
    
    /* Check if it necessary a change of scale */
    if( get_scale_for_graph(wins) ){
      wins->mpow = wins->mpow_new;
    }
  }
	
  /* Check if we reach the end of the canvas */
  if( get_range_for_graph(wins) ){
    /* If we do, clear the canvas */
    clear_canvas(wins->ch_m, wins->g_width, wins->g_height);
  }

  set_values_in_column(wins);
  add_to_the_right(wins);

  /* Draw the canvas and set the title */
  draw_canvas(wins);
  set_title(wins);
	
  wrefresh(wins->graphs);
  
  return 0;
}

/* CREATE_NEWWIN */
WINDOW *create_newwin(const int height, const int width,
		      const int starty, const int startx){
  WINDOW *local;

  local = newwin(height, width, starty, startx);
  
  wrefresh(local);

  return local;
}

/* DESTROY_WINDOWS */
void destroy_windows(Wins *wins){

  free(wins->ch_m);
  free(wins->ch_v);

  delwin(wins->warns);
  delwin(wins->msgs);
  delwin(wins->prog);
  delwin(wins->upds);
  delwin(wins->disp);
  delwin(wins->graphs);
		
  endwin();

  return;
}

/* ADD_TO_THE_RIGHT */
void add_to_the_right(Wins *wins){

  chtype *ch_m = wins->ch_m;
  
  const chtype *ch_v = wins->ch_v;

  int j, index_ch;
  
  const int g_width = wins->g_width;
  const int g_height = wins->g_height;
  const int set_i = wins->set_i;
  
  /* Filling the new i column */
  for(j=0; j<g_height; j++){
    index_ch = j*g_width + set_i;
    ch_m[index_ch] = ch_v[j];
  }
  
  return;
}

/* CLEAR_CANVAS */
void clear_canvas(chtype *ch_m, const int g_width, const int g_height){

  int i, j, index_ch;
  
  for(j=0; j<g_height; j++){
    for(i=0; i<g_width; i++){
      
      index_ch = j*g_width+i;
      ch_m[index_ch] = ' ';
      
    }
  }
  
  return;
}

/* CLEAR_VECTOR */
void clear_vector(chtype *ch_v, const int g_height){

  int j;
  
  for(j=0; j<g_height; j++){

    ch_v[j] = ' ';  
    
  }
  
  return;
}

/* DRAW_CANVAS */
void draw_canvas(Wins *wins){
  
  const chtype *ch_m = wins->ch_m;

  int i, j, index_ch;
  
  const int offset_i = wins->offset_i;
  const int offset_j = wins->offset_j;
  
  const int g_width = wins->g_width;
  const int g_height = wins->g_height;
  
  for(j=0; j<g_height; j++){
    for(i=0; i<g_width; i++){
      
      index_ch = j*g_width+i;

      mvwaddch(wins->graphs, j+offset_j, i+offset_i, ch_m[index_ch]);
      
    }
  }

  return;
}

/* SET_VALUES_IN_COLUMN */
int set_values_in_column(Wins *wins){

  chtype *ch_v = wins->ch_v;

  int j;
  int n;

  int err = -1;
  int pos;
  int u_OK = 0;
  int v_OK = 0;
  int w_OK = 0;
  int c_OK = 0;
  int k_OK = 0;
  int e_OK = 0;
  int MB_OK = 0;
  int phi_OK = 0;
  int gL_OK = 0;

  const int g_height = wins->g_height;
  const int semilog_OK = wins->semilog_OK;

  float alpha, beta, x;
  
  const double dy = wins->dy;

  const double *svalues = wins->svalues;
  
  /* Clear previous buffer */
  clear_vector(ch_v, g_height);
  
  if(semilog_OK){
    if(g_height == 101){
      alpha = -10; beta = 31;
    }
    else if(g_height == 51){
      alpha = -5; beta = 16;
    }
    else{
      // put here odd labels only
      alpha = -2.5; beta = 8.5;
    }
     
    // u
    if(!isnan(svalues[0])){
      x = log10((float)svalues[0]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SUCHTYPE;
    }

    // v
    if(!isnan(svalues[1])){
      x = log10((float)svalues[1]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SVCHTYPE;
    }

    // w
    if(!isnan(svalues[2])){
      x = log10((float)svalues[2]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SWCHTYPE;
    }
    
    // c
    if(!isnan(svalues[3])){
      x = log10((float)svalues[3]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SCCHTYPE;
    }

    // k
    if(!isnan(svalues[4])){
      x = log10((float)svalues[4]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SKCHTYPE;
    }

    // e
    if(!isnan(svalues[5])){
      x = log10((float)svalues[5]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SECHTYPE;
    }

    // MB
    if(!isnan(svalues[6])){
      x = log10((float)svalues[6]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SMBCHTYPE;
    }

    // phi
    if(!isnan(svalues[7])){
      x = log10((float)svalues[7]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SPHICHTYPE;
    }

    // gL
    if(!isnan(svalues[8])){
      x = log10((float)svalues[8]);
      n = (int)round(alpha*x+beta);
      // Leaving overflowed points within the canvas
      if(n >= g_height){
	n = g_height;
      }
      if(n <= 1){
	n = 1;
      }
      pos = n-1;
      ch_v[pos] = SGLCHTYPE;
    }
  }
  else{
    
    pos = g_height - 1;
    
    for(j=0; j<g_height; j++){
      
      if(!u_OK){
	if(!isnan(svalues[0]) && svalues[0] <= j*dy){
	  ch_v[pos] = SUCHTYPE;
	  u_OK = 1;
	}
      }
      
      if(!v_OK){
	if(!isnan(svalues[1]) && svalues[1] <= j*dy){
	  ch_v[pos] = SVCHTYPE;
	  v_OK = 1;
	}
      }

      if(!w_OK){
	if(!isnan(svalues[2]) && svalues[2] <= j*dy){
	  ch_v[pos] = SWCHTYPE;
	  w_OK = 1;
	}
      }
      
      if(!c_OK){
	if(!isnan(svalues[3]) && svalues[3] <= j*dy){
	  ch_v[pos] = SCCHTYPE;
	  c_OK = 1;
	}
      }
      
      if(!k_OK){
	if(!isnan(svalues[4]) && svalues[4] <= j*dy){
	  ch_v[pos] = SKCHTYPE;
	  k_OK = 1;
	}
      }
      
      if(!e_OK){
	if(!isnan(svalues[5]) && svalues[5] <= j*dy){
	  ch_v[pos] = SECHTYPE;
	  e_OK = 1;
	}
      }
      
      if(!MB_OK){
	if(!isnan(svalues[6]) && svalues[6] <= j*dy){
	  ch_v[pos] = SMBCHTYPE;
	  MB_OK = 1;
	}
      }
      
      if(!phi_OK){
	if(!isnan(svalues[7]) && svalues[7] <= j*dy){
	  ch_v[pos] = SPHICHTYPE;
	  phi_OK = 1;
	}
      }
      
      if(!gL_OK){
	if(!isnan(svalues[8]) && svalues[8] <= j*dy){
	  ch_v[pos] = SGLCHTYPE;
	  gL_OK = 1;
	}
      }
      
      if(u_OK && v_OK && w_OK && c_OK && k_OK && e_OK && MB_OK && phi_OK && gL_OK){
	break;
      }
      
      pos--;
    }
  }
  
  return err;
}

/* SET_TITLE */
void set_title(Wins *wins){

  char msg[SIZE];
  
  int offset;

  int len = 43;

  const int mpow = wins->mpow;
  const int it_i = wins->it_i;
  const int it_f = wins->it_f;

  const int semilog_OK = wins->semilog_OK;
  const int g_width = wins->g_width;
  const int offset_i = wins->offset_i;

  const int base = g_width - offset_i;
  
  const int turb_model = *(wins->turb_model);

  if(semilog_OK){
    snprintf(msg, SIZE, "Sigmas: semilogy scale, range [%5d, %5d], ",
	     it_i, it_f);
    
    len = 46;
  }
  else{ 
    snprintf(msg, SIZE, "Sigmas: scale 1e%+03d, range [%5d, %5d], ",
	     mpow, it_i, it_f);
    len = 43;
  }
  
  wmove(wins->graphs, 0, 0);
  wdeleteln(wins->graphs);
  winsertln(wins->graphs);

  /* if(!flow_done_OK){ A unified display from the coupled physics incorporation */
    
    if(turb_model){
      offset = base - (len + 5*6 + 5);
      offset /= 2;
      mvwprintw(wins->graphs, 0, offset, msg);
      /* Legends */
      wprintw(wins->graphs, "su "); waddch(wins->graphs, SUCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sv "); waddch(wins->graphs, SVCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sw "); waddch(wins->graphs, SWCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sc "); waddch(wins->graphs, SCCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sk "); waddch(wins->graphs, SKCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "se "); waddch(wins->graphs, SECHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sphi "); waddch(wins->graphs, SPHICHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sgL "); waddch(wins->graphs, SGLCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sMB "); waddch(wins->graphs, SMBCHTYPE);
    }
    else{
      offset = base - (len + 3*6 + 5);
      offset /= 2;
      mvwprintw(wins->graphs, 0, offset, msg);
      /* Legends */
      wprintw(wins->graphs, "su "); waddch(wins->graphs, SUCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sv "); waddch(wins->graphs, SVCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sw "); waddch(wins->graphs, SWCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sc "); waddch(wins->graphs, SCCHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sphi "); waddch(wins->graphs, SPHICHTYPE);
      wprintw(wins->graphs, ", ");
      wprintw(wins->graphs, "sgL "); waddch(wins->graphs, SGLCHTYPE);
      wprintw(wins->graphs, ", ");    
      wprintw(wins->graphs, "sMB "); waddch(wins->graphs, SMBCHTYPE);
    }
    
    //  }

    /*
      else{
      
    offset = base - (len + 8 + 4);
    offset /= 2;
    mvwprintw(wins->graphs, 0, offset, msg);
    wprintw(wins->graphs, "sphi "); waddch(wins->graphs, SPHICHTYPE);
    wprintw(wins->graphs, ", ");
    wprintw(wins->graphs, "sgL "); waddch(wins->graphs, SGLCHTYPE);
    
    }
    */
    
  return;
}

/* GET_SMAX */
double get_smax(const double *svalues){

  double smax;

  smax = pow(10, MPOW_L);

  if(!isnan(svalues[0])){
    smax =  MAX( smax , svalues[0] );
  }
  if(!isnan(svalues[1])){
    smax =  MAX( smax , svalues[1] );
  }
  if(!isnan(svalues[2])){
    smax =  MAX( smax , svalues[2] );
  }
  if(!isnan(svalues[3])){
    smax =  MAX( smax , svalues[3] );
  }
  if(!isnan(svalues[4])){
    smax =  MAX( smax , svalues[4] );
  }
  if(!isnan(svalues[5])){
    smax =  MAX( smax , svalues[5] );
  }
  if(!isnan(svalues[6])){
    smax =  MAX( smax , svalues[6] );
  }
  if(!isnan(svalues[7])){
    smax =  MAX( smax , svalues[7] );
  }
  if(!isnan(svalues[8])){
    smax =  MAX( smax , svalues[8] );
  }

  return smax;

}

/* GET_RANGE_FOR_GRAPH */
int get_range_for_graph(Wins *wins){
  
  /*
    ret_value:
    0 it range remains.
    1 it range changed.
  */
  
  int ret_value = 0;
  
  const int g_width = wins->g_width;
  
  const int it = *(wins->iter);
  const int stride = *(wins->stride);

  int * const it_i = &(wins->it_i);
  int * const it_f = &(wins->it_f);
  int * const count_range = &(wins->count_range);
  int * const set_i = &(wins->set_i);
  
  while(1){
    if( it > (*it_f) ){
      
      (*count_range) = (*count_range) + 1;
      
      *it_i = (*count_range) * g_width * stride;
      *it_f = (*it_i) + (g_width - 1) * stride;
      
      ret_value = 1;
      
    }
    else{
      break;
    }
  }
  
  /* Set the actual column to be changed in the canvas */
  *set_i = (it/stride) - (*count_range) * g_width;
  
  return ret_value;
}

/* GET_SCALE_FOR_GRAPH */
int get_scale_for_graph(Wins *wins){

  /* ret_value:
     0 scale remains.
     1 scale changed.
  */
  
  int ret_value = 0;
  int compute_scale_OK = 0;

  const int mpow = wins->mpow;

  int * const mpow_new = &(wins->mpow_new);

  const double smax = wins->smax;

  double * const dy = &(wins->dy);
  
  *mpow_new = mpow;

  compute_scale_OK = *mpow_new < MPOW_H && *mpow_new > MPOW_L;

  /* This is the zone where is OK to compute the scale
     In other region we are not interested in see changes */
  while(compute_scale_OK){
    
    if(smax <= pow(10, *mpow_new - 1)){
      *mpow_new = *mpow_new - 1;
      ret_value = 1;
    }
    else if(smax > pow(10, *mpow_new)){
      *mpow_new = *mpow_new + 1;
      ret_value = 1;
    }
    else{
      /* We are done */
      break;
    }

    compute_scale_OK = *mpow_new < MPOW_H && *mpow_new > MPOW_L;
    
  }
  
  if(ret_value){
    *dy = (*dy) * ( (pow(10, *mpow_new)) / (pow(10, mpow)) );
  }
  
  return ret_value;
}

/* DUMP_CONFIG_FILE */
void dump_config_file(const char * filename, Wins *wins){
  
  FILE *fp = NULL;

  int ch;

  fp = fopen(filename, "r");
  
  if(fp == NULL){
    wprintw(wins->msgs, "Could not open the config file\n");
    wrefresh(wins->msgs);
    
    return;
  }

  while((ch = fgetc(fp)) != EOF ){
    waddch(wins->msgs, ch);
  }
  
  wrefresh(wins->msgs);
  
  fclose(fp);
  
  return;
}

/* INTERFACE_CELL_COUNT */
void interface_cell_count(Data_Mem *data){

  char msg[SIZE];

  int total_fluid_interface_cells = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  /* If solve_3D_OK = 0, Nz is set to 1 */
  const int Nz = data->Nz;

  const int curves = data->curves;

  Wins *wins = &(data->wins);

  if(curves > 0){
    
    total_fluid_interface_cells =
      data->Dirichlet_boundary_cell_count +
      data->Isoflux_boundary_cell_count +
      data->Conjugate_boundary_cell_count;
    
    /* Header */
    sprintf(msg,
	    "%20s %20s %20s %20s %20s\n",
	    "Phase",
	    "Total cells (%)",
	    "Dirichlet (%)",
	    "Isoflux (%)",
	    "Conjugate (%)");
    
    wattron(wins->disp, A_BOLD);
    wprintw(wins->disp, msg);
    wattroff(wins->disp, A_BOLD);
    
    /* Fluid */
    memset(msg, '\0', sizeof(msg));
    
    sprintf(msg,
	    "%20s %20.2f %20.2f %20.2f %20.2f\n",
	    "Fluid",
	    (double) total_fluid_interface_cells/(Nx*Ny*Nz) * 100,
	    (double) data->Dirichlet_boundary_cell_count/(Nx*Ny*Nz) * 100,
	    (double) data->Isoflux_boundary_cell_count/(Nx*Ny*Nz) * 100,
	    (double) data->Conjugate_boundary_cell_count/(Nx*Ny*Nz) * 100);
    
    wprintw(wins->disp, msg);
    
    /* Solid */
    memset(msg, '\0', sizeof(msg));
    
    sprintf(msg,
	    "%20s %20.2f %20s %20s %20.2f\n",
	    "Solid",
	    (double) data->solid_boundary_cell_count/(Nx*Ny*Nz) * 100,
	    "--",
	    "--",
	    (double) data->solid_boundary_cell_count/(Nx*Ny*Nz) * 100);
    // wattron(wins->disp, A_UNDERLINE);
    wprintw(wins->disp, msg);
    // wattroff(wins->disp, A_UNDERLINE);
    
  }
  
  return;
  
}

/* CUT_CELL_COUNT */
void cut_cell_count(Data_Mem *data){

  char msg[SIZE];

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int curves = data->curves;
  
  const int uss = data->u_slave_count;
  const int vss = data->v_slave_count;
  int wss = data->w_slave_count;
  
  Wins *wins = &(data->wins);

  if(Nz <= 1){
    wss = 0;
  }

  if(curves > 0){
    
    /* Header */
    sprintf(msg,
	    "%20s %20s %20s %20s\n",
	    "Slave Cells",
	    "u (%)",
	    "v (%)",
	    "w (%)");
    
    wattron(wins->disp, A_BOLD);
    wprintw(wins->disp, msg);
    wattroff(wins->disp, A_BOLD);
    
    memset(msg, '\0', sizeof(msg));
    
    sprintf(msg,
	    "%20s %13d (%4.2f) %13d (%4.2f) %13d (%4.2f)\n",
	    "",
	    uss, (float)uss/(Nx*Ny*Nz) * 100,
	    vss, (float)vss/(Nx*Ny*Nz) * 100,
	    wss, (float)wss/(Nx*Ny*Nz) * 100);
    
    wprintw(wins->disp, msg);
  }
  
  return;
  
}
#else

/* Dummies here*/
/* PRINT_PHI_IT_INFO */
void print_phi_it_info(const int it, const int max_it,
		       const u_int64_t time_i, const u_int64_t time_f,
		       Sigmas *sigmas, Wins *wins){
  return;
}

void table_header_phi_it(Wins *wins){
  return;
}

void print_flow_it_info(char *msg, const int msg_size, const int rstride,
			const char *solver_name,
			const int outer_iter, const int max_iter,
			const u_int64_t time_i, const u_int64_t time_f,
			const int turb_model, Sigmas *sigmas,
			Wins *wins){
  return;
  
}

void table_header_flow_it(char *msg, const int msg_size, Wins *wins){
  return;
}

void error_msg(Wins *wins, const char *text){
  printf("ERROR: %s\n", text);
  exit(-1);
  return;
}

void alert_msg(Wins *wins, const char *text){
  printf("ALERT: %s\n", text);
  return;
}

void info_msg(Wins *wins, const char *text){
  printf("%s\n", text);
  return;
}

/* PROGRESS_MSG */
void progress_msg(Wins *wins, const char *text, const double per){
  
  if(per >= 100.0){
    printf("%-*s%5.1f\%\n", NCOLS_MSGS-6, text, per);
  }
  
  return;
}


void serv_msg(Wins *wins, const char *text){
  printf("SERVER: %s\n", text);
  return;
}

int set_it_graph_sizes(Wins *wins){
  return 0;
}

int init_ncurses_processes(Wins *wins, const char *config_filename){
  return 0;
}

int update_graph_window(Data_Mem *data){
  return 0;
}

WINDOW *create_newwin(const int height, const int width,
		      const int starty, const int startx){
  WINDOW *local = NULL;
    
  return local;
}

void destroy_windows(Wins *wins){
  return;
}

void add_to_the_right(Wins *wins){
  return;
}

void clear_canvas(chtype *ch_m, const int g_width, const int g_height){
  return;
}

void clear_vector(chtype *ch_v, const int g_height){
  return;
}

void draw_canvas(Wins *wins){
  return;
}

int set_values_in_column(Wins *wins){
  return 0;
}

void set_title(Wins *wins){
  return;
}

double get_smax(const double *svalues){
  return 1.0;
}

int get_range_for_graph(Wins *wins){
  return 0;
}

int get_scale_for_graph(Wins *wins){
  return 0;
}

void dump_config_file(const char * filename, Wins *wins){
  return;
}

void interface_cell_count(Data_Mem *data){
  return;
}

int set_y_labels(Wins *wins){
  return 0;
}

int update_upds_window(Wins *wins, const int line){
  return 0;
}

void cut_cell_count(Data_Mem *data){
}
#endif
