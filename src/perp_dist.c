#include "include/some_defs.h"

extern const double Lx, Ly, Lz, cell_size, pol_tol;

/* Set this two to true to test L scheme */
#define ONLY_L_OK 1 // if true new scheme, if false legacy code
#define LIM_OK 1 // if true within new code, activate just modern limit adapted functions

#if ONLY_L_OK

/* PERP_DIST */
/*****************************************************************************/
/**

   On success: create Point and Dist text files on results_dir directory. Point contains x coordinates, 
   Dist contain distance from x to curve represented by poly_coeffs.

   @param poly_coeffs: curve coefficients.

   @param x: test point.

   @param cell_size: reference cell size.

   @param curve_is_solid_OK: 1 if curve interior area is solid, 0 if curve interior is fluid.

   @param results_dir: directory where text files are to be stored.

   @return 0 on success, 1 if x is inside curve.
*/
int perp_dist(double *poly_coeffs, double *x, 
	      const double cell_size, const int curve_is_solid_OK, 
	      const char *results_dir){
  
  int ret_value = 0;
  double dist = 0;

  if (polyn(x, poly_coeffs) * pow(-1, 1 + curve_is_solid_OK) < 0){
    ret_value = 1;
  }	
  else{
    dist = calc_dist(x, poly_coeffs, cell_size);
    write_double_to_file(x, 1, 3, "Point", 0, results_dir);
    write_double_to_file(&dist, 1, 1, "Dist", 0, results_dir);
  }

  return ret_value;
}

/* Wrappers */
double polyn(const double *x, const double *pc){

#if LIM_OK
  return polyn_l(x, pc);
#else
  return polyn_q(x, pc);
#endif
}

int normal(double *norm_v, const double *x, const double *pc){

#if LIM_OK
  return normal_l(norm_v, x, pc);
#else
  return normal_q(norm_v, x, pc);
#endif
}

double calc_dist(const double *x, const double *pc, const double cell_size){
  
#if LIM_OK
  return calc_dist_l(x, pc, cell_size);
#else
  return calc_dist_q(x, pc, cell_size);
#endif
}

int int_curv(double *xint, const double *x,
	     const double *norm_v, const double *pc, const double cell_size){

  int ret_value = 0;

  /* Lets try first to impact real q */
  if(int_curv_l(xint, x, norm_v, pc, cell_size) != 0){
    /* Ok could not impact real q, lets try ideal q */
    ret_value = int_curv_q(xint, x, norm_v, pc, cell_size);
  }

  return ret_value;

}

/**

   Gets the intersection point at the curve with aid of the curve factor.

   @param xint: intersection estimation.

   @param x: starting point.

   @param norm_v: vector to intersection.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @param cf: curve factor to adjust curves.

   @return 0 on success.
*/
int int_curv_cf(double *xint, const double *x,
		const double *norm_v, const double *pc, const double cell_size,
		const double cf){

  int ret_value = 0;
  
  /* Lets try first to impact real q */
  if(int_curv_l_cf(xint, x, norm_v, pc, cell_size, cf) != 0){
    /* Ok could not impact real q, lets try ideal q */
    ret_value = int_curv_q_cf(xint, x, norm_v, pc, cell_size, cf);
  }

  return ret_value;

}

int calc_xint(double *xint,
	      const double *x, const double *pc, const double cell_size){

#if LIM_OK
  return calc_xint_l(xint, x, pc, cell_size);
#else
  return calc_xint_q(xint, x, pc, cell_size);
#endif
}

/**

   Evaluates quadric and limits of solid body
   as Eq (38) Kirkpatrick with modifications for intervals

   MODIFIED: 31-07-2020

   @param x: test point

   @param pc: curve coefficients

   @return Evaluation result
*/
double polyn_l(const double *x, const double *pc){

  const int xlim_in_q = isnormal(pc[10]) || isnormal(pc[11]);
  const int ylim_in_q = isnormal(pc[12]) || isnormal(pc[13]);
  const int zlim_in_q = isnormal(pc[14]) || isnormal(pc[15]);

  /* This is the quadric per se */
  double q = 0;
  /* and its displaced version */
  double qa = 0;
  /* This will consider intervals min max of coordinates */
  double result = 0;
  /* The intervals. 
     These should be -inf and +inf for normal behaviour. 
     Finite values otherwise. */
  const double xmin = pc[10], xmax = pc[11];
  const double ymin = pc[12], ymax = pc[13];
  const double zmin = pc[14], zmax = pc[15];

  /* Displaced to lim auxiliary point */
  double xa[3] = {0};

  /* First evaluate the quadric */
  q = polyn_q(x, pc);
  /* We assume normal behaviour here, i.e., q > 0 is fluid, q < 0 solid. 
     curve_factor parameter should correct this otherwise */
  /* Need to copy this value to correctly discriminate */
  qa = q;
  
  /* Normal situation */
  result = q;

  /* If there is a limit we should consider the q at the point of the limit
     only and then fall like a stocking */
  /* So we copy the vector and displace it to where the limit is */
  if(xlim_in_q){
    
    copy_vec(xa, x);

    xa[0] = isnormal(xmax) ? xmax : xmin;

    /* Now evaluate q at the displaced point */
    qa = polyn_q(xa, pc);
  }

  if(ylim_in_q){
    
    copy_vec(xa, x);

    xa[1] = isnormal(ymax) ? ymax : ymin;

    /* Now evaluate q at the displaced point */
    qa = polyn_q(xa, pc);
  }

  if(zlim_in_q){
    
    copy_vec(xa, x);
    
    xa[2] = isnormal(zmax) ? zmax : zmin;
    
    /* Now evaluate q at the displaced point */
    qa = polyn_q(xa, pc);
  }

  /* At this point the value of q should be representative..... so we can
     begin to discriminate if we are within the solid or its projection
     stocking-wise from the limit */
  if(q < 0 && qa < 0){
    /* Inside q solid... Lets see if we are really within solid and correct
       accordingly */
    /* Support for only one coordinate to be finite for the moment */
    
    /* These are for the q < 0 and qa < 0 case */
    if(xlim_in_q){
      result = __add_interval_dist(x[0], q, xmin, xmax);
    }
    else if(ylim_in_q){
      result = __add_interval_dist(x[1], q, ymin, ymax);
    }
    else if(zlim_in_q){
      result = __add_interval_dist(x[2], q, zmin, zmax);
    }

  } // if(q < 0 && qa < 0)
  else if(q * qa < 0){
    /* Here it is covered stocking and non-stocking cases */
    if(xlim_in_q){
      result = __add_interval_dist2(x[0], q, xmin, xmax);
    }
    else if(ylim_in_q){
      result = __add_interval_dist2(x[1], q, ymin, ymax);
    }
    else if(zlim_in_q){
      result = __add_interval_dist2(x[2], q, zmin, zmax);
    }
    
  }
  
  return result;
}

/* POLYN_Q */
/*****************************************************************************/
/**

   Evaluates polynomial Eq (38) Kirkpatrick.

   MODIFIED: 25-07-2020

   @param x: test point

   @param pc: curve coefficients

   @return Evaluation result
*/
double polyn_q(const double *x, const double *pc){
  
  double result;
  
  result = pc[0]*x[0]*x[0] + pc[1]*x[1]*x[1] + pc[2]*x[2]*x[2] + 
    pc[3]*x[0]*x[1] + pc[4]*x[0]*x[2] + pc[5]*x[1]*x[2] + pc[6]*x[0] + 
    pc[7]*x[1] + pc[8]*x[2] + pc[9];

  return result;
}

/**

   Adds the proper distance for quadrics limited by (min,max) values in the
   coordinates. This works when q < 0 and we are in the stocking region
   deciding if we have a solid or fluid point.

   MODIFIED: 19-08-2020

   @param x: Coordinate x, y or z

   @param q: Quadric evaluated at (x,y,z) in the first call say x, later
   should be the incremental result.

   @param xmin: Lower limit of the interval, if -Inf it does not add distance.

   @param xmax: Upper limit of the interval, if +Inf it does not add distance.

   @return Evaluation result
*/
inline double __add_interval_dist(const double x,
				  const double q, const double xmin, const double xmax){

  const int xmin_OK = !isinf(xmin);
  const int xmax_OK = !isinf(xmax);
  
  double result = 0;

  const double dmin = xmin - x;
  const double dmax = x - xmax;

  /* Check x intervals */
  if( xmax_OK && (dmax > sqrt(pol_tol)) ){
    /* Outside... then is fluid with distance to boundary (not q) */
    result = dmax * dmax;
  }
  else if( xmin_OK && (dmin > sqrt(pol_tol))){
    /* Outside... then is fluid with distance to boundary (not q) */
    result = dmin * dmin;
  }
  else{
    /* Within solid for sure... add distance considering intervals when at
       least one xmin or xmax is finite.... */

    /* Check x intervals */
    if( xmax_OK && (fabs(dmax) < sqrt(pol_tol)) ){
      /* On the upper lid */
      result = 0;
    }
    else if( xmin_OK && (fabs(dmin) < sqrt(pol_tol))){
      /* On the lower lid */
      result = 0;
    }
    else{    
      result = q -
	MIN( (xmin_OK ? dmin * dmin : INFINITY) , (xmax_OK ? dmax * dmax : INFINITY) );
    }
  }
  
  return result;
}

/**

   Adds the proper distance for quadrics limited by (min,max) values in the
   coordinates. This works when q*qa < 0 and we are in the stocking and
   non-stocking region for an amiguous to qa fluid point.

   MODIFIED: 19-08-2020

   @param x: Coordinate x, y or z

   @param q: Quadric evaluated at (x,y,z) in the first call say x, later
   should be the incremental result.

   @param xmin: Lower limit of the interval, if -Inf it does not add distance.

   @param xmax: Upper limit of the interval, if +Inf it does not add distance.

   @return Evaluation result
*/
inline double __add_interval_dist2(const double x, const double q, const double xmin, const double xmax){

  const int xmin_OK = !isinf(xmin);
  const int xmax_OK = !isinf(xmax);
  
  double result = 0;
  
  const double dmin = xmin - x;
  const double dmax = x - xmax;

  if( xmax_OK && dmax > sqrt(pol_tol) ){
    result = dmax * dmax;
  }
  else if( xmin_OK && dmin > sqrt(pol_tol) ){
    result = dmin * dmin;
  }
  else{
    result = q;
  }
  
  return result;
}

/**

   Computes normal vector to nearest solid body surface (quadric + limits) on
   point x, x is not necessarily on the curve.

   MODIFIED 27-07-2020

   @param norm_v: array where normal vector is returned.

   @param x: test point.

   @param pc: curve coefficients.

   @return +1, +2, +3 if normal is relative to limits (lid) xyz respectively.
   0 if norm_q is set.  -1, -2, -3 if it is an artificially constructed normal
   vector regarding some limit in xyz respectively, on a wedge of surface
   (intersection of q and a limit), this means that the intersection with the
   solid must follow this and only this path (+ or -), but not modify it.

*/
int normal_l(double *norm_v, const double *x, const double *pc){

  int ret_value = 0;

  /* The polinomial coefficients are not good since when there is rotations
     and translations there may appear as different from 0. We need to use
     intervals */ 
  const int xlim_in_q = isnormal(pc[10]) || isnormal(pc[11]);
  const int ylim_in_q = isnormal(pc[12]) || isnormal(pc[13]);
  const int zlim_in_q = isnormal(pc[14]) || isnormal(pc[15]);

  const int need_for_l_OK = xlim_in_q || ylim_in_q || zlim_in_q;

  double q = 0;
  /* Distance to quadric */
  double dist_q = 0;
  /* Distance to limits */
  double dist_l = INFINITY;
  double dist_u = INFINITY;
  /* Displaced to lim auxiliary point */
  double xa[3] = {0};
  /* Point at the surface for constructing alpha normal */
  double xint[3] = {0};

  const double xmin = pc[10], xmax = pc[11];
  const double ymin = pc[12], ymax = pc[13];
  const double zmin = pc[14], zmax = pc[15];

  /* Get normal to quadric */
  normal_q(norm_v, x, pc);

  if(need_for_l_OK){
  
    /* Need to compute q, for if we are inside the region we need to check the
       shortest distance to boundary for assignment of normal vector */

    /* Evaluate (correctly) the quadric */
    q = polyn_l(x, pc);

    if(q < 0){
      /* Inside q solid... we need distance to quadric and compare it with
	 distance to lids.... it should be safe to use _q function within the
	 solid */
      dist_q = calc_dist_q(x, pc, cell_size);
      /* Support for only one coordinate to be finite for the moment that does
	 not participate in the quadric */
      if(xlim_in_q){
	/* Check x intervals */
	/* Within solid for sure... see if there is one limit value that is
	   finite.... otherwise leave as is (norm_q) */
	dist_l = (isinf(xmin) ? INFINITY : fabs(xmin - x[0]));
	dist_u = (isinf(xmax) ? INFINITY : fabs(x[0] - xmax));
	
	if(dist_u < dist_q){
	  /* Closer to upper lid */
	  norm_v[0] = +1;
	  norm_v[1] = 0;
	  norm_v[2] = 0;
	  
	  ret_value = 1;
	}
	
	if(dist_l < dist_q){
	  /* Closer to lower lid */
	  norm_v[0] = -1;
	  norm_v[1] = 0;
	  norm_v[2] = 0;
	  
	  ret_value = 1;
	}
	
      } // if(xlim_in_q)
      else if(ylim_in_q){
	/* Check y intervals */
	/* Within solid for sure... see if there is one limit value that is
	   finite.... otherwise leave as is (norm_q) */
	dist_l = (isinf(ymin) ? INFINITY : fabs(ymin - x[1]));
	dist_u = (isinf(ymax) ? INFINITY : fabs(x[1] - ymax));
	
	if(dist_u < dist_q){
	  /* Closer to upper lid */
	  norm_v[0] = 0;
	  norm_v[1] = +1;
	  norm_v[2] = 0;
	  
	  ret_value = 2;
	}
	
	if(dist_l < dist_q){
	  /* Closer to lower lid */
	  norm_v[0] = 0;
	  norm_v[1] = -1;
	  norm_v[2] = 0;
	  
	  ret_value = 2;
	}
	
      } // if(ylim_in_q)
      else if(zlim_in_q){
	/* Check z intervals */
	/* Within solid for sure... see if there is one limit value that is
	   finite.... otherwise leave as is (norm_q) */
	dist_l = (isinf(zmin) ? INFINITY : fabs(zmin - x[2]));
	dist_u = (isinf(zmax) ? INFINITY : fabs(x[2] - zmax));
	
	if(dist_u < dist_q){
	  /* Closer to upper lid */
	  norm_v[0] = 0;
	  norm_v[1] = 0;
	  norm_v[2] = +1;
	  
	  ret_value = 3;
	}
	
	if(dist_l < dist_q){
	  /* Closer to lower lid */
	  norm_v[0] = 0;
	  norm_v[1] = 0;
	  norm_v[2] = -1;
	  
	  ret_value = 3;
	}
	
      } // zlim_in_q
      
    } /* if(q < 0) */
    else{
      /* Here the fluid cases (q >= 0) */
      if(xlim_in_q){

	if(x[0] > xmax){
	
	  /* Set point xa within the limit region */
	  copy_vec(xa, x);
	  
	  xa[0] = xmax;
	  
	  if(polyn_q(xa, pc) <= 0){
	    /* Here we are in the stocking region */
	    norm_v[0] = +1;
	    norm_v[1] = 0;
	    norm_v[2] = 0;
	    
	    ret_value = 1;
	  }
	  else{
	    /* Compute point at q surface using xa instead of x with norm_v
	       component nullified, with int_curv_q should be enough */
	    norm_v[0] = 0;
	    /* Optional */
	    normalize_vec(norm_v);
	    
	    int_curv_q(xint, xa, norm_v, pc, cell_size);
	    
	    /* Build vector between xint and x */
	    norm_v[0] = x[0] - xint[0];
	    norm_v[1] = x[1] - xint[1];
	    norm_v[2] = x[2] - xint[2];
	    
	    /* Normalize */	  
	    normalize_vec(norm_v);
	    
	    ret_value = -1;
	  }
	  
	}
	else if(x[0] < xmin){
	  
	  /* Could not find solid surface traveling with norm_v */
	  /* This means norm_v is not good.... so we contruct our own */
	  copy_vec(xa, x);
	  
	  xa[0] = xmin;

	  if(polyn_q(xa, pc) <= 0){
	    /* Here we are in the stocking region */
	    norm_v[0] = -1;
	    norm_v[1] = 0;
	    norm_v[2] = 0;

	    ret_value = 1;
	  }
	  else{
	    /* Compute point at q surface using xa instead of x with norm_v
	       component nullified, with int_curv_q should be enough */
	    norm_v[0] = 0;
	    /* Optional */
	    normalize_vec(norm_v);
	    
	    int_curv_q(xint, xa, norm_v, pc, cell_size);
	    
	    /* Build vector between xint and x */
	    norm_v[0] = x[0] - xint[0];
	    norm_v[1] = x[1] - xint[1];
	    norm_v[2] = x[2] - xint[2];
	    
	    /* Normalize */
	    normalize_vec(norm_v);
	    
	    ret_value = -1;
	  }
	  
	}
	else{
	  /* This includes, middle portion, xmin == -inf, and xmax == +inf */
	  /* Do nothing here normal_q is good  */
	  ret_value = 0;
	}
      
      } //     if(xlim_in_q)

      if(ylim_in_q){

	if(x[1] > ymax){
	
	  /* Could not find solid surface traveling with norm_v */
	  /* This means norm_v is not good.... so we contruct our own */
	  copy_vec(xa, x);
	  
	  xa[1] = ymax;
	  
	  if(polyn_q(xa, pc) <= 0){
	    /* Here we are in the stocking region */
	    norm_v[0] = 0;
	    norm_v[1] = +1;
	    norm_v[2] = 0;
	    
	    ret_value = 2;
	  }
	  else{
	    /* Compute point at q surface using xa instead of x with norm_v
	       component nullified, with int_curv_q should be enough */
	    norm_v[1] = 0;
	    /* Optional */
	    normalize_vec(norm_v);
	    
	    int_curv_q(xint, xa, norm_v, pc, cell_size);
	    
	    /* Build vector between xint and x */
	    norm_v[0] = x[0] - xint[0];
	    norm_v[1] = x[1] - xint[1];
	    norm_v[2] = x[2] - xint[2];
	    
	    /* Normalize */
	    normalize_vec(norm_v);
	    
	    ret_value = -2;
	  }
	  
	}
	else if(x[1] < ymin){

	  /* Could not find solid surface traveling with norm_v */
	  /* This means norm_v is not good.... so we contruct our own */
	  copy_vec(xa, x);
	  
	  xa[1] = ymin;
	  
	  if(polyn_q(xa, pc) <= 0){
	    /* Here we are in the stocking region */
	    norm_v[0] = 0;
	    norm_v[1] = -1;
	    norm_v[2] = 0;
	    
	    ret_value = 2;
	  }
	  else{
	    /* Compute point at q surface using xa instead of x with norm_v
	       component nullified, with int_curv_q should be enough */
	    norm_v[1] = 0;
	    /* Optional */
	    normalize_vec(norm_v);
	    
	    int_curv_q(xint, xa, norm_v, pc, cell_size);
	    
	    /* Build vector between xint and x */
	    norm_v[0] = x[0] - xint[0];
	    norm_v[1] = x[1] - xint[1];
	    norm_v[2] = x[2] - xint[2];
	    
	    /* Normalize */
	    normalize_vec(norm_v);
	    
	    ret_value = -2;
	  }
	  
	}
	else{
	  /* This includes, middle portion, ymin == -inf, and ymax == +inf */
	  /* Do nothing here normal_q is good  */

	  ret_value = 0;
	
	}

      } //     if(ylim_in_q)
	
      /* Here we see the fluid (q > 0) cases */
      if(zlim_in_q){

	if(x[2] > zmax){
	
	  /* Could not find solid surface traveling with norm_v */
	  /* This means norm_v is not good.... so we contruct our own */
	  copy_vec(xa, x);
	  
	  xa[2] = zmax;
	  
	  if(polyn_q(xa, pc) <= 0){
	    /* Here we are in the stocking region */
	    norm_v[0] = 0;
	    norm_v[1] = 0;
	    norm_v[2] = +1;
	    
	    ret_value = 3;
	  }
	  else{
	    /* Compute point at q surface using xa instead of x with norm_v
	       component nullified, with int_curv_q should be enough */
	    norm_v[2] = 0;
	    /* Optional */
	    normalize_vec(norm_v);
	    
	    int_curv_q(xint, xa, norm_v, pc, cell_size);
	    
	    /* Build vector between xint and x */
	    norm_v[0] = x[0] - xint[0];
	    norm_v[1] = x[1] - xint[1];
	    norm_v[2] = x[2] - xint[2];
	    
	    /* Normalize */
	    normalize_vec(norm_v);
	    
	    ret_value = -3;
	  }
	  
	}
	else if(x[2] < zmin){

	  /* Could not find solid surface traveling with norm_v */
	  /* This means norm_v is not good.... so we contruct our own */
	  copy_vec(xa, x);
	  
	  xa[2] = zmin;
	  
	  if(polyn_q(xa, pc) <= 0){
	    /* Here we are in the stocking region */
	    norm_v[0] = 0;
	    norm_v[1] = 0;
	    norm_v[2] = -1;
	    
	    ret_value = 3;
	  }
	  else{
	    /* Compute point at q surface using xa instead of x with norm_v
	       component nullified, with int_curv_q should be enough */
	    norm_v[2] = 0;
	    /* Optional */
	    normalize_vec(norm_v);
	    
	    int_curv_q(xint, xa, norm_v, pc, cell_size);
	    
	    /* Build vector between xint and x */
	    norm_v[0] = x[0] - xint[0];
	    norm_v[1] = x[1] - xint[1];
	    norm_v[2] = x[2] - xint[2];
	    
	    /* Normalize */
	    normalize_vec(norm_v);
	    
	    ret_value = -3;
	  }
	  
	}
	else{
	  /* This includes, middle portion, zmin == -inf, and zmax == +inf */
	  /* Do nothing here normal_q is good  */
	  ret_value = 0;
	}

      } //     if(zlim_in_q)
    
    }

  } // if(need_for_l_OK)
  
  return ret_value;
}

/**

   Computes normal vector to curve represented by pc on point x,
   x is not necessarily on the curve.

   MODIFIED 27-07-2020

   @param norm_v: array where normal vector is returned.

   @param x: test point.

   @param pc: curve coefficients.

   @return 0 on success, -1 otherwise

*/
int normal_q(double *norm_v, const double *x, const double *pc){

  int ret = 0;

  ret = Normal_q(norm_v, x, pc);
  
  /* Normalizacion */
  normalize_vec(norm_v);

  return ret;
}

/**

   Computes normal vector to curve represented by pc on point x, x is not
   necessarily on the curve. This vector is NOT normalized.

   MODIFIED 31-07-2020

   @param norm_v: array where normal vector is returned.

   @param x: test point.

   @param pc: curve coefficients.

   @return 0 on success, -1 otherwise

*/
int Normal_q(double *norm_v, const double *x, const double *pc){

  int ret = -1;
  int normal_numbers_OK = 0;
  double nx = 0, ny = 0, nz = 0;
  /* Vector normal to quadric */
  nx = 2*pc[0]*x[0] + pc[3]*x[1] + pc[4]*x[2] + pc[6];
  ny = pc[3]*x[0] + 2*pc[1]*x[1] + pc[5]*x[2] + pc[7];
  nz = pc[4]*x[0] + pc[5]*x[1] + 2*pc[2]*x[2] + pc[8];

  /* normal_numbers_OK = (isfinite(nx) && isfinite(ny) && isfinite(nz)) && */
  /*   !(iszero(nx) && iszero(ny) && iszero(nz)); */

  /* Only zeros would give NAN when normalizing */
  normal_numbers_OK = !(iszero(nx) && iszero(ny) && iszero(nz));

  norm_v[0] = nx;
  norm_v[1] = ny;
  norm_v[2] = nz;
  
  if(normal_numbers_OK){
    ret = 0;
  }
  
  return ret;
}

/* CALC_DIST_L */
/*****************************************************************************/
/**

   Returns distance between xint and x. Where the normal to the curve on
   point xint pass through x.

   @param x: reference point.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return Distance between x and xint.

*/
double calc_dist_l(const double *x, const double *pc, const double cell_size){

  int count = 1;
  /* If this flag is < 0 do not change the normal vector */
  int flag = 0;

  const int max_iter = 100;

  double xint_old[3];
  double xint[3];
  double norm_v[3];
	
  flag = normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);

  /* Only flag == 0 (normal_q) should be iteratively evaluated since in other
     cases the normal vector is tailored constructed */

  while ((flag == 0) &&
	 (count < max_iter) && 
	 (max_abs_d(xint, xint_old) > 1e-6)){
      
    copy_vec(xint_old, xint);


    flag = normal(norm_v, xint, pc);
      
    int_curv(xint, x, norm_v, pc, cell_size);

    count = count + 1;
      
  }
    
  return vec_dist(x, xint);
}

/* CALC_DIST_Q */
/*****************************************************************************/
/**

   Computes distance to quadric using normal vector to quadric

   MODIFIED 27-07-2020

   @param x: test point.

   @param pc: curve coefficients.

   @param cell_size: reference length of a cell

   @return distance to quadric

*/
double calc_dist_q(const double *x, const double *pc, const double cell_size){
  
  int count = 1;

  const int max_iter = 100;

  double xint_old[3];
  double xint[3];
  double norm_v[3];
	
  normal_q(norm_v, x, pc);
  int_curv_q(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);
    
  while ((count < max_iter) && 
	 (max_abs_d(xint, xint_old) > 1e-6)){

    copy_vec(xint_old, xint);
    normal_q(norm_v, xint, pc);
    int_curv_q(xint, x, norm_v, pc, cell_size);
    count = count + 1;

  }

  return vec_dist(x, xint);
}

/* INT_CURV_L */
/*****************************************************************************/
/**

   Find intersection between curve represented by pc and straight line that pass
   through x. The straight line is defined using norm_v, which is assumed to point
   OUTSIDE the curve (from f(x) < 0 to f(x) > 0).

   @param xint: array where intersection is to be stored.

   @param x: reference point.

   @param norm_v: normal vector to curve represented by pc.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return 0 on success, 1 otherwise.
*/
int int_curv_l(double *xint, const double *x,
	       const double *norm_v, const double *pc, const double cell_size){

  int d_flag = 0;
  int l_flag = 0;
  int u_flag = 0;
  int count = 1;
  int ret_value = 0;
  
  const int max_iter = 20000;
  
  double step = 0.05 * cell_size;
  double cum_step = 0;
  double F = 0;
  double F_old = 0;
  /* To return to this point in the improved searching algorithm */
  double xstart[3] = {0};

  /* Initial values */
  copy_vec(xint, x);
  copy_vec(xstart, x);
  
  F = polyn(xint, pc);

  /* Determinacion de interseccion */
  while (fabs(F) > pol_tol){

    F_old = F;

    if (F > 0){
      u_flag = 1;
      
      if (l_flag){step = step * 0.5;}
      
      adds_vec_sca(xint, norm_v, -step);
      cum_step -= step;
    }
    else{
      
      l_flag = 1;
      
      if (u_flag){step = step * 0.5;}
      
      adds_vec_sca(xint, norm_v, step);
      cum_step += step;
    }

    F = polyn(xint, pc);
    
    /* If new F value is larger than the old one we can be, within the
       precision of the method, traveling past the xint point determined in
       cases for normal flag < 0. If that is the case we need to revert search
       direction taking a smaller step or jump back with huge step and go from
       there with smaller steps */
    d_flag = (fabs(F) - fabs(F_old)) > 0 ? 1 : 0;

    if(d_flag){
      /* Reset position */
      copy_vec(xint, xstart);
      /* Travel 45 % */
      adds_vec_sca(xint, norm_v, 0.45 * cum_step);
      /* Reset starting point */
      copy_vec(xstart, xint);
      /* Compute new F for new xint */
      F = polyn(xint, pc);
      /* Make steps smaller */
      step = step * 0.4;
      /* Reset cumulative step */
      cum_step = 0;
      /* Reset flags */
      u_flag = 0;
      l_flag = 0;
    }
    
    count++;

    if(count >= max_iter){
      ret_value = 1;
      break;
    }

    /* Stop if xint is out of bounds */
    if(!__checks_bnd(xint)){
      ret_value = -99;
      break;
    }
    
  } 

  return ret_value;
}

/* INT_CURV_Q */
/*****************************************************************************/
/**

   Find intersection between curve represented by pc and straight line that pass
   through x. The straight line is defined using norm_v, which is assumed to point
   OUTSIDE the curve (from f(x) < 0 to f(x) > 0).

   @param xint: array where intersection is to be stored.

   @param x: reference point.

   @param norm_v: normal vector to curve represented by pc.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return 0 on success, 1 otherwise.
*/
int int_curv_q(double *xint, const double *x,
	       const double *norm_v, const double *pc, const double cell_size){

  int d_flag = 0;
  int l_flag = 0;
  int u_flag = 0;
  int count = 1;
  int ret_value = 0;
  
  const int max_iter = 20000;
  
  double step = 0.05*cell_size;
  double cum_step = 0;
  double F = 0;
  double F_old = 0;
  /* To return to this point in the improved searching algorithm */
  double xstart[3] = {0};
  
  /* Initial values */
  copy_vec(xint, x);
  copy_vec(xstart, x);
  
  F = polyn_q(xint, pc);
  
  /* Determinacion de interseccion */
  while (fabs(F) > pol_tol){

    F_old = F;

    if (F > 0){
      u_flag = 1;
      
      if (l_flag){step = step*0.5;}
      
      adds_vec_sca(xint, norm_v, -step);
      cum_step -= step;
    }
    else{
      
      l_flag = 1;
      
      if (u_flag){step = step*0.5;}
      
      adds_vec_sca(xint, norm_v, step);
      cum_step += step;
    }

    F = polyn_q(xint, pc);

    /* If new F value is larger than the old one we can be, within the
       precision of the method, traveling past the xint point determined in
       cases for normal flag < 0. If that is the case we need to revert search
       direction taking a smaller step or jump back with huge step and go from
       there with smaller steps */
    d_flag = (fabs(F) - fabs(F_old)) > 0 ? 1 : 0;

    if(d_flag){
      /* Reset position */
      copy_vec(xint, xstart);
      /* Travel 45 % */
      adds_vec_sca(xint, norm_v, 0.45*cum_step);
      /* Reset starting point */
      copy_vec(xstart, xint);
      /* Compute new F for new xint */
      F = polyn(xint, pc);
      /* Make steps smaller */
      step = step*0.4;
      /* Reset cumulative step */
      cum_step = 0;
      /* Reset flags */
      u_flag = 0;
      l_flag = 0;
    }
    
    count++;

    if(count >= max_iter){
      ret_value = 1;
      break;
    }

    /* Stop if xint is out of bounds */
    if(!__checks_bnd(xint)){
      ret_value = -99;
      break;
    }
    
  }

  return ret_value;
}

/* INT_CURV_L_CF */
/*****************************************************************************/
/**

   Find intersection between curve represented by pc and straight line that pass
   through x. The straight line is defined using norm_v, which is assumed to point
   OUTSIDE the curve (from f(x) < 0 to f(x) > 0).

   @param xint: array where intersection is to be stored.

   @param x: reference point.

   @param norm_v: normal vector to curve represented by pc.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @param cf: curve factor to adjust curves.

   @return 0 on success, 1 otherwise.
*/
int int_curv_l_cf(double *xint, const double *x,
		  const double *norm_v, const double *pc,
		  const double cell_size, const double cf){

  int d_flag = 0;
  int l_flag = 0;
  int u_flag = 0;
  int count = 1;
  int ret_value = 0;
  
  const int max_iter = 20000;
  
  double step = 0.05 * cell_size;
  double cum_step = 0;
  double F = 0;
  double F_old = 0;
  /* To return to this point in the improved searching algorithm */
  double xstart[3] = {0};

  /* Initial values */
  copy_vec(xint, x);
  copy_vec(xstart, x);
  
  F = polyn(xint, pc) * cf;

  /* Determinacion de interseccion */
  while (fabs(F) > pol_tol){

    F_old = F;

    if (F > 0){
      u_flag = 1;
      
      if (l_flag){step = step * 0.5;}
      
      adds_vec_sca(xint, norm_v, -step);
      cum_step -= step;
    }
    else{
      
      l_flag = 1;
      
      if (u_flag){step = step * 0.5;}
      
      adds_vec_sca(xint, norm_v, step);
      cum_step += step;
    }

    F = polyn(xint, pc) * cf;
    
    /* If new F value is larger than the old one we can be, within the
       precision of the method, traveling past the xint point determined in
       cases for normal flag < 0. If that is the case we need to revert search
       direction taking a smaller step or jump back with huge step and go from
       there with smaller steps */
    d_flag = (fabs(F) - fabs(F_old)) > 0 ? 1 : 0;

    if(d_flag){
      /* Reset position */
      copy_vec(xint, xstart);
      /* Travel 45 % */
      adds_vec_sca(xint, norm_v, 0.45 * cum_step);
      /* Reset starting point */
      copy_vec(xstart, xint);
      /* Compute new F for new xint */
      F = polyn(xint, pc) * cf;
      /* Make steps smaller */
      step = step * 0.4;
      /* Reset cumulative step */
      cum_step = 0;
      /* Reset flags */
      u_flag = 0;
      l_flag = 0;
    }
    
    count++;

    if(count >= max_iter){
      ret_value = 1;
      break;
    }

    /* Stop if xint is out of bounds */
    if(!__checks_bnd(xint)){
      ret_value = -99;
      break;
    }
    
  } 

  return ret_value;
}

/* INT_CURV_Q_CF */
/*****************************************************************************/
/**

   Find intersection between curve represented by pc and straight line that pass
   through x. The straight line is defined using norm_v, which is assumed to point
   OUTSIDE the curve (from f(x) < 0 to f(x) > 0).

   @param xint: array where intersection is to be stored.

   @param x: reference point.

   @param norm_v: normal vector to curve represented by pc.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @param cf: curve factor to adjust curves.

   @return 0 on success, 1 otherwise.
*/
int int_curv_q_cf(double *xint, const double *x,
		  const double *norm_v, const double *pc,
		  const double cell_size, const double cf){

  int d_flag = 0;
  int l_flag = 0;
  int u_flag = 0;
  int count = 1;
  int ret_value = 0;
  
  const int max_iter = 20000;
  
  double step = 0.05*cell_size;
  double cum_step = 0;
  double F = 0;
  double F_old = 0;
  /* To return to this point in the improved searching algorithm */
  double xstart[3] = {0};
  
  /* Initial values */
  copy_vec(xint, x);
  copy_vec(xstart, x);
  
  F = polyn_q(xint, pc) * cf;
  
  /* Determinacion de interseccion */
  while (fabs(F) > pol_tol){

    F_old = F;

    if (F > 0){
      u_flag = 1;
      
      if (l_flag){step = step*0.5;}
      
      adds_vec_sca(xint, norm_v, -step);
      cum_step -= step;
    }
    else{
      
      l_flag = 1;
      
      if (u_flag){step = step*0.5;}
      
      adds_vec_sca(xint, norm_v, step);
      cum_step += step;
    }

    F = polyn_q(xint, pc) * cf;

    /* If new F value is larger than the old one we can be, within the
       precision of the method, traveling past the xint point determined in
       cases for normal flag < 0. If that is the case we need to revert search
       direction taking a smaller step or jump back with huge step and go from
       there with smaller steps */
    d_flag = (fabs(F) - fabs(F_old)) > 0 ? 1 : 0;

    if(d_flag){
      /* Reset position */
      copy_vec(xint, xstart);
      /* Travel 45 % */
      adds_vec_sca(xint, norm_v, 0.45*cum_step);
      /* Reset starting point */
      copy_vec(xstart, xint);
      /* Compute new F for new xint */
      F = polyn(xint, pc) * cf;
      /* Make steps smaller */
      step = step*0.4;
      /* Reset cumulative step */
      cum_step = 0;
      /* Reset flags */
      u_flag = 0;
      l_flag = 0;
    }
    
    count++;

    if(count >= max_iter){
      ret_value = 1;
      break;
    }

    /* Stop if xint is out of bounds */
    if(!__checks_bnd(xint)){
      ret_value = -99;
      break;
    }
    
  }

  return ret_value;
}

/* __CHECKS_BND */
/*****************************************************************************/
/**

   Checks if point is within the computational domain, i.e., 0 <= x <= Lx, etc.

   A tolerance is added to account for rounding errors in mesh creation.

   @param x: point.
   
   @return 1 on success, 0 otherwise.
*/
inline int __checks_bnd(const double *x){
  int ret_value = 1;

  const int out_of_bounds_OK = (x[0] < 0 - 0.001*Lx) || (x[0] > 1.001*Lx) ||
    (x[1] < 0 - 0.001*Ly) || (x[1] > 1.001*Ly) ||
    (x[2] < 0 - 0.001*Lz) || (x[2] > 1.001*Lz);

  if(out_of_bounds_OK){
    ret_value = 0;
  }

  return ret_value;
}


/* CALC_XINT_L */
/*****************************************************************************/
/**

   Finds point over curve whose normal pass through x and returns it.  In
   every iteration it computes normal vectors starting from x as a first
   approximation and then xint candidates.

   @param xint: array where intersection is to be stored.

   @param x: reference point.
   
   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return 0 on success.
*/
int calc_xint_l(double *xint,
		const double *x, const double *pc, const double cell_size){

  int ret_value = 0;
  int count = 1;

  const int max_iter = 100;

  double xint_old[3];
  double norm_v[3];
	
  normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);
    
  while ((count < max_iter) && 
	 (max_abs_d(xint, xint_old) > 1e-6)){

    copy_vec(xint_old, xint);
    normal(norm_v, xint, pc);
    int_curv(xint, x, norm_v, pc, cell_size);
    count = count + 1;

  }

  return ret_value;
}

/* CALC_XINT_Q */
/*****************************************************************************/
/**

   Finds point over curve whose normal pass through x and returns it.  In
   every iteration it computes normal vectors starting from x as a first
   approximation and then xint candidates.

   @param xint: array where intersection is to be stored.

   @param x: reference point.
   
   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return 0 on success.
*/
int calc_xint_q(double *xint,
		const double *x, const double *pc, const double cell_size){

  int ret_value = 0;
  int count = 1;

  const int max_iter = 100;

  double xint_old[3];
  double norm_v[3];
	
  normal_q(norm_v, x, pc);
  int_curv_q(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);
    
  while ((count < max_iter) && 
	 (max_abs_d(xint, xint_old) > 1e-6)){

    copy_vec(xint_old, xint);
    normal_q(norm_v, xint, pc);
    int_curv_q(xint, x, norm_v, pc, cell_size);
    count = count + 1;

  }

  return ret_value;
}

/* CALC_DIST_FAR */
/*****************************************************************************/
/**

   Returns distance between xint and x. Where the normal to the curve on
   point xint pass through x. Used on wall distance determination.

   @param x: reference point.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return distance between x and xint.

*/
double calc_dist_far(const double *x, const double *pc, const double cell_size){

  int count = 1;

  const int max_iter = 100;

  double est_dist;

  const double approx_factor = 0.5;

  double xint_old[3];
  double xint[3];
  double norm_v[3];
  double x_start[3] = {0};
	
  normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);
    
  while ((count < max_iter) && 
	 (max_abs_d(xint, xint_old) > 1e-6)){

    copy_vec(xint_old, xint);
    normal(norm_v, xint, pc);
    est_dist = vec_dist(x, xint);
    if(polyn(x, pc) > 0){
      x_start[0] = -approx_factor * norm_v[0] * est_dist + x[0];
      x_start[1] = -approx_factor * norm_v[1] * est_dist + x[1];
      x_start[2] = -approx_factor * norm_v[2] * est_dist + x[2];
    }
    else{
      x_start[0] = approx_factor * norm_v[0] * est_dist + x[0];
      x_start[1] = approx_factor * norm_v[1] * est_dist + x[1];
      x_start[2] = approx_factor * norm_v[2] * est_dist + x[2];
    }
    int_curv(xint, x_start, norm_v, pc, cell_size);
    count = count + 1;
  }

  return vec_dist(x, xint);
}

/* CALC_DIST_NORM */
/*****************************************************************************/
/**

   Returns distance between xint and x, and normal vector at point
   xint. Where the normal to the curve on point xint pass through x.

   @param x: reference point.

   @param pc: curve coefficients.

   @param d: value where distance is to be stored.

   @param norm_v: array where normal vector is to be stored.

   @param cell_size: reference cell size.

   @return ret_value not implemented.

*/
void calc_dist_norm(const double *x, const double *pc, double *d, double *norm_v, const double cell_size){
  
  int count = 1;
  
  const int max_iter = 100;
  
  double xint_old[3];
  double xint[3];
  
  normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);

  while ((count < max_iter) && (max_abs_d(xint, xint_old) > 1e-8)){
    copy_vec(xint_old, xint);
    normal(norm_v, xint, pc);
    int_curv(xint, x, norm_v, pc, cell_size);
    count = count + 1;
  }

  *d = vec_dist(x, xint);

  return;
}

/* MAX_ABS_D */
/*****************************************************************************/
/**

   Returns max absolute difference between components of vectos x1 and x2.
   max_d = max(abs(x1[i] - x2[i]))

   @param x1: first vector.

   @param x2: second vector.

   @return max_d
*/
double max_abs_d(const double *x1, const double *x2){

  int i;
  double max_d = 0;

  for (i=0; i<3; i++){
    if (fabs(x1[i] - x2[i]) > max_d){
      max_d = fabs(x1[i] - x2[i]);
    }
  }

  return max_d;
}

/* VEC_DIST */
/*****************************************************************************/
/**

   Returns magnitude of difference between vectors x1 and x2.

   @param x1: first vector.
   
   @param x2: second vector.

   @return sqrt(sum((x1[i] - x2[i])**2))
*/
double vec_dist(const double *x1, const double *x2){
  
  double result;
  
  result = sqrt(powf(x1[0] - x2[0], 2) + powf(x1[1] - x2[1], 2) + powf(x1[2] - x2[2], 2));
  return result;
}

/* SUBS_SCALAR */
/*****************************************************************************/
/**
   Returns vector x after substraction of scalar value to every component.

   @param x: original vector.

   @param scalar: scalar to be substracted.

   @return ret_value not implemented.
*/
void subs_scalar(double *x, const double scalar){
  
  int i;
  
  for (i = 0; i < 3; i++){
    x[i] = x[i] - scalar;
  }
  return;
}

/* ADDS_VEC_SCA */ 
/*****************************************************************************/
/**
   
   Returns vector x1 after addition of x2 * scalar.
   
   @param x1: starting vector.
   
   @param x2: vector used in addition.

   @param scalar: value used in addition.

   @return ret_value not implemented.
*/
void adds_vec_sca(double *x1, const double *x2, const double scalar){

  int i;
  
  for (i = 0; i < 3; i++){
    x1[i] = x1[i] + x2[i] * scalar;
  }
  return;
}

/* COPY_VEC */
/*****************************************************************************/
/**

   Copy the value of vector x2 on vector x1.

   @param x1: first vector.

   @param x2: second vector.

   @return ret_value not implemented.
*/
void copy_vec(double *x1, const double *x2){

  int i;
  
  for (i = 0; i < 3; i++){
    x1[i] = x2[i];
  }
  return;
}

/* AVG_VEC */
/*****************************************************************************/
/**
   Returns on x_result the average of x0 and x1.

   @param x_result: vector where result is to be stored.

   @param x0: first vector.

   @param x1: second vector.
   
   @return ret_value not implemented.
*/
void avg_vec(double *x_result, const double *x0, const double *x1){

  int i;

  for(i=0; i<3; i++){
    x_result[i] = 0.5 * (x0[i] + x1[i]);
  }

  return;
}

/* BISECTION */
/*****************************************************************************/
/**
   
   Locates a root int the interval [x0, x1] for the polynomial defined by pc and
   returns it on xint.

   @param xint: vector where root is stored.

   @param x0: first vector.

   @param x1: second vector.

   @param pc: curve coefficients.

   @return ret_value not implemented.
*/
void bisection(double *xint, const double *x0, const double *x1, const double *pc){
  
  int iter = 0;
  
  const int max_iter = 30;

  double f_aux0, f_aux1, f_test;
  double x_aux0[3];
  double x_aux1[3];
  double x_test[3] = {0};
  
  const double f_tol = pol_tol;

  /* Initial check for root */

  f_aux0 = polyn(x0, pc);
  f_aux1 = polyn(x1, pc);
  
  if(fabs(f_aux0) < f_tol){
    copy_vec(xint, x0);
    return;
  }
  else if(fabs(f_aux1) < f_tol){
    copy_vec(xint, x1);
    return;
  }

  avg_vec(x_test, x0, x1);
  f_test = polyn(x_test, pc);
  
  if(fabs(f_test) < f_tol){
    copy_vec(xint, x_test);
    return;
  }

  copy_vec(x_aux0, x0);
  copy_vec(x_aux1, x1);

  /* Determinacion de interseccion */
  
  while (iter <= max_iter){

    if(f_test > 0){
      if(f_aux1 > 0){
	copy_vec(x_aux1, x_test);
	f_aux1 = polyn(x_aux1, pc);
      }
      else{
	copy_vec(x_aux0, x_test);
	f_aux0 = polyn(x_aux0, pc);
      }
    }
    else{
      if(f_aux1 > 0){
	copy_vec(x_aux0, x_test);
	f_aux0 = polyn(x_aux0, pc);
      }
      else{
	copy_vec(x_aux1, x_test);
	f_aux1 = polyn(x_aux1, pc);
      }
    }
    
    avg_vec(x_test, x_aux0, x_aux1);
    f_test = polyn(x_test, pc);
    
    if(fabs(f_test) < f_tol){
      copy_vec(xint, x_test);
      return;
    }
    
    iter += 1;
  }

  copy_vec(xint, x_test);

  return;
}

/* FIND_SOLID_AREA */
/*****************************************************************************/
/**
   Find area of solid and its normal vector located inside a cut velocity cell
   From: An Adaptive Cartesian Grid Method for Unsteady Compressible Flow in Irregular Regions.
  
   @param norm_v: array where normal vector is returned.
  
   @param solid_area: magnitude of solid area.

   @param area_x: difference between areas perpendicular to x axis.

   @param area_y: difference between areas perpendicular to y axis.
  
   @param area_z: difference between areas perpendicular to z axis.

   @param max_area: maximum solid area that defines search interval.
*/

void find_solid_area(double *norm_v, double *solid_area, const double area_x, const double area_y,
		     const double area_z, const double max_area){


  int iter = 0;
  
  const int max_iter = 50;

  double f_aux0, f_aux1, f_test;

  double solid_area_0 = max_area;
  double solid_area_1 = 0.001 * max_area;
  double solid_area_test = 0;
  
  double norm_v_0[3];
  double norm_v_1[3];
  double norm_v_test[3];
  
  const double f_tol = pol_tol;

  /* Initial check for root */

  norm_v_0[0] = area_x / solid_area_0;
  norm_v_0[1] = area_y / solid_area_0;
  norm_v_0[2] = area_z / solid_area_0;
  f_aux0 = 1 - vec_mag(norm_v_0);

  norm_v_1[0] = area_x / solid_area_1;
  norm_v_1[1] = area_y / solid_area_1;
  norm_v_1[2] = area_z / solid_area_1;
  f_aux1 = 1 - vec_mag(norm_v_1);

  if(fabs(f_aux0) < f_tol){    
    copy_vec(norm_v, norm_v_0);
    *solid_area = solid_area_0;
    return;
  }
  else if(fabs(f_aux1) < f_tol){
    copy_vec(norm_v, norm_v_1);
    *solid_area = solid_area_1;
    return;
  }

  solid_area_test = 0.5 * (solid_area_0 + solid_area_1);

  norm_v_test[0] = area_x / solid_area_test;
  norm_v_test[1] = area_y / solid_area_test;
  norm_v_test[2] = area_z / solid_area_test;
  f_test = 1 - vec_mag(norm_v_test);

  if(fabs(f_test) < f_tol){
    copy_vec(norm_v, norm_v_test);
    *solid_area = solid_area_test;
    return;
  }

  /* Determinacion de interseccion */
  
  while(iter <= max_iter){

    if(f_test > 0){
      if(f_aux1 > 0){
	solid_area_1 = solid_area_test;
	norm_v_1[0] = area_x / solid_area_1;
	norm_v_1[1] = area_y / solid_area_1;
	norm_v_1[2] = area_z / solid_area_1;
	f_aux1 = 1 - vec_mag(norm_v_1);
      }
      else{
	solid_area_0 = solid_area_test;
	norm_v_0[0] = area_x / solid_area_0;
	norm_v_0[1] = area_y / solid_area_0;
	norm_v_0[2] = area_z / solid_area_0;
	f_aux0 = 1 - vec_mag(norm_v_0);
      }
    }
    else{
      if(f_aux1 > 0){
	solid_area_0 = solid_area_test;
	norm_v_0[0] = area_x / solid_area_0;
	norm_v_0[1] = area_y / solid_area_0;
	norm_v_0[2] = area_z / solid_area_0;
	f_aux0 = 1 - vec_mag(norm_v_0);
      }
      else{
	solid_area_1 = solid_area_test;
	norm_v_1[0] = area_x / solid_area_1;
	norm_v_1[1] = area_y / solid_area_1;
	norm_v_1[2] = area_z / solid_area_1;
	f_aux1 = 1 - vec_mag(norm_v_1);
      }
    }
    
    solid_area_test = 0.5 * (solid_area_0 + solid_area_1);

    norm_v_test[0] = area_x / solid_area_test;
    norm_v_test[1] = area_y / solid_area_test;
    norm_v_test[2] = area_z / solid_area_test;
    f_test = 1 - vec_mag(norm_v_test);
    
    if(fabs(f_test) < f_tol){
      copy_vec(norm_v, norm_v_test);
      *solid_area = solid_area_test;
      return;
    }
    
    iter += 1;
  }

  copy_vec(norm_v, norm_v_test);
  *solid_area = solid_area_test;

  
  return;
}

/* VEC_MAG */
/*****************************************************************************/
/**  
     @param vec: vector whose magnitud is to be calculated.

     @return Magnitude of vector vec.

*/
double vec_mag(const double *vec){
  
  return sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
}

/* NORMALIZE_VEC */
/*****************************************************************************/
/**  
     Returns vector vec after normalization.

     @param vec: vector to be normalized.

     @return ret_value not implemented.

*/
void normalize_vec(double *vec){

  int i;
  
  double mag;

  mag = sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);

  for(i=0; i<3; i++){
    vec[i] = vec[i] / mag;
  }
  
  return;
}

/* DIFF_VEC */
/*****************************************************************************/
/**

   Returns on x0 the difference between vectors x1 and x2.

   @param x0: vector where result is to be stored.

   @param x1: first vector.
   
   @param x2: second vector.

   @return ret_value not implemented.
*/
void diff_vec(double *x0, const double *x1, const double *x2){

  int i;
  
  for(i=0; i<3; i++){
    x0[i] = x1[i] - x2[i];
  }
  
  return;
}

/* SCALE_VEC */
/*****************************************************************************/
/**

   Returns vector x after every component is multiplied by scalar value.

   @param x: original vector.

   @param value: value used to scale.

   @return ret_value not implemented.
*/
void scale_vec(double *x, const double value){

  int i;

  for(i=0; i<3; i++){
    x[i] = x[i] * value;
  }
}












#else

/* Aqui va el archivo perp_dist.c antes de los cambios peligrosos */

/* PERP_DIST */
/*****************************************************************************/
/**

   On success: create Point and Dist text files on results_dir directory. Point contains x coordinates, 
   Dist contain distance from x to curve represented by poly_coeffs.

   @param poly_coeffs: curve coefficients.

   @param x: test point.

   @param cell_size: reference cell size.

   @param curve_is_solid_OK: 1 if curve interior area is solid, 0 if curve interior is fluid.

   @param results_dir: directory where text files are to be stored.

   @return 0 on sucess, 1 if x is inside curve.
*/
int perp_dist(double *poly_coeffs, double *x, 
	      const double cell_size, const int curve_is_solid_OK, 
	      const char *results_dir){
  
  int ret_value = 0;
  double dist = 0;

  if (polyn(x, poly_coeffs) * pow(-1, 1 + curve_is_solid_OK) < 0){
    ret_value = 1;
  }	
  else{
    dist = calc_dist(x, poly_coeffs, cell_size);
    write_double_to_file(x, 1, 3, "Point", 0, results_dir);
    write_double_to_file(&dist, 1, 1, "Dist", 0, results_dir);
  }

  return ret_value;
}

/* POLYN */
/*****************************************************************************/
/**

   Evaluates polynomial Ec (38) Kirkpatrick.

   @param x: test point.

   @param pc: curve coefficients.

   @return Evaluation result
*/
double polyn(const double *x, const double *pc){
  
  double result;
  
  result = pc[0]*x[0]*x[0] + pc[1]*x[1]*x[1] + pc[2]*x[2]*x[2] + 
    pc[3]*x[0]*x[1] + pc[4]*x[0]*x[2] + pc[5]*x[1]*x[2] + pc[6]*x[0] + 
    pc[7]*x[1] + pc[8]*x[2] + pc[9];

  return result;
}

/* NORMAL */
/*****************************************************************************/
/**

   Computes normal vector to curve represented by pc on point x,
   x is not necessarily on the curve.

   @param norm_v: array where normal vector is returned.

   @param x: test point.

   @param pc: curve coefficients.

   @return ret_value not implemented

*/
int normal(double *norm_v, const double *x, const double *pc){ 

  /* Vector normal: gradiente Ec (38) Kirkpatrick */
  norm_v[0] = 2*pc[0]*x[0] + pc[3]*x[1] + pc[4]*x[2] + pc[6];
  norm_v[1] = pc[3]*x[0] + 2*pc[1]*x[1] + pc[5]*x[2] + pc[7];
  norm_v[2] = pc[4]*x[0] + pc[5]*x[1] + 2*pc[2]*x[2] + pc[8];
  /* Normalizacion */
  normalize_vec(norm_v);
  
  return 0;
}

/* INT_CURV */
/*****************************************************************************/
/**

   Find intersection between curve represented by pc and straight line that pass
   through x. The straight line is defined using norm_v, which is assumed to point
   OUTSIDE the curve (from f(x)<0 to f(x)>0).

   @param xint: array where intersection is to be stored.

   @param x: reference point.

   @param norm_v: normal vector to curve represented by pc.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return ret_value not implemented.
*/
int int_curv(double *xint, const double *x,
	     const double *norm_v, const double *pc, const double cell_size){

  int l_flag = 0;
  int u_flag = 0;
  int count = 1;
  
  const int max_iter = 20000;
  
  double step = 0.05*cell_size;
  double F = 0;

  /* Valor inicial xint = x */    
  copy_vec(xint, x);
  
  F = polyn(xint, pc);

  /* Determinacion de interseccion */
  do{

    if (F > 0){
      u_flag = 1;
      
      if (l_flag){step = step*0.5;}
      
      adds_vec_sca(xint, norm_v, -step);
    }
    else{
      
      l_flag = 1;
      
      if (u_flag){step = step*0.5;}
      
      adds_vec_sca(xint, norm_v, step);
    }

    F = polyn(xint, pc);
    
    count++;
    
  } while ((count <= max_iter) && (fabs(F) > 1e-11));

  return 0;
}

/* CALC_XINT */
/*****************************************************************************/
/**

   Finds point over curve whose normal pass through x and returns it.

   @param xint: array where intersection is to be stored.

   @param x: reference point.
   
   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return ret_value not implemented.
*/
void calc_xint(double *xint,
	       const double *x, const double *pc, const double cell_size){

  int count = 1;

  const int max_iter = 100;

  double xint_old[3];
  double norm_v[3];
	
  normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);
    
  while ((count < max_iter) && 
	 (max_abs_d(xint, xint_old) > 1e-6)){

    copy_vec(xint_old, xint);
    normal(norm_v, xint, pc);
    int_curv(xint, x, norm_v, pc, cell_size);
    count = count + 1;

  }

  return;
}

/* CALC_DIST */
/*****************************************************************************/
/**

   Returns distance between xint and x. Where the normal to the curve on point xint
   pass through x.

   @param x: reference point.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return Distance between x and xint.

*/
double calc_dist(const double *x, const double *pc, const double cell_size){

  int count = 1;

  const int max_iter = 100;

  double xint_old[3];
  double xint[3];
  double norm_v[3];
	
  normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);
    
  while ((count < max_iter) && 
	 (max_abs_d(xint, xint_old) > 1e-6)){

    copy_vec(xint_old, xint);
    normal(norm_v, xint, pc);
    int_curv(xint, x, norm_v, pc, cell_size);
    count = count + 1;

  }

  return vec_dist(x, xint);
}

/* CALC_DIST_FAR */
/*****************************************************************************/
/**

   Returns distance between xint and x. Where the normal to the curve on point xint
   pass through x. Used on wall distance determination.

   @param x: reference point.

   @param pc: curve coefficients.

   @param cell_size: reference cell size.

   @return distance between x and xint.

*/
double calc_dist_far(const double *x, const double *pc, const double cell_size){

  int count = 1;

  const int max_iter = 100;

  double est_dist;

  const double approx_factor = 0.5;

  double xint_old[3];
  double xint[3];
  double norm_v[3];
  double x_start[3] = {0};
	
  normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);
    
  while ((count < max_iter) && 
	 (max_abs_d(xint, xint_old) > powf(10, -6))){

    copy_vec(xint_old, xint);
    normal(norm_v, xint, pc);
    est_dist = vec_dist(x, xint);
    if(polyn(x, pc) > 0){
      x_start[0] = -approx_factor * norm_v[0] * est_dist + x[0];
      x_start[1] = -approx_factor * norm_v[1] * est_dist + x[1];
      x_start[2] = -approx_factor * norm_v[2] * est_dist + x[2];
    }
    else{
      x_start[0] = approx_factor * norm_v[0] * est_dist + x[0];
      x_start[1] = approx_factor * norm_v[1] * est_dist + x[1];
      x_start[2] = approx_factor * norm_v[2] * est_dist + x[2];
    }
    int_curv(xint, x_start, norm_v, pc, cell_size);
    count = count + 1;
  }

  return vec_dist(x, xint);
}

/* CALC_DIST_NORM */
/*****************************************************************************/
/**

   Returns distance between xint and x, and normal vector at point xint. Where the normal to the curve on point xint pass through x.

   @param x: reference point.

   @param pc: curve coefficients.

   @param d: value where distance is to be stored.

   @param norm_v: array where normal vector is to be stored.

   @param cell_size: reference cell size.

   @return ret_value not implemented.

*/
void calc_dist_norm(const double *x, const double *pc, double *d, double *norm_v, const double cell_size){
  
  int count = 1;
  
  const int max_iter = 100;
  
  double xint_old[3];
  double xint[3];
  
  normal(norm_v, x, pc);
  int_curv(xint, x, norm_v, pc, cell_size);
  copy_vec(xint_old, x);

  while ((count < max_iter) && (max_abs_d(xint, xint_old) > powf(10, -8))){
    copy_vec(xint_old, xint);
    normal(norm_v, xint, pc);
    int_curv(xint, x, norm_v, pc, cell_size);
    count = count + 1;
  }

  *d = vec_dist(x, xint);

  return;
}

/* MAX_ABS_D */
/*****************************************************************************/
/**

   Returns max absolute difference between components of vectos x1 and x2.
   max_d = max(abs(x1[i] - x2[i]))

   @param x1: first vector.

   @param x2: second vector.

   @return max_d
*/
double max_abs_d(const double *x1, const double *x2){

  int i;
  double max_d = 0;

  for (i=0; i<3; i++){
    if (fabs(x1[i] - x2[i]) > max_d){
      max_d = fabs(x1[i] - x2[i]);
    }
  }

  return max_d;
}

/* VEC_DIST */
/*****************************************************************************/
/**

   Returns magnitude of difference between vectors x1 and x2.

   @param x1: first vector.
   
   @param x2: second vector.

   @return sqrt(sum((x1[i] - x2[i])**2))
*/
double vec_dist(const double *x1, const double *x2){
  
  double result;
  
  result = sqrt(powf(x1[0] - x2[0], 2) + powf(x1[1] - x2[1], 2) + powf(x1[2] - x2[2], 2));
  return result;
}

/* SUBS_SCALAR */
/*****************************************************************************/
/**
   Returns vector x after substraction of scalar value to every component.

   @param x: original vector.

   @param scalar: scalar to be substracted.

   @return ret_value not implemented.
*/
void subs_scalar(double *x, const double scalar){
  
  int i;
  
  for (i = 0; i < 3; i++){
    x[i] = x[i] - scalar;
  }
  return;
}

/* ADDS_VEC_SCA */ 
/*****************************************************************************/
/**
   
   Returns vector x1 after addition of x2 * scalar.
   
   @param x1: starting vector.
   
   @param x2: vector used in addition.

   @param scalar: value used in addition.

   @return ret_value not implemented.
*/
void adds_vec_sca(double *x1, const double *x2, const double scalar){

  int i;
  
  for (i = 0; i < 3; i++){
    x1[i] = x1[i] + x2[i] * scalar;
  }
  return;
}

/* COPY_VEC */
/*****************************************************************************/
/**

   Copy the value of vector x2 on vector x1.

   @param x1: first vector.

   @param x2: second vector.

   @return ret_value not implemented.
*/
void copy_vec(double *x1, const double *x2){

  int i;
  
  for (i = 0; i < 3; i++){
    x1[i] = x2[i];
  }
  return;
}

/* AVG_VEC */
/*****************************************************************************/
/**
   Returns on x_result the average of x0 and x1.

   @param x_result: vector where result is to be stored.

   @param x0: first vector.

   @param x1: second vector.
   
   @return ret_value not implemented.
*/
void avg_vec(double *x_result, const double *x0, const double *x1){

  int i;

  for(i=0; i<3; i++){
    x_result[i] = 0.5 * (x0[i] + x1[i]);
  }

  return;
}

/* BISECTION */
/*****************************************************************************/
/**
   
   Locates a root int the interval [x0, x1] for the polynomial defined by pc and
   returns it on xint.

   @param xint: vector where root is stored.

   @param x0: first vector.

   @param x1: second vector.

   @param pc: curve coefficients.

   @return ret_value not implemented.
*/
void bisection(double *xint, const double *x0, const double *x1, const double *pc){
  
  int iter = 0;
  
  const int max_iter = 30;

  double f_aux0, f_aux1, f_test;
  double x_aux0[3];
  double x_aux1[3];
  double x_test[3] = {0};
  
  const double f_tol = powf(10, -11);


  /* Initial check for root */

  f_aux0 = polyn(x0, pc);
  f_aux1 = polyn(x1, pc);
  
  if(fabs(f_aux0) < f_tol){
    copy_vec(xint, x0);
    return;
  }
  else if(fabs(f_aux1) < f_tol){
    copy_vec(xint, x1);
    return;
  }

  avg_vec(x_test, x0, x1);
  f_test = polyn(x_test, pc);
  
  if(fabs(f_test) < f_tol){
    copy_vec(xint, x_test);
    return;
  }

  copy_vec(x_aux0, x0);
  copy_vec(x_aux1, x1);

  /* Determinacion de interseccion */
  
  while (iter <= max_iter){

    if(f_test > 0){
      if(f_aux1 > 0){
	copy_vec(x_aux1, x_test);
	f_aux1 = polyn(x_aux1, pc);
      }
      else{
	copy_vec(x_aux0, x_test);
	f_aux0 = polyn(x_aux0, pc);
      }
    }
    else{
      if(f_aux1 > 0){
	copy_vec(x_aux0, x_test);
	f_aux0 = polyn(x_aux0, pc);
      }
      else{
	copy_vec(x_aux1, x_test);
	f_aux1 = polyn(x_aux1, pc);
      }
    }
    
    avg_vec(x_test, x_aux0, x_aux1);
    f_test = polyn(x_test, pc);
    
    if(fabs(f_test) < f_tol){
      copy_vec(xint, x_test);
      return;
    }
    
    iter += 1;
  }

  copy_vec(xint, x_test);

  return;
}

/* FIND_SOLID_AREA */
/*****************************************************************************/
/**
   Find area of solid and its normal vector located inside a cut velocity cell
   From: An Adaptive Cartesian Grid Method for Unsteady Compressible Flow in Irregular Regions.
  
   @param norm_v: array where normal vector is returned.
  
   @param solid_area: magnitude of solid area.

   @param area_x: difference between areas perpendicular to x axis.

   @param area_y: difference between areas perpendicular to y axis.
  
   @param area_z: difference between areas perpendicular to z axis.

   @param max_area: maximum solid area that defines search interval.
*/

void find_solid_area(double *norm_v, double *solid_area, const double area_x, const double area_y,
		     const double area_z, const double max_area){


  int iter = 0;
  
  const int max_iter = 50;

  double f_aux0, f_aux1, f_test;

  double solid_area_0 = max_area;
  double solid_area_1 = 0.001 * max_area;
  double solid_area_test = 0;
  
  double norm_v_0[3];
  double norm_v_1[3];
  double norm_v_test[3];
  
  const double f_tol = powf(10, -11);

  /* Initial check for root */

  norm_v_0[0] = area_x / solid_area_0;
  norm_v_0[1] = area_y / solid_area_0;
  norm_v_0[2] = area_z / solid_area_0;
  f_aux0 = 1 - vec_mag(norm_v_0);

  norm_v_1[0] = area_x / solid_area_1;
  norm_v_1[1] = area_y / solid_area_1;
  norm_v_1[2] = area_z / solid_area_1;
  f_aux1 = 1 - vec_mag(norm_v_1);

  if(fabs(f_aux0) < f_tol){    
    copy_vec(norm_v, norm_v_0);
    *solid_area = solid_area_0;
    return;
  }
  else if(fabs(f_aux1) < f_tol){
    copy_vec(norm_v, norm_v_1);
    *solid_area = solid_area_1;
    return;
  }

  solid_area_test = 0.5 * (solid_area_0 + solid_area_1);

  norm_v_test[0] = area_x / solid_area_test;
  norm_v_test[1] = area_y / solid_area_test;
  norm_v_test[2] = area_z / solid_area_test;
  f_test = 1 - vec_mag(norm_v_test);

  if(fabs(f_test) < f_tol){
    copy_vec(norm_v, norm_v_test);
    *solid_area = solid_area_test;
    return;
  }

  /* Determinacion de interseccion */
  
  while(iter <= max_iter){

    if(f_test > 0){
      if(f_aux1 > 0){
	solid_area_1 = solid_area_test;
	norm_v_1[0] = area_x / solid_area_1;
	norm_v_1[1] = area_y / solid_area_1;
	norm_v_1[2] = area_z / solid_area_1;
	f_aux1 = 1 - vec_mag(norm_v_1);
      }
      else{
	solid_area_0 = solid_area_test;
	norm_v_0[0] = area_x / solid_area_0;
	norm_v_0[1] = area_y / solid_area_0;
	norm_v_0[2] = area_z / solid_area_0;
	f_aux0 = 1 - vec_mag(norm_v_0);
      }
    }
    else{
      if(f_aux1 > 0){
	solid_area_0 = solid_area_test;
	norm_v_0[0] = area_x / solid_area_0;
	norm_v_0[1] = area_y / solid_area_0;
	norm_v_0[2] = area_z / solid_area_0;
	f_aux0 = 1 - vec_mag(norm_v_0);
      }
      else{
	solid_area_1 = solid_area_test;
	norm_v_1[0] = area_x / solid_area_1;
	norm_v_1[1] = area_y / solid_area_1;
	norm_v_1[2] = area_z / solid_area_1;
	f_aux1 = 1 - vec_mag(norm_v_1);
      }
    }
    
    solid_area_test = 0.5 * (solid_area_0 + solid_area_1);

    norm_v_test[0] = area_x / solid_area_test;
    norm_v_test[1] = area_y / solid_area_test;
    norm_v_test[2] = area_z / solid_area_test;
    f_test = 1 - vec_mag(norm_v_test);
    
    if(fabs(f_test) < f_tol){
      copy_vec(norm_v, norm_v_test);
      *solid_area = solid_area_test;
      return;
    }
    
    iter += 1;
  }

  copy_vec(norm_v, norm_v_test);
  *solid_area = solid_area_test;

  
  return;
}

/* VEC_MAG */
/*****************************************************************************/
/**  
     @param vec: vector whose magnitud is to be calculated.

     @return Magnitude of vector vec.

*/
double vec_mag(const double *vec){
  
  return powf(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2], 0.5);
}

/* NORMALIZE_VEC */
/*****************************************************************************/
/**  
     Returns vector vec after normalization.

     @param vec: vector to be normalized.

     @return ret_value not implemented.

*/
void normalize_vec(double *vec){

  int i;
  
  double mag;

  mag = powf(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2], 0.5);

  for(i=0; i<3; i++){
    vec[i] = vec[i] / mag;
  }
  
  return;
}

/* DIFF_VEC */
/*****************************************************************************/
/**

   Returns on x0 the difference between vectors x1 and x2.

   @param x0: vector where result is to be stored.

   @param x1: first vector.
   
   @param x2: second vector.

   @return ret_value not implemented.
*/
void diff_vec(double *x0, const double *x1, const double *x2){

  int i;
  
  for(i=0; i<3; i++){
    x0[i] = x1[i] - x2[i];
  }
  
  return;
}

/* SCALE_VEC */
/*****************************************************************************/
/**

   Returns vector x after every component is multiplied by scalar value.

   @param x: original vector.

   @param value: value used to scale.

   @return ret_value not implemented.
*/
void scale_vec(double *x, const double value){

  int i;

  for(i=0; i<3; i++){
    x[i] = x[i] * value;
  }
}

#endif
