#include "include/some_defs.h"

#define PENDING_OK 1
#define ACTIVE_OK 0

#define _r(a,b,c,d,e,f) ( ((a) - (b)) / (c) * (d) / ((e) - (f)) )
#define _psi(r,R) __wahyd(r,R) // __upwind(r,R) // __QUICK(r,R) // __wahyd(r,R) // __van_Leer(r,R)

/* COEFFS_PHI_TVD */
/*****************************************************************************/
/**
  
   Sets:

   Structure coeffs_phi: coefficients of phi for internal nodes using 
   van Leer TVD approach for nonuniform grids as described in Hou et
   al. 2011 with some twiches like fall into upwind scheme for complicated
   cell R factor cases.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 26-12-2022
*/
void coeffs_phi_TVD(Data_Mem *data){

  int I, J, index_;
  int i, j, index_u, index_v;

  int current_domain;
  int E_domain, W_domain, N_domain, S_domain;

  int boundary_cell_OK, Dirichlet_cell_OK;
  
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int nx = data->nx;

  const int phi_dF_OK = data->phi_dF_OK;

  const int steady_state_OK = data->steady_state_OK;
  const int interface_molecular_flux_model = data->interface_molecular_flux_model;

  const int *Isoflux_OK = data->Isoflux_OK;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;

  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  
  const int *W_coeff = data->W_coeff;
  const int *E_coeff = data->E_coeff;
  const int *S_coeff = data->S_coeff;
  const int *N_coeff = data->N_coeff;

  const int *dom = data->domain_matrix;
  const int *curve_phase_change_OK = data->curve_phase_change_OK;

  double Da, m;

  double De, Dw, Dn, Ds;
  double Fe, Fw, Fn, Fs;

  double phiP, phiE, phiW, phiN, phiS;
  double phiWW, phiEE, phiSS, phiNN;
  
  double SP = 0, SC = 0, SPC = 0;  
  double aP0 = 0;
  double dF = 0;
  
  /* Distances to nodes */
  double dEE, dWW, dE, dW;
  double dNN, dSS, dN, dS;
  
  /* Source terms */
  double Se, Sw, Sn, Ss;
  
  /* Source terms divided by convective parameter */
  double Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam;
  
  /* Upwind to downwind gradient */
  double rep, rem, rwp, rwm;
  double rnp, rnm, rsp, rsm;
  
  /* Nonuniform grid parameter */
  double Rep, Rem, Rwp, Rwm;
  double Rnp, Rnm, Rsp, Rsm;

  /* Cell sizes */
  double DW, DP, DE, DN, DS;
  double Dx, Dy;
  double de, dw, dn, ds;

  /* Propiedades fisicas */
  const double dt = data->dt[3] / 2; // For 2D case

  double *aP = data->coeffs_phi.aP;
  double *aW = data->coeffs_phi.aW;
  double *aE = data->coeffs_phi.aE;
  double *aS = data->coeffs_phi.aS;
  double *aN = data->coeffs_phi.aN;
  double *b = data->coeffs_phi.b;

  const double *phi = data->phi;
  const double *phi0 = data->phi0;

  const double *phi0_v = data->phi0_v;
  const double *phi_b_v = data->phi_b_v;
  const double *SC_vol_m = data->SC_vol_m;
  const double *cp_m = data->cp_m;
  const double *rho_m = data->rho_m;
  const double *enthalpy_diff = data->enthalpy_diff;
  const double *gL_guess = data->gL_guess;
  const double *gL = data->gL;
  const double *phase_change_vol = data->phase_change_vol;
  const double *vol = data->vol_uncut;
  
  const double *Dphi_x = data->Dphi_x;
  const double *Dphi_y = data->Dphi_y;
  const double *Fphi_x = data->Fphi_x;
  const double *Fphi_y = data->Fphi_y;

  const double *dphidxb = data->dphidxb;
  const double *dphidyb = data->dphidyb;

  const double *cdE = data->cdE;
  const double *cdW = data->cdW;
  const double *cdN = data->cdN;
  const double *cdS = data->cdS;

  const double *Dx_ = data->Delta_x;
  const double *Dy_ = data->Delta_y;

  const double *de_ = data->Delta_xe;
  const double *dw_ = data->Delta_xw;
  const double *dn_ = data->Delta_yn;
  const double *ds_ = data->Delta_ys;
  
  set_D_phi(data);
  set_F_phi(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_u, index_v,				\
	  current_domain,						\
	  E_domain, W_domain, N_domain, S_domain,			\
	  boundary_cell_OK, Dirichlet_cell_OK,				\
	  De, Dw, Dn, Ds, Fe, Fw, Fn, Fs,				\
	  aP0, Da, m, SC, SP, SPC, dF,					\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
	  dEE, dWW, dE, dW, dNN, dSS, dN, dS,				\
	  rep, rem, rwp, rwm, rnp, rnm, rsp, rsm,			\
  	  Rep, Rem, Rwp, Rwm, Rnp, Rnm, Rsp, Rsm,			\
	  DP, DE, DW, DN, DS,						\
	  Dx, Dy,							\
	  de, dw, dn, ds,						\
	  Se, Sw, Sn, Ss,						\
	  Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam,		\
	  phiP, phiE, phiW, phiN, phiS, phiWW, phiEE, phiSS, phiNN)	\
  shared(dom, aE, aW, aS, aN, aP, b,					\
	 rho_m, cp_m, vol, phi, phi0, phi0_v, phi_b_v,			\
	 Isoflux_OK,							\
	 P_solve_phase_OK,						\
	 E_solve_phase_OK, W_solve_phase_OK,				\
	 N_solve_phase_OK, S_solve_phase_OK,				\
	 E_is_fluid_OK, W_is_fluid_OK,					\
	 N_is_fluid_OK, S_is_fluid_OK,					\
	 E_coeff, W_coeff, N_coeff, S_coeff,				\
	 dphidxb, dphidyb,						\
	 curve_phase_change_OK,						\
	 SC_vol_m, enthalpy_diff, gL, gL_guess, phase_change_vol,	\
	 Dphi_x, Dphi_y, Fphi_x, Fphi_y,				\
	 cdE, cdW, cdN, cdS, Dx_, Dy_,					\
	 de_, dw_, dn_, ds_)
  
  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      i = I-1;
      j = J-1;

      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;

      SC = 0;
      SP = 0;
      SPC = 0;

      b[index_] = 0;

      if(steady_state_OK){
	aP0 = 0;
      }
      else{
	aP0 = rho_m[index_] * cp_m[index_] * vol[index_] / dt;
      }
      
      /* Distances */
      Dx = Dx_[index_];
      Dy = Dy_[index_];

      de = de_[index_];
      dw = dw_[index_];
      dn = dn_[index_];
      ds = ds_[index_];
	
      dE = cdE[index_];
      dW = cdW[index_];
      dN = cdN[index_];
      dS = cdS[index_];

      dEE = dE + cdE[index_ + 1];
      dWW = dW + cdW[index_ - 1];
      dNN = dN + cdN[index_ + Nx];
      dSS = dS + cdS[index_ - Nx];

      /* Phi */
      phiP = phi[index_];
      phiE = phi[index_ + 1];
      phiW = phi[index_ - 1];
      phiN = phi[index_ + Nx];
      phiS = phi[index_ - Nx];

      /* Diffusive */
      De = Dphi_x[index_u+1]  * E_coeff[index_];
      Dw = Dphi_x[index_u]    * W_coeff[index_];
      Dn = Dphi_y[index_v+Nx] * N_coeff[index_];
      Ds = Dphi_y[index_v]    * S_coeff[index_];

      /* Corrections to parameters 
	 in account for Dirichlet or Isoflux effects */
      if(!E_solve_phase_OK[index_]){
	  
	Da = dw + dE;
	  
	m = Dx / Da;
	  
	De = De * m;
	Dw = Dw * m;
      }
      if(!W_solve_phase_OK[index_]){
	  
	Da = dW + de;
	  
	m = Dx / Da;
	  
	De = De * m;
	Dw = Dw * m;
      }
	
      if(!N_solve_phase_OK[index_]){
	  
	Da = ds + dN;
	  
	m = Dy / Da;
	  
	Dn = Dn * m;
	Ds = Ds * m;
      }
	
      if(!S_solve_phase_OK[index_]){
	  
	Da = dS + dn;
	  
	m = Dy / Da;
	  
	Dn = Dn * m;
	Ds = Ds * m;
      }
	
      /* Convective */
      Fe = Fphi_x[index_u+1]  * E_coeff[index_];
      Fw = Fphi_x[index_u]    * W_coeff[index_];
      Fn = Fphi_y[index_v+Nx] * N_coeff[index_];
      Fs = Fphi_y[index_v]    * S_coeff[index_];
	
      Fe_positive_OK = (Fe > 0);
      Fw_positive_OK = (Fw > 0);
      Fn_positive_OK = (Fn > 0);
      Fs_positive_OK = (Fs > 0);

      if(!P_solve_phase_OK[index_]){
	/* We are "not solving" this phase */
	aP[index_] = 1;
	aE[index_] = 0;
	aW[index_] = 0;
	aN[index_] = 0;
	aS[index_] = 0;
	b[index_] = phi0_v[ dom[index_] ];
      }
      else{
	/* This phase must be addressed */
	if(dom[index_] != 0){
	  /* The solid (no convection) case */
	  aE[index_] = De * E_solve_phase_OK[index_] * E_coeff[index_];
	  aW[index_] = Dw * W_solve_phase_OK[index_] * W_coeff[index_];
	  aN[index_] = Dn * N_solve_phase_OK[index_] * N_coeff[index_];
	  aS[index_] = Ds * S_solve_phase_OK[index_] * S_coeff[index_];
	    
	  if((curve_phase_change_OK[dom[index_]-1]) && !steady_state_OK){
	    /* EC 16 Voller  */
	    SPC = enthalpy_diff[index_] * 
	      (gL[index_] - gL_guess[index_]) *
	      phase_change_vol[index_] / dt;
	  }
	}
	else{
	  /* The fluid (convection) case */

	  /* See if cell is at inner boundary */
	  boundary_cell_OK = 
	    !E_is_fluid_OK[index_] || !W_is_fluid_OK[index_] ||
	    !N_is_fluid_OK[index_] || !S_is_fluid_OK[index_];

	  if(boundary_cell_OK){
	    /* Value of phi at boundaries if index_ is a FLUID (inner) boundary node */
	    /* The value at boundary should replace phiEWNS 
	       since we already count with the correct distances in cdEWNS 
	       no ghost or mirror is needed (adjust for small distances cdEWNS for mirror node) 
	       for eps_xy_f < 0.5 it will fall back to upwind case */
	    
	    /* See if cell is of the Dirichlet type */
	    Dirichlet_cell_OK = 
	      (!E_solve_phase_OK[index_] && !Isoflux_OK[ dom[index_+1] - 1 ]) ||
	      (!W_solve_phase_OK[index_] && !Isoflux_OK[ dom[index_-1] - 1 ]) ||
	      (!N_solve_phase_OK[index_] && !Isoflux_OK[ dom[index_+Nx] - 1 ]) ||
	      (!S_solve_phase_OK[index_] && !Isoflux_OK[ dom[index_-Nx] - 1 ]);

	    if(Dirichlet_cell_OK){
	      /* For Dirichlet type we use the phib value directly */
	      if(!E_solve_phase_OK[index_]){
		phiE = phi_b_v[ dom[index_+1] - 1 ];
	      }
	      
	      if(!W_solve_phase_OK[index_]){
		phiW = phi_b_v[ dom[index_-1] - 1 ];
	      }

	      if(!N_solve_phase_OK[index_]){
		phiN = phi_b_v[ dom[index_+Nx] - 1 ];
	      }

	      if(!S_solve_phase_OK[index_]){
		phiS = phi_b_v[ dom[index_-Nx] - 1 ];
	      }
	      
	      
	    } // end if(Dirichlet_cell_OK){
	    else{
	      /* For Isoflux and Conjugate we use interpolation/extrapolation to the boundary */

	      current_domain = dom[index_];
	      
	      E_domain = dom[index_+1];
	      W_domain = dom[index_-1];
	      N_domain = dom[index_+Nx];
	      S_domain = dom[index_-Nx];
	      
	      if(current_domain != E_domain){
		phiE = phiP + dE * dphidxb[index_];
	      }
	      
	      if(current_domain != W_domain){
		phiW = phiP - dW * dphidxb[index_];
	      }
	      
	      if(current_domain != N_domain){
		phiN = phiP + dN * dphidyb[index_];
	      }
	      
	      if(current_domain != S_domain){
		phiS = phiP - dS * dphidyb[index_];
	      }
	      
	    } // end else if(Dirichlet_cell_OK){
	    
	  } // end if(boundary_cell_OK){
	  
	  /* Extrapolation for presence of boundaries */
	  if(I == 1){
	    phiWW = 2 * phiW - phiP;
	    dWW = 2*dW;
	  }
	  else{
	    phiWW = phi[index_-2];
	  }
	  
	  if(I == Nx-2){
	    phiEE = 2 * phiE - phiP;
	    dEE = 2*dE;
	  }
	  else{
	    phiEE = phi[index_+2];
	  }

	  if(J == 1){
	    phiSS = 2 * phiS - phiP;
	    dSS = 2*dS;
	  }
	  else{
	    phiSS = phi[index_-2*Nx];
	  }

	  if(J == Ny-2){
	    phiNN = 2 * phiN - phiP;
	    dNN = 2*dN;
	  }
	  else{
	    phiNN = phi[index_+2*Nx];
	  }
	
	  aE[index_] = (De + MAX(-Fe, 0)) * E_solve_phase_OK[index_] * E_coeff[index_];
	  aW[index_] = (Dw + MAX(+Fw, 0)) * W_solve_phase_OK[index_] * W_coeff[index_];
	  aN[index_] = (Dn + MAX(-Fn, 0)) * N_solve_phase_OK[index_] * N_coeff[index_];
	  aS[index_] = (Ds + MAX(+Fs, 0)) * S_solve_phase_OK[index_] * S_coeff[index_];

	  // EW
	  DP = Dx;
	  DE = 2*(dE - DP/2);
	  DW = 2*(dW - DP/2);
	
	  // e
	  rep = _r(phiP, phiW,      dW, dE, phiE, phiP);
	  rem = _r(phiE, phiEE, dEE-dE, dE, phiP, phiE);

	  Rep = (DP + DE) / DP;
	  Rem = (DE + DP) / DE;
	
	  Seap = _psi(rep,Rep) / Rep;
	
	  Seam = _psi(rem,Rem) / Rem;
	  
	  Se = Fe * ((1 - Fe_positive_OK) * Seam - Fe_positive_OK * Seap) * (phiE - phiP);

	  // Back to upwind scheme if no convective face at e
	  Se = (DE <= 0) ? 0 : Se;
	  Se = Se * E_solve_phase_OK[index_] * E_coeff[index_];

	  // w
	  rwp = _r(phiW, phiWW, dWW-dW, dW, phiP, phiW);
	  rwm = _r(phiP,  phiE,     dE, dW, phiW, phiP);
	
	  Rwp = (DW + DP) / DW;
	  Rwm = (DP + DW) / DP;
	
	  Swap = _psi(rwp,Rwp) / Rwp;
	
	  Swam = _psi(rwm,Rwm) / Rwm;
	  
	  Sw = Fw * (Fw_positive_OK * Swap - (1 - Fw_positive_OK) * Swam) * (phiP - phiW);

	  Sw = (DW <= 0) ? 0 : Sw;
	  Sw = Sw * W_solve_phase_OK[index_] * W_coeff[index_];

	  // NS
	  DP = Dy;
	  DN = 2*(dN - DP/2);
	  DS = 2*(dS - DP/2);
	
	  // n
	  rnp = _r(phiP, phiS,      dS, dN, phiN, phiP);
	  rnm = _r(phiN, phiNN, dNN-dN, dN, phiP, phiN);

	  Rnp = (DP + DN) / DP;
	  Rnm = (DN + DP) / DN;
	
	  Snap = _psi(rnp,Rnp) / Rnp;
	
	  Snam = _psi(rnm,Rnm) / Rnm;
	  
	  Sn = Fn * ((1 - Fn_positive_OK) * Snam - Fn_positive_OK * Snap) * (phiN - phiP);

	  Sn = (DN <= 0) ? 0 : Sn;
	  Sn = Sn * N_solve_phase_OK[index_] * N_coeff[index_];

	  // s
	  rsp = _r(phiS, phiSS, dSS-dS, dS, phiP, phiS);
	  rsm = _r(phiP,  phiN,     dN, dS, phiS, phiP);
	
	  Rsp = (DS + DP) / DS;
	  Rsm = (DP + DS) / DP;
	
	  Ssap = _psi(rsp,Rsp) / Rsp;
	
	  Ssam = _psi(rsm,Rsm) / Rsm;
	  
	  Ss = Fs * (Fs_positive_OK * Ssap - (1 - Fs_positive_OK) * Ssam) * (phiP - phiS);

	  Ss = (DS <= 0) ? 0 : Ss;
	  Ss = Ss * S_solve_phase_OK[index_] * S_coeff[index_];

	  /* Putting TVD contributions on SPC parameter for the FLUID case */
	  /* This same parameter is used for storing gL information on the SOLID case */
	  SPC = Se + Sw + Sn + Ss;
	} // end else if(dom[index_] != 0){ (FLUID case)

	/* Mainly for testing the two forms of the phi equation */
	dF = Fe - Fw + Fn - Fs;
	dF = phi_dF_OK ? dF : 0;
	
	aP[index_] =
	  aE[index_] + aW[index_] + aN[index_] + aS[index_] +
	  aP0 - SP * vol[index_] + dF;
	
	/* With SC_m computes arrays of SC due to 
	   internal boundaries regardless of the case 
	   (Dirichlet, Isoflux, Conjugate) */
	b[index_] =
	  SC_vol_m[index_] + SC * vol[index_] + SPC + aP0 * phi0[index_];

      } /* end else if(!P_solve_phase_OK[index_]) */
	
    } // for(I=1; I<Nx-1; I++){
  } // for(J=1; J<Ny-1; J++){
  
    /* Conjugate heat transfer semi-implicit scheme */
  if(interface_molecular_flux_model == 6){
    compute_near_boundary_molecular_flux_experimental_SI(data);
  }
  
  /* Relax coefficients */
  relax_coeffs(data->phi, P_solve_phase_OK, 1, data->coeffs_phi, Nx, Ny, data->alpha_phi);
  
  return;
}

#if PENDING_OK

void coeffs_phi_TVD_3D(Data_Mem *data){
  return;
}

#endif

#if ACTIVE_OK
  /* COEFFS_PHI_TVD_3D */
  /*****************************************************************************/
  /**
  
     Sets:

     Structure coeffs_phi: coefficients of phi for internal nodes using 
     van Leer TVD approach for nonuniform grids as described in Hou et
     al. 2011 with some twiches like fall into upwind scheme for complicated
     cell R factor cases.

     @param data
     @return ret_value not implemented.
  
     MODIFIED: 26-12-2022
  */
  void coeffs_phi_TVD_3D(Data_Mem *data){

    int I, J, K, i, j, k, z;
    int index_, index_u;
    int index_face_y, index_face_z;
    int Fw_positive_OK, Fe_positive_OK;
    int Fs_positive_OK, Fn_positive_OK;
    int Fb_positive_OK, Ft_positive_OK;

    const int Nx = data->Nx;
    const int Ny = data->Ny;
    const int Nz = data->Nz;
    const int nx = data->nx;
    const int ny = data->ny;

    const int p_source_orientation = data->p_source_orientation;
    const int u_slave_count = data->u_slave_count;
    const int consider_wall_shear_OK = data->consider_wall_shear_OK;
    const int u_sol_factor_count = data->u_sol_factor_count;
    const int curves = data->curves;
    const int pv_coupling_algorithm = data->pv_coupling_algorithm;
    const int flow_steady_state_OK = data->flow_steady_state_OK;
    const int diff_u_face_x_count = data->diff_u_face_x_count;
    const int add_momentum_source_OK = data->add_momentum_source_OK;

    const int *E_is_fluid_OK = data->E_is_fluid_OK;
    const int *W_is_fluid_OK = data->W_is_fluid_OK;
    const int *N_is_fluid_OK = data->N_is_fluid_OK;
    const int *S_is_fluid_OK = data->S_is_fluid_OK;
    const int *T_is_fluid_OK = data->T_is_fluid_OK;
    const int *B_is_fluid_OK = data->B_is_fluid_OK;

    const int *u_dom_matrix = data->u_dom_matrix;
    const int *u_slave_nodes = data->u_slave_nodes;
    const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
    const int *u_sol_factor_index = data->u_sol_factor_index;
    const int *diff_u_face_x_index = data->diff_u_face_x_index;
    const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
    const int *diff_u_face_x_I = data->diff_u_face_x_I;

    double dpA, mu_eff;
    double De, Dw, Dn, Ds, Dt, Db;
    double Fe, Fw, Fn, Fs, Ft, Fb;
    double phiP, phiE, phiW, phiN, phiS, phiT, phiB;
    double phiWW, phiEE, phiSS, phiNN, phiBB, phiTT;
    double aP0, S_diff;
    /* Distances to nodes */
    double dEE, dWW, dE, dW;
    double dNN, dSS, dN, dS;
    double dTT, dBB, dT, dB;
    /* Source terms */
    double Se, Sw, Sn, Ss, St, Sb;
    /* Source terms divided by convective parameter */
    double Seap, Seam, Swap, Swam;
    double Snap, Snam, Ssap, Ssam;
    double Stap, Stam, Sbap, Sbam;
    /* Upwind to downwind gradient */
    double rep, rem, rwp, rwm;
    double rnp, rnm, rsp, rsm;
    double rtp, rtm, rbp, rbm;
    /* Nonuniform grid parameter */
    double Rep, Rem, Rwp, Rwm;
    double Rnp, Rnm, Rsp, Rsm;
    double Rtp, Rtm, Rbp, Rbm;

    /* Cell sizes */
    double DW, DP, DE, DN, DS, DT, DB;

    /* Propiedades fisicas */
    const double mu = data->mu;
    const double rho = data->rho_v[0];
    const double dt = data->dt[3] / 3; // For 3D case
    const double periodic_source = data->periodic_source;
 
    double *aP_u = data->coeffs_u.aP;
    double *aW_u = data->coeffs_u.aW;
    double *aE_u = data->coeffs_u.aE;
    double *aS_u = data->coeffs_u.aS;
    double *aN_u = data->coeffs_u.aN;
    double *aB_u = data->coeffs_u.aB;
    double *aT_u = data->coeffs_u.aT;
    double *b_u = data->coeffs_u.b;

    const double *u = data->u;
    const double *p = data->p;

    const double *u_faces_x = data->u_faces_x;
    const double *diff_factor_u_face_x = data->diff_factor_u_face_x;

    const double *Du_x = data->Du_x;
    const double *Du_y = data->Du_y;
    const double *Du_z = data->Du_z;
    const double *Fu_x = data->Fu_x;
    const double *Fu_y = data->Fu_y;
    const double *Fu_z = data->Fu_z;
  
    const double *u_sol_factor = data->u_sol_factor;
    const double *u_p_factor = data->u_p_factor;
    const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  
    const double *u_cut_fe = data->u_cut_fe;
    const double *u_cut_fw = data->u_cut_fw;
    const double *vol_x = data->vol_x;
    const double *alpha_u_x = data->alpha_u_x;
    const double *alpha_u_y = data->alpha_u_y;
    const double *alpha_u_z = data->alpha_u_z;
    const double *u_p_factor_W = data->u_p_factor_W;
    const double *u0 = data->u0;

#ifdef OLDINT_OK
    const double *cudE = data->Delta_xE_u; /**< Distance from node to East neighbour, u nodes. */
    const double *cudW = data->Delta_xW_u; /**< Distance from node to West neighbour, u nodes. */
    const double *cudN = data->Delta_yN_u; /**< Distance from node to East neighbour, u nodes. */
    const double *cudS = data->Delta_yS_u; /**< Distance from node to West neighbour, u nodes. */
    const double *cudT = data->Delta_zT_u; /**< Distance from node to East neighbour, u nodes. */
    const double *cudB = data->Delta_zB_u; /**< Distance from node to West neighbour, u nodes. */
#else
    const double *cudE = data->cudE;
    const double *cudW = data->cudW;
    const double *cudN = data->cudN;
    const double *cudS = data->cudS;
    const double *cudT = data->cudT;
    const double *cudB = data->cudB;
#endif

    const double *Dxu = data->Delta_x_u;
    const double *Dyu = data->Delta_y_u;
    const double *Dzu = data->Delta_z_u;
  
    set_D_u_3D(data);
    set_F_u_3D(data);

#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_u,				\
	  index_face_y, index_face_z,					\
	  De, Dw, Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb,		\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
  	  Fb_positive_OK, Ft_positive_OK,				\
	  dEE, dWW, dE, dW,						\
	  dNN, dSS, dN, dS,						\
	  dTT, dBB, dT, dB,						\
  	  rep, rem, rwp, rwm,						\
	  rnp, rnm, rsp, rsm,						\
	  rtp, rtm, rbp, rbm,						\
  	  Rep, Rem, Rwp, Rwm,						\
	  Rnp, Rnm, Rsp, Rsm,						\
	  Rtp, Rtm, Rbp, Rbm,						\
	  DP, DE, DW, DN, DS, DT, DB,					\
	  Se, Sw, Sn, Ss, St, Sb,					\
	  Seap, Seam, Swap, Swam,					\
	  Snap, Snam, Ssap, Ssam,					\
	  Stap, Stam, Sbap, Sbam,					\
	  phiP, phiE, phiW, phiN, phiS, phiT, phiB,			\
	  phiWW, phiEE, phiSS, phiNN, phiBB, phiTT)			\
  shared(u_dom_matrix,							\
	 aE_u, aW_u, aS_u, aN_u, aT_u, aB_u, aP_u, b_u,			\
	 u, p, u_faces_x,						\
	 Du_x, Du_y, Du_z, Fu_x, Fu_y, Fu_z, mu_turb_at_u_nodes,	\
	 Dxu, Dyu, Dzu, cudE, cudW, cudN, cudS, cudT, cudB, 		\
	 alpha_u_x, alpha_u_y, alpha_u_z)

    for(K=1; K<Nz-1; K++){  
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	
	  index_u = K*nx*Ny + J*nx + i;
	
	  if(u_dom_matrix[index_u] == 0){

	    I = i+1;      
	    j = J-1;
	    k = K-1;
	
	    index_ = K*Nx*Ny + J*Nx + I;
	    index_face_y = K*nx*ny + j*nx + i; // Du,Fu_y
	    index_face_z = k*nx*Ny + J*nx + i; // Du,Fu_z

	    /* Distances */
	    dE = cudE[index_u];
	    dW = cudW[index_u];
	    dN = cudN[index_u];
	    dS = cudS[index_u];
	    dT = cudT[index_u];
	    dB = cudB[index_u];

	    dEE = dE + cudE[index_u + 1];
	    dWW = dW + cudW[index_u - 1];
	    dNN = dN + cudN[index_u + nx];
	    dSS = dS + cudS[index_u - nx];
	    dTT = dT + cudT[index_u + nx*Ny];
	    dBB = dB + cudB[index_u - nx*Ny];

	    /* Velocities */
	    phiP = u[index_u];
	    phiE = u[index_u + 1];
	    phiW = u[index_u - 1];
	    phiN = u[index_u + nx];
	    phiS = u[index_u - nx];
	    phiT = u[index_u + nx*Ny];
	    phiB = u[index_u - nx*Ny];

	    /* Diffusive */
	    De = Du_x[index_];
	    Dw = Du_x[index_ - 1];
	    Dn = Du_y[index_face_y + nx];
	    Ds = Du_y[index_face_y];
	    Dt = Du_z[index_face_z + nx*Ny];
	    Db = Du_z[index_face_z];

	    /* Convective */
	    Fe = Fu_x[index_] * alpha_u_x[index_] * alpha_u_x[index_];
	    Fw = Fu_x[index_ - 1] * alpha_u_x[index_ - 1] * alpha_u_x[index_ - 1];
	    Fn = Fu_y[index_face_y + nx] * alpha_u_y[index_face_y + nx];
	    Fs = Fu_y[index_face_y] * alpha_u_y[index_face_y];
	    Ft = Fu_z[index_face_z + nx*Ny] * alpha_u_z[index_face_z + nx*Ny];
	    Fb = Fu_z[index_face_z] * alpha_u_z[index_face_z];
	
	    Fe_positive_OK = (Fe > 0);
	    Fw_positive_OK = (Fw > 0);
	    Fn_positive_OK = (Fn > 0);
	    Fs_positive_OK = (Fs > 0);
	    Ft_positive_OK = (Ft > 0);
	    Fb_positive_OK = (Fb > 0);

	    /* Extrapolation for presence of boundaries */
	    /* Should be done for internal boundaries as well */
	
	    if(i == 1){
	      phiWW = 2 * phiW - phiP;
	      dWW = 2*dW;
	    }
	    else if (u_dom_matrix[index_u - 2] != 0){
	      /* WW within solid */
	      phiWW = 2 * phiW - phiP;
	      dWW = 2*dW;
	    }
	    else{
	      phiWW = u[index_u-2];
	    }

	    if(i == nx-2){
	      phiEE = 2 * phiE - phiP;
	      dEE = 2*dE;
	    }
	    else if(u_dom_matrix[index_u + 2] != 0){
	      /* EE within solid */
	      phiEE = 2 * phiE - phiP;
	      dEE = 2*dE;
	    }
	    else{
	      phiEE = u[index_u+2];
	    }

	    if(J == 1){
	      phiSS = 2 * phiS - phiP;
	      dSS = 2*dS;
	    }
	    else if(u_dom_matrix[index_u - 2*nx] != 0){
	      phiSS = 2 * phiS - phiP;
	      dSS = 2*dS;
	    }
	    else{
	      phiSS = u[index_u-2*nx];
	    }

	    if(J == Ny-2){
	      phiNN = 2 * phiN - phiP;
	      dNN = 2*dN;
	    }
	    else if(u_dom_matrix[index_u + 2*nx] != 0){
	      phiNN = 2 * phiN - phiP;
	      dNN = 2*dN;
	    }
	    else{
	      phiNN = u[index_u+2*nx];
	    }
	
	    if(K == 1){
	      phiBB = 2 * phiB - phiP;
	      dBB = 2*dB;
	    }
	    else if(u_dom_matrix[index_u - 2*nx*Ny] != 0){
	      phiBB = 2 * phiB - phiP;
	      dBB = 2*dB;
	    }
	    else{
	      phiBB = u[index_u - 2*nx*Ny];
	    }

	    if(K == Nz-2){
	      phiTT = 2 * phiT - phiP;
	      dTT = 2*dT;
	    }
	    else if(u_dom_matrix[index_u + 2*nx*Ny] != 0){
	      phiTT = 2 * phiT - phiP;
	      dTT = 2*dT;
	    }
	    else{
	      phiTT = u[index_u + 2*nx*Ny];
	    }

	    dpA = u_faces_x[index_] * p[index_] - 
	      u_faces_x[index_-1] * p[index_-1];

	    b_u[index_u] = -dpA;
	
	    aE_u[index_u] = De + MAX(-Fe, 0);
	    aW_u[index_u] = Dw + MAX(+Fw, 0);
	    aN_u[index_u] = Dn + MAX(-Fn, 0);
	    aS_u[index_u] = Ds + MAX(+Fs, 0);
	    aT_u[index_u] = Dt + MAX(-Ft, 0);
	    aB_u[index_u] = Db + MAX(+Fb, 0);

	    aP_u[index_u] =
	      aE_u[index_u] + aW_u[index_u] +
	      aN_u[index_u] + aS_u[index_u] +
	      aT_u[index_u] + aB_u[index_u];
	    /*
	      aE_u[index_u] + aW_u[index_u] +
	      aN_u[index_u] + aS_u[index_u] +
	      aT_u[index_u] + aB_u[index_u] +
	      (Fe - Fw) + (Fn - Fs) + (Ft - Fb);
	    */

	    // EW
	    DP = Dxu[index_u];
	    DE = 2*(dE - DP/2);
	    DW = 2*(dW - DP/2);
	
	    // e
	    rep = _r(phiP, phiW,      dW, dE, phiE, phiP);
	    rem = _r(phiE, phiEE, dEE-dE, dE, phiP, phiE);

	    Rep = (DP + DE) / DP;
	    Rem = (DE + DP) / DE;
	
	    Seap = _psi(rep,Rep) / Rep;
	
	    Seam = _psi(rem,Rem) / Rem;
	  
	    Se = Fe * ((1 - Fe_positive_OK) * Seam - Fe_positive_OK * Seap) * (phiE - phiP);

	    // Back to upwind scheme if no convective face at e
	    Se = (DE <= 0) ? 0 : Se;

	    // w
	    rwp = _r(phiW, phiWW, dWW-dW, dW, phiP, phiW);
	    rwm = _r(phiP,  phiE,     dE, dW, phiW, phiP);
	
	    Rwp = (DW + DP) / DW;
	    Rwm = (DP + DW) / DP;
	
	    Swap = _psi(rwp,Rwp) / Rwp;
	
	    Swam = _psi(rwm,Rwm) / Rwm;
	  
	    Sw = Fw * (Fw_positive_OK * Swap - (1 - Fw_positive_OK) * Swam) * (phiP - phiW);

	    Sw = (DW <= 0) ? 0 : Sw;

	    // NS
	    DP = Dyu[index_u];
	    DN = 2*(dN - DP/2);
	    DS = 2*(dS - DP/2);
	
	    // n
	    rnp = _r(phiP, phiS,      dS, dN, phiN, phiP);
	    rnm = _r(phiN, phiNN, dNN-dN, dN, phiP, phiN);

	    Rnp = (DP + DN) / DP;
	    Rnm = (DN + DP) / DN;
	
	    Snap = _psi(rnp,Rnp) / Rnp;
	
	    Snam = _psi(rnm,Rnm) / Rnm;
	  
	    Sn = Fn * ((1 - Fn_positive_OK) * Snam - Fn_positive_OK * Snap) * (phiN - phiP);

	    Sn = (DN <= 0) ? 0 : Sn;

	    // s
	    rsp = _r(phiS, phiSS, dSS-dS, dS, phiP, phiS);
	    rsm = _r(phiP,  phiN,     dN, dS, phiS, phiP);
	
	    Rsp = (DS + DP) / DS;
	    Rsm = (DP + DS) / DP;
	
	    Ssap = _psi(rsp,Rsp) / Rsp;
	
	    Ssam = _psi(rsm,Rsm) / Rsm;
	  
	    Ss = Fs * (Fs_positive_OK * Ssap - (1 - Fs_positive_OK) * Ssam) * (phiP - phiS);

	    Ss = (DS <= 0) ? 0 : Ss;

	    // TB
	    DP = Dzu[index_u];
	    DT = 2*(dT - DP/2);
	    DB = 2*(dB - DP/2);
	
	    // t
	    rtp = _r(phiP, phiB,      dB, dT, phiT, phiP);
	    rtm = _r(phiT, phiTT, dTT-dT, dT, phiP, phiT);

	    Rtp = (DP + DT) / DP;
	    Rtm = (DT + DP) / DT;
	
	    Stap = _psi(rtp,Rtp) / Rtp;
	
	    Stam = _psi(rtm,Rtm) / Rtm;
	  
	    St = Ft * ((1 - Ft_positive_OK) * Stam - Ft_positive_OK * Stap) * (phiT - phiP);

	    // Back to upwind scheme if no convective face at e
	    St = (DT <= 0) ? 0 : St;

	    // b
	    rbp = _r(phiB, phiBB, dBB-dB, dB, phiP, phiB);
	    rbm = _r(phiP,  phiT,     dT, dB, phiB, phiP);
	
	    Rbp = (DB + DP) / DB;
	    Rbm = (DP + DB) / DP;
	
	    Sbap = _psi(rbp,Rbp) / Rbp;
	
	    Sbam = _psi(rbm,Rbm) / Rbm;
	  
	    Sb = Fb * (Fb_positive_OK * Sbap - (1 - Fb_positive_OK) * Sbam) * (phiP - phiB);

	    Sb = (DB <= 0) ? 0 : Sb;

	    b_u[index_u] += Se + Sw + Sn + Ss + St + Sb; // Sw - Se + Ss - Sn + Sb - St;
	
	  }
	  else{
	    aP_u[index_u] = 1;
	    aW_u[index_u] = 0;
	    aE_u[index_u] = 0;
	    aS_u[index_u] = 0;
	    aN_u[index_u] = 0;
	    aB_u[index_u] = 0;
	    aT_u[index_u] = 0;
	    b_u[index_u] = 0;
	  }
	}
      }
    }
  
    /* Diffusive factor */
    for(z=0; z<diff_u_face_x_count; z++){
    
      index_ = diff_u_face_x_index[z];
      index_u = diff_u_face_x_index_u[z];
    
      I = diff_u_face_x_I[z];
    
      mu_eff = mu + 0.5 * (mu_turb_at_u_nodes[index_u] + mu_turb_at_u_nodes[index_u+1]);
      S_diff = mu_eff * diff_factor_u_face_x[z] *
	(u[index_u] * u_cut_fw[index_] + u[index_u+1] * u_cut_fe[index_]);

      /* Boundary cells coefficients are set at set_bc_flow. 
	 We do not want to modify its values here. */    
      if((u_dom_matrix[index_u] == 0) && (I > 1)){
	b_u[index_u] -= S_diff;
      }
      if((u_dom_matrix[index_u+1] == 0) && (I < Nx-2)){
	b_u[index_u+1] += S_diff;
      }
    }

    /* Solid factor */
    if((curves > 0) && (consider_wall_shear_OK)){
      for(z=0; z<u_sol_factor_count; z++){
      
	index_u = u_sol_factor_index_u[z];
      
	if(u_dom_matrix[index_u] == 0){

	  index_ = u_sol_factor_index[z];
	
	  mu_eff = mu + mu_turb_at_u_nodes[index_u];
	
	  aP_u[index_u] += mu_eff * u_sol_factor[z];
	  b_u[index_u] += u_p_factor[z] *
	    (u_p_factor_W[z] * p[index_-1] + (1 - u_p_factor_W[z]) * p[index_]);
	}
      }
    }

    /* Adding source corresponding to periodic flow if relevant */

    if(p_source_orientation == 1){
    
#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(u_dom_matrix, b_u, vol_x)
      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	  
	    index_u = K*nx*Ny + J*nx + i;
	  
	    if(u_dom_matrix[index_u] == 0){
	      b_u[index_u] += periodic_source * vol_x[index_u];
	    }
	  
	  }
	}
      }
    
    }

    /* Transient contribution */

    if(!flow_steady_state_OK){
    
#pragma omp parallel for default(none) private(i, J, K, index_u, aP0)	\
  shared(u_dom_matrix, vol_x, aP_u, b_u, u0)
      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  for(i=1; i<nx-1; i++){
	  
	    index_u = K*nx*Ny + J*nx + i;
	  
	    if(u_dom_matrix[index_u] == 0){
	      aP0 = rho * vol_x[index_u] / dt;
	      aP_u[index_u] += aP0;
	      b_u[index_u] += aP0 * u0[index_u];
	    }
	  }
	}
      }
    }
  
    /* Add custom source term */

    if(add_momentum_source_OK){
      add_u_momentum_source_3D(data);
    }

    /* Relax coeficients */
    if(pv_coupling_algorithm){
      relax_coeffs_3D(u, u_dom_matrix, 0, data->coeffs_u, nx, Ny, Nz, data->alpha_u);
    }

    /* Set coefficients for slave cells */
    for(i=0; i<u_slave_count; i++){
      index_u = u_slave_nodes[i];
      aP_u[index_u] = 1;
      aW_u[index_u] = 0;
      aE_u[index_u] = 0;
      aS_u[index_u] = 0;
      aN_u[index_u] = 0;
      aB_u[index_u] = 0;
      aT_u[index_u] = 0;
      b_u[index_u] = 0;
    }
  
    return;
  }
#endif
