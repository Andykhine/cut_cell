#include "include/some_defs.h"

#define _r(a,b,c,d,e,f) ( ((a) - (b)) / (c) * (d) / ((e) - (f)) )
#define _psi(r,R) __wahyd(r,R) //__upwind(r,R) // __QUICK(r,R) // __wahyd(r,R) // __van_Leer(r,R)


/* This is the modified van Leer scheme as presented by Hou et al 2011 */
double __van_Leer(double r, double R){
  
  double psi = 0;

  if(isnan(r) || isinf(r)){
    // Case for no gradient downstream phiC = phiD
    // psi = 1; // <- this is the central difference scheme
    psi = R; // <- limiting case for TVD condition
  }
  else if (r < 0){
    psi = 0;
  }
  else{
    psi = 0.5 * R * ( r + fabs(r) ) / ( R - 1 + r );
  }
  
  return psi;
  
}

/* This is the WAHYD scheme as presented by Hou et al 2011, essentially a
   modificaiton of the vL scheme */
double __wahyd(double r, double R){
  
  double psi = 0;

  if (r < 0){
    psi = 0;
  }
  else if(r <= 1){
    psi = 0.5 * R * ( r + fabs(r) ) / ( R - 1 + r );
  }
  else{ /* r > 1 */
    psi = MIN( ( r + R * r * fabs(r) ) / ( R + r * r ), R );
  }
  
  return psi;
  
}

/* This is the QUICK scheme as presented in Versteeg pp 181 valid for regular grid only */
double __QUICK(double r, double R){
  
  double psi = (3+r)/4;
  
  return psi;
  
}

/* This is the QUICK_TVD scheme as presented in Versteeg pp 184 valid for regular grid only */
double __QUICK_TVD(double r, double R){
  
  double psi = MAX( 0, MIN( MIN( 2*r, (3+r)/4 ), 2 ) );
  
  return psi;
  
}

/* This is the upwind scheme */
double __upwind(double r, double R){
  
  double psi = 0;
  
  return psi;
  
}

/* COEFFS_FLOW_u_TVD */
/*****************************************************************************/
/**
  
   Sets:

   Structure coeffs_u: coefficients of u momentum equations for internal nodes
   using van Leer TVD approach for nonuniform grids as described in Hou et
   al. 2011 with some twiches like fall into upwind scheme for complicated
   cell R factor cases.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 15-12-2020
*/
void coeffs_flow_u_TVD(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face, index_u;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int p_source_orientation = data->p_source_orientation;
  const int u_slave_count = data->u_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_u_face_x_count = data->diff_u_face_x_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
  const int *diff_u_face_x_I = data->diff_u_face_x_I;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds;
  double Fe, Fw, Fn, Fs;
  double uP, uE, uW, uN, uS;
  double uWW, uEE, uSS, uNN;
  double aP0, S_diff;
  /* Distances to nodes */
  double dEE, dWW, dE, dW;
  double dNN, dSS, dN, dS;
  /* Source terms */
  double Se, Sw, Sn, Ss;
  /* Source terms divided by convective parameter */
  double Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam;
  /* Upwind to downwind gradient */
  double rep, rem, rwp, rwm;
  double rnp, rnm, rsp, rsm;
  /* Nonuniform grid parameter */
  double Rep, Rem, Rwp, Rwm;
  double Rnp, Rnm, Rsp, Rsm;

  /* Cell sizes */
  double DW, DP, DE, DN, DS;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 2; // For 2D case
  const double periodic_source = data->periodic_source;
 
  double *aP_u = data->coeffs_u.aP;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *b_u = data->coeffs_u.b;

  const double *u = data->u;
  const double *p = data->p;

  const double *u_faces_x = data->u_faces_x;
  const double *diff_factor_u_face_x = data->diff_factor_u_face_x;

  const double *Du_x = data->Du_x;
  const double *Du_y = data->Du_y;
  const double *Fu_x = data->Fu_x;
  const double *Fu_y = data->Fu_y;
  const double *u_sol_factor = data->u_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  const double *u_cut_fe = data->u_cut_fe;
  const double *u_cut_fw = data->u_cut_fw;
  const double *vol_x = data->vol_x;
  const double *alpha_u_x = data->alpha_u_x;
  const double *alpha_u_y = data->alpha_u_y;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *u0 = data->u0;

#ifdef OLDINT_OK
  const double *cudE = data->Delta_xE_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudW = data->Delta_xW_u; /**< Distance from node to West neighbour, u nodes. */
  const double *cudN = data->Delta_yN_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudS = data->Delta_yS_u; /**< Distance from node to West neighbour, u nodes. */
#else
  const double *cudE = data->cudE;
  const double *cudW = data->cudW;
  const double *cudN = data->cudN;
  const double *cudS = data->cudS;
#endif

  const double *Dxu = data->Delta_x_u;
  const double *Dyu = data->Delta_y_u;
  
  set_D_u(data);
  set_F_u(data);
  
#pragma omp parallel for default(none)				\
  private(I, J, i, j, index_, index_u, index_face,		\
	  De, Dw, Dn, Ds, Fe, Fw, Fn, Fs,			\
	  dpA,							\
	  Fw_positive_OK, Fe_positive_OK,			\
	  Fs_positive_OK, Fn_positive_OK,			\
	  dEE, dWW, dE, dW, dNN, dSS, dN, dS,			\
	  rep, rem, rwp, rwm, rnp, rnm, rsp, rsm,		\
  	  Rep, Rem, Rwp, Rwm, Rnp, Rnm, Rsp, Rsm,		\
	  DP, DE, DW, DN, DS,					\
	  Se, Sw, Sn, Ss,					\
	  Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam,	\
	  uP, uE, uW, uN, uS, uWW, uEE, uSS, uNN)		\
  shared(u_dom_matrix, aE_u, aW_u, aS_u, aN_u, aP_u, b_u,	\
	 u, p, u_faces_x,					\
	 Du_x, Du_y, Fu_x, Fu_y, mu_turb_at_u_nodes,		\
	 cudE, cudW, cudN, cudS, Dxu, Dyu,			\
	 alpha_u_x, alpha_u_y)
  
  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      index_u = J*nx + i;

      if(u_dom_matrix[index_u] == 0){

	I = i+1;      
	j = J-1;
      
	index_ = J*Nx + I;
	index_face = j*nx + i;

	/* Distances */
	dE = cudE[index_u];
	dW = cudW[index_u];
	dN = cudN[index_u];
	dS = cudS[index_u];

	dEE = dE + cudE[index_u + 1];
	dWW = dW + cudW[index_u - 1];
	dNN = dN + cudN[index_u + nx];
	dSS = dS + cudS[index_u - nx];

	/* Velocities */
	uP = u[index_u];
	uE = u[index_u + 1];
	uW = u[index_u - 1];
	uN = u[index_u + nx];
	uS = u[index_u - nx];

	/* Diffusive */
	De = Du_x[index_];
	Dw = Du_x[index_-1];
	Dn = Du_y[index_face+nx];
	Ds = Du_y[index_face];

	/* Convective */
	Fe = Fu_x[index_] * alpha_u_x[index_] * alpha_u_x[index_];
	Fw = Fu_x[index_-1] * alpha_u_x[index_-1] * alpha_u_x[index_-1];
	Fn = Fu_y[index_face+nx] * alpha_u_y[index_face+nx];
	Fs = Fu_y[index_face] * alpha_u_y[index_face];
	
	Fe_positive_OK = (Fe > 0);
	Fw_positive_OK = (Fw > 0);
	Fn_positive_OK = (Fn > 0);
	Fs_positive_OK = (Fs > 0);

	/* Extrapolation for presence of boundaries */
	/* Should be done for internal boundaries as well */
	
	if(i == 1){
	  uWW = 2 * uW - uP;
	  dWW = 2*dW;
	}
	else if (u_dom_matrix[index_u - 2] != 0){
	  /* WW within solid */
	  uWW = 2 * uW - uP;
	  dWW = 2*dW;
	}
	else{
	  uWW = u[index_u-2];
	}

	if(i == nx-2){
	  uEE = 2 * uE - uP;
	  dEE = 2*dE;
	}
	else if(u_dom_matrix[index_u + 2] != 0){
	  /* EE within solid */
	  uEE = 2 * uE - uP;
	  dEE = 2*dE;
	}
	else{
	  uEE = u[index_u+2];
	}

	if(J == 1){
	  uSS = 2 * uS - uP;
	  dSS = 2*dS;
	}
	else if(u_dom_matrix[index_u - 2*nx] != 0){
	  uSS = 2 * uS - uP;
	  dSS = 2*dS;
	}
	else{
	  uSS = u[index_u-2*nx];
	}

	if(J == Ny-2){
	  uNN = 2 * uN - uP;
	  dNN = 2*dN;
	}
	else if(u_dom_matrix[index_u + 2*nx] != 0){
	  uNN = 2 * uN - uP;
	  dNN = 2*dN;
	}
	else{
	  uNN = u[index_u+2*nx];
	}

	dpA = u_faces_x[index_] * p[index_] - 
	  u_faces_x[index_-1] * p[index_-1];

	b_u[index_u] = -dpA;
	
	aE_u[index_u] = De + MAX(-Fe, 0);
	aW_u[index_u] = Dw + MAX(+Fw, 0);
	aN_u[index_u] = Dn + MAX(-Fn, 0);
	aS_u[index_u] = Ds + MAX(+Fs, 0);

	aP_u[index_u] = aE_u[index_u] + aW_u[index_u] + aN_u[index_u] + aS_u[index_u];
	//	  aE_u[index_u] + aW_u[index_u] + aN_u[index_u] + aS_u[index_u] + (Fe - Fw) + (Fn - Fs);

	// EW
	DP = Dxu[index_u];
	DE = 2*(dE - DP/2);
	DW = 2*(dW - DP/2);
	
	// e
	rep = _r(uP, uW,      dW, dE, uE, uP);
	rem = _r(uE, uEE, dEE-dE, dE, uP, uE);

	Rep = (DP + DE) / DP;
	Rem = (DE + DP) / DE;
	
	Seap = _psi(rep,Rep) / Rep;
	
	Seam = _psi(rem,Rem) / Rem;
	  
	Se = Fe * ((1 - Fe_positive_OK) * Seam - Fe_positive_OK * Seap) * (uE - uP);

	// Back to upwind scheme if no convective face at e
	Se = (DE <= 0) ? 0 : Se;

	// w
	rwp = _r(uW, uWW, dWW-dW, dW, uP, uW);
	rwm = _r(uP,  uE,     dE, dW, uW, uP);
	
	Rwp = (DW + DP) / DW;
	Rwm = (DP + DW) / DP;
	
	Swap = _psi(rwp,Rwp) / Rwp;
	
	Swam = _psi(rwm,Rwm) / Rwm;
	  
	Sw = Fw * (Fw_positive_OK * Swap - (1 - Fw_positive_OK) * Swam) * (uP - uW);

	Sw = (DW <= 0) ? 0 : Sw;

	// NS
	DP = Dyu[index_u];
	DN = 2*(dN - DP/2);
	DS = 2*(dS - DP/2);
	
	// n
	rnp = _r(uP, uS,      dS, dN, uN, uP);
	rnm = _r(uN, uNN, dNN-dN, dN, uP, uN);

	Rnp = (DP + DN) / DP;
	Rnm = (DN + DP) / DN;
	
	Snap = _psi(rnp,Rnp) / Rnp;
	
	Snam = _psi(rnm,Rnm) / Rnm;
	  
	Sn = Fn * ((1 - Fn_positive_OK) * Snam - Fn_positive_OK * Snap) * (uN - uP);

	Sn = (DN <= 0) ? 0 : Sn;

	// s
	rsp = _r(uS, uSS, dSS-dS, dS, uP, uS);
	rsm = _r(uP,  uN,     dN, dS, uS, uP);
	
	Rsp = (DS + DP) / DS;
	Rsm = (DP + DS) / DP;
	
	Ssap = _psi(rsp,Rsp) / Rsp;
	
	Ssam = _psi(rsm,Rsm) / Rsm;
	  
	Ss = Fs * (Fs_positive_OK * Ssap - (1 - Fs_positive_OK) * Ssam) * (uP - uS);

	Ss = (DS <= 0) ? 0 : Ss;

	b_u[index_u] += Se + Sw + Sn + Ss;
	
      }
      else{
	aP_u[index_u] = 1;
	aW_u[index_u] = 0;
	aE_u[index_u] = 0;
	aS_u[index_u] = 0;
	aN_u[index_u] = 0;
	b_u[index_u] = 0;
      }
    }
  }

  /* Diffusive factor */  
  for(z=0; z<diff_u_face_x_count; z++){
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    I = diff_u_face_x_I[z];
    mu_eff = mu + 0.5 * (mu_turb_at_u_nodes[index_u] + mu_turb_at_u_nodes[index_u+1]);
    S_diff = mu_eff * diff_factor_u_face_x[z] * (u[index_u] * u_cut_fw[index_] + u[index_u+1] * u_cut_fe[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((u_dom_matrix[index_u] == 0) && (I > 1)){
      b_u[index_u] -= S_diff;
    }
    if((u_dom_matrix[index_u+1] == 0) && (I < Nx-2)){
      b_u[index_u+1] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<u_sol_factor_count; z++){
      index_u = u_sol_factor_index_u[z];
      
      if(u_dom_matrix[index_u] == 0){

	index_ = u_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_u_nodes[index_u];
	aP_u[index_u] += mu_eff * u_sol_factor[z];
	b_u[index_u] += u_p_factor[z] * (u_p_factor_W[z] * p[index_-1] 
					 + (1 - u_p_factor_W[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 1){

#pragma omp parallel for default(none) private(i, J, index_u)	\
  shared(u_dom_matrix, b_u, vol_x)

    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = J*nx + i;
	if(u_dom_matrix[index_u] == 0){
	  b_u[index_u] += periodic_source * vol_x[index_u];
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(i, J, index_u, aP0)	\
  shared(u_dom_matrix, vol_x, aP_u, b_u, u0)

    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = J*nx + i;
	if(u_dom_matrix[index_u] == 0){
	  aP0 = rho * vol_x[index_u] / dt;
	  aP_u[index_u] += aP0;
	  b_u[index_u] += aP0 * u0[index_u];
	}
      }
    }
  }

  /* Add custom source term */

  if(add_momentum_source_OK){
    add_u_momentum_source(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs(u, u_dom_matrix, 0, data->coeffs_u, nx, Ny, data->alpha_u);
  }

  /* Set coefficients for slave cells */
  for(i=0; i<u_slave_count; i++){
    index_u = u_slave_nodes[i];
    aP_u[index_u] = 1;
    aW_u[index_u] = 0;
    aE_u[index_u] = 0;
    aS_u[index_u] = 0;
    aN_u[index_u] = 0;
    b_u[index_u] = 0;
  }
  
  return;
}

/* COEFFS_FLOW_v_TVD */
/**
  
   Sets:

   Structure coeffs_v: coefficients of v momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 16-12-2020
*/
void coeffs_flow_v_TVD(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face, index_v;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int v_slave_count = data->v_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int p_source_orientation = data->p_source_orientation;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_v_face_y_count = data->diff_v_face_y_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *v_dom_matrix = data->v_dom_matrix;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
  const int *diff_v_face_y_J = data->diff_v_face_y_J;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds;
  double Fe, Fw, Fn, Fs;
  double vP, vE, vW, vN, vS;
  double vWW, vEE, vSS, vNN;
  double aP0, S_diff;
  /* Distances to nodes */
  double dEE, dWW, dE, dW, dNN, dSS, dN, dS;
  /* Source terms */
  double Se, Sw, Sn, Ss;
  /* Source terms divided by convective parameter */
  double Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam;
  /* Upwind to downwind gradient */
  double rep, rem, rwp, rwm;
  double rnp, rnm, rsp, rsm;
  /* Nonuniform grid parameter */
  double Rep, Rem, Rwp, Rwm;
  double Rnp, Rnm, Rsp, Rsm;

  /* Cell sizes */
  double DW, DP, DE, DN, DS;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 2; // For 2D case
  const double periodic_source = data->periodic_source;
 
  double *aP_v = data->coeffs_v.aP;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *b_v = data->coeffs_v.b;

  const double *v = data->v;
  const double *p = data->p;

  const double *v_faces_y = data->v_faces_y;
  const double *diff_factor_v_face_y = data->diff_factor_v_face_y;
  const double *Dv_x = data->Dv_x;
  const double *Dv_y = data->Dv_y;
  const double *Fv_x = data->Fv_x;
  const double *Fv_y = data->Fv_y;
  const double *v_sol_factor = data->v_sol_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  const double *v_cut_fn = data->v_cut_fn;
  const double *v_cut_fs = data->v_cut_fs;
  const double *vol_y = data->vol_y;
  const double *alpha_v_x = data->alpha_v_x;
  const double *alpha_v_y = data->alpha_v_y;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *v0 = data->v0;

#ifdef OLDINT_OK
  const double *cvdE = data->Delta_xE_v;
  const double *cvdW = data->Delta_xW_v;
  const double *cvdN = data->Delta_yN_v;
  const double *cvdS = data->Delta_yS_v;
#else
  const double *cvdE = data->cvdE;
  const double *cvdW = data->cvdW;
  const double *cvdN = data->cvdN;
  const double *cvdS = data->cvdS;
#endif

  const double *Dxv = data->Delta_x_v;
  const double *Dyv = data->Delta_y_v;
  
  set_D_v(data);
  set_F_v(data);
  
  /* V */
#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_v, index_face,			\
	  De, Dw, Dn, Ds, Fe, Fw, Fn, Fs,				\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
	  dEE, dWW, dE, dW, dNN, dSS, dN, dS,				\
	  rep, rem, rwp, rwm, rnp, rnm, rsp, rsm,			\
  	  Rep, Rem, Rwp, Rwm, Rnp, Rnm, Rsp, Rsm,			\
	  DP, DE, DW, DN, DS,						\
	  Se, Sw, Sn, Ss,						\
	  Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam,		\
	  vP, vE, vW, vN, vS, vWW, vEE, vSS, vNN)			\
	  shared(v_dom_matrix, aE_v, aW_v, aS_v, aN_v, aP_v, b_v,	\
		 v, p, v_faces_y,					\
		 Dv_x, Dv_y, Fv_x, Fv_y, mu_turb_at_v_nodes,		\
		 cvdE, cvdW, cvdN, cvdS, Dxv, Dyv,			\
		 alpha_v_x, alpha_v_y)

  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){

      index_v = j*Nx + I;

      if(v_dom_matrix[index_v] == 0){

	i = I-1;
	J = j+1;
      
	index_ = J*Nx + I;
	index_face = j*nx + i;

	/* Distances */
	dE = cvdE[index_v];
	dW = cvdW[index_v];
	dN = cvdN[index_v];
	dS = cvdS[index_v];

	dEE = dE + cvdE[index_v + 1];
	dWW = dW + cvdW[index_v - 1];
	dNN = dN + cvdN[index_v + Nx];
	dSS = dS + cvdS[index_v - Nx];

	/* Velocities */
	vP = v[index_v];
	vE = v[index_v + 1];
	vW = v[index_v - 1];
	vN = v[index_v + Nx];
	vS = v[index_v - Nx];
	
	/* Diffusive */
	De = Dv_x[index_face+1];
	Dw = Dv_x[index_face];
	Dn = Dv_y[index_];
	Ds = Dv_y[index_-Nx];

	/* Convective */
	Fe = Fv_x[index_face+1] * alpha_v_x[index_face+1];
	Fw = Fv_x[index_face] * alpha_v_x[index_face];
	Fn = Fv_y[index_] * alpha_v_y[index_] * alpha_v_y[index_];
	Fs = Fv_y[index_-Nx] * alpha_v_y[index_-Nx] * alpha_v_y[index_-Nx];

	Fe_positive_OK = (Fe > 0);
	Fw_positive_OK = (Fw > 0);
	Fn_positive_OK = (Fn > 0);
	Fs_positive_OK = (Fs > 0);

	if(I == 1){
	  vWW = 2 * vW - vP;
	  dWW = 2*dW;
	}
	else if(v_dom_matrix[index_v - 2] != 0){
	  vWW = 2 * vW - vP;
	  dWW = 2*dW;
	}
	else{
	  vWW = v[index_v-2];
	}

	if(I == Nx-2){
	  vEE = 2 * vE - vP;
	  dEE = 2*dE;
	}
	else if(v_dom_matrix[index_v + 2] != 0){
	  vEE = 2 * vE - vP;
	  dEE = 2*dE;
	}
	else{
	  vEE = v[index_v+2];
	}
	
	if(j == 1){
	  vSS = 2 * vS - vP;
	  dSS = 2*dS;
	}
	else if(v_dom_matrix[index_v - 2*Nx] != 0){
	  vSS = 2 * vS - vP;
	  dSS = 2*dS;
	}
	else{
	  vSS = v[index_v-2*Nx];
	}

	if(j == ny-2){
	  vNN = 2 * vN - vP;
	  dNN = 2*dN;
	}
	else if(v_dom_matrix[index_v + 2*Nx] != 0){
	  vNN = 2 * vN - vP;
	  dNN = 2*dN;
	}
	else{
	  vNN = v[index_v+2*Nx];
	}

	dpA = v_faces_y[index_] * p[index_] - 
	  v_faces_y[index_-Nx] * p[index_-Nx];

	b_v[index_v] = -dpA;

	aE_v[index_v] = De + MAX(-Fe, 0);
	aW_v[index_v] = Dw + MAX(+Fw, 0);
	aN_v[index_v] = Dn + MAX(-Fn, 0);
	aS_v[index_v] = Ds + MAX(+Fs, 0);

	aP_v[index_v] = aE_v[index_v] + aW_v[index_v] + aN_v[index_v] + aS_v[index_v];
	//	  aE_v[index_v] + aW_v[index_v] + aN_v[index_v] + aS_v[index_v] + (Fe - Fw) + (Fn - Fs);

	// EW
	DP = Dxv[index_v];
	DE = 2*(dE - DP/2);
	DW = 2*(dW - DP/2);
	
	// e
	rep = _r(vP, vW,      dW, dE, vE, vP);
	rem = _r(vE, vEE, dEE-dE, dE, vP, vE);

	Rep = (DP + DE) / DP;
	Rem = (DE + DP) / DE;
	
	Seap = _psi(rep,Rep) / Rep;
	
	Seam = _psi(rem,Rem) / Rem;
	  
	Se = Fe * ((1 - Fe_positive_OK) * Seam - Fe_positive_OK * Seap) * (vE - vP);

	Se = (DE <= 0) ? 0 : Se;

	// w
	rwp = _r(vW, vWW, dWW-dW, dW, vP, vW);
	rwm = _r(vP,  vE,     dE, dW, vW, vP);
	
	Rwp = (DW + DP) / DW;
	Rwm = (DP + DW) / DP;
	
	Swap = _psi(rwp,Rwp) / Rwp;
	
	Swam = _psi(rwm,Rwm) / Rwm;
	  
	Sw = Fw * (Fw_positive_OK * Swap - (1 - Fw_positive_OK) * Swam) * (vP - vW);

	Sw = (DW <= 0) ? 0 : Sw;

	// NS
	DP = Dyv[index_v];
	DN = 2*(dN - DP/2);
	DS = 2*(dS - DP/2);

	// n
	rnp = _r(vP, vS,      dS, dN, vN, vP);
	rnm = _r(vN, vNN, dNN-dN, dN, vP, vN);

	Rnp = (DP + DN) / DP;
	Rnm = (DN + DP) / DN;
	
	Snap = _psi(rnp,Rnp) / Rnp;
	
	Snam = _psi(rnm,Rnm) / Rnm;
	  
	Sn = Fn * ((1 - Fn_positive_OK) * Snam - Fn_positive_OK * Snap) * (vN - vP);

	Sn = (DN <= 0) ? 0 : Sn;

	// s
	rsp = _r(vS, vSS, dSS-dS, dS, vP, vS);
	rsm = _r(vP,  vN,     dN, dS, vS, vP);
	
	Rsp = (DS + DP) / DS;
	Rsm = (DP + DS) / DP;
	
	Ssap = _psi(rsp,Rsp) / Rsp;
	
	Ssam = _psi(rsm,Rsm) / Rsm;
	  
	Ss = Fs * (Fs_positive_OK * Ssap - (1 - Fs_positive_OK) * Ssam) * (vP - vS);

	Ss = (DS <= 0) ? 0 : Ss;

	b_v[index_v] += Se + Sw + Sn + Ss;
	
      }
      else{
	aP_v[index_v] = 1;
	aW_v[index_v] = 0;
	aE_v[index_v] = 0;
	aS_v[index_v] = 0;
	aN_v[index_v] = 0;
	b_v[index_v] = 0;
      }
    }
  }

  /* Diffusive factor */
  for(z=0; z<diff_v_face_y_count; z++){
    index_v = diff_v_face_y_index_v[z];
    index_ = diff_v_face_y_index[z];
    J = diff_v_face_y_J[z];
    mu_eff = mu + 0.5 * (mu_turb_at_v_nodes[index_v] + mu_turb_at_v_nodes[index_v+Nx]);
    S_diff = mu_eff * diff_factor_v_face_y[z] * (v[index_v] * v_cut_fs[index_] + v[index_v+Nx] * v_cut_fn[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((v_dom_matrix[index_v] == 0) && (J > 1)){
      b_v[index_v] -= S_diff;
    }
    if((v_dom_matrix[index_v+Nx] == 0) && (J < Ny-2)){
      b_v[index_v+Nx] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<v_sol_factor_count; z++){
      index_v = v_sol_factor_index_v[z];
      
      if(v_dom_matrix[index_v] == 0){
	index_ = v_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_v_nodes[index_v];
	aP_v[index_v] += mu_eff * v_sol_factor[z];
	b_v[index_v] += v_p_factor[z] * (v_p_factor_S[z] * p[index_-Nx] 
					 + (1 - v_p_factor_S[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, j, index_v)	\
  shared(v_dom_matrix, b_v, vol_y)

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = j*Nx + I;
	if(v_dom_matrix[index_v] == 0){
	  b_v[index_v] += periodic_source * vol_y[index_v];
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, j, index_v, aP0)	\
  shared(v_dom_matrix, vol_y, aP_v, b_v, v0)

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = j*Nx + I;
	if(v_dom_matrix[index_v] == 0){
	  aP0 = rho * vol_y[index_v] / dt;
	  aP_v[index_v] += aP0;
	  b_v[index_v] += aP0 * v0[index_v];
	}
      }
    }
  }

  /* Add custom source term */

  if(add_momentum_source_OK){
    add_v_momentum_source(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs(v, v_dom_matrix, 0, data->coeffs_v, Nx, ny, data->alpha_v);
  }

  for(i=0; i<v_slave_count; i++){
    index_v = v_slave_nodes[i];
    aP_v[index_v] = 1;
    aW_v[index_v] = 0;
    aE_v[index_v] = 0;
    aS_v[index_v] = 0;
    aN_v[index_v] = 0;
    b_v[index_v] = 0;
  }

  return;
}

/* COEFFS_FLOW_u_TVD_3D */
/*****************************************************************************/
/**
  
   Sets:

   Structure coeffs_u: coefficients of u momentum equations for internal nodes
   using van Leer TVD approach for nonuniform grids as described in Hou et
   al. 2011 with some twiches like fall into upwind scheme for complicated
   cell R factor cases.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 09-05-2021
*/
void coeffs_flow_u_TVD_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_u;
  int index_face_y, index_face_z;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;
  int Fb_positive_OK, Ft_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;

  const int p_source_orientation = data->p_source_orientation;
  const int u_slave_count = data->u_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_u_face_x_count = data->diff_u_face_x_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
  const int *diff_u_face_x_I = data->diff_u_face_x_I;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double uP, uE, uW, uN, uS, uT, uB;
  double uWW, uEE, uSS, uNN, uBB, uTT;
  double aP0, S_diff;
  /* Distances to nodes */
  double dEE, dWW, dE, dW;
  double dNN, dSS, dN, dS;
  double dTT, dBB, dT, dB;
  /* Source terms */
  double Se, Sw, Sn, Ss, St, Sb;
  /* Source terms divided by convective parameter */
  double Seap, Seam, Swap, Swam;
  double Snap, Snam, Ssap, Ssam;
  double Stap, Stam, Sbap, Sbam;
  /* Upwind to downwind gradient */
  double rep, rem, rwp, rwm;
  double rnp, rnm, rsp, rsm;
  double rtp, rtm, rbp, rbm;
  /* Nonuniform grid parameter */
  double Rep, Rem, Rwp, Rwm;
  double Rnp, Rnm, Rsp, Rsm;
  double Rtp, Rtm, Rbp, Rbm;

  /* Cell sizes */
  double DW, DP, DE, DN, DS, DT, DB;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_u = data->coeffs_u.aP;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *aB_u = data->coeffs_u.aB;
  double *aT_u = data->coeffs_u.aT;
  double *b_u = data->coeffs_u.b;

  const double *u = data->u;
  const double *p = data->p;

  const double *u_faces_x = data->u_faces_x;
  const double *diff_factor_u_face_x = data->diff_factor_u_face_x;

  const double *Du_x = data->Du_x;
  const double *Du_y = data->Du_y;
  const double *Du_z = data->Du_z;
  const double *Fu_x = data->Fu_x;
  const double *Fu_y = data->Fu_y;
  const double *Fu_z = data->Fu_z;
  
  const double *u_sol_factor = data->u_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  
  const double *u_cut_fe = data->u_cut_fe;
  const double *u_cut_fw = data->u_cut_fw;
  const double *vol_x = data->vol_x;
  const double *alpha_u_x = data->alpha_u_x;
  const double *alpha_u_y = data->alpha_u_y;
  const double *alpha_u_z = data->alpha_u_z;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *u0 = data->u0;

#ifdef OLDINT_OK
  const double *cudE = data->Delta_xE_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudW = data->Delta_xW_u; /**< Distance from node to West neighbour, u nodes. */
  const double *cudN = data->Delta_yN_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudS = data->Delta_yS_u; /**< Distance from node to West neighbour, u nodes. */
  const double *cudT = data->Delta_zT_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudB = data->Delta_zB_u; /**< Distance from node to West neighbour, u nodes. */
#else
  const double *cudE = data->cudE;
  const double *cudW = data->cudW;
  const double *cudN = data->cudN;
  const double *cudS = data->cudS;
  const double *cudT = data->cudT;
  const double *cudB = data->cudB;
#endif

  const double *Dxu = data->Delta_x_u;
  const double *Dyu = data->Delta_y_u;
  const double *Dzu = data->Delta_z_u;
  
  set_D_u_3D(data);
  set_F_u_3D(data);

#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_u,				\
	  index_face_y, index_face_z,					\
	  De, Dw, Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb,		\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
  	  Fb_positive_OK, Ft_positive_OK,				\
	  dEE, dWW, dE, dW,						\
	  dNN, dSS, dN, dS,						\
	  dTT, dBB, dT, dB,						\
  	  rep, rem, rwp, rwm,						\
	  rnp, rnm, rsp, rsm,						\
	  rtp, rtm, rbp, rbm,						\
  	  Rep, Rem, Rwp, Rwm,						\
	  Rnp, Rnm, Rsp, Rsm,						\
	  Rtp, Rtm, Rbp, Rbm,						\
	  DP, DE, DW, DN, DS, DT, DB,					\
	  Se, Sw, Sn, Ss, St, Sb,					\
	  Seap, Seam, Swap, Swam,					\
	  Snap, Snam, Ssap, Ssam,					\
	  Stap, Stam, Sbap, Sbam,					\
	  uP, uE, uW, uN, uS, uT, uB,					\
	  uWW, uEE, uSS, uNN, uBB, uTT)					\
  shared(u_dom_matrix,							\
	 aE_u, aW_u, aS_u, aN_u, aT_u, aB_u, aP_u, b_u,			\
	 u, p, u_faces_x,						\
	 Du_x, Du_y, Du_z, Fu_x, Fu_y, Fu_z, mu_turb_at_u_nodes,	\
	 Dxu, Dyu, Dzu, cudE, cudW, cudN, cudS, cudT, cudB, 		\
	 alpha_u_x, alpha_u_y, alpha_u_z)

  for(K=1; K<Nz-1; K++){  
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	
	index_u = K*nx*Ny + J*nx + i;
	
	if(u_dom_matrix[index_u] == 0){

	  I = i+1;      
	  j = J-1;
	  k = K-1;
	
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_face_y = K*nx*ny + j*nx + i; // Du,Fu_y
	  index_face_z = k*nx*Ny + J*nx + i; // Du,Fu_z

	  /* Distances */
	  dE = cudE[index_u];
	  dW = cudW[index_u];
	  dN = cudN[index_u];
	  dS = cudS[index_u];
	  dT = cudT[index_u];
	  dB = cudB[index_u];

	  dEE = dE + cudE[index_u + 1];
	  dWW = dW + cudW[index_u - 1];
	  dNN = dN + cudN[index_u + nx];
	  dSS = dS + cudS[index_u - nx];
	  dTT = dT + cudT[index_u + nx*Ny];
	  dBB = dB + cudB[index_u - nx*Ny];

	  /* Velocities */
	  uP = u[index_u];
	  uE = u[index_u + 1];
	  uW = u[index_u - 1];
	  uN = u[index_u + nx];
	  uS = u[index_u - nx];
	  uT = u[index_u + nx*Ny];
	  uB = u[index_u - nx*Ny];

	  /* Diffusive */
	  De = Du_x[index_];
	  Dw = Du_x[index_ - 1];
	  Dn = Du_y[index_face_y + nx];
	  Ds = Du_y[index_face_y];
	  Dt = Du_z[index_face_z + nx*Ny];
	  Db = Du_z[index_face_z];

	  /* Convective */
	  Fe = Fu_x[index_] * alpha_u_x[index_] * alpha_u_x[index_];
	  Fw = Fu_x[index_ - 1] * alpha_u_x[index_ - 1] * alpha_u_x[index_ - 1];
	  Fn = Fu_y[index_face_y + nx] * alpha_u_y[index_face_y + nx];
	  Fs = Fu_y[index_face_y] * alpha_u_y[index_face_y];
	  Ft = Fu_z[index_face_z + nx*Ny] * alpha_u_z[index_face_z + nx*Ny];
	  Fb = Fu_z[index_face_z] * alpha_u_z[index_face_z];
	
	  Fe_positive_OK = (Fe > 0);
	  Fw_positive_OK = (Fw > 0);
	  Fn_positive_OK = (Fn > 0);
	  Fs_positive_OK = (Fs > 0);
	  Ft_positive_OK = (Ft > 0);
	  Fb_positive_OK = (Fb > 0);

	  /* Extrapolation for presence of boundaries */
	  /* Should be done for internal boundaries as well */
	
	  if(i == 1){
	    uWW = 2 * uW - uP;
	    dWW = 2*dW;
	  }
	  else if (u_dom_matrix[index_u - 2] != 0){
	    /* WW within solid */
	    uWW = 2 * uW - uP;
	    dWW = 2*dW;
	  }
	  else{
	    uWW = u[index_u-2];
	  }

	  if(i == nx-2){
	    uEE = 2 * uE - uP;
	    dEE = 2*dE;
	  }
	  else if(u_dom_matrix[index_u + 2] != 0){
	    /* EE within solid */
	    uEE = 2 * uE - uP;
	    dEE = 2*dE;
	  }
	  else{
	    uEE = u[index_u+2];
	  }

	  if(J == 1){
	    uSS = 2 * uS - uP;
	    dSS = 2*dS;
	  }
	  else if(u_dom_matrix[index_u - 2*nx] != 0){
	    uSS = 2 * uS - uP;
	    dSS = 2*dS;
	  }
	  else{
	    uSS = u[index_u-2*nx];
	  }

	  if(J == Ny-2){
	    uNN = 2 * uN - uP;
	    dNN = 2*dN;
	  }
	  else if(u_dom_matrix[index_u + 2*nx] != 0){
	    uNN = 2 * uN - uP;
	    dNN = 2*dN;
	  }
	  else{
	    uNN = u[index_u+2*nx];
	  }
	
	  if(K == 1){
	    uBB = 2 * uB - uP;
	    dBB = 2*dB;
	  }
	  else if(u_dom_matrix[index_u - 2*nx*Ny] != 0){
	    uBB = 2 * uB - uP;
	    dBB = 2*dB;
	  }
	  else{
	    uBB = u[index_u - 2*nx*Ny];
	  }

	  if(K == Nz-2){
	    uTT = 2 * uT - uP;
	    dTT = 2*dT;
	  }
	  else if(u_dom_matrix[index_u + 2*nx*Ny] != 0){
	    uTT = 2 * uT - uP;
	    dTT = 2*dT;
	  }
	  else{
	    uTT = u[index_u + 2*nx*Ny];
	  }

	  dpA = u_faces_x[index_] * p[index_] - 
	    u_faces_x[index_-1] * p[index_-1];

	  b_u[index_u] = -dpA;
	
	  aE_u[index_u] = De + MAX(-Fe, 0);
	  aW_u[index_u] = Dw + MAX(+Fw, 0);
	  aN_u[index_u] = Dn + MAX(-Fn, 0);
	  aS_u[index_u] = Ds + MAX(+Fs, 0);
	  aT_u[index_u] = Dt + MAX(-Ft, 0);
	  aB_u[index_u] = Db + MAX(+Fb, 0);

	  aP_u[index_u] =
	    aE_u[index_u] + aW_u[index_u] +
	    aN_u[index_u] + aS_u[index_u] +
	    aT_u[index_u] + aB_u[index_u];
	    /*
	    aE_u[index_u] + aW_u[index_u] +
	    aN_u[index_u] + aS_u[index_u] +
	    aT_u[index_u] + aB_u[index_u] +
	    (Fe - Fw) + (Fn - Fs) + (Ft - Fb);
	    */

	  // EW
	  DP = Dxu[index_u];
	  DE = 2*(dE - DP/2);
	  DW = 2*(dW - DP/2);
	
	  // e
	  rep = _r(uP, uW,      dW, dE, uE, uP);
	  rem = _r(uE, uEE, dEE-dE, dE, uP, uE);

	  Rep = (DP + DE) / DP;
	  Rem = (DE + DP) / DE;
	
	  Seap = _psi(rep,Rep) / Rep;
	
	  Seam = _psi(rem,Rem) / Rem;
	  
	  Se = Fe * ((1 - Fe_positive_OK) * Seam - Fe_positive_OK * Seap) * (uE - uP);

	  // Back to upwind scheme if no convective face at e
	  Se = (DE <= 0) ? 0 : Se;

	  // w
	  rwp = _r(uW, uWW, dWW-dW, dW, uP, uW);
	  rwm = _r(uP,  uE,     dE, dW, uW, uP);
	
	  Rwp = (DW + DP) / DW;
	  Rwm = (DP + DW) / DP;
	
	  Swap = _psi(rwp,Rwp) / Rwp;
	
	  Swam = _psi(rwm,Rwm) / Rwm;
	  
	  Sw = Fw * (Fw_positive_OK * Swap - (1 - Fw_positive_OK) * Swam) * (uP - uW);

	  Sw = (DW <= 0) ? 0 : Sw;

	  // NS
	  DP = Dyu[index_u];
	  DN = 2*(dN - DP/2);
	  DS = 2*(dS - DP/2);
	
	  // n
	  rnp = _r(uP, uS,      dS, dN, uN, uP);
	  rnm = _r(uN, uNN, dNN-dN, dN, uP, uN);

	  Rnp = (DP + DN) / DP;
	  Rnm = (DN + DP) / DN;
	
	  Snap = _psi(rnp,Rnp) / Rnp;
	
	  Snam = _psi(rnm,Rnm) / Rnm;
	  
	  Sn = Fn * ((1 - Fn_positive_OK) * Snam - Fn_positive_OK * Snap) * (uN - uP);

	  Sn = (DN <= 0) ? 0 : Sn;

	  // s
	  rsp = _r(uS, uSS, dSS-dS, dS, uP, uS);
	  rsm = _r(uP,  uN,     dN, dS, uS, uP);
	
	  Rsp = (DS + DP) / DS;
	  Rsm = (DP + DS) / DP;
	
	  Ssap = _psi(rsp,Rsp) / Rsp;
	
	  Ssam = _psi(rsm,Rsm) / Rsm;
	  
	  Ss = Fs * (Fs_positive_OK * Ssap - (1 - Fs_positive_OK) * Ssam) * (uP - uS);

	  Ss = (DS <= 0) ? 0 : Ss;

	  // TB
	  DP = Dzu[index_u];
	  DT = 2*(dT - DP/2);
	  DB = 2*(dB - DP/2);
	
	  // t
	  rtp = _r(uP, uB,      dB, dT, uT, uP);
	  rtm = _r(uT, uTT, dTT-dT, dT, uP, uT);

	  Rtp = (DP + DT) / DP;
	  Rtm = (DT + DP) / DT;
	
	  Stap = _psi(rtp,Rtp) / Rtp;
	
	  Stam = _psi(rtm,Rtm) / Rtm;
	  
	  St = Ft * ((1 - Ft_positive_OK) * Stam - Ft_positive_OK * Stap) * (uT - uP);

	  // Back to upwind scheme if no convective face at e
	  St = (DT <= 0) ? 0 : St;

	  // b
	  rbp = _r(uB, uBB, dBB-dB, dB, uP, uB);
	  rbm = _r(uP,  uT,     dT, dB, uB, uP);
	
	  Rbp = (DB + DP) / DB;
	  Rbm = (DP + DB) / DP;
	
	  Sbap = _psi(rbp,Rbp) / Rbp;
	
	  Sbam = _psi(rbm,Rbm) / Rbm;
	  
	  Sb = Fb * (Fb_positive_OK * Sbap - (1 - Fb_positive_OK) * Sbam) * (uP - uB);

	  Sb = (DB <= 0) ? 0 : Sb;

	  b_u[index_u] += Se + Sw + Sn + Ss + St + Sb; // Sw - Se + Ss - Sn + Sb - St;
	
	}
	else{
	  aP_u[index_u] = 1;
	  aW_u[index_u] = 0;
	  aE_u[index_u] = 0;
	  aS_u[index_u] = 0;
	  aN_u[index_u] = 0;
	  aB_u[index_u] = 0;
	  aT_u[index_u] = 0;
	  b_u[index_u] = 0;
	}
      }
    }
  }
  
  /* Diffusive factor */
  for(z=0; z<diff_u_face_x_count; z++){
    
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    
    I = diff_u_face_x_I[z];
    
    mu_eff = mu + 0.5 * (mu_turb_at_u_nodes[index_u] + mu_turb_at_u_nodes[index_u+1]);
    S_diff = mu_eff * diff_factor_u_face_x[z] *
      (u[index_u] * u_cut_fw[index_] + u[index_u+1] * u_cut_fe[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((u_dom_matrix[index_u] == 0) && (I > 1)){
      b_u[index_u] -= S_diff;
    }
    if((u_dom_matrix[index_u+1] == 0) && (I < Nx-2)){
      b_u[index_u+1] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<u_sol_factor_count; z++){
      
      index_u = u_sol_factor_index_u[z];
      
      if(u_dom_matrix[index_u] == 0){

	index_ = u_sol_factor_index[z];
	
	mu_eff = mu + mu_turb_at_u_nodes[index_u];
	
	aP_u[index_u] += mu_eff * u_sol_factor[z];
	b_u[index_u] += u_p_factor[z] *
	  (u_p_factor_W[z] * p[index_-1] + (1 - u_p_factor_W[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 1){
    
#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(u_dom_matrix, b_u, vol_x)
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	  
	  index_u = K*nx*Ny + J*nx + i;
	  
	  if(u_dom_matrix[index_u] == 0){
	    b_u[index_u] += periodic_source * vol_x[index_u];
	  }
	  
	}
      }
    }
    
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){
    
#pragma omp parallel for default(none) private(i, J, K, index_u, aP0)	\
  shared(u_dom_matrix, vol_x, aP_u, b_u, u0)
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	  
	  index_u = K*nx*Ny + J*nx + i;
	  
	  if(u_dom_matrix[index_u] == 0){
	    aP0 = rho * vol_x[index_u] / dt;
	    aP_u[index_u] += aP0;
	    b_u[index_u] += aP0 * u0[index_u];
	  }
	}
      }
    }
  }
  
  /* Add custom source term */

  if(add_momentum_source_OK){
    add_u_momentum_source_3D(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(u, u_dom_matrix, 0, data->coeffs_u, nx, Ny, Nz, data->alpha_u);
  }

  /* Set coefficients for slave cells */
  for(i=0; i<u_slave_count; i++){
    index_u = u_slave_nodes[i];
    aP_u[index_u] = 1;
    aW_u[index_u] = 0;
    aE_u[index_u] = 0;
    aS_u[index_u] = 0;
    aN_u[index_u] = 0;
    aB_u[index_u] = 0;
    aT_u[index_u] = 0;
    b_u[index_u] = 0;
  }
  
  return;
}

/* COEFFS_FLOW_v_TVD_3D */
/**
  
   Sets:

   Structure coeffs_v: coefficients of v momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 09-05-2021
*/
void coeffs_flow_v_TVD_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_v;
  int index_face_x, index_face_z;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;
  int Fb_positive_OK, Ft_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;

  const int v_slave_count = data->v_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int p_source_orientation = data->p_source_orientation;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_v_face_y_count = data->diff_v_face_y_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *v_dom_matrix = data->v_dom_matrix;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
  const int *diff_v_face_y_J = data->diff_v_face_y_J;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double vP, vE, vW, vN, vS, vT, vB;
  double vWW, vEE, vSS, vNN, vBB, vTT;
  double aP0, S_diff;
  /* Distances to nodes */
  double dEE, dWW, dE, dW;
  double dNN, dSS, dN, dS;
  double dTT, dBB, dT, dB;
  /* Source terms */
  double Se, Sw, Sn, Ss, St, Sb;
  /* Source terms divided by convective parameter */
  double Seap, Seam, Swap, Swam;
  double Snap, Snam, Ssap, Ssam;
  double Stap, Stam, Sbap, Sbam;
  /* Upwind to downwind gradient */
  double rep, rem, rwp, rwm;
  double rnp, rnm, rsp, rsm;
  double rtp, rtm, rbp, rbm;
  /* Nonuniform grid parameter */
  double Rep, Rem, Rwp, Rwm;
  double Rnp, Rnm, Rsp, Rsm;
  double Rtp, Rtm, Rbp, Rbm;

  /* Cell sizes */
  double DW, DP, DE, DN, DS, DT, DB;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_v = data->coeffs_v.aP;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *aB_v = data->coeffs_v.aB;
  double *aT_v = data->coeffs_v.aT;
  double *b_v = data->coeffs_v.b;

  const double *v = data->v;
  const double *p = data->p;

  const double *v_faces_y = data->v_faces_y;
  const double *diff_factor_v_face_y = data->diff_factor_v_face_y;
  
  const double *Dv_x = data->Dv_x;
  const double *Dv_y = data->Dv_y;
  const double *Dv_z = data->Dv_z;
  const double *Fv_x = data->Fv_x;
  const double *Fv_y = data->Fv_y;
  const double *Fv_z = data->Fv_z;
  
  const double *v_sol_factor = data->v_sol_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  
  const double *v_cut_fn = data->v_cut_fn;
  const double *v_cut_fs = data->v_cut_fs;
  const double *vol_y = data->vol_y;
  const double *alpha_v_x = data->alpha_v_x;
  const double *alpha_v_y = data->alpha_v_y;
  const double *alpha_v_z = data->alpha_v_z;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *v0 = data->v0;

#ifdef OLDINT_OK
  const double *cvdE = data->Delta_xE_v; /**< Distance from node to E neighbour, v nodes. */
  const double *cvdW = data->Delta_xW_v; /**< Distance from node to W neighbour, v nodes. */
  const double *cvdN = data->Delta_yN_v; /**< Distance from node to N neighbour, v nodes. */
  const double *cvdS = data->Delta_yS_v; /**< Distance from node to S neighbour, v nodes. */
  const double *cvdT = data->Delta_zT_v; /**< Distance from node to T neighbour, v nodes. */
  const double *cvdB = data->Delta_zB_v; /**< Distance from node to B neighbour, v nodes. */
#else
  const double *cvdE = data->cvdE;
  const double *cvdW = data->cvdW;
  const double *cvdN = data->cvdN;
  const double *cvdS = data->cvdS;
  const double *cvdT = data->cvdT;
  const double *cvdB = data->cvdB;
#endif

  const double *Dxv = data->Delta_x_v;
  const double *Dyv = data->Delta_y_v;
  const double *Dzv = data->Delta_z_v;
  
  set_D_v_3D(data);
  set_F_v_3D(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_v,				\
	  index_face_x, index_face_z,					\
	  De, Dw, Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb,		\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
	  Fb_positive_OK, Ft_positive_OK,				\
	  dEE, dWW, dE, dW,						\
	  dNN, dSS, dN, dS,						\
	  dTT, dBB, dT, dB,						\
	  rep, rem, rwp, rwm,						\
	  rnp, rnm, rsp, rsm,						\
	  rtp, rtm, rbp, rbm,						\
	  Rep, Rem, Rwp, Rwm,						\
	  Rnp, Rnm, Rsp, Rsm,						\
	  Rtp, Rtm, Rbp, Rbm,						\
	  DP, DE, DW, DN, DS, DT, DB,					\
	  Se, Sw, Sn, Ss, St, Sb,					\
	  Seap, Seam, Swap, Swam,					\
	  Snap, Snam, Ssap, Ssam,					\
	  Stap, Stam, Sbap, Sbam,					\
	  vP, vE, vW, vN, vS, vT, vB,					\
	  vWW, vEE, vSS, vNN, vBB, vTT)					\
  shared(v_dom_matrix,							\
	 aE_v, aW_v, aS_v, aN_v, aT_v, aB_v, aP_v, b_v,			\
	 v, p, v_faces_y,						\
	 Dv_x, Dv_y, Dv_z, Fv_x, Fv_y, Fv_z, mu_turb_at_v_nodes,	\
	 Dxv, Dyv, Dzv, cvdE, cvdW, cvdN, cvdS, cvdT, cvdB, 		\
	 alpha_v_x, alpha_v_y, alpha_v_z)
    
  for(K=1; K<Nz-1; K++){
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	
	index_v = K*Nx*ny + j*Nx + I;

	if(v_dom_matrix[index_v] == 0){

	  i = I-1;
	  J = j+1;
	  k = K-1;
	
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_face_x = K*nx*ny + j*nx + i; // Dv,Fv_x
	  index_face_z = k*Nx*ny + j*Nx + I; // Dv,Fv_z
	
	  /* Distances */
	  dE = cvdE[index_v];
	  dW = cvdW[index_v];
	  dN = cvdN[index_v];
	  dS = cvdS[index_v];
	  dT = cvdT[index_v];
	  dB = cvdB[index_v];
	
	  dEE = dE + cvdE[index_v + 1];
	  dWW = dW + cvdW[index_v - 1];
	  dNN = dN + cvdN[index_v + Nx];
	  dSS = dS + cvdS[index_v - Nx];
	  dTT = dT + cvdT[index_v + Nx*ny];
	  dBB = dB + cvdB[index_v - Nx*ny];

	  /* Velocities */
	  vP = v[index_v];
	  vE = v[index_v + 1];
	  vW = v[index_v - 1];
	  vN = v[index_v + Nx];
	  vS = v[index_v - Nx];
	  vT = v[index_v + Nx*ny];
	  vB = v[index_v - Nx*ny];
	
	  /* Diffusive */
	  De = Dv_x[index_face_x + 1];
	  Dw = Dv_x[index_face_x];
	  Dn = Dv_y[index_];
	  Ds = Dv_y[index_ - Nx];
	  Dt = Dv_z[index_face_z + Nx*ny];
	  Db = Dv_z[index_face_z];
	
	  /* Convective */
	  Fe = Fv_x[index_face_x + 1] * alpha_v_x[index_face_x + 1];
	  Fw = Fv_x[index_face_x] * alpha_v_x[index_face_x];
	  Fn = Fv_y[index_] * alpha_v_y[index_] * alpha_v_y[index_];
	  Fs = Fv_y[index_-Nx] * alpha_v_y[index_-Nx] * alpha_v_y[index_-Nx];
	  Ft = Fv_z[index_face_z + Nx*ny] * alpha_v_z[index_face_z + Nx*ny];
	  Fb = Fv_z[index_face_z] * alpha_v_z[index_face_z];
	
	  Fe_positive_OK = (Fe > 0);
	  Fw_positive_OK = (Fw > 0);
	  Fn_positive_OK = (Fn > 0);
	  Fs_positive_OK = (Fs > 0);
	  Ft_positive_OK = (Ft > 0);
	  Fb_positive_OK = (Fb > 0);

	  if(I == 1){
	    vWW = 2 * vW - vP;
	    dWW = 2*dW;
	  }
	  else if(v_dom_matrix[index_v - 2] != 0){
	    vWW = 2 * vW - vP;
	    dWW = 2*dW;
	  }
	  else{
	    vWW = v[index_v-2];
	  }

	  if(I == Nx-2){
	    vEE = 2 * vE - vP;
	    dEE = 2*dE;
	  }
	  else if(v_dom_matrix[index_v + 2] != 0){
	    vEE = 2 * vE - vP;
	    dEE = 2*dE;
	  }
	  else{
	    vEE = v[index_v+2];
	  }
	
	  if(j == 1){
	    vSS = 2 * vS - vP;
	    dSS = 2*dS;
	  }
	  else if(v_dom_matrix[index_v - 2*Nx] != 0){
	    vSS = 2 * vS - vP;
	    dSS = 2*dS;
	  }
	  else{
	    vSS = v[index_v-2*Nx];
	  }

	  if(j == ny-2){
	    vNN = 2 * vN - vP;
	    dNN = 2*dN;
	  }
	  else if(v_dom_matrix[index_v + 2*Nx] != 0){
	    vNN = 2 * vN - vP;
	    dNN = 2*dN;
	  }
	  else{
	    vNN = v[index_v+2*Nx];
	  }

	  if(K == 1){
	    vBB = 2 * vB - vP;
	    dBB = 2*dB;
	  }
	  else if(v_dom_matrix[index_v - 2*Nx*ny] != 0){
	    vBB = 2 * vB - vP;
	    dBB = 2*dB;
	  }
	  else{
	    vBB = v[index_v - 2*Nx*ny];
	  }
	
	  if(K == Nz-2){
	    vTT = 2 * vT - vP;
	    dTT = 2*dT;
	  }
	  else if(v_dom_matrix[index_v + 2*Nx*ny] != 0){
	    vTT = 2 * vT - vP;
	    dTT = 2*dT;
	  }
	  else{
	    vTT = v[index_v + 2*Nx*ny];
	  }

	  dpA = v_faces_y[index_] * p[index_] - 
	    v_faces_y[index_-Nx] * p[index_-Nx];
	
	  b_v[index_v] = -dpA;

	  aE_v[index_v] = De + MAX(-Fe, 0);
	  aW_v[index_v] = Dw + MAX(+Fw, 0);
	  aN_v[index_v] = Dn + MAX(-Fn, 0);
	  aS_v[index_v] = Ds + MAX(+Fs, 0);
	  aT_v[index_v] = Dt + MAX(-Ft, 0);
	  aB_v[index_v] = Db + MAX(+Fb, 0);
	
	  aP_v[index_v] =
	    aE_v[index_v] + aW_v[index_v] +
	    aN_v[index_v] + aS_v[index_v] +
	    aT_v[index_v] + aB_v[index_v];
	    /*
	    aE_v[index_v] + aW_v[index_v] +
	    aN_v[index_v] + aS_v[index_v] +
	    aT_v[index_v] + aB_v[index_v] +
	    (Fe - Fw) + (Fn - Fs) + (Ft - Fb);
	    */

	  // EW
	  DP = Dxv[index_v];
	  DE = 2*(dE - DP/2);
	  DW = 2*(dW - DP/2);
	
	  // e
	  rep = _r(vP, vW,      dW, dE, vE, vP);
	  rem = _r(vE, vEE, dEE-dE, dE, vP, vE);

	  Rep = (DP + DE) / DP;
	  Rem = (DE + DP) / DE;
	
	  Seap = _psi(rep,Rep) / Rep;
	
	  Seam = _psi(rem,Rem) / Rem;
	  
	  Se = Fe * ((1 - Fe_positive_OK) * Seam - Fe_positive_OK * Seap) * (vE - vP);

	  // Back to upwind scheme if no convective face at e
	  Se = (DE <= 0) ? 0 : Se;

	  // w
	  rwp = _r(vW, vWW, dWW-dW, dW, vP, vW);
	  rwm = _r(vP,  vE,     dE, dW, vW, vP);
	
	  Rwp = (DW + DP) / DW;
	  Rwm = (DP + DW) / DP;
	
	  Swap = _psi(rwp,Rwp) / Rwp;
	
	  Swam = _psi(rwm,Rwm) / Rwm;
	  
	  Sw = Fw * (Fw_positive_OK * Swap - (1 - Fw_positive_OK) * Swam) * (vP - vW);

	  Sw = (DW <= 0) ? 0 : Sw;

	  // NS
	  DP = Dyv[index_v];
	  DN = 2*(dN - DP/2);
	  DS = 2*(dS - DP/2);

	  // n
	  rnp = _r(vP, vS,      dS, dN, vN, vP);
	  rnm = _r(vN, vNN, dNN-dN, dN, vP, vN);

	  Rnp = (DP + DN) / DP;
	  Rnm = (DN + DP) / DN;
	
	  Snap = _psi(rnp,Rnp) / Rnp;
	
	  Snam = _psi(rnm,Rnm) / Rnm;
	  
	  Sn = Fn * ((1 - Fn_positive_OK) * Snam - Fn_positive_OK * Snap) * (vN - vP);

	  Sn = (DN <= 0) ? 0 : Sn;

	  // s
	  rsp = _r(vS, vSS, dSS-dS, dS, vP, vS);
	  rsm = _r(vP,  vN,     dN, dS, vS, vP);
	
	  Rsp = (DS + DP) / DS;
	  Rsm = (DP + DS) / DP;
	
	  Ssap = _psi(rsp,Rsp) / Rsp;
	
	  Ssam = _psi(rsm,Rsm) / Rsm;
	  
	  Ss = Fs * (Fs_positive_OK * Ssap - (1 - Fs_positive_OK) * Ssam) * (vP - vS);

	  Ss = (DS <= 0) ? 0 : Ss;

	  // TB
	  DP = Dzv[index_v];
	  DT = 2*(dT - DP/2);
	  DB = 2*(dB - DP/2);
	
	  // t
	  rtp = _r(vP, vB,      dB, dT, vT, vP);
	  rtm = _r(vT, vTT, dTT-dT, dT, vP, vT);

	  Rtp = (DP + DT) / DP;
	  Rtm = (DT + DP) / DT;
	
	  Stap = _psi(rtp,Rtp) / Rtp;
	
	  Stam = _psi(rtm,Rtm) / Rtm;
	  
	  St = Ft * ((1 - Ft_positive_OK) * Stam - Ft_positive_OK * Stap) * (vT - vP);

	  // Back to upwind scheme if no convective face at e
	  St = (DT <= 0) ? 0 : St;

	  // b
	  rbp = _r(vB, vBB, dBB-dB, dB, vP, vB);
	  rbm = _r(vP,  vT,     dT, dB, vB, vP);
	
	  Rbp = (DB + DP) / DB;
	  Rbm = (DP + DB) / DP;
	
	  Sbap = _psi(rbp,Rbp) / Rbp;
	
	  Sbam = _psi(rbm,Rbm) / Rbm;
	  
	  Sb = Fb * (Fb_positive_OK * Sbap - (1 - Fb_positive_OK) * Sbam) * (vP - vB);

	  Sb = (DB <= 0) ? 0 : Sb;

	  b_v[index_v] += Se + Sw + Sn + Ss + St + Sb; // Sw - Se + Ss - Sn + Sb - St;
	
	}
	else{
	  aP_v[index_v] = 1;
	  aW_v[index_v] = 0;
	  aE_v[index_v] = 0;
	  aS_v[index_v] = 0;
	  aN_v[index_v] = 0;
	  aB_v[index_v] = 0;
	  aT_v[index_v] = 0;
	  b_v[index_v] = 0;
	}
      }
    }
  }
  
  /* Diffusive factor */
  for(z=0; z<diff_v_face_y_count; z++){
    
    index_v = diff_v_face_y_index_v[z];
    index_ = diff_v_face_y_index[z];
    
    J = diff_v_face_y_J[z];
    
    mu_eff = mu + 0.5 * (mu_turb_at_v_nodes[index_v] + mu_turb_at_v_nodes[index_v+Nx]);
    S_diff = mu_eff * diff_factor_v_face_y[z] *
      (v[index_v] * v_cut_fs[index_] + v[index_v+Nx] * v_cut_fn[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((v_dom_matrix[index_v] == 0) && (J > 1)){
      b_v[index_v] -= S_diff;
    }
    if((v_dom_matrix[index_v+Nx] == 0) && (J < Ny-2)){
      b_v[index_v+Nx] += S_diff;
    }
  }
  
  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<v_sol_factor_count; z++){
      
      index_v = v_sol_factor_index_v[z];
      
      if(v_dom_matrix[index_v] == 0){
	
	index_ = v_sol_factor_index[z];
	
	mu_eff = mu + mu_turb_at_v_nodes[index_v];
	
	aP_v[index_v] += mu_eff * v_sol_factor[z];
	b_v[index_v] += v_p_factor[z] *
	  (v_p_factor_S[z] * p[index_-Nx] + (1 - v_p_factor_S[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(v_dom_matrix, b_v, vol_y)
    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	  
	  index_v = K*Nx*ny + j*Nx + I;
	  
	  if(v_dom_matrix[index_v] == 0){
	    b_v[index_v] += periodic_source * vol_y[index_v];
	  }
	  
	}
      }
    }
  }
  
  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, j, K, index_v, aP0)	\
  shared(v_dom_matrix, vol_y, aP_v, b_v, v0)
    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	
	  index_v = K*Nx*ny + j*Nx + I;
	  
	  if(v_dom_matrix[index_v] == 0){
	    
	    aP0 = rho * vol_y[index_v] / dt;
	    aP_v[index_v] += aP0;
	    b_v[index_v] += aP0 * v0[index_v];
	    
	  }
	}
      }
    }
  }
    
  /* Add custom source term */

  if(add_momentum_source_OK){
    add_v_momentum_source_3D(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(v, v_dom_matrix, 0, data->coeffs_v, Nx, ny, Nz, data->alpha_v);
  }

  for(i=0; i<v_slave_count; i++){
    index_v = v_slave_nodes[i];
    aP_v[index_v] = 1;
    aW_v[index_v] = 0;
    aE_v[index_v] = 0;
    aS_v[index_v] = 0;
    aN_v[index_v] = 0;
    aB_v[index_v] = 0;
    aT_v[index_v] = 0;
    b_v[index_v] = 0;
  }

  return;
}

/* COEFFS_FLOW_W_TVD_3D */
/**
  
   Sets:

   Structure coeffs_w: coefficients of w momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 09-05-2021
*/
void coeffs_flow_w_TVD_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_w;
  int index_face_x, index_face_y;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;
  int Fb_positive_OK, Ft_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int w_slave_count = data->w_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int w_sol_factor_count = data->w_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int p_source_orientation = data->p_source_orientation;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_w_face_z_count = data->diff_w_face_z_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *w_dom_matrix = data->w_dom_matrix;
  const int *w_slave_nodes = data->w_slave_nodes;
  const int *w_sol_factor_index_w = data->w_sol_factor_index_w;
  const int *w_sol_factor_index = data->w_sol_factor_index;
  const int *diff_w_face_z_index = data->diff_w_face_z_index;
  const int *diff_w_face_z_index_w = data->diff_w_face_z_index_w;
  const int *diff_w_face_z_K = data->diff_w_face_z_K;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double wP, wE, wW, wN, wS, wT, wB;
  double wWW, wEE, wSS, wNN, wBB, wTT;
  double aP0, S_diff;
  /* Distances to nodes */
  double dEE, dWW, dE, dW;
  double dNN, dSS, dN, dS;
  double dTT, dBB, dT, dB;
  /* Source terms */
  double Se, Sw, Sn, Ss, St, Sb;
  /* Source terms divided by convective parameter */
  double Seap, Seam, Swap, Swam;
  double Snap, Snam, Ssap, Ssam;
  double Stap, Stam, Sbap, Sbam;
  /* Upwind to downwind gradient */
  double rep, rem, rwp, rwm;
  double rnp, rnm, rsp, rsm;
  double rtp, rtm, rbp, rbm;
  /* Nonuniform grid parameter */
  double Rep, Rem, Rwp, Rwm;
  double Rnp, Rnm, Rsp, Rsm;
  double Rtp, Rtm, Rbp, Rbm;

  /* Cell sizes */
  double DW, DP, DE, DN, DS, DT, DB;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_w = data->coeffs_w.aP;
  double *aW_w = data->coeffs_w.aW;
  double *aE_w = data->coeffs_w.aE;
  double *aS_w = data->coeffs_w.aS;
  double *aN_w = data->coeffs_w.aN;
  double *aB_w = data->coeffs_w.aB;
  double *aT_w = data->coeffs_w.aT;
  double *b_w = data->coeffs_w.b;

  const double *w = data->w;
  const double *p = data->p;

  const double *w_faces_z = data->w_faces_z;
  const double *diff_factor_w_face_z = data->diff_factor_w_face_z;
  
  const double *Dw_x = data->Dw_x;
  const double *Dw_y = data->Dw_y;
  const double *Dw_z = data->Dw_z;
  const double *Fw_x = data->Fw_x;
  const double *Fw_y = data->Fw_y;
  const double *Fw_z = data->Fw_z;
  
  const double *w_sol_factor = data->w_sol_factor;
  const double *w_p_factor = data->w_p_factor;
  const double *mu_turb_at_w_nodes = data->mu_turb_at_w_nodes;
  
  const double *w_cut_ft = data->w_cut_ft;
  const double *w_cut_fb = data->w_cut_fb;
  const double *vol_z = data->vol_z;
  const double *alpha_w_x = data->alpha_w_x;
  const double *alpha_w_y = data->alpha_w_y;
  const double *alpha_w_z = data->alpha_w_z;
  const double *w_p_factor_B = data->w_p_factor_B;
  const double *w0 = data->w0;

#ifdef OLDINT_OK
  const double *cwdE = data->Delta_xE_w; /**< Distance from node to E neighbour, w nodes. */
  const double *cwdW = data->Delta_xW_w; /**< Distance from node to W neighbour, w nodes. */
  const double *cwdN = data->Delta_yN_w; /**< Distance from node to N neighbour, w nodes. */
  const double *cwdS = data->Delta_yS_w; /**< Distance from node to S neighbour, w nodes. */
  const double *cwdT = data->Delta_zT_w; /**< Distance from node to T neighbour, w nodes. */
  const double *cwdB = data->Delta_zB_w; /**< Distance from node to B neighbour, w nodes. */
#else
  const double *cwdE = data->cwdE;
  const double *cwdW = data->cwdW;
  const double *cwdN = data->cwdN;
  const double *cwdS = data->cwdS;
  const double *cwdT = data->cwdT;
  const double *cwdB = data->cwdB;
#endif

  const double *Dxw = data->Delta_x_w;
  const double *Dyw = data->Delta_y_w;
  const double *Dzw = data->Delta_z_w;
  
  set_D_w(data);
  set_F_w(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_w,				\
	  index_face_x, index_face_y,					\
	  De, Dw, Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb,		\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
	  Fb_positive_OK, Ft_positive_OK,				\
	  dEE, dWW, dE, dW,						\
	  dNN, dSS, dN, dS,						\
	  dTT, dBB, dT, dB,						\
	  rep, rem, rwp, rwm,						\
	  rnp, rnm, rsp, rsm,						\
	  rtp, rtm, rbp, rbm,						\
	  Rep, Rem, Rwp, Rwm,						\
	  Rnp, Rnm, Rsp, Rsm,						\
	  Rtp, Rtm, Rbp, Rbm,						\
	  DP, DE, DW, DN, DS, DT, DB,					\
	  Se, Sw, Sn, Ss, St, Sb,					\
	  Seap, Seam, Swap, Swam,					\
	  Snap, Snam, Ssap, Ssam,					\
	  Stap, Stam, Sbap, Sbam,					\
	  wP, wE, wW, wN, wS, wT, wB,					\
	  wWW, wEE, wSS, wNN, wBB, wTT)					\
  shared(w_dom_matrix,							\
	 aE_w, aW_w, aS_w, aN_w, aT_w, aB_w, aP_w, b_w,			\
	 w, p, w_faces_z,						\
	 Dw_x, Dw_y, Dw_z, Fw_x, Fw_y, Fw_z, mu_turb_at_w_nodes,	\
	 Dxw, Dyw, Dzw, cwdE, cwdW, cwdN, cwdS, cwdT, cwdB, 		\
	 alpha_w_x, alpha_w_y, alpha_w_z)
    
  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_w = k*Nx*Ny + J*Nx + I;
	
	if(w_dom_matrix[index_w] == 0){
	  
	  i = I-1;
	  j = J-1;
	  K = k+1;
      
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_face_x = k*nx*Ny + J*nx + i; // Dw,Fw_x
	  index_face_y = k*Nx*ny + j*Nx + I; // Dw,Fw_y
	
	  /* Distances */
	  dE = cwdE[index_w];
	  dW = cwdW[index_w];
	  dN = cwdN[index_w];
	  dS = cwdS[index_w];
	  dT = cwdT[index_w];
	  dB = cwdB[index_w];

	  dEE = dE + cwdE[index_w + 1];
	  dWW = dW + cwdW[index_w - 1];
	  dNN = dN + cwdN[index_w + Nx];
	  dSS = dS + cwdS[index_w - Nx];
	  dTT = dT + cwdT[index_w + Nx*Ny];
	  dBB = dB + cwdB[index_w - Nx*Ny];

	  /* Velocities */
	  wP = w[index_w];
	  wE = w[index_w + 1];
	  wW = w[index_w - 1];
	  wN = w[index_w + Nx];
	  wS = w[index_w - Nx];
	  wT = w[index_w + Nx*Ny];
	  wB = w[index_w - Nx*Ny];
	
	  /* Diffusive */
	  De = Dw_x[index_face_x + 1];
	  Dw = Dw_x[index_face_x];
	  Dn = Dw_y[index_face_y + Nx];
	  Ds = Dw_y[index_face_y];
	  Dt = Dw_z[index_];
	  Db = Dw_z[index_ - Nx*Ny];

	  /* Convective */
	  Fe = Fw_x[index_face_x + 1] * alpha_w_x[index_face_x + 1];
	  Fw = Fw_x[index_face_x] * alpha_w_x[index_face_x];
	  Fn = Fw_y[index_face_y + Nx*ny] * alpha_w_y[index_face_y + Nx*ny];
	  Fs = Fw_y[index_face_y] * alpha_w_y[index_face_y];
	  Ft = Fw_z[index_] * alpha_w_z[index_] * alpha_w_z[index_];
	  Fb = Fw_z[index_-Nx*Ny] * alpha_w_z[index_-Nx*Ny] * alpha_w_z[index_-Nx*Ny];
	
	  Fe_positive_OK = (Fe > 0);
	  Fw_positive_OK = (Fw > 0);
	  Fn_positive_OK = (Fn > 0);
	  Fs_positive_OK = (Fs > 0);
	  Ft_positive_OK = (Ft > 0);
	  Fb_positive_OK = (Fb > 0);

	  if(I == 1){
	    wWW = 2 * wW - wP;
	    dWW = 2*dW;
	  }
	  else if(w_dom_matrix[index_w - 2] != 0){
	    wWW = 2 * wW - wP;
	    dWW = 2*dW;
	  }
	  else{
	    wWW = w[index_w-2];
	  }

	  if(I == Nx-2){
	    wEE = 2 * wE - wP;
	    dEE = 2*dE;
	  }
	  else if(w_dom_matrix[index_w + 2] != 0){
	    wEE = 2 * wE - wP;
	    dEE = 2*dE;
	  }
	  else{
	    wEE = w[index_w+2];
	  }
	
	  if(J == 1){
	    wSS = 2 * wS - wP;
	    dSS = 2*dS;
	  }
	  else if(w_dom_matrix[index_w - 2*Nx] != 0){
	    wSS = 2 * wS - wP;
	    dSS = 2*dS;
	  }
	  else{
	    wSS = w[index_w-2*Nx];
	  }

	  if(J == Ny-2){
	    wNN = 2 * wN - wP;
	    dNN = 2*dN;
	  }
	  else if(w_dom_matrix[index_w + 2*Nx] != 0){
	    wNN = 2 * wN - wP;
	    dNN = 2*dN;
	  }
	  else{
	    wNN = w[index_w+2*Nx];
	  }

	  if(k == 1){
	    wBB = 2 * wB - wP;
	    dBB = 2*dB;
	  }
	  else if(w_dom_matrix[index_w - 2*Nx*Ny] != 0){
	    wBB = 2 * wB - wP;
	    dBB = 2*dB;
	  }
	  else{
	    wBB = w[index_w - 2*Nx*Ny];
	  }

	  if(k == nz-2){
	    wTT = 2 * wT - wP;
	    dTT = 2*dT;
	  }
	  else if(w_dom_matrix[index_w + 2*Nx*Ny] != 0){
	    wTT = 2 * wT - wP;
	    dTT = 2*dT;
	  }
	  else{
	    wTT = w[index_w + 2*Nx*Ny];
	  }
	  
	  dpA = w_faces_z[index_] * p[index_] - 
	    w_faces_z[index_-Nx*Ny] * p[index_-Nx*Ny];
	  
	  b_w[index_w] = -dpA;

	  aE_w[index_w] = De + MAX(-Fe, 0);
	  aW_w[index_w] = Dw + MAX(+Fw, 0);
	  aN_w[index_w] = Dn + MAX(-Fn, 0);
	  aS_w[index_w] = Ds + MAX(+Fs, 0);
	  aT_w[index_w] = Dt + MAX(-Ft, 0);
	  aB_w[index_w] = Db + MAX(+Fb, 0);
	  
	  aP_w[index_w] =
	    aE_w[index_w] + aW_w[index_w] +
	    aN_w[index_w] + aS_w[index_w] +
	    aT_w[index_w] + aB_w[index_w];
	    /*
	    aE_w[index_w] + aW_w[index_w] +
	    aN_w[index_w] + aS_w[index_w] +
	    aT_w[index_w] + aB_w[index_w] +
	    (Fe - Fw) + (Fn - Fs) + (Ft - Fb);
	    */

	  // EW
	  DP = Dxw[index_w];
	  DE = 2*(dE - DP/2);
	  DW = 2*(dW - DP/2);
	
	  // e
	  rep = _r(wP, wW,      dW, dE, wE, wP);
	  rem = _r(wE, wEE, dEE-dE, dE, wP, wE);

	  Rep = (DP + DE) / DP;
	  Rem = (DE + DP) / DE;
	
	  Seap = _psi(rep,Rep) / Rep;
	
	  Seam = _psi(rem,Rem) / Rem;
	  
	  Se = Fe * ((1 - Fe_positive_OK) * Seam - Fe_positive_OK * Seap) * (wE - wP);

	  // Back to upwind scheme if no convective face at e
	  Se = (DE <= 0) ? 0 : Se;

	  // w
	  rwp = _r(wW, wWW, dWW-dW, dW, wP, wW);
	  rwm = _r(wP,  wE,     dE, dW, wW, wP);
	
	  Rwp = (DW + DP) / DW;
	  Rwm = (DP + DW) / DP;
	
	  Swap = _psi(rwp,Rwp) / Rwp;
	
	  Swam = _psi(rwm,Rwm) / Rwm;
	  
	  Sw = Fw * (Fw_positive_OK * Swap - (1 - Fw_positive_OK) * Swam) * (wP - wW);

	  Sw = (DW <= 0) ? 0 : Sw;

	  // NS
	  DP = Dyw[index_w];
	  DN = 2*(dN - DP/2);
	  DS = 2*(dS - DP/2);

	  // n
	  rnp = _r(wP, wS,      dS, dN, wN, wP);
	  rnm = _r(wN, wNN, dNN-dN, dN, wP, wN);

	  Rnp = (DP + DN) / DP;
	  Rnm = (DN + DP) / DN;
	
	  Snap = _psi(rnp,Rnp) / Rnp;
	
	  Snam = _psi(rnm,Rnm) / Rnm;
	  
	  Sn = Fn * ((1 - Fn_positive_OK) * Snam - Fn_positive_OK * Snap) * (wN - wP);

	  Sn = (DN <= 0) ? 0 : Sn;

	  // s
	  rsp = _r(wS, wSS, dSS-dS, dS, wP, wS);
	  rsm = _r(wP,  wN,     dN, dS, wS, wP);
	
	  Rsp = (DS + DP) / DS;
	  Rsm = (DP + DS) / DP;
	
	  Ssap = _psi(rsp,Rsp) / Rsp;
	
	  Ssam = _psi(rsm,Rsm) / Rsm;
	  
	  Ss = Fs * (Fs_positive_OK * Ssap - (1 - Fs_positive_OK) * Ssam) * (wP - wS);

	  Ss = (DS <= 0) ? 0 : Ss;

	  // TB
	  DP = Dzw[index_w];
	  DT = 2*(dT - DP/2);
	  DB = 2*(dB - DP/2);
	
	  // t
	  rtp = _r(wP, wB,      dB, dT, wT, wP);
	  rtm = _r(wT, wTT, dTT-dT, dT, wP, wT);

	  Rtp = (DP + DT) / DP;
	  Rtm = (DT + DP) / DT;
	
	  Stap = _psi(rtp,Rtp) / Rtp;
	
	  Stam = _psi(rtm,Rtm) / Rtm;
	  
	  St = Ft * ((1 - Ft_positive_OK) * Stam - Ft_positive_OK * Stap) * (wT - wP);

	  // Back to upwind scheme if no convective face at e
	  St = (DT <= 0) ? 0 : St;

	  // b
	  rbp = _r(wB, wBB, dBB-dB, dB, wP, wB);
	  rbm = _r(wP,  wT,     dT, dB, wB, wP);
	
	  Rbp = (DB + DP) / DB;
	  Rbm = (DP + DB) / DP;
	
	  Sbap = _psi(rbp,Rbp) / Rbp;
	
	  Sbam = _psi(rbm,Rbm) / Rbm;
	  
	  Sb = Fb * (Fb_positive_OK * Sbap - (1 - Fb_positive_OK) * Sbam) * (wP - wB);

	  Sb = (DB <= 0) ? 0 : Sb;

	  b_w[index_w] += Se + Sw + Sn + Ss + St + Sb; // Sw - Se + Ss - Sn + Sb - St;
	
	}
	else{
	  aP_w[index_w] = 1;
	  aW_w[index_w] = 0;
	  aE_w[index_w] = 0;
	  aS_w[index_w] = 0;
	  aN_w[index_w] = 0;
	  aB_w[index_w] = 0;
	  aT_w[index_w] = 0;
	  b_w[index_w] = 0;
	}
      }
    }
  }
  
  /* Diffusive factor */
  for(z=0; z<diff_w_face_z_count; z++){
    
    index_w = diff_w_face_z_index_w[z];
    index_ = diff_w_face_z_index[z];
    
    K = diff_w_face_z_K[z];
    
    mu_eff = mu + 0.5 * (mu_turb_at_w_nodes[index_w] + mu_turb_at_w_nodes[index_w + Nx*Ny]);
    S_diff = mu_eff * diff_factor_w_face_z[z] *
      (w[index_w] * w_cut_fb[index_] + w[index_w+Nx*Ny] * w_cut_ft[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((w_dom_matrix[index_w] == 0) && (K > 1)){
      b_w[index_w] -= S_diff;
    }
    if((w_dom_matrix[index_w + Nx*Ny] == 0) && (K < Nz-2)){
      b_w[index_w + Nx*Ny] += S_diff;
    }
  }
  
  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<w_sol_factor_count; z++){
      
      index_w = w_sol_factor_index_w[z];
      
      if(w_dom_matrix[index_w] == 0){
	
	index_ = w_sol_factor_index[z];
	
	mu_eff = mu + mu_turb_at_w_nodes[index_w];
	
	aP_w[index_w] += mu_eff * w_sol_factor[z];
	b_w[index_w] += w_p_factor[z] *
	  (w_p_factor_B[z] * p[index_-Nx*Ny] + (1 - w_p_factor_B[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(w_dom_matrix, b_w, vol_z)
    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_w = k*Nx*Ny + J*Nx + I;
	
	  if(w_dom_matrix[index_w] == 0){
	    b_w[index_w] += periodic_source * vol_z[index_w];
	  }
	
	}
      }
    }
  }
  
  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, J, k, index_w, aP0)	\
  shared(w_dom_matrix, vol_z, aP_w, b_w, w0)
    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_w = k*Nx*Ny + J*Nx + I;
	
	  if(w_dom_matrix[index_w] == 0){
	  
	    aP0 = rho * vol_z[index_w] / dt;
	    aP_w[index_w] += aP0;
	    b_w[index_w] += aP0 * w0[index_w];
	  
	  }
	}
      }
    }
  }
    
  /* Add custom source term */

  if(add_momentum_source_OK){
    add_w_momentum_source_3D(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(w, w_dom_matrix, 0, data->coeffs_w, Nx, Ny, nz, data->alpha_w);
  }

  for(i=0; i<w_slave_count; i++){
    index_w = w_slave_nodes[i];
    aP_w[index_w] = 1;
    aW_w[index_w] = 0;
    aE_w[index_w] = 0;
    aS_w[index_w] = 0;
    aN_w[index_w] = 0;
    aB_w[index_w] = 0;
    aT_w[index_w] = 0;
    b_w[index_w] = 0;
  }
  
  return;
}
