#include "include/some_defs.h"

/* Ec 14 Sato, considering sqrt(k)_b = 0 */
#define der_sqrt_k_b(eps_, phi_c , phi_f, Delta) (( (eps_) * (eps_) * (phi_f) - ((eps_)+1) * ((eps_)+1) * (phi_c) )/( (eps_) * ((eps_)+1) * (Delta)))

/* Ec 25 Sato */
#define der_phi_center(eps_, dphi_b, phi1, phi0, Delta) ((1/(2 * (eps_) + 1)) * ((dphi_b) + 2 * (eps_) * ( (phi1) - (phi0)) / (Delta)))

/* SET_D_K */
/*****************************************************************************/
/**

   Sets:

   Array (double, nxNy): Dk_x: Diffusive parameter for k equation at x_faces.

   Array (double, Nxny): Dk_y: Diffusive parameter for k equation at y_faces.

   @param data

   @return void

   MODIFIED: 19-03-2020
*/
void set_D_k(Data_Mem *data){

  int i, j, I, J;
  int index_u, index_v, index_;

  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  double mu_turb_face;
  double mu_eff_0, mu_eff_1;

  const double C_sigma_k = data->C_sigma_k;

  const double mu = data->mu;

  double *Dk_y = data->Dk_y;
  double *Dk_x = data->Dk_x;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *mu_turb = data->mu_turb;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *fw = data->fw;
  const double *fs = data->fs;


  /* Dk_x */
  
#pragma omp parallel for default(none)					\
  private(I, J, i, index_u, index_, mu_eff_0, mu_eff_1, mu_turb_face)	\
  shared(mu_turb, Delta_xW, Delta_y, Dk_x, fw)
  
  for(J=1; J<Ny-1; J++){
    for(i=0; i<nx; i++){
      I = i + 1;
      index_u = J*nx + i;
      index_ = J*Nx + I;
      mu_eff_0 = mu + mu_turb[index_-1] / C_sigma_k;
      mu_eff_1 = mu + mu_turb[index_] / C_sigma_k;
      mu_turb_face = (mu_eff_0 * mu_eff_1) /
	(mu_eff_1 * fw[index_] + mu_eff_0 * (1 - fw[index_]));
      Dk_x[index_u] = Delta_y[index_] * mu_turb_face /
	Delta_xW[index_];
    }
  }

  /* Dk_y */
#pragma omp parallel for default(none)					\
  private(I, J, j, index_v, index_, mu_eff_0, mu_eff_1, mu_turb_face)	\
  shared(mu_turb, Delta_yS, Delta_x, Dk_y, fs)
  
  for(j=0; j<ny; j++){
    for(I=1; I<Nx-1; I++){
      J = j + 1;
      index_v = j*Nx + I;
      index_ = J*Nx + I;
      mu_eff_0 = mu + mu_turb[index_-Nx] / C_sigma_k;
      mu_eff_1 = mu + mu_turb[index_] /C_sigma_k;
      mu_turb_face = (mu_eff_0 * mu_eff_1) /
	( mu_eff_1 * fs[index_] + mu_eff_0 * (1 - fs[index_]));
      Dk_y[index_v] = Delta_x[index_] * mu_turb_face /
	Delta_yS[index_];
    }
  }
  
  return;
}

/* SET_D_EPS */
/*****************************************************************************/
/**

   Sets:

   Array (double, nxNy): Deps_x: Diffusive parameter for epsilon equation at x_faces.

   Array (double, Nxny): Deps_y: Diffusive parameter for epsilon equation at y_faces.

   @param data

   @return void

   MODIFIED: 19-03-2020
*/
void set_D_eps(Data_Mem *data){

  int i, j, I, J;
  int index_u, index_v, index_;

  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  double mu_eff_0, mu_eff_1;
  double mu_turb_face;

  const double C_sigma_eps = data->C_sigma_eps;

  const double mu = data->mu;

  double *Deps_x = data->Deps_x;
  double *Deps_y = data->Deps_y;

  const double *mu_turb = data->mu_turb;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *fw = data->fw;
  const double *fs = data->fs;

  /* Deps_x */
#pragma omp parallel for default(none) private(I, J, i, index_u, index_, mu_eff_0, mu_eff_1, mu_turb_face) \
  shared(mu_turb, Delta_xW, Delta_y, Deps_x, fw)

  for(J=1; J<Ny-1; J++){
    for(i=0; i<nx; i++){
      I = i + 1;
      index_u = J*nx + i;
      index_ = J*Nx + I;
      mu_eff_0 = mu + mu_turb[index_-1] / C_sigma_eps;
      mu_eff_1 = mu + mu_turb[index_] / C_sigma_eps;
      mu_turb_face = (mu_eff_0 * mu_eff_1) /
	(mu_eff_1 * fw[index_] + mu_eff_0 * (1 - fw[index_]));
      Deps_x[index_u] = Delta_y[index_] * mu_turb_face /
	Delta_xW[index_];
    }
  }
  
  /* Deps_y */
#pragma omp parallel for default(none) private(I, J, j, index_v, index_, mu_eff_0, mu_eff_1, mu_turb_face) \
  shared(mu_turb, Delta_yS, Delta_x, Deps_y, fs)
  
  for(j=0; j<ny; j++){
    for(I=1; I<Nx-1; I++){
      J = j + 1;
      index_v = j*Nx + I;
      index_ = J*Nx + I;
      mu_eff_0 = mu + mu_turb[index_-Nx] / C_sigma_eps;
      mu_eff_1 = mu + mu_turb[index_] / C_sigma_eps;
      mu_turb_face = (mu_eff_0 * mu_eff_1) /
	(mu_eff_1 * fs[index_] + mu_eff_0 * (1 - fs[index_]));
      Deps_y[index_v] = Delta_x[index_] * mu_turb_face /
	Delta_yS[index_];
    }
  }

  return;
}

/* SET_F_K */
/**

   Sets:

   Array (double, nxNy): Fk_x: Convective parameter for k equation at x_faces.

   Array (double, Nxny): Fk_y: Convective parameter for k equation at y_faces.

   @param data

   @return void

   MODIFIED: 19-03-2020
*/
void set_F_k(Data_Mem *data){

  int i, j, I, J;
  int index_u, index_v, index_;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const double rho = data->rho_v[0];

  double *Fk_x = data->Fk_x;
  double *Fk_y = data->Fk_y;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

  /* Fk_x */
#pragma omp parallel for default(none) private(i, J, index_u, index_) shared(u, Delta_y, Fk_x)
  
  for(J=1; J<Ny-1; J++){
    for(i=0; i<nx; i++){
      index_u = J*nx + i;
      index_ = J*Nx + i;
      Fk_x[index_u] = u[index_u] * Delta_y[index_] * rho;
    }
  }

  /* Fk_y */
#pragma omp parallel for default(none) private(I, j, index_v, index_) shared(v, Delta_x, Fk_y)
  
  for(j=0; j<ny; j++){
    for(I=1; I<Nx-1; I++){
      index_v = j*Nx + I;
      index_ = j*Nx + I;
      Fk_y[index_v] = v[index_v] * Delta_x[index_] * rho;
    }
  }
  
  return;
}

/* SET_F_EPS */
/**

   Sets:

   Array (double, nxNy): Feps_x: Convective parameter for epsislon equation at x_faces.

   Array (double, Nxny): Feps_y: Convective parameter for epsilon equation at y_faces.

   @param data

   @return void

   MODIFIED: 19-03-2020
*/
void set_F_eps(Data_Mem *data){

  int i, j, I, J;
  int index_u, index_v, index_;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const double rho = data->rho_v[0];

  double *Feps_x = data->Feps_x;
  double *Feps_y = data->Feps_y;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

  /* Feps_x */
#pragma omp parallel for default(none) private(i, J, index_u, index_) shared(u, Delta_y, Feps_x)
  
  for(J=1; J<Ny-1; J++){
    for(i=0; i<nx; i++){
      index_u = J*nx + i;
      index_ = J*Nx + i;
      Feps_x[index_u] = u[index_u] * Delta_y[index_] * rho;
    }
  }

  /* Feps_y */
#pragma omp parallel for default(none) private(I, j, index_v, index_) shared(v, Delta_x, Feps_y)
  
  for(j=0; j<ny; j++){
    for(I=1; I<Nx-1; I++){
      index_v = j*Nx + I;
      index_ = j*Nx + I;
      Feps_y[index_v] = v[index_v] * Delta_x[index_] * rho;
    }
  }

  return;
}

/* SET_COEFFS_K */
/*****************************************************************************/
/**

   Sets:

   Strucuture coeffs_k: coefficients of k equation for internal nodes.

   @param data

   @return void
 */
void set_coeffs_k(Data_Mem *data){

  int i, j, I, J;
  int index_, index_u, index_v;
  int count;

  int E_coeff, W_coeff, N_coeff, S_coeff;

  const int nx = data->nx;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int turb_boundary_cell_count =
    data->turb_boundary_cell_count;
  const int flow_steady_state_OK = data->flow_steady_state_OK;

  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_bnd_excl_indexs = data->turb_bnd_excl_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;

  double Dw, De, Dn, Ds;
  double aP0, Da, m;

  const double rho = data->rho_v[0];
  const double alpha_SC_k = data->alpha_SC_k;
  const double dt = data->dt[3];

  double *k_aP = data->coeffs_k.aP;
  double *k_aW = data->coeffs_k.aW;
  double *k_aE = data->coeffs_k.aE;
  double *k_aS = data->coeffs_k.aS;
  double *k_aN = data->coeffs_k.aN;
  double *k_b = data->coeffs_k.b;
  double *SC_vol_k_turb = data->SC_vol_k_turb;
  double *SC_vol_k_turb_lag = data->SC_vol_k_turb_lag;
  
  const double *P_k = data->P_k;
  const double *Dk_x = data->Dk_x;
  const double *Dk_y = data->Dk_y;
  const double *Fk_x = data->Fk_x;
  const double *Fk_y = data->Fk_y;
  const double *gamma = data->gamma;
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *vol = data->vol_uncut;
  const double *k_turb0 = data->k_turb0;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;
  
  /* Computing convective and diffusive contributions */
  set_D_k(data);
  set_F_k(data);

  /* Computing source terms */
  compute_k_SC_at_interface(data);

  /* Relax source term */
  sum_relax_array(SC_vol_k_turb, SC_vol_k_turb_lag, SC_vol_k_turb,
		  alpha_SC_k, turb_boundary_cell_count, 1, 1);
  copy_array(SC_vol_k_turb, SC_vol_k_turb_lag, turb_boundary_cell_count, 1, 1);

  /* k */

#pragma omp parallel for default(none) private(I, J, i, j, index_, index_u, index_v, Dw, De, Ds, Dn) \
  shared(dom_matrix, k_aW, k_aE, k_aS, k_aN, k_aP, k_b, Dk_x, Dk_y, Fk_x, Fk_y, P_k, gamma, \
	 vol)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;

      if(dom_matrix[index_] == 0){

	i = I - 1;
	j = J - 1;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	Dw = Dk_x[index_u];
	De = Dk_x[index_u+1];
	Ds = Dk_y[index_v];
	Dn = Dk_y[index_v+Nx];
	
	k_aW[index_] = Dw + MAX(0, Fk_x[index_u]);
	k_aE[index_] = De + MAX(0, -Fk_x[index_u+1]);
	k_aS[index_] = Ds + MAX(0, Fk_y[index_v]);
	k_aN[index_] = Dn + MAX(0, -Fk_y[index_v+Nx]);
	k_aP[index_] = Dw + De + Ds + Dn +
	  MAX(0, -Fk_x[index_u]) + MAX(0, Fk_x[index_u+1]) +
	  MAX(0, -Fk_y[index_v]) + MAX(0, Fk_y[index_v+Nx]) +
	  rho * gamma[index_] * vol[index_];
	k_b[index_] = P_k[index_] * vol[index_];

      }
      else{
	k_aP[index_] = 1;
	k_b[index_] = 0;
      }
    }
  } 

  for(count=0; count<turb_boundary_cell_count; count++){
    index_ = turb_boundary_indexs[count];
    if(turb_bnd_excl_indexs[count]){
      k_aW[index_] = 0;
      k_aE[index_] = 0;
      k_aS[index_] = 0;
      k_aN[index_] = 0;
      k_aP[index_] = 1;
      k_b[index_] = 0;
    }
    else{
      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      i = I - 1;
      j = J - 1;
      index_u = J*nx + i;
      index_v = j*Nx + I;
      Dw = Dk_x[index_u];
      De = Dk_x[index_u+1];
      Ds = Dk_y[index_v];
      Dn = Dk_y[index_v+Nx];
      
      E_coeff = 1;
      W_coeff = 1;
      N_coeff = 1;
      S_coeff = 1;

      if(!E_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
	m = Delta_x[index_] / Da;
	De = 0;
	Dw = Dw * m;
	E_coeff = 0;
      }

      if(!W_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
	m = Delta_x[index_] / Da;
	Dw = 0;       
	De = De * m;
	W_coeff = 0;
      }

      if(!N_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
	m = Delta_y[index_] / Da;
	Dn = 0;
	Ds = Ds * m;
	N_coeff = 0;
      }

      if(!S_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
	m = Delta_y[index_] / Da;
	Ds = 0;
	Dn = Dn * m;
	S_coeff = 0;
      }
	
      k_aW[index_] = Dw + MAX(0, Fk_x[index_u]) * W_coeff;
      k_aE[index_] = De + MAX(0, -Fk_x[index_u+1]) * E_coeff;
      k_aS[index_] = Ds + MAX(0, Fk_y[index_v]) * S_coeff;
      k_aN[index_] = Dn + MAX(0, -Fk_y[index_v+Nx]) * N_coeff;
      k_aP[index_] = Dw + De + Ds + Dn +
	MAX(0, -Fk_x[index_u]) * W_coeff + MAX(0, Fk_x[index_u+1]) * E_coeff +
	MAX(0, -Fk_y[index_v]) * S_coeff + MAX(0, Fk_y[index_v+Nx]) * N_coeff +
	rho * gamma[index_] * vol[index_];
      k_b[index_] = P_k[index_] * vol[index_] + SC_vol_k_turb[count];
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, J, index_, aP0)	\
  shared(dom_matrix, k_aP, k_b, k_turb0, vol)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	if(dom_matrix[index_] == 0){
	  aP0 = rho * vol[index_] / dt;
	  k_aP[index_] += aP0;
	  k_b[index_] += aP0 * k_turb0[index_];
	}
      }
    }
  }


  relax_coeffs(data->k_turb, dom_matrix, 0, data->coeffs_k, Nx, Ny, data->alpha_k);

  return;
}

/* SET_COEFFS_EPS */
/*****************************************************************************/
/**

   Sets:

   Strucuture coeffs_eps: coefficients of epsilon equation for internal nodes.

   @param data

   @return void
 */
void set_coeffs_eps(Data_Mem *data){

  int i, j, I, J;
  int index_, index_u, index_v;
  int count;

  int E_coeff, W_coeff, N_coeff, S_coeff;

  const int nx = data->nx;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int turb_model = data->turb_model;
  const int turb_boundary_cell_count =
    data->turb_boundary_cell_count;
  const int flow_steady_state_OK = data->flow_steady_state_OK;

  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_bnd_excl_indexs = data->turb_bnd_excl_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;

  double E_vol;
  double Dw, De, Ds, Dn;
  double aP0, Da, m;

  const double C_1 = data->C_1;
  const double C_2 = data->C_2;
  const double rho = data->rho_v[0];
  const double alpha_SC_eps = data->alpha_SC_eps;
  const double dt = data->dt[3];

  double *eps_aP = data->coeffs_eps.aP;
  double *eps_aW = data->coeffs_eps.aW;
  double *eps_aE = data->coeffs_eps.aE;
  double *eps_aN = data->coeffs_eps.aN;
  double *eps_aS = data->coeffs_eps.aS;
  double *eps_b = data->coeffs_eps.b;
  double *SC_vol_eps_turb = data->SC_vol_eps_turb;
  double *SC_vol_eps_turb_lag = data->SC_vol_eps_turb_lag;

  const double *P_k = data->P_k;
  const double *Deps_x = data->Deps_x;
  const double *Deps_y = data->Deps_y;
  const double *Feps_x = data->Feps_x;
  const double *Feps_y = data->Feps_y;
  const double *gamma = data->gamma;
  const double *f_2 = data->f_2;
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *S_CG_eps = data->S_CG_eps;
  const double *eps_bnd_x = data->eps_bnd_x;
  const double *eps_bnd_y = data->eps_bnd_y;
  const double *vol = data->vol_uncut;
  const double *eps_turb0 = data->eps_turb0;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;  
  
  /* Computing convective and diffusive contributions */
  set_D_eps(data);
  set_F_eps(data);

  /* Computing source terms */
  compute_eps_SC_at_interface(data);

  /* Relax source term */
  sum_relax_array(SC_vol_eps_turb, SC_vol_eps_turb_lag, SC_vol_eps_turb,
		  alpha_SC_eps, turb_boundary_cell_count, 1, 1);
  copy_array(SC_vol_eps_turb, SC_vol_eps_turb_lag, turb_boundary_cell_count, 1, 1);

  /* eps */

#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_u, index_v, Dw, De, Ds, Dn, E_vol)	\
  shared(dom_matrix, eps_aW, eps_aE, eps_aS, eps_aN, eps_aP, eps_b,	\
	 Deps_x, Deps_y, Feps_x, Feps_y, P_k, f_2, gamma,		\
	 S_CG_eps, vol)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;

      if(dom_matrix[index_] == 0){

	i = I - 1;
	j = J - 1;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	Dw = Deps_x[index_u];
	De = Deps_x[index_u+1];
	Ds = Deps_y[index_v];
	Dn = Deps_y[index_v+Nx];

	if(turb_model == 2){
	  E_vol = S_CG_eps[index_] * vol[index_];	  
	}
	else{
	  E_vol = 0;
	}

	eps_aW[index_] = Dw + MAX(0, Feps_x[index_u]);
	eps_aE[index_] = De + MAX(0, -Feps_x[index_u+1]);
	eps_aS[index_] = Ds + MAX(0, Feps_y[index_v]);
	eps_aN[index_] = Dn + MAX(0, -Feps_y[index_v+Nx]);
	eps_aP[index_] = Dw + De + Ds + Dn + 
	  MAX(0, -Feps_x[index_u]) + MAX(0, Feps_x[index_u+1]) + 
	  MAX(0, -Feps_y[index_v]) + MAX(0, Feps_y[index_v+Nx]) + 
	  C_2 * f_2[index_] * rho * gamma[index_] * vol[index_];
	eps_b[index_] = C_1 * P_k[index_] * gamma[index_] * vol[index_] + E_vol;

      }
      else{
	eps_aP[index_] = 1;
	eps_b[index_] = 0;
      }
    }
  }

  /* Treatment for cut-cells fluid boundary cells */
  for(count=0; count<turb_boundary_cell_count; count++){
    
    index_ = turb_boundary_indexs[count];

    if(turb_bnd_excl_indexs[count]){
      eps_aW[index_] = 0;
      eps_aE[index_] = 0;
      eps_aS[index_] = 0;
      eps_aN[index_] = 0;
      eps_aP[index_] = 1;
      if((!W_is_fluid_OK[index_]) || (!E_is_fluid_OK[index_])){
	if((!S_is_fluid_OK[index_]) || (!N_is_fluid_OK[index_])){
	  eps_b[index_] = 0.5 * (eps_bnd_x[count] + eps_bnd_y[count]);
	}
	else{
	  eps_b[index_] = eps_bnd_x[count];
	}
      }
      else{
	eps_b[index_] = eps_bnd_y[count];
      }

    }
    else{
      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      i = I - 1;
      j = J - 1;
      index_u = J*nx + i;
      index_v = j*Nx + I;
      Dw = Deps_x[index_u];
      De = Deps_x[index_u+1];
      Ds = Deps_y[index_v];
      Dn = Deps_y[index_v+Nx];
      E_coeff = 1;
      W_coeff = 1;
      N_coeff = 1;
      S_coeff = 1;

      if(!E_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
	m = Delta_x[index_] / Da;	
	De = 0;
	Dw = Dw * m;
	E_coeff = 0;
      }
      if(!W_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
	m = Delta_x[index_] / Da;	
	Dw = 0;
	De = De * m;
	W_coeff = 0;
      }
      if(!N_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
	m = Delta_y[index_] / Da;	
	Dn = 0;
	Ds = Ds * m;
	N_coeff = 0;
      }
      if(!S_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
	m = Delta_y[index_] / Da;	
	Ds = 0;
	Dn = Dn * m;
	S_coeff = 0;
      }

      if(turb_model == 2){
	E_vol = S_CG_eps[index_] * vol[index_];	  
      }
      else{
	E_vol = 0;
      }

      eps_aW[index_] = Dw + MAX(0, Feps_x[index_u]) * W_coeff;
      eps_aE[index_] = De + MAX(0, -Feps_x[index_u+1]) * E_coeff;
      eps_aS[index_] = Ds + MAX(0, Feps_y[index_v]) * S_coeff;
      eps_aN[index_] = Dn + MAX(0, -Feps_y[index_v+Nx]) * N_coeff;
      eps_aP[index_] = Dw + De + Ds + Dn + 
	MAX(0, -Feps_x[index_u]) * W_coeff + MAX(0, Feps_x[index_u+1]) * E_coeff + 
	MAX(0, -Feps_y[index_v]) * S_coeff + MAX(0, Feps_y[index_v+Nx]) *N_coeff + 
	C_2 * f_2[index_] * rho * gamma[index_] * vol[index_];
      eps_b[index_] = C_1 * P_k[index_] * gamma[index_] * vol[index_] + E_vol + SC_vol_eps_turb[count];
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, J, index_, aP0)	\
  shared(dom_matrix, vol, eps_aP, eps_b, eps_turb0)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	if(dom_matrix[index_] == 0){
	  aP0 = rho * vol[index_] / dt;
	  eps_aP[index_] += aP0;
	  eps_b[index_] += aP0 * eps_turb0[index_];
	}
      }
    }
  }
  
  relax_coeffs(data->eps_turb, dom_matrix, 0, data->coeffs_eps, Nx, Ny, data->alpha_eps);
  
  return;
}

/* SET_BC_TURB_K */
/*****************************************************************************/
/**
  
  Sets:

  Struct coeffs_k: coefficients for boundary values, according to specified 
  boundary conditions

  @param data

  @return void
  
  MODIFIED: 26-03-2019
*/
void set_bc_turb_k(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  
  const int *dom_matrix = data->domain_matrix;

  double *k_aP = data->coeffs_k.aP;
  double *k_aW = data->coeffs_k.aW;
  double *k_aE = data->coeffs_k.aE;
  double *k_aS = data->coeffs_k.aS;
  double *k_aN = data->coeffs_k.aN;
  double *k_b = data->coeffs_k.b;

  const double *k_turb = data->k_turb;
  const double *k_W_non_uni = data->k_W_non_uni;
  const double *k_E_non_uni = data->k_W_non_uni;
  const double *k_S_non_uni = data->k_S_non_uni;
  const double *k_N_non_uni = data->k_N_non_uni;
  const double *u = data->u;
  const double *v = data->v;
  const double *fn = data->fn;
  const double *fe = data->fe;

  const b_cond_flow *bc_w = &(data->bc_flow_west);
  const b_cond_flow *bc_e = &(data->bc_flow_east);
  const b_cond_flow *bc_s = &(data->bc_flow_south);
  const b_cond_flow *bc_n = &(data->bc_flow_north);

#pragma omp parallel sections default(none) private(I, J, i, j, index_, index_u, index_v) \
  shared(dom_matrix, k_W_non_uni, k_E_non_uni, k_S_non_uni, k_N_non_uni, k_turb, k_aP, k_aW, \
	 k_aE, k_aS, k_aN, k_b, u, v, fn, fe, \
	 bc_w, bc_e, bc_s, bc_n)
  {

#pragma omp section 
    {
      /* South boundary */
      J = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_s->type == 0) || (bc_s->type == 2)){
	
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){   
	    k_aN[index_] = 1;
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_s->type == 1){
	if(bc_s->parabolize_profile_OK){
	  
	  for(I=1; I<Nx-1; I++){

	    index_ = J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = k_S_non_uni[I];

	  }
	}
	else{
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = J*Nx + I;
	    k_aP[index_] = 1;
	    
	    if(dom_matrix[index_] == 0){
	      k_b[index_] = bc_s->k_value;
	    }
	    else{
	      k_b[index_] = 0;    
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_s->type == 3){
	
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;
	  k_b[index_] = 0;

	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_s->type == 4){

	j = 0;
	
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  k_aP[index_] = 1;
	  
	  if(dom_matrix[index_] == 0){
	    
	    index_v = j*Nx + I;
	    
	    if(v[index_v] > 0){
	      k_b[index_] = k_S_non_uni[I];
	      k_aN[index_] = 0;
	    }
	    else{
	      k_b[index_] = 0;
	      k_aN[index_] = 1;
	    }
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(bc_s->type == 5){
	
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    k_b[index_] = k_turb[(Ny-3)*Nx + I] * fn[(Ny-3)*Nx + I] + 
	      k_turb[(Ny-2)*Nx + I] * (1 - fn[(Ny-3)*Nx + I]);
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }

    } /* South section */

#pragma omp section 
    {
      /* North boundary */
      J = Ny - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_n->type == 0) || (bc_n->type == 2)){
	
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    k_aS[index_] = 1;
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }
      /*Type = 1, Fixed value */
      else if(bc_n->type == 1){
	if(bc_n->parabolize_profile_OK){
	  
	  for(I=1; I<Nx-1; I++){

	    index_ = J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = k_N_non_uni[I];

	  }
	}
	else{
	  for(I=1; I<Nx-1; I++){

	    index_ = J*Nx + I;
	    k_aP[index_] = 1;
	    
	    if(dom_matrix[index_] == 0){
	      k_b[index_] = bc_n->k_value;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      /* Type = 3, Wall */
      else if(bc_n->type == 3){
	
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;
	  k_b[index_] = 0;
	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_n->type == 4){

	j = ny - 1;
	
	for(I=1; I<Nx-1; I++){
 
	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
 
	    index_v = j*Nx + I;
 
	    if(v[index_v] < 0){
	      k_b[index_] = k_N_non_uni[I];
	      k_aS[index_] = 0;
	    }
	    else{
	      k_b[index_] = 0;
	      k_aS[index_] = 1;
	    }
	  }
	  else{
	    k_b[index_] = 0;	    
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(bc_n->type == 5){
	
	for(I=1; I<Nx-1; I++){
	  
	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    k_b[index_] = k_turb[Nx + I] * fn[Nx + I] + 
	      k_turb[2*Nx + I] * (1 - fn[Nx + I]);
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }

    } /* North section */

#pragma omp section
    {
      /* West boundary */
      I = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_w->type == 0) || (bc_w->type == 2)){
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    k_aE[index_] = 1;
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }
      /* Type = 1, Fixed value */
      else if(bc_w->type == 1){
	if(bc_w->parabolize_profile_OK){
	  
	  for(J=1; J<Ny-1; J++){

	    index_ = J*Nx + I;
	    k_aP[index_] = 1;	    
	    k_b[index_] = k_W_non_uni[J];
	  }
	}
	else{
	  for(J=1; J<Ny-1; J++){

	    index_ = J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_b[index_] = bc_w->k_value;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      /* Type = 3, Wall */
      else if(bc_w->type == 3){
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;
	  k_b[index_] = 0;
	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_w->type == 4){

	i = 0;
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    
	    index_u = J*nx + i;
	    
	    if(u[index_u] > 0){
	      k_b[index_] = k_W_non_uni[J];
	      k_aE[index_] = 0;
	    }
	    else{
	      k_b[index_] = 0;
	      k_aE[index_] = 1;
	    }
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(bc_w->type == 5){
	
	for(J=1; J<Ny-1; J++){
	  
	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    k_b[index_] = k_turb[J*Nx + Nx-3] * fe[J*Nx + Nx-3] +
	      k_turb[J*Nx + Nx-2] * (1 - fe[J*Nx + Nx-3]);
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }

    } /* West section */

#pragma omp section 
    {
      /* East boundary */
      I = Nx - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_e->type == 0) || (bc_e->type == 2)){
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    k_aW[index_] = 1;
	  }
	  else{
	    k_b[index_] = 0;
	  }    
	}
      }
      /* Type = 1, Fixed value */
      else if(bc_e->type == 1){
	if(bc_e->parabolize_profile_OK){
	  
	  for(J=1; J<Ny-1; J++){

	    index_ = J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = k_E_non_uni[J];
	  }
	}
	else{
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_b[index_] = bc_e->k_value;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      /* Type = 3, Wall */
      else if(bc_e->type == 3){
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  k_aP[index_] = 1;
	  k_b[index_] = 0;
	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_e->type == 4){

	i = nx - 1;
	
	for(J=1; J<Ny-1; J++){
	  
	  index_ = J*Nx + I;
	  k_aP[index_] = 1;
	  
	  if(dom_matrix[index_] == 0){
	    
	    index_u = J*nx + i;
	    
	    if(u[index_u] < 0){
	      k_b[index_] = k_E_non_uni[J];
	      k_aW[index_] = 0;
	    }
	    else{
	      k_b[index_] = 0;
	      k_aW[index_] = 1;
	    }
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(bc_e->type == 5){
	
	for(J=1; J<Ny-1; J++){
	  
	  index_ = J*Nx + I;
	  k_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    k_b[index_] = k_turb[J*Nx + 1] * fe[J*Nx + 1] + 
	      k_turb[J*Nx + 2] * (1 - fe[J*Nx + 1]);
	  }
	  else{
	    k_b[index_] = 0;
	  }
	}
      }

    } /* East section*/
  } /* sections */

  return;
}

/* SET_BC_TURB_EPS */
/*****************************************************************************/
/**
  
  Sets:

  Struct coeffs_eps: coefficients for boundary values, according to 
  specified boundary conditions

  @param data

  @return void
  
  MODIFIED: 19-03-2019
*/
void set_bc_turb_eps(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int turb_model = data->turb_model;

  const int *dom_matrix = data->domain_matrix;

  const double mu = data->mu;
  const double rho = data->rho_v[0];

  double *eps_aP = data->coeffs_eps.aP;
  double *eps_aW = data->coeffs_eps.aW;
  double *eps_aE = data->coeffs_eps.aE;
  double *eps_aS = data->coeffs_eps.aS;
  double *eps_aN = data->coeffs_eps.aN;
  double *eps_b = data->coeffs_eps.b;

  const double *k_turb = data->k_turb;
  const double *eps_turb = data->eps_turb;
  const double *y_wall = data->y_wall;
  const double *eps_W_non_uni = data->eps_W_non_uni;
  const double *eps_E_non_uni = data->eps_E_non_uni;
  const double *eps_S_non_uni = data->eps_S_non_uni;
  const double *eps_N_non_uni = data->eps_N_non_uni;
  const double *u = data->u;
  const double *v = data->v;
  const double *fn = data->fn;
  const double *fe = data->fe;

  const b_cond_flow *bc_w = &(data->bc_flow_west);
  const b_cond_flow *bc_e = &(data->bc_flow_east);
  const b_cond_flow *bc_s = &(data->bc_flow_south);
  const b_cond_flow *bc_n = &(data->bc_flow_north);

#pragma omp parallel sections default(none) private(I, J, i, j, index_, index_u, index_v) \
  shared(dom_matrix, eps_W_non_uni, eps_E_non_uni, eps_S_non_uni, eps_N_non_uni, y_wall, \
  k_turb, eps_turb, eps_aP, eps_aW, eps_aE, eps_aS, eps_aN, eps_b, u, v, fn, fe, \
  bc_w, bc_e, bc_s, bc_n)
  
  {

#pragma omp section 
    {
      /* South boundary */
      J = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_s->type == 0) || (bc_s->type == 2)){
	
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){   
	    eps_aN[index_] = 1;
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_s->type == 1){
	if(bc_s->parabolize_profile_OK){
	  
	  for(I=1; I<Nx-1; I++){

	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;
	    eps_b[index_] = eps_S_non_uni[I];

	  }
	}
	else{
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;
	    
	    if(dom_matrix[index_] == 0){
	      eps_b[index_] = bc_s->eps_value;
	    }
	    else{
	      eps_b[index_] = 0;	      
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_s->type == 3){
	
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){   
	    /* AKN model*/
	    if(turb_model == 1){
	      if(y_wall[index_+Nx] == 0){
		eps_b[index_] = 0;
	      }
	      else{
		eps_b[index_] = 2 * mu * k_turb[index_+Nx] / 
		  (rho * y_wall[index_+Nx] * y_wall[index_+Nx]);
	      }
	    }
	    /* CG model*/
	    else if(turb_model == 2){
	      eps_aN[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      
      /* Type = 4, Constant pressure */
      else if(bc_s->type == 4){

	j = 0;
	
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;
	  
	  if(dom_matrix[index_] == 0){
	    
	    index_v = j*Nx + I;
	    
	    if(v[index_v] > 0){
	      eps_b[index_] = eps_S_non_uni[I];
	      eps_aN[0] = 0;
	    }
	    else{
	      eps_b[index_] = 0;
	      eps_aN[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      /* Type = 5, Periodic */
      else if(bc_s->type == 5){
	
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    eps_b[index_] = eps_turb[(Ny-3)*Nx + I] * fn[(Ny-3)*Nx + I] +
	      eps_turb[(Ny-2)*Nx + I] * (1 - fn[(Ny-3)*Nx + I]);	    
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }

    } /* South section */

#pragma omp section 
    {
      /* North boundary */
      J = Ny - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_n->type == 0) || (bc_n->type == 2)){
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    eps_aS[index_] = 1;
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      /*Type = 1, Fixed value */
      else if(bc_n->type == 1){
	if(bc_n->parabolize_profile_OK){
	  
	  for(I=1; I<Nx-1; I++){

	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;
	    eps_b[index_] = eps_N_non_uni[I];

	  }
	}
	else{
	  for(I=1; I<Nx-1; I++){

	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;
	    
	    if(dom_matrix[index_] == 0){
	      eps_b[index_] = bc_n->eps_value;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }
      /* Type = 3, Wall */
      else if(bc_n->type == 3){
	for(I=1; I<Nx-1; I++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    
	    if(turb_model == 1){
	      if(y_wall[index_-Nx] == 0){
		eps_b[index_] = 0;
	      }
	      else{
		eps_b[index_] = 2 * mu * k_turb[index_-Nx] / 
		  (rho * y_wall[index_-Nx] * y_wall[index_-Nx]);
	      }
	    }
	    else if(turb_model == 2){
	      eps_aS[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_n->type == 4){

	j = ny - 1;
	
	for(I=1; I<Nx-1; I++){
	  
	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    
	    index_v = J*Nx + I;
	    
	    if(v[index_v] < 0){
	      eps_b[index_] = eps_N_non_uni[I];
	      eps_aS[index_] = 0;
	    }
	    else{
	      eps_b[index_] = 0;
	      eps_aS[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;	    
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(bc_n->type == 5){
	
	for(I=1; I<Nx-1; I++){
	  
	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    eps_b[index_] = eps_turb[Nx + I] * fn[Nx + I] + 
	      eps_turb[2*Nx + I] * (1 - fn[Nx + I]);
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }

    } /* North section */

#pragma omp section
    {
      /* West boundary */
      I = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_w->type == 0) || (bc_w->type == 2)){
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    eps_aE[index_] = 1;
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      /* Type = 1, Fixed value */
      else if(bc_w->type == 1){
	if(bc_w->parabolize_profile_OK){
	  
	  for(J=1; J<Ny-1; J++){

	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;
	    eps_b[index_] = eps_W_non_uni[J];
	  }
	}
	else{
	  for(J=1; J<Ny-1; J++){

	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_b[index_] = bc_w->eps_value;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }
      /* Type = 3, Wall */
      else if(bc_w->type == 3){
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    if(turb_model == 1){
	      if(y_wall[index_+1] == 0){
		eps_b[index_] = 0;
	      }
	      else{
		eps_b[index_] = 2 * mu * k_turb[index_+1] / 
		  (rho * y_wall[index_+1] * y_wall[index_+1]);
	      }
	    }
	    else if(turb_model == 2){
	      eps_aE[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_w->type == 4){

	i = 0;
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    
	    index_u = J*nx + i;

	    if(u[index_u] > 0){
	      eps_b[index_] = eps_W_non_uni[J];
	      eps_aE[index_] = 0;
	    }
	    else{
	      eps_b[index_] = 0;
	      eps_aE[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(bc_w->type == 5){
	
	for(J=1; J<Ny-1; J++){
	  
	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    eps_b[index_] = eps_turb[J*Nx + Nx-3] * fe[J*Nx + Nx-3] + 
	      eps_turb[J*Nx + Nx-2] * (1 - fe[J*Nx + Nx-3]);
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }

    } /* West section */

#pragma omp section 
    {
      /* East boundary */
      I = Nx - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_e->type == 0) || (bc_e->type == 2)){
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    eps_aW[index_] = 1;
	  }
	  else{
	    eps_b[index_] = 0;
	  }    
	}
      }
      /* Type = 1, Fixed value */
      else if(bc_e->type == 1){
	if(bc_e->parabolize_profile_OK){
	  
	  for(J=1; J<Ny-1; J++){

	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;
	    eps_b[index_] = eps_E_non_uni[J];  
	  }
	}
	else{
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_b[index_] = bc_e->eps_value;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }
      /* Type = 3, Wall */
      else if(bc_e->type == 3){
	
	for(J=1; J<Ny-1; J++){

	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    if(turb_model == 1){
	      if(y_wall[index_-1] == 0){
		eps_b[index_] = 0;
	      }
	      else{
		eps_b[index_] = 2 * mu * k_turb[index_-1] / 
		  (rho * y_wall[index_-1] * y_wall[index_-1]);
	      }
	    }
	    else if(turb_model == 2){
	      eps_aW[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;
	  }    
	}
      }
      /* Type = 4, Constant pressure */
      else if(bc_e->type == 4){

	i = nx - 1;
	
	for(J=1; J<Ny-1; J++){
	  
	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;
	  
	  if(dom_matrix[index_] == 0){

	    index_u = J*nx + i;
	    
	    if(u[index_u] < 0){
	      eps_b[index_] = eps_E_non_uni[J];
	      eps_aW[index_] = 0;
	    }
	    else{
	      eps_b[index_] = 0;
	      eps_aW[index_] = 1;
	    }
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }
      /* Type = 5, Periodic boundary */
      else if(bc_e->type == 5){
	
	for(J=1; J<Ny-1; J++){
	  
	  index_ = J*Nx + I;
	  eps_aP[index_] = 1;

	  if(dom_matrix[index_] == 0){
	    eps_b[index_] = eps_turb[J*Nx + 1] * fe[J*Nx + 1] +
	      eps_turb[J*Nx + 2] * (1 - fe[J*Nx + 1]);
	  }
	  else{
	    eps_b[index_] = 0;
	  }
	}
      }

    } /* East section*/
  } /* sections */

  return;
}

/* TURB_BOUNDARY_NODES */
/*****************************************************************************/
/**

  Sets:

  Array (int, turb_boundary_cell_count) turb_boundary_indexs: domain_matrix 
  fluid cells that have a non fluid neighbour.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 13-08-2019
*/
int turb_boundary_nodes(Data_Mem *data){

  int I, J, index_;
  int turb_boundary_cell_count = 0;
  int cell_OK = 0;
  int ret_value = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *dom_matrix = data->domain_matrix;
  
#pragma omp parallel for default(none) private(I, J, index_, cell_OK) shared(dom_matrix) reduction(+:turb_boundary_cell_count)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
        
      index_ = J*Nx + I;

      cell_OK = 
	(dom_matrix[index_+1] != 0) || (dom_matrix[index_-1] != 0) ||
	(dom_matrix[index_+Nx] != 0) || (dom_matrix[index_-Nx] != 0);

      if((dom_matrix[index_] == 0) && cell_OK){
	turb_boundary_cell_count++;
      }
    }
  }
  
  data->turb_boundary_indexs = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_I = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_J = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_K = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->depsdxb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->depsdyb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_k_turb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_eps_turb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_k_turb_lag = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_eps_turb_lag = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_cell_count = turb_boundary_cell_count;
  data->eps_bnd_x = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->eps_bnd_y = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->turb_bnd_excl_indexs = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->y_plus = create_array(turb_boundary_cell_count, 1, 1, 0);

  turb_boundary_cell_count = 0;

  int *turb_boundary_indexs = data->turb_boundary_indexs;
  int *turb_boundary_I = data->turb_boundary_I;
  int *turb_boundary_J = data->turb_boundary_J;
  int *turb_boundary_K = data->turb_boundary_K;
  
#pragma omp parallel for default(none) private(I, J, index_, cell_OK)	\
  shared(dom_matrix, turb_boundary_indexs, turb_boundary_I, turb_boundary_J, \
	 turb_boundary_K, turb_boundary_cell_count) 

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
        
      index_ = J*Nx + I;

      cell_OK = 
	(dom_matrix[index_+1] != 0) || (dom_matrix[index_-1] != 0) ||
	(dom_matrix[index_+Nx] != 0) || (dom_matrix[index_-Nx] != 0);

      if((dom_matrix[index_] == 0) && cell_OK){
#pragma omp critical
	{
	  turb_boundary_indexs[turb_boundary_cell_count] = index_;
	  turb_boundary_I[turb_boundary_cell_count] = I;
	  turb_boundary_J[turb_boundary_cell_count] = J;
	  turb_boundary_K[turb_boundary_cell_count] = 0;
	  turb_boundary_cell_count++;
	}
      }
    }   
  }
 
  return ret_value;
}

/* COMPUTE_K_SC_AT_INTERFACE */
/*****************************************************************************/
/**

   Sets:

   Array (double, turb_boundary_cell_count): Data_Mem::SC_vol_k_turb: Source term due to solid boundary for Data_Mem::k_turb equation.

   @param data

   @return void

   MODIFIED: 07-04-2020
*/
void compute_k_SC_at_interface(Data_Mem *data){
	
  int I, J, i, j;
  int count, index_;
  int index_u, index_v;

  const int Nx = data->Nx;
  const int nx = data->nx;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  
  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;

  double k_b, mu_eff, k_m;
  double dkdxb, dkdyb;
  double Fw, Fe, Fs, Fn;
  double Da, m;

  const double C_sigma_k = data->C_sigma_k;
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double eps_dist_tol = data->eps_dist_tol;
  
  double *SC_vol_k_turb = data->SC_vol_k_turb;

  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *mu_turb = data->mu_turb;
  const double *k_turb = data->k_turb;
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;
  

  /* To account for the contribution of more than one neighbour */
  for(count=0; count<turb_boundary_cell_count; count++){
    SC_vol_k_turb[count] = 0;
  }
  
  /* Boundary value is zero for k */

  k_b = 0;

  for(count=0; count<turb_boundary_cell_count; count++){
      
    index_ = turb_boundary_indexs[count];
    mu_eff = mu + mu_turb[index_] / C_sigma_k;
    I = turb_boundary_I[count];
    J = turb_boundary_J[count];
    i = I - 1;
    j = J - 1;
    index_u = J*nx + i;
    index_v = j*Nx + I;

    if(dom_matrix[index_-1] != 0){
      /* Diffusive correction factor */
      Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
      m = Delta_x[index_] / Da;
      /* Convective parameter */
      Fw = rho * u[index_u];

      /* Sato (W: -+-) */      
      __get_b_values_Dirichlet(&dkdxb, &k_m, eps_x[index_], eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_+1],
			       Delta_xW[index_], Delta_xE[index_], -1);            

      /* Contribution to SC_vol_k_turb (W: -+) */
      SC_vol_k_turb[count] += (-1) * mu_eff * dkdxb * Delta_y[index_] * m + 
	Fw * k_m * Delta_y[index_];

    }

    if(dom_matrix[index_+1] != 0){
      /* Diffusive correction factor */
      Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
      m = Delta_x[index_] / Da;
      /* Convective parameter */
      Fe = rho * u[index_u+1];

      /* Sato (E: +-+) */
      __get_b_values_Dirichlet(&dkdxb, &k_m, eps_x[index_], eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_-1],
			       Delta_xE[index_], Delta_xW[index_], 1);
      
      /* Contribution to SC_vol_k_turb (E: +-) */
      SC_vol_k_turb[count] += mu_eff * dkdxb * Delta_y[index_] * m - 
	Fe * k_m * Delta_y[index_];
    }

    if(dom_matrix[index_-Nx] != 0){
      /* Diffusive correction factor */
      Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
      m = Delta_y[index_] / Da;
      /* Convective parameter */
      Fs = rho * v[index_v];

      /* Sato (S: -+-) */
      __get_b_values_Dirichlet(&dkdyb, &k_m, eps_y[index_], eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_+Nx],
			       Delta_yS[index_], Delta_yN[index_], -1);
     
      /* Contribution to SC_vol_k_turb (S: -+) */
      SC_vol_k_turb[count] += (-1) * mu_eff * dkdyb * Delta_x[index_] * m + 
	Fs * k_m * Delta_x[index_];
    }

    if(dom_matrix[index_+Nx] != 0){
      /* Diffusive correction factor */
      Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
      m = Delta_y[index_] / Da;
      /* Convective parameter */
      Fn = rho * v[index_v+Nx];

      /* Sato (N: +-+) */
      __get_b_values_Dirichlet(&dkdyb, &k_m, eps_y[index_], eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_-Nx],
			       Delta_yN[index_], Delta_yS[index_], 1);
      
      /* Contribution to SC_vol_k_turb (N: +-) */
      SC_vol_k_turb[count] += mu_eff * dkdyb * Delta_x[index_] * m - 
	Fn * k_m * Delta_x[index_];

    }
  }

  return;
}

/* COMPUTE_EPS_SC_AT_INTERFACE */
/*****************************************************************************/
/**

   Sets:

   Array (double, turb_boundary_cell_count): Data_Mem::SC_vol_eps_turb: Source term due to solid boundary for Data_Mem::eps_turb equation.

   @param data

   @return void

   MODIFIED: 23-04-2020
*/
void compute_eps_SC_at_interface(Data_Mem *data){
	
  int count, index_;
  int i, j, I, J, index_u, index_v;
  int iter_count;
  int cell_done_OK = 0;
  int E_flag, W_flag, N_flag, S_flag;

  const int nx = data->nx;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  const int turb_model = data->turb_model;
  const int iter_total = 100;
  
  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;

  double eps_bnd, mu_eff, eps_m;
  double Jx = 0, Jy = 0;
  double eps_m_ew = 0, eps_m_ns = 0;
  double Few = 0, Fns = 0;
  double Fw, Fe, Fs, Fn;
  double depsdSface_x, depsdSface_y;
  double Da, m, mx = 0, my = 0;

  /* [0]: (I,J), [1]: (I+-1,J), [2]: (I,J+-1) */
  double depsdx[3] = {0};
  double depsdy[3] = {0};
  double depsdS[3] = {0};

  const double C_sigma_eps = data->C_sigma_eps;
  const double mu = data->mu;
  const double max_eps_wall_value = 1e10;
  const double rho = data->rho_v[0];
  const double eps_dist_tol = data->eps_dist_tol;
  const double depsdNb = 0;  

  double *depsdxb = data->depsdxb;
  double *depsdyb = data->depsdyb;
  double *SC_vol_eps_turb = data->SC_vol_eps_turb;
   
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *mu_turb = data->mu_turb;
  const double *eps_turb = data->eps_turb;
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  const double *eps_bnd_x = data->eps_bnd_x;
  const double *eps_bnd_y = data->eps_bnd_y;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;

  /* To account for the contribution of more than one neighbour */
  for(count=0; count<turb_boundary_cell_count; count++){
    SC_vol_eps_turb[count] = 0;
  }
  
  if(turb_model == 1){

    /* Dirichlet boundary condition for epsilon
       Calculate boundary values according to eps_bnd = 2*nu*(d(sqrt(k))/dy_wall)**2 */

    compute_eps_bnd(data);

    for(count=0; count<turb_boundary_cell_count; count++){
      
      index_ = turb_boundary_indexs[count];
      mu_eff = mu + mu_turb[index_] / C_sigma_eps;
      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      i = I - 1;
      j = J - 1;
      index_u = J*nx + i;
      index_v = j*Nx + I;

      if(dom_matrix[index_-1]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_x[count]);
	/* Diffusive correction factor */
	Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
	m = Delta_x[index_] / Da;
	/* Convective parameter */
	Fw = rho * u[index_u];

	/* Sato (W: -+-) */
	__get_b_values_Dirichlet(&(depsdxb[count]), &eps_m, eps_x[index_], eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_+1],
				 Delta_xW[index_], Delta_xE[index_], -1);	

	/* Contribution to SC_vol_eps_turb (W: -+) */
	SC_vol_eps_turb[count] += (-1) * mu_eff * depsdxb[count] * 
	  Delta_y[index_] * m + Fw * eps_m * Delta_y[index_];

      }

      if(dom_matrix[index_+1]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_x[count]);
	/* Diffusive correction factor */
	Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
	m = Delta_x[index_] / Da;
	/* Convective parameter */
	Fe = rho * u[index_u+1];

	/* Sato (E: +-+): derivative at boundary */
	__get_b_values_Dirichlet(&(depsdxb[count]), &eps_m, eps_x[index_], eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_-1],
				 Delta_xE[index_], Delta_xW[index_], 1);		

	/* Contribution to SC_vol_eps_turb (E: +-) */
	SC_vol_eps_turb[count] += mu_eff * depsdxb[count] * 
	  Delta_y[index_] * m - Fe * eps_m * Delta_y[index_];

      }

      if(dom_matrix[index_-Nx]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_y[count]);
	/* Diffusive correction factor */
	Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
	m = Delta_y[index_] / Da;
	/* Convective parameter */
	Fs = rho * v[index_v];

	/* Sato (S: -+-): derivative at face */
	__get_b_values_Dirichlet(&(depsdyb[count]), &eps_m, eps_y[index_], eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_+Nx],
				 Delta_yS[index_], Delta_yN[index_], -1);		

	/* Contribution to SC_vol_eps_turb (S: -+) */
	SC_vol_eps_turb[count] += (-1) * mu_eff * depsdyb[count] * 
	  Delta_x[index_] * m + Fs * eps_m * Delta_x[index_];

      }

      if(dom_matrix[index_+Nx]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_y[count]);
	/* Diffusive correction factor */
	Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
	m = Delta_y[index_] / Da;
	/* Convective parameter */
	Fn = rho * v[index_v+Nx];

	/* Sato (N: +-+): derivative at boundary */
	__get_b_values_Dirichlet(&(depsdyb[count]), &eps_m, eps_y[index_], eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_-Nx],
				 Delta_yN[index_], Delta_yS[index_], 1);       	

	/* Contribution to SC_vol_eps_turb (N: +-) */
	SC_vol_eps_turb[count] += mu_eff * depsdyb[count] * 
	  Delta_x[index_] * m - Fn * eps_m * Delta_x[index_];
      }      
    }
  } //   end if(turb_model == 1)


  if(turb_model == 2){

    // Isoflux boundary condition depsdN = 0

    for(iter_count=0; iter_count<iter_total; iter_count++){

      for(count=0; count<turb_boundary_cell_count; count++){
      
	index_ = turb_boundary_indexs[count];
      
	I = turb_boundary_I[count];
	J = turb_boundary_J[count];

	depsdx[0] = 0;
	depsdx[1] = 0;

	depsdy[0] = 0;
	depsdy[1] = 0;

	depsdS[0] = 0;
	depsdS[1] = 0;

	Jx = 0;
	Jy = 0;

	cell_done_OK = 0;
      
	i = I - 1;
	j = J - 1;

	index_u = J*nx + i;
	index_v = j*Nx + I;

	mu_eff = mu + mu_turb[index_] / C_sigma_eps;

	/* E-P line intersects with curve */
	if(dom_matrix[index_+1] && !cell_done_OK){
	  cell_done_OK = 1;
	  /* depsdx: [0], [1], [2] */
	  depsdx[0] = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_],
			  eps_turb[index_-1], Delta_xE[index_], Delta_xW[index_]);

	  if (I == 1){
	    depsdx[1] = (eps_turb[index_] - eps_turb[index_-1]) / Delta_xW[index_];
	  }
	  else{
	    depsdx[1] = __central_der(eps_turb[index_], eps_turb[index_-1], eps_turb[index_-2],
			  Delta_xE[index_-1], Delta_xW[index_-1]);
	  }

	  /* Case E-N */
	  if(dom_matrix[index_+Nx]){
				    
	    if(dom_matrix[index_-Nx+1]){
	      depsdx[2] = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx],
			  eps_turb[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }
	    else if(dom_matrix[index_-Nx-1]){
	      depsdx[2] = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx+1],
			  eps_turb[index_-Nx], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	    }
	    else{
	      depsdx[2] = __central_der(eps_turb[index_-Nx+1], eps_turb[index_-Nx], eps_turb[index_-Nx-1],
			  Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }
	  }
	  /* Case E-S */
	  if(dom_matrix[index_-Nx]){
	    
	    if(dom_matrix[index_+Nx+1]){
	      depsdx[2] = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx],
			  eps_turb[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }
	    else if(dom_matrix[index_+Nx-1]){
	      depsdx[2] = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx+1],
			  eps_turb[index_+Nx], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	    }
	    else{
	      depsdx[2] = __central_der(eps_turb[index_+Nx+1], eps_turb[index_+Nx], eps_turb[index_+Nx-1],
			  Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }
	  }

	  /* depsdy: [0], [1], [2] */
	  depsdy[0] = __central_der(eps_turb[index_+Nx], eps_turb[index_], eps_turb[index_-Nx],
			  Delta_yN[index_], Delta_yS[index_]);
	  depsdy[1] = __central_der(eps_turb[index_+Nx-1], eps_turb[index_-1], eps_turb[index_-Nx-1],
			  Delta_yN[index_-1], Delta_yS[index_-1]);

	  /* Cases E-N */
	  if(dom_matrix[index_+Nx]){

	    depsdy[0] = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_],
			  eps_turb[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	  
	    if (J == 1){
	      depsdy[2] = (eps_turb[index_] - eps_turb[index_-Nx]) / Delta_yS[index_];
	    }
	    else{
	      depsdy[2] = __central_der(eps_turb[index_], eps_turb[index_-Nx], eps_turb[index_-2*Nx],
			  Delta_yN[index_-Nx], Delta_yS[index_-Nx]);
	    }
	  }

	  if(dom_matrix[index_+Nx-1]){

	    depsdy[1] = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_-1],
			  eps_turb[index_-1-Nx], Delta_yN[index_-1], Delta_yS[index_-1]);
	  }
	
	  /* Cases E-S */
	  if(dom_matrix[index_-Nx]){

	    depsdy[0] = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_+Nx],
			  eps_turb[index_], Delta_yS[index_], Delta_yN[index_]);
	  
	    if (J == Ny-2){
	      depsdy[2] = (eps_turb[index_+Nx] - eps_turb[index_]) / Delta_yN[index_];
	    }
	    else{
	      depsdy[2] = __central_der(eps_turb[index_+2*Nx], eps_turb[index_+Nx], eps_turb[index_],
			  Delta_yN[index_+Nx], Delta_yS[index_+Nx]);
	    }
	  }
	  if(dom_matrix[index_-Nx-1]){

	    depsdy[1] = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_+Nx-1],
			  eps_turb[index_-1], Delta_yS[index_-1], Delta_yN[index_-1]);
	  }
	} /* end case E-P */

	/* W-P line intersects with curve */    
	if(dom_matrix[index_-1] && !cell_done_OK){
	  cell_done_OK = 1;
	  /* depsdx: [0], [1], [2] */
	  depsdx[0] = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_+1],
			  eps_turb[index_], Delta_xW[index_], Delta_xE[index_]);

	  if (I == Nx-2){
	    depsdx[1] = (eps_turb[index_+1] - eps_turb[index_]) / Delta_xE[index_];
	  }
	  else{
	    depsdx[1] = __central_der(eps_turb[index_+2], eps_turb[index_+1], eps_turb[index_],
			  Delta_xE[index_+1], Delta_xW[index_+1]);
	  }

	  /* W-N */
	  if(dom_matrix[index_+Nx]){
	    
	    if(dom_matrix[index_-Nx+1]){
	      depsdx[2] = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx],
			  eps_turb[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }
	    else if(dom_matrix[index_-Nx-1]){
	      depsdx[2] = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx+1],
			  eps_turb[index_-Nx], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	    }
	    else{
	      depsdx[2] = __central_der(eps_turb[index_-Nx+1], eps_turb[index_-Nx], eps_turb[index_-Nx-1],
			  Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }
	  }
	  /* W-S */
	  if(dom_matrix[index_-Nx]){
	    
	    if(dom_matrix[index_+Nx+1]){
	      depsdx[2] = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx],
			  eps_turb[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }
	    else if(dom_matrix[index_+Nx-1]){
	      depsdx[2] = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx+1],
			  eps_turb[index_+Nx], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	    }
	    else{
	      depsdx[2] = __central_der(eps_turb[index_+Nx+1], eps_turb[index_+Nx], eps_turb[index_+Nx-1],
			  Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }
	  }

	  /* depsdy: [0], [1], [2] */
	  depsdy[0] = __central_der(eps_turb[index_+Nx], eps_turb[index_], eps_turb[index_-Nx],
			  Delta_yN[index_], Delta_yS[index_]);
	  depsdy[1] = __central_der(eps_turb[index_+Nx+1], eps_turb[index_+1], eps_turb[index_-Nx+1],
			  Delta_yN[index_+1], Delta_yS[index_+1]);

	  /* W-N */
	  if(dom_matrix[index_+Nx]){

	    depsdy[0] = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_],
			  eps_turb[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	  
	    if (J == 1){
	      depsdy[2] = (eps_turb[index_] - eps_turb[index_-Nx]) / Delta_yS[index_];
	    }
	    else{
	      depsdy[2] = __central_der(eps_turb[index_], eps_turb[index_-Nx], eps_turb[index_-2*Nx],
			  Delta_yN[index_-Nx], Delta_yS[index_-Nx]);
	    }
	  }
	  if(dom_matrix[index_+Nx+1]){
	    depsdy[1] = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+1],
			  eps_turb[index_+1-Nx], Delta_yN[index_+1], Delta_yS[index_+1]);
	  }
	
	  /* W-S */
	  if(dom_matrix[index_-Nx]){

	    depsdy[0] = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_+Nx],
			  eps_turb[index_], Delta_yS[index_], Delta_yN[index_]);
	  
	    if (J == Ny-1){
	      depsdy[2] = (eps_turb[index_+Nx] - eps_turb[index_]) / Delta_yN[index_];
	    }
	    else{
	      depsdy[2] = __central_der(eps_turb[index_+2*Nx], eps_turb[index_+Nx], eps_turb[index_],
			  Delta_yN[index_+Nx], Delta_yS[index_+Nx]);
	    }
	  }
	  if(dom_matrix[index_-Nx+1]){

	    depsdy[1] = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+Nx+1],
			  eps_turb[index_+1], Delta_yS[index_+1], Delta_yN[index_+1]);
	  }
	} /* end case W-P */

	/* N-P line intersects with curve */
	if(dom_matrix[index_+Nx] && !cell_done_OK){
	  cell_done_OK = 1;
	  /* depsdy: [0], [1], [2] */
	  depsdy[0] = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_],
			  eps_turb[index_-Nx], Delta_yN[index_], Delta_yS[index_]);

	  if (J == 1){
	    depsdy[2] = (eps_turb[index_] - eps_turb[index_-Nx]) / Delta_yS[index_];
	  }
	  else{
	    depsdy[2] = __central_der(eps_turb[index_], eps_turb[index_-Nx], eps_turb[index_-2*Nx],
			  Delta_yN[index_-Nx], Delta_yS[index_-Nx]);
	  }
	  /* Case N-E */
	  if(dom_matrix[index_+1]){
	    
	    if(dom_matrix[index_+Nx-1]){
	      depsdy[1] = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_-1],
			  eps_turb[index_-1-Nx], Delta_yN[index_-1], Delta_yS[index_-1]);
	    }
	    else if(dom_matrix[index_-Nx-1]){
	      depsdy[1] = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_-1+Nx],
			  eps_turb[index_-1], Delta_yS[index_-1], Delta_yN[index_-1]);
	    }
	    else{
	      depsdy[1] = __central_der(eps_turb[index_-1+Nx], eps_turb[index_-1], eps_turb[index_-1-Nx],
			  Delta_yN[index_-1], Delta_yS[index_-1]);
	    }
	  }

	  /* Case N-W */
	  if(dom_matrix[index_-1]){
	    
	    if(dom_matrix[index_+Nx+1]){
	      depsdy[1] = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+1],
			  eps_turb[index_+1-Nx], Delta_yN[index_+1], Delta_yS[index_+1]);
	    }
	    else if(dom_matrix[index_-Nx+1]){
	      depsdy[1] = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+1+Nx],
			  eps_turb[index_+1], Delta_yS[index_+1], Delta_yN[index_+1]);
	    }
	    else{
	      depsdy[1] = __central_der(eps_turb[index_+1+Nx], eps_turb[index_+1], eps_turb[index_+1-Nx],
			  Delta_yN[index_+1], Delta_yS[index_+1]);
	    }
	  }

	  /* depsdx: [0], [1], [2] */
	  depsdx[0] = __central_der(eps_turb[index_+1], eps_turb[index_], eps_turb[index_-1],
			  Delta_xE[index_], Delta_xW[index_]);
	  depsdx[2] = __central_der(eps_turb[index_-Nx+1], eps_turb[index_-Nx], eps_turb[index_-Nx-1],
			  Delta_xE[index_-Nx], Delta_xW[index_-Nx]);

	  /* Cases N-E */
	  if(dom_matrix[index_+1]){

	    depsdx[0] = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_],
			  eps_turb[index_-1], Delta_xE[index_], Delta_xW[index_]);
	  
	    if (I == 1){
	      depsdx[1] =  (eps_turb[index_] - eps_turb[index_-1]) / Delta_xW[index_];
	    }
	    else{
	      depsdx[1] = __central_der(eps_turb[index_], eps_turb[index_-1], eps_turb[index_-2],
			  Delta_xE[index_-1], Delta_xW[index_-1]);
	    }
	  }
	  if(dom_matrix[index_-Nx+1]){

	    depsdx[2] = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx],
			  eps_turb[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }

	  /* Cases N-W */
	  if(dom_matrix[index_-1]){

	    depsdx[0] = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_+1],
			  eps_turb[index_], Delta_xW[index_], Delta_xE[index_]);
	  
	    if (I == Nx-2){
	      depsdx[1] = (eps_turb[index_+1] - eps_turb[index_]) / Delta_xE[index_];
	    }
	    else{
	      depsdx[1] = __central_der(eps_turb[index_+2], eps_turb[index_], eps_turb[index_+1],
			  Delta_xE[index_+1], Delta_xW[index_+1]);
	    }
	  }
	  if(dom_matrix[index_-Nx-1]){
	    
	    depsdx[2] = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx+1],
			  eps_turb[index_-Nx], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	  }
	} /* end case N-P */

	/* S-P line intersects with curve */
	if(dom_matrix[index_-Nx] && !cell_done_OK){
	  cell_done_OK = 1;
	  /* depsdy: [0], [1], [2] */
	  depsdy[0] = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_+Nx],
			  eps_turb[index_], Delta_yS[index_], Delta_yN[index_]);

	  if (J == Ny-2){
	    depsdy[2] = (eps_turb[index_+Nx] - eps_turb[index_]) / Delta_yN[index_];
	  }
	  else{
	    depsdy[2] = __central_der(eps_turb[index_+2*Nx], eps_turb[index_+Nx], eps_turb[index_],
			  Delta_yN[index_+Nx], Delta_yS[index_+Nx]);
	  }
	  /* Case S-E */
	  if(dom_matrix[index_+1]){
	    
	    if(dom_matrix[index_+Nx-1]){
	      depsdy[1] = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_-1],
			  eps_turb[index_-1-Nx], Delta_yN[index_-1], Delta_yS[index_-1]);
	    }
	    else if(dom_matrix[index_-Nx-1]){
	      depsdy[1] = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_-1+Nx],
			  eps_turb[index_-1], Delta_yS[index_-1], Delta_yN[index_-1]);
	    }
	    else{
	      depsdy[1] = __central_der(eps_turb[index_-1+Nx], eps_turb[index_-1], eps_turb[index_-1-Nx],
			  Delta_yN[index_-1], Delta_yS[index_-1]);
	    }
	  }

	  /* Case S-W */
	  if(dom_matrix[index_-1]){
	    
	    if(dom_matrix[index_+Nx+1]){
	      depsdy[1] = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+1],
			  eps_turb[index_+1-Nx], Delta_yN[index_+1], Delta_yS[index_+1]);
	    }
	    else if(dom_matrix[index_-Nx+1]){
	      depsdy[1] = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+1+Nx],
			  eps_turb[index_+1], Delta_yS[index_+1], Delta_yN[index_+1]);
	    }
	    else{
	      depsdy[1] = __central_der(eps_turb[index_+1+Nx], eps_turb[index_+1], eps_turb[index_+1-Nx],
			  Delta_yN[index_+1], Delta_yS[index_+1]);
	    }
	  }

	  /* depsdx: [0], [1], [2] */
	  depsdx[0] = __central_der(eps_turb[index_+1], eps_turb[index_], eps_turb[index_-1],
			  Delta_xE[index_], Delta_xW[index_]);
	  depsdx[2] = __central_der(eps_turb[index_+Nx+1], eps_turb[index_+Nx], eps_turb[index_+Nx-1],
			  Delta_xE[index_+Nx], Delta_xW[index_+Nx]);

	  /* Cases S-E */
	  if(dom_matrix[index_+1]){

	    depsdx[0] = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_],
			  eps_turb[index_-1], Delta_xE[index_], Delta_xW[index_]);
	  
	    if (I == 1){
	      depsdx[1] = (eps_turb[index_] - eps_turb[index_-1]) / Delta_xW[index_];
	    }
	    else{
	      depsdx[1] = __central_der(eps_turb[index_], eps_turb[index_-1], eps_turb[index_-2],
			  Delta_xE[index_-1], Delta_xW[index_-1]);
	    }
	  }
	  if(dom_matrix[index_+Nx+1]){

	    depsdx[2] = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx],
			  eps_turb[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }

	  /* Cases S-W */
	  if(dom_matrix[index_-1]){

	    depsdx[0] = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_+1],
			  eps_turb[index_], Delta_xW[index_], Delta_xE[index_]);
	  
	    if (I == Nx-2){
	      depsdx[1] = (eps_turb[index_+1] - eps_turb[index_]) / Delta_xE[index_];
	    }
	    else{
	      depsdx[1] = __central_der(eps_turb[index_+2], eps_turb[index_+1], eps_turb[index_],
			  Delta_xE[index_+1], Delta_xW[index_+1]);
	    }
	  }
	  if(dom_matrix[index_+Nx-1]){

	    depsdx[2] = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx+1],
			  eps_turb[index_+Nx], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	  }
	} /* end case S-P */

	/* Here is the contribution for the source term */

	/* Determination of depsdxb */
	if(dom_matrix[index_-1] || dom_matrix[index_+1]){
	  /* Eq 27 Sato, construction of depsdS 
	     in points (I,J), (I+-1,J), (I,J+-1) */
	  depsdS[0] = depsdx[0] * par_vec_x_x[index_] + depsdy[0] * par_vec_x_y[index_];
	  depsdS[1] = depsdx[1] * par_vec_x_x[index_] + depsdy[1] * par_vec_x_y[index_];
	  /* Eq 28 Sato, extrapolation to boundary 
	     for determination of depsdSb */
	  if(dom_matrix[index_-1]){
	    depsdSface_x = __linear_extrap_to_b(depsdS[0], depsdS[1], eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	  }
	  else{
	    depsdSface_x = __linear_extrap_to_b(depsdS[0], depsdS[1], eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	  }
 
	  /* Eq 24 Sato */
	  depsdxb[count] = depsdNb * norm_vec_x_x[index_] + depsdSface_x * par_vec_x_x[index_];
	}

	/* Determination of depsdyb */
	if(dom_matrix[index_-Nx] || dom_matrix[index_+Nx]){
	  /* Eq 27 Sato, construction of depsdS 
	     in points (I,J), (I+-1,J), (I,J+-1) */
	  depsdS[0] = depsdx[0] * par_vec_y_x[index_] + depsdy[0] * par_vec_y_y[index_];
	  depsdS[2] = depsdx[2] * par_vec_y_x[index_] + depsdy[2] * par_vec_y_y[index_];
	  
	  if(dom_matrix[index_-Nx]){
	    depsdSface_y = __linear_extrap_to_b(depsdS[0], depsdS[2], eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	  }
	  else{
	    depsdSface_y = __linear_extrap_to_b(depsdS[0], depsdS[2], eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
	  }
	  
	  depsdyb[count] = depsdNb * norm_vec_y_y[index_] + depsdSface_y * par_vec_y_y[index_];
	}

	/* Convective part at the face */
	if(dom_matrix[index_-1]){
	  Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
	  mx = Delta_x[index_] / Da;
	  Few = rho * u[index_u];
	  eps_m_ew = eps_turb[index_] - 0.5 * Delta_x[index_] * depsdxb[count];
	}

	if(dom_matrix[index_+1]){
	  Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
	  mx = Delta_x[index_] / Da;
	  Few = rho * u[index_u+1];
	  eps_m_ew = eps_turb[index_] + 0.5 * Delta_x[index_] * depsdxb[count];
	}

	if(dom_matrix[index_-Nx]){
	  Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
	  my = Delta_y[index_] / Da;
	  Fns = rho * v[index_v];
	  eps_m_ns = eps_turb[index_] - 0.5 * Delta_y[index_] * depsdyb[count];
	}

	if(dom_matrix[index_+Nx]){
	  Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
	  my = Delta_y[index_] / Da;
	  Fns = rho * v[index_v+Nx];
	  eps_m_ns = eps_turb[index_] + 0.5 * Delta_y[index_] * depsdyb[count];
	}

	/* Build the combined fluxes. */
	/* It must be noted that there are different lengths 
	   for the molecular and convective parts 
	   which are taken into account by the eps factors */
	Jx = - mu_eff * mx * depsdxb[count] + Few * eps_m_ew;
	Jy = - mu_eff * my * depsdyb[count] + Fns * eps_m_ns;

	/* Effect on SC_vol_eps_turb */
	E_flag = E_is_fluid_OK[index_];
	W_flag = W_is_fluid_OK[index_];
	N_flag = N_is_fluid_OK[index_];
	S_flag = S_is_fluid_OK[index_];

	SC_vol_eps_turb[count] = 
	  (-1) * (1 - E_flag) * Jx * Delta_y[index_] +
	  (-1) * (1 - N_flag) * Jy * Delta_x[index_] +
	  (1 - W_flag) * Jx * Delta_y[index_] + 
	  (1 - S_flag) * Jy * Delta_x[index_];      
      
      } /* end for(count=0; count<boundary_cell_count; count++) */
    }
  }//  end if(turb_model == 2)

  return;
}

/* FILTER_NEG_K_EPS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) Data_Mem::k_turb: Replace negative values by zero.

  Array (double, NxNyNz) Data_Mem::eps_turb: Replace negative values by zero.

  @param data

  @return void
  
  MODIFIED: 24-03-2020
*/
void filter_neg_k_eps(Data_Mem *data){
  
  int I, J, K, index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  double *k_turb = data->k_turb;
  double *eps_turb = data->eps_turb;

#pragma omp parallel for default(none) private(I, J, K, index_) shared(k_turb, eps_turb)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
      
	index_ = K*Nx*Ny + J*Nx + I;
	if(k_turb[index_] < 0){
	  k_turb[index_] = 0;
	}
	if(eps_turb[index_] < 0){
	  eps_turb[index_] = 0;
	}
      }
    }
  }

  return;
}

/* CHANGE_BND_TURB_TO_DEFAULT */
/*****************************************************************************/
/**
  
  Sets:

  (double) bc_flow_*->k_value: Set default value for k at inlet if necessary

  (double) bc_flow_*->eps_value: Set default value for eps at inlet if necessary

  @param data

  @return void
  
  MODIFIED: 19-03-2020
*/
void change_bnd_turb_to_default(Data_Mem *data){

  const int solve_3D_OK = data->solve_3D_OK;
  
  const double l0 = data->l0;
  const double cbc = data->cbc;
  const double C_mu = data->C_mu;

  b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  b_cond_flow *bc_flow_north = &(data->bc_flow_north);
  b_cond_flow *bc_flow_bottom = &(data->bc_flow_bottom);
  b_cond_flow *bc_flow_top = &(data->bc_flow_top);


  // West
  if(bc_flow_west->type == 1){    
    if(bc_flow_west->k_value < 0){
      // Ec. (11) Kuzmin, Mierka
      bc_flow_west->k_value = cbc * powf(bc_flow_west->v_value[0], 2);
    }
    if(bc_flow_west->eps_value < 0){
      // Ec. (11) Kuzmin, Mierka
      bc_flow_west->eps_value = C_mu * powf(bc_flow_west->k_value, 1.5) / l0;
    }
  }

  // East  
  if(bc_flow_east->type == 1){
    if(bc_flow_east->k_value < 0){
      bc_flow_east->k_value = cbc * powf(bc_flow_east->v_value[0], 2);
    }
    if(bc_flow_east->eps_value < 0){
      bc_flow_east->eps_value = C_mu * powf(bc_flow_east->k_value, 1.5) / l0;
    }    
  }

  // South
  if(bc_flow_south->type == 1){
    if(bc_flow_south->k_value < 0){
      bc_flow_south->k_value = cbc * powf(bc_flow_south->v_value[1], 2);
    }
    if(bc_flow_south->eps_value < 0){
      bc_flow_south->eps_value = C_mu * powf(bc_flow_south->k_value, 1.5) / l0;
    }
  }

  // North
  if(bc_flow_north->type == 1){
    if(bc_flow_north->k_value < 0){
      bc_flow_north->k_value = cbc * powf(bc_flow_north->v_value[1], 2);
    }
    if(bc_flow_north->eps_value < 0){
      bc_flow_north->eps_value = C_mu * powf(bc_flow_north->k_value, 1.5) / l0;
    }
  }

  if(solve_3D_OK){

    // Top
    if(bc_flow_top->type == 1){
      if(bc_flow_top->k_value < 0){
	bc_flow_top->k_value = cbc * powf(bc_flow_top->v_value[2], 2);
      }
      if(bc_flow_top->eps_value < 0){
	bc_flow_top->eps_value = C_mu * powf(bc_flow_top->k_value, 1.5) / l0;
      }
    }

    // Bottom
    if(bc_flow_bottom->type == 1){
      if(bc_flow_bottom->k_value < 0){
	bc_flow_bottom->k_value = cbc * powf(bc_flow_bottom->v_value[2], 2);
      }
      if(bc_flow_bottom->eps_value < 0){
	bc_flow_bottom->eps_value = C_mu * powf(bc_flow_bottom->k_value, 1.5) / l0;
      }
    }
  }

  return;
}

/* SET_TURB_INIT_VALUES */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) Data_Mem::k_turb: Set initial value.

  Array (double, NxNy) Data_Mem::eps_turb: Set initial value.

  Array (double, NxNy) Data_Mem::mu_turb: Set initial value.

  @param data
    
  @return void
    
  MODIFIED: 04-08-2020
*/
void set_turb_init_values(Data_Mem *data){
  
  int I, J, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int use_y_wall_OK = data->use_y_wall_OK;
  
  const int *domain_matrix = data->domain_matrix;

  double k0, eps0, mu_turb0;
  double max_y_wall;

  const double mu_eff0 = data->mu_eff0;
  const double l0 =  data->l0;
  const double C_mu = data->C_mu;
  const double rho = data->rho_v[0];
  const double mu = data->mu;

  double *k_turb = data->k_turb;
  double *eps_turb = data->eps_turb;
  double *mu_turb = data->mu_turb;
  
  const double *y_wall = data->y_wall;

  // Initial values

  // Ec. (10) Kuzmin, Mierka
  k0 = powf(mu_eff0 / (rho * l0), 2);
  // Ec. (10) Kuzmin, Mierka
  eps0 = C_mu * powf(k0, 1.5) / l0;
  mu_turb0 = MAX(0, mu_eff0 - mu);

  max_y_wall = get_max(y_wall, Nx, Ny, Nz);

#pragma omp parallel for default(none) private(I, J, index_)		\
  shared(k_turb, eps_turb, mu_turb, k0, eps0, mu_turb0, domain_matrix, y_wall, max_y_wall)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
      if(domain_matrix[index_] == 0){
	if(use_y_wall_OK){
	  k_turb[index_] = k0 * y_wall[index_] / max_y_wall;
	}
	else{
	  k_turb[index_] = k0;
	}
	eps_turb[index_] = eps0;
	mu_turb[index_] = mu_turb0;
      }
    }
  }

  /* Boundary values initialization */
  /* Helps to get consistent starting field for k and eps calculation loops */

#pragma omp parallel sections default(none) private(I, J, index_)	\
  shared(domain_matrix, k_turb, eps_turb, k0, eps0, y_wall, max_y_wall, mu_turb, mu_turb0)
  {
  
#pragma omp section 
    {
      /* West */
      I = 0;
      for(J=1; J<Ny-1; J++){
	index_ = J*Nx + I;
	if(domain_matrix[index_] == 0){
	  if(use_y_wall_OK){
	    k_turb[index_] = k0 * y_wall[index_] / max_y_wall;
	  }
	  else{
	    k_turb[index_] = k0;
	  }
	  eps_turb[index_] = eps0;
	  mu_turb[index_] = mu_turb0;
	}
      }
    }

#pragma omp section  
    {
      /* East */
      I = Nx - 1;
      for(J=1; J<Ny-1; J++){
	index_ = J*Nx + I;
	if(domain_matrix[index_] == 0){
	  if(use_y_wall_OK){
	    k_turb[index_] = k0 * y_wall[index_] / max_y_wall;
	  }
	  else{
	    k_turb[index_] = k0;
	  }
	  eps_turb[index_] = eps0;
	  mu_turb[index_] = mu_turb0;
	}
      }
    }

#pragma omp section 
    {
      /* South */
      J = 0;
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	if(domain_matrix[index_] == 0){
	  if(use_y_wall_OK){
	    k_turb[index_] = k0 * y_wall[index_] / max_y_wall;
	  }
	  else{
	    k_turb[index_] = k0;
	  }
	  eps_turb[index_] = eps0;
	  mu_turb[index_] = mu_turb0;
	}
      }
    }

#pragma omp section
    {
      /* North */
      J = Ny - 1;
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	if(domain_matrix[index_] == 0){
	  if(use_y_wall_OK){
	    k_turb[index_] = k0 * y_wall[index_] / max_y_wall;
	  }
	  else{
	    k_turb[index_] = k0;
	  }
	  eps_turb[index_] = eps0;
	  mu_turb[index_] = mu_turb0;
	}
      }
    }
  }

  return;
}

/* RELAX_MU_TURB */
/*****************************************************************************/
/**

   Sets:

   Array (double, NxNy): mu_turb: Update value to alpha * mu_t(i) + (1 - alpha) * mu_t(i-1)

   @param data

   @return void

   MODIFIED: 24-03-2020
*/
void relax_mu_turb(Data_Mem *data){

  int I, J, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const double alpha_mu_turb = data->alpha_mu_turb;

  double *mu_turb = data->mu_turb;
  double *mu_turb_lag = data->mu_turb_lag;
 

#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(mu_turb, mu_turb_lag)
  
  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
      mu_turb[index_] = alpha_mu_turb * mu_turb[index_] +
	(1 - alpha_mu_turb) * mu_turb_lag[index_];
    }
  }

  /* Copy values to boundary */
  
#pragma omp parallel sections default(none) private(I, J, index_) shared(mu_turb)
  {
#pragma omp section 
    {
      I = 0;
      for(J=1; J<Ny-1; J++){
	index_ = J*Nx + I;
	mu_turb[index_] = mu_turb[index_+1];
      }
    }  

#pragma omp section
    {
      I = Nx - 1;
      for(J=1; J<Ny-1; J++){
	index_ = J*Nx + I;
	mu_turb[index_] = mu_turb[index_-1];
      }
    }

#pragma omp section
    {
      J = 0;
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	mu_turb[index_] = mu_turb[index_+Nx];
      }
    }

#pragma omp section 
    {
      J = Ny - 1;
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	mu_turb[index_] = mu_turb[index_-Nx];
      }
    }
  }

  copy_array(mu_turb, mu_turb_lag, Nx, Ny, Nz);  

  return;
}

/* INIT_TURB_LAG_VARS */
/*****************************************************************************/
/**

   When solving flow from tmp file sets:

   Array (double, NxNyNz): Data_Mem::mu_turb_lag, Data_Mem::P_k_lag, Data_Mem::SC_vol_eps_turb_lag, Data_Mem::SC_vol_k_turb_lag:
   values from previous iteration.
   
   Array (double, nxNynz): Data_Mem::mu_turb_at_u_nodes

   Array (double, NxnyNz): Data_Mem::mu_turb_at_v_nodes

   Array (double, NxNynz): Data_Mem::mu_turb_at_w_nodes (3D case)

   @param data

   @return void

   MODIFIED: 02-06-2020
*/
void init_turb_lag_vars(Data_Mem *data){
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  const int solve_3D_OK = data->solve_3D_OK;

  double *P_k = data->P_k;
  double *P_k_lag = data->P_k_lag;
  double *mu_turb = data->mu_turb;
  double *mu_turb_lag = data->mu_turb_lag;
  double *SC_vol_k_turb = data->SC_vol_k_turb;
  double *SC_vol_k_turb_lag = data->SC_vol_k_turb_lag;
  double *SC_vol_eps_turb = data->SC_vol_eps_turb;
  double *SC_vol_eps_turb_lag = data->SC_vol_eps_turb_lag;

  if(solve_3D_OK){
    /* Initialize mu_turb_lag */
    copy_array(mu_turb, mu_turb_lag, Nx, Ny, Nz);
    vel_at_crossed_faces_3D(data);
    compute_nabla_u_3D(data);
    compute_def_tens_3D(data);
    compute_tau_turb_3D(data);
    compute_P_k_3D(data);

    /* Initialize P_k_lag */
    copy_array(P_k, P_k_lag, Nx, Ny, Nz);

    /* Initialize SC_vol_eps_lag */
    compute_eps_SC_at_interface_3D(data);
    copy_array(SC_vol_eps_turb, SC_vol_eps_turb_lag, turb_boundary_cell_count, 1, 1);

    /* Initialize SC_vol_k_lag */
    compute_k_SC_at_interface_3D(data);
    copy_array(SC_vol_k_turb, SC_vol_k_turb_lag, turb_boundary_cell_count, 1, 1);

    /* To start calculation with mu_turb value read from tmp.dat 
       instead of the one calculated from compute_tau_turb */
    copy_array(mu_turb_lag, mu_turb, Nx, Ny, Nz);

    interp_mu_turb_to_p_nodes_in_solid(data);

    /* Initialize mu_turb at velocity nodes */
    compute_mu_turb_at_vel_nodes_3D(data);    
  }
  else{
    /* Initialize mu_turb_lag */
    copy_array(mu_turb, mu_turb_lag, Nx, Ny, Nz);
    vel_at_crossed_faces(data);
    compute_nabla_u(data);
    compute_def_tens(data);
    compute_tau_turb(data);
    compute_P_k(data);

    /* Initialize P_k_lag */
    copy_array(P_k, P_k_lag, Nx, Ny, Nz);

    /* Initialize SC_vol_eps_lag */
    compute_eps_SC_at_interface(data);
    copy_array(SC_vol_eps_turb, SC_vol_eps_turb_lag, turb_boundary_cell_count, 1, 1);

    /* Initialize SC_vol_k_lag */
    compute_k_SC_at_interface(data);
    copy_array(SC_vol_k_turb, SC_vol_k_turb_lag, turb_boundary_cell_count, 1, 1);

    /* To start calculation with mu_turb value read from tmp.dat 
       instead of the one calculated from compute_tau_turb */
    copy_array(mu_turb_lag, mu_turb, Nx, Ny, Nz);

    interp_mu_turb_to_p_nodes_in_solid(data);

    /* Initialize mu_turb at velocity nodes */
    compute_mu_turb_at_vel_nodes(data);
  }

  return;
}

/* COMPUTE_EPS_BND */
/*****************************************************************************/
/**

  Sets:

  Array (double, Data_Mem::turb_boundary_cell_count) Data_Mem::eps_bnd_x: Values of Data_Mem::eps_turb at boundary 
  for W-P || E-P interfaces

  Array (double, Data_Mem::turb_boundary_cell_count) Data_Mem::eps_bnd_y: Values of Data_Mem::eps_turb at boundary
  for S-P || N-P interfaces

  @param data
    
  @return void
    
  MODIFIED: 04-03-2019
*/
void compute_eps_bnd(Data_Mem *data){

  int count, index_;
  int cell_done_OK;

  const int Nx = data->Nx;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *dom_matrix = data->domain_matrix;
  
  double dphidyb_x, dphidxb_y;
  double dphidN;

  double sqrt_k_turb[9] = {0};
  /* [0]: (-1,Nx) [1]: (-1,0) [2]: (-1,-Nx)
     [3]: (0,Nx) [4]: (0,0) [5]: (0,-Nx)
     [6]: (1,Nx) [7]: (1,0) [8]: (1,-Nx) */
  double dphidx[3] = {0};
  double dphidy[3] = {0};
  double dphidxb[3] = {0};
  double dphidyb[3] = {0};

  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double eps_dist_tol = data->eps_dist_tol;

  double *eps_bnd_x = data->eps_bnd_x;
  double *eps_bnd_y = data->eps_bnd_y;

  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;  
  const double *k_turb = data->k_turb;
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;

  /* phi: sqrt(k) */

  for(count=0; count<turb_boundary_cell_count; count++){
      
    index_ = turb_boundary_indexs[count];
      
    cell_done_OK = 0;

    sqrt_k_turb[0] = powf(k_turb[index_-1+Nx], 0.5);
    sqrt_k_turb[1] = powf(k_turb[index_-1], 0.5);
    sqrt_k_turb[2] = powf(k_turb[index_-1-Nx], 0.5);
    sqrt_k_turb[3] = powf(k_turb[index_+Nx], 0.5);
    sqrt_k_turb[4] = powf(k_turb[index_], 0.5);
    sqrt_k_turb[5] = powf(k_turb[index_-Nx], 0.5);
    sqrt_k_turb[6] = powf(k_turb[index_+1+Nx], 0.5);
    sqrt_k_turb[7] = powf(k_turb[index_+1], 0.5);
    sqrt_k_turb[8] = powf(k_turb[index_+1-Nx], 0.5);

    dphidx[0] = 0;
    dphidx[1] = 0;
    dphidx[2] = 0;

    dphidy[0] = 0;
    dphidy[1] = 0;
    dphidy[2] = 0;

    dphidxb[0] = 0;
    dphidxb[1] = 0;
    dphidxb[2] = 0;
    
    dphidyb[0] = 0;
    dphidyb[1] = 0;
    dphidyb[2] = 0;

    /* E-P line intersects with curve */
    if((dom_matrix[index_+1]) && !cell_done_OK){
      cell_done_OK = 1;

      /* Ec 14 Sato, considering sqrt(k)_b = 0 */
      /* This value is used for calculation of dphidN later */      

      dphidxb[0] = __get_b_derivative_value_Dirichlet(eps_x[index_], eps_dist_tol,
						      0, sqrt_k_turb[4], sqrt_k_turb[1],
						      Delta_xE[index_], Delta_xW[index_], 1);
      
      /* dphidx: [0], [1], [2] */
      /* Ec 25 Sato */
      dphidx[0] = __central_b_der(eps_x[index_], dphidxb[0], sqrt_k_turb[4],
				  sqrt_k_turb[1], Delta_xE[index_], Delta_xW[index_]);

      /* Case E-N */
      if(dom_matrix[index_+Nx]){

	if(dom_matrix[index_-Nx+1]){
	  
	  /* Calculation of dphidxb at south position */	  
	  dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_-Nx], eps_dist_tol,
							  0, sqrt_k_turb[5], sqrt_k_turb[2],
							  Delta_xE[index_-Nx], Delta_xW[index_-Nx], 1);
	  
	  /* Ec 25 Sato */
	  dphidx[2] = __central_b_der(eps_x[index_-Nx], dphidxb[2], sqrt_k_turb[5],
				      sqrt_k_turb[2], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}
	else{
	  dphidx[2] = __central_der(sqrt_k_turb[8], sqrt_k_turb[5], sqrt_k_turb[2],
				    Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}
      }
      
      /* Case E-S */
      if(dom_matrix[index_-Nx]){

	if(dom_matrix[index_+Nx+1]){
	  
	  /* Calculation of dphidxb at north position */

	  dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_+Nx], eps_dist_tol,
							  0, sqrt_k_turb[3], sqrt_k_turb[0],
							  Delta_xE[index_+Nx], Delta_xW[index_-Nx], 1);
	  
	  /* Ec 25 Sato */
	  dphidx[2] = __central_b_der(eps_x[index_+Nx], dphidxb[2], sqrt_k_turb[3],
				      sqrt_k_turb[0], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}
	else{
	  dphidx[2] = __central_der(sqrt_k_turb[6], sqrt_k_turb[3], sqrt_k_turb[0],
				    Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}
      }

      /* dphidy: [0], [1], [2] */
      dphidy[0] = __central_der(sqrt_k_turb[3], sqrt_k_turb[4], sqrt_k_turb[5],
				Delta_yN[index_], Delta_yS[index_]);
      dphidy[1] = __central_der(sqrt_k_turb[0], sqrt_k_turb[1], sqrt_k_turb[2],
				Delta_yN[index_-1], Delta_yS[index_-1]);								    					 
      /* Cases E-N */
      if(dom_matrix[index_+Nx]){

	/* Ec 14 Sato */
	dphidyb[0] = __get_b_derivative_value_Dirichlet(eps_y[index_], eps_dist_tol,
							0, sqrt_k_turb[4], sqrt_k_turb[5],
							Delta_yN[index_], Delta_yS[index_], 1);
	
	/* Ec 25 Sato */
	dphidy[0] = __central_b_der(eps_y[index_], dphidyb[0], sqrt_k_turb[4],
				    sqrt_k_turb[5], Delta_yN[index_], Delta_yS[index_]);
	  
      }

      if(dom_matrix[index_+Nx-1]){

	  /* Ec 14 Sato */
	dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_-1], eps_dist_tol,
							0, sqrt_k_turb[1], sqrt_k_turb[2],
							Delta_yN[index_-1], Delta_yS[index_-1], 1);

	/* Ec 25 Sato */
	dphidy[1] = __central_b_der(eps_y[index_-1], dphidyb[1], sqrt_k_turb[1],
				    sqrt_k_turb[2], Delta_yN[index_-1], Delta_yS[index_-1]);

      }
	
      /* Cases E-S */
      if(dom_matrix[index_-Nx]){

	  /* Ec 14 Sato */
	dphidyb[0] = __get_b_derivative_value_Dirichlet(eps_y[index_], eps_dist_tol,
							0, sqrt_k_turb[4], sqrt_k_turb[3],
							Delta_yS[index_], Delta_yN[index_], -1);

	/* Ec 25 Sato */
	dphidy[0] = __central_b_der(eps_y[index_], dphidyb[0], sqrt_k_turb[3],
				    sqrt_k_turb[4], Delta_yS[index_], Delta_yN[index_]);
      }

      if(dom_matrix[index_-Nx-1]){

	  /* Ec 14 Sato */
	dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_-1], eps_dist_tol,
							0, sqrt_k_turb[1], sqrt_k_turb[0],
							Delta_yS[index_-1], Delta_yN[index_-1], -1);

	/* Ec 25 Sato */
	dphidy[1] = __central_b_der(eps_y[index_-1], dphidyb[1], sqrt_k_turb[0],
				    sqrt_k_turb[1], Delta_yS[index_-1], Delta_yN[index_-1]);
      }
    } /* end case E-P */

      /* W-P line intersects with curve */    
    if(dom_matrix[index_-1] && !cell_done_OK){
      cell_done_OK = 1;

      /* Ec 14 Sato, considering sqrt(k)_b = 0*/
      dphidxb[0] = __get_b_derivative_value_Dirichlet(eps_x[index_], eps_dist_tol,
						      0, sqrt_k_turb[4], sqrt_k_turb[7],
						      Delta_xW[index_], Delta_xE[index_], -1);

      /* dphidx: [0], [1], [2] */
      dphidx[0] = __central_b_der(eps_x[index_], dphidxb[0], sqrt_k_turb[7],
		      sqrt_k_turb[4], Delta_xW[index_], Delta_xE[index_]);
					       
      /* W-N */
      if(dom_matrix[index_+Nx]){
	
	if(dom_matrix[index_-Nx-1]){

	  dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_-Nx], eps_dist_tol,
							  0, sqrt_k_turb[5], sqrt_k_turb[8],
							  Delta_xW[index_-Nx], Delta_xE[index_-Nx], -1);
	  
	  dphidx[2] = __central_b_der(eps_x[index_-Nx], dphidxb[2], sqrt_k_turb[8],
				      sqrt_k_turb[5], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	}
	else{
	  dphidx[2] = __central_der(sqrt_k_turb[8], sqrt_k_turb[5], sqrt_k_turb[2],
				    Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}
      }
      /* W-S */
      if(dom_matrix[index_-Nx]){
	
	if(dom_matrix[index_+Nx-1]){	  

	  dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_+Nx], eps_dist_tol,
							  0, sqrt_k_turb[3], sqrt_k_turb[6],
							  Delta_xW[index_+Nx], Delta_xE[index_+Nx], -1);

	  /* Ec 25 Sato */
	  dphidx[2] = __central_b_der(eps_x[index_+Nx], dphidxb[2], sqrt_k_turb[6],
				      sqrt_k_turb[3], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	}
	else{
	  dphidx[2] = __central_der(sqrt_k_turb[6], sqrt_k_turb[3], sqrt_k_turb[0],
				    Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}
      }

      /* dphidy: [0], [1], [2] */
      dphidy[0] = __central_der(sqrt_k_turb[3], sqrt_k_turb[4], sqrt_k_turb[5],
		    Delta_yN[index_], Delta_yS[index_]);
      dphidy[1] = __central_der(sqrt_k_turb[6], sqrt_k_turb[7], sqrt_k_turb[8],
		    Delta_yN[index_+1], Delta_yS[index_+1]);

      /* W-N */
      if(dom_matrix[index_+Nx]){

	dphidyb[0] = __get_b_derivative_value_Dirichlet(eps_y[index_], eps_dist_tol,
						      0, sqrt_k_turb[4], sqrt_k_turb[5],
						      Delta_yN[index_], Delta_yS[index_], 1);

	/* Ec 25 Sato */
	dphidy[0] = __central_b_der(eps_y[index_], dphidyb[0], sqrt_k_turb[4],
				    sqrt_k_turb[5], Delta_yN[index_], Delta_yS[index_]);
      }     

      if(dom_matrix[index_+Nx+1]){

	dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_+1], eps_dist_tol,
							0, sqrt_k_turb[7], sqrt_k_turb[8],
							Delta_yN[index_+1], Delta_yS[index_+1], 1);

	/* Ec 25 Sato */
	dphidy[1] = __central_b_der(eps_y[index_+1], dphidyb[1], sqrt_k_turb[7],
				    sqrt_k_turb[8], Delta_yN[index_+1], Delta_yS[index_+1]);
      }
	
      /* W-S */
      if(dom_matrix[index_-Nx]){

	dphidyb[0] = __get_b_derivative_value_Dirichlet(eps_y[index_], eps_dist_tol,
							0, sqrt_k_turb[4], sqrt_k_turb[3],
							Delta_yS[index_], Delta_yN[index_], -1);

	/* Ec 25 Sato */
	dphidy[0] = __central_b_der(eps_y[index_], dphidyb[0], sqrt_k_turb[3],
				    sqrt_k_turb[4], Delta_yS[index_], Delta_yN[index_]);
      }

      if(dom_matrix[index_-Nx+1]){

	dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_+1], eps_dist_tol,
							0, sqrt_k_turb[7], sqrt_k_turb[6],
							Delta_yS[index_+1], Delta_yN[index_+1], -1);

	/* Ec 25 Sato */
	dphidy[1] = __central_b_der(eps_y[index_+1], dphidyb[1], sqrt_k_turb[6],
				    sqrt_k_turb[7], Delta_yS[index_+1], Delta_yN[index_+1]);
      }

    } /* end case W-P */

      /* N-P line intersects with curve */
    if(dom_matrix[index_+Nx] && !cell_done_OK){
      cell_done_OK = 1;
      
      dphidyb[0] = __get_b_derivative_value_Dirichlet(eps_y[index_], eps_dist_tol,
					 0, sqrt_k_turb[4], sqrt_k_turb[5],
					 Delta_yN[index_], Delta_yS[index_], 1);
      
      /* dphidy: [0], [1], [2] */
      /* Ec 25 Sato */
      dphidy[0] = __central_b_der(eps_y[index_], dphidyb[0], sqrt_k_turb[4],
		      sqrt_k_turb[5], Delta_yN[index_], Delta_yS[index_]);
      
      /* Case N-E */
      if(dom_matrix[index_+1]){
	
	if(dom_matrix[index_+Nx-1]){

	  dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_-1], eps_dist_tol,
					 0, sqrt_k_turb[1], sqrt_k_turb[2],
					 Delta_yN[index_-1], Delta_yS[index_-1], 1);
    
	  dphidy[1] = __central_b_der(eps_y[index_-1], dphidyb[1], sqrt_k_turb[1],
		      sqrt_k_turb[2], Delta_yN[index_-1], Delta_yS[index_-1]);
	  
	}
	else{
	  dphidy[1] = __central_der(sqrt_k_turb[0], sqrt_k_turb[1], sqrt_k_turb[2],
			Delta_yN[index_-1], Delta_yS[index_-1]);
	}
      }

      /* Case N-W */
      if(dom_matrix[index_-1]){

	if(dom_matrix[index_+Nx+1]){

	  dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_+1], eps_dist_tol,
					 0, sqrt_k_turb[7], sqrt_k_turb[8],
					 Delta_yN[index_+1], Delta_yS[index_+1], 1);
	  
	  dphidy[1] = __central_b_der(eps_y[index_+1], dphidyb[1], sqrt_k_turb[7],
		      sqrt_k_turb[8], Delta_yN[index_+1], Delta_yS[index_+1]);	  
	}
	else{
	  dphidy[1] = __central_der(sqrt_k_turb[6], sqrt_k_turb[7], sqrt_k_turb[8],
			Delta_yN[index_+1], Delta_yS[index_+1]);
	}
      }

      /* dphidx: [0], [1], [2] */
      dphidx[0] = __central_der(sqrt_k_turb[7], sqrt_k_turb[4], sqrt_k_turb[1],
			Delta_xE[index_], Delta_xW[index_]);
      dphidx[2] = __central_der(sqrt_k_turb[8], sqrt_k_turb[5], sqrt_k_turb[2],
			Delta_xE[index_-Nx], Delta_xW[index_-Nx]);

      /* Cases N-E */
      if(dom_matrix[index_+1]){

	dphidxb[0] = __get_b_derivative_value_Dirichlet(eps_x[index_], eps_dist_tol,
					 0, sqrt_k_turb[4], sqrt_k_turb[1],
					 Delta_xE[index_], Delta_xW[index_], 1);

	dphidx[0] = __central_b_der(eps_x[index_], dphidxb[0], sqrt_k_turb[4],
		      sqrt_k_turb[1], Delta_xE[index_], Delta_xW[index_]);
      }
      
      if(dom_matrix[index_-Nx+1]){	

	dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_-Nx], eps_dist_tol,
					 0, sqrt_k_turb[5], sqrt_k_turb[2],
					 Delta_xE[index_-Nx], Delta_xW[index_-Nx], 1);
	
	dphidx[2] = __central_b_der(eps_x[index_-Nx], dphidxb[2], sqrt_k_turb[5],
		      sqrt_k_turb[2], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      }

      /* Cases N-W */
      if(dom_matrix[index_-1]){

	dphidxb[0] = __get_b_derivative_value_Dirichlet(eps_x[index_], eps_dist_tol,
					 0, sqrt_k_turb[4], sqrt_k_turb[7],
					 Delta_xW[index_], Delta_xE[index_], -1);
	
	dphidx[0] = __central_b_der(eps_x[index_], dphidxb[0], sqrt_k_turb[7],
		      sqrt_k_turb[4], Delta_xW[index_], Delta_xE[index_]);   
      }
      
      if(dom_matrix[index_-Nx-1]){

	dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_-Nx], eps_dist_tol,
					 0, sqrt_k_turb[5], sqrt_k_turb[8],
					 Delta_xW[index_-Nx], Delta_xE[index_-Nx], -1);
	
	dphidx[2] = __central_b_der(eps_x[index_-Nx], dphidxb[2], sqrt_k_turb[8],
		      sqrt_k_turb[5], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
      }
    } /* end case N-P */

      /* S-P line intersects with curve */
    if(dom_matrix[index_-Nx] && !cell_done_OK){
      cell_done_OK = 1;

      dphidyb[0] = __get_b_derivative_value_Dirichlet(eps_y[index_], eps_dist_tol,
					 0, sqrt_k_turb[4], sqrt_k_turb[3],
					 Delta_yS[index_], Delta_yN[index_], -1);
      
      /* dphidy: [0], [1], [2] */
      dphidy[0] = __central_b_der(eps_y[index_], dphidyb[0], sqrt_k_turb[3],
		      sqrt_k_turb[4], Delta_yS[index_], Delta_yN[index_]);

      /* Case S-E */
      if(dom_matrix[index_+1]){
	
	if(dom_matrix[index_-Nx-1]){

	  dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_-1], eps_dist_tol,
					 0, sqrt_k_turb[1], sqrt_k_turb[0],
					 Delta_yS[index_-1], Delta_yN[index_-1], -1);

	  dphidy[1] = __central_b_der(eps_y[index_-1], dphidyb[1], sqrt_k_turb[0],
		      sqrt_k_turb[1], Delta_yS[index_-1], Delta_yN[index_-1]);
	}
	else{
	  dphidy[1] = __central_der(sqrt_k_turb[0], sqrt_k_turb[1], sqrt_k_turb[2],
			Delta_yN[index_-1], Delta_yS[index_-1]);
	}
      }

      /* Case S-W */
      if(dom_matrix[index_-1]){
	
	if(dom_matrix[index_-Nx+1]){

	  dphidyb[1] = __get_b_derivative_value_Dirichlet(eps_y[index_+1], eps_dist_tol,
					 0, sqrt_k_turb[7], sqrt_k_turb[6],
					 Delta_yS[index_+1], Delta_yN[index_+1], -1);
	  
	  dphidy[1] = __central_b_der(eps_y[index_+1], dphidyb[1], sqrt_k_turb[6],
		      sqrt_k_turb[7], Delta_yS[index_+1], Delta_yN[index_+1]);
	}
	else{
	  dphidy[1] = __central_der(sqrt_k_turb[6], sqrt_k_turb[7], sqrt_k_turb[8],
			Delta_yN[index_+1], Delta_yS[index_+1]);
	}
      }

      /* dphidx: [0], [1], [2] */
      dphidx[0] = __central_der(sqrt_k_turb[7], sqrt_k_turb[4], sqrt_k_turb[1],
			Delta_xE[index_], Delta_xW[index_]);
      dphidx[2] = __central_der(sqrt_k_turb[6], sqrt_k_turb[3], sqrt_k_turb[0],
			Delta_xE[index_+Nx], Delta_xW[index_+Nx]);

      /* Cases S-E */
      if(dom_matrix[index_+1]){

	dphidxb[0] = __get_b_derivative_value_Dirichlet(eps_x[index_], eps_dist_tol,
					 0, sqrt_k_turb[4], sqrt_k_turb[1],
					 Delta_xE[index_], Delta_xW[index_], 1);
	
	dphidxb[0] = __central_b_der(eps_x[index_], dphidxb[0], sqrt_k_turb[4],
		      sqrt_k_turb[1], Delta_xE[index_], Delta_xW[index_]); 
      }
      
      if(dom_matrix[index_+Nx+1]){

	dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_+Nx], eps_dist_tol,
					 0, sqrt_k_turb[3], sqrt_k_turb[0],
					 Delta_xE[index_+Nx], Delta_xW[index_+Nx], 1);

	dphidx[2] = __central_b_der(eps_x[index_+Nx], dphidxb[2], sqrt_k_turb[3],
		      sqrt_k_turb[0], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      }

      /* Cases S-W */
      if(dom_matrix[index_-1]){

	dphidxb[0] = __get_b_derivative_value_Dirichlet(eps_x[index_], eps_dist_tol,
					 0, sqrt_k_turb[4], sqrt_k_turb[7],
					 Delta_xW[index_], Delta_xE[index_], -1);

	dphidx[0] = __central_b_der(eps_x[index_], dphidxb[0], sqrt_k_turb[7],
		      sqrt_k_turb[4], Delta_xW[index_], Delta_xE[index_]);
      }
      if(dom_matrix[index_+Nx-1]){

	dphidxb[2] = __get_b_derivative_value_Dirichlet(eps_x[index_-Nx], eps_dist_tol,
					 0, sqrt_k_turb[3], sqrt_k_turb[6],
					 Delta_xW[index_+Nx], Delta_xE[index_+Nx], -1);
	
	dphidx[2] = __central_b_der(eps_x[index_-Nx], dphidxb[2], sqrt_k_turb[6],
		      sqrt_k_turb[3], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
      }
    } /* end case S-P */


    if(dom_matrix[index_-1] || dom_matrix[index_+1]){
      if((dom_matrix[index_-Nx] == 0) && (dom_matrix[index_+Nx] == 0)){
	/* Case W || Case E */
	/* Ec 28 Sato */
	if(dom_matrix[index_-1]){
	  dphidyb[0] = __linear_extrap_to_b(dphidy[0], dphidy[1], eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	}
	else{
	  dphidyb[0] = __linear_extrap_to_b(dphidy[0], dphidy[1], eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	}

	dphidN = dphidxb[0] * norm_vec_x_x[index_] + dphidyb[0] * norm_vec_x_y[index_];
	eps_bnd_x[count] = 2 * mu * (dphidN * dphidN) / rho;

      }
      else{
	/* Combined cases
	   Estimation of eps_b along the y direction */
	/* Ec 28 Sato */
	if(dom_matrix[index_-Nx]){
	  dphidxb_y = __linear_extrap_to_b(dphidx[0], dphidx[2], eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	}
	else{
	  dphidxb_y = __linear_extrap_to_b(dphidx[0], dphidx[2], eps_y[index_], Delta_yN[index_], Delta_yS[index_]);	  
	}

	dphidN = dphidxb_y * norm_vec_y_x[index_] + dphidyb[0] * norm_vec_y_y[index_];
	eps_bnd_y[count] = 2 * mu * (dphidN * dphidN) / rho;
	/* Estimation of eps_b along the x direction */
	/* Ec 28 Sato */
	if(dom_matrix[index_-1]){
	  dphidyb_x = __linear_extrap_to_b(dphidy[0], dphidy[1], eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	}
	else{
	  dphidyb_x = __linear_extrap_to_b(dphidy[0], dphidy[1], eps_x[index_], Delta_xE[index_], Delta_xW[index_]);	  
	}

	dphidN = dphidxb[0] * norm_vec_x_x[index_] + dphidyb_x * norm_vec_x_y[index_];
	eps_bnd_x[count] = 2 * mu * (dphidN * dphidN) / rho;
      }
    }
    else{
      // Case S || Case N
      /* Ec 28 Sato */
      if(dom_matrix[index_-Nx]){
	dphidxb[0] = __linear_extrap_to_b(dphidx[0], dphidx[2], eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
      }
      else{
	dphidxb[0] = __linear_extrap_to_b(dphidx[0], dphidx[2], eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
      }

      dphidN = dphidxb[0] * norm_vec_y_x[index_] + dphidyb[0] * norm_vec_y_y[index_];
      eps_bnd_y[count] = 2 * mu * (dphidN * dphidN) / rho;
    }


  } /* end for(count=0; count<boundary_cell_count; count++) */
 
  return;
  
}

/* FILTER_Y_PLUS */
/*****************************************************************************/
/**

  Sets:

  Array (double, turb_boundary_cell_count) y_plus: Value of y_plus for
  nodes next to boundary.

  Array (int, turb_boundary_cell_count) turb_bnd_excl_indexs: 1 if node
  is excluded, the criteria for exclusion is y_plus < y_plus_limit.
 
  @param data

  @return ret_value not implemented.
  
  MODIFIED: 09-02-2020
*/
void filter_y_plus(Data_Mem *data){

  int index_, count;
  int I, J, i, j, index_u, index_v;

  const int Nx = data->Nx;
  const int nx = data->nx;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;

  int *turb_bnd_excl_indexs = data->turb_bnd_excl_indexs;

  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;  

  double U, V;
  double tau_wall, fric_vel;
  
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double y_plus_limit = data->y_plus_limit;

  double *y_plus = data->y_plus;

  const double *y_wall = data->y_wall;
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;

  for(count=0; count<turb_boundary_cell_count; count++){

    index_ = turb_boundary_indexs[count];

    if(y_wall[index_] != 0){

      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      i = I - 1;
      j = J - 1;
      index_u = J*nx + i;
      index_v = j*Nx + I;
    
      U = 0.5 * (u[index_u+1] + u[index_u]);
      V = 0.5 * (v[index_v+Nx] + v[index_v]);
    
      tau_wall = mu * powf(U*U + V*V, 0.5) / y_wall[index_];
      fric_vel = powf(tau_wall / rho, 0.5);
      y_plus[count] = fric_vel * y_wall[index_] * rho / mu;

      if(y_plus[count] < y_plus_limit){
	turb_bnd_excl_indexs[count] = 1;
      }
      else{
	turb_bnd_excl_indexs[count] = 0;
      }
    }
    else{
      y_plus[count] = 0;
      turb_bnd_excl_indexs[count] = 1;
    }
  }

  return;
}

/* COMPUTE_MU_TURB_AT_VEL_NODES */
/*****************************************************************************/
/**

  Sets:

  Array (double, nxNy) Data_Mem::mu_turb_at_u_nodes

  Array (double, Nxny) Data_Mem::mu_turb_at_v_nodes

  The values are intepolated at every node.
 
  @param data

  @return ret_value not implemented.
  
  MODIFIED: 24-04-2020
*/
void compute_mu_turb_at_vel_nodes(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;

  const double *mu_turb = data->mu_turb;
  const double *fe = data->fe;
  const double *fn = data->fn;

#pragma omp parallel for default(none) private(I, i, J, index_u, index_) \
  shared(mu_turb, mu_turb_at_u_nodes, fe)

  for(J=0; J<Ny; J++){
    for(i=0; i<nx; i++){
      I = i + 1;
      index_u = J*nx + i;
      index_ = J*Nx + I;
      mu_turb_at_u_nodes[index_u] = mu_turb[index_-1] * fe[index_-1] + 
	mu_turb[index_] * (1 - fe[index_-1]);
    }
  }

#pragma omp parallel for default(none) private(I, J, j, index_v, index_) \
  shared(mu_turb_at_v_nodes, mu_turb, fn)

  for(j=0; j<ny; j++){
    for(I=0; I<Nx; I++){
      J = j + 1;
      index_v = j*Nx + I;
      index_ = J*Nx + I;
      mu_turb_at_v_nodes[index_v] = mu_turb[index_-Nx] * fn[index_-Nx] +
	mu_turb[index_] * (1 - fn[index_-Nx]);
    }
  }

  return;
}

/* INTERP_MU_TURB_TO_P_NODES_IN_SOLID */
/*****************************************************************************/
/**

  Sets:

  Array (double, nxNy) Data_Mem::mu_turb: Interpolates values to positions indicated on 
  array Data_Mem::p_nodes_in_solid_index.
 
  @param data

  @return ret_value not implemented.
  
  MODIFIED: 24-04-2020
*/
void interp_mu_turb_to_p_nodes_in_solid(Data_Mem *data){

  int z, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int p_nodes_in_solid_count = data->p_nodes_in_solid_count;
  const int solve_3D_OK = data->solve_3D_OK;

  int *p_nodes_in_solid_indx = data->p_nodes_in_solid_indx;
  
  double *mu_turb = data->mu_turb;

  if(solve_3D_OK){
    for(z=0; z<p_nodes_in_solid_count; z++){
      index_ = p_nodes_in_solid_indx[z];
      mu_turb[index_] = 0.1666 * (mu_turb[index_-1] + mu_turb[index_+1] + 
				  mu_turb[index_-Nx] + mu_turb[index_+Nx] +
				  mu_turb[index_-Nx*Ny] + mu_turb[index_+Nx*Ny]);
    }    
  }
  else{
    for(z=0; z<p_nodes_in_solid_count; z++){
      index_ = p_nodes_in_solid_indx[z];
      mu_turb[index_] = 0.25 * (mu_turb[index_-1] + mu_turb[index_+1] + 
				mu_turb[index_-Nx] + mu_turb[index_+Nx]);
    }
  }

  return;
}

/* COMPUTE_TURB_AUX_FIELDS */
/*****************************************************************************/
/**

  Sets:

  Array (double, NxNy): P_k production of turbulent kinetic energy.

  Array (double, NxNy): mu_turb turbulent viscosity.
 
  @param data

  @return ret_value not implemented.
  
  MODIFIED: 24-04-2020
*/
void compute_turb_aux_fields(Data_Mem *data){

  /* Compute P_k, mu_turb 
     See description in compute_turb_prop function definition
  */
  compute_turb_prop(data);

  /* Interpolates mu_turb values to p_nodes_in_solid nodes */ 
  interp_mu_turb_to_p_nodes_in_solid(data);
	
  /* Relax mu_turb using mu_turb_lag */
  relax_mu_turb(data);

  return;
}

/* TURB_BOUNDARY_NODES_3D */
/*****************************************************************************/
/**

  Sets:

  Array (int, turb_boundary_cell_count) turb_boundary_indexs: domain_matrix 
  fluid cells that have a non fluid neighbour.

  @param data
    
  @return 0 (ret_value not fully implemented).
    
  MODIFIED: 19-03-2020
*/
int turb_boundary_nodes_3D(Data_Mem *data){

  int I, J, K, index_;
  int turb_boundary_cell_count = 0;
  int cell_OK = 0;
  int ret_value = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *dom_matrix = data->domain_matrix;
  
#pragma omp parallel for default(none) private(I, J, K, index_, cell_OK) \
  shared(dom_matrix) reduction(+:turb_boundary_cell_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
        
	index_ = K*Nx*Ny + J*Nx + I;

	cell_OK = 
	  (dom_matrix[index_+1] != 0) || (dom_matrix[index_-1] != 0) ||
	  (dom_matrix[index_+Nx] != 0) || (dom_matrix[index_-Nx] != 0) ||
	  (dom_matrix[index_+Nx*Ny] != 0) || (dom_matrix[index_-Nx*Ny] != 0) ;

	if((dom_matrix[index_] == 0) && cell_OK){
	  turb_boundary_cell_count++;
	}
      }
    }
  }
  
  data->turb_boundary_indexs = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_I = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_J = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_K = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->depsdxb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->depsdyb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->depsdzb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_k_turb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_eps_turb = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_k_turb_lag = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->SC_vol_eps_turb_lag = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->turb_boundary_cell_count = turb_boundary_cell_count;
  data->eps_bnd_x = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->eps_bnd_y = create_array(turb_boundary_cell_count, 1, 1, 0);
  data->eps_bnd_z = create_array(turb_boundary_cell_count, 1, 1, 0);
  
  data->turb_bnd_excl_indexs = create_array_int(turb_boundary_cell_count, 1, 1, 0);
  data->y_plus = create_array(turb_boundary_cell_count, 1, 1, 0);

  turb_boundary_cell_count = 0;

  int *turb_boundary_indexs = data->turb_boundary_indexs;
  int *turb_boundary_I = data->turb_boundary_I;
  int *turb_boundary_J = data->turb_boundary_J;
  int *turb_boundary_K = data->turb_boundary_K;
  
#pragma omp parallel for default(none) private(I, J, K, index_, cell_OK) \
  shared(dom_matrix, turb_boundary_indexs, turb_boundary_I, turb_boundary_J, \
	 turb_boundary_K, turb_boundary_cell_count) 

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
        
	index_ = K*Nx*Ny + J*Nx + I;

	cell_OK = 
	  (dom_matrix[index_+1] != 0) || (dom_matrix[index_-1] != 0) ||
	  (dom_matrix[index_+Nx] != 0) || (dom_matrix[index_-Nx] != 0) ||
	  (dom_matrix[index_+Nx*Ny] != 0) || (dom_matrix[index_-Nx*Ny] != 0);

	if((dom_matrix[index_] == 0) && cell_OK){
#pragma omp critical
	  {
	    turb_boundary_indexs[turb_boundary_cell_count] = index_;
	    turb_boundary_I[turb_boundary_cell_count] = I;
	    turb_boundary_J[turb_boundary_cell_count] = J;
	    turb_boundary_K[turb_boundary_cell_count] = K;
	    turb_boundary_cell_count++;
	  }
	}
      }   
    }
  }
 
  return ret_value;
}

/* FILTER_Y_PLUS_3D */
/*****************************************************************************/
/**

  Sets:

  Array (double, turb_boundary_cell_count) y_plus: Value of y_plus for
  nodes next to boundary.

  Array (int, turb_boundary_cell_count) turb_bnd_excl_indexs: 1 if node
  is excluded, the criteria for exclusion is y_plus < y_plus_limit.
 
  @param data

  @return ret_value not implemented.
  
  MODIFIED: 09-02-2020
*/
void filter_y_plus_3D(Data_Mem *data){

  int index_, count;
  int I, J, K;
  int i, j, k;
  int index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;

  int *turb_bnd_excl_indexs = data->turb_bnd_excl_indexs;

  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *turb_boundary_K = data->turb_boundary_K;

  double U, V, W;
  double tau_wall, fric_vel;
  
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double y_plus_limit = data->y_plus_limit;

  double *y_plus = data->y_plus;

  const double *y_wall = data->y_wall;
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;

  for(count=0; count<turb_boundary_cell_count; count++){

    index_ = turb_boundary_indexs[count];

    if(y_wall[index_] != 0){

      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      K = turb_boundary_K[count];
      i = I - 1;
      j = J - 1;
      k = K - 1;
      index_u = K*nx*Ny + J*nx + i;
      index_v = K*Nx*ny + j*Nx + I;
      index_w = k*Nx*Ny + J*Nx + I;
    
      U = 0.5 * (u[index_u+1] + u[index_u]);
      V = 0.5 * (v[index_v+Nx] + v[index_v]);
      W = 0.5 * (w[index_w+Nx*Ny] + w[index_w]);
    
      tau_wall = mu * powf(U*U + V*V + W*W, 0.5) / y_wall[index_];
      fric_vel = powf(tau_wall / rho, 0.5);
      y_plus[count] = fric_vel * y_wall[index_] * rho / mu;

      if(y_plus[count] < y_plus_limit){
	turb_bnd_excl_indexs[count] = 1;
      }
      else{
	turb_bnd_excl_indexs[count] = 0;
      }
    }
    else{
      y_plus[count] = 0;
      turb_bnd_excl_indexs[count] = 1;
    }
  }

  return;
}

/* COMPUTE_TURB_AUX_FIELDS_3D */
/*****************************************************************************/
/**

  Sets:

  Array (double, NxNyNz): P_k production of turbulent kinetic energy.

  Array (double, NxNyNz): mu_turb turbulent viscosity.
 
  @param data

  @return ret_value not implemented.
  
  MODIFIED: 24-04-2020
*/
void compute_turb_aux_fields_3D(Data_Mem *data){

  /* Compute P_k, mu_turb 
     See description in compute_turb_prop function definition
  */
  compute_turb_prop_3D(data);

  /* Interpolates mu_turb values to p_nodes_in_solid nodes */ 
  interp_mu_turb_to_p_nodes_in_solid(data);
	
  /* Relax mu_turb using mu_turb_lag */
  relax_mu_turb_3D(data);

  return;
}

/* RELAX_MU_TURB_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, NxNyNz): mu_turb: Update value to alpha * mu_t(i) + (1 - alpha) * mu_t(i-1)

   @param data

   @return void

   MODIFIED: 24-03-2020
*/
void relax_mu_turb_3D(Data_Mem *data){

  int I, J, K, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const double alpha_mu_turb = data->alpha_mu_turb;

  double *mu_turb = data->mu_turb;
  double *mu_turb_lag = data->mu_turb_lag;
 

#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(mu_turb, mu_turb_lag)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;
	mu_turb[index_] = alpha_mu_turb * mu_turb[index_] +
	  (1 - alpha_mu_turb) * mu_turb_lag[index_];
      }
    }
  }

  /* Copy values to boundary */
  
#pragma omp parallel sections default(none) private(I, J, K, index_) shared(mu_turb)
  {
#pragma omp section 
    {
      /* West */
      I = 0;
      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  mu_turb[index_] = mu_turb[index_+1];
	}
      }
    }  

#pragma omp section
    {
      /* East */
      I = Nx - 1;
      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  mu_turb[index_] = mu_turb[index_-1];
	}
      }
    }

#pragma omp section
    {
      /* South */
      J = 0;
      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  mu_turb[index_] = mu_turb[index_+Nx];
	}
      }
    }

#pragma omp section 
    {
      /* North */
      J = Ny - 1;
      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  mu_turb[index_] = mu_turb[index_-Nx];
	}
      }
    }

#pragma omp section
    {
      /* Bottom */
      K = 0;
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  mu_turb[index_] = mu_turb[index_+Nx*Ny];
	}
      }
    }

#pragma omp section
    {
      /* Top */
      K = Nz - 1;
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  mu_turb[index_] = mu_turb[index_-Nx*Ny];
	}
      }
    }
    
  } /* sections */

  copy_array(mu_turb, mu_turb_lag, Nx, Ny, Nz);  

  return;
}

/* SET_D_K_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, nxNyNz): Dk_x: Diffusive parameter for k equation at x_faces.

   Array (double, NxnyNz): Dk_y: Diffusive parameter for k equation at y_faces.

   Array (double, NxNynz): Dk_z: Diffusive parameter for k equation at z_faces.

   @param data

   @return void

   MODIFIED: 20-03-2020
*/
void set_D_k_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_u, index_v, index_w, index_;

  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  double mu_turb_face;
  double mu_eff_0, mu_eff_1;

  const double C_sigma_k = data->C_sigma_k;
  const double mu = data->mu;

  double *Dk_y = data->Dk_y;
  double *Dk_x = data->Dk_x;
  double *Dk_z = data->Dk_z;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;
  const double *mu_turb = data->mu_turb;
  const double *fw = data->fw;
  const double *fs = data->fs;
  const double *fb = data->fb;
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;

  /* Dk_x */
  
#pragma omp parallel for default(none)					\
  private(I, J, K, i, index_u, index_, mu_eff_0, mu_eff_1, mu_turb_face)	\
  shared(mu_turb, Delta_xW, faces_x, Dk_x, fw)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	I = i + 1;
	index_u = K*nx*Ny + J*nx + i;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_eff_0 = mu + mu_turb[index_-1] / C_sigma_k;
	mu_eff_1 = mu + mu_turb[index_] / C_sigma_k;
	mu_turb_face = (mu_eff_0 * mu_eff_1) /
	  (mu_eff_1 * fw[index_] + mu_eff_0 * (1 - fw[index_]));
	Dk_x[index_u] = faces_x[index_u] * mu_turb_face /
	  Delta_xW[index_];
      }
    }
  }

  /* Dk_y */
#pragma omp parallel for default(none)					\
  private(I, J, K, j, index_v, index_, mu_eff_0, mu_eff_1, mu_turb_face)	\
  shared(mu_turb, Delta_yS, faces_y, Dk_y, fs)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
      
	J = j + 1;
	index_v = K*Nx*ny + j*Nx + I;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_eff_0 = mu + mu_turb[index_-Nx] / C_sigma_k;
	mu_eff_1 = mu + mu_turb[index_] /C_sigma_k;
	mu_turb_face = (mu_eff_0 * mu_eff_1) /
	  ( mu_eff_1 * fs[index_] + mu_eff_0 * (1 - fs[index_]));
	Dk_y[index_v] = faces_y[index_v] * mu_turb_face /
	  Delta_yS[index_];
      }
    }
  }

  /* Dk_z */
#pragma omp parallel for default(none)		\
  private(I, J, K, k, index_w, index_, mu_eff_0, mu_eff_1, mu_turb_face) \
  shared(mu_turb, Delta_zB, faces_z, Dk_z, fb)

  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	K = k + 1;
	index_w = k*Nx*Ny + J*Nx + I;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_eff_0 = mu + mu_turb[index_-Nx*Ny] / C_sigma_k;
	mu_eff_1 = mu + mu_turb[index_] / C_sigma_k;
	mu_turb_face = (mu_eff_0 + mu_eff_1) /
	  (mu_eff_1 * fb[index_] + mu_eff_0 * (1 - fb[index_]));
	Dk_z[index_w] = faces_z[index_w] * mu_turb_face /
	  Delta_zB[index_];
      }
    }
  }
  
  return;
}

/* SET_F_K_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, nxNyNz): Fk_x: Convective parameter for k equation at x_faces.

   Array (double, NxnyNz): Fk_y: Convective parameter for k equation at y_faces.

   Array (double, NxNynz): Fk_z: Convective parameter for k equation at z_faces.

   @param data

   @return void

   MODIFIED: 20-03-2020
*/
void set_F_k_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_u, index_v, index_w;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const double rho = data->rho_v[0];

  double *Fk_x = data->Fk_x;
  double *Fk_y = data->Fk_y;
  double *Fk_z = data->Fk_z;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;
  

  /* Fk_x */
#pragma omp parallel for default(none) private(i, J, K, index_u) shared(u, faces_x, Fk_x)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	index_u = K*nx*Ny + J*nx + i;
	Fk_x[index_u] = u[index_u] * faces_x[index_u] * rho;
      }
    }
  }

  /* Fk_y */
#pragma omp parallel for default(none) private(I, j, K, index_v) shared(v, faces_y, Fk_y)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
	
	index_v = K*Nx*ny + j*Nx + I;
	Fk_y[index_v] = v[index_v] * faces_y[index_v] * rho;
      }
    }
  }

  /* F_k_z */
#pragma omp parallel for default(none) private(I, J, k, index_w) shared(w, faces_z, Fk_z)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_w = k*Nx*Ny + J*Nx + I;
	Fk_z[index_w] = w[index_w] * faces_z[index_w] * rho;
      }
    }
  }
  
  return;
}

/* SET_D_EPS_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, nxNyNz): Deps_x: Diffusive parameter for epsilon equation at x_faces.

   Array (double, NxnyNz): Deps_y: Diffusive parameter for epsilon equation at y_faces.

   Array (double, NxNynz): Deps_z: Diffusive parameter for epsilon equation at z_faces.

   @param data

   @return void

   MODIFIED: 21-03-2020
*/
void set_D_eps_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_u, index_v, index_w, index_;

  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  double mu_eff_0, mu_eff_1;
  double mu_turb_face;

  const double C_sigma_eps = data->C_sigma_eps;
  const double mu = data->mu;

  double *Deps_x = data->Deps_x;
  double *Deps_y = data->Deps_y;
  double *Deps_z = data->Deps_z;

  const double *mu_turb = data->mu_turb;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;
  const double *fw = data->fw;
  const double *fs = data->fs;
  const double *fb = data->fb;
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;

  /* Deps_x */
#pragma omp parallel for default(none)					\
  private(I, J, K, i, index_u, index_, mu_eff_0, mu_eff_1, mu_turb_face)	\
  shared(mu_turb, Delta_xW, faces_x, Deps_x, fw)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
      
	I = i + 1;
	index_u = K*nx*Ny + J*nx + i;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_eff_0 = mu + mu_turb[index_-1] / C_sigma_eps;
	mu_eff_1 = mu + mu_turb[index_] / C_sigma_eps;
	mu_turb_face = (mu_eff_0 * mu_eff_1) /
	  (mu_eff_1 * fw[index_] + mu_eff_0 * (1 - fw[index_]));
	Deps_x[index_u] = faces_x[index_u] * mu_turb_face /
	  Delta_xW[index_];
      }
    }
  }
  
  /* Deps_y */
#pragma omp parallel for default(none)					\
  private(I, J, K, j, index_v, index_, mu_eff_0, mu_eff_1, mu_turb_face)	\
  shared(mu_turb, Delta_yS, faces_y, Deps_y, fs)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
      
	J = j + 1;
	index_v = K*Nx*ny + j*Nx + I;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_eff_0 = mu + mu_turb[index_-Nx] / C_sigma_eps;
	mu_eff_1 = mu + mu_turb[index_] / C_sigma_eps;
	mu_turb_face = (mu_eff_0 * mu_eff_1) /
	  (mu_eff_1 * fs[index_] + mu_eff_0 * (1 - fs[index_]));
	Deps_y[index_v] = faces_y[index_v] * mu_turb_face /
	  Delta_yS[index_];
      }
    }
  }

  /* Deps_z */
#pragma omp parallel for default(none)		\
  private(I, J, K, k, index_w, index_, mu_eff_0, mu_eff_1, mu_turb_face) \
  shared(mu_turb, Delta_zB, faces_z, Deps_z, fb)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	K = k + 1;
	index_w = k*Nx*Ny + J*Nx + I;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_eff_0 = mu + mu_turb[index_-Nx*Ny] / C_sigma_eps;
	mu_eff_1 = mu + mu_turb[index_] / C_sigma_eps;
	mu_turb_face = (mu_eff_0 * mu_eff_1) /
	  (mu_eff_1 * fb[index_] + mu_eff_0 * (1 - fb[index_]));
	Deps_z[index_w] = faces_z[index_w] * mu_turb_face /
	  Delta_zB[index_];
      }
    }
  }
  
  return;
}

/* SET_F_EPS_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, nxNyNz): Feps_x: Convective parameter for epsislon equation at x_faces.

   Array (double, NxnyNz): Feps_y: Convective parameter for epsilon equation at y_faces.

   Array (double, NxNynz): Feps_z: Convective parameter for epsilon equation at z_faces.

   @param data

   @return void

   MODIFIED: 21-03-2020
*/
void set_F_eps_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_u, index_v, index_w;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const double rho = data->rho_v[0];

  double *Feps_x = data->Feps_x;
  double *Feps_y = data->Feps_y;
  double *Feps_z = data->Feps_z;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;

  /* Feps_x */
#pragma omp parallel for default(none) private(i, J, K, index_u) shared(u, faces_x, Feps_x)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	index_u = K*nx*Ny + J*nx + i;
	Feps_x[index_u] = u[index_u] * faces_x[index_u] * rho;
      }
    }
  }

  /* Feps_y */
#pragma omp parallel for default(none) private(I, j, K, index_v) shared(v, faces_y, Feps_y)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
      
	index_v = K*Nx*ny + j*Nx + I;
	Feps_y[index_v] = v[index_v] * faces_y[index_v] * rho;
      }
    }
  }

  /* Feps_z */
#pragma omp parallel for default(none) private(I, J, k, index_w) shared(w, faces_z, Feps_z)

  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_w = k*Nx*Ny + J*Nx + I;
	Feps_z[index_w] = w[index_w] * faces_z[index_w] * rho;
      }
    }
  }

  return;
}

/* SET_COEFFS_K_3D */
/*****************************************************************************/
/**

   Sets:

   Strucuture coeffs_k: coefficients of k equation for internal nodes.

   @param data

   @return void
 */
void set_coeffs_k_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_, index_u, index_v, index_w;
  int count;

  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int turb_boundary_cell_count =
    data->turb_boundary_cell_count;
  const int flow_steady_state_OK = data->flow_steady_state_OK;

  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_bnd_excl_indexs = data->turb_bnd_excl_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *turb_boundary_K = data->turb_boundary_K;
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;

  double Dw, De, Dn, Ds, Dt, Db;
  double Fw, Fe, Fn, Fs, Ft, Fb;
  double aP0, Da, m;

  const double rho = data->rho_v[0];
  const double alpha_SC_k = data->alpha_SC_k;
  const double dt = data->dt[3];

  double *k_aP = data->coeffs_k.aP;
  double *k_aW = data->coeffs_k.aW;
  double *k_aE = data->coeffs_k.aE;
  double *k_aS = data->coeffs_k.aS;
  double *k_aN = data->coeffs_k.aN;
  double *k_aB = data->coeffs_k.aB;
  double *k_aT = data->coeffs_k.aT;
  double *k_b = data->coeffs_k.b;
  double *SC_vol_k_turb = data->SC_vol_k_turb;
  double *SC_vol_k_turb_lag = data->SC_vol_k_turb_lag;
  
  const double *P_k = data->P_k;
  const double *Dk_x = data->Dk_x;
  const double *Dk_y = data->Dk_y;
  const double *Dk_z = data->Dk_z;
  const double *Fk_x = data->Fk_x;
  const double *Fk_y = data->Fk_y;
  const double *Fk_z = data->Fk_z;
  const double *gamma = data->gamma;
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;
  const double *vol = data->vol_uncut;
  const double *k_turb0 = data->k_turb0;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;    
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zB = data->Delta_zB;
  const double *Delta_zT = data->Delta_zT;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;
  const double *Delta_zb = data->Delta_zb;
  const double *Delta_zt = data->Delta_zt;  
  
  /* Computing convective and diffusive contributions */
  set_D_k_3D(data);
  set_F_k_3D(data);

  /* Computing source terms */
  compute_k_SC_at_interface_3D(data);

  /* Relax source term */
  sum_relax_array(SC_vol_k_turb, SC_vol_k_turb_lag, SC_vol_k_turb,
		  alpha_SC_k, turb_boundary_cell_count, 1, 1);
  copy_array(SC_vol_k_turb, SC_vol_k_turb_lag, turb_boundary_cell_count, 1, 1);

  /* k */

#pragma omp parallel for default(none) private(I, J, K, i, j, k, index_, index_u, index_v, \
					       index_w, Dw, De, Ds, Dn, Db, Dt, Fw, Fe, Fs, Fn, Fb, Ft) \
  shared(dom_matrix, k_aW, k_aE, k_aS, k_aN, k_aB, k_aT, k_aP, k_b, Dk_x, Dk_y, Dk_z, Fk_x, \
	 Fk_y, Fk_z, P_k, gamma, vol)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(dom_matrix[index_] == 0){

	  i = I - 1;
	  j = J - 1;
	  k = K - 1;
	  index_u = K*nx*Ny + J*nx + i;
	  index_v = K*Nx*ny + j*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
	  Dw = Dk_x[index_u];
	  De = Dk_x[index_u+1];
	  Ds = Dk_y[index_v];
	  Dn = Dk_y[index_v+Nx];
	  Db = Dk_z[index_w];
	  Dt = Dk_z[index_w-Nx*Ny];
	  Fw = Fk_x[index_u];
	  Fe = Fk_x[index_u+1];
	  Fs = Fk_y[index_v];
	  Fn = Fk_y[index_v+Nx];
	  Fb = Fk_z[index_w];
	  Ft = Fk_z[index_w+Nx*Ny];
	
	  k_aW[index_] = Dw + MAX(0, Fw);
	  k_aE[index_] = De + MAX(0, -Fe);
	  k_aS[index_] = Ds + MAX(0, Fs);
	  k_aN[index_] = Dn + MAX(0, -Fn);
	  k_aB[index_] = Db + MAX(0, Fb);
	  k_aT[index_] = Dt + MAX(0, -Ft);
	  k_aP[index_] = Dw + De + Ds + Dn + Db + Dt +
	    MAX(0, -Fw) + MAX(0, Fe) +
	    MAX(0, -Fs) + MAX(0, Fn) +
	    MAX(0, -Fb) + MAX(0, Ft) +
	    rho * gamma[index_] * vol[index_];
	  k_b[index_] = P_k[index_] * vol[index_];

	}
	else{
	  k_aP[index_] = 1;
	  k_b[index_] = 0;
	}
      }
    }
  }

  /* Treatment for fluid boundary cells */

#pragma omp parallel for default(none) private(count, index_, I, J, K, i, j, k, index_u, \
					       index_v, index_w, Dw, De, Ds, Dn, Db, Dt, Fw, \
					       Fe, Fs, Fn, Fb, Ft, Da, m) \
  shared(turb_boundary_indexs, turb_bnd_excl_indexs, k_aW, k_aE, k_aS, k_aN, k_aB, k_aT, k_aP, \
	 k_b, turb_boundary_I, turb_boundary_J, turb_boundary_K, Dk_x, Dk_y, Dk_z, Fk_x, Fk_y, Fk_z, \
	 W_is_fluid_OK, E_is_fluid_OK, S_is_fluid_OK, N_is_fluid_OK, B_is_fluid_OK, T_is_fluid_OK, \
	 eps_x, eps_y, eps_z, Delta_xW, Delta_xE, Delta_yS, Delta_yN, Delta_zB, Delta_zT, \
	 Delta_xw, Delta_xe, Delta_ys, Delta_yn, Delta_zb, Delta_zt, Delta_x, Delta_y, Delta_z, \
	 gamma, vol, P_k, SC_vol_k_turb)
  
  for(count=0; count<turb_boundary_cell_count; count++){
    
    index_ = turb_boundary_indexs[count];
    
    if(turb_bnd_excl_indexs[count]){
      k_aW[index_] = 0;
      k_aE[index_] = 0;
      k_aS[index_] = 0;
      k_aN[index_] = 0;
      k_aB[index_] = 0;
      k_aT[index_] = 0;
      k_aP[index_] = 1;
      k_b[index_] = 0;
    }
    else{
      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      K = turb_boundary_K[count];
      i = I - 1;
      j = J - 1;
      k = K - 1;
      index_u = K*nx*Ny + J*nx + i;
      index_v = K*Nx*ny + j*Nx + I;
      index_w = k*Nx*Ny + J*Nx + I;
      Dw = Dk_x[index_u];
      De = Dk_x[index_u+1];
      Ds = Dk_y[index_v];
      Dn = Dk_y[index_v+Nx];
      Db = Dk_z[index_w];
      Dt = Dk_z[index_w+Nx*Ny];
      Fw = Fk_x[index_u];
      Fe = Fk_x[index_u+1];
      Fs = Fk_y[index_v];
      Fn = Fk_y[index_v+Nx];
      Fb = Fk_z[index_w];
      Ft = Fk_z[index_w+Nx*Ny];

      if(!W_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
	m = Delta_x[index_] / Da;	
	Dw = 0;
	De = De * m;
	Fw = 0;
      }
      
      if(!E_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
	m = Delta_x[index_] / Da;	
	De = 0;
	Dw = Dw * m;
	Fe = 0;
      }

      if(!S_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
	m = Delta_y[index_] / Da;	
	Ds = 0;
	Dn = Dn * m;
	Fs = 0;
      }

      if(!N_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
	m = Delta_y[index_] / Da;	
	Dn = 0;
	Ds = Ds * m;
	Fn = 0;
      }

      if(!B_is_fluid_OK[index_]){
	Da = eps_z[index_] * Delta_zB[index_] + Delta_zt[index_];
	m = Delta_z[index_] / Da;	
	Db = 0;
	Dt = Dt * m;
	Fb = 0;
      }

      if(!T_is_fluid_OK[index_]){
	Da = eps_z[index_] * Delta_zT[index_] + Delta_zb[index_];
	m = Delta_z[index_] / Da;	
	Dt = 0;
	Db = Db * m;
	Ft = 0;
      }
	
      k_aW[index_] = Dw + MAX(0, Fw);
      k_aE[index_] = De + MAX(0, -Fe);
      k_aS[index_] = Ds + MAX(0, Fs);
      k_aN[index_] = Dn + MAX(0, -Fn);
      k_aB[index_] = Db + MAX(0, Fb);
      k_aT[index_] = Dt + MAX(0, -Ft);
      k_aP[index_] = Dw + De + Ds + Dn + Db + Dt + 
	MAX(0, -Fw) + MAX(0, Fe) +
	MAX(0, -Fs) + MAX(0, Fn) +
	MAX(0, -Fb) + MAX(0, Ft) + 	
	rho * gamma[index_] * vol[index_];
      k_b[index_] = P_k[index_] * vol[index_] + SC_vol_k_turb[count];
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, J, K, index_, aP0)	\
  shared(dom_matrix, k_aP, k_b, k_turb0, vol)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_ = K*Nx*Ny + J*Nx + I;
	  
	  if(dom_matrix[index_] == 0){
	    aP0 = rho * vol[index_] / dt;
	    k_aP[index_] += aP0;
	    k_b[index_] += aP0 * k_turb0[index_];
	  }
	}
      }
    }
    
  }


  relax_coeffs_3D(data->k_turb, dom_matrix, 0, data->coeffs_k, Nx, Ny, Nz, data->alpha_k);

  return;
}

/* COMPUTE_K_SC_AT_INTERFACE_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, turb_boundary_cell_count): Data_Mem::SC_vol_k_turb: Source term due to solid boundary for Data_Mem::k_turb equation.

   @param data

   @return void

   MODIFIED: 07-04-2020
*/
void compute_k_SC_at_interface_3D(Data_Mem *data){
	
  int I, J, K, i, j, k;
  int count, index_;
  int index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  
  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *turb_boundary_K = data->turb_boundary_K;

  double k_b, mu_eff, k_m;
  double dkdxb, dkdyb, dkdzb;
  double Fw, Fe, Fs, Fn, Fb, Ft;
  double e_x, e_y, e_z;
  double Da, m;

  const double C_sigma_k = data->C_sigma_k;
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double eps_dist_tol = data->eps_dist_tol;
  
  double *SC_vol_k_turb = data->SC_vol_k_turb;

  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;
  const double *mu_turb = data->mu_turb;
  const double *k_turb = data->k_turb;
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zB = data->Delta_zB;
  const double *Delta_zT = data->Delta_zT;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;
  const double *Delta_zb = data->Delta_zb;
  const double *Delta_zt = data->Delta_zt;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  

  /* Value is reset to 0 here, contributions from solid neighbouring nodes 
     are added consecutively */
  for(count=0; count<turb_boundary_cell_count; count++){
    SC_vol_k_turb[count] = 0;
  }
  
  /* Boundary value is zero for k */

  k_b = 0;

  for(count=0; count<turb_boundary_cell_count; count++){
      
    index_ = turb_boundary_indexs[count];
    mu_eff = mu + mu_turb[index_] / C_sigma_k;
    I = turb_boundary_I[count];
    J = turb_boundary_J[count];
    K = turb_boundary_K[count];
    i = I - 1;
    j = J - 1;
    k = K - 1;
    index_u = K*nx*Ny + J*nx + i;
    index_v = K*Nx*ny + j*Nx + I;
    index_w = k*Nx*Ny + J*Nx + I;

    if(dom_matrix[index_-1] != 0){
      /* Diffusive correction factor */
      e_x = eps_x[index_];
      Da = e_x * Delta_xW[index_] + Delta_xe[index_];
      m = Delta_x[index_] / Da;
      
      /* Convective parameter */
      Fw = rho * u[index_u];

      /* Sato (W: -+-) : derivative at boundary */
      __get_b_values_Dirichlet(&dkdxb, &k_m, e_x, eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_+1],
			       Delta_xW[index_], Delta_xE[index_], -1);      

      /* Contribution to SC_vol_k_turb (W: -+) */
      SC_vol_k_turb[count] += (-1) * mu_eff * dkdxb * faces_x[index_u] * m + 
	Fw * k_m * faces_x[index_u];

    }

    if(dom_matrix[index_+1] != 0){
      /* Diffusive correction factor */
      e_x = eps_x[index_];
      Da = e_x * Delta_xE[index_] + Delta_xw[index_];
      m = Delta_x[index_] / Da;
      
      /* Convective parameter */
      Fe = rho * u[index_u+1];


      /* Sato (E: +-+) : derivative at boundary */
      __get_b_values_Dirichlet(&dkdxb, &k_m, e_x, eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_-1],
			       Delta_xE[index_], Delta_xW[index_], 1);
      
      /* Contribution to SC_vol_k_turb (E: +-) */
      SC_vol_k_turb[count] += mu_eff * dkdxb * faces_x[index_u+1] * m - 
	Fe * k_m * faces_x[index_u+1];
    }

    if(dom_matrix[index_-Nx] != 0){
      /* Diffusive correction factor */
      e_y = eps_y[index_];
      Da = e_y * Delta_yS[index_] + Delta_yn[index_];
      m = Delta_y[index_] / Da;
      
      /* Convective parameter */
      Fs = rho * v[index_v];      

      /* Sato (S: -+-) : derivative at boundary */      
      __get_b_values_Dirichlet(&dkdyb, &k_m, e_y, eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_+Nx],
			       Delta_yS[index_], Delta_yN[index_], -1);
      
      /* Contribution to SC_vol_k_turb (S: -+) */
      SC_vol_k_turb[count] += (-1) * mu_eff * dkdyb * faces_y[index_v] * m + 
	Fs * k_m * faces_y[index_v];
    }

    if(dom_matrix[index_+Nx] != 0){
      /* Diffusive correction factor */      
      e_y = eps_y[index_];
      Da = e_y * Delta_yN[index_] + Delta_ys[index_];
      m = Delta_y[index_] / Da;
	
      /* Convective parameter */
      Fn = rho * v[index_v+Nx];
      
      /* Sato (N: +-+) : derivative at boundary */
      __get_b_values_Dirichlet(&dkdyb, &k_m, e_y, eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_-Nx],
			       Delta_yN[index_], Delta_yS[index_], 1);
      
      /* Contribution to SC_vol_k_turb (N: +-) */
      SC_vol_k_turb[count] += mu_eff * dkdyb * faces_y[index_v+Nx] * m -
	Fn * k_m * faces_y[index_v+Nx];

    }

    if(dom_matrix[index_-Nx*Ny] != 0){
      /* Diffusive correction factor */      
      e_z = eps_z[index_];
      Da = e_z * Delta_zB[index_] + Delta_zt[index_];
      m = Delta_z[index_] / Da;
      
      /* Convective parameter */
      Fb = rho * w[index_w];


      /* Sato (B: -+-) : derivative at boundary */            
      __get_b_values_Dirichlet(&dkdzb, &k_m, e_z, eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_+Nx*Ny],
			       Delta_zB[index_], Delta_zT[index_], -1);
					   
      /* Contribution to SC_vol_k_turb (B: -+) */
      SC_vol_k_turb[count] += (-1) * mu_eff * dkdzb * faces_z[index_w] * m +
	Fb * k_m * faces_z[index_w];
						       
    }

    if(dom_matrix[index_+Nx*Ny] != 0){
      /* Diffusive correction factor */
      e_z = eps_z[index_];
      Da = e_z * Delta_zT[index_] + Delta_zb[index_];
      m = Delta_z[index_] / Da;
      
      /* Convective parameter */
      Ft = rho * w[index_+Nx*Ny];

      /* Sato (T: +-+) : derivative at boundary */      
      __get_b_values_Dirichlet(&dkdzb, &k_m, e_z, eps_dist_tol,
			       k_b, k_turb[index_], k_turb[index_-Nx*Ny],
			       Delta_zT[index_], Delta_zB[index_], 1);
      
      /* Contribution to SC_vol_k_turb (T: +-) */
      SC_vol_k_turb[count] += mu_eff * dkdzb * faces_z[index_w+Nx*Ny] * m -
	Ft * k_m * faces_z[index_w+Nx*Ny];
    }
	    
  }
  
  return;
}

/* SET_COEFFS_EPS_3D */
/*****************************************************************************/
/**

   Sets:

   Strucuture coeffs_eps: coefficients of epsilon equation for internal nodes.

   @param data

   @return void
 */
void set_coeffs_eps_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_, index_u, index_v, index_w;
  int count;
  int WE_solid_OK, SN_solid_OK, BT_solid_OK;

  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int turb_model = data->turb_model;
  const int turb_boundary_cell_count =
    data->turb_boundary_cell_count;
  const int flow_steady_state_OK = data->flow_steady_state_OK;

  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_bnd_excl_indexs = data->turb_bnd_excl_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *turb_boundary_K = data->turb_boundary_K;
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;

  double E_vol;
  double Dw, De, Ds, Dn, Db, Dt;
  double Fw, Fe, Fs, Fn, Fb, Ft;
  double aP0, Da, m;

  const double C_1 = data->C_1;
  const double C_2 = data->C_2;
  const double rho = data->rho_v[0];
  const double alpha_SC_eps = data->alpha_SC_eps;
  const double dt = data->dt[3];

  double *eps_aP = data->coeffs_eps.aP;
  double *eps_aW = data->coeffs_eps.aW;
  double *eps_aE = data->coeffs_eps.aE;
  double *eps_aS = data->coeffs_eps.aS;  
  double *eps_aN = data->coeffs_eps.aN;
  double *eps_aB = data->coeffs_eps.aB;
  double *eps_aT = data->coeffs_eps.aT;
  double *eps_b = data->coeffs_eps.b;
  double *SC_vol_eps_turb = data->SC_vol_eps_turb;
  double *SC_vol_eps_turb_lag = data->SC_vol_eps_turb_lag;

  const double *P_k = data->P_k;
  const double *Deps_x = data->Deps_x;
  const double *Deps_y = data->Deps_y;
  const double *Deps_z = data->Deps_z;
  const double *Feps_x = data->Feps_x;
  const double *Feps_y = data->Feps_y;
  const double *Feps_z = data->Feps_z;
  const double *gamma = data->gamma;
  const double *f_2 = data->f_2;
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;
  const double *S_CG_eps = data->S_CG_eps;
  const double *eps_bnd_x = data->eps_bnd_x;
  const double *eps_bnd_y = data->eps_bnd_y;
  const double *eps_bnd_z = data->eps_bnd_z;
  const double *vol = data->vol_uncut;
  const double *eps_turb0 = data->eps_turb0;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;    
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zB = data->Delta_zB;
  const double *Delta_zT = data->Delta_zT;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;
  const double *Delta_zb = data->Delta_zb;
  const double *Delta_zt = data->Delta_zt;  
  
  /* Computing convective and diffusive contributions */
  set_D_eps_3D(data);
  set_F_eps_3D(data);

  /* Computing source terms */
  compute_eps_SC_at_interface_3D(data);

  /* Relax source term */
  sum_relax_array(SC_vol_eps_turb, SC_vol_eps_turb_lag, SC_vol_eps_turb,
		  alpha_SC_eps, turb_boundary_cell_count, 1, 1);
  copy_array(SC_vol_eps_turb, SC_vol_eps_turb_lag, turb_boundary_cell_count, 1, 1);

  /* eps */

#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_u, index_v, index_w, Dw, De, Ds, \
	  Dn, Db, Dt, E_vol, Fw, Fe, Fs, Fn, Fb, Ft)			\
  shared(dom_matrix, eps_aW, eps_aE, eps_aS, eps_aN, eps_aB, eps_aT, eps_aP, eps_b, \
	 Deps_x, Deps_y, Deps_z, Feps_x, Feps_y, Feps_z, P_k, f_2, gamma, \
	 S_CG_eps, vol)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(dom_matrix[index_] == 0){

	  i = I - 1;
	  j = J - 1;
	  k = K - 1;
	  index_u = K*nx*Ny + J*nx + i;
	  index_v = K*Nx*ny + j*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
	  Dw = Deps_x[index_u];
	  De = Deps_x[index_u+1];
	  Ds = Deps_y[index_v];
	  Dn = Deps_y[index_v+Nx];
	  Db = Deps_z[index_w];
	  Dt = Deps_z[index_w+Nx+Ny];
	  Fw = Feps_x[index_u];
	  Fe = Feps_x[index_u+1];
	  Fs = Feps_y[index_v];
	  Fn = Feps_y[index_v+Nx];
	  Fb = Feps_z[index_w];
	  Ft = Feps_z[index_w+Nx*Ny];

	  if(turb_model == 2){
	    E_vol = S_CG_eps[index_] * vol[index_];	  
	  }
	  else{
	    E_vol = 0;
	  }

	  eps_aW[index_] = Dw + MAX(0, Fw);
	  eps_aE[index_] = De + MAX(0, -Fe);
	  eps_aS[index_] = Ds + MAX(0, Fs);
	  eps_aN[index_] = Dn + MAX(0, -Fn);
	  eps_aB[index_] = Db + MAX(0, Fb);
	  eps_aT[index_] = Dt + MAX(0, -Ft);	  
	  eps_aP[index_] = Dw + De + Ds + Dn + Db + Dt + 
	    MAX(0, -Fw) + MAX(0, Fe) + 
	    MAX(0, -Fs) + MAX(0, Fn) +
	    MAX(0, -Fb) + MAX(0, Ft) + 
	    C_2 * f_2[index_] * rho * gamma[index_] * vol[index_];
	  eps_b[index_] = C_1 * P_k[index_] * gamma[index_] * vol[index_] + E_vol;

	}
	else{
	  eps_aP[index_] = 1;
	  eps_b[index_] = 0;
	}
      }
    }
  }

  /* Treatment for fluid boundary cells */

#pragma omp parallel for default(none) private(count, index_, WE_solid_OK, SN_solid_OK, BT_solid_OK, \
					       I, J, K, i, j, k, index_u, index_v, index_w, Dw, De, Ds, Dn, Db, Dt, \
					       Fw, Fe, Fs, Fn, Fb, Ft, Da, m, E_vol) \
  shared(turb_boundary_indexs, turb_bnd_excl_indexs, eps_aW, eps_aE, eps_aS, eps_aN, eps_aB, \
	 eps_aT, eps_aP, eps_b, W_is_fluid_OK, E_is_fluid_OK, S_is_fluid_OK, N_is_fluid_OK, B_is_fluid_OK, \
	 T_is_fluid_OK, eps_bnd_x, eps_bnd_y, eps_bnd_z, turb_boundary_I, turb_boundary_J, turb_boundary_K, \
	 Deps_x, Deps_y, Deps_z, Feps_x, Feps_y, Feps_z, eps_x, eps_y, eps_z, Delta_xW, Delta_xE, Delta_yS, \
	 Delta_yN, Delta_zT, Delta_zB, Delta_xw, Delta_xe, Delta_ys, Delta_yn, Delta_zb, Delta_zt, S_CG_eps, \
	 vol, f_2, gamma, P_k, SC_vol_eps_turb, Delta_x, Delta_y, Delta_z)
  
  for(count=0; count<turb_boundary_cell_count; count++){
    
    index_ = turb_boundary_indexs[count];

    if(turb_bnd_excl_indexs[count]){
      
      eps_aW[index_] = 0;
      eps_aE[index_] = 0;
      eps_aS[index_] = 0;
      eps_aN[index_] = 0;
      eps_aB[index_] = 0;
      eps_aT[index_] = 0;
      eps_aP[index_] = 1;

      WE_solid_OK = (!W_is_fluid_OK[index_]) || (!E_is_fluid_OK[index_]);
      SN_solid_OK = (!S_is_fluid_OK[index_]) || (!N_is_fluid_OK[index_]);
      BT_solid_OK = (!B_is_fluid_OK[index_]) || (!T_is_fluid_OK[index_]);
      

      eps_b[index_] = (eps_bnd_x[count] * WE_solid_OK + eps_bnd_y[count] * SN_solid_OK +
		       eps_bnd_z[count] * BT_solid_OK) / (WE_solid_OK + SN_solid_OK + BT_solid_OK);


    }
    else{
      
      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      K = turb_boundary_K[count];
      i = I - 1;
      j = J - 1;
      k = K - 1;
      index_u = K*nx*Ny + J*nx + i;
      index_v = K*Nx*ny + j*Nx + I;
      index_w = k*Nx*Ny + J*Nx + I;
      Dw = Deps_x[index_u];
      De = Deps_x[index_u+1];
      Ds = Deps_y[index_v];
      Dn = Deps_y[index_v+Nx];
      Db = Deps_z[index_w];
      Dt = Deps_z[index_w+Nx*Ny];
      Fw = Feps_x[index_u];
      Fe = Feps_x[index_u+1];
      Fs = Feps_y[index_v];
      Fn = Feps_y[index_v+Nx];
      Fb = Feps_z[index_w];
      Ft = Feps_z[index_w+Nx*Ny];

      if(!W_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
	m = Delta_x[index_] / Da;	
	Dw = 0;
	De = De * m;
	Fw = 0;
      }
      
      if(!E_is_fluid_OK[index_]){
	Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
	m = Delta_x[index_] / Da;	
	De = 0;
	Dw = Dw * m;
	Fe = 0;
      }

      if(!S_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
	m = Delta_y[index_] / Da;	
	Ds = 0;
	Dn = Dn * m;
	Fs = 0;
      }      

      if(!N_is_fluid_OK[index_]){
	Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
	m = Delta_y[index_] / Da;	
	Dn = 0;
	Ds = Ds * m;
	Fn = 0;
      }

      if(!B_is_fluid_OK[index_]){
	Da = eps_z[index_] * Delta_zB[index_] + Delta_zt[index_];
	m = Delta_z[index_] / Da;	
	Db = 0;
	Dt = Dt * m;
	Fb = 0;
      }

      if(!T_is_fluid_OK[index_]){
	Da = eps_z[index_] * Delta_zT[index_] + Delta_zb[index_];
	m = Delta_z[index_] / Da;	
	Dt = 0;
	Db = Db * m;
	Ft = 0;
      }

      if(turb_model == 2){
	E_vol = S_CG_eps[index_] * vol[index_];	  
      }
      else{
	E_vol = 0;
      }

      eps_aW[index_] = Dw + MAX(0, Fw);
      eps_aE[index_] = De + MAX(0, -Fe);
      eps_aS[index_] = Ds + MAX(0, Fs);
      eps_aN[index_] = Dn + MAX(0, -Fn);
      eps_aB[index_] = Db + MAX(0, Fb);
      eps_aT[index_] = Dt + MAX(0, -Ft);
      
      eps_aP[index_] = Dw + De + Ds + Dn + Db + Dt + 
	MAX(0, -Fw) + MAX(0, Fe) + 
	MAX(0, -Fs) + MAX(0, Fn) +
	MAX(0, -Fb) + MAX(0, Ft) +
	C_2 * f_2[index_] * rho * gamma[index_] * vol[index_];
      eps_b[index_] = C_1 * P_k[index_] * gamma[index_] * vol[index_] + E_vol + SC_vol_eps_turb[count];
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, J, K, index_, aP0)	\
  shared(dom_matrix, vol, eps_aP, eps_b, eps_turb0)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_ = K*Nx*Ny + J*Nx + I;
	  
	  if(dom_matrix[index_] == 0){
	    aP0 = rho * vol[index_] / dt;
	    eps_aP[index_] += aP0;
	    eps_b[index_] += aP0 * eps_turb0[index_];
	  }
	}
      }
    }
    
  }
  
  relax_coeffs_3D(data->eps_turb, dom_matrix, 0, data->coeffs_eps, Nx, Ny, Nz, data->alpha_eps);
  
  return;
}

/* COMPUTE_EPS_SC_AT_INTERFACE_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, turb_boundary_cell_count): Data_Mem::SC_vol_eps_turb: Source term due to solid boundary for Data_Mem::eps_turb equation.

   @param data

   @return void

   MODIFIED: 08-04-2020
*/
void compute_eps_SC_at_interface_3D(Data_Mem *data){
	
  int count, index_;
  int i, j, k, I, J, K, index_u, index_v, index_w;
  int iter_count, cell_done_OK;
  int W_fluid, E_fluid, S_fluid, N_fluid, B_fluid, T_fluid;

  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  const int turb_model = data->turb_model;
  const int iter_total = 100;
  
  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;
  const int *turb_boundary_K = data->turb_boundary_K;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;  

  double eps_bnd, mu_eff, eps_m;
  double e_x, e_y, e_z;
  double Fw, Fe, Fs, Fn, Fb, Ft;
  double depsdx_P, depsdx_W, depsdx_E, depsdx_S, depsdx_N, depsdx_B, depsdx_T;
  double depsdy_P, depsdy_W, depsdy_E, depsdy_S, depsdy_N, depsdy_B, depsdy_T;  
  double depsdz_P, depsdz_W, depsdz_E, depsdz_S, depsdz_N, depsdz_B, depsdz_T;
  double depsdS_P, depsdS_W, depsdS_E, depsdS_S, depsdS_N, depsdS_B, depsdS_T;
  double depsdS_2_P, depsdS_2_W, depsdS_2_E, depsdS_2_S, depsdS_2_N, depsdS_2_B, depsdS_2_T;
  double depsdSface_x, depsdS_2face_x, depsdSface_y, depsdS_2face_y, depsdSface_z, depsdS_2face_z;
  double Few = 0, Fns = 0, Ftb = 0;
  double eps_m_ew = 0, eps_m_ns = 0, eps_m_tb = 0;
  double Jx, Jy, Jz;
  double Da, m, mx = 0, my = 0, mz = 0;

  const double C_sigma_eps = data->C_sigma_eps;
  const double mu = data->mu;
  const double max_eps_wall_value = 1e10;
  const double rho = data->rho_v[0];
  const double eps_dist_tol = data->eps_dist_tol;
  const double depsdNb = 0;

  double *depsdxb = data->depsdxb;
  double *depsdyb = data->depsdyb;
  double *depsdzb = data->depsdzb;
  double *SC_vol_eps_turb = data->SC_vol_eps_turb;
   
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  const double *mu_turb = data->mu_turb;
  const double *eps_turb = data->eps_turb;
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;
  const double *eps_bnd_x = data->eps_bnd_x;
  const double *eps_bnd_y = data->eps_bnd_y;
  const double *eps_bnd_z = data->eps_bnd_z;
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;
  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_x_z = data->par_vec_x_z;
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  const double *par_vec_y_z = data->par_vec_y_z;  
  const double *par_vec_z_x = data->par_vec_z_x;
  const double *par_vec_z_y = data->par_vec_z_y;
  const double *par_vec_z_z = data->par_vec_z_z;
  const double *par_vec_x_x_2 = data->par_vec_x_x_2;
  const double *par_vec_x_y_2 = data->par_vec_x_y_2;
  const double *par_vec_x_z_2 = data->par_vec_x_z_2;
  const double *par_vec_y_x_2 = data->par_vec_y_x_2;
  const double *par_vec_y_y_2 = data->par_vec_y_y_2;
  const double *par_vec_y_z_2 = data->par_vec_y_z_2;  
  const double *par_vec_z_x_2 = data->par_vec_z_x_2;
  const double *par_vec_z_y_2 = data->par_vec_z_y_2;
  const double *par_vec_z_z_2 = data->par_vec_z_z_2;
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *norm_vec_z_z = data->norm_vec_z_z;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zB = data->Delta_zB;
  const double *Delta_zT = data->Delta_zT;
  const double *Delta_xw = data->Delta_xw;
  const double *Delta_xe = data->Delta_xe;
  const double *Delta_ys = data->Delta_ys;
  const double *Delta_yn = data->Delta_yn;
  const double *Delta_zb = data->Delta_zb;
  const double *Delta_zt = data->Delta_zt;

  /* Value is reset to 0 here, contributions from solid neighbouring nodes 
     are added consecutively */
  for(count=0; count<turb_boundary_cell_count; count++){
    SC_vol_eps_turb[count] = 0;
  }
  
  if(turb_model == 1){

    /* Dirichlet boundary condition for epsilon
       First calculate boundary values according to eps_bnd = 2*nu*(d(sqrt(k))/dy_wall)**2 */

    compute_eps_bnd_3D(data);

    for(count=0; count<turb_boundary_cell_count; count++){
      
      index_ = turb_boundary_indexs[count];
      mu_eff = mu + mu_turb[index_] / C_sigma_eps;
      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      K = turb_boundary_K[count];
      i = I - 1;
      j = J - 1;
      k = K - 1;
      index_u = K*nx*Ny + J*nx + i;
      index_v = K*Nx*ny + j*Nx + I;
      index_w = k*Nx*Ny + J*Nx + I;

      if(dom_matrix[index_-1]){

	/* Here we assume that only one of W|E neighbour can be a solid node */
	eps_bnd = MIN(max_eps_wall_value, eps_bnd_x[count]);
	e_x = eps_x[index_];

	/* Diffusive correction factor */
	Da = e_x * Delta_xW[index_] + Delta_xe[index_];
	m = Delta_x[index_] / Da;
	
	/* Convective parameter */
	Fw = rho * u[index_u];

	/* Sato (W: -+-): derivative at boundary */	
	__get_b_values_Dirichlet(&(depsdxb[count]), &eps_m, e_x, eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_+1],
				 Delta_xW[index_], Delta_xE[index_], -1);
				 
	/* Contribution to SC_vol_eps_turb (W: -+) */
	SC_vol_eps_turb[count] += (-1) * mu_eff * depsdxb[count] * faces_x[index_u] * m + 
	  Fw * eps_m * faces_x[index_u];

      }

      if(dom_matrix[index_+1]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_x[count]);
	e_x = eps_x[index_];

	/* Diffusive correction factor */
	Da = e_x * Delta_xE[index_] + Delta_xw[index_];
	m = Delta_x[index_] / Da;
	
	/* Convective parameter */
	Fe = rho * u[index_u+1];


	/* Sato (E: +-+): derivative at boundary */
	__get_b_values_Dirichlet(&(depsdxb[count]), &eps_m, e_x, eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_-1],
				 Delta_xE[index_], Delta_xW[index_], 1);	

	/* Contribution to SC_vol_eps_turb (E: +-) */
	SC_vol_eps_turb[count] += mu_eff * depsdxb[count] * faces_x[index_u+1] * m - 
	  Fe * eps_m * faces_x[index_u+1];

      }

      if(dom_matrix[index_-Nx]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_y[count]);
	e_y = eps_y[index_];

	/* Diffusive correction factor */
	Da = e_y * Delta_yS[index_] + Delta_yn[index_];
	m = Delta_y[index_] / Da;
	
	/* Convective parameter */
	Fs = rho * v[index_v];


	/* Sato (S: -+-): derivative at face */
	__get_b_values_Dirichlet(&(depsdyb[count]), &eps_m, e_y, eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_+Nx],
				 Delta_yS[index_], Delta_yN[index_], -1);		 

	/* Contribution to SC_vol_eps_turb (S: -+) */
	SC_vol_eps_turb[count] += (-1) * mu_eff * depsdyb[count] * faces_y[index_v] * m + 
	  Fs * eps_m * faces_y[index_v];

      }

      if(dom_matrix[index_+Nx]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_y[count]);
	e_y = eps_y[index_];

	/* Diffusive correction factor */
	Da = e_y * Delta_yN[index_] + Delta_ys[index_];
	m = Delta_y[index_] / Da;
	
	/* Convective parameter */
	Fn = rho * v[index_v+Nx];

	/* Sato (N: +-+): derivative at boundary */
	__get_b_values_Dirichlet(&(depsdyb[count]), &eps_m, e_y, eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_-Nx],
				 Delta_yN[index_], Delta_yS[index_], 1);       

	/* Contribution to SC_vol_eps_turb (N: +-) */
	SC_vol_eps_turb[count] += mu_eff * depsdyb[count] * faces_y[index_v+Nx] * m - 
	  Fn * eps_m * faces_y[index_v+Nx];
      }      

      if(dom_matrix[index_-Nx*Ny]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_z[count]);
	e_z = eps_z[index_];

	/* Diffusive correction factor */
	Da = e_z * Delta_zB[index_] + Delta_zt[index_];
	m  = Delta_z[index_] / Da;
	
	/* Convective parameter */
	Fb = rho * w[index_w];

	/* Sato (B: -+-): derivative at face */
	__get_b_values_Dirichlet(&(depsdzb[count]), &eps_m, e_z, eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_+Nx*Ny],
				 Delta_zB[index_], Delta_zT[index_], -1);       

	/* Contribution to SC_vol_eps_turb (B: -+) */
	SC_vol_eps_turb[count] += (-1) * mu_eff * depsdzb[count] * faces_z[index_w] * m + 
	  Fb * eps_m * faces_z[index_w];

      }

      if(dom_matrix[index_+Nx*Ny]){

	eps_bnd = MIN(max_eps_wall_value, eps_bnd_z[count]);
	e_z = eps_z[index_];

	/* Diffusive correction factor */
	Da = e_z * Delta_zT[index_] + Delta_zb[index_];
	m = Delta_z[index_] / Da;
	
	/* Convective parameter */
	Ft = rho * w[index_v+Nx*Ny];

	/* Sato (T: +-+): derivative at boundary */
	__get_b_values_Dirichlet(&(depsdzb[count]), &eps_m, e_z, eps_dist_tol,
				 eps_bnd, eps_turb[index_], eps_turb[index_-Nx*Ny],
				 Delta_zT[index_], Delta_zB[index_], 1);       

	/* Contribution to SC_vol_eps_turb (T: +-) */
	SC_vol_eps_turb[count] += mu_eff * depsdzb[count] * faces_z[index_w+Nx*Ny] * m - 
	  Ft * eps_m * faces_z[index_w+Nx*Ny];
      }      
    }      
    
  } /* end if(turb_model == 1) */

  if(turb_model == 2){
    
    /* Isoflux boundary condition depsdN = 0 */

    for(iter_count=0; iter_count<iter_total; iter_count++){

      for(count=0; count<turb_boundary_cell_count; count++){

	I = turb_boundary_I[count];
	J = turb_boundary_J[count];
	K = turb_boundary_K[count];
	index_ = turb_boundary_indexs[count];
	i = I - 1;
	j = J - 1;
	k = K - 1;

	mu_eff = mu + mu_turb[index_] / C_sigma_eps;

	index_u = K*nx*Ny + J*nx + i;
	index_v = K*Nx*ny + j*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	cell_done_OK = 0;

	depsdx_P = 0;
	depsdx_W = 0;
	depsdx_E = 0;
	depsdx_S = 0;
	depsdx_N = 0;
	depsdx_B = 0;
	depsdx_T = 0;
	depsdy_P = 0;
	depsdy_W = 0;
	depsdy_E = 0;
	depsdy_S = 0;
	depsdy_N = 0;
	depsdy_B = 0;
	depsdy_T = 0;	
	depsdz_P = 0;
	depsdz_W = 0;
	depsdz_E = 0;
	depsdz_S = 0;
	depsdz_N = 0;
	depsdz_B = 0;
	depsdz_T = 0;
	
	/* E-P line intersects with curve */
	if(dom_matrix[index_+1] && !cell_done_OK){

	  cell_done_OK = 1;
	  depsdx_P = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_],
				     eps_turb[index_-1], Delta_xE[index_], Delta_xW[index_]);

	  depsdx_W = __central_der(eps_turb[index_], eps_turb[index_-1], eps_turb[index_-2],
				   Delta_xE[index_-1], Delta_xW[index_-1]);

	  /* From here on we use depsdxb[count] as approximation for depsdxb at other locations */	  
	  
	  /* Case E-N */
	  if(dom_matrix[index_+Nx]){

	    /* Derivatives required for N case at South position:
	     depsdx_S, depsdy_S, depsdz_S */
	    if(dom_matrix[index_-Nx+1]){
	      
	      depsdx_S = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx],
					 eps_turb[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }
	    else if(dom_matrix[index_-Nx-1]){

	      depsdx_S = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx+1],
					 eps_turb[index_-Nx], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	    }
	    else{
	      
	      depsdx_S = __central_der(eps_turb[index_-Nx+1], eps_turb[index_-Nx], eps_turb[index_-Nx-1],
				       Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }

	    depsdy_S = __central_der(eps_turb[index_], eps_turb[index_-Nx], eps_turb[index_-2*Nx],
				     Delta_yN[index_-Nx], Delta_yS[index_-Nx]);

	    if(dom_matrix[index_+Nx*Ny-Nx]){

	      depsdz_S = __central_b_der(eps_z[index_-Nx], depsdzb[count], eps_turb[index_-Nx],
					 eps_turb[index_-Nx-Nx*Ny], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-Nx]){

	      depsdz_S = __central_b_der(eps_z[index_-Nx], depsdzb[count], eps_turb[index_-Nx+Nx*Ny],
					 eps_turb[index_-Nx], Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	    }
	    else{

	      depsdz_S = __central_der(eps_turb[index_-Nx+Nx*Ny], eps_turb[index_-Nx], eps_turb[index_-Nx-Nx*Ny],
				       Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	    }
	  }

	  /* Case E-S */
	  if(dom_matrix[index_-Nx]){

	    /* Derivatives required for S case at North position:
	     depsdx_N, depsdy_N, depsdz_N */	    
	    if(dom_matrix[index_+Nx+1]){

	      depsdx_N = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx],
					 eps_turb[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }
	    else if(dom_matrix[index_+Nx-1]){

	      depsdx_N = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx+1],
					 eps_turb[index_+Nx], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	    }
	    else{
	      depsdx_N = __central_der(eps_turb[index_+Nx+1], eps_turb[index_+Nx], eps_turb[index_+Nx-1],
				       Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }

	    depsdy_N = __central_der(eps_turb[index_+2*Nx], eps_turb[index_+Nx], eps_turb[index_],
				     Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

	    if(dom_matrix[index_+Nx*Ny+Nx]){

	      depsdz_N = __central_b_der(eps_z[index_+Nx], depsdzb[count], eps_turb[index_+Nx],
					 eps_turb[index_+Nx-Nx*Ny], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	    }
	    else if(dom_matrix[index_-Nx*Ny+Nx]){

	      depsdz_N = __central_b_der(eps_z[index_+Nx], depsdzb[count], eps_turb[index_+Nx+Nx*Ny],
					 eps_turb[index_+Nx], Delta_zB[index_+Nx], Delta_zT[index_+Nx*Ny]);
	    }
	    else{

	      depsdz_N = __central_der(eps_turb[index_+Nx+Nx*Ny], eps_turb[index_+Nx], eps_turb[index_+Nx-Nx*Ny],
				       Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	    }
	    
	  }

	  /* Case E-T */
	  if(dom_matrix[index_+Nx*Ny]){

	    /* Derivatives required for T case at Bottom position:
	       depsdx_B, depsdy_B, depsdz_B */
	    if(dom_matrix[index_-Nx*Ny+1]){

	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-1]){

	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny+1],
					 eps_turb[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	    }
	    else{

	      depsdx_B = __central_der(eps_turb[index_-Nx*Ny+1], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }

	    if(dom_matrix[index_-Nx*Ny+Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny+Nx],
					 eps_turb[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	    }
	    else{

	      depsdy_B = __central_der(eps_turb[index_-Nx*Ny+Nx], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }

	    depsdz_B = __central_der(eps_turb[index_], eps_turb[index_-Nx*Ny], eps_turb[index_-2*Nx*Ny],
				     Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	  }

	  /* Case E-B */
	  if(dom_matrix[index_-Nx*Ny]){

	    /* Derivatives required for B case at Top position:
	       depsdx_T, depsdy_T, depsdz_T */
	    if(dom_matrix[index_+Nx*Ny+1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny+1],
					 eps_turb[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	    }
	    else{
	      
	      depsdx_T = __central_der(eps_turb[index_+Nx*Ny+1], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }

	    if(dom_matrix[index_+Nx*Ny+Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny+Nx],
					 eps_turb[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	    }
	    else{

	      depsdy_T = __central_der(eps_turb[index_+Nx*Ny+Nx], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }

	    depsdz_T = __central_der(eps_turb[index_+2*Nx*Ny], eps_turb[index_+Nx*Ny], eps_turb[index_],
				     Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);
	  }

	  /* Derivatives required for E case */

	  depsdy_P = __central_der(eps_turb[index_+Nx], eps_turb[index_], eps_turb[index_-Nx],
				   Delta_yN[index_], Delta_yS[index_]);
	  depsdy_W = __central_der(eps_turb[index_+Nx-1], eps_turb[index_-1], eps_turb[index_-Nx-1],
				   Delta_yN[index_-1], Delta_yS[index_-1]);
	  depsdz_P = __central_der(eps_turb[index_+Nx*Ny], eps_turb[index_], eps_turb[index_-Nx*Ny],
				   Delta_zT[index_], Delta_zB[index_]);
	  depsdz_W = __central_der(eps_turb[index_+Nx*Ny-1], eps_turb[index_-1], eps_turb[index_-Nx*Ny-1],
				   Delta_zT[index_-1], Delta_zB[index_-1]);

	  /* Check if depsdy_P or depsdy_W needs to be corrected */

	  /* Case E-N */
	  if(dom_matrix[index_+Nx]){
	    depsdy_P = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_],
				       eps_turb[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	  }

	  if(dom_matrix[index_+Nx-1]){
	    depsdy_W = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_-1],
				       eps_turb[index_-1-Nx], Delta_yN[index_-1], Delta_yS[index_-1]);
	  }

	  /* Case E-S */
	  if(dom_matrix[index_-Nx]){
	    depsdy_P = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_+Nx],
				       eps_turb[index_], Delta_yS[index_], Delta_yN[index_]);
	  }

	  if(dom_matrix[index_-Nx-1]){
	    depsdy_W = __central_b_der(eps_y[index_-1], depsdyb[count], eps_turb[index_-1+Nx],
				       eps_turb[index_-1], Delta_yS[index_-1], Delta_yN[index_-1]);
	  }

	  /* Check if depsdz_P or depsdz_W needs to be corrected */

	  /* Case E-T */
	  if(dom_matrix[index_+Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_],
				       eps_turb[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	  }

	  if(dom_matrix[index_+Nx*Ny-1]){
	    depsdz_W = __central_b_der(eps_z[index_-1], depsdzb[count], eps_turb[index_-1],
				       eps_turb[index_-1-Nx*Ny], Delta_zT[index_-1], Delta_zB[index_-1]);
	  }

	  /* Case E-B */
	  if(dom_matrix[index_-Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_+Nx*Ny],
				       eps_turb[index_], Delta_zT[index_], Delta_zB[index_]);
	  }

	  if(dom_matrix[index_-Nx*Ny-1]){
	    depsdz_W = __central_b_der(eps_z[index_-1], depsdzb[count], eps_turb[index_-1+Nx*Ny],
				       eps_turb[index_-1], Delta_zB[index_-1], Delta_zT[index_-1]);
	  }
	
	} /* End case E-P */

	/*******************************************************************/	
	/* W-P line intersects with curve */
	if(dom_matrix[index_-1] && !cell_done_OK){

	  cell_done_OK = 1;
	  depsdx_P = __central_b_der(eps_x[index_], depsdxb[count], eps_turb[index_+1],
				     eps_turb[index_], Delta_xW[index_], Delta_xE[index_]);

	  depsdx_E = __central_der(eps_turb[index_+2], eps_turb[index_+1], eps_turb[index_],
				   Delta_xE[index_+1], Delta_xW[index_+1]);

	  /* Case W-N */
	  if(dom_matrix[index_+Nx]){

	    /* Derivatives required for N case at South position:
	     depsdx_S, depsdy_S, depsdz_S */
	    if(dom_matrix[index_-Nx+1]){

	      depsdx_S = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx],
					 eps_turb[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }
	    else if(dom_matrix[index_-Nx-1]){
	      
	      depsdx_S = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx+1],
					 eps_turb[index_-Nx], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	    }
	    else{

	      depsdx_S = __central_der(eps_turb[index_-Nx+1], eps_turb[index_-Nx], eps_turb[index_-Nx-1],
			Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	    }

	    depsdy_S = __central_der(eps_turb[index_], eps_turb[index_-Nx], eps_turb[index_-2*Nx],
			Delta_yN[index_-Nx], Delta_yS[index_-Nx]);

	    if(dom_matrix[index_+Nx*Ny-Nx]){

	      depsdz_S = __central_b_der(eps_z[index_-Nx], depsdzb[count], eps_turb[index_-Nx],
					 eps_turb[index_-Nx-Nx*Ny], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-Nx]){

	      depsdz_S = __central_b_der(eps_z[index_-Nx], depsdzb[count], eps_turb[index_-Nx+Nx*Ny],
					 eps_turb[index_-Nx], Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	    }
	    else{

	      depsdz_S = __central_der(eps_turb[index_-Nx+Nx*Ny], eps_turb[index_-Nx], eps_turb[index_-Nx-Nx*Ny],
				       Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	    }	    
	  }

	  /* Case W-S */
	  if(dom_matrix[index_-Nx]){
	    
	    /* Derivatives required for S case at North position:
	     depsdx_N, depsdy_N, depsdz_N */
	    if(dom_matrix[index_+Nx+1]){

	      depsdx_N = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx],
					 eps_turb[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }
	    else if(dom_matrix[index_+Nx-1]){
	      
	      depsdx_N = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx+1],
					 eps_turb[index_+Nx], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	    }
	    else{
	      
	      depsdx_N = __central_der(eps_turb[index_+Nx+1], eps_turb[index_+Nx], eps_turb[index_+Nx-1],
				       Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	    }

	    depsdy_N = __central_der(eps_turb[index_+2*Nx], eps_turb[index_+Nx], eps_turb[index_],
				     Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

	    if(dom_matrix[index_+Nx*Ny+Nx]){

	      depsdz_N = __central_b_der(eps_z[index_+Nx], depsdzb[count], eps_turb[index_+Nx],
					 eps_turb[index_+Nx-Nx*Ny], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	    }
	    else if(dom_matrix[index_-Nx*Ny+Nx]){

	      depsdz_N = __central_b_der(eps_z[index_+Nx], depsdzb[count], eps_turb[index_+Nx+Nx*Ny],
					 eps_turb[index_+Nx], Delta_zB[index_+Nx], Delta_zB[index_+Nx]);
	    }
	    else{

	      depsdz_N = __central_der(eps_turb[index_+Nx+Nx*Ny], eps_turb[index_+Nx], eps_turb[index_+Nx-Nx*Ny],
				       Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	    }	    
	  }

	  /* Case W-T */
	  if(dom_matrix[index_+Nx*Ny]){

	    /* Derivatives required for T case at Bottom position:
	       depsdx_B, depsdy_B, depsdz_B */
	    if(dom_matrix[index_-Nx*Ny+1]){

	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-1]){

	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny+1],
					 eps_turb[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	    }
	    else{

	      depsdx_B = __central_der(eps_turb[index_-Nx*Ny+1], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }

	    if(dom_matrix[index_-Nx*Ny+Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny+Nx],
					 eps_turb[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	    }
	    else{

	      depsdy_B = __central_der(eps_turb[index_-Nx*Ny+Nx], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }

	    depsdz_B = __central_der(eps_turb[index_], eps_turb[index_-Nx*Ny], eps_turb[index_-2*Nx*Ny],
				     Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	  }

	  /* Case W-B */
	  if(dom_matrix[index_-Nx*Ny]){

	    /* Derivatives required for B case at Top position:
	       depsdx_T, depsdy_T, depsdz_T */
	    if(dom_matrix[index_+Nx*Ny+1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny+1],
					 eps_turb[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	    }
	    else{
	      
	      depsdx_T = __central_der(eps_turb[index_+Nx*Ny+1], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }

	    if(dom_matrix[index_+Nx*Ny+Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny+Nx],
					 eps_turb[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	    }
	    else{

	      depsdy_T = __central_der(eps_turb[index_+Nx*Ny+Nx], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }

	    depsdz_T = __central_der(eps_turb[index_+2*Nx*Ny], eps_turb[index_+Nx*Ny], eps_turb[index_],
				     Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);
	  }
	  

	  /* Derivatives required for W case */

	  depsdy_P = __central_der(eps_turb[index_+Nx], eps_turb[index_], eps_turb[index_-Nx],
				   Delta_yN[index_], Delta_yS[index_]);
	  depsdy_E = __central_der(eps_turb[index_+1+Nx], eps_turb[index_+1], eps_turb[index_+1-Nx],
				   Delta_yN[index_+1], Delta_yS[index_+1]);
	  depsdz_P = __central_der(eps_turb[index_+Nx*Ny], eps_turb[index_], eps_turb[index_-Nx*Ny],
				   Delta_zT[index_], Delta_zB[index_]);
	  depsdz_E = __central_der(eps_turb[index_+1+Nx*Ny], eps_turb[index_+1], eps_turb[index_+1-Nx*Ny],
				   Delta_zT[index_+1], Delta_zB[index_+1]);

	  /* Check if depsdy_P or depsdy_E needs to be corrected */

	  /* Case W-N */
	  if(dom_matrix[index_+Nx]){
	    depsdy_P = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_],
				       eps_turb[index_-Nx], Delta_yN[index_], Delta_yS[index_]); 
	  }

	  if(dom_matrix[index_+Nx+1]){
	    depsdy_E = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+1],
				       eps_turb[index_+1-Nx], Delta_yN[index_+1], Delta_yS[index_+1]);
	  }

	  /* Case W-S */
	  if(dom_matrix[index_-Nx]){
	    depsdy_P = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_+Nx],
				       eps_turb[index_], Delta_yS[index_], Delta_yN[index_]);
	  }

	  if(dom_matrix[index_-Nx+1]){
	    depsdy_E = __central_b_der(eps_y[index_+1], depsdyb[count], eps_turb[index_+1+Nx],
				       eps_turb[index_+1], Delta_yS[index_+1], Delta_yN[index_+1]);
	  }

	  /* Check if depsdz_P or depsdz_E needs to be corrected */

	  /* Case W-T */
	  if(dom_matrix[index_+Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_],
				       eps_turb[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	  }

	  if(dom_matrix[index_+Nx*Ny+1]){
	    depsdz_E = __central_b_der(eps_z[index_+1], depsdzb[count], eps_turb[index_+1],
				       eps_turb[index_+1-Nx*Ny], Delta_zT[index_+1], Delta_zB[index_+1]);
	  }

	  /* Case W-B */
	  if(dom_matrix[index_-Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_+Nx*Ny],
				       eps_turb[index_], Delta_zB[index_], Delta_zT[index_]);
	  }

	  if(dom_matrix[index_-Nx*Ny+1]){
	    depsdz_E = __central_b_der(eps_z[index_+1], depsdzb[count], eps_turb[index_+1+Nx*Ny],
				       eps_turb[index_+1], Delta_zB[index_+1], Delta_zT[index_+1]);
	  }
	  
	}/* End case W-P */

	/*******************************************************************/		
	/* N-P line intersects with curve */
	if(dom_matrix[index_+Nx] && !cell_done_OK){
	  
	  cell_done_OK = 1;
	  depsdy_P = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_],
				     eps_turb[index_-Nx], Delta_yN[index_], Delta_yS[index_]);

	  depsdy_S = __central_der(eps_turb[index_], eps_turb[index_-Nx], eps_turb[index_-2*Nx],
				   Delta_yN[index_-Nx], Delta_yS[index_-Nx]);

	  /* Case N-T */
	  if(dom_matrix[index_+Nx*Ny]){
	    
	    /* Derivatives required for T case at Bottom position:
	     depsdx_B, depsdy_B, depsdz_B */
	    if(dom_matrix[index_-Nx*Ny+1]){
	      
	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-1]){

	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny+1],
					 eps_turb[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	    }
	    else{

	      depsdx_B = __central_der(eps_turb[index_-Nx*Ny+1], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }
	    
	    if(dom_matrix[index_-Nx*Ny+Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny+Nx],
					 eps_turb[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	    }
	    else{
	      
	      depsdy_B = __central_der(eps_turb[index_-Nx*Ny+Nx], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }
	    
	    depsdz_B = __central_der(eps_turb[index_], eps_turb[index_-Nx*Ny], eps_turb[index_-2*Nx*Ny],
				     Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	  }

	  /* Case N-B */
	  if(dom_matrix[index_-Nx*Ny]){

	    /* Derivatives required for B case at Top position:
	     depsdx_T, depsdy_T, depsdz_T */
	    if(dom_matrix[index_+Nx*Ny+1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny+1],
					 eps_turb[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	    }
	    else{

	      depsdx_T = __central_der(eps_turb[index_+Nx*Ny+1], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }
	    
	    if(dom_matrix[index_+Nx*Ny+Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny+Nx],
					 eps_turb[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	    }
	    else{

	      depsdy_T = __central_der(eps_turb[index_+Nx*Ny+Nx], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }

	    depsdz_T = __central_der(eps_turb[index_+2*Nx*Ny], eps_turb[index_+Nx*Ny], eps_turb[index_],
				     Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);
	  }

	  /* Derivatives required for N case */
	  
	  depsdx_P = __central_der(eps_turb[index_+1], eps_turb[index_], eps_turb[index_-1],
				   Delta_xE[index_], Delta_xW[index_]);
	  depsdx_S = __central_der(eps_turb[index_-Nx+1], eps_turb[index_-Nx], eps_turb[index_-Nx-1],
				   Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  depsdz_P = __central_der(eps_turb[index_+Nx*Ny], eps_turb[index_], eps_turb[index_-Nx*Ny],
				   Delta_zT[index_], Delta_zB[index_]);
	  depsdz_S = __central_der(eps_turb[index_-Nx+Nx*Ny], eps_turb[index_-Nx], eps_turb[index_-Nx-Nx*Ny],
				   Delta_zT[index_-Nx], Delta_zB[index_-Nx]);

	  /* Check if depsdx_S needs to be corrected */
	  if(dom_matrix[index_-Nx+1]){
	    depsdx_S = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx],
				       eps_turb[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }

	  if(dom_matrix[index_-Nx-1]){
	    depsdx_S = __central_b_der(eps_x[index_-Nx], depsdxb[count], eps_turb[index_-Nx+1],
				       eps_turb[index_-Nx], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	  }

	  /* Check if depsdz_P or depsdz_S needs to be corrected */

	  /* Case N-T */
	  if(dom_matrix[index_+Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_],
				       eps_turb[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	  }

	  if(dom_matrix[index_+Nx*Ny-Nx]){
	    depsdz_S = __central_b_der(eps_z[index_-Nx], depsdzb[count], eps_turb[index_-Nx],
				       eps_turb[index_-Nx-Nx*Ny], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	  }

	  /* Case N-B */
	  if(dom_matrix[index_-Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_+Nx*Ny],
			  eps_turb[index_], Delta_zB[index_], Delta_zT[index_]);
	  }

	  if(dom_matrix[index_-Nx*Ny-Nx]){
	    depsdz_S = __central_b_der(eps_z[index_-Nx], depsdzb[count], eps_turb[index_-Nx+Nx*Ny],
				       eps_turb[index_-Nx], Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	  }
	} /* End case N-P */

	/*******************************************************************/		
	/* S-P line intersects with curve */
	if(dom_matrix[index_-Nx] && !cell_done_OK){

	  cell_done_OK = 1;
	  depsdy_P = __central_b_der(eps_y[index_], depsdyb[count], eps_turb[index_+Nx],
				     eps_turb[index_], Delta_yS[index_], Delta_yN[index_]);

	  depsdy_N = __central_der(eps_turb[index_+2*Nx], eps_turb[index_+Nx], eps_turb[index_],
				   Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

	  /* Case S-T */
	  if(dom_matrix[index_+Nx*Ny]){

	    /* Derivatives required for T case at Bottom position:
	     depsdx_T, depsdy_T, depsdz_T */
	    if(dom_matrix[index_-Nx*Ny+1]){

	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-1]){

	      depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny+1],
					 eps_turb[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	    }
	    else{

	      depsdx_B = __central_der(eps_turb[index_-Nx*Ny+1], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	    }

	    if(dom_matrix[index_-Nx*Ny+Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny],
					 eps_turb[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }
	    else if(dom_matrix[index_-Nx*Ny-Nx]){

	      depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny+Nx],
					 eps_turb[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	    }
	    else{

	      depsdy_B = __central_der(eps_turb[index_-Nx*Ny+Nx], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	    }

	    depsdz_B = __central_der(eps_turb[index_], eps_turb[index_-Nx*Ny], eps_turb[index_-2*Nx*Ny],
				     Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	  }


	  /* Case N-B */
	  if(dom_matrix[index_-Nx*Ny]){

	    /* Derivatives required for B case at Top position:
	       depsdx_T, depsdy_T, depsdz_T */
	    if(dom_matrix[index_+Nx*Ny+1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-1]){

	      depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny+1],
					 eps_turb[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	    }
	    else{
	      
	      depsdx_T = __central_der(eps_turb[index_+Nx*Ny+1], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	    }

	    if(dom_matrix[index_+Nx*Ny+Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny],
					 eps_turb[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }
	    else if(dom_matrix[index_+Nx*Ny-Nx]){

	      depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny+Nx],
					 eps_turb[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	    }
	    else{

	      depsdy_T = __central_der(eps_turb[index_+Nx*Ny+Nx], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	    }

	    depsdz_T = __central_der(eps_turb[index_+2*Nx*Ny], eps_turb[index_+Nx*Ny], eps_turb[index_],
				     Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);

	  }

	  /* Derivatives required for S case */

	  depsdx_P = __central_der(eps_turb[index_+1], eps_turb[index_], eps_turb[index_-1],
				   Delta_xE[index_], Delta_xW[index_]);
	  depsdx_N = __central_der(eps_turb[index_+Nx+1], eps_turb[index_+Nx], eps_turb[index_+Nx-1],
				   Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  depsdz_P = __central_der(eps_turb[index_+Nx*Ny], eps_turb[index_], eps_turb[index_-Nx*Ny],
				   Delta_zT[index_], Delta_zB[index_]);
	  depsdz_N = __central_der(eps_turb[index_+Nx+Nx*Ny], eps_turb[index_+Nx], eps_turb[index_+Nx-Nx*Ny],
				   Delta_zT[index_+Nx], Delta_zB[index_+Nx]);

	  /* Check if depsdx_N needs to be corrected */
	  if(dom_matrix[index_+Nx+1]){
	    depsdx_N = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx],
				       eps_turb[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }

	  if(dom_matrix[index_+Nx-1]){
	    depsdx_N = __central_b_der(eps_x[index_+Nx], depsdxb[count], eps_turb[index_+Nx+1],
			  eps_turb[index_+Nx], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	  }

	  /* Check if depsdz_P or depsdz_N needs to be corrected */

	  /* Case S-T */
	  if(dom_matrix[index_+Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_],
				       eps_turb[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	  }

	  if(dom_matrix[index_+Nx*Ny+Nx]){
	    depsdz_N = __central_b_der(eps_z[index_+Nx], depsdzb[count], eps_turb[index_+Nx],
				       eps_turb[index_+Nx-Nx*Ny], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	  }

	  /* Case S-B */
	  if(dom_matrix[index_-Nx*Ny]){
	    depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_+Nx*Ny],
				       eps_turb[index_], Delta_zB[index_], Delta_zT[index_]);
	  }

	  if(dom_matrix[index_-Nx*Ny+Nx]){
	    depsdz_N = __central_b_der(eps_z[index_+Nx], depsdzb[count], eps_turb[index_+Nx+Nx*Ny],
			  eps_turb[index_+Nx], Delta_zB[index_+Nx], Delta_zT[index_+Nx]);
	  }
	} /* End case S-P */

	/*******************************************************************/			
	/* T-P line intersects with curve */
	if(dom_matrix[index_+Nx*Ny] && !cell_done_OK){

	  cell_done_OK = 1;
	  depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_],
				     eps_turb[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	  depsdz_B = __central_der(eps_turb[index_], eps_turb[index_-Nx*Ny], eps_turb[index_-2*Nx*Ny],
				   Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);

	  /* Derivatives required for T case */

	  depsdx_P = __central_der(eps_turb[index_+1], eps_turb[index_], eps_turb[index_-1],
				   Delta_xE[index_], Delta_xW[index_]);
	  depsdx_B = __central_der(eps_turb[index_-Nx*Ny+1], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-1],
				   Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  depsdy_P = __central_der(eps_turb[index_+Nx], eps_turb[index_], eps_turb[index_-Nx],
				   Delta_yN[index_], Delta_yS[index_]);
	  depsdy_B = __central_der(eps_turb[index_-Nx*Ny+Nx], eps_turb[index_-Nx*Ny], eps_turb[index_-Nx*Ny-Nx],
				   Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);

	  /* Check if depsdx_B needs to be corrected */
	  if(dom_matrix[index_-Nx*Ny+1]){
	    depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny],
				       eps_turb[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }

	  if(dom_matrix[index_-Nx*Ny-1]){
	    depsdx_B = __central_b_der(eps_x[index_-Nx*Ny], depsdxb[count], eps_turb[index_-Nx*Ny+1],
				       eps_turb[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	  }

	  /* Check if depsdy_B needs to be corrected */
	  if(dom_matrix[index_-Nx*Ny+Nx]){
	    depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny],
				       eps_turb[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }

	  if(dom_matrix[index_-Nx*Ny-Nx]){
	    depsdy_B = __central_b_der(eps_y[index_-Nx*Ny], depsdyb[count], eps_turb[index_-Nx*Ny+Nx],
				       eps_turb[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	  }
	} /* End case T-P */

	/*******************************************************************/			
	/* B-P line intersects with curve */
	if(dom_matrix[index_-Nx*Ny] && !cell_done_OK){

	  cell_done_OK = 1;
	  depsdz_P = __central_b_der(eps_z[index_], depsdzb[count], eps_turb[index_+Nx*Ny],
				     eps_turb[index_], Delta_zB[index_], Delta_zT[index_]);
	  depsdz_T = __central_der(eps_turb[index_+2*Nx*Ny], eps_turb[index_+Nx*Ny], eps_turb[index_],
				   Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);

	  /* Derivatives required for B case */

	  depsdx_P = __central_der(eps_turb[index_+1], eps_turb[index_], eps_turb[index_-1],
				   Delta_xE[index_], Delta_xW[index_]);
	  depsdx_T = __central_der(eps_turb[index_+Nx*Ny+1], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-1],
				   Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  depsdy_P = __central_der(eps_turb[index_+Nx], eps_turb[index_], eps_turb[index_-Nx],
				   Delta_yN[index_], Delta_yS[index_]);
	  depsdy_T = __central_der(eps_turb[index_+Nx*Ny+Nx], eps_turb[index_+Nx*Ny], eps_turb[index_+Nx*Ny-Nx],
				   Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);

	  /* Check if depsdx_T needs to be corrected */
	  if(dom_matrix[index_+Nx*Ny+1]){
	    depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny],
				       eps_turb[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }

	  if(dom_matrix[index_+Nx*Ny-1]){
	    depsdx_T = __central_b_der(eps_x[index_+Nx*Ny], depsdxb[count], eps_turb[index_+Nx*Ny+1],
				       eps_turb[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	  }

	  /* Check if depsdy_T needs to be corrected */
	  if(dom_matrix[index_+Nx*Ny+Nx]){
	    depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny],
				       eps_turb[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }

	  if(dom_matrix[index_+Nx*Ny-Nx]){
	    depsdy_T = __central_b_der(eps_y[index_+Nx*Ny], depsdyb[count], eps_turb[index_+Nx*Ny+Nx],
				       eps_turb[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	  }
	} /* End case B-P */	

	/* Update the value of depsdxb */
	if(dom_matrix[index_-1] || dom_matrix[index_+1]){

	  /* Eq 27 Sato */
	  depsdS_P = depsdx_P * par_vec_x_x[index_] +
	    depsdy_P * par_vec_x_y[index_] + depsdz_P * par_vec_x_z[index_];
	  depsdS_2_P = depsdx_P * par_vec_x_x_2[index_] +
	    depsdy_P * par_vec_x_y_2[index_] + depsdz_P * par_vec_x_z_2[index_];	  
	  
	  if(dom_matrix[index_-1]){
	    depsdS_E = depsdx_E * par_vec_x_x[index_] +
	      depsdy_E * par_vec_x_y[index_] + depsdz_E * par_vec_x_z[index_];
	    depsdS_2_E = depsdx_E * par_vec_x_x_2[index_] +
	      depsdy_E * par_vec_x_y_2[index_] + depsdz_E * par_vec_x_z_2[index_];
	    
	    /* Ec 28 Sato */
	    depsdSface_x = __linear_extrap_to_b(depsdS_P, depsdS_E, eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	    depsdS_2face_x = __linear_extrap_to_b(depsdS_2_P, depsdS_2_E, eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	  }
	  else{
	    depsdS_W = depsdx_W * par_vec_x_x[index_] +
	      depsdy_W * par_vec_x_y[index_] + depsdz_W * par_vec_x_z[index_];
	    depsdS_2_W = depsdx_W * par_vec_x_x_2[index_] +
	      depsdy_W * par_vec_x_y_2[index_] + depsdz_W * par_vec_x_z_2[index_];

	    /* Ec 28 Sato */
	    depsdSface_x = __linear_extrap_to_b(depsdS_P, depsdS_W, eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	    depsdS_2face_x = __linear_extrap_to_b(depsdS_2_P, depsdS_2_W, eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	  }
	  /* Ec 24 Sato */
	  depsdxb[count] = depsdNb * norm_vec_x_x[index_] +
	    depsdSface_x * par_vec_x_x[index_] + depsdS_2face_x * par_vec_x_x_2[index_];
	}
	/* Update the value of depsdyb */
	if(dom_matrix[index_-Nx] || dom_matrix[index_+Nx]){

	  /* Eq 27 Sato */
	  depsdS_P = depsdx_P * par_vec_y_x[index_] +
	    depsdy_P * par_vec_y_y[index_] + depsdz_P * par_vec_y_z[index_];
	  depsdS_2_P = depsdx_P * par_vec_y_x_2[index_] +
	    depsdy_P * par_vec_y_y_2[index_] + depsdz_P * par_vec_y_z_2[index_];	  

	  if(dom_matrix[index_-Nx]){
	    depsdS_N = depsdx_N * par_vec_y_x[index_]  +
	      depsdy_N * par_vec_y_y[index_] + depsdz_N * par_vec_y_z[index_];
	    depsdS_2_N = depsdx_N * par_vec_y_x_2[index_] +
	      depsdy_N * par_vec_y_y_2[index_] + depsdz_N * par_vec_y_z_2[index_];

	    /* Ec 28 Sato */
	    depsdSface_y = __linear_extrap_to_b(depsdS_P, depsdS_N, eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	    depsdS_2face_y = __linear_extrap_to_b(depsdS_2_P, depsdS_2_N, eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	  }
	  else{
	    depsdS_S = depsdx_S * par_vec_y_x[index_] +
	      depsdy_S * par_vec_y_y[index_] + depsdz_S * par_vec_y_z[index_];
	    depsdS_2_S = depsdx_S * par_vec_y_x_2[index_] +
	      depsdy_S * par_vec_y_y_2[index_] + depsdz_S * par_vec_y_z_2[index_];

	    /* Ec 28 Sato */
	    depsdSface_y = __linear_extrap_to_b(depsdS_P, depsdS_S, eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
	    depsdS_2face_y = __linear_extrap_to_b(depsdS_2_P, depsdS_2_S, eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
	  }
	  /* Ec 24 Sato */
	  depsdyb[count] = depsdNb * norm_vec_y_y[index_] +
	    depsdSface_y * par_vec_y_y[index_] + depsdS_2face_y * par_vec_y_y_2[index_];
	}
	/* Update the value of depsdzb */
	if(dom_matrix[index_-Nx*Ny] || dom_matrix[index_+Nx*Ny]){

	/* Eq 27 Sato */
	  depsdS_P = depsdx_P * par_vec_z_x[index_] +
	    depsdy_P * par_vec_z_y[index_] + depsdz_P * par_vec_z_z[index_];
	  depsdS_2_P = depsdx_P * par_vec_z_x_2[index_] +
	    depsdy_P * par_vec_z_y_2[index_] + depsdz_P * par_vec_z_z_2[index_];	  

	  if(dom_matrix[index_-Nx*Ny]){
	    depsdS_T = depsdx_T * par_vec_z_x[index_] +
	      depsdy_T * par_vec_z_y[index_] + depsdz_T * par_vec_z_z[index_];
	    depsdS_2_T = depsdx_T * par_vec_z_x_2[index_] +
	      depsdy_T * par_vec_z_y_2[index_] + depsdz_T * par_vec_z_z_2[index_];

	    /* Ec 28 Sato */
	    depsdSface_z = __linear_extrap_to_b(depsdS_P, depsdS_T, eps_z[index_], Delta_zB[index_], Delta_zT[index_]);
	    depsdS_2face_z = __linear_extrap_to_b(depsdS_2_P, depsdS_2_T, eps_z[index_], Delta_zB[index_], Delta_zT[index_]);	    
	  }
	  else{
	    depsdS_B = depsdx_B * par_vec_z_x[index_] +
	      depsdy_B * par_vec_z_y[index_] + depsdz_B * par_vec_z_z[index_];
	    depsdS_2_B = depsdx_B * par_vec_z_x_2[index_] +
	      depsdy_B * par_vec_z_y_2[index_] + depsdz_B * par_vec_z_z_2[index_];

	    /* Ec 28 Sato */
	    depsdSface_z = __linear_extrap_to_b(depsdS_P, depsdS_B, eps_z[index_], Delta_zT[index_], Delta_zB[index_]);
	    depsdS_2face_z = __linear_extrap_to_b(depsdS_2_P, depsdS_2_B, eps_z[index_], Delta_zT[index_], Delta_zB[index_]);
	  }
	  /* Ec 24 Sato */
	  depsdzb[count] = depsdNb * norm_vec_z_z[index_] +
	    depsdSface_z * par_vec_z_z[index_] + depsdS_2face_z * par_vec_z_z_2[index_];
	}
	
      } /* for(count=0; count<turb_boundary_cell_count; count++) */
    } /* for(iter_count=0; iter_count<iter_total; iter_count++) */

    /* We only need to calculate J after convergence */
    
    for(count=0; count<turb_boundary_cell_count; count++){
      
      index_ = turb_boundary_indexs[count];      
      I = turb_boundary_I[count];
      J = turb_boundary_J[count];
      K = turb_boundary_K[count];
      i = I - 1;
      j = J - 1;
      k = K - 1;

      index_u = K*nx*Ny + J*nx + i;
      index_v = K*Nx*ny + j*Nx + I;
      index_w = k*Nx*Ny + J*Nx + I;

      Jx = 0;
      Jy = 0;
      Jz = 0;

      mu_eff = mu + mu_turb[index_] / C_sigma_eps;

      /* Convective part at the face */
      if(dom_matrix[index_-1]){
	Da = eps_x[index_] * Delta_xW[index_] + Delta_xe[index_];
	mx = Delta_x[index_] / Da;
	Few = rho * u[index_u];
	eps_m_ew = eps_turb[index_] - 0.5 * Delta_x[index_] * depsdxb[count];
      }

      if(dom_matrix[index_+1]){
	Da = eps_x[index_] * Delta_xE[index_] + Delta_xw[index_];
	mx = Delta_x[index_] / Da;
	Few = rho * u[index_u+1];
	eps_m_ew = eps_turb[index_] + 0.5 * Delta_x[index_] * depsdxb[count];
      }

      if(dom_matrix[index_-Nx]){
	Da = eps_y[index_] * Delta_yS[index_] + Delta_yn[index_];
	my = Delta_y[index_] / Da;
	Fns = rho * v[index_v];
	eps_m_ns = eps_turb[index_] - 0.5 * Delta_y[index_] * depsdyb[count];
      }

      if(dom_matrix[index_+Nx]){
	Da = eps_y[index_] * Delta_yN[index_] + Delta_ys[index_];
	my = Delta_y[index_] / Da;
	Fns = rho * v[index_+Nx];
	eps_m_ns = eps_turb[index_] + 0.5 * Delta_y[index_] * depsdyb[count];
      }

      if(dom_matrix[index_-Nx*Ny]){
	Da = eps_z[index_] * Delta_zB[index_] + Delta_zt[index_];
	mz = Delta_z[index_] / Da;
	Ftb = rho * w[index_w];
	eps_m_tb = eps_turb[index_] - 0.5 * Delta_z[index_] * depsdzb[count];
      }

      if(dom_matrix[index_+Nx*Ny]){
	Da = eps_z[index_] * Delta_zT[index_] + Delta_zb[index_];
	mz = Delta_z[index_] / Da;
	Ftb = rho * w[index_w+Nx*Ny];
	eps_m_tb = eps_turb[index_] + 0.5 * Delta_z[index_] * depsdzb[count];
      }

      /* Build the combined fluxes. It must be noted that there are different lengths
	 for the molecular and convective parts which are taken into account by 
	 the eps factors */
      Jx = - mu_eff * mx * depsdxb[count] + Few * eps_m_ew;
      Jy = - mu_eff * my * depsdyb[count] + Fns * eps_m_ns;
      Jz = - mu_eff * mz * depsdzb[count] + Ftb * eps_m_tb;

      
      W_fluid = W_is_fluid_OK[index_];
      E_fluid = E_is_fluid_OK[index_];
      S_fluid = S_is_fluid_OK[index_];
      N_fluid = N_is_fluid_OK[index_];
      B_fluid = B_is_fluid_OK[index_];
      T_fluid = T_is_fluid_OK[index_];

      SC_vol_eps_turb[count] =
	(-1) * (1 - E_fluid) * Jx * faces_x[index_u+1] +
	(-1) * (1 - N_fluid) * Jy * faces_y[index_v+Nx] +
	(-1) * (1 - T_fluid) * Jz * faces_z[index_w+Nx*Ny] +
	(1 - W_fluid) * Jx * faces_x[index_u] +
	(1 - S_fluid) * Jy * faces_y[index_v] +
	(1 - B_fluid) * Jz * faces_z[index_w];				          
    } /* for(count=0; count<turb_boundary_cell_count; count++) */
  }
  

  return;
}

/* SET_BC_TURB_K_3D */
/*****************************************************************************/
/**
  
  Sets:

  Struct coeffs_k: coefficients for boundary values, according to specified 
  boundary conditions

  @param data

  @return void
  
  MODIFIED: 27-03-2019
*/
void set_bc_turb_k_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  int index_aux;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int *dom_matrix = data->domain_matrix;

  double *k_aP = data->coeffs_k.aP;
  double *k_aW = data->coeffs_k.aW;
  double *k_aE = data->coeffs_k.aE;
  double *k_aS = data->coeffs_k.aS;
  double *k_aN = data->coeffs_k.aN;
  double *k_aB = data->coeffs_k.aB;
  double *k_aT = data->coeffs_k.aT;
  double *k_b = data->coeffs_k.b;

  const double *k_turb = data->k_turb;
  const double *k_W_non_uni = data->k_W_non_uni;
  const double *k_E_non_uni = data->k_E_non_uni;
  const double *k_S_non_uni = data->k_S_non_uni;
  const double *k_N_non_uni = data->k_N_non_uni;
  const double *k_B_non_uni = data->k_B_non_uni;
  const double *k_T_non_uni = data->k_T_non_uni;
  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *fn = data->fn;
  const double *fe = data->fe;
  const double *ft = data->ft;

  const b_cond_flow *bc_w = &(data->bc_flow_west);
  const b_cond_flow *bc_e = &(data->bc_flow_east);
  const b_cond_flow *bc_s = &(data->bc_flow_south);
  const b_cond_flow *bc_n = &(data->bc_flow_north);
  const b_cond_flow *bc_b = &(data->bc_flow_bottom);
  const b_cond_flow *bc_t = &(data->bc_flow_top);


#pragma omp parallel sections default(none) private(I, J, K, i, j, k, index_, index_u, index_v, index_w, index_aux) \
  shared(dom_matrix, k_W_non_uni, k_E_non_uni, k_S_non_uni, k_N_non_uni, k_B_non_uni, k_T_non_uni, k_turb, \
  k_aP, k_aW, k_aE, k_aS, k_aN, k_aB, k_aT, k_b, u, v, w, fe, fn, ft, \
	 bc_w, bc_e, bc_s, bc_n, bc_b, bc_t)
  {

#pragma omp section
    {
      /* South boundary */
      J = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */

      if((bc_s->type == 0) || (bc_s->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_aN[index_] = 1;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}	
      }

      /* Type = 1, Fixed value */
      else if(bc_s->type == 1){

	if(bc_s->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;
	      k_b[index_] = k_S_non_uni[K*Nx + I];
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		k_b[index_] = bc_s->k_value;
	      }
	      else{
		k_b[index_] = 0;
	      }
	    }
	  }	  
	}
      }

      /* Type = 3, Wall */
      else if(bc_s->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = 0;
	  }
	}	
      }

      /* Type = 4, Constant pressure */
      else if(bc_s->type == 4){

	j = 0;

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_v = K*Nx*ny + j*Nx + I;

	      if(v[index_v] > 0){
		k_b[index_] = k_S_non_uni[K*Nx+I];
		k_aN[index_] = 0;
	      }
	      else{
		k_b[index_] = 0;
		k_aN[index_] = 1;
	      }
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_s->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + (Ny-3)*Nx + I;
	      k_b[index_] = k_turb[index_aux] * fn[index_aux] +
		k_turb[index_aux + Nx] * (1 - fn[index_aux]);
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      
    } /* South section */

    
#pragma omp section
    {
      /* North boundary */
      J = Ny - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */

      if((bc_n->type == 0) || (bc_n->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_aS[index_] = 1;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}	
      }

      /* Type = 1, Fixed value */
      else if(bc_n->type == 1){

	if(bc_n->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;
	      k_b[index_] = k_N_non_uni[K*Nx + I];
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		k_b[index_] = bc_n->k_value;
	      }
	      else{
		k_b[index_] = 0;
	      }
	    }
	  }	  
	}
      }

      /* Type = 3, Wall */
      else if(bc_n->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = 0;
	  }
	}	
      }

      /* Type = 4, Constant pressure */
      else if(bc_n->type == 4){

	j = ny - 1;

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_v = K*Nx*ny + j*Nx + I;

	      if(v[index_v] < 0){
		k_b[index_] = k_N_non_uni[K*Nx + I];
		k_aS[index_] = 0;
	      }
	      else{
		k_b[index_] = 0;
		k_aS[index_] = 1;
	      }
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_n->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + Nx + I;
	      k_b[index_] = k_turb[index_aux] * fn[index_aux] +
		k_turb[index_aux + Nx] * (1 - fn[index_aux]);
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      
    } /* North section */
  
#pragma omp section
    {
      /* West boundary */
      I = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */

      if((bc_w->type == 0) || (bc_w->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_aE[index_] = 1;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}	
      }

      /* Type = 1, Fixed value */
      else if(bc_w->type == 1){

	if(bc_w->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;
	      k_b[index_] = k_W_non_uni[K*Ny + J];
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		k_b[index_] = bc_w->k_value;
	      }
	      else{
		k_b[index_] = 0;
	      }
	    }
	  }	  
	}
      }

      /* Type = 3, Wall */
      else if(bc_w->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = 0;
	  }
	}	
      }

      /* Type = 4, Constant pressure */
      else if(bc_w->type == 4){

	i = 0;

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_u = K*nx*Ny + J*nx + i;

	      if(u[index_u] > 0){
		k_b[index_] = k_W_non_uni[K*Ny+J];
		k_aE[index_] = 0;
	      }
	      else{
		k_b[index_] = 0;
		k_aE[index_] = 1;
	      }
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_w->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + J*Nx + Nx-3;
	      k_b[index_] = k_turb[index_aux] * fe[index_aux] +
		k_turb[index_aux + 1] * (1 - fe[index_aux]);
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      
    } /* West section */

    
#pragma omp section
    {
      /* East boundary */
      I = Nx - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */

      if((bc_e->type == 0) || (bc_e->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_aW[index_] = 1;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}	
      }

      /* Type = 1, Fixed value */
      else if(bc_e->type == 1){

	if(bc_e->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;
	      k_b[index_] = k_E_non_uni[K*Ny + J];
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		k_b[index_] = bc_e->k_value;
	      }
	      else{
		k_b[index_] = 0;
	      }
	    }
	  }	  
	}
      }

      /* Type = 3, Wall */
      else if(bc_e->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = 0;
	  }
	}	
      }

      /* Type = 4, Constant pressure */
      else if(bc_e->type == 4){

	i = nx - 1;

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_u = K*nx*Ny + J*nx + i;

	      if(u[index_u] < 0){
		k_b[index_] = k_E_non_uni[K*Ny + J];
		k_aW[index_] = 0;
	      }
	      else{
		k_b[index_] = 0;
		k_aW[index_] = 1;
	      }
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_e->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + Nx + 1;
	      k_b[index_] = k_turb[index_aux] * fe[index_aux] +
		k_turb[index_aux + 1] * (1 - fe[index_aux]);
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      
    } /* East section */

#pragma omp section
    {
      /* Bottom boundary */
      K = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */

      if((bc_b->type == 0) || (bc_b->type == 2)){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_aT[index_] = 1;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}	
      }

      /* Type = 1, Fixed value */
      else if(bc_b->type == 1){

	if(bc_b->parabolize_profile_OK){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;
	      k_b[index_] = k_B_non_uni[J*Nx + I];
	    }
	  }
	}
	else{

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		k_b[index_] = bc_b->k_value;
	      }
	      else{
		k_b[index_] = 0;
	      }
	    }
	  }	  
	}
      }

      /* Type = 3, Wall */
      else if(bc_b->type == 3){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = 0;
	  }
	}	
      }

      /* Type = 4, Constant pressure */
      else if(bc_b->type == 4){

	k = 0;

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_w = k*Nx*Ny + J*Nx + I;

	      if(w[index_w] > 0){
		k_b[index_] = k_B_non_uni[J*Nx + I];
		k_aT[index_] = 0;
	      }
	      else{
		k_b[index_] = 0;
		k_aT[index_] = 1;
	      }
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_b->type == 5){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = (Nz-3)*Nx*Ny + J*Nx + I;
	      k_b[index_] = k_turb[index_aux] * ft[index_aux] +
		k_turb[index_aux + Nx*Ny] * (1 - ft[index_aux]);
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      
    } /* Bottom section */

    
#pragma omp section
    {
      /* Top boundary */
      K = Nz - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */

      if((bc_t->type == 0) || (bc_t->type == 2)){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      k_aB[index_] = 1;
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}	
      }

      /* Type = 1, Fixed value */
      else if(bc_t->type == 1){

	if(bc_t->parabolize_profile_OK){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;
	      k_b[index_] = k_T_non_uni[J*Nx + I];
	    }
	  }
	}
	else{

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      k_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		k_b[index_] = bc_t->k_value;
	      }
	      else{
		k_b[index_] = 0;
	      }
	    }
	  }	  
	}
      }

      /* Type = 3, Wall */
      else if(bc_t->type == 3){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;
	    k_b[index_] = 0;
	  }
	}	
      }

      /* Type = 4, Constant pressure */
      else if(bc_t->type == 4){

	k = nz - 1;

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_w = k*Nx*Ny + J*Nx + I;

	      if(w[index_w] < 0){
		k_b[index_] = k_T_non_uni[J*Nx + I];
		k_aB[index_] = 0;
	      }
	      else{
		k_b[index_] = 0;
		k_aB[index_] = 1;
	      }
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_t->type == 5){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    k_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = 1*Nx*Ny + Nx + I;
	      k_b[index_] = k_turb[index_aux] * ft[index_aux] +
		k_turb[index_aux + Nx*Ny] * (1 - ft[index_aux]);
	    }
	    else{
	      k_b[index_] = 0;
	    }
	  }
	}
      }
      
    } /* Top section */
      
  } /* sections */    
  
  return;
}
  
/* SET_BC_TURB_EPS_3D */
/*****************************************************************************/
/**
  
  Sets:

  Struct coeffs_eps: coefficients for boundary values, according to 
  specified boundary conditions

  @param data

  @return void
  
  MODIFIED: 27-03-2019
*/
void set_bc_turb_eps_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  int index_aux;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int turb_model = data->turb_model;

  const int *dom_matrix = data->domain_matrix;

  const double mu = data->mu;
  const double rho = data->rho_v[0];

  double *eps_aP = data->coeffs_eps.aP;
  double *eps_aW = data->coeffs_eps.aW;
  double *eps_aE = data->coeffs_eps.aE;
  double *eps_aS = data->coeffs_eps.aS;
  double *eps_aN = data->coeffs_eps.aN;
  double *eps_aB = data->coeffs_eps.aB;
  double *eps_aT = data->coeffs_eps.aT;
  double *eps_b = data->coeffs_eps.b;

  const double *k_turb = data->k_turb;
  const double *eps_turb = data->eps_turb;
  const double *y_wall = data->y_wall;
  const double *eps_W_non_uni = data->eps_W_non_uni;
  const double *eps_E_non_uni = data->eps_E_non_uni;
  const double *eps_S_non_uni = data->eps_S_non_uni;
  const double *eps_N_non_uni = data->eps_N_non_uni;
  const double *eps_B_non_uni = data->eps_B_non_uni;
  const double *eps_T_non_uni = data->eps_T_non_uni;
  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *fn = data->fn;
  const double *fe = data->fn;
  const double *ft = data->ft;

  const b_cond_flow *bc_w = &(data->bc_flow_west);
  const b_cond_flow *bc_e = &(data->bc_flow_east);
  const b_cond_flow *bc_s = &(data->bc_flow_south);
  const b_cond_flow *bc_n = &(data->bc_flow_north);
  const b_cond_flow *bc_b = &(data->bc_flow_bottom);
  const b_cond_flow *bc_t = &(data->bc_flow_top);


#pragma omp parallel sections default(none) private(I, J, K, i, j, k, index_, index_u, index_v, index_w, index_aux) \
  shared(dom_matrix, eps_W_non_uni, eps_E_non_uni, eps_S_non_uni, eps_N_non_uni, eps_B_non_uni, \
  eps_T_non_uni, y_wall, k_turb, eps_turb, eps_aP, eps_aW, eps_aE, eps_aS, eps_aN, eps_aB, \
  eps_aT, eps_b, u, v, w, fn, fe, ft, bc_w, bc_e, bc_s, bc_n, bc_b, bc_t)
  
  {

#pragma omp section
    {
      /* South boundary */
      J = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_s->type == 0) || (bc_s->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_aN[index_] = 1;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_s->type == 1){
	if(bc_s->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;
	      eps_b[index_] = eps_S_non_uni[K*Nx + I];   		    
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		eps_b[index_] = bc_s->eps_value;
	      }
	      else{
		eps_b[index_] = 0;
	      }
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_s->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      /* AKN model */
	      if(turb_model == 1){
		if(y_wall[index_+Nx] == 0){
		  eps_b[index_] = 0;
		}
		else{
		  eps_b[index_] = 2 * mu * k_turb[index_+Nx] /
		    (rho * y_wall[index_+Nx] * y_wall[index_+Nx]);
		}
	      }
	      /* CG model */
	      else if(turb_model == 2){
		eps_aN[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 4, Constant pressure */
      else if(bc_s->type == 4){

	j = 0;

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_v = K*Nx*ny + j*Nx + I;

	      if(v[index_v] > 0){
		eps_b[index_] = eps_S_non_uni[K*Nx + I];
		eps_aN[index_] = 0;
	      }
	      else{
		eps_b[index_] = 0;
		eps_aN[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_s->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + (Ny-3)*Nx + I;
	      eps_b[index_] = eps_turb[index_aux] * fn[index_aux] +
		eps_turb[index_aux + Nx] * (1 - fn[index_aux]);			  
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}	
      }
      
    } /* South section */

#pragma omp section
    {
      /* North boundary */
      J = Ny - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_n->type == 0) || (bc_n->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_aS[index_] = 1;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_n->type == 1){
	if(bc_n->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;
	      eps_b[index_] = eps_N_non_uni[K*Nx + I];   		    
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		eps_b[index_] = bc_n->eps_value;
	      }
	      else{
		eps_b[index_] = 0;
	      }
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_n->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      /* AKN model */
	      if(turb_model == 1){
		if(y_wall[index_-Nx] == 0){
		  eps_b[index_] = 0;
		}
		else{
		  eps_b[index_] = 2 * mu * k_turb[index_-Nx] /
		    (rho * y_wall[index_-Nx] * y_wall[index_-Nx]);
		}
	      }
	      /* CG model */
	      else if(turb_model == 2){
		eps_aS[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 4, Constant pressure */
      else if(bc_n->type == 4){

	j = ny - 1;

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_v = K*Nx*ny + j*Nx + I;

	      if(v[index_v] < 0){
		eps_b[index_] = eps_N_non_uni[K*Nx + I];
		eps_aS[index_] = 0;
	      }
	      else{
		eps_b[index_] = 0;
		eps_aS[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_n->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + 1*Nx + I;
	      eps_b[index_] = eps_turb[index_aux] * fn[index_aux] +
		eps_turb[index_aux + Nx] * (1 - fn[index_aux]);			  
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}	
      }
      
    } /* North section */
    
#pragma omp section
    {
      /* West boundary */
      I = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_w->type == 0) || (bc_w->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_aE[index_] = 1;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_w->type == 1){
	if(bc_w->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;
	      eps_b[index_] = eps_W_non_uni[K*Ny + J];   		    
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		eps_b[index_] = bc_w->eps_value;
	      }
	      else{
		eps_b[index_] = 0;
	      }
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_w->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      /* AKN model */
	      if(turb_model == 1){
		if(y_wall[index_+1] == 0){
		  eps_b[index_] = 0;
		}
		else{
		  eps_b[index_] = 2 * mu * k_turb[index_+1] /
		    (rho * y_wall[index_+1] * y_wall[index_+1]);
		}
	      }
	      /* CG model */
	      else if(turb_model == 2){
		eps_aE[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 4, Constant pressure */
      else if(bc_w->type == 4){

	i = 0;

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_u = K*nx*Ny + J*nx + i;

	      if(u[index_u] > 0){
		eps_b[index_] = eps_W_non_uni[K*Ny + J];
		eps_aE[index_] = 0;
	      }
	      else{
		eps_b[index_] = 0;
		eps_aE[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_w->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + J*Nx + Nx-3;
	      eps_b[index_] = eps_turb[index_aux] * fe[index_aux] +
		eps_turb[index_aux + 1] * (1 - fe[index_aux]);			  
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}	
      }
      
    } /* West section */

#pragma omp section
    {
      /* East boundary */
      I = Nx - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_e->type == 0) || (bc_e->type == 2)){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_aW[index_] = 1;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_e->type == 1){
	if(bc_e->parabolize_profile_OK){

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;
	      eps_b[index_] = eps_E_non_uni[K*Ny + J];   		    
	    }
	  }
	}
	else{

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		eps_b[index_] = bc_e->eps_value;
	      }
	      else{
		eps_b[index_] = 0;
	      }
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_e->type == 3){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      /* AKN model */
	      if(turb_model == 1){
		if(y_wall[index_-1] == 0){
		  eps_b[index_] = 0;
		}
		else{
		  eps_b[index_] = 2 * mu * k_turb[index_-1] /
		    (rho * y_wall[index_-1] * y_wall[index_-1]);
		}
	      }
	      /* CG model */
	      else if(turb_model == 2){
		eps_aW[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 4, Constant pressure */
      else if(bc_w->type == 4){

	i = nx - 1;

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_u = K*nx*Ny + J*nx + i;

	      if(u[index_u] < 0){
		eps_b[index_] = eps_E_non_uni[K*Ny + J];
		eps_aW[index_] = 0;
	      }
	      else{
		eps_b[index_] = 0;
		eps_aW[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_e->type == 5){

	for(K=1; K<Nz-1; K++){
	  for(J=1; J<Ny-1; J++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = K*Nx*Ny + J*Nx + 1;
	      eps_b[index_] = eps_turb[index_aux] * fe[index_aux] +
		eps_turb[index_aux + 1] * (1 - fe[index_aux]);			  
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}	
      }
      
    } /* East section */    

#pragma omp section
    {
      /* Bottom boundary */
      K = 0;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_b->type == 0) || (bc_b->type == 2)){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_aT[index_] = 1;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_b->type == 1){
	if(bc_b->parabolize_profile_OK){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;
	      eps_b[index_] = eps_B_non_uni[J*Nx + I];   		    
	    }
	  }
	}
	else{

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		eps_b[index_] = bc_b->eps_value;
	      }
	      else{
		eps_b[index_] = 0;
	      }
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_b->type == 3){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      /* AKN model */
	      if(turb_model == 1){
		if(y_wall[index_+Nx*Ny] == 0){
		  eps_b[index_] = 0;
		}
		else{
		  eps_b[index_] = 2 * mu * k_turb[index_+Nx*Ny] /
		    (rho * y_wall[index_+Nx*Ny] * y_wall[index_+Nx*Ny]);
		}
	      }
	      /* CG model */
	      else if(turb_model == 2){
		eps_aT[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 4, Constant pressure */
      else if(bc_b->type == 4){

	k = 0;

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_w = k*Nx*Ny + J*Nx + I;

	      if(w[index_w] > 0){
		eps_b[index_] = eps_B_non_uni[J*Nx + I];
		eps_aT[index_] = 0;
	      }
	      else{
		eps_b[index_] = 0;
		eps_aT[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_b->type == 5){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = (Nz-3)*Nx*Ny + J*Nx + I;
	      eps_b[index_] = eps_turb[index_aux] * ft[index_aux] +
		eps_turb[index_aux + Nx*Ny] * (1 - ft[index_aux]);			  
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}	
      }
      
    } /* Bottom section */

#pragma omp section
    {
      /* East boundary */
      K = Nz - 1;

      /* Type = 0, Zero gradient. Type = 2, Symmetry */
      if((bc_t->type == 0) || (bc_t->type == 2)){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      eps_aB[index_] = 1;
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 1, Fixed value */
      else if(bc_t->type == 1){
	if(bc_t->parabolize_profile_OK){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;
	      eps_b[index_] = eps_T_non_uni[J*Nx + I];   		    
	    }
	  }
	}
	else{

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){

	      index_ = K*Nx*Ny + J*Nx + I;
	      eps_aP[index_] = 1;

	      if(dom_matrix[index_] == 0){
		eps_b[index_] = bc_t->eps_value;
	      }
	      else{
		eps_b[index_] = 0;
	      }
	    }
	  }
	}
      }

      /* Type = 3, Wall */
      else if(bc_t->type == 3){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      /* AKN model */
	      if(turb_model == 1){
		if(y_wall[index_-Nx*Ny] == 0){
		  eps_b[index_] = 0;
		}
		else{
		  eps_b[index_] = 2 * mu * k_turb[index_-Nx*Ny] /
		    (rho * y_wall[index_-Nx*Ny] * y_wall[index_-Nx*Ny]);
		}
	      }
	      /* CG model */
	      else if(turb_model == 2){
		eps_aB[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 4, Constant pressure */
      else if(bc_t->type == 4){

	k = nz - 1;

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){

	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){

	      index_w = k*Nx*Ny + J*Nx + I;

	      if(w[index_w] < 0){
		eps_b[index_] = eps_T_non_uni[J*Nx + I];
		eps_aB[index_] = 0;
	      }
	      else{
		eps_b[index_] = 0;
		eps_aB[index_] = 1;
	      }
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}
      }

      /* Type = 5, Periodic */
      else if(bc_t->type == 5){

	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    eps_aP[index_] = 1;

	    if(dom_matrix[index_] == 0){
	      index_aux = 1*Nx*Ny + J*Nx + I;
	      eps_b[index_] = eps_turb[index_aux] * ft[index_aux] +
		eps_turb[index_aux + Nx*Ny] * (1 - ft[index_aux]);			  
	    }
	    else{
	      eps_b[index_] = 0;
	    }
	  }
	}	
      }
      
    } /* Top section */          
      
  } /* sections */
 
  return;
}

/* COMPUTE_EPS_BND_3D */
/*****************************************************************************/
/**

  Sets:

  Array (double, turb_boundary_cell_count) Data_Mem::eps_bnd_x: Values of Data_Mem::eps_turb at boundary 
  for W-P || E-P interfaces

  Array (double, turb_boundary_cell_count) Data_Mem::eps_bnd_y: Values of Data_Mem::eps_turb at boundary
  for S-P || N-P interfaces

  Array (double, turb_boundary_cell_count) Data_Mem::eps_bnd_z: Values of Data_Mem::eps_turb at boundary
  for B-P || T-P interfaces

  @param data
    
  @return void
    
  MODIFIED: 04-03-2019
*/
void compute_eps_bnd_3D(Data_Mem *data){

  int count, index_;
  int cell_done_OK;
  int W_is_solid_OK, E_is_solid_OK, S_is_solid_OK;
  int N_is_solid_OK, B_is_solid_OK, T_is_solid_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *dom_matrix = data->domain_matrix;
  
  double dphidxb_y, dphidyb_y, dphidzb_y;
  double dphidxb_x, dphidyb_x, dphidzb_x;
  double dphidxb_z, dphidyb_z, dphidzb_z;
  double dphidN;
  double e_x, e_y, e_z;
  double dphidxb_aux, dphidyb_aux, dphidzb_aux;
  double dphidx_P, dphidy_P, dphidz_P;     
  double dphidx_S, dphidx_N, dphidx_T, dphidx_B;
  double dphidz_W, dphidz_E, dphidz_S, dphidz_N;
  double dphidy_W, dphidy_E, dphidy_B, dphidy_T;

  double sqrt_k[27] = {0};
  /* [0]: (-1,-Nx,-NxNy) [1]: (-1,-Nx,0) [2]: (-1,-Nx,NxNy)
     [3]: (-1,0,-NxNy) [4]: (-1,0,0) [5]: (-1,0,NxNy)
     [6]: (-1,Nx,-NxNy) [7]: (-1,Nx,0) [8]: (-1,Nx,NxNy)
     [9]: (0,-Nx,-NxNy) [10]: (0,-Nx,0) [11]: (0,-Nx,NxNy)
     [12]: (0,0,-NxNy) [13]: (0,0,0) [14]: (0,0,NxNy)
     [15]: (0,Nx,-NxNy) [16]: (0,Nx,0) [17]: (0,Nx,NxNy)
     [18]: (1,-Nx,-NxNy) [19]: (1,-Nx,0) [20]: (1,-Nx,NxNy)
     [21]: (1,0,-NxNy) [22]: (1,0,0) [23]: (1,0,NxNy)
     [24]: (1,Nx,-NxNy) [25]: (1,Nx,0) [26]: (1,Nx,NxNy)*/

  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double eps_dist_tol = data->eps_dist_tol;

  double *eps_bnd_x = data->eps_bnd_x;
  double *eps_bnd_y = data->eps_bnd_y;
  double *eps_bnd_z = data->eps_bnd_z;

  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;
  const double *k_turb = data->k_turb;
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_x_z = data->norm_vec_x_z;
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *norm_vec_y_z = data->norm_vec_y_z;
  const double *norm_vec_z_x = data->norm_vec_z_x;
  const double *norm_vec_z_y = data->norm_vec_z_y;
  const double *norm_vec_z_z = data->norm_vec_z_z;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zB = data->Delta_zB;
  const double *Delta_zT = data->Delta_zT;

  /* phi: sqrt(k) */

  for(count=0; count<turb_boundary_cell_count; count++){
      
    index_ = turb_boundary_indexs[count];
      
    cell_done_OK = 0;

    sqrt_k[0] = powf(k_turb[index_-1-Nx-Nx*Ny], 0.5);
    sqrt_k[1] = powf(k_turb[index_-1-Nx], 0.5);
    sqrt_k[2] = powf(k_turb[index_-1-Nx+Nx*Ny], 0.5);
    sqrt_k[3] = powf(k_turb[index_-1-Nx*Ny], 0.5);
    sqrt_k[4] = powf(k_turb[index_-1], 0.5);
    sqrt_k[5] = powf(k_turb[index_-1+Nx*Ny], 0.5);
    sqrt_k[6] = powf(k_turb[index_-1+Nx-Nx*Ny], 0.5);
    sqrt_k[7] = powf(k_turb[index_-1+Nx], 0.5);
    sqrt_k[8] = powf(k_turb[index_-1+Nx+Nx*Ny], 0.5);
    sqrt_k[9] = powf(k_turb[index_-Nx-Nx*Ny], 0.5);
    sqrt_k[10] = powf(k_turb[index_-Nx], 0.5);
    sqrt_k[11] = powf(k_turb[index_-Nx+Nx*Ny], 0.5);
    sqrt_k[12] = powf(k_turb[index_-Nx*Ny], 0.5);
    sqrt_k[13] = powf(k_turb[index_], 0.5);
    sqrt_k[14] = powf(k_turb[index_+Nx*Ny], 0.5);
    sqrt_k[15] = powf(k_turb[index_+Nx-Nx*Ny], 0.5);
    sqrt_k[16] = powf(k_turb[index_+Nx], 0.5);
    sqrt_k[17] = powf(k_turb[index_+Nx+Nx*Ny], 0.5);
    sqrt_k[18] = powf(k_turb[index_+1-Nx-Nx*Ny], 0.5);
    sqrt_k[19] = powf(k_turb[index_+1-Nx], 0.5);
    sqrt_k[20] = powf(k_turb[index_+1-Nx+Nx*Ny], 0.5);
    sqrt_k[21] = powf(k_turb[index_+1-Nx*Ny], 0.5);
    sqrt_k[22] = powf(k_turb[index_+1], 0.5);
    sqrt_k[23] = powf(k_turb[index_+1+Nx*Ny], 0.5);
    sqrt_k[24] = powf(k_turb[index_+1+Nx-Nx*Ny], 0.5);
    sqrt_k[25] = powf(k_turb[index_+1+Nx], 0.5);
    sqrt_k[26] = powf(k_turb[index_+1+Nx+Nx*Ny], 0.5);

    dphidxb_x = 0;
    dphidyb_x = 0;
    dphidzb_x = 0;
    dphidxb_y = 0;
    dphidyb_y = 0;
    dphidzb_y = 0;
    dphidxb_z = 0;
    dphidyb_z = 0;
    dphidzb_z = 0;
    dphidx_P = 0;
    dphidy_P = 0;
    dphidz_P = 0;
    dphidy_W = 0;
    dphidy_E = 0;
    dphidz_W = 0;
    dphidz_E = 0;
    dphidx_S = 0;
    dphidx_N = 0;
    dphidz_S = 0;
    dphidz_N = 0;
    dphidx_B = 0;
    dphidx_T = 0;
    dphidy_B = 0;
    dphidy_T = 0;

    W_is_solid_OK = dom_matrix[index_-1];
    E_is_solid_OK = dom_matrix[index_+1];
    S_is_solid_OK = dom_matrix[index_-Nx];
    N_is_solid_OK = dom_matrix[index_+Nx];
    B_is_solid_OK = dom_matrix[index_-Nx*Ny];
    T_is_solid_OK = dom_matrix[index_+Nx*Ny];

    /*******************************************************************/
    /* E-P line intersects with curve */
    if(E_is_solid_OK && !cell_done_OK){

      cell_done_OK = 1;
      e_x = eps_x[index_];
      
      /* dphidxb_x is used for calculation of dphidN on E cases (13:P, 4:W) */
      /* Using phi_b = sqrt_k_b = 0, obtains derivative at boundary */
      dphidxb_x = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol,
					 0, sqrt_k[13], sqrt_k[4],
					 Delta_xE[index_], Delta_xW[index_], 1);
       
      /* Ec 25 Sato: dphidx_P value needed for S|N and B|T cases (13:P, 4:W) */
      dphidx_P = __central_b_der(e_x, dphidxb_x, sqrt_k[13], sqrt_k[4], Delta_xE[index_], Delta_xW[index_]);

      /* Case E-N */      
      if(N_is_solid_OK){
	/* There is solid at N, so we compute derivatives needed in other directions, namely, 
	   dphidx_S, dphidz_S checking if there is solid at SE and ST cells */

	/* dphidx_S value needed for N case: Inside East branch we only check
	 for solid on East direction (+1) */
	e_x = eps_x[index_-Nx];

	/* Checking SE cell */
	if(dom_matrix[index_-Nx+1]){
	  /* SE cell is solid */
	  
	  /* Computing derivative at boundary in SE cell using (10:S, 1:SW) phi values */
	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[10], sqrt_k[1],
					 Delta_xE[index_-Nx], Delta_xW[index_-Nx], 1);
	  /* Derivative at center (S) */
	  dphidx_S = __central_b_der(e_x, dphidxb_aux, sqrt_k[10], sqrt_k[1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}
	else{
	  /* SE cell is fluid, so use node values (19:SE, 1:SW) */
	  dphidx_S = __central_der(sqrt_k[19], sqrt_k[10], sqrt_k[1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}

	/* dphidz_S value needed for N case */
	e_z = eps_z[index_-Nx];

	/* Checking ST cell */
	if(dom_matrix[index_-Nx+Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[10], sqrt_k[9],
					 Delta_zT[index_-Nx], Delta_zB[index_-Nx], 1);

	  dphidz_S = __central_b_der(e_z, dphidzb_aux, sqrt_k[10], sqrt_k[9], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	}
	else if(dom_matrix[index_-Nx-Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[10], sqrt_k[11],
					 Delta_zB[index_-Nx], Delta_zT[index_-Nx], -1);
	  dphidz_S = __central_b_der(e_z, dphidzb_aux, sqrt_k[11], sqrt_k[10], Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	}
	else{
	  dphidz_S = __central_der(sqrt_k[11], sqrt_k[10], sqrt_k[9], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	}

      }  // end if(N_is_solid_OK)
 	        
      /* Case E-S */
      if(S_is_solid_OK){

	/* dphidx_N value needed for S case: Inside East branch we only check 
	   for solid on East direction (+1) */
	e_x = eps_x[index_+Nx];
	
	if(dom_matrix[index_+Nx+1]){

	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[16], sqrt_k[7],
					 Delta_xE[index_+Nx], Delta_xW[index_+Nx], 1);
	  
	  dphidx_N = __central_b_der(e_x, dphidxb_aux, sqrt_k[16], sqrt_k[7], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}
	else{
	  dphidx_N = __central_der(sqrt_k[25], sqrt_k[16], sqrt_k[7], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}

	/* dphidz_N value needed for S case */
	e_z = eps_z[index_+Nx];

	if(dom_matrix[index_+Nx+Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[16], sqrt_k[15],
					 Delta_zT[index_+Nx], Delta_zB[index_+Nx], 1);
	  dphidz_N = __central_b_der(e_z, dphidzb_aux, sqrt_k[16], sqrt_k[15], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	}
	else if(dom_matrix[index_+Nx-Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[16], sqrt_k[17],
					 Delta_zB[index_+Nx], Delta_zT[index_+Nx], -1);
	  dphidz_N = __central_b_der(e_z, dphidzb_aux, sqrt_k[17], sqrt_k[16], Delta_zB[index_+Nx], Delta_zT[index_+Nx]);
	}
	else{
	  dphidz_N = __central_der(sqrt_k[17], sqrt_k[16], sqrt_k[15], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	}
      }

      /* Case E-T */
      if(T_is_solid_OK){

	/* dphidx_B value needed for T case: Inside East branch we only check
	   for solid on East direction (+1) */
	e_x = eps_x[index_-Nx*Ny];

	if(dom_matrix[index_-Nx*Ny+1]){
	  
	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[3],
					 Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], 1);

	  dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}
	else{
	  dphidx_B = __central_der(sqrt_k[21], sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}

	/* dphidy_B value needed for T case */
	e_y = eps_y[index_-Nx*Ny];

	if(dom_matrix[index_+Nx-Nx*Ny]){

	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[9],
					 Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], 1);
	  dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}
	else if(dom_matrix[index_-Nx-Nx*Ny]){
	  
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[15],
					 Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny], -1);
	  dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[15], sqrt_k[12], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	}
	else{
	  dphidy_B = __central_der(sqrt_k[15], sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}
	
      }


      /* Case E-B */
      if(B_is_solid_OK){

	/* dphidx_T value needed for B case: Inside East branch we only check
	   for solid on East direction (+1) */
	e_x = eps_x[index_+Nx*Ny];

	if(dom_matrix[index_+Nx*Ny+1]){

	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[5],
					 Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], 1);

	  dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}
	else{
	  dphidx_T = __central_der(sqrt_k[23], sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}

	/* dphidy_T value needed for B case */
	e_y = eps_y[index_+Nx*Ny];

	if(dom_matrix[index_+Nx+Nx*Ny]){

	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[11],
					 Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], 1);
	  dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}
	else if(dom_matrix[index_-Nx+Nx*Ny]){
	  
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[17],
					 Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny], -1);
	  dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[17], sqrt_k[14], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	}
	else{
	  dphidy_T = __central_der(sqrt_k[17], sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}
	
      }

      /* dphidy_P, dphidy_W, dphidz_P, dphidz_W required for E case */

      /* First try the simplest approximation */
      dphidy_P = __central_der(sqrt_k[16], sqrt_k[13], sqrt_k[10], Delta_yN[index_], Delta_yS[index_]);
      dphidy_W = __central_der(sqrt_k[7], sqrt_k[4], sqrt_k[1], Delta_yN[index_-1], Delta_yS[index_-1]);
      dphidz_P = __central_der(sqrt_k[14], sqrt_k[13], sqrt_k[12], Delta_zT[index_], Delta_zB[index_]);
      dphidz_W = __central_der(sqrt_k[5], sqrt_k[4], sqrt_k[3], Delta_zT[index_-1], Delta_zB[index_-1]);

      /* Check if dphidy_P or dphidy_W need to be corrected */

      /* If N is solid, correct dphidy_P */
      if(N_is_solid_OK){

	e_y = eps_y[index_];

	/* dphidyb_y is used for calculation of dphidN on N cases (13:P, 10:S) */
	dphidyb_y = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[13], sqrt_k[10],
					 Delta_yN[index_], Delta_yS[index_], 1);

	dphidy_P = __central_b_der(e_y, dphidyb_y, sqrt_k[13], sqrt_k[10], Delta_yN[index_], Delta_yS[index_]);
      }

      /* If NW is solid, correct dphidy_W */
      if(dom_matrix[index_+Nx-1]){

	e_y = eps_y[index_-1];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[4], sqrt_k[1],
					 Delta_yN[index_-1], Delta_yS[index_-1], 1);

	dphidy_W = __central_b_der(e_y, dphidyb_aux, sqrt_k[4], sqrt_k[1], Delta_yN[index_-1], Delta_yS[index_-1]);
      }
	
      /* If S is solid, correct dphidy_P */
      if(S_is_solid_OK){

	e_y = eps_y[index_];
	
	/* dphidyb_y is used for calculation of dphidN on S cases */
	dphidyb_y = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[13], sqrt_k[16],
					 Delta_yS[index_], Delta_yN[index_], -1);

	dphidy_P = __central_b_der(e_y, dphidyb_y, sqrt_k[16], sqrt_k[13], Delta_yS[index_], Delta_yN[index_]);
      }

      /* If SW is solid, correct dphidy_W */
      if(dom_matrix[index_-Nx-1]){

	e_y = eps_y[index_-1];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[4], sqrt_k[7],
					 Delta_yS[index_-1], Delta_yN[index_-1], -1);

	dphidy_W = __central_b_der(e_y, dphidyb_aux, sqrt_k[7], sqrt_k[4], Delta_yS[index_-1], Delta_yN[index_-1]);
      }

      /* Check if dphidz_P or dphidz_W need to be corrected */

      /* If T is solid, correct dphidz_P */
      if(T_is_solid_OK){

	e_z = eps_z[index_];

	/* dphidzb_z is used for calculation of dphidN on T cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[12],
					 Delta_zT[index_], Delta_zB[index_], 1);

	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[13], sqrt_k[12], Delta_zT[index_], Delta_zB[index_]);
      }

      /* If TW is solid, correct dphidz_W */
      if(dom_matrix[index_+Nx*Ny-1]){

	e_z = eps_z[index_-1];

	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[4], sqrt_k[3],
					 Delta_zT[index_-1], Delta_zB[index_-1], 1);

	dphidz_W = __central_b_der(e_z, dphidzb_aux, sqrt_k[4], sqrt_k[3], Delta_zT[index_-1], Delta_zB[index_-1]);
      }
	
      /* If B is solid, correct dphidz_P */
      if(B_is_solid_OK){

	e_z = eps_z[index_];

	/* dphidzb_z is used for calculation of dphidN on B cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[14],
					 Delta_zB[index_], Delta_zT[index_], -1);

	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[14], sqrt_k[13], Delta_zB[index_], Delta_zT[index_]);
      }

      /* If BW is solid, correct dphidz_W */
      if(dom_matrix[index_-Nx*Ny-1]){

	e_z = eps_z[index_-1];

	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[4], sqrt_k[5],
					 Delta_zB[index_-1], Delta_zT[index_-1], -1);

	dphidz_W = __central_b_der(e_z, dphidzb_aux, sqrt_k[5], sqrt_k[4], Delta_zB[index_-1], Delta_zT[index_-1]);
      }
      
      /* Now we have dphidxb_x, dphidy_P, dphidy_W, dphidz_P, dphidz_W for E cases
	 dphidyb_y, dphidx_P, dphidx_N|S, dphidz_P, dphidz_N|S for S-N cases
	 dphidzb_z, dphidx_P, dphidx_T|B, dphidy_P, dphidy_T|B for B-T cases */
      
    } /* end case E-P */

    /*******************************************************************/
    /* W-P line intersects with curve */
    if(W_is_solid_OK && !cell_done_OK){

      cell_done_OK = 1;      
      e_x = eps_x[index_];

      /* dphidxb_x is used for calculation of dphidN on W cases */
      dphidxb_x = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[13], sqrt_k[22],
					 Delta_xW[index_], Delta_xE[index_], -1);
      
      dphidx_P = __central_b_der(e_x, dphidxb_x, sqrt_k[22], sqrt_k[13], Delta_xW[index_], Delta_xE[index_]);

      /* Case W-N  */
      if(N_is_solid_OK){

	/* dphidx_S value needed for N case: Inside West branch we only check 
	   for solid on West direction (-1) */
	e_x = eps_x[index_-Nx];
	
	if(dom_matrix[index_-Nx-1]){
	  
	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[10], sqrt_k[19],
					 Delta_xW[index_-Nx], Delta_xE[index_-Nx], -1);
	  
	  dphidx_S = __central_b_der(e_x, dphidxb_aux, sqrt_k[19], sqrt_k[10], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	}
	else{
	  dphidx_S = __central_der(sqrt_k[19], sqrt_k[10], sqrt_k[1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}

	/* dphidz_S value needed for N case */
	e_z = eps_z[index_-Nx];
	
	if(dom_matrix[index_-Nx+Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[10], sqrt_k[9],
					 Delta_zT[index_-Nx], Delta_zB[index_-Nx], 1);

	  dphidz_S = __central_b_der(e_z, dphidzb_aux, sqrt_k[10], sqrt_k[9], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	}
	else if(dom_matrix[index_-Nx-Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[10], sqrt_k[11],
					 Delta_zB[index_-Nx], Delta_zT[index_-Nx], -1);
	  dphidz_S = __central_b_der(e_z, dphidzb_aux, sqrt_k[11], sqrt_k[10], Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	}
	else{
	  dphidz_S = __central_der(sqrt_k[11], sqrt_k[10], sqrt_k[9], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	}
	
      }

      
      /* Case W-S */
      if(S_is_solid_OK){

	/* dphidx_N value needed for S case: Inside West branch we only check
	   for solid on West direction (-1) */
	e_x = eps_x[index_+Nx];
	
	if(dom_matrix[index_+Nx-1]){

	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[16], sqrt_k[25],
					 Delta_xW[index_+Nx], Delta_xE[index_+Nx], -1);
	  
	  dphidx_N = __central_b_der(e_x, dphidxb_aux, sqrt_k[25], sqrt_k[16], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	}
	else{
	  dphidx_N = __central_der(sqrt_k[25], sqrt_k[16], sqrt_k[7], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}

	/* dphidz_N value needed for S case */
	e_z = eps_z[index_+Nx];

	if(dom_matrix[index_+Nx+Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[16], sqrt_k[15],
					 Delta_zT[index_+Nx], Delta_zB[index_+Nx], 1);
	  dphidz_N = __central_b_der(e_z, dphidzb_aux, sqrt_k[16], sqrt_k[15], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	}
	else if(dom_matrix[index_+Nx-Nx*Ny]){

	  dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[16], sqrt_k[17],
					 Delta_zB[index_+Nx], Delta_zT[index_+Nx], -1);
	  dphidz_N = __central_b_der(e_z, dphidzb_aux, sqrt_k[17], sqrt_k[16], Delta_zB[index_+Nx], Delta_zT[index_+Nx]);
	}
	else{
	  dphidz_N = __central_der(sqrt_k[17], sqrt_k[16], sqrt_k[15], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	}
	
      }

      /* Case W-T */
      if(T_is_solid_OK){
	
	/* dphidx_B value needed for T case: Inside West branch we only check
	   for solid on West direction (-1) */
	e_x = eps_x[index_-Nx*Ny];

	if(dom_matrix[index_-Nx*Ny-1]){

	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[21],
					 Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny], -1);

	  dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[21], sqrt_k[12], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	}
	else{
	  dphidx_B = __central_der(sqrt_k[21], sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}

	/* dphidy_B value needed for T case */
	e_y = eps_y[index_-Nx*Ny];

	if(dom_matrix[index_+Nx-Nx*Ny]){

	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[9],
					 Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], 1);
	  dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}
	else if(dom_matrix[index_-Nx-Nx*Ny]){
	  
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[15],
					 Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny], -1);
	  dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[15], sqrt_k[12], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	}
	else{
	  dphidy_B = __central_der(sqrt_k[15], sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}
	
      }

      /* Case W-B */
      if(B_is_solid_OK){

	/* dphidx_T value needed for B case: Inside West branch we only check
	   for solid on West direction (-1) */
	e_x = eps_x[index_+Nx*Ny];

	if(dom_matrix[index_+Nx*Ny-1]){

	  /* No need to store this value */
	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[23],
					 Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny], -1);

	  dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[23], sqrt_k[14], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	}
	else{
	  dphidx_T = __central_der(sqrt_k[23], sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}

	/* dphidy_T value needed for B case */
	e_y = eps_y[index_+Nx*Ny];

	if(dom_matrix[index_+Nx+Nx*Ny]){

	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[11],
					 Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], 1);
	  dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}
	else if(dom_matrix[index_-Nx+Nx*Ny]){
	  
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[17],
					 Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny], -1);
	  dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[17], sqrt_k[14], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	}
	else{
	  dphidy_T = __central_der(sqrt_k[17], sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}
	
      }      

      /* dphidy_P, dphidy_E, dphidz_p, dphidz_E required for W case */

      /* First try the simplest approximation */
      dphidy_P = __central_der(sqrt_k[16], sqrt_k[13], sqrt_k[10], Delta_yN[index_], Delta_yS[index_]);
      dphidy_E = __central_der(sqrt_k[25], sqrt_k[22], sqrt_k[19], Delta_yN[index_+1], Delta_yS[index_+1]);
      dphidz_P = __central_der(sqrt_k[14], sqrt_k[13], sqrt_k[12], Delta_zT[index_], Delta_zB[index_]);
      dphidz_E = __central_der(sqrt_k[23], sqrt_k[22], sqrt_k[21], Delta_zT[index_+1], Delta_zB[index_+1]);

      /* Check if dphidy_P or dphidy_E need to be corrected */
      
      /* If N is solid, correct dphidy_P */
      if(N_is_solid_OK){

	e_y = eps_y[index_];
	
	/* dphidyb_y is used for calculation of dphidN on N cases */

	dphidyb_y = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[13], sqrt_k[10],
					 Delta_yN[index_], Delta_yS[index_], 1);

	dphidy_P = __central_b_der(e_y, dphidyb_y, sqrt_k[13], sqrt_k[10], Delta_yN[index_], Delta_yS[index_]);
      }

      /* If NE is solid, correct dphidy_E */
      if(dom_matrix[index_+Nx+1]){

	e_y = eps_y[index_+1];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[22], sqrt_k[19],
					 Delta_yN[index_+1], Delta_yS[index_+1], 1);

	dphidy_E = __central_b_der(e_y, dphidyb_aux, sqrt_k[22], sqrt_k[19], Delta_yN[index_+1], Delta_yS[index_+1]);
      }
	
    /* If S is solid, correct dphidy_P */
      if(S_is_solid_OK){

	e_y = eps_y[index_];

	/* dphidyb_y is used for calculation of dphidN on S cases */
	dphidyb_y = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[13], sqrt_k[16],
					 Delta_yS[index_], Delta_yN[index_], -1);

	dphidy_P = __central_b_der(e_y, dphidyb_y, sqrt_k[16], sqrt_k[13], Delta_yS[index_], Delta_yN[index_]);
      }


      /* If SE is solid, correct dphidy_E */
      if(dom_matrix[index_-Nx+1]){

	e_y = eps_y[index_+1];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[22], sqrt_k[25],
					 Delta_yS[index_+1], Delta_yN[index_+1], -1);
	
	dphidy_E = __central_b_der(e_y, dphidyb_aux, sqrt_k[25], sqrt_k[22], Delta_yS[index_+1], Delta_yN[index_+1]);
      }

      /* Check if dphidz_P or dphidz_E need to be corrected */

      /* If T is solid, correct dphidz_P */
      if(T_is_solid_OK){

	e_z = eps_z[index_];

	/* dphidzb_z is used for calculation of dphidN on T cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[12],
					 Delta_zT[index_], Delta_zB[index_], 1);

	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[13], sqrt_k[12], Delta_zT[index_], Delta_zB[index_]);
      }

      /* If TE is solid, correct dphidz_E */
      if(dom_matrix[index_+Nx*Ny+1]){

	e_z = eps_z[index_+1];

	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[22], sqrt_k[21],
					 Delta_zT[index_+1], Delta_zB[index_+1], 1);

	dphidz_E = __central_b_der(e_z, dphidzb_aux, sqrt_k[22], sqrt_k[21], Delta_zT[index_+1], Delta_zB[index_+1]);
      }

      /* If B is solid, correct dphidz_P */
      if(B_is_solid_OK){

	e_z = eps_z[index_];

	/* dphidb_z is used for calculation of dphidN on B cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[14],
					 Delta_zB[index_], Delta_zT[index_], -1);

	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[14], sqrt_k[13], Delta_zB[index_], Delta_zT[index_]);
      }

      /* If BE is solid, correct dphidz_E */
      if(dom_matrix[index_-Nx*Ny+1]){

	e_z = eps_z[index_+1];

	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[22], sqrt_k[23],
					 Delta_zB[index_+1], Delta_zT[index_+1], -1);

	dphidz_E = __central_b_der(e_z, dphidzb_aux, sqrt_k[23], sqrt_k[22], Delta_zB[index_+1], Delta_zT[index_+1]);
      }           

      /* Now we have dphidxb_x, dphidy_P, dphidy_E, dphidz_P, dphidz_E for W cases
	 dphidyb_y, dphidx_P, dphidx_N|S, dphidz_P, dphidz_N|S for S-N cases
	 dphidzb_z, dphidx_P, dphidx_T|B, dphidy_P, dphidy_T|B for B-T cases */
      
     } /* end case W-P */ 

    /*******************************************************************/
    /* N-P line intersects with curve */
    if(N_is_solid_OK && !cell_done_OK){

      cell_done_OK = 1;
      e_y = eps_y[index_];

      /* dphidyb_y is used for calculation of dphidN on N cases */

      dphidyb_y = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[13], sqrt_k[10],
					 Delta_yN[index_], Delta_yS[index_], 1);

      dphidy_P = __central_b_der(e_y, dphidyb_y, sqrt_k[13], sqrt_k[10], Delta_yN[index_], Delta_yS[index_]);

      /* We don't need to check for E|W and calculate dphidy_W|E, that case was coded on W|E branch */
      
      /* Case N-T */
      if(T_is_solid_OK){

	/* dphidy_B value needed for T case: Inside North branch we only check for solid on North direction */
	e_y = eps_y[index_-Nx*Ny];

	if(dom_matrix[index_-Nx*Ny+Nx]){

	  /* No need to store this value */
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[9],
					 Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], 1);

	  dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}
	else{
	  dphidy_B = __central_der(sqrt_k[15], sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}

	/* dphidx_B value needed for T case */
	e_x = eps_x[index_-Nx*Ny];

	if(dom_matrix[index_+1-Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[3],
					 Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], 1);
	  dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}
	else if(dom_matrix[index_-1-Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[21],
					 Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny], -1);
	  dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[21], sqrt_k[12], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	}
	else{
	  dphidx_B = __central_der(sqrt_k[21], sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}
	
      }

      /* Case N-B */
      if(B_is_solid_OK){

	/* dphidy_T value needed for B case: Inside North branch we only check for solid on North direction */
	e_y = eps_y[index_+Nx*Ny];

	if(dom_matrix[index_+Nx*Ny+Nx]){

	  /* No need to store this value */
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[11],
					 Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], 1);

	  dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}
	else{
	  dphidy_T = __central_der(sqrt_k[17], sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}

	/* dphidx_T value needed for B case */
	e_x = eps_x[index_+Nx*Ny];

	if(dom_matrix[index_+1+Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[5],
					 Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], 1);
	  dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}
	else if(dom_matrix[index_-1+Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[23],
					 Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny], -1);
	  dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[23], sqrt_k[14], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	}
	else{
	  dphidx_T = __central_der(sqrt_k[23], sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}
	
      }

      /* dphidx_P, dphidx_S, dphidz_P, dphidz_S required for N case */

      /* First try the simplest approximation */
      dphidx_P = __central_der(sqrt_k[22], sqrt_k[13], sqrt_k[4], Delta_xE[index_], Delta_xW[index_]);
      dphidx_S = __central_der(sqrt_k[19], sqrt_k[10], sqrt_k[1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      dphidz_P = __central_der(sqrt_k[14], sqrt_k[13], sqrt_k[12], Delta_zT[index_], Delta_zB[index_]);
      dphidz_S = __central_der(sqrt_k[11], sqrt_k[10], sqrt_k[9], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);

      /* If SE is solid, correct dphidx_S */      
      if(dom_matrix[index_-Nx+1]){

	e_x = eps_x[index_-Nx];	

      /* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[10], sqrt_k[1],
					 Delta_xE[index_-Nx], Delta_xW[index_-Nx], 1);
      
	dphidx_S = __central_b_der(e_x, dphidxb_aux, sqrt_k[10], sqrt_k[1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      }

      /* If SW is solid, correct dphidx_S */
      if(dom_matrix[index_-Nx-1]){

	e_x = eps_x[index_-Nx];
	
	/* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[10], sqrt_k[19],
					 Delta_xW[index_-Nx], Delta_xE[index_-Nx], -1);

	dphidx_S = __central_b_der(e_x, dphidxb_aux, sqrt_k[19], sqrt_k[10], Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
      }

      /* Check if dphidz_P or dphidz_S need to be corrected */
      /* If T is solid, correct dphidz_P */
      if(T_is_solid_OK){

	e_z = eps_z[index_];

	/* dphidzb_z is used for calculation of dphidN on T cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[12],
					 Delta_zT[index_], Delta_zB[index_], 1);

	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[13], sqrt_k[12], Delta_zT[index_], Delta_zB[index_]);
      }

      /* If TS is solid, correct dphidz_S */
      if(dom_matrix[index_+Nx*Ny-Nx]){

	e_z = eps_z[index_-Nx];

	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[10], sqrt_k[9],
					 Delta_zT[index_-Nx], Delta_zB[index_-Nx], 1);

	dphidz_S = __central_b_der(e_z, dphidzb_aux, sqrt_k[10], sqrt_k[9], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
      }

      /* If B is solid, correct dphidz_P */
      if(B_is_solid_OK){

	e_z = eps_z[index_];

	/* dphidzb_z is used for calculation of dphidN on B cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[14],
					 Delta_zB[index_], Delta_zT[index_], -1);

	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[14], sqrt_k[13], Delta_zB[index_], Delta_zT[index_]);
      }

      /* If BS is solid, correct dphidz_S */
      if(dom_matrix[index_-Nx*Ny-Nx]){

	e_z = eps_z[index_-Nx];
	
	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[10], sqrt_k[11],
					 Delta_zB[index_-Nx], Delta_zT[index_-Nx], -1);

	dphidz_S = __central_b_der(e_z, dphidzb_aux, sqrt_k[11], sqrt_k[10], Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
      }

      /* Now we have dphidyb_y, dphidx_P, dphidx_S, dphidz_P, dphidz_S for N cases
	 dphidzb_z, dphidx_P, dphidx_T|B, dphidy_P, dphidy_T|B for B-T cases */
      
    } /* end case N-P */

    /*******************************************************************/
    /* S-P line intersects with curve */
    if(S_is_solid_OK && !cell_done_OK){
      
      cell_done_OK = 1;
      e_y = eps_y[index_];

      /* dphidyb_y is used for calculation of dphidN on S cases */
      
      dphidyb_y = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[13], sqrt_k[16],
					 Delta_yS[index_], Delta_yN[index_], -1);
      
      dphidy_P = __central_b_der(e_y, dphidyb_y, sqrt_k[16], sqrt_k[13], Delta_yS[index_], Delta_yN[index_]);

      /* We don't need to check for E|W and calculate dphidy_W|E, that case was coded on W|E branch */

      /* Case S-T */
      if(T_is_solid_OK){

	/* dphidy_B value needed for T case: Inside South branch we only check for solid on South direction */
	e_y = eps_y[index_-Nx*Ny];

	if(dom_matrix[index_-Nx*Ny-Nx]){

	  /* No need to store this value */
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[15],
					 Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny], -1);

	  dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[15], sqrt_k[12], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	}
	else{
	  dphidy_B = __central_der(sqrt_k[15], sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}

	/* dphidx_B value needed for T case */
	e_x = eps_x[index_-Nx*Ny];

	if(dom_matrix[index_+1-Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[3],
					 Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], 1);
	  dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}
	else if(dom_matrix[index_-1-Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[21],
					 Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny], -1);
	  dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[21], sqrt_k[12], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	}
	else{
	  dphidx_B = __central_der(sqrt_k[21], sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}
	
      }

      /* Case S-B */
      if(B_is_solid_OK){

	/* dphidy_T value needed for B case: Inside South branch we only check for solid on South direction */
	e_y = eps_y[index_+Nx*Ny];

	if(dom_matrix[index_+Nx*Ny-Nx]){

	  /* No need to store this value */
	  dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[17],
					 Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny], -1);

	  dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[17], sqrt_k[14], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	}
	else{
	  dphidy_T = __central_der(sqrt_k[17], sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}

	/* dphidx_T value needed for B case */
	e_x = eps_x[index_+Nx*Ny];

	if(dom_matrix[index_+1+Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[5],
					 Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], 1);
	  dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}
	else if(dom_matrix[index_-1+Nx*Ny]){

	  dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[23],
					 Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny], -1);
	  dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[23], sqrt_k[14], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	}
	else{
	  dphidx_T = __central_der(sqrt_k[23], sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}
	
      }
      
      /* dphidx_P, dphidx_N, dphidz_P, dphidz_N required for S case */

      /* First try the simplest approximation */
      dphidx_P = __central_der(sqrt_k[22], sqrt_k[13], sqrt_k[4], Delta_xE[index_], Delta_xW[index_]);
      dphidx_N = __central_der(sqrt_k[25], sqrt_k[16], sqrt_k[7], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      dphidz_P = __central_der(sqrt_k[14], sqrt_k[13], sqrt_k[12], Delta_yN[index_], Delta_yS[index_]);
      dphidz_N = __central_der(sqrt_k[17], sqrt_k[16], sqrt_k[15], Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

      /* If NE is solid, correct dphidx_N */
      if(dom_matrix[index_+Nx+1]){

	e_x = eps_x[index_+Nx];

	/* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[16], sqrt_k[7],
					 Delta_xE[index_+Nx], Delta_xW[index_+Nx], 1);

	dphidx_N = __central_b_der(e_x, dphidxb_aux, sqrt_k[16], sqrt_k[7], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      }

      /* If NW is solid, correct dphidx_N */
      if(dom_matrix[index_+Nx-1]){

	e_x = eps_x[index_+Nx];

	/* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[16], sqrt_k[25],
					 Delta_xW[index_+Nx], Delta_xE[index_+Nx], -1);
	
	dphidx_N = __central_b_der(e_x, dphidxb_aux, sqrt_k[25], sqrt_k[16], Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
      }

      /* Check if dphidz_P or dphidz_N need to be corrected */
      /* If T is solid, correct dphidz_P */
      if(T_is_solid_OK){

	e_z =  eps_z[index_];

	/* dphidzb_z is used for calculation of dphidN on T cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[12],
					 Delta_zT[index_], Delta_zB[index_], 1);
				      
	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[13], sqrt_k[12], Delta_zT[index_], Delta_zB[index_]);
      }

      /* If TN is solid, correct dphidz_N */
      if(dom_matrix[index_+Nx*Ny+Nx]){

	e_z = eps_z[index_+Nx];

	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[16], sqrt_k[15],
					 Delta_zT[index_+Nx], Delta_zB[index_+Nx], 1);

	dphidz_N = __central_b_der(e_z, dphidzb_aux, sqrt_k[16], sqrt_k[15], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
      }

      /* If B is solid, correct dphidz_P */
      if(B_is_solid_OK){

	e_z = eps_z[index_];

	/* dphidzb_z is used for calculation of dphidN on B cases */
	dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[14],
					 Delta_zB[index_], Delta_zT[index_], -1);

	dphidz_P = __central_b_der(e_z, dphidzb_z, sqrt_k[14], sqrt_k[13], Delta_zB[index_], Delta_zT[index_]);
      }

      /* If BN is solid, correct dphidz_N */
      if(dom_matrix[index_-Nx*Ny+Nx]){

	e_z = eps_z[index_+Nx];

	/* No need to store this value */
	dphidzb_aux = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[16], sqrt_k[17],
					 Delta_zB[index_+Nx], Delta_zT[index_+Nx], -1);

	dphidz_N = __central_b_der(e_z, dphidzb_aux, sqrt_k[17], sqrt_k[16], Delta_zB[index_+Nx], Delta_zT[index_+Nx]);
      }
      
      /* Now we have dphidyb_y, dphidx_P, dphidx_N, dphidz_P, dphidz_N for S cases
	 dphidzb_z, dphidx_P, dphidx_T|B, dphidy_P, dphidy_T|B for B-T cases */      
      
    } /* end case S-P */

    /*******************************************************************/    
    /* T-P line intersects with curve */
    if(T_is_solid_OK && !cell_done_OK){

      cell_done_OK = 1;
      e_z = eps_z[index_];

      /* dphidzb_z is used for calculation of dphidN on T cases */
      dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[12],
					 Delta_zT[index_], Delta_zB[index_], 1);

      /* dphidx_P, dphidx_B, dphidy_P, dphidy_B required for T case */
      dphidx_P = __central_der(sqrt_k[22], sqrt_k[13], sqrt_k[4], Delta_xE[index_], Delta_xW[index_]);
      dphidx_B = __central_der(sqrt_k[21], sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
      dphidy_P = __central_der(sqrt_k[16], sqrt_k[13], sqrt_k[10], Delta_yN[index_], Delta_yS[index_]);
      dphidy_B = __central_der(sqrt_k[15], sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);

      /* If BE is solid, correct dphidx_B */
      if(dom_matrix[index_-Nx*Ny+1]){

	e_x = eps_x[index_-Nx*Ny];

	/* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[3],
					 Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny], 1);

	dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[12], sqrt_k[3], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
      }

      /* If BW is solid, correct dphidx_B */
      if(dom_matrix[index_-Nx*Ny-1]){

	e_x = eps_x[index_-Nx*Ny];

	/* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[12], sqrt_k[21],
					 Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny], -1);

	dphidx_B = __central_b_der(e_x, dphidxb_aux, sqrt_k[21], sqrt_k[12], Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
      }

      /* If BN is solid, correct dphidy_B */
      if(dom_matrix[index_-Nx*Ny+Nx]){

	e_y = eps_y[index_-Nx*Ny];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[9],
					 Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny], 1);

	dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[12], sqrt_k[9], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
      }

      /* If BS is solid, correct dphidy_B */
      if(dom_matrix[index_-Nx*Ny-Nx]){

	e_y = eps_y[index_-Nx*Ny];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[12], sqrt_k[15],
					 Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny], -1);

	dphidy_B = __central_b_der(e_y, dphidyb_aux, sqrt_k[15], sqrt_k[12], Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
      }

      /* Now we have dphidzb_z, dphidx_P, dphidx_B, dphidy_P, dphidy_B for T cases */
    } /* end case T-P */

    /*******************************************************************/        
    /* B-P line intersects with curve */
    if(B_is_solid_OK && !cell_done_OK){

      cell_done_OK = 1;
      e_z = eps_z[index_];

      /* dphidzb_z is used for calculation of dphidN on B cases */
      dphidzb_z = __get_b_derivative_value_Dirichlet(e_z, eps_dist_tol, 0, sqrt_k[13], sqrt_k[14],
					 Delta_zB[index_], Delta_zT[index_], -1);

      /* dphidx_P, dphidx_T, dphidy_P, dphidy_T required for B case */
      dphidx_P = __central_der(sqrt_k[22], sqrt_k[13], sqrt_k[4], Delta_xE[index_], Delta_xW[index_]);
      dphidx_T = __central_der(sqrt_k[23], sqrt_k[14], sqrt_k[5], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
      dphidy_P = __central_der(sqrt_k[16], sqrt_k[13], sqrt_k[10], Delta_yN[index_], Delta_yS[index_]);
      dphidy_T = __central_der(sqrt_k[17], sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);

      /* If TE is solid, correct dphidx_T */
      if(dom_matrix[index_+Nx*Ny+1]){

	e_x = eps_x[index_+Nx*Ny];
	
	/* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[5],
					 Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny], 1);

	dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[14], sqrt_k[5], Delta_xE[index_], Delta_xW[index_]);
      }

      /* If TW is solid, correct dphidx_T */
      if(dom_matrix[index_+Nx*Ny-1]){

	e_x = eps_x[index_+Nx*Ny];

	/* No need to store this value */
	dphidxb_aux = __get_b_derivative_value_Dirichlet(e_x, eps_dist_tol, 0, sqrt_k[14], sqrt_k[23],
					 Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny], -1);

	dphidx_T = __central_b_der(e_x, dphidxb_aux, sqrt_k[23], sqrt_k[14], Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
      }

      /* If TN is solid, correct dphidy_T */
      if(dom_matrix[index_+Nx*Ny+Nx]){

	e_y = eps_y[index_+Nx*Ny];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[11],
					 Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny], 1);

	dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[14], sqrt_k[11], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
      }

      /* If TS is solid, correct dphidy_T */
      if(dom_matrix[index_+Nx*Ny-Nx]){

	e_y = eps_y[index_+Nx*Ny];

	/* No need to store this value */
	dphidyb_aux = __get_b_derivative_value_Dirichlet(e_y, eps_dist_tol, 0, sqrt_k[14], sqrt_k[17],
					 Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny], -1);

	dphidy_T = __central_b_der(e_y, dphidyb_aux, sqrt_k[17], sqrt_k[14], Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
      }

      /* Now we have dphidzb_z, dphidx_P, dphidx_T, dphidy_P, dphidy_T for B cases */      
    }  /* end case B-P */

    if(dom_matrix[index_-1] || dom_matrix[index_+1]){
      /* W-E case 
	 Ec 28 Sato */
      if(dom_matrix[index_+1]){
	dphidyb_x = __linear_extrap_to_b(dphidy_P, dphidy_W, eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	dphidzb_x = __linear_extrap_to_b(dphidz_P, dphidz_W, eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
      }
      else{
	dphidyb_x = __linear_extrap_to_b(dphidy_P, dphidy_E, eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	dphidzb_x = __linear_extrap_to_b(dphidz_P, dphidz_E, eps_x[index_], Delta_xW[index_], Delta_xE[index_]);	
      }
	
      dphidN = dphidxb_x * norm_vec_x_x[index_] + dphidyb_x * norm_vec_x_y[index_] +
	dphidzb_x * norm_vec_x_z[index_];
      eps_bnd_x[count] = 2 * mu * (dphidN * dphidN) / rho;
      
    }
    if(dom_matrix[index_-Nx] || dom_matrix[index_+Nx]){
      /* S-N case */
      if(dom_matrix[index_+Nx]){
	dphidxb_y = __linear_extrap_to_b(dphidx_P, dphidx_S, eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
	dphidzb_y = __linear_extrap_to_b(dphidz_P, dphidz_S, eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
      }
      else{
	dphidxb_y = __linear_extrap_to_b(dphidx_P, dphidx_N, eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	dphidzb_y = __linear_extrap_to_b(dphidz_P, dphidz_N, eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
      }

      dphidN = dphidxb_y * norm_vec_y_x[index_] + dphidyb_y * norm_vec_y_y[index_] +
	dphidzb_y * norm_vec_y_z[index_];;
      eps_bnd_y[count] = 2 * mu * (dphidN * dphidN) / rho;		
    }

    if(dom_matrix[index_-Nx*Ny] || dom_matrix[index_+Nx*Ny]){
      /* B-T case */
      if(dom_matrix[index_+Nx*Ny]){
	dphidxb_z = __linear_extrap_to_b(dphidx_P, dphidx_B, eps_z[index_], Delta_zT[index_], Delta_zB[index_]);
	dphidyb_z = __linear_extrap_to_b(dphidy_P, dphidy_B, eps_z[index_], Delta_zT[index_], Delta_zB[index_]);	
      }
      else{
	dphidxb_z = __linear_extrap_to_b(dphidx_P, dphidx_T, eps_z[index_], Delta_zB[index_], Delta_zT[index_]);
	dphidyb_z = __linear_extrap_to_b(dphidy_P, dphidy_T, eps_z[index_], Delta_zB[index_], Delta_zT[index_]);
      }

      dphidN = dphidxb_z * norm_vec_z_x[index_] + dphidyb_z * norm_vec_z_y[index_] +
	dphidzb_z  * norm_vec_z_z[index_];
      eps_bnd_z[count] = 2 * mu * (dphidN * dphidN) / rho;
    }


  } /* end for(count=0; count<boundary_cell_count; count++) */
 
  return;
  
}

/* COMPUTE_MU_TURB_AT_VEL_NODES_3D */
/*****************************************************************************/
/**

  Sets:

  Array (double, nxNyNz) Data_Mem::mu_turb_at_u_nodes

  Array (double, NxnyNz) Data_Mem::mu_turb_at_v_nodes

  Array (double, NxNynz) Data_Mem::mu_turb_at_w_nodes
 
  @param data

  @return void
  
  MODIFIED: 01-05-2020
*/
void compute_mu_turb_at_vel_nodes_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  double *mu_turb_at_w_nodes = data->mu_turb_at_w_nodes;

  const double *mu_turb = data->mu_turb;
  const double *fe = data->fe;
  const double *fn = data->fn;
  const double *ft = data->ft;

#pragma omp parallel for default(none) private(I, i, J, K, index_u, index_) \
  shared(mu_turb, mu_turb_at_u_nodes, fe)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
      
	I = i + 1;
	index_u = K*nx*Ny + J*nx + i;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_turb_at_u_nodes[index_u] = mu_turb[index_-1] * fe[index_-1] + 
	  mu_turb[index_] * (1 - fe[index_-1]);
      }
    }
  }

#pragma omp parallel for default(none) private(I, J, j, K, index_v, index_) \
  shared(mu_turb_at_v_nodes, mu_turb, fn)

  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
      
	J = j + 1;
	index_v = K*Nx*ny + j*Nx + I;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_turb_at_v_nodes[index_v] = mu_turb[index_-Nx] * fn[index_-Nx] +
	  mu_turb[index_] * (1 - fn[index_-Nx]);
      }
    }
  }

#pragma omp parallel for default(none) private(I, J, K, k, index_w, index_) \
  shared(mu_turb_at_w_nodes, mu_turb, ft)

  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){

	K = k + 1;
	index_w = k*Nx*Ny + J*Nx + I;
	index_ = K*Nx*Ny + J*Nx + I;
	mu_turb_at_w_nodes[index_w] = mu_turb[index_-Nx*Ny] * ft[index_-Nx*Ny] +
	  mu_turb[index_] * (1 - ft[index_-Nx*Ny]);
      }
    }
  }

  return;
}

/* SET_TURB_INIT_VALUES_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) Data_Mem::k_turb: Set initial value.

  Array (double, NxNyNz) Data_Mem::eps_turb: Set initial value.

  Array (double, NxNyNz) Data_Mem::mu_turb: Set initial value.

  @param data
    
  @return void
    
  MODIFIED: 04-08-2020
*/
void set_turb_init_values_3D(Data_Mem *data){

  int I, J, K, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int use_y_wall_OK = data->use_y_wall_OK;

  const int *dom_matrix = data->domain_matrix;

  double k0, eps0, mu_turb0;
  double max_y_wall;

  const double mu_eff0 = data->mu_eff0;
  const double l0 = data->l0;
  const double C_mu = data->C_mu;
  const double rho = data->rho_v[0];
  const double mu = data->mu;

  double *k_turb = data->k_turb;
  double *eps_turb = data->eps_turb;
  double *mu_turb = data->mu_turb;

  const double *y_wall = data->y_wall;

  /* Initial values */

  /* Ec. (10) Kuzmin, Mierka */
  k0 = powf(mu_eff0 / (rho * l0), 2);
  /* Ec. (10) Kuzmin, Mierka */
  eps0 = C_mu * powf(k0, 1.5) / l0;
  mu_turb0 = MAX(0, mu_eff0 - mu);

  max_y_wall = get_max(y_wall, Nx, Ny, Nz);

#pragma omp parallel for default(none) private(I, J, K, index_) \
  shared(k_turb, eps_turb, mu_turb, k0, eps0, mu_turb0, dom_matrix, y_wall, max_y_wall)
  
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	
	if(dom_matrix[index_] == 0){
	  
	  k_turb[index_] = (use_y_wall_OK)? k0 * y_wall[index_] / max_y_wall : k0;
	  eps_turb[index_] = eps0;
	  mu_turb[index_] = mu_turb0;
	}
      }
    }
  }


  /* Boundary values initialization */
  /* Helps to get consistent starting field for k and eps calculation loops */

#pragma omp parallel sections default(none) private(I, J, K, index_)	\
  shared(dom_matrix, k_turb, eps_turb, k0, eps0, mu_turb0, y_wall, max_y_wall, mu_turb)
  {

#pragma omp section
    {
      /* West */
      I = 0;
      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  if(dom_matrix[index_] == 0){
	    k_turb[index_] = (use_y_wall_OK)? k0 * y_wall[index_] / max_y_wall : k0;
	    eps_turb[index_] = eps0;
	    mu_turb[index_] = mu_turb0;
	  }
	}
      }
    }

#pragma omp section
    {
      /* East */
      I = Nx - 1;
      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  if(dom_matrix[index_] == 0){
	    k_turb[index_] = (use_y_wall_OK)? k0 * y_wall[index_] / max_y_wall : k0;
	    eps_turb[index_] = eps0;
	    mu_turb[index_] = mu_turb0;
	  }
	}
      }
    }

#pragma omp section
    {
      /* South */
      J = 0;
      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  if(dom_matrix[index_] == 0){
	    k_turb[index_] = (use_y_wall_OK)? k0 * y_wall[index_] / max_y_wall : k0;
	    eps_turb[index_] = eps0;
	    mu_turb[index_] = mu_turb0;
	  }
	}
      }
    }

#pragma omp section
    {
      /* North */
      J = Ny - 1;
      for(K=1; K<Nz-1; K++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  if(dom_matrix[index_] == 0){
	    k_turb[index_] = (use_y_wall_OK)? k0 * y_wall[index_] / max_y_wall : k0;
	    eps_turb[index_] = eps0;
	    mu_turb[index_] = mu_turb0;
	  }
	}
      }
    }

#pragma omp section
    {
      /* Bottom */
      K = 0;
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  if(dom_matrix[index_] == 0){
	    k_turb[index_] = (use_y_wall_OK)? k0 * y_wall[index_] / max_y_wall : k0;
	    eps_turb[index_] = eps0;
	    mu_turb[index_] = mu_turb0;
	  }
	}
      }
    }

#pragma omp section
    {
      /* Top */
      K = Nz - 1;
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  if(dom_matrix[index_] == 0){
	    k_turb[index_] = (use_y_wall_OK)? k0 * y_wall[index_] / max_y_wall : k0;
	    eps_turb[index_] = eps0;
	    mu_turb[index_] = mu_turb0;
	  }
	}
      }
    }    
  }

  return;
}

/* _GET_DER_SQRT_K_B */
/*****************************************************************************/
/**

   Get derivative at boundary, assuming boundary value is zero.

   @return double

   MODIFIED: 24-03-2020
*/
inline double _get_der_sqrt_k_b(double e_, const double e_tol, const double phi_c,
			       const double phi_f, const double Delta, const double dir_fact){

  double der_at_b;

  /* Taking care of small eps_x values*/  
  if(e_ > e_tol){
    der_at_b = dir_fact * der_sqrt_k_b(e_, phi_c, phi_f, Delta);
  }
  else{
    der_at_b = -dir_fact * phi_f / ((1 + e_) * Delta);
  }
			       
  return der_at_b;
}

/* __GET_B_DERIVATIVE_VALUE_DIRICHLET */
/*****************************************************************************/
/**

   Get derivative at Dirichlet boundary type, assuming boundary value is phi_b, 
   using Eq 14 of Sato.

   @param e_: Normalized distance to boundary:

   @param e_tol: Lower limit acceptable for e_.

   @param phi_b: Boundary value.

   @param phi_c: Value closest to boundary.

   @param phi_f: Value far from boundary.

   @param Dic: Distance from phi_c to phi inside boundary.

   @param Dcf: Distance from phi_c to phi_f.

   @param dir_fact == -1 for solid boundary at W || S || B (-+-), 
   +1 for solid boundary at E || N || T (+-+)

   @return der_at_b: Pointer to address where derivative value is returned.

   MODIFIED: 13-07-2020
*/
double __get_b_derivative_value_Dirichlet(const double e_, const double e_tol,
					  const double phi_b, const double phi_c,
					  const double phi_f, const double Dic,
					  const double Dcf, const int dir_fact){

  double der_at_b;
  
  /* Distance to boundary from c */
  const double Dbc = Dic * e_;
  
  /* Taking care of small eps_x values*/
  if(e_ > e_tol){
    
    der_at_b = __asymm_b_der(phi_b, phi_c, phi_f, Dbc, Dcf, dir_fact);

  }
  else{
    
    der_at_b = dir_fact * (phi_b - phi_f) / (Dcf + Dbc);
    
  }
  
  return der_at_b;
}
