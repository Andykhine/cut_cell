#include "include/some_defs.h"

/* 
   0: Hayase regular grid
   2: Kirkpatrick with Hayase treatment
*/
#define QUICK_SCHEME 2

/* COEFFS_FLOW_u_QUICK */
/*****************************************************************************/
/**
  
   Sets:

   Structure coeffs_u: coefficients of u momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 01-09-2020
*/
void coeffs_flow_u_quick(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face, index_u;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int p_source_orientation = data->p_source_orientation;
  const int u_slave_count = data->u_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_u_face_x_count = data->diff_u_face_x_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
  const int *diff_u_face_x_I = data->diff_u_face_x_I;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds;
  double Fe, Fw, Fn, Fs;
  double uP, uE, uW, uN, uS;
  double uWW, uEE, uSS, uNN;
  double aP0, S_diff;
  double dEE, dWW, dE, dW, de, dw;
  double dNN, dSS, dN, dS, dn, ds;
  double Se, Sw, Sn, Ss;
  double Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 2; // For 2D case
  const double periodic_source = data->periodic_source;
 
  double *aP_u = data->coeffs_u.aP;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *b_u = data->coeffs_u.b;

  const double *u = data->u;
  const double *p = data->p;

  const double *u_faces_x = data->u_faces_x;
  const double *diff_factor_u_face_x = data->diff_factor_u_face_x;

  const double *Du_x = data->Du_x;
  const double *Du_y = data->Du_y;
  const double *Fu_x = data->Fu_x;
  const double *Fu_y = data->Fu_y;
  const double *u_sol_factor = data->u_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  const double *u_cut_fe = data->u_cut_fe;
  const double *u_cut_fw = data->u_cut_fw;
  const double *vol_x = data->vol_x;
  const double *alpha_u_x = data->alpha_u_x;
  const double *alpha_u_y = data->alpha_u_y;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *u0 = data->u0;

#ifdef OLDINT_OK
  const double *cudE = data->Delta_xE_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudW = data->Delta_xW_u; /**< Distance from node to West neighbour, u nodes. */
  const double *cudN = data->Delta_yN_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudS = data->Delta_yS_u; /**< Distance from node to West neighbour, u nodes. */
#else
  const double *cudE = data->cudE;
  const double *cudW = data->cudW;
  const double *cudN = data->cudN;
  const double *cudS = data->cudS;
#endif

  const double *Dx = data->Delta_x;
  const double *Dy = data->Delta_y;
  
  set_D_u(data);
  set_F_u(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_u, index_face, De, Dw, Dn, Ds, Fe, Fw, Fn, Fs, \
	  dpA, Fw_positive_OK, Fe_positive_OK, Fs_positive_OK, Fn_positive_OK, \
	  dEE, dWW, dE, dW, de, dw,					\
	  dNN, dSS, dN, dS, dn, ds,					\
	  Se, Sw, Sn, Ss,						\
	  Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam,		\
	  uP, uE, uW, uN, uS, uWW, uEE, uSS, uNN)			\
  shared(u_dom_matrix, aE_u, aW_u, aS_u, aN_u, aP_u, b_u, u, p, u_faces_x, \
	 Du_x, Du_y, Fu_x, Fu_y, mu_turb_at_u_nodes,			\
	 Dx, Dy, cudE, cudW, cudN, cudS,				\
	 alpha_u_x, alpha_u_y)
  
  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      index_u = J*nx + i;

      if(u_dom_matrix[index_u] == 0){

	I = i+1;      
	j = J-1;
      
	index_ = J*Nx + I;
	index_face = j*nx + i;

	/* Distances */
	dE = cudE[index_u];
	dW = cudW[index_u];
	dN = cudN[index_u];
	dS = cudS[index_u];

	dEE = dE + cudE[index_u + 1];
	dWW = dW + cudW[index_u - 1];
	dNN = dN + cudN[index_u + nx];
	dSS = dS + cudS[index_u - nx];

	de = 0.5 * Dx[index_];
	dw = 0.5 * Dx[index_-1];
	dn = 0.5 * Dy[index_];
	ds = dn;

	/* Velocities */
	uP = u[index_u];
	uE = u[index_u + 1];
	uW = u[index_u - 1];
	uN = u[index_u + nx];
	uS = u[index_u - nx];

	/* Diffusive */
	De = Du_x[index_];
	Dw = Du_x[index_-1];
	Dn = Du_y[index_face+nx];
	Ds = Du_y[index_face];

	/* Convective */
	Fe = Fu_x[index_] * alpha_u_x[index_] * alpha_u_x[index_];
	Fw = Fu_x[index_-1] * alpha_u_x[index_-1] * alpha_u_x[index_-1];
	Fn = Fu_y[index_face+nx] * alpha_u_y[index_face+nx];
	Fs = Fu_y[index_face] * alpha_u_y[index_face];
	
	Fe_positive_OK = (Fe > 0);
	Fw_positive_OK = (Fw > 0);
	Fn_positive_OK = (Fn > 0);
	Fs_positive_OK = (Fs > 0);

	/* Extrapolation for presence of boundaries */
	/* Should be done for internal boundaries as well */
	
	if(i == 1){
	  uWW = 2 * uW - uP;
	  dWW = 2*dW;
	}
	else if (u_dom_matrix[index_u - 2] != 0){
	  /* WW within solid */
	  uWW = 2 * uW - uP;
	  dWW = 2*dW;
	}
	else{
	  uWW = u[index_u-2];
	}

	if(i == nx-2){
	  uEE = 2 * uE - uP;
	  dEE = 2*dE;
	}
	else if(u_dom_matrix[index_u + 2] != 0){
	  /* EE within solid */
	  uEE = 2 * uE - uP;
	  dEE = 2*dE;
	}
	else{
	  uEE = u[index_u+2];
	}

	if(J == 1){
	  uSS = 2 * uS - uP;
	  dSS = 2*dS;
	}
	else if(u_dom_matrix[index_u - 2*nx] != 0){
	  uSS = 2 * uS - uP;
	  dSS = 2*dS;
	}
	else{
	  uSS = u[index_u-2*nx];
	}

	if(J == Ny-2){
	  uNN = 2 * uN - uP;
	  dNN = 2*dN;
	}
	else if(u_dom_matrix[index_u + 2*nx] != 0){
	  uNN = 2 * uN - uP;
	  dNN = 2*dN;
	}
	else{
	  uNN = u[index_u+2*nx];
	}

	dpA = u_faces_x[index_] * p[index_] - 
	  u_faces_x[index_-1] * p[index_-1];

	b_u[index_u] = -dpA;
	
#if(QUICK_SCHEME == 0) /* Hayase regular grid */

	aE_u[index_u] = De - (1 - Fe_positive_OK) * Fe;
	aW_u[index_u] = Dw + Fw_positive_OK * Fw;
	aN_u[index_u] = Dn - (1 - Fn_positive_OK) * Fn;
	aS_u[index_u] = Ds + Fs_positive_OK * Fs;

	aP_u[index_u] = aE_u[index_u] + aW_u[index_u] + aN_u[index_u] + aS_u[index_u];
	  //	  De + Dw + Dn + Ds + Fe_positive_OK * Fe - (1 - Fw_positive_OK) * Fw + Fn_positive_OK * Fn - (1 - Fs_positive_OK) * Fs;

	Se = 0.125 * Fe_positive_OK * Fe * (   -uW - 2 * uP + 3 * uE) +
	  0.125 * (1 - Fe_positive_OK) * Fe * (3 * uP - 2 * uE - uEE);

	Sw = 0.125 * Fw_positive_OK * Fw * (  -uWW - 2 * uW + 3 * uP) +
	  0.125 * (1 - Fw_positive_OK) * Fw * (3 * uW - 2 * uP - uE) ;

	Sn = 0.125 * Fn_positive_OK * Fn * (   -uS - 2 * uP + 3 * uN) +
	  0.125 * (1 - Fn_positive_OK) * Fn * (3 * uP - 2 * uN - uNN);

	Ss = 0.125 * Fs_positive_OK * Fs * (  -uSS - 2 * uS + 3 * uP) +
	  0.125 * (1 - Fs_positive_OK) * Fs * (3 * uS - 2 * uP - uN);

	b_u[index_u] += Sw - Se + Ss - Sn;
	
#elif(QUICK_SCHEME == 2) /* Kirkpatrick-Hayase */
	
	aE_u[index_u] = De + MAX(-Fe, 0);
	aW_u[index_u] = Dw + MAX(+Fw, 0);
	aN_u[index_u] = Dn + MAX(-Fn, 0);
	aS_u[index_u] = Ds + MAX(+Fs, 0);

	aP_u[index_u] = aE_u[index_u] + aW_u[index_u] + aN_u[index_u] + aS_u[index_u];
	// aE_u[index_u] + aW_u[index_u] + aN_u[index_u] + aS_u[index_u] + (Fe - Fw) + (Fn - Fs);

	// e
	Seap =
	  uW * (de/dW) * (de - dE)/(dW + dE) +
	  uP * ( (dE - de)/dE * (1 + de/dW) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uE * (de/dE) * (1 + (de - dE)/(dW + dE));
	
	Seam =
	  uP  * (1 - de/dEE) * (dE - de)/dE +
	  uE  * ( (1 + (dE - de)/(dEE - dE)) * (de/dE) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uEE * (de - dE)/(dEE - dE) * (de/dEE);
	  
	Se = Fe * (Fe_positive_OK * Seap + (1 - Fe_positive_OK) * Seam);

	// w
	Swam =
	  uE * (dw/dE) * (dw - dW)/(dW + dE) +
	  uP * ( (dW - dw)/dW * (1 + dw/dE) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uW * (dw/dW) * (1 + (dw - dW)/(dW + dE));
	
	Swap =
	  uP  * (1 - dw/dWW) * (dW - dw)/dW +
	  uW  * ( (1 + (dW - dw)/(dWW - dW)) * (dw/dW) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uWW * (dw - dW)/(dWW - dW) * (dw/dWW);
	  
	Sw = Fw * (Fw_positive_OK * Swap + (1 - Fw_positive_OK) * Swam);

	// n
	Snap =
	  uS * (dn/dS) * (dn - dN)/(dS + dN) +
	  uP * ( (dN - dn)/dN * (1 + dn/dS) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uN * (dn/dN) * (1 + (dn - dN)/(dS + dN));
	
	Snam =
	  uP  * (1 - dn/dNN) * (dN - dn)/dN +
	  uN  * ( (1 + (dN - dn)/(dNN - dN)) * (dn/dN) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uNN * (dn - dN)/(dNN - dN) * (dn/dNN);
	  
	Sn = Fn * (Fn_positive_OK * Snap + (1 - Fn_positive_OK) * Snam);

	// s
	Ssam =
	  uN * (ds/dN) * (ds - dS)/(dS + dN) +
	  uP * ( (dS - ds)/dS * (1 + ds/dN) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uS * (ds/dS) * (1 + (ds - dS)/(dS + dN));
	
	Ssap =
	  uP  * (1 - ds/dSS) * (dS - ds)/dS +
	  uS  * ( (1 + (dS - ds)/(dSS - dS)) * (ds/dS) - 1 ) + // here modification needed (substract 1).. DONE!!
	  uSS * (ds - dS)/(dSS - dS) * (ds/dSS);
	  
	Ss = Fs * (Fs_positive_OK * Ssap + (1 - Fs_positive_OK) * Ssam);

	b_u[index_u] += Sw - Se + Ss - Sn;
#endif
      }
      else{
	aP_u[index_u] = 1;
	aW_u[index_u] = 0;
	aE_u[index_u] = 0;
	aS_u[index_u] = 0;
	aN_u[index_u] = 0;
	b_u[index_u] = 0;
      }
    }
  }

  /* Diffusive factor */  
  for(z=0; z<diff_u_face_x_count; z++){
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    I = diff_u_face_x_I[z];
    mu_eff = mu + 0.5 * (mu_turb_at_u_nodes[index_u] + mu_turb_at_u_nodes[index_u+1]);
    S_diff = mu_eff * diff_factor_u_face_x[z] * (u[index_u] * u_cut_fw[index_] + u[index_u+1] * u_cut_fe[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((u_dom_matrix[index_u] == 0) && (I > 1)){
      b_u[index_u] -= S_diff;
    }
    if((u_dom_matrix[index_u+1] == 0) && (I < Nx-2)){
      b_u[index_u+1] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<u_sol_factor_count; z++){
      index_u = u_sol_factor_index_u[z];
      
      if(u_dom_matrix[index_u] == 0){

	index_ = u_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_u_nodes[index_u];
	aP_u[index_u] += mu_eff * u_sol_factor[z];
	b_u[index_u] += u_p_factor[z] * (u_p_factor_W[z] * p[index_-1] 
					 + (1 - u_p_factor_W[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 1){

#pragma omp parallel for default(none) private(i, J, index_u)	\
  shared(u_dom_matrix, b_u, vol_x)

    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = J*nx + i;
	if(u_dom_matrix[index_u] == 0){
	  b_u[index_u] += periodic_source * vol_x[index_u];
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(i, J, index_u, aP0)	\
  shared(u_dom_matrix, vol_x, aP_u, b_u, u0)

    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = J*nx + i;
	if(u_dom_matrix[index_u] == 0){
	  aP0 = rho * vol_x[index_u] / dt;
	  aP_u[index_u] += aP0;
	  b_u[index_u] += aP0 * u0[index_u];
	}
      }
    }
  }

  /* Add custom source term */

  if(add_momentum_source_OK){
    add_u_momentum_source(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs(u, u_dom_matrix, 0, data->coeffs_u, nx, Ny, data->alpha_u);
  }

  /* Set coefficients for slave cells */
  for(i=0; i<u_slave_count; i++){
    index_u = u_slave_nodes[i];
    aP_u[index_u] = 1;
    aW_u[index_u] = 0;
    aE_u[index_u] = 0;
    aS_u[index_u] = 0;
    aN_u[index_u] = 0;
    b_u[index_u] = 0;
  }
  
  return;
}

/* COEFFS_FLOW_v_QUICK */
/**
  
   Sets:

   Structure coeffs_v: coefficients of v momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 01-09-2020
*/
void coeffs_flow_v_quick(Data_Mem *data){

  int I, J, i, j, z;
  int index_, index_face, index_v;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int v_slave_count = data->v_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int p_source_orientation = data->p_source_orientation;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_v_face_y_count = data->diff_v_face_y_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *v_dom_matrix = data->v_dom_matrix;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
  const int *diff_v_face_y_J = data->diff_v_face_y_J;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds;
  double Fe, Fw, Fn, Fs;
  double vP, vE, vW, vN, vS;
  double vWW, vEE, vSS, vNN;
  double aP0, S_diff;
  double dEE, dWW, dE, dW, de, dw;
  double dNN, dSS, dN, dS, dn, ds;
  double Se, Sw, Sn, Ss;
  double Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dt = data->dt[3] / 2; // For 2D case
  const double periodic_source = data->periodic_source;
 
  double *aP_v = data->coeffs_v.aP;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *b_v = data->coeffs_v.b;

  const double *v = data->v;
  const double *p = data->p;

  const double *v_faces_y = data->v_faces_y;
  const double *diff_factor_v_face_y = data->diff_factor_v_face_y;
  const double *Dv_x = data->Dv_x;
  const double *Dv_y = data->Dv_y;
  const double *Fv_x = data->Fv_x;
  const double *Fv_y = data->Fv_y;
  const double *v_sol_factor = data->v_sol_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  const double *v_cut_fn = data->v_cut_fn;
  const double *v_cut_fs = data->v_cut_fs;
  const double *vol_y = data->vol_y;
  const double *alpha_v_x = data->alpha_v_x;
  const double *alpha_v_y = data->alpha_v_y;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *v0 = data->v0;

#ifdef OLDINT_OK
  const double *cvdE = data->Delta_xE_v;
  const double *cvdW = data->Delta_xW_v;
  const double *cvdN = data->Delta_yN_v;
  const double *cvdS = data->Delta_yS_v;
#else
  const double *cvdE = data->cvdE;
  const double *cvdW = data->cvdW;
  const double *cvdN = data->cvdN;
  const double *cvdS = data->cvdS;
#endif

  const double *Dx = data->Delta_x;
  const double *Dy = data->Delta_y;
  
  set_D_v(data);
  set_F_v(data);
  
  /* V */
#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_v, index_face, De, Dw, Dn, Ds, Fe, Fw, Fn, Fs, \
	  dpA, Fw_positive_OK, Fe_positive_OK, Fs_positive_OK, Fn_positive_OK, \
	  dEE, dWW, dE, dW, de, dw,					\
	  dNN, dSS, dN, dS, dn, ds,					\
	  Se, Sw, Sn, Ss,						\
  	  Seap, Seam, Swap, Swam, Snap, Snam, Ssap, Ssam,		\
	  vP, vE, vW, vN, vS, vWW, vEE, vSS, vNN)			\
  shared(v_dom_matrix, aE_v, aW_v, aS_v, aN_v, aP_v, b_v, v, p, v_faces_y, \
	 Dv_x, Dv_y, Fv_x, Fv_y, mu_turb_at_v_nodes,			\
	 Dx, Dy, cvdE, cvdW, cvdN, cvdS,				\
	 alpha_v_x, alpha_v_y)

  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){

      index_v = j*Nx + I;

      if(v_dom_matrix[index_v] == 0){

	i = I-1;
	J = j+1;
      
	index_ = J*Nx + I;
	index_face = j*nx + i;

	/* Distances */
	dE = cvdE[index_v];
	dW = cvdW[index_v];
	dN = cvdN[index_v];
	dS = cvdS[index_v];

	dEE = dE + cvdE[index_v + 1];
	dWW = dW + cvdW[index_v - 1];
	dNN = dN + cvdN[index_v + Nx];
	dSS = dS + cvdS[index_v - Nx];

	de = 0.5 * Dx[index_];
	dw = de;
	dn = 0.5 * Dy[index_];
	ds = 0.5 * Dy[index_-Nx];

	/* Velocities */
	vP = v[index_v];
	vE = v[index_v + 1];
	vW = v[index_v - 1];
	vN = v[index_v + Nx];
	vS = v[index_v - Nx];
	
	/* Diffusive */
	De = Dv_x[index_face+1];
	Dw = Dv_x[index_face];
	Dn = Dv_y[index_];
	Ds = Dv_y[index_-Nx];

	/* Convective */
	Fe = Fv_x[index_face+1] * alpha_v_x[index_face+1];
	Fw = Fv_x[index_face] * alpha_v_x[index_face];
	Fn = Fv_y[index_] * alpha_v_y[index_] * alpha_v_y[index_];
	Fs = Fv_y[index_-Nx] * alpha_v_y[index_-Nx] * alpha_v_y[index_-Nx];

	Fe_positive_OK = (Fe > 0);
	Fw_positive_OK = (Fw > 0);
	Fn_positive_OK = (Fn > 0);
	Fs_positive_OK = (Fs > 0);

	if(I == 1){
	  vWW = 2 * vW - vP;
	  dWW = 2*dW;
	}
	else if(v_dom_matrix[index_v - 2] != 0){
	  vWW = 2 * vW - vP;
	  dWW = 2*dW;
	}
	else{
	  vWW = v[index_v-2];
	}

	if(I == Nx-2){
	  vEE = 2 * vE - vP;
	  dEE = 2*dE;
	}
	else if(v_dom_matrix[index_v + 2] != 0){
	  vEE = 2 * vE - vP;
	  dEE = 2*dE;
	}
	else{
	  vEE = v[index_v+2];
	}
	
	if(j == 1){
	  vSS = 2 * vS - vP;
	  dSS = 2*dS;
	}
	else if(v_dom_matrix[index_v - 2*Nx] != 0){
	  vSS = 2 * vS - vP;
	  dSS = 2*dS;
	}
	else{
	  vSS = v[index_v-2*Nx];
	}

	if(j == ny-2){
	  vNN = 2 * vN - vP;
	  dNN = 2*dN;
	}
	else if(v_dom_matrix[index_v + 2*Nx] != 0){
	  vNN = 2 * vN - vP;
	  dNN = 2*dN;
	}
	else{
	  vNN = v[index_v+2*Nx];
	}

	dpA = v_faces_y[index_] * p[index_] - 
	  v_faces_y[index_-Nx] * p[index_-Nx];

	b_v[index_v] = -dpA;

#if(QUICK_SCHEME == 0) /* Hayase regular grid */
	
	aE_v[index_v] = De - (1 - Fe_positive_OK) * Fe;
	aW_v[index_v] = Dw + Fw_positive_OK * Fw;
	aN_v[index_v] = Dn - (1 - Fn_positive_OK) * Fn;
	aS_v[index_v] = Ds + Fs_positive_OK * Fs;

	aP_v[index_v] = aE_v[index_v] + aW_v[index_v] + aN_v[index_v] + aS_v[index_v];
	  //	  De + Dw + Dn + Ds + Fe_positive_OK * Fe - (1 - Fw_positive_OK) * Fw + Fn_positive_OK * Fn - (1 - Fs_positive_OK) * Fs;

	Se = 0.125 * Fe_positive_OK * Fe * (   -vW - 2 * vP + 3 * vE) +
	  0.125 * (1 - Fe_positive_OK) * Fe * (3 * vP - 2 * vE - vEE);
	
	Sw = 0.125 * Fw_positive_OK * Fw * (  -vWW - 2 * vW + 3 * vP) +
	  0.125 * (1 - Fw_positive_OK) * Fw * (3 * vW - 2 * vP - vE) ;
	
	Sn = 0.125 * Fn_positive_OK * Fn * (   -vS - 2 * vP + 3 * vN) +
	  0.125 * (1 - Fn_positive_OK) * Fn * (3 * vP - 2 * vN - vNN);
	
	Ss = 0.125 * Fs_positive_OK * Fs * (  -vSS - 2 * vS + 3 * vP) +
	  0.125 * (1 - Fs_positive_OK) * Fs * (3 * vS - 2 * vP - vN);

	b_v[index_v] += Sw - Se + Ss - Sn;
	
#elif(QUICK_SCHEME == 2) /* Kirkpatrick-Hayase */
	
	aE_v[index_v] = De + MAX(-Fe, 0);
	aW_v[index_v] = Dw + MAX(+Fw, 0);
	aN_v[index_v] = Dn + MAX(-Fn, 0);
	aS_v[index_v] = Ds + MAX(+Fs, 0);

	aP_v[index_v] = aE_v[index_v] + aW_v[index_v] + aN_v[index_v] + aS_v[index_v];
	//	  aE_v[index_v] + aW_v[index_v] + aN_v[index_v] + aS_v[index_v] + (Fe - Fw) + (Fn - Fs);

	// e
	Seap =
	  vW * (de/dW) * (de - dE)/(dW + dE) +
	  vP * ( (dE - de)/dE * (1 + de/dW) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vE * (de/dE) * (1 + (de - dE)/(dW + dE));
	
	Seam =
	  vP  * (1 - de/dEE) * (dE - de)/dE +
	  vE  * ( (1 + (dE - de)/(dEE - dE)) * (de/dE) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vEE * (de - dE)/(dEE - dE) * (de/dEE);
	  
	Se = Fe * (Fe_positive_OK * Seap + (1 - Fe_positive_OK) * Seam);

	// w
	Swam =
	  vE * (dw/dE) * (dw - dW)/(dW + dE) +
	  vP * ( (dW - dw)/dW * (1 + dw/dE) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vW * (dw/dW) * (1 + (dw - dW)/(dW + dE));
	
	Swap =
	  vP  * (1 - dw/dWW) * (dW - dw)/dW +
	  vW  * ( (1 + (dW - dw)/(dWW - dW)) * (dw/dW) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vWW * (dw - dW)/(dWW - dW) * (dw/dWW);
	  
	Sw = Fw * (Fw_positive_OK * Swap + (1 - Fw_positive_OK) * Swam);

	// n
	Snap =
	  vS * (dn/dS) * (dn - dN)/(dS + dN) +
	  vP * ( (dN - dn)/dN * (1 + dn/dS) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vN * (dn/dN) * (1 + (dn - dN)/(dS + dN));
	
	Snam =
	  vP  * (1 - dn/dNN) * (dN - dn)/dN +
	  vN  * ( (1 + (dN - dn)/(dNN - dN)) * (dn/dN) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vNN * (dn - dN)/(dNN - dN) * (dn/dNN);
	  
	Sn = Fn * (Fn_positive_OK * Snap + (1 - Fn_positive_OK) * Snam);

	// s
	Ssam =
	  vN * (ds/dN) * (ds - dS)/(dS + dN) +
	  vP * ( (dS - ds)/dS * (1 + ds/dN) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vS * (ds/dS) * (1 + (ds - dS)/(dS + dN));
	
	Ssap =
	  vP  * (1 - ds/dSS) * (dS - ds)/dS +
	  vS  * ( (1 + (dS - ds)/(dSS - dS)) * (ds/dS) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	  vSS * (ds - dS)/(dSS - dS) * (ds/dSS);
	  
	Ss = Fs * (Fs_positive_OK * Ssap + (1 - Fs_positive_OK) * Ssam);

	b_v[index_v] += Sw - Se + Ss - Sn;
#endif
      }
      else{
	aP_v[index_v] = 1;
	aW_v[index_v] = 0;
	aE_v[index_v] = 0;
	aS_v[index_v] = 0;
	aN_v[index_v] = 0;
	b_v[index_v] = 0;
      }
    }
  }

  /* Diffusive factor */
  for(z=0; z<diff_v_face_y_count; z++){
    index_v = diff_v_face_y_index_v[z];
    index_ = diff_v_face_y_index[z];
    J = diff_v_face_y_J[z];
    mu_eff = mu + 0.5 * (mu_turb_at_v_nodes[index_v] + mu_turb_at_v_nodes[index_v+Nx]);
    S_diff = mu_eff * diff_factor_v_face_y[z] * (v[index_v] * v_cut_fs[index_] + v[index_v+Nx] * v_cut_fn[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((v_dom_matrix[index_v] == 0) && (J > 1)){
      b_v[index_v] -= S_diff;
    }
    if((v_dom_matrix[index_v+Nx] == 0) && (J < Ny-2)){
      b_v[index_v+Nx] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<v_sol_factor_count; z++){
      index_v = v_sol_factor_index_v[z];
      
      if(v_dom_matrix[index_v] == 0){
	index_ = v_sol_factor_index[z];
	mu_eff = mu + mu_turb_at_v_nodes[index_v];
	aP_v[index_v] += mu_eff * v_sol_factor[z];
	b_v[index_v] += v_p_factor[z] * (v_p_factor_S[z] * p[index_-Nx] 
					 + (1 - v_p_factor_S[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, j, index_v)	\
  shared(v_dom_matrix, b_v, vol_y)

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = j*Nx + I;
	if(v_dom_matrix[index_v] == 0){
	  b_v[index_v] += periodic_source * vol_y[index_v];
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, j, index_v, aP0)	\
  shared(v_dom_matrix, vol_y, aP_v, b_v, v0)

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = j*Nx + I;
	if(v_dom_matrix[index_v] == 0){
	  aP0 = rho * vol_y[index_v] / dt;
	  aP_v[index_v] += aP0;
	  b_v[index_v] += aP0 * v0[index_v];
	}
      }
    }
  }

  /* Add custom source term */

  if(add_momentum_source_OK){
    add_v_momentum_source(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs(v, v_dom_matrix, 0, data->coeffs_v, Nx, ny, data->alpha_v);
  }

  for(i=0; i<v_slave_count; i++){
    index_v = v_slave_nodes[i];
    aP_v[index_v] = 1;
    aW_v[index_v] = 0;
    aE_v[index_v] = 0;
    aS_v[index_v] = 0;
    aN_v[index_v] = 0;
    b_v[index_v] = 0;
  }

  return;
}


/* COEFFS_FLOW_u_QUICK_3D */
/*****************************************************************************/
/**
  
   Sets:

   Structure coeffs_u: coefficients of u momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 01-10-2020
*/
void coeffs_flow_u_quick_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_u;
  int index_face_y, index_face_z;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;
  int Fb_positive_OK, Ft_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
    
  const int p_source_orientation = data->p_source_orientation;
  const int u_slave_count = data->u_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_u_face_x_count = data->diff_u_face_x_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *diff_u_face_x_index = data->diff_u_face_x_index;
  const int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
  const int *diff_u_face_x_I = data->diff_u_face_x_I;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double uP, uE, uW, uN, uS, uT, uB;
  double uWW, uEE, uSS, uNN, uBB, uTT;
  double aP0, S_diff;
  double dEE, dWW, dE, dW, de, dw;
  double dNN, dSS, dN, dS, dn, ds;
  double dTT, dBB, dT, dB, dt, db;
  double Se, Sw, Sn, Ss, St, Sb;
  double Seap, Seam, Swap, Swam;
  double Snap, Snam, Ssap, Ssam;
  double Stap, Stam, Sbap, Sbam;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dtime = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_u = data->coeffs_u.aP;
  double *aW_u = data->coeffs_u.aW;
  double *aE_u = data->coeffs_u.aE;
  double *aS_u = data->coeffs_u.aS;
  double *aN_u = data->coeffs_u.aN;
  double *aB_u = data->coeffs_u.aB;
  double *aT_u = data->coeffs_u.aT;
  double *b_u = data->coeffs_u.b;

  const double *u = data->u;
  const double *p = data->p;

  const double *u_faces_x = data->u_faces_x;
  const double *diff_factor_u_face_x = data->diff_factor_u_face_x;

  const double *Du_x = data->Du_x;
  const double *Du_y = data->Du_y;
  const double *Du_z = data->Du_z;
  const double *Fu_x = data->Fu_x;
  const double *Fu_y = data->Fu_y;
  const double *Fu_z = data->Fu_z;
  const double *u_sol_factor = data->u_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *mu_turb_at_u_nodes = data->mu_turb_at_u_nodes;
  const double *u_cut_fe = data->u_cut_fe;
  const double *u_cut_fw = data->u_cut_fw;
  const double *vol_x = data->vol_x;
  const double *alpha_u_x = data->alpha_u_x;
  const double *alpha_u_y = data->alpha_u_y;
  const double *alpha_u_z = data->alpha_u_z;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *u0 = data->u0;

#ifdef OLDINT_OK
  const double *cudE = data->Delta_xE_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudW = data->Delta_xW_u; /**< Distance from node to West neighbour, u nodes. */
  const double *cudN = data->Delta_yN_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudS = data->Delta_yS_u; /**< Distance from node to West neighbour, u nodes. */
  const double *cudT = data->Delta_zT_u; /**< Distance from node to East neighbour, u nodes. */
  const double *cudB = data->Delta_zB_u; /**< Distance from node to West neighbour, u nodes. */
#else
  const double *cudE = data->cudE;
  const double *cudW = data->cudW;
  const double *cudN = data->cudN;
  const double *cudS = data->cudS;
  const double *cudT = data->cudT;
  const double *cudB = data->cudB;
#endif

  const double *Dx = data->Delta_x;
  const double *Dy = data->Delta_y;
  const double *Dz = data->Delta_z;
  
  set_D_u_3D(data);
  set_F_u_3D(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_u,				\
	  index_face_y, index_face_z,					\
	  De, Dw, Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb,		\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
  	  Fb_positive_OK, Ft_positive_OK,				\
	  dEE, dWW, dE, dW, de, dw,					\
	  dNN, dSS, dN, dS, dn, ds,					\
	  dTT, dBB, dT, dB, dt, db,					\
	  Se, Sw, Sn, Ss, St, Sb,					\
	  Seap, Seam, Swap, Swam,					\
	  Snap, Snam, Ssap, Ssam,					\
	  Stap, Stam, Sbap, Sbam,					\
	  uP, uE, uW, uN, uS, uT, uB,					\
	  uWW, uEE, uSS, uNN, uBB, uTT)					\
  shared(u_dom_matrix,							\
	 aE_u, aW_u, aS_u, aN_u, aT_u, aB_u, aP_u, b_u,			\
	 u, p, u_faces_x,						\
	 Du_x, Du_y, Du_z, Fu_x, Fu_y, Fu_z, mu_turb_at_u_nodes,	\
	 Dx, Dy, Dz, cudE, cudW, cudN, cudS, cudT, cudB, 		\
	 alpha_u_x, alpha_u_y, alpha_u_z)
  for(K=1; K<Nz-1; K++){  
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	
	index_u = K*nx*Ny + J*nx + i;

	if(u_dom_matrix[index_u] == 0){
	
	  I = i+1;      
	  j = J-1;
	  k = K-1;
	
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_face_y = K*nx*ny + j*nx + i; // Du,Fu_y
	  index_face_z = k*nx*Ny + J*nx + i; // Du,Fu_z

	  /* Distances */
	  dE = cudE[index_u];
	  dW = cudW[index_u];
	  dN = cudN[index_u];
	  dS = cudS[index_u];
	  dT = cudT[index_u];
	  dB = cudB[index_u];

	  dEE = dE + cudE[index_u + 1];
	  dWW = dW + cudW[index_u - 1];
	  dNN = dN + cudN[index_u + nx];
	  dSS = dS + cudS[index_u - nx];
	  dTT = dT + cudT[index_u + nx*Ny];
	  dBB = dB + cudB[index_u - nx*Ny];

	  de = 0.5 * Dx[index_];
	  dw = 0.5 * Dx[index_-1];
	  dn = 0.5 * Dy[index_];
	  ds = dn;
	  dt = 0.5 * Dz[index_];
	  db = dt;

	  /* Velocities */
	  uP = u[index_u];
	  uE = u[index_u + 1];
	  uW = u[index_u - 1];
	  uN = u[index_u + nx];
	  uS = u[index_u - nx];
	  uT = u[index_u + nx*Ny];
	  uB = u[index_u - nx*Ny];

	  /* Diffusive */
	  De = Du_x[index_];
	  Dw = Du_x[index_ - 1];
	  Dn = Du_y[index_face_y + nx];
	  Ds = Du_y[index_face_y];
	  Dt = Du_z[index_face_z + nx*Ny];
	  Db = Du_z[index_face_z];

	  /* Convective */
	  Fe = Fu_x[index_] * alpha_u_x[index_] * alpha_u_x[index_];
	  Fw = Fu_x[index_ - 1] * alpha_u_x[index_ - 1] * alpha_u_x[index_ - 1];
	  Fn = Fu_y[index_face_y + nx] * alpha_u_y[index_face_y + nx];
	  Fs = Fu_y[index_face_y] * alpha_u_y[index_face_y];
	  Ft = Fu_z[index_face_z + nx*Ny] * alpha_u_z[index_face_z + nx*Ny];
	  Fb = Fu_z[index_face_z] * alpha_u_z[index_face_z];
	
	  Fe_positive_OK = (Fe > 0);
	  Fw_positive_OK = (Fw > 0);
	  Fn_positive_OK = (Fn > 0);
	  Fs_positive_OK = (Fs > 0);
	  Ft_positive_OK = (Ft > 0);
	  Fb_positive_OK = (Fb > 0);

	  /* Extrapolation for presence of boundaries */
	  /* Should be done for internal boundaries as well */
	
	  if(i == 1){
	    uWW = 2 * uW - uP;
	    dWW = 2*dW;
	  }
	  else if (u_dom_matrix[index_u - 2] != 0){
	    /* WW within solid */
	    uWW = 2 * uW - uP;
	    dWW = 2*dW;
	  }
	  else{
	    uWW = u[index_u-2];
	  }

	  if(i == nx-2){
	    uEE = 2 * uE - uP;
	    dEE = 2*dE;
	  }
	  else if(u_dom_matrix[index_u + 2] != 0){
	    /* EE within solid */
	    uEE = 2 * uE - uP;
	    dEE = 2*dE;
	  }
	  else{
	    uEE = u[index_u+2];
	  }

	  if(J == 1){
	    uSS = 2 * uS - uP;
	    dSS = 2*dS;
	  }
	  else if(u_dom_matrix[index_u - 2*nx] != 0){
	    uSS = 2 * uS - uP;
	    dSS = 2*dS;
	  }
	  else{
	    uSS = u[index_u-2*nx];
	  }

	  if(J == Ny-2){
	    uNN = 2 * uN - uP;
	    dNN = 2*dN;
	  }
	  else if(u_dom_matrix[index_u + 2*nx] != 0){
	    uNN = 2 * uN - uP;
	    dNN = 2*dN;
	  }
	  else{
	    uNN = u[index_u+2*nx];
	  }

	  if(K == 1){
	    uBB = 2 * uB - uP;
	    dBB = 2*dB;
	  }
	  else if(u_dom_matrix[index_u - 2*nx*Ny] != 0){
	    uBB = 2 * uB - uP;
	    dBB = 2*dB;
	  }
	  else{
	    uBB = u[index_u - 2*nx*Ny];
	  }

	  if(K == Nz-2){
	    uTT = 2 * uT - uP;
	    dTT = 2*dT;
	  }
	  else if(u_dom_matrix[index_u + 2*nx*Ny] != 0){
	    uTT = 2 * uT - uP;
	    dTT = 2*dT;
	  }
	  else{
	    uTT = u[index_u + 2*nx*Ny];
	  }

	  dpA = u_faces_x[index_] * p[index_] - 
	    u_faces_x[index_-1] * p[index_-1];

	  b_u[index_u] = -dpA;
	
#if(QUICK_SCHEME == 0) /* Hayase regular grid */

	  exit(EXIT_FAILURE);
	
#elif(QUICK_SCHEME == 2) /* Kirkpatrick-Hayase */
	
	  aE_u[index_u] = De + MAX(-Fe, 0);
	  aW_u[index_u] = Dw + MAX(+Fw, 0);
	  aN_u[index_u] = Dn + MAX(-Fn, 0);
	  aS_u[index_u] = Ds + MAX(+Fs, 0);
	  aT_u[index_u] = Dt + MAX(-Ft, 0);
	  aB_u[index_u] = Db + MAX(+Fb, 0);

	  aP_u[index_u] =
	    aE_u[index_u] + aW_u[index_u] +
	    aN_u[index_u] + aS_u[index_u] +
	    aT_u[index_u] + aB_u[index_u];
	  /*
	    aE_u[index_u] + aW_u[index_u] +
	    aN_u[index_u] + aS_u[index_u] +
	    aT_u[index_u] + aB_u[index_u] +
	    (Fe - Fw) + (Fn - Fs) + (Ft - Fb);
	  */

	  // e
	  Seap =
	    uW * (de/dW) * (de - dE)/(dW + dE) +
	    uP * ( (dE - de)/dE * (1 + de/dW) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uE * (de/dE) * (1 + (de - dE)/(dW + dE));
	
	  Seam =
	    uP  * (1 - de/dEE) * (dE - de)/dE +
	    uE  * ( (1 + (dE - de)/(dEE - dE)) * (de/dE) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uEE * (de - dE)/(dEE - dE) * (de/dEE);
	  
	  Se = Fe * (Fe_positive_OK * Seap + (1 - Fe_positive_OK) * Seam);

	  // w
	  Swam =
	    uE * (dw/dE) * (dw - dW)/(dW + dE) +
	    uP * ( (dW - dw)/dW * (1 + dw/dE) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uW * (dw/dW) * (1 + (dw - dW)/(dW + dE));
	
	  Swap =
	    uP  * (1 - dw/dWW) * (dW - dw)/dW +
	    uW  * ( (1 + (dW - dw)/(dWW - dW)) * (dw/dW) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uWW * (dw - dW)/(dWW - dW) * (dw/dWW);
	  
	  Sw = Fw * (Fw_positive_OK * Swap + (1 - Fw_positive_OK) * Swam);

	  // n
	  Snap =
	    uS * (dn/dS) * (dn - dN)/(dS + dN) +
	    uP * ( (dN - dn)/dN * (1 + dn/dS) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uN * (dn/dN) * (1 + (dn - dN)/(dS + dN));
	
	  Snam =
	    uP  * (1 - dn/dNN) * (dN - dn)/dN +
	    uN  * ( (1 + (dN - dn)/(dNN - dN)) * (dn/dN) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uNN * (dn - dN)/(dNN - dN) * (dn/dNN);
	  
	  Sn = Fn * (Fn_positive_OK * Snap + (1 - Fn_positive_OK) * Snam);

	  // s
	  Ssam =
	    uN * (ds/dN) * (ds - dS)/(dS + dN) +
	    uP * ( (dS - ds)/dS * (1 + ds/dN) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uS * (ds/dS) * (1 + (ds - dS)/(dS + dN));
	
	  Ssap =
	    uP  * (1 - ds/dSS) * (dS - ds)/dS +
	    uS  * ( (1 + (dS - ds)/(dSS - dS)) * (ds/dS) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uSS * (ds - dS)/(dSS - dS) * (ds/dSS);
	  
	  Ss = Fs * (Fs_positive_OK * Ssap + (1 - Fs_positive_OK) * Ssam);

	  // t
	  Stap =
	    uB * (dt/dB) * (dt - dT)/(dB + dT) +
	    uP * ( (dT - dt)/dT * (1 + dt/dB) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uT * (dt/dT) * (1 + (dt - dT)/(dB + dT));
	
	  Stam =
	    uP  * (1 - dt/dTT) * (dT - dt)/dT +
	    uT  * ( (1 + (dT - dt)/(dTT - dT)) * (dt/dT) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uTT * (dt - dT)/(dTT - dT) * (dt/dTT);
	  
	  St = Ft * (Ft_positive_OK * Stap + (1 - Ft_positive_OK) * Stam);

	  // b
	  Sbam =
	    uT * (db/dT) * (db - dB)/(dB + dT) +
	    uP * ( (dB - db)/dB * (1 + db/dT) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uB * (db/dB) * (1 + (db - dB)/(dB + dT));
	
	  Sbap =
	    uP  * (1 - db/dBB) * (dB - db)/dB +
	    uB  * ( (1 + (dB - db)/(dBB - dB)) * (db/dB) - 1 ) + // here modification needed (substract 1).. DONE!!
	    uBB * (db - dB)/(dBB - dB) * (db/dBB);
	  
	  Sb = Fb * (Fb_positive_OK * Sbap + (1 - Fb_positive_OK) * Sbam);

	  b_u[index_u] += Sw - Se + Ss - Sn + Sb - St;
#endif
	}
	else{
	  aP_u[index_u] = 1;
	  aW_u[index_u] = 0;
	  aE_u[index_u] = 0;
	  aS_u[index_u] = 0;
	  aN_u[index_u] = 0;
	  aB_u[index_u] = 0;
	  aT_u[index_u] = 0;
	  b_u[index_u] = 0;
	}
      }
    }
  }
  
  /* Diffusive factor */
  for(z=0; z<diff_u_face_x_count; z++){
    
    index_ = diff_u_face_x_index[z];
    index_u = diff_u_face_x_index_u[z];
    
    I = diff_u_face_x_I[z];
    
    mu_eff = mu + 0.5 * (mu_turb_at_u_nodes[index_u] + mu_turb_at_u_nodes[index_u + 1]);
    S_diff = mu_eff * diff_factor_u_face_x[z] *
      (u[index_u] * u_cut_fw[index_] + u[index_u + 1] * u_cut_fe[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((u_dom_matrix[index_u] == 0) && (I > 1)){
      b_u[index_u] -= S_diff;
    }
    if((u_dom_matrix[index_u + 1] == 0) && (I < Nx-2)){
      b_u[index_u + 1] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<u_sol_factor_count; z++){
      
      index_u = u_sol_factor_index_u[z];
      
      if(u_dom_matrix[index_u] == 0){

	index_ = u_sol_factor_index[z];
	
	mu_eff = mu + mu_turb_at_u_nodes[index_u];
	
	aP_u[index_u] += mu_eff * u_sol_factor[z];
	b_u[index_u] += u_p_factor[z] *
	  (u_p_factor_W[z] * p[index_-1] + (1 - u_p_factor_W[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 1){

#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(u_dom_matrix, b_u, vol_x)
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	  
	  index_u = K*nx*Ny + J*nx + i;
	  
	  if(u_dom_matrix[index_u] == 0){
	    b_u[index_u] += periodic_source * vol_x[index_u];
	  }
	  
	}
      }
    }
  }
  
  /* Transient contribution */

  if(!flow_steady_state_OK){
    
#pragma omp parallel for default(none) private(i, J, K, index_u, aP0)	\
  shared(u_dom_matrix, vol_x, aP_u, b_u, u0)
    for(K=1; K<Nz-1; K++){  
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){

	  index_u = K*nx*Ny + J*nx + i;
	
	  if(u_dom_matrix[index_u] == 0){
	  
	    aP0 = rho * vol_x[index_u] / dtime;
	    aP_u[index_u] += aP0;
	    b_u[index_u] += aP0 * u0[index_u];
	  
	  }
	
	}
      }
    }
  }
  
  /* Add custom source term */

  if(add_momentum_source_OK){
    add_u_momentum_source_3D(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(u, u_dom_matrix, 0, data->coeffs_u, nx, Ny, Nz, data->alpha_u);
  }

  /* Set coefficients for slave cells */
  for(i=0; i<u_slave_count; i++){
    index_u = u_slave_nodes[i];
    aP_u[index_u] = 1;
    aW_u[index_u] = 0;
    aE_u[index_u] = 0;
    aS_u[index_u] = 0;
    aN_u[index_u] = 0;
    aB_u[index_u] = 0;
    aT_u[index_u] = 0;
    b_u[index_u] = 0;
  }
  
  return;
}


/* COEFFS_FLOW_v_QUICK_3D */
/**
  
   Sets:

   Structure coeffs_v: coefficients of v momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 02-10-2020
*/
void coeffs_flow_v_quick_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_v;
  int index_face_x, index_face_z;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;
  int Fb_positive_OK, Ft_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  
  const int p_source_orientation = data->p_source_orientation;
  const int v_slave_count = data->v_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_v_face_y_count = data->diff_v_face_y_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *v_dom_matrix = data->v_dom_matrix;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *diff_v_face_y_index = data->diff_v_face_y_index;
  const int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
  const int *diff_v_face_y_J = data->diff_v_face_y_J;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double vP, vE, vW, vN, vS, vT, vB;
  double vWW, vEE, vSS, vNN, vBB, vTT;
  double aP0, S_diff;
  double dEE, dWW, dE, dW, de, dw;
  double dNN, dSS, dN, dS, dn, ds;
  double dTT, dBB, dT, dB, dt, db;
  double Se, Sw, Sn, Ss, St, Sb;
  double Seap, Seam, Swap, Swam;
  double Snap, Snam, Ssap, Ssam;
  double Stap, Stam, Sbap, Sbam;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dtime = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_v = data->coeffs_v.aP;
  double *aW_v = data->coeffs_v.aW;
  double *aE_v = data->coeffs_v.aE;
  double *aS_v = data->coeffs_v.aS;
  double *aN_v = data->coeffs_v.aN;
  double *aB_v = data->coeffs_v.aB;
  double *aT_v = data->coeffs_v.aT;
  double *b_v = data->coeffs_v.b;

  const double *v = data->v;
  const double *p = data->p;

  const double *v_faces_y = data->v_faces_y;
  const double *diff_factor_v_face_y = data->diff_factor_v_face_y;
  
  const double *Dv_x = data->Dv_x;
  const double *Dv_y = data->Dv_y;
  const double *Dv_z = data->Dv_z;
  const double *Fv_x = data->Fv_x;
  const double *Fv_y = data->Fv_y;
  const double *Fv_z = data->Fv_z;
  const double *v_sol_factor = data->v_sol_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *mu_turb_at_v_nodes = data->mu_turb_at_v_nodes;
  const double *v_cut_fn = data->v_cut_fn;
  const double *v_cut_fs = data->v_cut_fs;
  const double *vol_y = data->vol_y;
  const double *alpha_v_x = data->alpha_v_x;
  const double *alpha_v_y = data->alpha_v_y;
  const double *alpha_v_z = data->alpha_v_z;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *v0 = data->v0;

#ifdef OLDINT_OK
  const double *cvdE = data->Delta_xE_v; /**< Distance from node to East neighbour, u nodes. */
  const double *cvdW = data->Delta_xW_v; /**< Distance from node to West neighbour, u nodes. */
  const double *cvdN = data->Delta_yN_v; /**< Distance from node to East neighbour, u nodes. */
  const double *cvdS = data->Delta_yS_v; /**< Distance from node to West neighbour, u nodes. */
  const double *cvdT = data->Delta_zT_v; /**< Distance from node to East neighbour, u nodes. */
  const double *cvdB = data->Delta_zB_v; /**< Distance from node to West neighbour, u nodes. */
#else
  const double *cvdE = data->cvdE;
  const double *cvdW = data->cvdW;
  const double *cvdN = data->cvdN;
  const double *cvdS = data->cvdS;
  const double *cvdT = data->cvdT;
  const double *cvdB = data->cvdB;
#endif

  const double *Dx = data->Delta_x;
  const double *Dy = data->Delta_y;
  const double *Dz = data->Delta_z;
  
  set_D_v_3D(data);
  set_F_v_3D(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_v,				\
	  index_face_x, index_face_z,					\
	  De, Dw, Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb,		\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
  	  Fb_positive_OK, Ft_positive_OK,				\
	  dEE, dWW, dE, dW, de, dw,					\
	  dNN, dSS, dN, dS, dn, ds,					\
	  dTT, dBB, dT, dB, dt, db,					\
	  Se, Sw, Sn, Ss, St, Sb,					\
	  Seap, Seam, Swap, Swam,					\
	  Snap, Snam, Ssap, Ssam,					\
	  Stap, Stam, Sbap, Sbam,					\
	  vP, vE, vW, vN, vS, vT, vB,					\
	  vWW, vEE, vSS, vNN, vBB, vTT)					\
  shared(v_dom_matrix,							\
	 aE_v, aW_v, aS_v, aN_v, aT_v, aB_v, aP_v, b_v,			\
	 v, p, v_faces_y,						\
	 Dv_x, Dv_y, Dv_z, Fv_x, Fv_y, Fv_z, mu_turb_at_v_nodes,	\
	 Dx, Dy, Dz, cvdE, cvdW, cvdN, cvdS, cvdT, cvdB, 		\
	 alpha_v_x, alpha_v_y, alpha_v_z)
  for(K=1; K<Nz-1; K++){  
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	
	index_v = K*Nx*ny + j*Nx + I;
	
	if(v_dom_matrix[index_v] == 0){
	  
	  i = I-1;
	  J = j+1;
	  k = K-1;
      
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_face_x = K*nx*ny + j*nx + i; // Dv,Fv_x
	  index_face_z = k*Nx*ny + j*Nx + I; // Dv,Fv_z

	  /* Distances */
	  dE = cvdE[index_v];
	  dW = cvdW[index_v];
	  dN = cvdN[index_v];
	  dS = cvdS[index_v];
	  dT = cvdT[index_v];
	  dB = cvdB[index_v];

	  dEE = dE + cvdE[index_v + 1];
	  dWW = dW + cvdW[index_v - 1];
	  dNN = dN + cvdN[index_v + Nx];
	  dSS = dS + cvdS[index_v - Nx];
	  dTT = dT + cvdT[index_v + Nx*ny];
	  dBB = dB + cvdB[index_v - Nx*ny];

	  de = 0.5 * Dx[index_];
	  dw = de;
	  dn = 0.5 * Dy[index_];
	  ds = 0.5 * Dy[index_-Nx];
	  dt = 0.5 * Dz[index_];
	  db = dt;

	  /* Velocities */
	  vP = v[index_v];
	  vE = v[index_v + 1];
	  vW = v[index_v - 1];
	  vN = v[index_v + Nx];
	  vS = v[index_v - Nx];
	  vT = v[index_v + Nx*ny];
	  vB = v[index_v - Nx*ny];
	
	  /* Diffusive */
	  De = Dv_x[index_face_x + 1];
	  Dw = Dv_x[index_face_x];
	  Dn = Dv_y[index_];
	  Ds = Dv_y[index_ - Nx];
	  Dt = Dv_z[index_face_z + Nx*ny];
	  Db = Dv_z[index_face_z];

	  /* Convective */
	  Fe = Fv_x[index_face_x + 1] * alpha_v_x[index_face_x + 1];
	  Fw = Fv_x[index_face_x] * alpha_v_x[index_face_x];
	  Fn = Fv_y[index_] * alpha_v_y[index_] * alpha_v_y[index_];
	  Fs = Fv_y[index_-Nx] * alpha_v_y[index_-Nx] * alpha_v_y[index_-Nx];
	  Ft = Fv_z[index_face_z + Nx*ny] * alpha_v_z[index_face_z + Nx*ny];
	  Fb = Fv_z[index_face_z] * alpha_v_z[index_face_z];

	  Fe_positive_OK = (Fe > 0);
	  Fw_positive_OK = (Fw > 0);
	  Fn_positive_OK = (Fn > 0);
	  Fs_positive_OK = (Fs > 0);
	  Ft_positive_OK = (Ft > 0);
	  Fb_positive_OK = (Fb > 0);

	  if(I == 1){
	    vWW = 2 * vW - vP;
	    dWW = 2*dW;
	  }
	  else if(v_dom_matrix[index_v - 2] != 0){
	    vWW = 2 * vW - vP;
	    dWW = 2*dW;
	  }
	  else{
	    vWW = v[index_v-2];
	  }

	  if(I == Nx-2){
	    vEE = 2 * vE - vP;
	    dEE = 2*dE;
	  }
	  else if(v_dom_matrix[index_v + 2] != 0){
	    vEE = 2 * vE - vP;
	    dEE = 2*dE;
	  }
	  else{
	    vEE = v[index_v+2];
	  }
	
	  if(j == 1){
	    vSS = 2 * vS - vP;
	    dSS = 2*dS;
	  }
	  else if(v_dom_matrix[index_v - 2*Nx] != 0){
	    vSS = 2 * vS - vP;
	    dSS = 2*dS;
	  }
	  else{
	    vSS = v[index_v-2*Nx];
	  }

	  if(j == ny-2){
	    vNN = 2 * vN - vP;
	    dNN = 2*dN;
	  }
	  else if(v_dom_matrix[index_v + 2*Nx] != 0){
	    vNN = 2 * vN - vP;
	    dNN = 2*dN;
	  }
	  else{
	    vNN = v[index_v+2*Nx];
	  }

	  if(K == 1){
	    vBB = 2 * vB - vP;
	    dBB = 2*dB;
	  }
	  else if(v_dom_matrix[index_v - 2*Nx*ny] != 0){
	    vBB = 2 * vB - vP;
	    dBB = 2*dB;
	  }
	  else{
	    vBB = v[index_v - 2*Nx*ny];
	  }

	  if(K == Nz-2){
	    vTT = 2 * vT - vP;
	    dTT = 2*dT;
	  }
	  else if(v_dom_matrix[index_v + 2*Nx*ny] != 0){
	    vTT = 2 * vT - vP;
	    dTT = 2*dT;
	  }
	  else{
	    vTT = v[index_v + 2*Nx*ny];
	  }
	  
	  dpA = v_faces_y[index_] * p[index_] - 
	    v_faces_y[index_-Nx] * p[index_-Nx];

	  b_v[index_v] = -dpA;

#if(QUICK_SCHEME == 0) /* Hayase regular grid */
	
	  exit(EXIT_FAILURE);
	
#elif(QUICK_SCHEME == 2) /* Kirkpatrick-Hayase */
	
	  aE_v[index_v] = De + MAX(-Fe, 0);
	  aW_v[index_v] = Dw + MAX(+Fw, 0);
	  aN_v[index_v] = Dn + MAX(-Fn, 0);
	  aS_v[index_v] = Ds + MAX(+Fs, 0);
	  aT_v[index_v] = Dt + MAX(-Ft, 0);
	  aB_v[index_v] = Db + MAX(+Fb, 0);

	  aP_v[index_v] =
	    aE_v[index_v] + aW_v[index_v] +
	    aN_v[index_v] + aS_v[index_v] +
	    aT_v[index_v] + aB_v[index_v];
	  /*
	    aE_v[index_v] + aW_v[index_v] +
	    aN_v[index_v] + aS_v[index_v] +
	    aT_v[index_v] + aB_v[index_v] +
	    (Fe - Fw) + (Fn - Fs) + (Ft - Fb);
	  */

	  // e
	  Seap =
	    vW * (de/dW) * (de - dE)/(dW + dE) +
	    vP * ( (dE - de)/dE * (1 + de/dW) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vE * (de/dE) * (1 + (de - dE)/(dW + dE));
	
	  Seam =
	    vP  * (1 - de/dEE) * (dE - de)/dE +
	    vE  * ( (1 + (dE - de)/(dEE - dE)) * (de/dE) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vEE * (de - dE)/(dEE - dE) * (de/dEE);
	  
	  Se = Fe * (Fe_positive_OK * Seap + (1 - Fe_positive_OK) * Seam);

	  // w
	  Swam =
	    vE * (dw/dE) * (dw - dW)/(dW + dE) +
	    vP * ( (dW - dw)/dW * (1 + dw/dE) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vW * (dw/dW) * (1 + (dw - dW)/(dW + dE));
	
	  Swap =
	    vP  * (1 - dw/dWW) * (dW - dw)/dW +
	    vW  * ( (1 + (dW - dw)/(dWW - dW)) * (dw/dW) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vWW * (dw - dW)/(dWW - dW) * (dw/dWW);
	  
	  Sw = Fw * (Fw_positive_OK * Swap + (1 - Fw_positive_OK) * Swam);

	  // n
	  Snap =
	    vS * (dn/dS) * (dn - dN)/(dS + dN) +
	    vP * ( (dN - dn)/dN * (1 + dn/dS) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vN * (dn/dN) * (1 + (dn - dN)/(dS + dN));
	
	  Snam =
	    vP  * (1 - dn/dNN) * (dN - dn)/dN +
	    vN  * ( (1 + (dN - dn)/(dNN - dN)) * (dn/dN) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vNN * (dn - dN)/(dNN - dN) * (dn/dNN);
	  
	  Sn = Fn * (Fn_positive_OK * Snap + (1 - Fn_positive_OK) * Snam);

	  // s
	  Ssam =
	    vN * (ds/dN) * (ds - dS)/(dS + dN) +
	    vP * ( (dS - ds)/dS * (1 + ds/dN) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vS * (ds/dS) * (1 + (ds - dS)/(dS + dN));
	
	  Ssap =
	    vP  * (1 - ds/dSS) * (dS - ds)/dS +
	    vS  * ( (1 + (dS - ds)/(dSS - dS)) * (ds/dS) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vSS * (ds - dS)/(dSS - dS) * (ds/dSS);
	  
	  Ss = Fs * (Fs_positive_OK * Ssap + (1 - Fs_positive_OK) * Ssam);

	  // t
	  Stap =
	    vB * (dt/dB) * (dt - dT)/(dB + dT) +
	    vP * ( (dT - dt)/dT * (1 + dt/dB) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vT * (dt/dT) * (1 + (dt - dT)/(dB + dT));
	
	  Stam =
	    vP  * (1 - dt/dTT) * (dT - dt)/dT +
	    vT  * ( (1 + (dT - dt)/(dTT - dT)) * (dt/dT) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vTT * (dt - dT)/(dTT - dT) * (dt/dTT);
	  
	  St = Ft * (Ft_positive_OK * Stap + (1 - Ft_positive_OK) * Stam);

	  // b
	  Sbam =
	    vT * (db/dT) * (db - dB)/(dB + dT) +
	    vP * ( (dB - db)/dB * (1 + db/dT) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vB * (db/dB) * (1 + (db - dB)/(dB + dT));
	
	  Sbap =
	    vP  * (1 - db/dBB) * (dB - db)/dB +
	    vB  * ( (1 + (dB - db)/(dBB - dB)) * (db/dB) - 1 ) + // here modification needed (svbstract 1).. DONE!!
	    vBB * (db - dB)/(dBB - dB) * (db/dBB);
	  
	  Sb = Fb * (Fb_positive_OK * Sbap + (1 - Fb_positive_OK) * Sbam);

	  b_v[index_v] += Sw - Se + Ss - Sn + Sb - St;
#endif
	}
	else{
	  aP_v[index_v] = 1;
	  aW_v[index_v] = 0;
	  aE_v[index_v] = 0;
	  aS_v[index_v] = 0;
	  aN_v[index_v] = 0;
	  aB_v[index_v] = 0;
	  aT_v[index_v] = 0;
	  b_v[index_v] = 0;
	}
      }
    }
  }

  /* Diffusive factor */
  for(z=0; z<diff_v_face_y_count; z++){
    
    index_ = diff_v_face_y_index[z];
    index_v = diff_v_face_y_index_v[z];

    J = diff_v_face_y_J[z];
    
    mu_eff = mu + 0.5 * (mu_turb_at_v_nodes[index_v] + mu_turb_at_v_nodes[index_v + Nx]);
    S_diff = mu_eff * diff_factor_v_face_y[z] *
      (v[index_v] * v_cut_fs[index_] + v[index_v + Nx] * v_cut_fn[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((v_dom_matrix[index_v] == 0) && (J > 1)){
      b_v[index_v] -= S_diff;
    }
    if((v_dom_matrix[index_v + Nx] == 0) && (J < Ny-2)){
      b_v[index_v + Nx] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<v_sol_factor_count; z++){
      
      index_v = v_sol_factor_index_v[z];
      
      if(v_dom_matrix[index_v] == 0){
	
	index_ = v_sol_factor_index[z];
	
	mu_eff = mu + mu_turb_at_v_nodes[index_v];
	
	aP_v[index_v] += mu_eff * v_sol_factor[z];
	b_v[index_v] += v_p_factor[z] *
	  (v_p_factor_S[z] * p[index_-Nx] + (1 - v_p_factor_S[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(v_dom_matrix, b_v, vol_y)
    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	
	  index_v = K*Nx*ny + j*Nx + I;
	
	  if(v_dom_matrix[index_v] == 0){
	    b_v[index_v] += periodic_source * vol_y[index_v];
	  }
	
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, j, K, index_v, aP0)	\
  shared(v_dom_matrix, vol_y, aP_v, b_v, v0)
    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	
	  index_v = K*Nx*ny + j*Nx + I;
	
	  if(v_dom_matrix[index_v] == 0){
	  
	    aP0 = rho * vol_y[index_v] / dtime;
	    aP_v[index_v] += aP0;
	    b_v[index_v] += aP0 * v0[index_v];
	  
	  }
	}
      }
    }
  }

  /* Add custom source term */

  if(add_momentum_source_OK){
    add_v_momentum_source_3D(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(v, v_dom_matrix, 0, data->coeffs_v, Nx, ny, Nz, data->alpha_v);
  }

  for(i=0; i<v_slave_count; i++){
    index_v = v_slave_nodes[i];
    aP_v[index_v] = 1;
    aW_v[index_v] = 0;
    aE_v[index_v] = 0;
    aS_v[index_v] = 0;
    aN_v[index_v] = 0;
    aB_v[index_v] = 0;
    aT_v[index_v] = 0;
    b_v[index_v] = 0;
  }

  return;
}

/* COEFFS_FLOW_w_QUICK_3D */
/**
  
   Sets:

   Structure coeffs_v: coefficients of v momentum equations for internal nodes.

   @param data
   @return ret_value not implemented.
  
   MODIFIED: 02-10-2020
*/
void coeffs_flow_w_quick_3D(Data_Mem *data){

  int I, J, K, i, j, k, z;
  int index_, index_w;
  int index_face_x, index_face_y;
  int Fw_positive_OK, Fe_positive_OK;
  int Fs_positive_OK, Fn_positive_OK;
  int Fb_positive_OK, Ft_positive_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  
  const int p_source_orientation = data->p_source_orientation;
  const int w_slave_count = data->w_slave_count;
  const int consider_wall_shear_OK = data->consider_wall_shear_OK;
  const int w_sol_factor_count = data->w_sol_factor_count;
  const int curves = data->curves;
  const int pv_coupling_algorithm = data->pv_coupling_algorithm;
  const int flow_steady_state_OK = data->flow_steady_state_OK;
  const int diff_w_face_z_count = data->diff_w_face_z_count;
  const int add_momentum_source_OK = data->add_momentum_source_OK;

  const int *w_dom_matrix = data->w_dom_matrix;
  const int *w_slave_nodes = data->w_slave_nodes;
  const int *w_sol_factor_index_w = data->w_sol_factor_index_w;
  const int *w_sol_factor_index = data->w_sol_factor_index;
  const int *diff_w_face_z_index = data->diff_w_face_z_index;
  const int *diff_w_face_z_index_w = data->diff_w_face_z_index_w;
  const int *diff_w_face_z_K = data->diff_w_face_z_K;

  double dpA, mu_eff;
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double wP, wE, wW, wN, wS, wT, wB;
  double wWW, wEE, wSS, wNN, wBB, wTT;
  double aP0, S_diff;
  double dEE, dWW, dE, dW, de, dw;
  double dNN, dSS, dN, dS, dn, ds;
  double dTT, dBB, dT, dB, dt, db;
  double Se, Sw, Sn, Ss, St, Sb;
  double Seap, Seam, Swap, Swam;
  double Snap, Snam, Ssap, Ssam;
  double Stap, Stam, Sbap, Sbam;

  /* Propiedades fisicas */
  const double mu = data->mu;
  const double rho = data->rho_v[0];
  const double dtime = data->dt[3] / 3; // For 3D case
  const double periodic_source = data->periodic_source;
 
  double *aP_w = data->coeffs_w.aP;
  double *aW_w = data->coeffs_w.aW;
  double *aE_w = data->coeffs_w.aE;
  double *aS_w = data->coeffs_w.aS;
  double *aN_w = data->coeffs_w.aN;
  double *aB_w = data->coeffs_w.aB;
  double *aT_w = data->coeffs_w.aT;
  double *b_w = data->coeffs_w.b;

  const double *w = data->w;
  const double *p = data->p;

  const double *w_faces_z = data->w_faces_z;
  const double *diff_factor_w_face_z = data->diff_factor_w_face_z;
  
  const double *Dw_x = data->Dw_x;
  const double *Dw_y = data->Dw_y;
  const double *Dw_z = data->Dw_z;
  const double *Fw_x = data->Fw_x;
  const double *Fw_y = data->Fw_y;
  const double *Fw_z = data->Fw_z;
  const double *w_sol_factor = data->w_sol_factor;
  const double *w_p_factor = data->w_p_factor;
  const double *mu_turb_at_w_nodes = data->mu_turb_at_w_nodes;
  const double *w_cut_ft = data->w_cut_ft;
  const double *w_cut_fb = data->w_cut_fb;
  const double *vol_z = data->vol_z;
  const double *alpha_w_x = data->alpha_w_x;
  const double *alpha_w_y = data->alpha_w_y;
  const double *alpha_w_z = data->alpha_w_z;
  const double *w_p_factor_B = data->w_p_factor_B;
  const double *w0 = data->w0;

#ifdef OLDINT_OK
  const double *cwdE = data->Delta_xE_w; /**< Distance from node to East neighbour, u nodes. */
  const double *cwdW = data->Delta_xW_w; /**< Distance from node to West neighbour, u nodes. */
  const double *cwdN = data->Delta_yN_w; /**< Distance from node to East neighbour, u nodes. */
  const double *cwdS = data->Delta_yS_w; /**< Distance from node to West neighbour, u nodes. */
  const double *cwdT = data->Delta_zT_w; /**< Distance from node to East neighbour, u nodes. */
  const double *cwdB = data->Delta_zB_w; /**< Distance from node to West neighbour, u nodes. */
#else
  const double *cwdE = data->cwdE;
  const double *cwdW = data->cwdW;
  const double *cwdN = data->cwdN;
  const double *cwdS = data->cwdS;
  const double *cwdT = data->cwdT;
  const double *cwdB = data->cwdB;
#endif

  const double *Dx = data->Delta_x;
  const double *Dy = data->Delta_y;
  const double *Dz = data->Delta_z;
  
  set_D_w(data);
  set_F_w(data);
  
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_w,				\
	  index_face_x, index_face_y,					\
	  De, Dw, Dn, Ds, Dt, Db, Fe, Fw, Fn, Fs, Ft, Fb,		\
	  dpA,								\
	  Fw_positive_OK, Fe_positive_OK,				\
	  Fs_positive_OK, Fn_positive_OK,				\
  	  Fb_positive_OK, Ft_positive_OK,				\
	  dEE, dWW, dE, dW, de, dw,					\
	  dNN, dSS, dN, dS, dn, ds,					\
	  dTT, dBB, dT, dB, dt, db,					\
	  Se, Sw, Sn, Ss, St, Sb,					\
	  Seap, Seam, Swap, Swam,					\
	  Snap, Snam, Ssap, Ssam,					\
	  Stap, Stam, Sbap, Sbam,					\
	  wP, wE, wW, wN, wS, wT, wB,					\
	  wWW, wEE, wSS, wNN, wBB, wTT)					\
  shared(w_dom_matrix,							\
	 aE_w, aW_w, aS_w, aN_w, aT_w, aB_w, aP_w, b_w,			\
	 w, p, w_faces_z,						\
	 Dw_x, Dw_y, Dw_z, Fw_x, Fw_y, Fw_z, mu_turb_at_w_nodes,	\
	 Dx, Dy, Dz, cwdE, cwdW, cwdN, cwdS, cwdT, cwdB, 		\
	 alpha_w_x, alpha_w_y, alpha_w_z)
  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_w = k*Nx*Ny + J*Nx + I;
	
	if(w_dom_matrix[index_w] == 0){
	  
	  i = I-1;
	  j = J-1;
	  K = k+1;
      
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_face_x = k*nx*Ny + J*nx + i; // Dw,Fw_x
	  index_face_y = k*Nx*ny + j*Nx + I; // Dw,Fw_y

	  /* Distances */
	  dE = cwdE[index_w];
	  dW = cwdW[index_w];
	  dN = cwdN[index_w];
	  dS = cwdS[index_w];
	  dT = cwdT[index_w];
	  dB = cwdB[index_w];

	  dEE = dE + cwdE[index_w + 1];
	  dWW = dW + cwdW[index_w - 1];
	  dNN = dN + cwdN[index_w + Nx];
	  dSS = dS + cwdS[index_w - Nx];
	  dTT = dT + cwdT[index_w + Nx*Ny];
	  dBB = dB + cwdB[index_w - Nx*Ny];

	  de = 0.5 * Dx[index_];
	  dw = de;
	  dn = 0.5 * Dy[index_];
	  ds = dn;
	  dt = 0.5 * Dz[index_];
	  db = 0.5 * Dz[index_-Nx*Nz];

	  /* Velocities */
	  wP = w[index_w];
	  wE = w[index_w + 1];
	  wW = w[index_w - 1];
	  wN = w[index_w + Nx];
	  wS = w[index_w - Nx];
	  wT = w[index_w + Nx*Ny];
	  wB = w[index_w - Nx*Ny];
	
	  /* Diffusive */
	  De = Dw_x[index_face_x + 1];
	  Dw = Dw_x[index_face_x];
	  Dn = Dw_y[index_face_y + Nx];
	  Ds = Dw_y[index_face_y];
	  Dt = Dw_z[index_];
	  Db = Dw_z[index_ - Nx*Ny];

	  /* Convective */
	  Fe = Fw_x[index_face_x + 1] * alpha_w_x[index_face_x + 1];
	  Fw = Fw_x[index_face_x] * alpha_w_x[index_face_x];
	  Fn = Fw_y[index_face_y + Nx*ny] * alpha_w_y[index_face_y + Nx*ny];
	  Fs = Fw_y[index_face_y] * alpha_w_y[index_face_y];
	  Ft = Fw_z[index_] * alpha_w_z[index_] * alpha_w_z[index_];
	  Fb = Fw_z[index_-Nx*Ny] * alpha_w_z[index_-Nx*Ny] * alpha_w_z[index_-Nx*Ny];

	  Fe_positive_OK = (Fe > 0);
	  Fw_positive_OK = (Fw > 0);
	  Fn_positive_OK = (Fn > 0);
	  Fs_positive_OK = (Fs > 0);
	  Ft_positive_OK = (Ft > 0);
	  Fb_positive_OK = (Fb > 0);

	  if(I == 1){
	    wWW = 2 * wW - wP;
	    dWW = 2*dW;
	  }
	  else if(w_dom_matrix[index_w - 2] != 0){
	    wWW = 2 * wW - wP;
	    dWW = 2*dW;
	  }
	  else{
	    wWW = w[index_w-2];
	  }

	  if(I == Nx-2){
	    wEE = 2 * wE - wP;
	    dEE = 2*dE;
	  }
	  else if(w_dom_matrix[index_w + 2] != 0){
	    wEE = 2 * wE - wP;
	    dEE = 2*dE;
	  }
	  else{
	    wEE = w[index_w+2];
	  }
	
	  if(J == 1){
	    wSS = 2 * wS - wP;
	    dSS = 2*dS;
	  }
	  else if(w_dom_matrix[index_w - 2*Nx] != 0){
	    wSS = 2 * wS - wP;
	    dSS = 2*dS;
	  }
	  else{
	    wSS = w[index_w-2*Nx];
	  }

	  if(J == Ny-2){
	    wNN = 2 * wN - wP;
	    dNN = 2*dN;
	  }
	  else if(w_dom_matrix[index_w + 2*Nx] != 0){
	    wNN = 2 * wN - wP;
	    dNN = 2*dN;
	  }
	  else{
	    wNN = w[index_w+2*Nx];
	  }

	  if(k == 1){
	    wBB = 2 * wB - wP;
	    dBB = 2*dB;
	  }
	  else if(w_dom_matrix[index_w - 2*Nx*Ny] != 0){
	    wBB = 2 * wB - wP;
	    dBB = 2*dB;
	  }
	  else{
	    wBB = w[index_w - 2*Nx*Ny];
	  }

	  if(k == nz-2){
	    wTT = 2 * wT - wP;
	    dTT = 2*dT;
	  }
	  else if(w_dom_matrix[index_w + 2*Nx*Ny] != 0){
	    wTT = 2 * wT - wP;
	    dTT = 2*dT;
	  }
	  else{
	    wTT = w[index_w + 2*Nx*Ny];
	  }
	  
	  dpA = w_faces_z[index_] * p[index_] - 
	    w_faces_z[index_-Nx*Ny] * p[index_-Nx*Ny];

	  b_w[index_w] = -dpA;

#if(QUICK_SCHEME == 0) /* Hayase regular grid */
	
	  exit(EXIT_FAILURE);
	
#elif(QUICK_SCHEME == 2) /* Kirkpatrick-Hayase */
	
	  aE_w[index_w] = De + MAX(-Fe, 0);
	  aW_w[index_w] = Dw + MAX(+Fw, 0);
	  aN_w[index_w] = Dn + MAX(-Fn, 0);
	  aS_w[index_w] = Ds + MAX(+Fs, 0);
	  aT_w[index_w] = Dt + MAX(-Ft, 0);
	  aB_w[index_w] = Db + MAX(+Fb, 0);

	  aP_w[index_w] =
	    aE_w[index_w] + aW_w[index_w] +
	    aN_w[index_w] + aS_w[index_w] +
	    aT_w[index_w] + aB_w[index_w];
	  /*
	    aE_w[index_w] + aW_w[index_w] +
	    aN_w[index_w] + aS_w[index_w] +
	    aT_w[index_w] + aB_w[index_w] +
	    (Fe - Fw) + (Fn - Fs) + (Ft - Fb);
	  */

	  // e
	  Seap =
	    wW * (de/dW) * (de - dE)/(dW + dE) +
	    wP * ( (dE - de)/dE * (1 + de/dW) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wE * (de/dE) * (1 + (de - dE)/(dW + dE));
	
	  Seam =
	    wP  * (1 - de/dEE) * (dE - de)/dE +
	    wE  * ( (1 + (dE - de)/(dEE - dE)) * (de/dE) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wEE * (de - dE)/(dEE - dE) * (de/dEE);
	  
	  Se = Fe * (Fe_positive_OK * Seap + (1 - Fe_positive_OK) * Seam);

	  // w
	  Swam =
	    wE * (dw/dE) * (dw - dW)/(dW + dE) +
	    wP * ( (dW - dw)/dW * (1 + dw/dE) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wW * (dw/dW) * (1 + (dw - dW)/(dW + dE));
	
	  Swap =
	    wP  * (1 - dw/dWW) * (dW - dw)/dW +
	    wW  * ( (1 + (dW - dw)/(dWW - dW)) * (dw/dW) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wWW * (dw - dW)/(dWW - dW) * (dw/dWW);
	  
	  Sw = Fw * (Fw_positive_OK * Swap + (1 - Fw_positive_OK) * Swam);

	  // n
	  Snap =
	    wS * (dn/dS) * (dn - dN)/(dS + dN) +
	    wP * ( (dN - dn)/dN * (1 + dn/dS) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wN * (dn/dN) * (1 + (dn - dN)/(dS + dN));
	
	  Snam =
	    wP  * (1 - dn/dNN) * (dN - dn)/dN +
	    wN  * ( (1 + (dN - dn)/(dNN - dN)) * (dn/dN) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wNN * (dn - dN)/(dNN - dN) * (dn/dNN);
	  
	  Sn = Fn * (Fn_positive_OK * Snap + (1 - Fn_positive_OK) * Snam);

	  // s
	  Ssam =
	    wN * (ds/dN) * (ds - dS)/(dS + dN) +
	    wP * ( (dS - ds)/dS * (1 + ds/dN) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wS * (ds/dS) * (1 + (ds - dS)/(dS + dN));
	
	  Ssap =
	    wP  * (1 - ds/dSS) * (dS - ds)/dS +
	    wS  * ( (1 + (dS - ds)/(dSS - dS)) * (ds/dS) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wSS * (ds - dS)/(dSS - dS) * (ds/dSS);
	  
	  Ss = Fs * (Fs_positive_OK * Ssap + (1 - Fs_positive_OK) * Ssam);

	  // t
	  Stap =
	    wB * (dt/dB) * (dt - dT)/(dB + dT) +
	    wP * ( (dT - dt)/dT * (1 + dt/dB) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wT * (dt/dT) * (1 + (dt - dT)/(dB + dT));
	
	  Stam =
	    wP  * (1 - dt/dTT) * (dT - dt)/dT +
	    wT  * ( (1 + (dT - dt)/(dTT - dT)) * (dt/dT) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wTT * (dt - dT)/(dTT - dT) * (dt/dTT);
	  
	  St = Ft * (Ft_positive_OK * Stap + (1 - Ft_positive_OK) * Stam);

	  // b
	  Sbam =
	    wT * (db/dT) * (db - dB)/(dB + dT) +
	    wP * ( (dB - db)/dB * (1 + db/dT) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wB * (db/dB) * (1 + (db - dB)/(dB + dT));
	
	  Sbap =
	    wP  * (1 - db/dBB) * (dB - db)/dB +
	    wB  * ( (1 + (dB - db)/(dBB - dB)) * (db/dB) - 1 ) + // here modification needed (swbstract 1).. DONE!!
	    wBB * (db - dB)/(dBB - dB) * (db/dBB);
	  
	  Sb = Fb * (Fb_positive_OK * Sbap + (1 - Fb_positive_OK) * Sbam);

	  b_w[index_w] += Sw - Se + Ss - Sn + Sb - St;
#endif
	}
	else{
	  aP_w[index_w] = 1;
	  aW_w[index_w] = 0;
	  aE_w[index_w] = 0;
	  aS_w[index_w] = 0;
	  aN_w[index_w] = 0;
	  aB_w[index_w] = 0;
	  aT_w[index_w] = 0;
	  b_w[index_w] = 0;
	}
      }
    }
  }

  /* Diffusive factor */
  for(z=0; z<diff_w_face_z_count; z++){

    index_ = diff_w_face_z_index[z];
    index_w = diff_w_face_z_index_w[z];
    
    K = diff_w_face_z_K[z];
    
    mu_eff = mu + 0.5 * (mu_turb_at_w_nodes[index_w] + mu_turb_at_w_nodes[index_w + Nx*Ny]);
    S_diff = mu_eff * diff_factor_w_face_z[z] *
      (w[index_w] * w_cut_fb[index_] + w[index_w + Nx*Ny] * w_cut_ft[index_]);

    /* Boundary cells coefficients are set at set_bc_flow. 
       We do not want to modify its values here. */    
    if((w_dom_matrix[index_w] == 0) && (K > 1)){
      b_w[index_w] -= S_diff;
    }
    if((w_dom_matrix[index_w + Nx*Ny] == 0) && (K < Nz-2)){
      b_w[index_w + Nx*Ny] += S_diff;
    }
  }

  /* Solid factor */
  if((curves > 0) && (consider_wall_shear_OK)){
    for(z=0; z<w_sol_factor_count; z++){
      
      index_w = w_sol_factor_index_w[z];
      
      if(w_dom_matrix[index_w] == 0){
	
	index_ = w_sol_factor_index[z];
	
	mu_eff = mu + mu_turb_at_w_nodes[index_w];
	
	aP_w[index_w] += mu_eff * w_sol_factor[z];
	b_w[index_w] += w_p_factor[z] *
	  (w_p_factor_B[z] * p[index_-Nx*Ny] + (1 - w_p_factor_B[z]) * p[index_]);
      }
    }
  }

  /* Adding source corresponding to periodic flow if relevant */

  if(p_source_orientation == 2){

#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(w_dom_matrix, b_w, vol_z)
    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_w = k*Nx*Ny + J*Nx + I;
	
	  if(w_dom_matrix[index_w] == 0){
	    b_w[index_w] += periodic_source * vol_z[index_w];
	  }
	
	}
      }
    }
  }

  /* Transient contribution */

  if(!flow_steady_state_OK){

#pragma omp parallel for default(none) private(I, J, k, index_w, aP0)	\
  shared(w_dom_matrix, vol_z, aP_w, b_w, w0)
    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_w = k*Nx*Ny + J*Nx + I;
	
	  if(w_dom_matrix[index_w] == 0){
	  
	    aP0 = rho * vol_z[index_w] / dtime;
	    aP_w[index_w] += aP0;
	    b_w[index_w] += aP0 * w0[index_w];
	  
	  }
	}
      }
    }
  }

  /* Add custom source term */

  if(add_momentum_source_OK){
    add_w_momentum_source_3D(data);
  }

  /* Relax coeficients */
  if(pv_coupling_algorithm){
    relax_coeffs_3D(w, w_dom_matrix, 0, data->coeffs_w, Nx, Ny, nz, data->alpha_w);
  }

  for(i=0; i<w_slave_count; i++){
    index_w = w_slave_nodes[i];
    aP_w[index_w] = 1;
    aW_w[index_w] = 0;
    aE_w[index_w] = 0;
    aS_w[index_w] = 0;
    aN_w[index_w] = 0;
    aB_w[index_w] = 0;
    aT_w[index_w] = 0;
    b_w[index_w] = 0;
  }

  return;
}

