#include "include/some_defs.h"

/* YMY_TEG_PHASE */
/**

   Determines if there are TEG phases.

   MODIFIED: 01-09-2020

   @param teg: Pointer to TEG structure.

   @return 0 if no TEG phases are found.
*/
int ymy_teg_phase(const TEG *teg){

  int i;

  int TE_OK_summary = 0;

  const int curves = teg->curves;
  const int *TE_OK = teg->TE_OK;
  
  if(curves >0){
    
    for(i=0; i<curves; i++){
      TE_OK_summary = TE_OK_summary || TE_OK[i];
    }
    
  }
  
  return TE_OK_summary;
}

/* SAVE_TRANSIENT_TEG_DATA */
void save_transient_teg_data(Data_Mem * data, const char* tr_filename){

  const TEG *teg = &(data->teg);

  Wins *wins = &(data->wins);
  
  FILE *fp = NULL;
  
  int i;
  
  const int curves = teg->curves;

  open_check_file(tr_filename, &fp, wins, 1);
  
  /* (min) */
  fprintf(fp, "%8.2f", data->dt[6] / 60);
  /* (mA) */
  fprintf(fp, " %8.2f", teg->I * 1000);

  for(i=0; i<curves; i++){
    fprintf(fp, " %8.1f", teg->QL[i]);
    fprintf(fp, " %8.1f", teg->Q0[i]);
    fprintf(fp, " %8.2f", teg->TL[i]);
    fprintf(fp, " %8.2f", teg->T0[i]);
  }

  fprintf(fp, ";\n");
  
  fclose(fp);
  
  return;
}

/* SET_TEM_PROPERTIES */
int set_TEM_properties(TEG *teg){

  int ret_value = 0;
  int i;

  const int curves = teg->curves;

  const int *TE_OK = teg->TE_OK;

  double *K = teg->K;
  double *Re = teg->Re;
  
  double *QL = teg->QL;
  double *Q0 = teg->Q0;

  double *TL = teg->TL;
  double *T0 = teg->T0;

  const double *RHOe = teg->RHOe;
  const double *k = teg->k;
  const double *A = teg->A;
  const double *L = teg->L;
  
  /* Properties per pair of legs */
  for(i=0; i<curves; i++){
    
    if(TE_OK[i]){
      K[i] = 2*A[i]*k[i]/L[i];
      Re[i] = 2*RHOe[i]*L[i]/A[i];
    }
    else{
      QL[i] = NAN;
      Q0[i] = NAN;
      TL[i] = NAN;
      T0[i] = NAN;
    }
    
  }

  return ret_value;
}

/* F_CURRENT */
/**

   Determines error between guessed and estimated current.

   MODIFIED: 01-09-2020

   @param x: Guessed current (A).

   @param teg: Pointer to TEG structure.

   @return error between guessed and estimated current.
*/
double F_current(double x, TEG *teg){

  int i;

  const int curves = teg->curves;
  const int *TE_OK = teg->TE_OK;
  const int *ntppm = teg->ntppm;

  double delta, epsilon, theta, phi;
  double joule;

  double num = 0, den = 0;

  double *QL = teg->QL;
  double *Q0 = teg->Q0;
  
  double *TL = teg->TL;
  double *T0 = teg->T0;
  
  const double Rea = 1;

  const double *TfH = teg->TfH;
  const double *TfC = teg->TfC;

  const double *hL = teg->hL;
  const double *h0 = teg->h0;
  
  const double *ALPHA = teg->ALPHA;
  const double *K = teg->K;
  const double *Re = teg->Re;

  const double *A = teg->A;
  
  /* Given current determine temperatures */
  for(i=0; i<curves; i++){

    if(TE_OK[i]){
      
      joule = x*x*Re[i]/2;
      
      delta = 2*A[i]*hL[i]*TfH[i];
      epsilon = 2*A[i]*h0[i]*TfC[i];
      theta = 2*A[i]*h0[i] - x*ALPHA[i] + K[i];
      phi = 2*A[i]*hL[i] + x*ALPHA[i] + K[i]*(1 - K[i]/theta);
      
      TL[i] = (delta + joule)/phi + K[i]*(epsilon + joule)/(phi*theta);
      T0[i] = (K[i]*TL[i] + epsilon + joule)/theta;
      
      /* Heat Flow per module */
      QL[i] = ntppm[i]*(x*ALPHA[i]*TL[i] + K[i]*(TL[i] - T0[i]) - joule);
      Q0[i] = ntppm[i]*(x*ALPHA[i]*T0[i] + K[i]*(TL[i] - T0[i]) + joule);
    }
    
  }
  
  /* Given temperatures determine current */
  for(i=0; i<curves; i++){
    
    if(TE_OK[i]){
      num = num + ntppm[i]*ALPHA[i]*(TL[i] - T0[i]);
      den = den + ntppm[i]*Re[i];
    }
    
   }
  
  den = den * (1 + Rea);

  teg->I = x;
  teg->V = num;
  teg->P = num * x;
  
  return num/den - x;
}

/* SECANT_METHOD */
double secant_method(double xA, double xB,
		     TEG *teg,
                     double (*f) (double, TEG *)){

  int i;
  const int limit = 50;
  
  double fA, fB;
  double d;
  /* Tolerance in (A) */
  const double e = 1.0e-12;
 
  fA = (*f)(xA, teg);
  for(i=0; i<limit; i++){
    fB = (*f)(xB, teg);
    d = (xB - xA) / (fB - fA) * fB;
    if (fabs(xB - xA) < e){ 
      break;
    }
    xA = xB;
    fA = fB;
    xB -= d;
  }
    
  if (i >= limit - 1) {
    return NAN;
  }
    
  return xB;
}

/* SOLVE_CURRENT */
int solve_current(Data_Mem *data, char *current_filename){

  TEG *teg = &(data->teg);

  FILE *current_fp = NULL;

  int i;
  int ntm = 0;
  const int curves = teg->curves;

  const int *TE_OK = teg->TE_OK;

  double x = 0.0;

  double *TfH = teg->TfH;
  
  Wins *wins = &(data->wins);

  open_check_file(current_filename, &current_fp, wins, 1);

  /* Set hot reservoir temperatures */
  for(i=0; i<curves; i++){
    if(TE_OK[i]){
      TfH[i] = compute_surface_average_phase_phi(data, i+1);
      ntm++;
    }
  }

  /* Active number of TEMs */
  teg->ntm = ntm;
    
  x = secant_method(x-1e-4, x, teg, &F_current);

  /* With current solved lets call one last time F_current to set IVP */
  F_current(x, teg);
  
  if (!isnan(x)){
    fprintf(current_fp, "%7.2f %7.2f %7.2f\n", teg->I*1000, teg->V*1000, teg->P*1000);
  }
  else{
    error_msg(wins, "Current not found");
  }
  
  fclose(current_fp);

  return 0;
}

