#include "include/some_defs.h"
extern sig_atomic_t action_on_signal_OK;
extern int timer_flag_OK;


/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_EXPERIMENTAL_3D */
/**

   This scheme uses the gradients accross phases to estimate dphidN and dphidS, 
   which are evaluated at the faces (ewns).

   Consequently it does not call to compute_solid_der_at_interface.

   It does not uses the Lagrangian interpolation.

   It uses the harmonic and arithmetic transport property with the volume fraction
   computed at the displaced (velocity) cell, i.e., for an E fluid cell it uses
   alpha = v_fraction_x[index_+1];

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data
  
   @return void
   
   MODIFIED: 18-07-2020
*/
void compute_near_boundary_molecular_experimental_3D(Data_Mem *data){

  int count, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nxnz, nynx, nyny, nynz, nznx, nzny, nznz;
  /* Products of parallel vector components */
  double sxsx, sxsy, sxsz, sysx, sysy, sysz, szsx, szsy, szsz;
  /* Products of parallel_2 vector components */
  double s2xs2x, s2xs2y, s2xs2z, s2ys2x, s2ys2y, s2ys2z, s2zs2x, s2zs2y, s2zs2z;
  
  double dphidx, dphidy, dphidz;
  double dphidS_x, dphidS_y, dphidS_z;
  double dphidS_2_x, dphidS_2_y, dphidS_2_z;
  
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  double *J_par_2 = data->J_par_2;
    
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  double *J_z = data->J_z;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;

  const double *Delta_zT = data->Delta_zT;
  const double *Delta_zB = data->Delta_zB;
  
  const double *v_fraction_x = data->v_fraction_x;
  const double *v_fraction_y = data->v_fraction_y;
  const double *v_fraction_z = data->v_fraction_z;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_x_z = data->norm_vec_x_z;
  
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *norm_vec_y_z = data->norm_vec_y_z;

  const double *norm_vec_z_x = data->norm_vec_z_x;
  const double *norm_vec_z_y = data->norm_vec_z_y;
  const double *norm_vec_z_z = data->norm_vec_z_z;

  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_x_z = data->par_vec_x_z;
  
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  const double *par_vec_y_z = data->par_vec_y_z;
  
  const double *par_vec_z_x = data->par_vec_z_x;
  const double *par_vec_z_y = data->par_vec_z_y;
  const double *par_vec_z_z = data->par_vec_z_z;
  
  const double *par_vec_x_x_2 = data->par_vec_x_x_2;
  const double *par_vec_x_y_2 = data->par_vec_x_y_2;
  const double *par_vec_x_z_2 = data->par_vec_x_z_2;
  
  const double *par_vec_y_x_2 = data->par_vec_y_x_2;
  const double *par_vec_y_y_2 = data->par_vec_y_y_2;
  const double *par_vec_y_z_2 = data->par_vec_y_z_2;
  
  const double *par_vec_z_x_2 = data->par_vec_z_x_2;
  const double *par_vec_z_y_2 = data->par_vec_z_y_2;
  const double *par_vec_z_z_2 = data->par_vec_z_z_2;
  
  for(count=0; count<solid_boundary_cell_count; count++){
      
    index_ = sf_boundary_solid_indexs[count];

    /* Derivatives */
    dphidx = __central_der(phi[index_+1], phi[index_], phi[index_-1],
			   Delta_xE[index_], Delta_xW[index_]);
    dphidy = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx],
			   Delta_yN[index_], Delta_yS[index_]);
    dphidz = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny],
			   Delta_zT[index_], Delta_zB[index_]);
    
    /* East fluid cell ==> Internal boundary */
    if (E_is_fluid_OK[index_]){

      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];
      nxnz = norm_vec_x_x[index_+1] * norm_vec_x_z[index_+1];

      sxsx = par_vec_x_x[index_+1] * par_vec_x_x[index_+1];
      sxsy = par_vec_x_x[index_+1] * par_vec_x_y[index_+1];
      sxsz = par_vec_x_x[index_+1] * par_vec_x_z[index_+1];

      s2xs2x = par_vec_x_x_2[index_+1] * par_vec_x_x_2[index_+1];
      s2xs2y = par_vec_x_x_2[index_+1] * par_vec_x_y_2[index_+1];
      s2xs2z = par_vec_x_x_2[index_+1] * par_vec_x_z_2[index_+1];

      
      /* Volume fraction */
      alpha = v_fraction_x[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny + dphidz * nxnz);
      
      /* J_par (only x component) */
      dphidS_x = sxsx * dphidx + sxsy * dphidy + sxsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_x;

      /* J_par_2 (only x component) */
      dphidS_2_x = s2xs2x * dphidx + s2xs2y * dphidy + s2xs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_x[index_+1] = J_x[index_];

    }
      
    /* West fluid cell */
    if (W_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the W */
      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];
      nxnz = norm_vec_x_x[index_-1] * norm_vec_x_z[index_-1];

      sxsx = par_vec_x_x[index_-1] * par_vec_x_x[index_-1];
      sxsy = par_vec_x_x[index_-1] * par_vec_x_y[index_-1];
      sxsz = par_vec_x_x[index_-1] * par_vec_x_z[index_-1];

      s2xs2x = par_vec_x_x_2[index_-1] * par_vec_x_x_2[index_-1];
      s2xs2y = par_vec_x_x_2[index_-1] * par_vec_x_y_2[index_-1];
      s2xs2z = par_vec_x_x_2[index_-1] * par_vec_x_z_2[index_-1];

      /* Volume fraction */
      alpha = v_fraction_x[index_-1];

      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny + dphidz * nxnz);
	  
      /* J_par (only x component) */
      dphidS_x = sxsx * dphidx + sxsy * dphidy + sxsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_x;

      /* J_par_2 (only x component) */
      dphidS_2_x = s2xs2x * dphidx + s2xs2y * dphidy + s2xs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_x;

      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_x[index_-1] = J_x[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (N_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the N */
      nynx = norm_vec_y_y[index_+Nx] * norm_vec_y_x[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];
      nynz = norm_vec_y_y[index_+Nx] * norm_vec_y_z[index_+Nx];

      sysx = par_vec_y_y[index_+Nx] * par_vec_y_x[index_+Nx];
      sysy = par_vec_y_y[index_+Nx] * par_vec_y_y[index_+Nx];
      sysz = par_vec_y_y[index_+Nx] * par_vec_y_z[index_+Nx];

      s2ys2x = par_vec_y_y_2[index_+Nx] * par_vec_y_x_2[index_+Nx];
      s2ys2y = par_vec_y_y_2[index_+Nx] * par_vec_y_y_2[index_+Nx];
      s2ys2z = par_vec_y_y_2[index_+Nx] * par_vec_y_z_2[index_+Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_+Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nynx + dphidy * nyny + dphidz * nynz);

      dphidS_y = sysx * dphidx + sysy * dphidy + sysz * dphidz;
      J_par[index_] = -gamma_a * dphidS_y;

      dphidS_2_y = s2ys2x * dphidx + s2ys2y * dphidy + s2ys2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_y;

      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_y[index_+Nx] = J_y[index_];
      
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (S_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the S */
      nynx = norm_vec_y_y[index_-Nx] * norm_vec_y_x[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];
      nynz = norm_vec_y_y[index_-Nx] * norm_vec_y_z[index_-Nx];

      sysx = par_vec_y_y[index_-Nx] * par_vec_y_x[index_-Nx];
      sysy = par_vec_y_y[index_-Nx] * par_vec_y_y[index_-Nx];
      sysz = par_vec_y_y[index_-Nx] * par_vec_y_z[index_-Nx];

      s2ys2x = par_vec_y_y_2[index_-Nx] * par_vec_y_x_2[index_-Nx];
      s2ys2y = par_vec_y_y_2[index_-Nx] * par_vec_y_y_2[index_-Nx];
      s2ys2z = par_vec_y_y_2[index_-Nx] * par_vec_y_z_2[index_-Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_-Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nynx + dphidy * nyny + dphidz * nynz);

      dphidS_y = sysx * dphidx + sysy * dphidy + sysz * dphidz;
      J_par[index_] = -gamma_a * dphidS_y;

      dphidS_2_y = s2ys2x * dphidx + s2ys2y * dphidy + s2ys2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_y;
      
      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_y[index_-Nx] = J_y[index_];
    }

    /* Top (Fluid cell located at top position from solid domain) */
    if (T_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the T */
      nznx = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_x[index_+Nx*Ny];
      nzny = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_y[index_+Nx*Ny];
      nznz = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_z[index_+Nx*Ny];

      szsx = par_vec_z_z[index_+Nx*Ny] * par_vec_z_x[index_+Nx*Ny];
      szsy = par_vec_z_z[index_+Nx*Ny] * par_vec_z_y[index_+Nx*Ny];
      szsz = par_vec_z_z[index_+Nx*Ny] * par_vec_z_z[index_+Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_x_2[index_+Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_y_2[index_+Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_z_2[index_+Nx*Ny];

      /* Volume fraction */
      alpha = v_fraction_z[index_+Nx*Ny];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nznx + dphidy * nzny + dphidz * nznz);

      dphidS_z = szsx * dphidx + szsy * dphidy + szsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_z;

      dphidS_2_z = s2zs2x * dphidx + s2zs2y * dphidy + s2zs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_z;

      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_z[index_+Nx*Ny] = J_z[index_];
      
    }

    /* Bottom (Fluid cell located at bottom position from solid domain) */
    if (B_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the B */
      nznx = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_x[index_-Nx*Ny];
      nzny = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_y[index_-Nx*Ny];
      nznz = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_z[index_-Nx*Ny];

      szsx = par_vec_z_z[index_-Nx*Ny] * par_vec_z_x[index_-Nx*Ny];
      szsy = par_vec_z_z[index_-Nx*Ny] * par_vec_z_y[index_-Nx*Ny];
      szsz = par_vec_z_z[index_-Nx*Ny] * par_vec_z_z[index_-Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_x_2[index_-Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_y_2[index_-Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_z_2[index_-Nx*Ny];

      /* Volume fraction */
      alpha = v_fraction_z[index_-Nx*Ny];

      /* Phase properties */
      gamma_f = gamma_m[index_-Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nznx + dphidy * nzny + dphidz * nznz);

      dphidS_z = szsx * dphidx + szsy * dphidy + szsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_z;

      dphidS_2_z = s2zs2x * dphidx + s2zs2y * dphidy + s2zs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_z;

      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_z[index_-Nx*Ny] = J_z[index_];
      
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_TSUTSUMI_DEF_3D */
/**

   This is coded following the derivations from the paper of Tsutsumi 2014.
   It gives better results than Sato for more than gamma_s/gamma_f = 100.
   Difference being in the normal vector wich must be extracted from the ij cell.
   This must be corrected, or announced as a new method :).

   This scheme uses the gradients accross phases to estimate dphidN and dphidS, 
   across phases in a simplifyed manner, i.e.
   dphidx = (phiE - phiW]) / (2 * Delta_x[index_]);
   dphidy = (phiN - phiS]) / (2 * Delta_y[index_]);
   
   It is DEFerred in the sense that the tensor used is on the fluid side. 
   For E fluid is
   nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
   nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];
   
   Consequently it does not call to compute_solid_der_at_interface.
   It does not uses the Lagrangian interpolation.
   It uses the harmonic and arithmetic transport property, for an E fluid cell it uses
   alpha = v_fraction[index_]; (no 'xyz')

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data
  
   @return void
   
   MODIFIED: 18-07-2020
*/
void compute_near_boundary_molecular_flux_Tsutsumi_def_3D(Data_Mem *data){

  int count, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nxnz, nynx, nyny, nynz, nznx, nzny, nznz;
  /* Products of parallel vector components */
  double sxsx, sxsy, sxsz, sysx, sysy, sysz, szsx, szsy, szsz;
  /* Products of parallel_2 vector components */
  double s2xs2x, s2xs2y, s2xs2z, s2ys2x, s2ys2y, s2ys2z, s2zs2x, s2zs2y, s2zs2z;
  
  double dphidx, dphidy, dphidz;
  double dphidS_x, dphidS_y, dphidS_z;
  double dphidS_2_x, dphidS_2_y, dphidS_2_z;
  
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  double *J_par_2 = data->J_par_2;
    
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  double *J_z = data->J_z;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;

  const double *Delta_zT = data->Delta_zT;
  const double *Delta_zB = data->Delta_zB;
  
  const double *v_fraction = data->v_fraction;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_x_z = data->norm_vec_x_z;
  
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *norm_vec_y_z = data->norm_vec_y_z;

  const double *norm_vec_z_x = data->norm_vec_z_x;
  const double *norm_vec_z_y = data->norm_vec_z_y;
  const double *norm_vec_z_z = data->norm_vec_z_z;

  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_x_z = data->par_vec_x_z;
  
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  const double *par_vec_y_z = data->par_vec_y_z;
  
  const double *par_vec_z_x = data->par_vec_z_x;
  const double *par_vec_z_y = data->par_vec_z_y;
  const double *par_vec_z_z = data->par_vec_z_z;
  
  const double *par_vec_x_x_2 = data->par_vec_x_x_2;
  const double *par_vec_x_y_2 = data->par_vec_x_y_2;
  const double *par_vec_x_z_2 = data->par_vec_x_z_2;
  
  const double *par_vec_y_x_2 = data->par_vec_y_x_2;
  const double *par_vec_y_y_2 = data->par_vec_y_y_2;
  const double *par_vec_y_z_2 = data->par_vec_y_z_2;
  
  const double *par_vec_z_x_2 = data->par_vec_z_x_2;
  const double *par_vec_z_y_2 = data->par_vec_z_y_2;
  const double *par_vec_z_z_2 = data->par_vec_z_z_2;
  
  for(count=0; count<solid_boundary_cell_count; count++){
      
    index_ = sf_boundary_solid_indexs[count];

    /* Derivatives */
    dphidx = __central_der(phi[index_+1], phi[index_], phi[index_-1],
			   Delta_xE[index_], Delta_xW[index_]);
    dphidy = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx],
			   Delta_yN[index_], Delta_yS[index_]);
    dphidz = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny],
			   Delta_zT[index_], Delta_zB[index_]);

    /* Volume fraction */
    alpha = v_fraction[index_];
    
    /* East fluid cell ==> Internal boundary */
    if (E_is_fluid_OK[index_]){

      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];
      nxnz = norm_vec_x_x[index_+1] * norm_vec_x_z[index_+1];

      sxsx = par_vec_x_x[index_+1] * par_vec_x_x[index_+1];
      sxsy = par_vec_x_x[index_+1] * par_vec_x_y[index_+1];
      sxsz = par_vec_x_x[index_+1] * par_vec_x_z[index_+1];

      s2xs2x = par_vec_x_x_2[index_+1] * par_vec_x_x_2[index_+1];
      s2xs2y = par_vec_x_x_2[index_+1] * par_vec_x_y_2[index_+1];
      s2xs2z = par_vec_x_x_2[index_+1] * par_vec_x_z_2[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny + dphidz * nxnz);
      
      /* J_par (only x component) */
      dphidS_x = sxsx * dphidx + sxsy * dphidy + sxsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_x;

      /* J_par_2 (only x component) */
      dphidS_2_x = s2xs2x * dphidx + s2xs2y * dphidy + s2xs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_x[index_+1] = J_x[index_];

    }
      
    /* West fluid cell */
    if (W_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the W */
      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];
      nxnz = norm_vec_x_x[index_-1] * norm_vec_x_z[index_-1];

      sxsx = par_vec_x_x[index_-1] * par_vec_x_x[index_-1];
      sxsy = par_vec_x_x[index_-1] * par_vec_x_y[index_-1];
      sxsz = par_vec_x_x[index_-1] * par_vec_x_z[index_-1];

      s2xs2x = par_vec_x_x_2[index_-1] * par_vec_x_x_2[index_-1];
      s2xs2y = par_vec_x_x_2[index_-1] * par_vec_x_y_2[index_-1];
      s2xs2z = par_vec_x_x_2[index_-1] * par_vec_x_z_2[index_-1];

      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny + dphidz * nxnz);
	  
      /* J_par (only x component) */
      dphidS_x = sxsx * dphidx + sxsy * dphidy + sxsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_x;

      /* J_par_2 (only x component) */
      dphidS_2_x = s2xs2x * dphidx + s2xs2y * dphidy + s2xs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_x;

      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_x[index_-1] = J_x[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (N_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the N */
      nynx = norm_vec_y_y[index_+Nx] * norm_vec_y_x[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];
      nynz = norm_vec_y_y[index_+Nx] * norm_vec_y_z[index_+Nx];

      sysx = par_vec_y_y[index_+Nx] * par_vec_y_x[index_+Nx];
      sysy = par_vec_y_y[index_+Nx] * par_vec_y_y[index_+Nx];
      sysz = par_vec_y_y[index_+Nx] * par_vec_y_z[index_+Nx];

      s2ys2x = par_vec_y_y_2[index_+Nx] * par_vec_y_x_2[index_+Nx];
      s2ys2y = par_vec_y_y_2[index_+Nx] * par_vec_y_y_2[index_+Nx];
      s2ys2z = par_vec_y_y_2[index_+Nx] * par_vec_y_z_2[index_+Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nynx + dphidy * nyny + dphidz * nynz);

      dphidS_y = sysx * dphidx + sysy * dphidy + sysz * dphidz;
      J_par[index_] = -gamma_a * dphidS_y;

      dphidS_2_y = s2ys2x * dphidx + s2ys2y * dphidy + s2ys2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_y;

      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_y[index_+Nx] = J_y[index_];
      
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (S_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the S */
      nynx = norm_vec_y_y[index_-Nx] * norm_vec_y_x[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];
      nynz = norm_vec_y_y[index_-Nx] * norm_vec_y_z[index_-Nx];

      sysx = par_vec_y_y[index_-Nx] * par_vec_y_x[index_-Nx];
      sysy = par_vec_y_y[index_-Nx] * par_vec_y_y[index_-Nx];
      sysz = par_vec_y_y[index_-Nx] * par_vec_y_z[index_-Nx];

      s2ys2x = par_vec_y_y_2[index_-Nx] * par_vec_y_x_2[index_-Nx];
      s2ys2y = par_vec_y_y_2[index_-Nx] * par_vec_y_y_2[index_-Nx];
      s2ys2z = par_vec_y_y_2[index_-Nx] * par_vec_y_z_2[index_-Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nynx + dphidy * nyny + dphidz * nynz);

      dphidS_y = sysx * dphidx + sysy * dphidy + sysz * dphidz;
      J_par[index_] = -gamma_a * dphidS_y;

      dphidS_2_y = s2ys2x * dphidx + s2ys2y * dphidy + s2ys2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_y;
      
      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_y[index_-Nx] = J_y[index_];
    }

    /* Top (Fluid cell located at top position from solid domain) */
    if (T_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the T */
      nznx = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_x[index_+Nx*Ny];
      nzny = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_y[index_+Nx*Ny];
      nznz = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_z[index_+Nx*Ny];

      szsx = par_vec_z_z[index_+Nx*Ny] * par_vec_z_x[index_+Nx*Ny];
      szsy = par_vec_z_z[index_+Nx*Ny] * par_vec_z_y[index_+Nx*Ny];
      szsz = par_vec_z_z[index_+Nx*Ny] * par_vec_z_z[index_+Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_x_2[index_+Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_y_2[index_+Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_z_2[index_+Nx*Ny];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nznx + dphidy * nzny + dphidz * nznz);

      dphidS_z = szsx * dphidx + szsy * dphidy + szsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_z;

      dphidS_2_z = s2zs2x * dphidx + s2zs2y * dphidy + s2zs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_z;

      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_z[index_+Nx*Ny] = J_z[index_];
      
    }

    /* Bottom (Fluid cell located at bottom position from solid domain) */
    if (B_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the B */
      nznx = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_x[index_-Nx*Ny];
      nzny = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_y[index_-Nx*Ny];
      nznz = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_z[index_-Nx*Ny];

      szsx = par_vec_z_z[index_-Nx*Ny] * par_vec_z_x[index_-Nx*Ny];
      szsy = par_vec_z_z[index_-Nx*Ny] * par_vec_z_y[index_-Nx*Ny];
      szsz = par_vec_z_z[index_-Nx*Ny] * par_vec_z_z[index_-Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_x_2[index_-Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_y_2[index_-Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_z_2[index_-Nx*Ny];

      /* Phase properties */
      gamma_f = gamma_m[index_-Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nznx + dphidy * nzny + dphidz * nznz);

      dphidS_z = szsx * dphidx + szsy * dphidy + szsz * dphidz;
      J_par[index_] = -gamma_a * dphidS_z;

      dphidS_2_z = s2zs2x * dphidx + s2zs2y * dphidy + s2zs2z * dphidz;
      J_par_2[index_] = -gamma_a * dphidS_2_z;

      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_z[index_-Nx*Ny] = J_z[index_];
      
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_TSUTSUMI_3D */
/**

   This scheme uses the gradients accross phases to estimate dphidN and dphidS, 
   across phases in a simplifyed manner, i.e.
   dphidx = (phiE - phiW]) / (2 * Delta_x[index_]);
   dphidy = (phiN - phiS]) / (2 * Delta_y[index_]);

   Moreover, it uses a tensorial construction common for all cases (EWNS)
   at the boundary fluid cell
   nxnx = norm_x[index_] * norm_x[index_];
   nxny = norm_x[index_] * norm_y[index_];
   nyny = norm_y[index_] * norm_y[index_];

   Consequently it does not call to compute_solid_der_at_interface.
   It does not uses the Lagrangian interpolation.
   It uses the harmonic and arithmetic transport property, for an E fluid cell it uses
   alpha = v_fraction[index_]; (no 'xyz')

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data
  
   @return void
   
   MODIFIED: 20-07-2020
*/
void compute_near_boundary_molecular_flux_Tsutsumi_3D(Data_Mem *data){

  int count, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nxnz, nyny, nynz, nznz;
  /* Products of parallel vector components */
  double sxsx, sxsy, sxsz, sysy, sysz, szsz;
  /* Products of parallel_2 vector components */
  double s2xs2x, s2xs2y, s2xs2z, s2ys2y, s2ys2z, s2zs2z;
  
  double dphidx, dphidy, dphidz;
  double dphidS_x, dphidS_y, dphidS_z;
  double dphidS_2_x, dphidS_2_y, dphidS_2_z;
  
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  double *J_par_2 = data->J_par_2;
    
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  double *J_z = data->J_z;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;

  const double *Delta_zT = data->Delta_zT;
  const double *Delta_zB = data->Delta_zB;
  
  const double *v_fraction = data->v_fraction;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_x = data->norm_x;
  const double *norm_y = data->norm_y;
  const double *norm_z = data->norm_z;

  const double *par_x = data->par_x;
  const double *par_y = data->par_y;
  const double *par_z = data->par_z;

  const double *par_x_2 = data->par_x_2;
  const double *par_y_2 = data->par_y_2;
  const double *par_z_2 = data->par_z_2;
  
  for(count=0; count<solid_boundary_cell_count; count++){
      
    index_ = sf_boundary_solid_indexs[count];

    /* Derivatives */
    dphidx = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
    dphidy = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
    dphidz = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);

    /* East fluid cell ==> Internal boundary */
    if (E_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_+1];
    
      nxnx = norm_x[index_+1] * norm_x[index_+1];
      nxny = norm_x[index_+1] * norm_y[index_+1];
      nxnz = norm_x[index_+1] * norm_z[index_+1];
      nyny = norm_y[index_+1] * norm_y[index_+1];
      nynz = norm_y[index_+1] * norm_z[index_+1];
      nznz = norm_z[index_+1] * norm_z[index_+1];

      sxsx = par_x[index_+1] * par_x[index_+1];
      sxsy = par_x[index_+1] * par_y[index_+1];
      sxsz = par_x[index_+1] * par_z[index_+1];
      sysy = par_y[index_+1] * par_y[index_+1];
      sysz = par_y[index_+1] * par_z[index_+1];
      szsz = par_z[index_+1] * par_z[index_+1];

      s2xs2x = par_x_2[index_+1] * par_x_2[index_+1];
      s2xs2y = par_x_2[index_+1] * par_y_2[index_+1];
      s2xs2z = par_x_2[index_+1] * par_z_2[index_+1];
      s2ys2y = par_y_2[index_+1] * par_y_2[index_+1];
      s2ys2z = par_y_2[index_+1] * par_z_2[index_+1];
      s2zs2z = par_z_2[index_+1] * par_z_2[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny + dphidz * nxnz);
	  
      /* x component of tangential flux */
      dphidS_x = sxsx * dphidx + sxsy * dphidy + sxsz * dphidz;

      /* x component of tangential_2 flux */
      dphidS_2_x = s2xs2x * dphidx + s2xs2y * dphidy + s2xs2z * dphidz;
      
      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_x;

      /* J_par_2 (only x component) */
      J_par_2[index_] = -gamma_a * dphidS_2_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_x[index_+1] = J_x[index_];

    }
      
    /* West fluid cell */
    if (W_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_-1];
    
      nxnx = norm_x[index_-1] * norm_x[index_-1];
      nxny = norm_x[index_-1] * norm_y[index_-1];
      nxnz = norm_x[index_-1] * norm_z[index_-1];
      nyny = norm_y[index_-1] * norm_y[index_-1];
      nynz = norm_y[index_-1] * norm_z[index_-1];
      nznz = norm_z[index_-1] * norm_z[index_-1];

      sxsx = par_x[index_-1] * par_x[index_-1];
      sxsy = par_x[index_-1] * par_y[index_-1];
      sxsz = par_x[index_-1] * par_z[index_-1];
      sysy = par_y[index_-1] * par_y[index_-1];
      sysz = par_y[index_-1] * par_z[index_-1];
      szsz = par_z[index_-1] * par_z[index_-1];

      s2xs2x = par_x_2[index_-1] * par_x_2[index_-1];
      s2xs2y = par_x_2[index_-1] * par_y_2[index_-1];
      s2xs2z = par_x_2[index_-1] * par_z_2[index_-1];
      s2ys2y = par_y_2[index_-1] * par_y_2[index_-1];
      s2ys2z = par_y_2[index_-1] * par_z_2[index_-1];
      s2zs2z = par_z_2[index_-1] * par_z_2[index_-1];

      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny + dphidz * nxnz);
	  
      /* x component of tangential flux */
      dphidS_x = sxsx * dphidx + sxsy * dphidy + sxsz * dphidz;

      /* x component of tangential_2 flux */
      dphidS_2_x = s2xs2x * dphidx + s2xs2y * dphidy + s2xs2z * dphidz;
      
      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_x;

      /* J_par_2 (only x component) */
      J_par_2[index_] = -gamma_a * dphidS_2_x;

      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_-1] = J_x[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (N_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_+Nx];
    
      nxnx = norm_x[index_+Nx] * norm_x[index_+Nx];
      nxny = norm_x[index_+Nx] * norm_y[index_+Nx];
      nxnz = norm_x[index_+Nx] * norm_z[index_+Nx];
      nyny = norm_y[index_+Nx] * norm_y[index_+Nx];
      nynz = norm_y[index_+Nx] * norm_z[index_+Nx];
      nznz = norm_z[index_+Nx] * norm_z[index_+Nx];

      sxsx = par_x[index_+Nx] * par_x[index_+Nx];
      sxsy = par_x[index_+Nx] * par_y[index_+Nx];
      sxsz = par_x[index_+Nx] * par_z[index_+Nx];
      sysy = par_y[index_+Nx] * par_y[index_+Nx];
      sysz = par_y[index_+Nx] * par_z[index_+Nx];
      szsz = par_z[index_+Nx] * par_z[index_+Nx];

      s2xs2x = par_x_2[index_+Nx] * par_x_2[index_+Nx];
      s2xs2y = par_x_2[index_+Nx] * par_y_2[index_+Nx];
      s2xs2z = par_x_2[index_+Nx] * par_z_2[index_+Nx];
      s2ys2y = par_y_2[index_+Nx] * par_y_2[index_+Nx];
      s2ys2z = par_y_2[index_+Nx] * par_z_2[index_+Nx];
      s2zs2z = par_z_2[index_+Nx] * par_z_2[index_+Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxny + dphidy * nyny + dphidz * nynz);

      dphidS_y = sxsy * dphidx + sysy * dphidy + sysz * dphidz;
      dphidS_2_y = s2xs2y * dphidx + s2ys2y * dphidy + s2ys2z * dphidz;

      J_par[index_] = -gamma_a * dphidS_y;
      J_par_2[index_] = -gamma_a * dphidS_2_y;

      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_y[index_+Nx] = J_y[index_];
      
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (S_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_-Nx];
    
      nxnx = norm_x[index_-Nx] * norm_x[index_-Nx];
      nxny = norm_x[index_-Nx] * norm_y[index_-Nx];
      nxnz = norm_x[index_-Nx] * norm_z[index_-Nx];
      nyny = norm_y[index_-Nx] * norm_y[index_-Nx];
      nynz = norm_y[index_-Nx] * norm_z[index_-Nx];
      nznz = norm_z[index_-Nx] * norm_z[index_-Nx];

      sxsx = par_x[index_-Nx] * par_x[index_-Nx];
      sxsy = par_x[index_-Nx] * par_y[index_-Nx];
      sxsz = par_x[index_-Nx] * par_z[index_-Nx];
      sysy = par_y[index_-Nx] * par_y[index_-Nx];
      sysz = par_y[index_-Nx] * par_z[index_-Nx];
      szsz = par_z[index_-Nx] * par_z[index_-Nx];

      s2xs2x = par_x_2[index_-Nx] * par_x_2[index_-Nx];
      s2xs2y = par_x_2[index_-Nx] * par_y_2[index_-Nx];
      s2xs2z = par_x_2[index_-Nx] * par_z_2[index_-Nx];
      s2ys2y = par_y_2[index_-Nx] * par_y_2[index_-Nx];
      s2ys2z = par_y_2[index_-Nx] * par_z_2[index_-Nx];
      s2zs2z = par_z_2[index_-Nx] * par_z_2[index_-Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxny + dphidy * nyny + dphidz * nynz);

      dphidS_y = sxsy * dphidx + sysy * dphidy + sysz * dphidz;
      dphidS_2_y = s2xs2y * dphidx + s2ys2y * dphidy + s2ys2z * dphidz;

      J_par[index_] = -gamma_a * dphidS_y;
      J_par_2[index_] = -gamma_a * dphidS_2_y;
      
      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_y[index_-Nx] = J_y[index_];
    }

    /* Top */
    if (T_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_+Nx*Ny];
    
      nxnx = norm_x[index_+Nx*Ny] * norm_x[index_+Nx*Ny];
      nxny = norm_x[index_+Nx*Ny] * norm_y[index_+Nx*Ny];
      nxnz = norm_x[index_+Nx*Ny] * norm_z[index_+Nx*Ny];
      nyny = norm_y[index_+Nx*Ny] * norm_y[index_+Nx*Ny];
      nynz = norm_y[index_+Nx*Ny] * norm_z[index_+Nx*Ny];
      nznz = norm_z[index_+Nx*Ny] * norm_z[index_+Nx*Ny];

      sxsx = par_x[index_+Nx*Ny] * par_x[index_+Nx*Ny];
      sxsy = par_x[index_+Nx*Ny] * par_y[index_+Nx*Ny];
      sxsz = par_x[index_+Nx*Ny] * par_z[index_+Nx*Ny];
      sysy = par_y[index_+Nx*Ny] * par_y[index_+Nx*Ny];
      sysz = par_y[index_+Nx*Ny] * par_z[index_+Nx*Ny];
      szsz = par_z[index_+Nx*Ny] * par_z[index_+Nx*Ny];

      s2xs2x = par_x_2[index_+Nx*Ny] * par_x_2[index_+Nx*Ny];
      s2xs2y = par_x_2[index_+Nx*Ny] * par_y_2[index_+Nx*Ny];
      s2xs2z = par_x_2[index_+Nx*Ny] * par_z_2[index_+Nx*Ny];
      s2ys2y = par_y_2[index_+Nx*Ny] * par_y_2[index_+Nx*Ny];
      s2ys2z = par_y_2[index_+Nx*Ny] * par_z_2[index_+Nx*Ny];
      s2zs2z = par_z_2[index_+Nx*Ny] * par_z_2[index_+Nx*Ny];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnz + dphidy * nynz + dphidz * nznz);

      dphidS_z = sxsz * dphidx + sysz * dphidy + szsz * dphidz;
      dphidS_2_z = s2xs2z * dphidx + s2ys2z * dphidy + s2zs2z * dphidz;

      J_par[index_] = -gamma_a * dphidS_z;
      J_par_2[index_] = -gamma_a * dphidS_2_z;

      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_z[index_+Nx*Ny] = J_z[index_];
      
    }

    /* Bottom */
    if (B_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_-Nx*Ny];
    
      nxnx = norm_x[index_-Nx*Ny] * norm_x[index_-Nx*Ny];
      nxny = norm_x[index_-Nx*Ny] * norm_y[index_-Nx*Ny];
      nxnz = norm_x[index_-Nx*Ny] * norm_z[index_-Nx*Ny];
      nyny = norm_y[index_-Nx*Ny] * norm_y[index_-Nx*Ny];
      nynz = norm_y[index_-Nx*Ny] * norm_z[index_-Nx*Ny];
      nznz = norm_z[index_-Nx*Ny] * norm_z[index_-Nx*Ny];

      sxsx = par_x[index_-Nx*Ny] * par_x[index_-Nx*Ny];
      sxsy = par_x[index_-Nx*Ny] * par_y[index_-Nx*Ny];
      sxsz = par_x[index_-Nx*Ny] * par_z[index_-Nx*Ny];
      sysy = par_y[index_-Nx*Ny] * par_y[index_-Nx*Ny];
      sysz = par_y[index_-Nx*Ny] * par_z[index_-Nx*Ny];
      szsz = par_z[index_-Nx*Ny] * par_z[index_-Nx*Ny];

      s2xs2x = par_x_2[index_-Nx*Ny] * par_x_2[index_-Nx*Ny];
      s2xs2y = par_x_2[index_-Nx*Ny] * par_y_2[index_-Nx*Ny];
      s2xs2z = par_x_2[index_-Nx*Ny] * par_z_2[index_-Nx*Ny];
      s2ys2y = par_y_2[index_-Nx*Ny] * par_y_2[index_-Nx*Ny];
      s2ys2z = par_y_2[index_-Nx*Ny] * par_z_2[index_-Nx*Ny];
      s2zs2z = par_z_2[index_-Nx*Ny] * par_z_2[index_-Nx*Ny];

      /* Phase properties */
      gamma_f = gamma_m[index_-Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnz + dphidy * nynz + dphidz * nznz);

      dphidS_z = sxsz * dphidx + sysz * dphidy + szsz * dphidz;
      dphidS_2_z = s2xs2z * dphidx + s2ys2z * dphidy + s2zs2z * dphidz;

      J_par[index_] = -gamma_a * dphidS_z;
      J_par_2[index_] = -gamma_a * dphidS_2_z;

      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      J_z[index_-Nx*Ny] = J_z[index_];
      
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_SATO_AT_FACE_3D */
/**

   Original implementation of Sato et al. (2016) with correction on its Eqs (41)-(42) 
   since it must be multiplied by the aritmethic mean transport property (gamma_a) 
   and NOT the phase alone property.

   Uses the solid phase derivatives for increased stability.

   There is an apparent confusion since there are combination of magnitudes 
   evaluated at the face and the interface:
   - dphidNface evaluated at the face (ewnstb). 
   - dphidS_face, evaluated at the face with Eq 45 Sato (2016).
   - dphidS_2_face, evaluated at the face with Eq 45 Sato (2016).

   The derivatives stored in local arrays dphidx, dphidy, dphidz are in a solid domain.

   Sets:

   Array (double, NxNyNz): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNyNz): Data_Mem::J_par: diffusive flux in the direction parallel to the interface.
   Array (double, NxNyNz): Data_Mem::J_par2: diffusive flux in the direction parallel2 to the interface.
   Array (double, NxNyNz): Data_Mem::J_x: diffusive flux in x direction at face.
   Array (double, NxNyNz): Data_Mem::J_y: diffusive flux in y direction at face.
   Array (double, NxNyNz): Data_Mem::J_z: diffusive flux in z direction at face.
   
   @param data

   @see compute_solid_der_at_interface_3D
   
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_near_boundary_molecular_flux_Sato_at_face_3D(Data_Mem *data){

  char cx, cy, cz;

  const char *case_s_type_x = data->case_s_type_x;
  const char *case_s_type_y = data->case_s_type_y;
  const char *case_s_type_z = data->case_s_type_z;

  int count, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
    
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;
  
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;
  /* Fluid and Solid */
  double gamma_f, gamma_s;

  /* Volume fraction */
  double alpha = 0;

  double es;
  double d, d10, d21;
  double L0, L1, L2;

  double dphi0, dphi1, f01;

  /* Products of normal vector components */
  double nxnx, nxny, nxnz, nynx, nyny, nynz, nznx, nzny, nznz;
  /* Products of parallel vector components */
  double sxsx, sxsy, sxsz, sysx, sysy, sysz, szsx, szsy, szsz;
  /* Products of parallel_2 vector components */
  double s2xs2x, s2xs2y, s2xs2z, s2ys2x, s2ys2y, s2ys2z, s2zs2x, s2zs2y, s2zs2z;

  /* These arrays hold derivatives at points within solid region away from interface,
     for instance if E_is_fluid_OK
     [0]: (I,J), [1]: (I-1,J), [2]: (I-2,J) 
  */
  double dphidx[3] = {0}, dphidy[3] = {0}, dphidz[3] = {0};
  double dphidS[3] = {0}, dphidS_2[3] = {0};
  
  /* Face values of dphidS extrapolated from directions x, y, z, respectively 
     as dictated by Eq 45 Sato (2016) */
  double dphidS_face_x, dphidS_face_y, dphidS_face_z;

  /* Face values of dphidS_2 extrapolated from directions x, y, z, respectively 
     as dictated by Eq 45 Sato (2016) */
  double dphidS_2_face_x, dphidS_2_face_y, dphidS_2_face_z;

  /* These are for estimating the normal gradient and are the derivatives at 
     the face where the curve intersects, for instance if E_is_fluid_OK, face == e.
     The derivatives are across phases.
  */
  double dphidxface, dphidyface, dphidzface;
  double dphidNface;

  /* Normal, parallel and parallel_2 vector */
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  double *J_par_2 = data->J_par_2;
  
  /* These are the molecular flux components measured in W/m2 if they were energy.
   * These are computed at the face ==> must compute velocity if applicable.
   * They are computed from the solid phase.
   * Must be stored in the solid and fluid nodes which share the interface.
   */
  
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  double *J_z = data->J_z;
  
  const double *dphidxs = data->dphidxs;
  const double *dphidys = data->dphidys;
  const double *dphidzs = data->dphidzs;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *v_fraction_x = data->v_fraction_x;
  const double *v_fraction_y = data->v_fraction_y;
  const double *v_fraction_z = data->v_fraction_z;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_x_z = data->norm_vec_x_z;
  
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *norm_vec_y_z = data->norm_vec_y_z;

  const double *norm_vec_z_x = data->norm_vec_z_x;
  const double *norm_vec_z_y = data->norm_vec_z_y;
  const double *norm_vec_z_z = data->norm_vec_z_z;

  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_x_z = data->par_vec_x_z;
  
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  const double *par_vec_y_z = data->par_vec_y_z;
  
  const double *par_vec_z_x = data->par_vec_z_x;
  const double *par_vec_z_y = data->par_vec_z_y;
  const double *par_vec_z_z = data->par_vec_z_z;
  
  const double *par_vec_x_x_2 = data->par_vec_x_x_2;
  const double *par_vec_x_y_2 = data->par_vec_x_y_2;
  const double *par_vec_x_z_2 = data->par_vec_x_z_2;
  
  const double *par_vec_y_x_2 = data->par_vec_y_x_2;
  const double *par_vec_y_y_2 = data->par_vec_y_y_2;
  const double *par_vec_y_z_2 = data->par_vec_y_z_2;
  
  const double *par_vec_z_x_2 = data->par_vec_z_x_2;
  const double *par_vec_z_y_2 = data->par_vec_z_y_2;
  const double *par_vec_z_z_2 = data->par_vec_z_z_2;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *fw = data->fw;
  const double *fs = data->fs;
  const double *fb = data->fb;

  const double *fe = data->fe;
  const double *fn = data->fn;
  const double *ft = data->ft;
  
  /* Compute derivatives at solid points near the interface */
  compute_solid_der_at_interface_3D(data);
  
  for(count=0; count<solid_boundary_cell_count; count++){
    
    index_ = sf_boundary_solid_indexs[count];

    cx = case_s_type_x[index_];
    cy = case_s_type_y[index_];
    cz = case_s_type_z[index_];

    /* dphidx_(I,J,K) */
    dphidx[0] = dphidxs[index_];
    /* dphidy_(I,J,K) */
    dphidy[0] = dphidys[index_];
    /* dphidz_(I,J,K) */
    dphidz[0] = dphidzs[index_];
    
    /* East fluid cell ==> Internal boundary */
    if (E_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the E */
      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];
      nxnz = norm_vec_x_x[index_+1] * norm_vec_x_z[index_+1];

      sxsx = par_vec_x_x[index_+1] * par_vec_x_x[index_+1];
      sxsy = par_vec_x_x[index_+1] * par_vec_x_y[index_+1];
      sxsz = par_vec_x_x[index_+1] * par_vec_x_z[index_+1];

      s2xs2x = par_vec_x_x_2[index_+1] * par_vec_x_x_2[index_+1];
      s2xs2y = par_vec_x_x_2[index_+1] * par_vec_x_y_2[index_+1];
      s2xs2z = par_vec_x_x_2[index_+1] * par_vec_x_z_2[index_+1];

      /* Volume fraction */
      alpha = v_fraction_x[index_+1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	
      /* Gradient estimated at the e face using values of phi across phases */
      dphidxface = (phi[index_+1] - phi[index_]) / Delta_xE[index_];

      f01 = fe[index_];
      
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+1], phi[index_-Nx+1], Delta_yN[index_+1], Delta_yS[index_+1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+1], phi[index_+1], phi[index_-Nx*Ny+1], Delta_zT[index_+1], Delta_zB[index_+1]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;

      dphidNface = (dphidxface * nxnx + dphidyface * nxny + dphidzface * nxnz);
      
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi_dx_(I-1,J,K), cases abc */
      dphidx[1] = dphidxs[index_-1];
      /* dphi_dx_(I-2,J,K), cases ab */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2];
	
      /* dphidy_(I-1,J,K), cases abc */
      dphidy[1] = dphidys[index_-1];
      /* dphidy_(I-2,J,K), cases ab */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2];

      /* dphidz_(I-1,J,K), cases abc */
      dphidz[1] = dphidzs[index_-1];
      /* dphidz_(I-2,J,K), cases ab */
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_-2];

      /* The [2] will have NAN in the case of a 'c' */
      /* x component of tangential flux */
      dphidS[0] = sxsx * dphidx[0] + sxsy * dphidy[0] + sxsz * dphidz[0];
      dphidS[1] = sxsx * dphidx[1] + sxsy * dphidy[1] + sxsz * dphidz[1];
      dphidS[2] = sxsx * dphidx[2] + sxsy * dphidy[2] + sxsz * dphidz[2];

      /* x component of tangential_2 flux */
      dphidS_2[0] = s2xs2x * dphidx[0] + s2xs2y * dphidy[0] + s2xs2z * dphidz[0];
      dphidS_2[1] = s2xs2x * dphidx[1] + s2xs2y * dphidy[1] + s2xs2z * dphidz[1];
      dphidS_2[2] = s2xs2x * dphidx[2] + s2xs2y * dphidy[2] + s2xs2z * dphidz[2];

      /* Lagrangian extrapolation to face */
      d = Delta_xE[index_];
      d10 = Delta_xW[index_];
      d21 = Delta_xW[index_-1];
      es = 1 - f01;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_face_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to face */
	dphidS_face_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_face_x = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_face_x = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }
      
      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_face_x;
      /* J_par_2 (only x component) */
      J_par_2[index_] = -gamma_a * dphidS_2_face_x;
      /* Constructing on the solid side */
      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_x[index_+1] = J_x[index_];
      
    }
    
    /* West fluid cell */
    if (W_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the W */
      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];
      nxnz = norm_vec_x_x[index_-1] * norm_vec_x_z[index_-1];

      sxsx = par_vec_x_x[index_-1] * par_vec_x_x[index_-1];
      sxsy = par_vec_x_x[index_-1] * par_vec_x_y[index_-1];
      sxsz = par_vec_x_x[index_-1] * par_vec_x_z[index_-1];

      s2xs2x = par_vec_x_x_2[index_-1] * par_vec_x_x_2[index_-1];
      s2xs2y = par_vec_x_x_2[index_-1] * par_vec_x_y_2[index_-1];
      s2xs2z = par_vec_x_x_2[index_-1] * par_vec_x_z_2[index_-1];

      /* Volume fraction */
      alpha = v_fraction_x[index_-1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the w face using values of phi across phases */
      dphidxface = (phi[index_] - phi[index_-1]) / Delta_xW[index_];

      f01 = fw[index_];
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx-1], phi[index_-1], phi[index_-Nx-1], Delta_yN[index_-1], Delta_yS[index_-1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny-1], phi[index_-1], phi[index_-Nx*Ny-1], Delta_zT[index_-1], Delta_zB[index_-1]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;
      
      dphidNface = (dphidxface * nxnx + dphidyface * nxny + dphidzface * nxnz);
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I+1, J) */
      dphidx[1] = dphidxs[index_+1];
      /* dphi/dx_(I+2, J) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2];

      /* dphi/dy_(I+1, J) */
      dphidy[1] = dphidys[index_+1];
      /* dphi/dy_(I+2, J) */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2];

      /* dphidz_(I+1,J,K) */
      dphidz[1] = dphidzs[index_+1];
      /* dphidz_(I+2,J,K) */
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_+2];

      /* x component of tangential flux */
      dphidS[0] = sxsx * dphidx[0] + sxsy * dphidy[0] + sxsz * dphidz[0];
      dphidS[1] = sxsx * dphidx[1] + sxsy * dphidy[1] + sxsz * dphidz[1];
      dphidS[2] = sxsx * dphidx[2] + sxsy * dphidy[2] + sxsz * dphidz[2];

      /* x component of tangential_2 flux */
      dphidS_2[0] = s2xs2x * dphidx[0] + s2xs2y * dphidy[0] + s2xs2z * dphidz[0];
      dphidS_2[1] = s2xs2x * dphidx[1] + s2xs2y * dphidy[1] + s2xs2z * dphidz[1];
      dphidS_2[2] = s2xs2x * dphidx[2] + s2xs2y * dphidy[2] + s2xs2z * dphidz[2];
	
      /* Lagrangian extrapolation to face */
      d = Delta_xW[index_];
      d10 = Delta_xE[index_];
      d21 = Delta_xE[index_+1];
      es = 1 - f01;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_face_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to face */
	dphidS_face_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_face_x = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_face_x = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_face_x;
      /* J_par_2 (only x component) */
      J_par_2[index_] = -gamma_a * dphidS_2_face_x;
      /* Constructing on the solid side */
      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_x[index_-1] = J_x[index_];
      
    }
    
    /* North (Fluid cell located at north position from solid domain) */
    if (N_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the N */
      nynx = norm_vec_y_y[index_+Nx] * norm_vec_y_x[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];
      nynz = norm_vec_y_y[index_+Nx] * norm_vec_y_z[index_+Nx];

      sysx = par_vec_y_y[index_+Nx] * par_vec_y_x[index_+Nx];
      sysy = par_vec_y_y[index_+Nx] * par_vec_y_y[index_+Nx];
      sysz = par_vec_y_y[index_+Nx] * par_vec_y_z[index_+Nx];

      s2ys2x = par_vec_y_y_2[index_+Nx] * par_vec_y_x_2[index_+Nx];
      s2ys2y = par_vec_y_y_2[index_+Nx] * par_vec_y_y_2[index_+Nx];
      s2ys2z = par_vec_y_y_2[index_+Nx] * par_vec_y_z_2[index_+Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_+Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the n face using values of phi across phases */
      dphidyface = (phi[index_+Nx] - phi[index_]) / Delta_yN[index_];

      f01 = fn[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;
      
      dphidNface = (dphidxface * nynx + dphidyface * nyny + dphidzface * nynz);
      
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J-1,K) */
      dphidx[1] = dphidxs[index_-Nx];
      /* dphi/dx_(I,J-2,K) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2*Nx];
      
      dphidy[1] = dphidys[index_-Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2*Nx];
      
      dphidz[1] = dphidzs[index_-Nx];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_-2*Nx];

      /* y component of tangential flux */
      dphidS[0] = sysx * dphidx[0] + sysy * dphidy[0] + sysz * dphidz[0];
      dphidS[1] = sysx * dphidx[1] + sysy * dphidy[1] + sysz * dphidz[1];
      dphidS[2] = sysx * dphidx[2] + sysy * dphidy[2] + sysz * dphidz[2];

      /* y component of tangential_2 flux */
      dphidS_2[0] = s2ys2x * dphidx[0] + s2ys2y * dphidy[0] + s2ys2z * dphidz[0];
      dphidS_2[1] = s2ys2x * dphidx[1] + s2ys2y * dphidy[1] + s2ys2z * dphidz[1];
      dphidS_2[2] = s2ys2x * dphidx[2] + s2ys2y * dphidy[2] + s2ys2z * dphidz[2];
      
      /* Lagrangian extrapolation to face */
      d = Delta_yN[index_];
      d10 = Delta_yS[index_];
      d21 = Delta_yS[index_-Nx];
      es = 1 - f01;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_face_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to face */
	dphidS_face_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_face_y = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_face_y = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }

      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_face_y;
      /* J_par_2 (only y component) */
      J_par_2[index_] = -gamma_a * dphidS_2_face_y;
      /* Constructing on the solid side */
      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_y[index_+Nx] = J_y[index_];
            
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (S_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the S */
      nynx = norm_vec_y_y[index_-Nx] * norm_vec_y_x[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];
      nynz = norm_vec_y_y[index_-Nx] * norm_vec_y_z[index_-Nx];

      sysx = par_vec_y_y[index_-Nx] * par_vec_y_x[index_-Nx];
      sysy = par_vec_y_y[index_-Nx] * par_vec_y_y[index_-Nx];
      sysz = par_vec_y_y[index_-Nx] * par_vec_y_z[index_-Nx];

      s2ys2x = par_vec_y_y_2[index_-Nx] * par_vec_y_x_2[index_-Nx];
      s2ys2y = par_vec_y_y_2[index_-Nx] * par_vec_y_y_2[index_-Nx];
      s2ys2z = par_vec_y_y_2[index_-Nx] * par_vec_y_z_2[index_-Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_-Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the s face using values of phi across phases */
      dphidyface = (phi[index_] - phi[index_-Nx]) / Delta_yS[index_];

      f01 = fs[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;
      
      dphidNface = (dphidxface * nynx + dphidyface * nyny + dphidzface * nynz);
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I, J+1) */
      dphidx[1] = dphidxs[index_+Nx];
      /* dphi/dx_(I, J+2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2*Nx];
      
      dphidy[1] = dphidys[index_+Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2*Nx];
      
      dphidz[1] = dphidzs[index_+Nx];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_+2*Nx];

      /* y component of tangential flux */
      dphidS[0] = sysx * dphidx[0] + sysy * dphidy[0] + sysz * dphidz[0];
      dphidS[1] = sysx * dphidx[1] + sysy * dphidy[1] + sysz * dphidz[1];
      dphidS[2] = sysx * dphidx[2] + sysy * dphidy[2] + sysz * dphidz[2];

      /* y component of tangential_2 flux */
      dphidS_2[0] = s2ys2x * dphidx[0] + s2ys2y * dphidy[0] + s2ys2z * dphidz[0];
      dphidS_2[1] = s2ys2x * dphidx[1] + s2ys2y * dphidy[1] + s2ys2z * dphidz[1];
      dphidS_2[2] = s2ys2x * dphidx[2] + s2ys2y * dphidy[2] + s2ys2z * dphidz[2];
            
      /* Lagrangian extrapolation to face */
      d = Delta_yS[index_];
      d10 = Delta_yN[index_];
      d21 = Delta_yN[index_+Nx];
      es = 1 - f01;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_face_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to face */
	dphidS_face_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_face_y = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_face_y = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }
      
      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_face_y;
      /* J_par_2 (only y component) */
      J_par_2[index_] = -gamma_a * dphidS_2_face_y;
      /* Constructing on the solid side */
      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_y[index_-Nx] = J_y[index_];
    }

    /* Top (Fluid cell located at top position from solid domain) */
    if (T_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the T */
      nznx = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_x[index_+Nx*Ny];
      nzny = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_y[index_+Nx*Ny];
      nznz = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_z[index_+Nx*Ny];

      szsx = par_vec_z_z[index_+Nx*Ny] * par_vec_z_x[index_+Nx*Ny];
      szsy = par_vec_z_z[index_+Nx*Ny] * par_vec_z_y[index_+Nx*Ny];
      szsz = par_vec_z_z[index_+Nx*Ny] * par_vec_z_z[index_+Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_x_2[index_+Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_y_2[index_+Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_z_2[index_+Nx*Ny];

      /* Volume fraction */
      alpha = v_fraction_z[index_+Nx*Ny];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the t face using values of phi across phases */
      dphidzface = (phi[index_+Nx*Ny] - phi[index_]) / Delta_zT[index_];

      f01 = ft[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+1], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphidNface = (dphidxface * nznx + dphidyface * nzny + dphidzface * nznz);

      /* J_norm (only z component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J,K-1) */
      dphidx[1] = dphidxs[index_-Nx*Ny];
      /* dphi/dx_(I,J,K-2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2*Nx*Ny];
      
      dphidy[1] = dphidys[index_-Nx*Ny];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2*Nx*Ny];
      
      dphidz[1] = dphidzs[index_-Nx*Ny];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_-2*Nx*Ny];

      /* z component of tangential flux */
      dphidS[0] = szsx * dphidx[0] + szsy * dphidy[0] + szsz * dphidz[0];
      dphidS[1] = szsx * dphidx[1] + szsy * dphidy[1] + szsz * dphidz[1];
      dphidS[2] = szsx * dphidx[2] + szsy * dphidy[2] + szsz * dphidz[2];

      /* z component of tangential_2 flux */
      dphidS_2[0] = s2zs2x * dphidx[0] + s2zs2y * dphidy[0] + s2zs2z * dphidz[0];
      dphidS_2[1] = s2zs2x * dphidx[1] + s2zs2y * dphidy[1] + s2zs2z * dphidz[1];
      dphidS_2[2] = s2zs2x * dphidx[2] + s2zs2y * dphidy[2] + s2zs2z * dphidz[2];
      
      /* Lagrangian extrapolation to face */
      d = Delta_zT[index_];
      d10 = Delta_zB[index_];
      d21 = Delta_zB[index_-Nx*Ny];
      es = 1 - f01;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_face_z = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to face */
	dphidS_face_z = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_face_z = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_face_z = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }
      
      /* J_par (only z component) */
      J_par[index_] = -gamma_a * dphidS_face_z;
      /* J_par_2 (only z component) */
      J_par_2[index_] = -gamma_a * dphidS_2_face_z;
      /* Constructing on the solid side */
      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_z[index_+Nx*Ny] = J_z[index_];
      
    }

    /* Bottom (Fluid cell located at bottom position from solid domain) */
    if (B_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the B */
      nznx = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_x[index_-Nx*Ny];
      nzny = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_y[index_-Nx*Ny];
      nznz = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_z[index_-Nx*Ny];

      szsx = par_vec_z_z[index_-Nx*Ny] * par_vec_z_x[index_-Nx*Ny];
      szsy = par_vec_z_z[index_-Nx*Ny] * par_vec_z_y[index_-Nx*Ny];
      szsz = par_vec_z_z[index_-Nx*Ny] * par_vec_z_z[index_-Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_x_2[index_-Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_y_2[index_-Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_z_2[index_-Nx*Ny];

      /* Volume fraction */
      alpha = v_fraction_z[index_-Nx*Ny];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the b face using values of phi across phases */
      dphidzface = (phi[index_] - phi[index_-Nx*Ny]) / Delta_zB[index_];

      f01 = fb[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_-Nx*Ny+1], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphidNface = (dphidxface * nznx + dphidyface * nzny + dphidzface * nznz);

      /* J_norm (only z component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J,K+1) */
      dphidx[1] = dphidxs[index_+Nx*Ny];
      /* dphi/dx_(I,J,K+2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2*Nx*Ny];
      
      dphidy[1] = dphidys[index_+Nx*Ny];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2*Nx*Ny];
      
      dphidz[1] = dphidzs[index_+Nx*Ny];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_+2*Nx*Ny];

      /* z component of tangential flux */
      dphidS[0] = szsx * dphidx[0] + szsy * dphidy[0] + szsz * dphidz[0];
      dphidS[1] = szsx * dphidx[1] + szsy * dphidy[1] + szsz * dphidz[1];
      dphidS[2] = szsx * dphidx[2] + szsy * dphidy[2] + szsz * dphidz[2];

      /* z component of tangential_2 flux */
      dphidS_2[0] = s2zs2x * dphidx[0] + s2zs2y * dphidy[0] + s2zs2z * dphidz[0];
      dphidS_2[1] = s2zs2x * dphidx[1] + s2zs2y * dphidy[1] + s2zs2z * dphidz[1];
      dphidS_2[2] = s2zs2x * dphidx[2] + s2zs2y * dphidy[2] + s2zs2z * dphidz[2];
      
      /* Lagrangian extrapolation to interface */
      /* Al copiar tener cuidado con indices y _x _xW */
      d = Delta_zB[index_];
      d10 = Delta_zT[index_];
      d21 = Delta_zT[index_+Nx*Ny];
      es = 1 - f01;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_face_z = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to face */
	dphidS_face_z = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_face_z = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_face_z = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }

      /* J_par (only z component) */
      J_par[index_] = -gamma_a * dphidS_face_z;
      /* J_par_2 (only z component) */
      J_par_2[index_] = -gamma_a * dphidS_2_face_z;
      /* Constructing on the solid side */
      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_z[index_-Nx*Ny] = J_z[index_];
      
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}



void debug_phi(Data_Mem *data, char c){
  
  int I, J, K;

  int index_;
 
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  double phi0, phi1;
  double m;

  double *phi = data->phi;
  
  const double Lx = data->Lx;
  const double Ly = data->Ly;
  const double Lz = data->Lz;
  
  const double *x = data->nodes_x;
  const double *y = data->nodes_y;
  const double *z = data->nodes_z;
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;

	if(c == 'x'){

	  phi0 = data->west.phi_b[0];
	  phi1 = data->east.phi_b[0];

	  m = (phi1 - phi0)/Lx;
	  
	  phi[index_] = m*x[index_] + phi0;
	  
	}
	
	if(c == 'y'){
		  
	  phi0 = data->south.phi_b[0];
	  phi1 = data->north.phi_b[0];

	  m = (phi1 - phi0)/Ly;
	  
	  phi[index_] = m*y[index_] + phi0;
	  
	}

	if(c == 'z'){
		  
	  phi0 = data->bottom.phi_b[0];
	  phi1 = data->top.phi_b[0];

	  m = (phi1 - phi0)/Lz;
	  
	  phi[index_] = m*z[index_] + phi0;
	  
	}
	
      }
    }
  }
  
  return;
  
}

/* COMPUTE_NEAR_BOUNDARY_CONVECTIVE_FLUX_3D */
/**
   For conjugate heat transfer and nodes that are neighbours of sf_boundary_solid_indexs
   
   Sets:
   
   Array (double, NxNyNz): Data_Mem::J_x: adds convective heat transport on x direction at interface
   to diffusive contribution calculated previously on function compute_near_boundary_molecular_flux_*.
   Array (double, NxNyNz): Data_Mem::J_y: idem to Data_Mem::J_x.
   Array (double, NxNyNz): Data_Mem::J_z: idem to Data_Mem::J_y.
   
   @param data

   @return void
   
   MODIFIED: 09-07-2020
*/
void compute_near_boundary_convective_flux_3D(Data_Mem *data){

  int I, J, K;
  int i, j, k;
  int count;
  int index_;
  int index_u, index_v, index_w;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int nx = data->nx;
  const int ny = data->ny;
  
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;
  
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *I_bm = data->I_bm;
  const int *J_bm = data->J_bm;
  const int *K_bm = data->K_bm;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  
  /* Fluid side gradient at the boundary or face for evaluation of phi at face */
  double dphidxf, dphidyf, dphidzf;
  double phie, phiw, phin, phis, phit, phib;

  /* Storing the derivative for TVD scheme computations */
  double *dphidxb = data->dphidxb;
  double *dphidyb = data->dphidyb;
  double *dphidzb = data->dphidzb;

  double *J_x = data->J_x;
  double *J_y = data->J_y;
  double *J_z = data->J_z;

  const double *phi = data->phi;

  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;
  const double *gamma_m = data->gamma_m;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;

  for(count=0; count<solid_boundary_cell_count; count++){

    index_ = sf_boundary_solid_indexs[count];

    I = I_bm[count];
    J = J_bm[count];
    K = K_bm[count];

    i = I-1;
    j = J-1;
    k = K-1;

    index_u = K*nx*Ny + J*nx + i;
    index_v = K*Nx*ny + j*Nx + I;
    index_w = k*Nx*Ny + J*Nx + I;

    if(E_is_fluid_OK[index_]){
      dphidxf = -J_x[index_] / (gamma_m[index_+1]);
      dphidxb[index_+1] = dphidxf;
      phie = phi[index_+1] - (Delta_x[index_+1] / 2) * dphidxf;
      J_x[index_+1] += rho_m[index_+1] * cp_m[index_+1] * u[index_u+1] * (phie - phi[index_+1]);
    }

    if(W_is_fluid_OK[index_]){
      dphidxf = -J_x[index_] / (gamma_m[index_-1]);
      dphidxb[index_-1] = dphidxf;
      phiw = phi[index_-1] + (Delta_x[index_-1] / 2) * dphidxf;
      J_x[index_-1] += rho_m[index_-1] * cp_m[index_-1] * u[index_u] * (phiw - phi[index_-1]);
    }

    if(N_is_fluid_OK[index_]){
      dphidyf = -J_y[index_] / (gamma_m[index_+Nx]);
      dphidyb[index_+Nx] = dphidyf;
      phin = phi[index_+Nx] - (Delta_y[index_+Nx] / 2) * dphidyf;
      J_y[index_+Nx] += rho_m[index_+Nx] * cp_m[index_+Nx] * v[index_v+Nx] * (phin - phi[index_+Nx]);
    }

    if(S_is_fluid_OK[index_]){
      dphidyf = -J_y[index_] / (gamma_m[index_-Nx]);
      dphidyb[index_-Nx] = dphidyf;
      phis = phi[index_-Nx] + (Delta_y[index_-Nx] / 2) * dphidyf;
      J_y[index_-Nx] += rho_m[index_-Nx] * cp_m[index_-Nx] * v[index_v] * (phis - phi[index_-Nx]);
    }

    if(T_is_fluid_OK[index_]){
      dphidzf = -J_z[index_] / (gamma_m[index_+Nx*Ny]); // gradient of fluid "felt" with gamma_f
      dphidzb[index_+Nx*Ny] = dphidzf;
      phit = phi[index_+Nx*Ny] - (Delta_z[index_+Nx*Ny] / 2) * dphidzf; // phi value at t face
      J_z[index_+Nx*Ny] += rho_m[index_+Nx*Ny] * cp_m[index_+Nx*Ny] * w[index_w+Nx*Ny] * (phit - phi[index_+Nx*Ny]);
    }

    if(B_is_fluid_OK[index_]){
      dphidzf = -J_z[index_] / (gamma_m[index_-Nx*Ny]);
      dphidzb[index_-Nx*Ny] = dphidzf;
      phib = phi[index_-Nx*Ny] + (Delta_z[index_-Nx*Ny] / 2) * dphidzf;
      J_z[index_-Nx*Ny] += rho_m[index_-Nx*Ny] * cp_m[index_-Nx*Ny] * w[index_w] * (phib - phi[index_-Nx*Ny]);
    }
    
  }

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_SATO_3D */
/**

   Original implementation of Sato et al. (2016) with correction on its Eqs (41)-(42) 
   since it must be multiplied by the aritmethic mean transport property (gamma_a) 
   and NOT the phase alone property.

   Uses the solid phase derivatives for increased stability.

   There is an apparent confusion since there are combination of magnitudes 
   evaluated at the face and the interface:
   - dphidNface evaluated at the face (ewnstb). 
   - dphidS_iface, evaluated at the INTERface with Eq 45 Sato (2016).
   - dphidS_2_iface, evaluated at the INTERface with Eq 45 Sato (2016).

   The derivatives stored in local arrays dphidx, dphidy, dphidz are in a solid domain.

   Sets:

   Array (double, NxNyNz): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNyNz): Data_Mem::J_par: diffusive flux in the direction parallel to the interface.
   Array (double, NxNyNz): Data_Mem::J_par2: diffusive flux in the direction parallel2 to the interface.
   Array (double, NxNyNz): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNyNz): Data_Mem::J_y: diffusive flux in y direction at interface.
   Array (double, NxNyNz): Data_Mem::J_z: diffusive flux in z direction at interface.
   
   @param data

   @see compute_solid_der_at_interface_3D
   
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_near_boundary_molecular_flux_Sato_3D(Data_Mem *data){

  char cx, cy, cz;

  const char *case_s_type_x = data->case_s_type_x;
  const char *case_s_type_y = data->case_s_type_y;
  const char *case_s_type_z = data->case_s_type_z;

  int count, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
    
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;
  
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;
  /* Fluid and Solid */
  double gamma_f, gamma_s;

  /* Volume fraction */
  double alpha = 0;

  double es;
  double es_x, es_y, es_z;
  double d, d10, d21;
  double L0, L1, L2;

  double dphi0, dphi1, f01;
  
  /* Products of normal vector components */
  double nxnx, nxny, nxnz, nynx, nyny, nynz, nznx, nzny, nznz;
  /* Products of parallel vector components */
  double sxsx, sxsy, sxsz, sysx, sysy, sysz, szsx, szsy, szsz;
  /* Products of parallel_2 vector components */
  double s2xs2x, s2xs2y, s2xs2z, s2ys2x, s2ys2y, s2ys2z, s2zs2x, s2zs2y, s2zs2z;

  /* These arrays hold derivatives at points within solid region away from interface,
     for instance if E_is_fluid_OK
     [0]: (I,J), [1]: (I-1,J), [2]: (I-2,J) 
  */
  double dphidx[3] = {0}, dphidy[3] = {0}, dphidz[3] = {0};
  double dphidS[3] = {0}, dphidS_2[3] = {0};
  
  /* Interface values of dphidS extrapolated from directions x, y, z, respectively 
     as dictated by Eq 45 Sato (2016) */
  double dphidS_iface_x, dphidS_iface_y, dphidS_iface_z;

  /* Interface values of dphidS_2 extrapolated from directions x, y, z, respectively 
     as dictated by Eq 45 Sato (2016) */
  double dphidS_2_iface_x, dphidS_2_iface_y, dphidS_2_iface_z;

  /* These are for estimating the normal gradient and are the derivatives at 
     the face where the curve intersects, for instance if E_is_fluid_OK, face == e.
     The derivatives are across phases.
  */
  double dphidxface, dphidyface, dphidzface;
  double dphidNface;

  /* Normal, parallel and parallel_2 vector */
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  double *J_par_2 = data->J_par_2;
  
  /* These are the molecular flux components measured in W/m2 if they were energy.
   * These are computed at the face ==> must compute velocity if applicable.
   * They are computed from the solid phase.
   * Must be stored in the solid and fluid nodes which share the interface.
   */
  
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  double *J_z = data->J_z;
  
  const double *dphidxs = data->dphidxs;
  const double *dphidys = data->dphidys;
  const double *dphidzs = data->dphidzs;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *fw = data->fw;
  const double *fs = data->fs;
  const double *fb = data->fb;

  const double *fe = data->fe;
  const double *fn = data->fn;
  const double *ft = data->ft;
  
  const double *v_fraction_x = data->v_fraction_x;
  const double *v_fraction_y = data->v_fraction_y;
  const double *v_fraction_z = data->v_fraction_z;
  
  const double *eps_x_sol = data->epsilon_x_sol;
  const double *eps_y_sol = data->epsilon_y_sol;
  const double *eps_z_sol = data->epsilon_z_sol;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_x_z = data->norm_vec_x_z;
  
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *norm_vec_y_z = data->norm_vec_y_z;

  const double *norm_vec_z_x = data->norm_vec_z_x;
  const double *norm_vec_z_y = data->norm_vec_z_y;
  const double *norm_vec_z_z = data->norm_vec_z_z;

  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_x_z = data->par_vec_x_z;
  
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  const double *par_vec_y_z = data->par_vec_y_z;
  
  const double *par_vec_z_x = data->par_vec_z_x;
  const double *par_vec_z_y = data->par_vec_z_y;
  const double *par_vec_z_z = data->par_vec_z_z;
  
  const double *par_vec_x_x_2 = data->par_vec_x_x_2;
  const double *par_vec_x_y_2 = data->par_vec_x_y_2;
  const double *par_vec_x_z_2 = data->par_vec_x_z_2;
  
  const double *par_vec_y_x_2 = data->par_vec_y_x_2;
  const double *par_vec_y_y_2 = data->par_vec_y_y_2;
  const double *par_vec_y_z_2 = data->par_vec_y_z_2;
  
  const double *par_vec_z_x_2 = data->par_vec_z_x_2;
  const double *par_vec_z_y_2 = data->par_vec_z_y_2;
  const double *par_vec_z_z_2 = data->par_vec_z_z_2;
  
  /* Compute derivatives at solid points near the interface */
  compute_solid_der_at_interface_3D(data);
  
  for(count=0; count<solid_boundary_cell_count; count++){
    
    index_ = sf_boundary_solid_indexs[count];

    cx = case_s_type_x[index_];
    cy = case_s_type_y[index_];
    cz = case_s_type_z[index_];

    es_x = eps_x_sol[index_];
    es_y = eps_y_sol[index_];
    es_z = eps_z_sol[index_];
    
    /* dphidx_(I,J,K) */
    dphidx[0] = dphidxs[index_];
    /* dphidy_(I,J,K) */
    dphidy[0] = dphidys[index_];
    /* dphidz_(I,J,K) */
    dphidz[0] = dphidzs[index_];
    
    /* East fluid cell ==> Internal boundary */
    if (E_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the E */
      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];
      nxnz = norm_vec_x_x[index_+1] * norm_vec_x_z[index_+1];

      sxsx = par_vec_x_x[index_+1] * par_vec_x_x[index_+1];
      sxsy = par_vec_x_x[index_+1] * par_vec_x_y[index_+1];
      sxsz = par_vec_x_x[index_+1] * par_vec_x_z[index_+1];

      s2xs2x = par_vec_x_x_2[index_+1] * par_vec_x_x_2[index_+1];
      s2xs2y = par_vec_x_x_2[index_+1] * par_vec_x_y_2[index_+1];
      s2xs2z = par_vec_x_x_2[index_+1] * par_vec_x_z_2[index_+1];

      /* Volume fraction */
      alpha = v_fraction_x[index_+1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	
      /* Gradient estimated at the e face using values of phi across phases */
      dphidxface = (phi[index_+1] - phi[index_]) / Delta_xE[index_];

      f01 = fe[index_];
      
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+1], phi[index_-Nx+1], Delta_yN[index_+1], Delta_yS[index_+1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+1], phi[index_+1], phi[index_-Nx*Ny+1], Delta_zT[index_+1], Delta_zB[index_+1]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;

      dphidNface = (dphidxface * nxnx + dphidyface * nxny + dphidzface * nxnz);
      
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi_dx_(I-1,J,K), cases abc */
      dphidx[1] = dphidxs[index_-1];
      /* dphi_dx_(I-2,J,K), cases ab */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2];
	
      /* dphidy_(I-1,J,K), cases abc */
      dphidy[1] = dphidys[index_-1];
      /* dphidy_(I-2,J,K), cases ab */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2];

      /* dphidz_(I-1,J,K), cases abc */
      dphidz[1] = dphidzs[index_-1];
      /* dphidz_(I-2,J,K), cases ab */
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_-2];

      /* The [2] will have NAN in the case of a 'c' */
      /* x component of tangential flux */
      dphidS[0] = sxsx * dphidx[0] + sxsy * dphidy[0] + sxsz * dphidz[0];
      dphidS[1] = sxsx * dphidx[1] + sxsy * dphidy[1] + sxsz * dphidz[1];
      dphidS[2] = sxsx * dphidx[2] + sxsy * dphidy[2] + sxsz * dphidz[2];

      /* x component of tangential_2 flux */
      dphidS_2[0] = s2xs2x * dphidx[0] + s2xs2y * dphidy[0] + s2xs2z * dphidz[0];
      dphidS_2[1] = s2xs2x * dphidx[1] + s2xs2y * dphidy[1] + s2xs2z * dphidz[1];
      dphidS_2[2] = s2xs2x * dphidx[2] + s2xs2y * dphidy[2] + s2xs2z * dphidz[2];

      /* Lagrangian extrapolation to interface */
      d = Delta_xE[index_];
      d10 = Delta_xW[index_];
      d21 = Delta_xW[index_-1];
      es = es_x;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_iface_x = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_iface_x = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }
      
      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_iface_x;
      /* J_par_2 (only x component) */
      J_par_2[index_] = -gamma_a * dphidS_2_iface_x;
      /* Constructing on the solid side */
      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_x[index_+1] = J_x[index_];
      
    }
    
    /* West fluid cell */
    if (W_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the W */
      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];
      nxnz = norm_vec_x_x[index_-1] * norm_vec_x_z[index_-1];

      sxsx = par_vec_x_x[index_-1] * par_vec_x_x[index_-1];
      sxsy = par_vec_x_x[index_-1] * par_vec_x_y[index_-1];
      sxsz = par_vec_x_x[index_-1] * par_vec_x_z[index_-1];

      s2xs2x = par_vec_x_x_2[index_-1] * par_vec_x_x_2[index_-1];
      s2xs2y = par_vec_x_x_2[index_-1] * par_vec_x_y_2[index_-1];
      s2xs2z = par_vec_x_x_2[index_-1] * par_vec_x_z_2[index_-1];

      /* Volume fraction */
      alpha = v_fraction_x[index_-1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the w face using values of phi across phases */
      dphidxface = (phi[index_] - phi[index_-1]) / Delta_xW[index_];

      f01 = fw[index_];
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx-1], phi[index_-1], phi[index_-Nx-1], Delta_yN[index_-1], Delta_yS[index_-1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny-1], phi[index_-1], phi[index_-Nx*Ny-1], Delta_zT[index_-1], Delta_zB[index_-1]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;
      
      dphidNface = (dphidxface * nxnx + dphidyface * nxny + dphidzface * nxnz);
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I+1, J) */
      dphidx[1] = dphidxs[index_+1];
      /* dphi/dx_(I+2, J) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2];

      /* dphi/dy_(I+1, J) */
      dphidy[1] = dphidys[index_+1];
      /* dphi/dy_(I+2, J) */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2];

      /* dphidz_(I+1,J,K) */
      dphidz[1] = dphidzs[index_+1];
      /* dphidz_(I+2,J,K) */
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_+2];

      /* x component of tangential flux */
      dphidS[0] = sxsx * dphidx[0] + sxsy * dphidy[0] + sxsz * dphidz[0];
      dphidS[1] = sxsx * dphidx[1] + sxsy * dphidy[1] + sxsz * dphidz[1];
      dphidS[2] = sxsx * dphidx[2] + sxsy * dphidy[2] + sxsz * dphidz[2];

      /* x component of tangential_2 flux */
      dphidS_2[0] = s2xs2x * dphidx[0] + s2xs2y * dphidy[0] + s2xs2z * dphidz[0];
      dphidS_2[1] = s2xs2x * dphidx[1] + s2xs2y * dphidy[1] + s2xs2z * dphidz[1];
      dphidS_2[2] = s2xs2x * dphidx[2] + s2xs2y * dphidy[2] + s2xs2z * dphidz[2];
	
      /* Lagrangian extrapolation to interface */
      d = Delta_xW[index_];
      d10 = Delta_xE[index_];
      d21 = Delta_xE[index_+1];
      es = es_x;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_iface_x = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_iface_x = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_iface_x;
      /* J_par_2 (only x component) */
      J_par_2[index_] = -gamma_a * dphidS_2_iface_x;
      /* Constructing on the solid side */
      J_x[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_x[index_-1] = J_x[index_];
      
    }
    
    /* North (Fluid cell located at north position from solid domain) */
    if (N_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the N */
      nynx = norm_vec_y_y[index_+Nx] * norm_vec_y_x[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];
      nynz = norm_vec_y_y[index_+Nx] * norm_vec_y_z[index_+Nx];

      sysx = par_vec_y_y[index_+Nx] * par_vec_y_x[index_+Nx];
      sysy = par_vec_y_y[index_+Nx] * par_vec_y_y[index_+Nx];
      sysz = par_vec_y_y[index_+Nx] * par_vec_y_z[index_+Nx];

      s2ys2x = par_vec_y_y_2[index_+Nx] * par_vec_y_x_2[index_+Nx];
      s2ys2y = par_vec_y_y_2[index_+Nx] * par_vec_y_y_2[index_+Nx];
      s2ys2z = par_vec_y_y_2[index_+Nx] * par_vec_y_z_2[index_+Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_+Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the n face using values of phi across phases */
      dphidyface = (phi[index_+Nx] - phi[index_]) / Delta_yN[index_];

      f01 = fn[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;
      
      dphidNface = (dphidxface * nynx + dphidyface * nyny + dphidzface * nynz);
      
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J-1,K) */
      dphidx[1] = dphidxs[index_-Nx];
      /* dphi/dx_(I,J-2,K) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2*Nx];
      
      dphidy[1] = dphidys[index_-Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2*Nx];
      
      dphidz[1] = dphidzs[index_-Nx];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_-2*Nx];

      /* y component of tangential flux */
      dphidS[0] = sysx * dphidx[0] + sysy * dphidy[0] + sysz * dphidz[0];
      dphidS[1] = sysx * dphidx[1] + sysy * dphidy[1] + sysz * dphidz[1];
      dphidS[2] = sysx * dphidx[2] + sysy * dphidy[2] + sysz * dphidz[2];

      /* y component of tangential_2 flux */
      dphidS_2[0] = s2ys2x * dphidx[0] + s2ys2y * dphidy[0] + s2ys2z * dphidz[0];
      dphidS_2[1] = s2ys2x * dphidx[1] + s2ys2y * dphidy[1] + s2ys2z * dphidz[1];
      dphidS_2[2] = s2ys2x * dphidx[2] + s2ys2y * dphidy[2] + s2ys2z * dphidz[2];
      
      /* Lagrangian extrapolation to interface */
      d = Delta_yN[index_];
      d10 = Delta_yS[index_];
      d21 = Delta_yS[index_-Nx];
      es = es_y;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_iface_y = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_iface_y = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }

      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_iface_y;
      /* J_par_2 (only y component) */
      J_par_2[index_] = -gamma_a * dphidS_2_iface_y;
      /* Constructing on the solid side */
      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_y[index_+Nx] = J_y[index_];
            
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (S_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the S */
      nynx = norm_vec_y_y[index_-Nx] * norm_vec_y_x[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];
      nynz = norm_vec_y_y[index_-Nx] * norm_vec_y_z[index_-Nx];

      sysx = par_vec_y_y[index_-Nx] * par_vec_y_x[index_-Nx];
      sysy = par_vec_y_y[index_-Nx] * par_vec_y_y[index_-Nx];
      sysz = par_vec_y_y[index_-Nx] * par_vec_y_z[index_-Nx];

      s2ys2x = par_vec_y_y_2[index_-Nx] * par_vec_y_x_2[index_-Nx];
      s2ys2y = par_vec_y_y_2[index_-Nx] * par_vec_y_y_2[index_-Nx];
      s2ys2z = par_vec_y_y_2[index_-Nx] * par_vec_y_z_2[index_-Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_-Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the s face using values of phi across phases */
      dphidyface = (phi[index_] - phi[index_-Nx]) / Delta_yS[index_];

      f01 = fs[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);

      dphidzface = (1 - f01)*dphi1 + f01*dphi0;
      
      dphidNface = (dphidxface * nynx + dphidyface * nyny + dphidzface * nynz);
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I, J+1) */
      dphidx[1] = dphidxs[index_+Nx];
      /* dphi/dx_(I, J+2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2*Nx];
      
      dphidy[1] = dphidys[index_+Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2*Nx];
      
      dphidz[1] = dphidzs[index_+Nx];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_+2*Nx];

      /* y component of tangential flux */
      dphidS[0] = sysx * dphidx[0] + sysy * dphidy[0] + sysz * dphidz[0];
      dphidS[1] = sysx * dphidx[1] + sysy * dphidy[1] + sysz * dphidz[1];
      dphidS[2] = sysx * dphidx[2] + sysy * dphidy[2] + sysz * dphidz[2];

      /* y component of tangential_2 flux */
      dphidS_2[0] = s2ys2x * dphidx[0] + s2ys2y * dphidy[0] + s2ys2z * dphidz[0];
      dphidS_2[1] = s2ys2x * dphidx[1] + s2ys2y * dphidy[1] + s2ys2z * dphidz[1];
      dphidS_2[2] = s2ys2x * dphidx[2] + s2ys2y * dphidy[2] + s2ys2z * dphidz[2];
            
      /* Lagrangian extrapolation to interface */
      d = Delta_yS[index_];
      d10 = Delta_yN[index_];
      d21 = Delta_yN[index_+Nx];
      es = es_y;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_iface_y = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_iface_y = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }
      
      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_iface_y;
      /* J_par_2 (only y component) */
      J_par_2[index_] = -gamma_a * dphidS_2_iface_y;
      /* Constructing on the solid side */
      J_y[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_y[index_-Nx] = J_y[index_];
    }

    /* Top (Fluid cell located at top position from solid domain) */
    if (T_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the T */
      nznx = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_x[index_+Nx*Ny];
      nzny = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_y[index_+Nx*Ny];
      nznz = norm_vec_z_z[index_+Nx*Ny] * norm_vec_z_z[index_+Nx*Ny];

      szsx = par_vec_z_z[index_+Nx*Ny] * par_vec_z_x[index_+Nx*Ny];
      szsy = par_vec_z_z[index_+Nx*Ny] * par_vec_z_y[index_+Nx*Ny];
      szsz = par_vec_z_z[index_+Nx*Ny] * par_vec_z_z[index_+Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_x_2[index_+Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_y_2[index_+Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_+Nx*Ny] * par_vec_z_z_2[index_+Nx*Ny];

      /* Volume fraction */
      alpha = v_fraction_z[index_+Nx*Ny];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the t face using values of phi across phases */
      dphidzface = (phi[index_+Nx*Ny] - phi[index_]) / Delta_zT[index_];

      f01 = ft[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+1], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphidNface = (dphidxface * nznx + dphidyface * nzny + dphidzface * nznz);

      /* J_norm (only z component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J,K-1) */
      dphidx[1] = dphidxs[index_-Nx*Ny];
      /* dphi/dx_(I,J,K-2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2*Nx*Ny];
      
      dphidy[1] = dphidys[index_-Nx*Ny];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2*Nx*Ny];
      
      dphidz[1] = dphidzs[index_-Nx*Ny];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_-2*Nx*Ny];

      /* z component of tangential flux */
      dphidS[0] = szsx * dphidx[0] + szsy * dphidy[0] + szsz * dphidz[0];
      dphidS[1] = szsx * dphidx[1] + szsy * dphidy[1] + szsz * dphidz[1];
      dphidS[2] = szsx * dphidx[2] + szsy * dphidy[2] + szsz * dphidz[2];

      /* z component of tangential_2 flux */
      dphidS_2[0] = s2zs2x * dphidx[0] + s2zs2y * dphidy[0] + s2zs2z * dphidz[0];
      dphidS_2[1] = s2zs2x * dphidx[1] + s2zs2y * dphidy[1] + s2zs2z * dphidz[1];
      dphidS_2[2] = s2zs2x * dphidx[2] + s2zs2y * dphidy[2] + s2zs2z * dphidz[2];
      
      /* Lagrangian extrapolation to interface */
      d = Delta_zT[index_];
      d10 = Delta_zB[index_];
      d21 = Delta_zB[index_-Nx*Ny];
      es = es_z;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_z = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_z = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_iface_z = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_iface_z = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }
      
      /* J_par (only z component) */
      J_par[index_] = -gamma_a * dphidS_iface_z;
      /* J_par_2 (only z component) */
      J_par_2[index_] = -gamma_a * dphidS_2_iface_z;
      /* Constructing on the solid side */
      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_z[index_+Nx*Ny] = J_z[index_];
      
    }

    /* Bottom (Fluid cell located at bottom position from solid domain) */
    if (B_is_fluid_OK[index_]){

      /* Since magnitudes are stored in the fluid cells, index_ is shifted to the B */
      nznx = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_x[index_-Nx*Ny];
      nzny = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_y[index_-Nx*Ny];
      nznz = norm_vec_z_z[index_-Nx*Ny] * norm_vec_z_z[index_-Nx*Ny];

      szsx = par_vec_z_z[index_-Nx*Ny] * par_vec_z_x[index_-Nx*Ny];
      szsy = par_vec_z_z[index_-Nx*Ny] * par_vec_z_y[index_-Nx*Ny];
      szsz = par_vec_z_z[index_-Nx*Ny] * par_vec_z_z[index_-Nx*Ny];

      s2zs2x = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_x_2[index_-Nx*Ny];
      s2zs2y = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_y_2[index_-Nx*Ny];
      s2zs2z = par_vec_z_z_2[index_-Nx*Ny] * par_vec_z_z_2[index_-Nx*Ny];

      /* Volume fraction */
      alpha = v_fraction_z[index_-Nx*Ny];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx*Ny];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* Gradient estimated at the b face using values of phi across phases */
      dphidzface = (phi[index_] - phi[index_-Nx*Ny]) / Delta_zB[index_];

      f01 = fb[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_-Nx*Ny+1], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;

      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;

      dphidNface = (dphidxface * nznx + dphidyface * nzny + dphidzface * nznz);

      /* J_norm (only z component) */
      J_norm[index_] = -gamma_h * dphidNface;

      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J,K+1) */
      dphidx[1] = dphidxs[index_+Nx*Ny];
      /* dphi/dx_(I,J,K+2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2*Nx*Ny];
      
      dphidy[1] = dphidys[index_+Nx*Ny];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2*Nx*Ny];
      
      dphidz[1] = dphidzs[index_+Nx*Ny];
      dphidz[2] = (cz == 'c') ? NAN : dphidzs[index_+2*Nx*Ny];

      /* z component of tangential flux */
      dphidS[0] = szsx * dphidx[0] + szsy * dphidy[0] + szsz * dphidz[0];
      dphidS[1] = szsx * dphidx[1] + szsy * dphidy[1] + szsz * dphidz[1];
      dphidS[2] = szsx * dphidx[2] + szsy * dphidy[2] + szsz * dphidz[2];

      /* z component of tangential_2 flux */
      dphidS_2[0] = s2zs2x * dphidx[0] + s2zs2y * dphidy[0] + s2zs2z * dphidz[0];
      dphidS_2[1] = s2zs2x * dphidx[1] + s2zs2y * dphidy[1] + s2zs2z * dphidz[1];
      dphidS_2[2] = s2zs2x * dphidx[2] + s2zs2y * dphidy[2] + s2zs2z * dphidz[2];
      
      /* Lagrangian extrapolation to interface */
      /* Al copiar tener cuidado con indices y _x _xW */
      d = Delta_zB[index_];
      d10 = Delta_zT[index_];
      d21 = Delta_zT[index_+Nx*Ny];
      es = es_z;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_z = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_z = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      if( isnan(dphidS_2[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;   
	
	dphidS_2_iface_z = L0 * dphidS_2[0] + L1 * dphidS_2[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	dphidS_2_iface_z = L0 * dphidS_2[0] + L1 * dphidS_2[1] + L2 * dphidS_2[2];
	
      }

      /* J_par (only z component) */
      J_par[index_] = -gamma_a * dphidS_iface_z;
      /* J_par_2 (only z component) */
      J_par_2[index_] = -gamma_a * dphidS_2_iface_z;
      /* Constructing on the solid side */
      J_z[index_] = J_norm[index_] + J_par[index_] + J_par_2[index_];
      /* Coupling on the fluid side */
      J_z[index_-Nx*Ny] = J_z[index_];
      
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_SOLID_DER_AT_INTERFACE_3D */
/**
   Compute derivatives at solid points near the interface
   
   Sets:

   Array (double, NxNyNz): Data_Mem::dphidxs: derivatives in x direction up to two points within solid body, using only values of phi on solid domain.
   Array (double, NxNyNz): Data_Mem::dphidys: idem y.
   Array (double, NxNyNz): Data_Mem::dphidzs: idem z.
   
   @param data
   
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_solid_der_at_interface_3D(Data_Mem *data){
  
  char cx, cy, cz;

  const char *case_s_type_x = data->case_s_type_x;
  const char *case_s_type_y = data->case_s_type_y;
  const char *case_s_type_z = data->case_s_type_z;

  int count, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;

  double *dphidxs = data->dphidxs;
  double *dphidys = data->dphidys;
  double *dphidzs = data->dphidzs;
  
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *phi = data->phi;
  
  for(count=0; count<solid_boundary_cell_count; count++){
    
    index_ = sf_boundary_solid_indexs[count];
    
    // need the case type see where we are and how many der can we safely compute
    cx = case_s_type_x[index_];
    cy = case_s_type_y[index_];
    cz = case_s_type_z[index_];
    
    /* EW Block */
    if(cx == 'a'){
      if(E_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1], 1);
	dphidxs[index_-1] = __central_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1]);
	dphidxs[index_-2] = __central_der(phi[index_-1], phi[index_-2], phi[index_-3], Delta_xW[index_-1], Delta_xW[index_-2]);
      }
      else if(W_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_+1], phi[index_+2], Delta_xE[index_], Delta_xE[index_+1], -1);
	dphidxs[index_+1] = __central_der(phi[index_+2], phi[index_+1], phi[index_], Delta_xE[index_+1], Delta_xE[index_]);
	dphidxs[index_+2] = __central_der(phi[index_+3], phi[index_+2], phi[index_+1], Delta_xE[index_+2], Delta_xE[index_+1]);
      }
      else{
	dphidxs[index_] = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      }
    }

    if(cx == 'b'){
      if(E_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1], 1);
	dphidxs[index_-1] = __central_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1]);
	dphidxs[index_-2] = (phi[index_-1] - phi[index_-2]) / Delta_xW[index_-1];
      }
      else if(W_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_+1], phi[index_+2], Delta_xE[index_], Delta_xE[index_+1], -1);
	dphidxs[index_+1] = __central_der(phi[index_+2], phi[index_+1], phi[index_], Delta_xE[index_+1], Delta_xE[index_]);
	dphidxs[index_+2] = (phi[index_+2] - phi[index_+1]) / Delta_xE[index_+1];
      }
      else{
	dphidxs[index_] = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      }
    }
    
    if(cx == 'c'){
      if(E_is_fluid_OK[index_]){
	dphidxs[index_] = (phi[index_] - phi[index_-1]) / Delta_xW[index_];
      }
      else if(W_is_fluid_OK[index_]){
	dphidxs[index_] = (phi[index_+1] - phi[index_]) / Delta_xE[index_];
      }
      else{
	dphidxs[index_] = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      }
    }
    
    /* N-S Block */
    if(cy == 'a'){
      if(N_is_fluid_OK[index_]){
	dphidys[index_] = __asymm_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx], 1);
	dphidys[index_-Nx] = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx]);
	dphidys[index_-2*Nx] = __central_der(phi[index_-Nx], phi[index_-2*Nx], phi[index_-3*Nx], Delta_yS[index_-Nx], Delta_yS[index_-2*Nx]);
      }
      else if(S_is_fluid_OK[index_]){
	dphidys[index_] = __asymm_der(phi[index_], phi[index_+Nx], phi[index_+2*Nx], Delta_yN[index_], Delta_yN[index_+Nx], -1);
	dphidys[index_+Nx] = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_], Delta_yN[index_+Nx], Delta_yN[index_]);
	dphidys[index_+2*Nx] = __central_der(phi[index_+3*Nx], phi[index_+2*Nx], phi[index_+Nx], Delta_yN[index_+2*Nx], Delta_yN[index_+Nx]);
      }
      else{
	dphidys[index_] = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      }
    }

    if(cy == 'b'){
      if(N_is_fluid_OK[index_]){
	dphidys[index_] = __asymm_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx], 1);
	dphidys[index_-Nx] = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx]);
	dphidys[index_-2*Nx] = (phi[index_-Nx] - phi[index_-2*Nx]) / Delta_yS[index_-Nx];
      }
      else if(S_is_fluid_OK[index_]){
	/* Eq 44 Sato */
	dphidys[index_] = __asymm_der(phi[index_], phi[index_+Nx], phi[index_+2*Nx], Delta_yN[index_], Delta_yN[index_+Nx], -1);
	dphidys[index_+Nx] = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_], Delta_yN[index_+Nx], Delta_yN[index_]);
	dphidys[index_+2*Nx] = (phi[index_+2*Nx] - phi[index_+Nx]) / Delta_yN[index_+Nx];
      }
      else{
	dphidys[index_] = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      }
    }

    if(cy == 'c'){
      if(N_is_fluid_OK[index_]){
	dphidys[index_] = (phi[index_] - phi[index_-Nx]) / Delta_yS[index_];
      }
      else if(S_is_fluid_OK[index_]){
	dphidys[index_] = (phi[index_+Nx] - phi[index_]) / Delta_yN[index_];
      }
      else{
	dphidys[index_] = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      }
    }

    /* T-B Block */
    if(cz == 'a'){
      if(T_is_fluid_OK[index_]){
	dphidzs[index_] = __asymm_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny], Delta_zB[index_], Delta_zB[index_-Nx*Ny], 1);
	dphidzs[index_-Nx*Ny] = __central_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny], Delta_zB[index_], Delta_zB[index_-Nx*Ny]);
	dphidzs[index_-2*Nx*Ny] = __central_der(phi[index_-Nx*Ny], phi[index_-2*Nx*Ny], phi[index_-3*Nx*Ny], Delta_zB[index_-Nx*Ny], Delta_zB[index_-2*Nx*Ny]);
      }
      else if(B_is_fluid_OK[index_]){
	dphidzs[index_] = __asymm_der(phi[index_], phi[index_+Nx*Ny], phi[index_+2*Nx*Ny], Delta_zT[index_], Delta_zT[index_+Nx*Ny], -1);
	dphidzs[index_+Nx*Ny] = __central_der(phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], phi[index_], Delta_zT[index_+Nx*Ny], Delta_zT[index_]);
	dphidzs[index_+2*Nx*Ny] = __central_der(phi[index_+3*Nx*Ny], phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], Delta_zT[index_+2*Nx*Ny], Delta_zT[index_+Nx*Ny]);	
      }
      else{
	dphidzs[index_] = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      }
    }

    if(cz == 'b'){
      if(T_is_fluid_OK[index_]){
	dphidzs[index_] = __asymm_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny], Delta_zB[index_], Delta_zB[index_-Nx*Ny], 1);
	dphidzs[index_-Nx*Ny] = __central_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny], Delta_zB[index_], Delta_zB[index_-Nx*Ny]);
	dphidzs[index_-2*Nx*Ny] = (phi[index_-Nx*Ny] - phi[index_-2*Nx*Ny]) / Delta_zB[index_-Nx*Ny];
      }
      else if(B_is_fluid_OK[index_]){
	dphidzs[index_] = __asymm_der(phi[index_], phi[index_+Nx*Ny], phi[index_+2*Nx*Ny], Delta_zT[index_], Delta_zT[index_+Nx*Ny], -1);
	dphidzs[index_+Nx*Ny] = __central_der(phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], phi[index_], Delta_zT[index_+Nx*Ny], Delta_zT[index_]);
	dphidzs[index_+2*Nx*Ny] = (phi[index_+2*Nx*Ny] - phi[index_+Nx*Ny]) / Delta_zT[index_+Nx*Ny];
      }
      else{
	dphidzs[index_] = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      }
    }
    
    if(cz == 'c'){
      if(T_is_fluid_OK[index_]){
	dphidzs[index_] = (phi[index_] - phi[index_-Nx*Ny]) / Delta_zB[index_];
      }
      else if(B_is_fluid_OK[index_]){
	dphidzs[index_] = (phi[index_+Nx*Ny] - phi[index_]) / Delta_zT[index_];
      }
      else{
	dphidzs[index_] = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
      }
    }
    
  } /* end for(I=0; I<solid_boundary_cell_count; I++) */
  
  return;
}

/* COMPUTE_PHI_SC_AT_INTERFACE_ISOFLUX_3D */
/*****************************************************************************/
/**

   Sets:

   Array (double, turb_boundary_cell_count): Data_Mem::SC_vol_eps_turb: Source term due to solid boundary for Data_Mem::eps_turb equation.

   @param data

   @return void

   MODIFIED: 08-04-2020
*/
void compute_phi_SC_at_interface_Isoflux_3D(Data_Mem *data){
	
  int i, j, k;
  int I, J, K;
  int index_;
  int index_u, index_v, index_w;
  int iter_count, count;
  int cell_done_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int nx = data->nx;
  const int ny = data->ny;

  const int iter_total = 10;

  const int Isoflux_boundary_cell_count =
    data->Isoflux_boundary_cell_count;

  const int Dirichlet_Isoflux_convective_boundary_OK = data->Dirichlet_Isoflux_convective_boundary_OK;
  
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  const int *T_solve_phase_OK = data->T_solve_phase_OK;
  const int *B_solve_phase_OK = data->B_solve_phase_OK;
  
  const int *dom_mat = data->domain_matrix;

  const int *I_Isoflux_v = data->I_Isoflux_v;
  const int *J_Isoflux_v = data->J_Isoflux_v;
  const int *K_Isoflux_v = data->K_Isoflux_v;
    
  const int *Isoflux_boundary_indexs =
    data->Isoflux_boundary_indexs;

  double rho_cp, gamma;

  /* Correction factor due to different lengths */
  double mx = 0, my = 0, mz = 0;
  double eps_val, Da;

  double dphidNb;
  
  double dphidx_P, dphidx_W, dphidx_E, dphidx_S, dphidx_N, dphidx_B, dphidx_T;
  double dphidy_P, dphidy_W, dphidy_E, dphidy_S, dphidy_N, dphidy_B, dphidy_T;  
  double dphidz_P, dphidz_W, dphidz_E, dphidz_S, dphidz_N, dphidz_B, dphidz_T;
  double dphidS_P, dphidS_W, dphidS_E, dphidS_S, dphidS_N, dphidS_B, dphidS_T;
  
  double dphidS_2_P;
  double dphidS_2_W, dphidS_2_E;
  double dphidS_2_S, dphidS_2_N;
  double dphidS_2_B, dphidS_2_T;
  
  double dphidSface_x, dphidS_2face_x;
  double dphidSface_y, dphidS_2face_y;
  double dphidSface_z, dphidS_2face_z;
  
  double phi_m_ew = 0, phi_m_ns = 0, phi_m_tb = 0;
  /* These are fluxes that need to be multiplied by corresponding area to enter SC_m */
  double Jxmol, Jxconv, Jx;
  double Jymol, Jyconv, Jy;
  double Jzmol, Jzconv, Jz;
  /* Same here */
  double Few = 0, Fns = 0, Ftb = 0;

  /* Isoflux derivatives */
  double *dphidxb = data->dphidxb;
  double *dphidyb = data->dphidyb;
  double *dphidzb = data->dphidzb;
  
  double *SC_vol_m = data->SC_vol_m;

  const double *dphidNb_v = data->dphidNb_v;
  
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;

  const double *fw = data->fw;
  const double *fs = data->fs;
  const double *fb = data->fb;

  const double *fe = data->fe;
  const double *fn = data->fn;
  const double *ft = data->ft;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;
  
  const double *phi = data->phi;

  const double *cp_m = data->cp_m;
  const double *rho_m = data->rho_m;
  const double *gamma_m = data->gamma_m;
  
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;
  
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;

  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *norm_vec_z_z = data->norm_vec_z_z;
  
  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_x_z = data->par_vec_x_z;
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  const double *par_vec_y_z = data->par_vec_y_z;  
  const double *par_vec_z_x = data->par_vec_z_x;
  const double *par_vec_z_y = data->par_vec_z_y;
  const double *par_vec_z_z = data->par_vec_z_z;
  
  const double *par_vec_x_x_2 = data->par_vec_x_x_2;
  const double *par_vec_x_y_2 = data->par_vec_x_y_2;
  const double *par_vec_x_z_2 = data->par_vec_x_z_2;
  const double *par_vec_y_x_2 = data->par_vec_y_x_2;
  const double *par_vec_y_y_2 = data->par_vec_y_y_2;
  const double *par_vec_y_z_2 = data->par_vec_y_z_2;  
  const double *par_vec_z_x_2 = data->par_vec_z_x_2;
  const double *par_vec_z_y_2 = data->par_vec_z_y_2;
  const double *par_vec_z_z_2 = data->par_vec_z_z_2;

  for(iter_count=0; iter_count<iter_total; iter_count++){

    for(count=0; count<Isoflux_boundary_cell_count; count++){

      index_ = Isoflux_boundary_indexs[count];
    
      I = I_Isoflux_v[count];
      J = J_Isoflux_v[count];
      K = K_Isoflux_v[count];
    	
      i = I - 1;
      j = J - 1;
      k = K - 1;
    
      index_u = K*nx*Ny + J*nx + i;
      index_v = K*Nx*ny + j*Nx + I;
      index_w = k*Nx*Ny + J*Nx + I;

      cell_done_OK = 0;

      rho_cp = rho_m[index_] * cp_m[index_];
      gamma = gamma_m[index_];

      dphidx_P = 0;
      dphidx_W = 0;
      dphidx_E = 0;
      dphidx_S = 0;
      dphidx_N = 0;
      dphidx_B = 0;
      dphidx_T = 0;
      dphidy_P = 0;
      dphidy_W = 0;
      dphidy_E = 0;
      dphidy_S = 0;
      dphidy_N = 0;
      dphidy_B = 0;
      dphidy_T = 0;	
      dphidz_P = 0;
      dphidz_W = 0;
      dphidz_E = 0;
      dphidz_S = 0;
      dphidz_N = 0;
      dphidz_B = 0;
      dphidz_T = 0;
	
      /* E-P line intersects with curve */
      if(!E_solve_phase_OK[index_] && !cell_done_OK){ // E

	cell_done_OK = 1;
	dphidx_P = __central_b_der(eps_x[index_], dphidxb[index_], phi[index_], phi[index_-1],
				   Delta_xE[index_], Delta_xW[index_]);

	dphidx_W = __central_der(phi[index_], phi[index_-1], phi[index_-2],
				 Delta_xE[index_-1], Delta_xW[index_-1]);

	/* Case E-N */
	if(!N_solve_phase_OK[index_]){ // N

	  /* Derivatives required for N case at South position:
	     dphidx_S, dphidy_S, dphidz_S */
	  if(!E_solve_phase_OK[index_-Nx]){ // SE
	      
	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx], phi[index_-Nx-1],
				       Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }
	  else if(!W_solve_phase_OK[index_-Nx]){ // SW

	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx+1], phi[index_-Nx],
				       Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	  }
	  else{
	      
	    dphidx_S = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1],
				     Delta_xE[index_-Nx],  Delta_xW[index_-Nx]);
	  }

	  dphidy_S = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx],
				   Delta_yN[index_-Nx], Delta_yS[index_-Nx]);

	  if(!T_solve_phase_OK[index_-Nx]){ // TS

	    dphidz_S = __central_b_der(eps_z[index_-Nx], dphidzb[index_-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx],
				       Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	  }
	  else if(!B_solve_phase_OK[index_-Nx]){ // BS

	    dphidz_S = __central_b_der(eps_z[index_-Nx], dphidzb[index_-Nx], phi[index_+Nx*Ny-Nx], phi[index_-Nx],
				       Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	  }
	  else{

	    dphidz_S = __central_der(phi[index_+Nx*Ny-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx],
				     Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	  }
	}

	/* Case E-S */
	if(!S_solve_phase_OK[index_]){ // S

	  /* Derivatives required for S case at North position:
	     dphidx_N, dphidy_N, dphidz_N */	    
	  if(!E_solve_phase_OK[index_+Nx]){ // NE

	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx], phi[index_+Nx-1],
				       Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }
	  else if(!W_solve_phase_OK[index_+Nx]){ // NW

	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx+1], phi[index_+Nx],
				       Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	  }
	  else{
	    dphidx_N = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1],
				     Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }

	  dphidy_N = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_],
				   Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

	  if(!T_solve_phase_OK[index_+Nx]){ // TN

	    dphidz_N = __central_b_der(eps_z[index_+Nx], dphidzb[index_+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx],
				       Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	  }
	  else if(!B_solve_phase_OK[index_+Nx]){ // BN

	    dphidz_N = __central_b_der(eps_z[index_+Nx], dphidzb[index_+Nx], phi[index_+Nx*Ny+Nx], phi[index_+Nx],
				       Delta_zB[index_+Nx], Delta_zT[index_+Nx]);
	  }
	  else{

	    dphidz_N = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx],
				     Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	  }
	  
	}

	/* Case E-T */
	if(!T_solve_phase_OK[index_]){ // T

	  /* Derivatives required for T case at Bottom position:
	     dphidx_B, dphidy_B, dphidz_B */
	  if(!E_solve_phase_OK[index_-Nx*Ny]){ // BE

	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }
	  else if(!W_solve_phase_OK[index_-Nx*Ny]){ // BW

	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny+1], phi[index_-Nx*Ny],
				       Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	  }
	  else{

	    dphidx_B = __central_der(phi[index_-Nx*Ny+1], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				     Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }
	  
	  if(!B_solve_phase_OK[index_+Nx]){ // BN

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }
	  else if(!B_solve_phase_OK[index_-Nx]){ // BS

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny],
				       Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	  }
	  else{

	    dphidy_B = __central_der(phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				     Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }

	  dphidz_B = __central_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny],
				   Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	}

	/* Case E-B */
	if(!B_solve_phase_OK[index_]){ // B
	  
	  /* Derivatives required for B case at Top position:
	     dphidx_T, dphidy_T, dphidz_T */
	  if(!E_solve_phase_OK[index_+Nx*Ny]){ // TE

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }
	  else if(!W_solve_phase_OK[index_+Nx*Ny]){ // TW

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny+1], phi[index_+Nx*Ny],
				       Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	  }
	  else{
	      
	    dphidx_T = __central_der(phi[index_+Nx*Ny+1], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				     Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }

	  if(!T_solve_phase_OK[index_+Nx]){ // TN

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }
	  else if(!T_solve_phase_OK[index_-Nx]){ // TS

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny],
				       Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	  }
	  else{

	    dphidy_T = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				     Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }

	  dphidz_T = __central_der(phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], phi[index_],
				   Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx]);
	}

	/* Derivatives required for E case */

	dphidy_P = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	dphidy_W = __central_der(phi[index_+Nx-1], phi[index_-1], phi[index_-Nx-1], Delta_yN[index_-1], Delta_yS[index_-1]);
	dphidz_P = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	dphidz_W = __central_der(phi[index_+Nx*Ny-1], phi[index_-1], phi[index_-Nx*Ny-1], Delta_zT[index_-1], Delta_zB[index_-1]);

	/* Check if dphidy_P or dphidy_W needs to be corrected */

	/* Case E-N */
	if(!N_solve_phase_OK[index_]){ // N
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_], phi[index_-Nx],
				     Delta_yN[index_], Delta_yS[index_]);
	}

	if(!N_solve_phase_OK[index_-1]){ // NW
	  dphidy_W = __central_b_der(eps_y[index_-1], dphidyb[index_-1], phi[index_-1], phi[index_-Nx-1],
				     Delta_yN[index_-1], Delta_yS[index_-1]);
	}

	/* Case E-S */
	if(!S_solve_phase_OK[index_]){ // S
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_+Nx], phi[index_],
				     Delta_yS[index_], Delta_yN[index_]);
	}

	if(!S_solve_phase_OK[index_-1]){ // SW
	  dphidy_W = __central_b_der(eps_y[index_-1], dphidyb[index_-1], phi[index_+Nx-1], phi[index_-1],
				     Delta_yS[index_-1], Delta_yN[index_-1]);
	}

	/* Check if dphidz_P or dphidz_W needs to be corrected */

	/* Case E-T */
	if(!T_solve_phase_OK[index_]){ // T
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_], phi[index_-Nx*Ny],
				     Delta_zT[index_], Delta_zB[index_]);
	}

	if(!T_solve_phase_OK[index_-1]){ // TW
	  dphidz_W = __central_b_der(eps_z[index_-1], dphidzb[index_-1], phi[index_-1], phi[index_-Nx*Ny-1],
				     Delta_zT[index_-1], Delta_zB[index_-1]);
	}

	/* Case E-B */
	if(!B_solve_phase_OK[index_]){ // B
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_+Nx*Ny], phi[index_],
				     Delta_zB[index_], Delta_zT[index_]);
	}

	if(!B_solve_phase_OK[index_-1]){ // BW
	  dphidz_W = __central_b_der(eps_z[index_-1], dphidzb[index_-1], phi[index_+Nx*Ny-1], phi[index_-1],
				     Delta_zB[index_-1], Delta_zT[index_-1]);
	}
	
      } /* End case E-P */

      /*******************************************************************/	
      /* W-P line intersects with curve */
      if(!W_solve_phase_OK[index_] && !cell_done_OK){ // W

	cell_done_OK = 1;
	dphidx_P = __central_b_der(eps_x[index_], dphidxb[index_], phi[index_+1], phi[index_],
				   Delta_xW[index_], Delta_xE[index_]);

	dphidx_E = __central_der(phi[index_+2], phi[index_+1], phi[index_],
				 Delta_xE[index_+1], Delta_xW[index_+1]);

	/* Case W-N */
	if(!N_solve_phase_OK[index_]){ // N

	  /* Derivatives required for N case at South position:
	     dphidx_S, dphidy_S, dphidz_S */
	  if(!S_solve_phase_OK[index_+1]){ // SE

	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx], phi[index_-Nx-1],
				       Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }
	  else if(!S_solve_phase_OK[index_-1]){ // SW
	      
	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx+1], phi[index_-Nx],
				       Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	  }
	  else{

	    dphidx_S = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1],
				     Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }

	  dphidy_S = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx],
				   Delta_yN[index_-Nx], Delta_yS[index_-Nx]);

	  if(!T_solve_phase_OK[index_-Nx]){ // TS

	    dphidz_S = __central_b_der(eps_z[index_-Nx], dphidzb[index_-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx],
				       Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	  }
	  else if(!B_solve_phase_OK[index_-Nx]){ // BS

	    dphidz_S = __central_b_der(eps_z[index_-Nx], dphidzb[index_-Nx], phi[index_+Nx*Ny-Nx], phi[index_-Nx],
				       Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	  }
	  else{

	    dphidz_S = __central_der(phi[index_+Nx*Ny-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx],
				     Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	  }
	}

	/* Case W-S */
	if(!S_solve_phase_OK[index_]){ // S
	    
	  /* Derivatives required for S case at North position:
	     dphidx_N, dphidy_N, dphidz_N */
	  if(!N_solve_phase_OK[index_+1]){ // NE

	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx], phi[index_+Nx-1],
				       Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }
	  else if(!N_solve_phase_OK[index_-1]){ // NW
	      
	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx+1], phi[index_+Nx],
				       Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	  }
	  else{
	      
	    dphidx_N = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1],
				     Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }

	  dphidy_N = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_],
				   Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

	  if(!T_solve_phase_OK[index_+Nx]){ // TN

	    dphidz_N = __central_b_der(eps_z[index_+Nx], dphidzb[index_+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx],
				       Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	  }
	  else if(!B_solve_phase_OK[index_+Nx]){ // BN

	    dphidz_N = __central_b_der(eps_z[index_+Nx], dphidzb[index_+Nx], phi[index_+Nx*Ny+Nx], phi[index_+Nx],
				       Delta_zB[index_+Nx], Delta_zT[index_+Nx]);
	  }
	  else{

	    dphidz_N = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx],
				     Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	  }	    
	}

	/* Case W-T */
	if(!T_solve_phase_OK[index_]){ // T

	  /* Derivatives required for T case at Bottom position:
	     dphidx_B, dphidy_B, dphidz_B */
	  if(!B_solve_phase_OK[index_+1]){ // BE

	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }
	  else if(!B_solve_phase_OK[index_-1]){ // BW

	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny+1], phi[index_-Nx*Ny],
				       Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	  }
	  else{

	    dphidx_B = __central_der(phi[index_-Nx*Ny+1], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				     Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }

	  if(!B_solve_phase_OK[index_+Nx]){ // BN

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }
	  else if(!B_solve_phase_OK[index_-Nx]){ // BS

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny],
				       Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	  }
	  else{

	    dphidy_B = __central_der(phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				     Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }

	  dphidz_B = __central_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny],
				   Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	}

	/* Case W-B */
	if(!B_solve_phase_OK[index_]){ // B

	  /* Derivatives required for B case at Top position:
	     dphidx_T, dphidy_T, dphidz_T */
	  if(!T_solve_phase_OK[index_+1]){ // TE

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }
	  else if(!T_solve_phase_OK[index_-1]){ // TW

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny+1], phi[index_+Nx*Ny],
				       Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	  }
	  else{
	      
	    dphidx_T = __central_der(phi[index_+Nx*Ny+1], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				     Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }

	  if(!T_solve_phase_OK[index_+Nx]){ // TN

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }
	  else if(!T_solve_phase_OK[index_-Nx]){ // TS

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny],
				       Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	  }
	  else{

	    dphidy_T = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				     Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }

	  dphidz_T = __central_der(phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], phi[index_],
				   Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);
	}
	  
	/* Derivatives required for W case */
	dphidy_P = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	dphidy_E = __central_der(phi[index_+Nx+1], phi[index_+1], phi[index_-Nx+1], Delta_yN[index_+1], Delta_yS[index_+1]);
	dphidz_P = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	dphidz_E = __central_der(phi[index_+Nx*Ny+1], phi[index_+1], phi[index_-Nx*Ny+1], Delta_zT[index_+1], Delta_zB[index_+1]);
	
	/* Check if dphidy_P or dphidy_E needs to be corrected */

	/* Case W-N */
	if(!N_solve_phase_OK[index_]){ // N
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_], phi[index_-Nx],
				     Delta_yN[index_], Delta_yS[index_]);
	}

	if(!N_solve_phase_OK[index_+1]){ // NE
	  dphidy_E = __central_b_der(eps_y[index_+1], dphidyb[index_+1], phi[index_+1], phi[index_-Nx+1],
				     Delta_yN[index_+1], Delta_yS[index_+1]);
	}

	/* Case W-S */
	if(!S_solve_phase_OK[index_]){ // S
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_+Nx], phi[index_],
				     Delta_yS[index_], Delta_yN[index_]);
	}

	if(!S_solve_phase_OK[index_+1]){ // SE
	  dphidy_E = __central_b_der(eps_y[index_+1], dphidyb[index_+1], phi[index_+Nx+1], phi[index_+1],
				     Delta_yS[index_+1], Delta_yN[index_+1]);
	}

	/* Check if dphidz_P or dphidz_E needs to be corrected */

	/* Case W-T */
	if(!T_solve_phase_OK[index_]){ // T
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_], phi[index_-Nx*Ny],
				     Delta_zT[index_], Delta_zB[index_]);
	}

	if(!T_solve_phase_OK[index_+1]){ // TE
	  dphidz_E = __central_b_der(eps_z[index_+1], dphidzb[index_+1], phi[index_+1], phi[index_-Nx*Ny+1],
				     Delta_zT[index_+1], Delta_zB[index_+1]);
	}

	/* Case W-B */
	if(!B_solve_phase_OK[index_]){ // B
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_+Nx*Ny], phi[index_],
				     Delta_zB[index_], Delta_zT[index_]);
	}

	if(!B_solve_phase_OK[index_+1]){ // BE
	  dphidz_E = __central_b_der(eps_z[index_+1], dphidzb[index_+1], phi[index_+Nx*Ny+1], phi[index_+1],
				     Delta_zB[index_+1], Delta_zT[index_+1]);
	}
	
      }/* End case W-P */

      /*******************************************************************/		
      /* N-P line intersects with curve */
      if(!N_solve_phase_OK[index_] && !cell_done_OK){ // N
	  
	cell_done_OK = 1;
	dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_], phi[index_-Nx],
				   Delta_yN[index_], Delta_yS[index_]);

	dphidy_S = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx],
				 Delta_yN[index_-Nx], Delta_yS[index_-Nx]);

	/* Case N-T */
	if(!T_solve_phase_OK[index_]){ // T
	    
	  /* Derivatives required for T case at Bottom position:
	     dphidx_B, dphidy_B, dphidz_B */
	  if(!B_solve_phase_OK[index_+1]){ // BE
	      
	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }
	  else if(!B_solve_phase_OK[index_-1]){ // BW

	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny+1], phi[index_-Nx*Ny],
				       Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	  }
	  else{

	    dphidx_B = __central_der(phi[index_-Nx*Ny+1], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				     Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }
	    
	  if(!B_solve_phase_OK[index_+Nx]){ // BN

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }
	  else if(!B_solve_phase_OK[index_-Nx]){ // BS

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny],
				       Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	  }
	  else{
	      
	    dphidy_B = __central_der(phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				     Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }
	    
	  dphidz_B = __central_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny],
				   Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	}

	/* Case N-B */
	if(!B_solve_phase_OK[index_]){ // B

	  /* Derivatives required for B case at Top position:
	     dphidx_T, dphidy_T, dphidz_T */
	  if(!T_solve_phase_OK[index_+1]){ // TE

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }
	  else if(!T_solve_phase_OK[index_-1]){ // TW

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny+1], phi[index_+Nx*Ny],
				       Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	  }
	  else{

	    dphidx_T = __central_der(phi[index_+Nx*Ny+1], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				     Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }
	    
	  if(!T_solve_phase_OK[index_+Nx]){ // TN

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }
	  else if(!T_solve_phase_OK[index_-Nx]){ // TS

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny],
				       Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	  }
	  else{

	    dphidy_T = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				     Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }

	  dphidz_T = __central_der(phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], phi[index_],
				   Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);
	}

	/* Derivatives required for N case */
	  
	dphidx_P = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
	dphidx_S = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	dphidz_P = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	dphidz_S = __central_der(phi[index_+Nx*Ny-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx], Delta_zT[index_-Nx], Delta_zB[index_-Nx]);

	/* Check if dphidx_S needs to be corrected */
	if(!S_solve_phase_OK[index_+1]){ // SE
	  dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx], phi[index_-Nx-1],
				     Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}

	if(!S_solve_phase_OK[index_-1]){ // SW
	  dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx+1], phi[index_-Nx],
				     Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	}

	/* Check if dphidz_P or dphidz_S needs to be corrected */

	/* Case N-T */
	if(!T_solve_phase_OK[index_]){ // T
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_], phi[index_-Nx*Ny],
				     Delta_zT[index_], Delta_zB[index_]);
	}

	if(!T_solve_phase_OK[index_-Nx]){ // TS
	  dphidz_S = __central_b_der(eps_z[index_-Nx], dphidzb[index_-Nx], phi[index_-Nx], phi[index_-Nx*Ny-Nx],
				     Delta_zT[index_-Nx], Delta_zB[index_-Nx]);
	}

	/* Case N-B */
	if(!B_solve_phase_OK[index_]){ // B
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_+Nx*Ny], phi[index_],
				     Delta_zB[index_], Delta_zT[index_]);
	}

	if(!B_solve_phase_OK[index_-Nx]){ // BS
	  dphidz_S = __central_b_der(eps_z[index_-Nx], dphidzb[index_-Nx], phi[index_+Nx*Ny-Nx], phi[index_-Nx],
				     Delta_zB[index_-Nx], Delta_zT[index_-Nx]);
	}
      } /* End case N-P */

      /*******************************************************************/		
      /* S-P line intersects with curve */
      if(!S_solve_phase_OK[index_] && !cell_done_OK){ // S

	cell_done_OK = 1;
	dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_+Nx], phi[index_],
				   Delta_yS[index_], Delta_yN[index_]);

	dphidy_N = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_],
				 Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

	/* Case S-T */
	if(!T_solve_phase_OK[index_]){ // T

	  /* Derivatives required for T case at Bottom position:
	     dphidx_T, dphidy_T, dphidz_T */
	  if(!B_solve_phase_OK[index_+1]){ // BE

	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				       Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }
	  else if(!B_solve_phase_OK[index_-1]){ // BW

	    dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny+1], phi[index_-Nx*Ny],
				       Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	  }
	  else{

	    dphidx_B = __central_der(phi[index_-Nx*Ny+1], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				     Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	  }

	  if(!B_solve_phase_OK[index_+Nx]){ // BN

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				       Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }
	  else if(!B_solve_phase_OK[index_-Nx]){ // BS

	    dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny],
				       Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	  }
	  else{

	    dphidy_B = __central_der(phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				     Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	  }

	  dphidz_B = __central_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny],
				   Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);
	}

	/* Case N-B */
	if(!B_solve_phase_OK[index_]){ // B

	  /* Derivatives required for B case at Top position:
	     dphidx_T, dphidy_T, dphidz_T */
	  if(!T_solve_phase_OK[index_+1]){ // TE

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				       Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }
	  else if(!T_solve_phase_OK[index_-1]){ // TW

	    dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny+1], phi[index_+Nx*Ny],
				       Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	  }
	  else{
	      
	    dphidx_T = __central_der(phi[index_+Nx*Ny+1], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				     Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	  }

	  if(!T_solve_phase_OK[index_+Nx]){ // TN

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				       Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }
	  else if(!T_solve_phase_OK[index_-Nx]){ // TS

	    dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny],
				       Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	  }
	  else{

	    dphidy_T = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				     Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	  }

	  dphidz_T = __central_der(phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], phi[index_],
				   Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);
	}

	/* Derivatives required for S case */

	dphidx_P = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
	dphidx_N = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	dphidz_P = __central_der(phi[index_+Nx*Ny], phi[index_], phi[index_-Nx*Ny], Delta_zT[index_], Delta_zB[index_]);
	dphidz_N = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx], Delta_zT[index_+Nx], Delta_zB[index_+Nx]);

	/* Check if dphidx_N needs to be corrected */
	if(!N_solve_phase_OK[index_+1]){ // NE
	  dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx], phi[index_+Nx-1],
				     Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}

	if(!N_solve_phase_OK[index_-1]){ // NW
	  dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx+1], phi[index_+Nx],
				     Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	}

	/* Check if dphidz_P or dphidz_N needs to be corrected */

	/* Case S-T */
	if(!T_solve_phase_OK[index_]){ // T
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_], phi[index_-Nx*Ny],
				     Delta_zT[index_], Delta_zB[index_]);
	}

	if(!T_solve_phase_OK[index_+Nx]){ // TN
	  dphidz_N = __central_b_der(eps_z[index_+Nx], dphidzb[index_+Nx], phi[index_+Nx], phi[index_-Nx*Ny+Nx],
				     Delta_zT[index_+Nx], Delta_zB[index_+Nx]);
	}

	/* Case S-B */
	if(!B_solve_phase_OK[index_]){ // B
	  dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_+Nx*Ny], phi[index_],
				     Delta_zB[index_], Delta_zT[index_]);
	}

	if(!B_solve_phase_OK[index_+Nx]){ // BN
	  dphidz_N = __central_b_der(eps_z[index_+Nx], dphidzb[index_+Nx], phi[index_+Nx*Ny+Nx], phi[index_+Nx],
				     Delta_zB[index_+Nx], Delta_zT[index_+Nx]);
	}
      } /* End case S-P */
      
      /*******************************************************************/
      /* T-P line intersects with curve */
      if(!T_solve_phase_OK[index_] && !cell_done_OK){ // T

	cell_done_OK = 1;
	dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_], phi[index_-Nx*Ny],
				   Delta_zT[index_], Delta_zB[index_]);
	dphidz_B = __central_der(phi[index_], phi[index_-Nx*Ny], phi[index_-2*Nx*Ny],
				 Delta_zT[index_-Nx*Ny], Delta_zB[index_-Nx*Ny]);

	/* Derivatives required for T case */

	dphidx_P = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
	dphidx_B = __central_der(phi[index_-Nx*Ny+1], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1], Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	dphidy_P = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	dphidy_B = __central_der(phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx], Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);

	/* Check if dphidx_B needs to be corrected */
	if(!B_solve_phase_OK[index_+1]){ // BE
	  dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-1],
				     Delta_xE[index_-Nx*Ny], Delta_xW[index_-Nx*Ny]);
	}

	if(!B_solve_phase_OK[index_-1]){ // BW
	  dphidx_B = __central_b_der(eps_x[index_-Nx*Ny], dphidxb[index_-Nx*Ny], phi[index_-Nx*Ny+1], phi[index_-Nx*Ny],
				     Delta_xW[index_-Nx*Ny], Delta_xE[index_-Nx*Ny]);
	}

	/* Check if dphidy_B needs to be corrected */
	if(!B_solve_phase_OK[index_+Nx]){ // BN
	  dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny], phi[index_-Nx*Ny-Nx],
				     Delta_yN[index_-Nx*Ny], Delta_yS[index_-Nx*Ny]);
	}

	if(!B_solve_phase_OK[index_-Nx]){ // BS
	  dphidy_B = __central_b_der(eps_y[index_-Nx*Ny], dphidyb[index_-Nx*Ny], phi[index_-Nx*Ny+Nx], phi[index_-Nx*Ny],
				     Delta_yS[index_-Nx*Ny], Delta_yN[index_-Nx*Ny]);
	}
      } /* End case T-P */

      /*******************************************************************/			
      /* B-P line intersects with curve */
      if(!B_solve_phase_OK[index_] && !cell_done_OK){ // B

	cell_done_OK = 1;
	dphidz_P = __central_b_der(eps_z[index_], dphidzb[index_], phi[index_+Nx*Ny], phi[index_],
				   Delta_zB[index_], Delta_zT[index_]);
	dphidz_T = __central_der(phi[index_+2*Nx*Ny], phi[index_+Nx*Ny], phi[index_],
				 Delta_zT[index_+Nx*Ny], Delta_zB[index_+Nx*Ny]);

	/* Derivatives required for B case */

	dphidx_P = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
	dphidx_T = __central_der(phi[index_+Nx*Ny+1], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1], Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	dphidy_P = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	dphidy_T = __central_der(phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx], Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);

	/* Check if dphidx_T needs to be corrected */
	if(!T_solve_phase_OK[index_+1]){ // TE
	  dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-1],
				     Delta_xE[index_+Nx*Ny], Delta_xW[index_+Nx*Ny]);
	}

	if(!T_solve_phase_OK[index_-1]){ // TW
	  dphidx_T = __central_b_der(eps_x[index_+Nx*Ny], dphidxb[index_+Nx*Ny], phi[index_+Nx*Ny+1], phi[index_+Nx*Ny],
				     Delta_xW[index_+Nx*Ny], Delta_xE[index_+Nx*Ny]);
	}

	/* Check if dphidy_T needs to be corrected */
	if(!T_solve_phase_OK[index_+Nx]){ // TN
	  dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny], phi[index_+Nx*Ny-Nx],
				     Delta_yN[index_+Nx*Ny], Delta_yS[index_+Nx*Ny]);
	}

	if(!T_solve_phase_OK[index_-Nx]){ // TS
	  dphidy_T = __central_b_der(eps_y[index_+Nx*Ny], dphidyb[index_+Nx*Ny], phi[index_+Nx*Ny+Nx], phi[index_+Nx*Ny],
				     Delta_yS[index_+Nx*Ny], Delta_yN[index_+Nx*Ny]);
	}
      } /* End case B-P */

      /* Update the value of dphidxb */
      if(!W_solve_phase_OK[index_] || !E_solve_phase_OK[index_]){ // W || E

	/* Eq 27 Sato */
	dphidS_P = dphidx_P * par_vec_x_x[index_] +
	  dphidy_P * par_vec_x_y[index_] + dphidz_P * par_vec_x_z[index_];
	dphidS_2_P = dphidx_P * par_vec_x_x_2[index_] +
	  dphidy_P * par_vec_x_y_2[index_] + dphidz_P * par_vec_x_z_2[index_];
	  
	if(!W_solve_phase_OK[index_]){ // W

	  dphidNb = dphidNb_v[ dom_mat[index_-1] - 1 ];
	  
	  dphidS_E = dphidx_E * par_vec_x_x[index_] +
	    dphidy_E * par_vec_x_y[index_] + dphidz_E * par_vec_x_z[index_];
	  dphidS_2_E = dphidx_E * par_vec_x_x_2[index_] +
	    dphidy_E * par_vec_x_y_2[index_] + dphidz_E * par_vec_x_z_2[index_];
	    
	  /* Ec 28 Sato */
	  dphidSface_x =
	    __linear_extrap_to_b(dphidS_P, dphidS_E, eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	  
	  dphidS_2face_x = 
	    __linear_extrap_to_b(dphidS_2_P, dphidS_2_E, eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	}
	else{
	  
	  dphidNb = dphidNb_v[ dom_mat[index_+1] - 1 ];
	  
	  dphidS_W = dphidx_W * par_vec_x_x[index_] +
	    dphidy_W * par_vec_x_y[index_] + dphidz_W * par_vec_x_z[index_];
	  dphidS_2_W = dphidx_W * par_vec_x_x_2[index_] +
	    dphidy_W * par_vec_x_y_2[index_] + dphidz_W * par_vec_x_z_2[index_];

	  /* Ec 28 Sato */
	  dphidSface_x = 
	    __linear_extrap_to_b(dphidS_P, dphidS_W, eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	  dphidS_2face_x = 
	    __linear_extrap_to_b(dphidS_2_P, dphidS_2_W, eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	}
	/* Ec 24 Sato */
	dphidxb[index_] = dphidNb * norm_vec_x_x[index_] +
	  dphidSface_x * par_vec_x_x[index_] + dphidS_2face_x * par_vec_x_x_2[index_];
      }
	
      /* Update the value of dphidyb */
      if(!S_solve_phase_OK[index_] || !N_solve_phase_OK[index_]){ // S || N

	/* Eq 27 Sato */
	dphidS_P = dphidx_P * par_vec_y_x[index_] +
	  dphidy_P * par_vec_y_y[index_] + dphidz_P * par_vec_y_z[index_];
	dphidS_2_P = dphidx_P * par_vec_y_x_2[index_] +
	  dphidy_P * par_vec_y_y_2[index_] + dphidz_P * par_vec_y_z_2[index_];	  

	if(!S_solve_phase_OK[index_]){ // S

	  dphidNb = dphidNb_v[ dom_mat[index_-Nx] - 1 ];
	  
	  dphidS_N = dphidx_N * par_vec_y_x[index_]  +
	    dphidy_N * par_vec_y_y[index_] + dphidz_N * par_vec_y_z[index_];
	  dphidS_2_N = dphidx_N * par_vec_y_x_2[index_] +
	    dphidy_N * par_vec_y_y_2[index_] + dphidz_N * par_vec_y_z_2[index_];

	  /* Ec 28 Sato */
	  dphidSface_y =
	    __linear_extrap_to_b(dphidS_P, dphidS_N, eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	  
	  dphidS_2face_y =
	    __linear_extrap_to_b(dphidS_2_P, dphidS_2_N, eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	}
	else{

	  dphidNb = dphidNb_v[ dom_mat[index_+Nx] - 1 ];	  
	  
	  dphidS_S = dphidx_S * par_vec_y_x[index_] +
	    dphidy_S * par_vec_y_y[index_] + dphidz_S * par_vec_y_z[index_];
	  dphidS_2_S = dphidx_S * par_vec_y_x_2[index_] +
	    dphidy_S * par_vec_y_y_2[index_] + dphidz_S * par_vec_y_z_2[index_];

	  /* Ec 28 Sato */
	  dphidSface_y =
	    __linear_extrap_to_b(dphidS_P, dphidS_S, eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
		    
	  dphidS_2face_y = 
	    __linear_extrap_to_b(dphidS_2_P, dphidS_2_S, eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
	}
	/* Ec 24 Sato */
	dphidyb[index_] = dphidNb * norm_vec_y_y[index_] +
	  dphidSface_y * par_vec_y_y[index_] + dphidS_2face_y * par_vec_y_y_2[index_];
      }

      /* Update the value of dphidzb */
      if(!B_solve_phase_OK[index_] || !T_solve_phase_OK[index_]){ // B || T

	/* Eq 27 Sato */
	dphidS_P = dphidx_P * par_vec_z_x[index_] +
	  dphidy_P * par_vec_z_y[index_] + dphidz_P * par_vec_z_z[index_];
	dphidS_2_P = dphidx_P * par_vec_z_x_2[index_] +
	  dphidy_P * par_vec_z_y_2[index_] + dphidz_P * par_vec_z_z_2[index_];	  

	if(!B_solve_phase_OK[index_]){ // B

	  dphidNb = dphidNb_v[ dom_mat[index_-Nx*Ny] - 1 ];
	  
	  dphidS_T = dphidx_T * par_vec_z_x[index_] +
	    dphidy_T * par_vec_z_y[index_] + dphidz_T * par_vec_z_z[index_];
	  dphidS_2_T = dphidx_T * par_vec_z_x_2[index_] +
	    dphidy_T * par_vec_z_y_2[index_] + dphidz_T * par_vec_z_z_2[index_];

	  /* Ec 28 Sato */
	  dphidSface_z =
	    __linear_extrap_to_b(dphidS_P, dphidS_T, eps_z[index_], Delta_zB[index_], Delta_zT[index_]);
	  
	  dphidS_2face_z =
	    __linear_extrap_to_b(dphidS_2_P, dphidS_2_T, eps_z[index_], Delta_zB[index_], Delta_zT[index_]);
	}
	else{
	  
	  dphidNb = dphidNb_v[ dom_mat[index_+Nx*Ny] - 1 ];
	  
	  dphidS_B = dphidx_B * par_vec_z_x[index_] +
	    dphidy_B * par_vec_z_y[index_] + dphidz_B * par_vec_z_z[index_];
	  dphidS_2_B = dphidx_B * par_vec_z_x_2[index_] +
	    dphidy_B * par_vec_z_y_2[index_] + dphidz_B * par_vec_z_z_2[index_];

	  /* Ec 28 Sato */
	  dphidSface_z =
	    __linear_extrap_to_b(dphidS_P, dphidS_B, eps_z[index_], Delta_zT[index_], Delta_zB[index_]);
	  
	  dphidS_2face_z =
	    __linear_extrap_to_b(dphidS_2_P, dphidS_2_B, eps_z[index_], Delta_zT[index_], Delta_zB[index_]);
	}
	/* Ec 24 Sato */
	dphidzb[index_] = dphidNb * norm_vec_z_z[index_] +
	  dphidSface_z * par_vec_z_z[index_] + dphidS_2face_z * par_vec_z_z_2[index_];
      }
	
      /* Convective part at the face and correction with m factor */
      if(!W_solve_phase_OK[index_]){ // W

	eps_val = eps_x[index_];
	
	Da = (1 - fe[index_])*Delta_xE[index_] + eps_val*Delta_xW[index_];
	
	mx = Delta_x[index_] / Da;
	
	Few = rho_cp * u[index_u];
	phi_m_ew = phi[index_] - 0.5 * Delta_x[index_] * dphidxb[index_];
      }

      if(!E_solve_phase_OK[index_]){ // E

	eps_val = eps_x[index_];
	
	Da = (1 - fw[index_])*Delta_xW[index_] + eps_val*Delta_xE[index_];
	
	mx = Delta_x[index_] / Da;
	
	Few = rho_cp * u[index_u+1];
	phi_m_ew = phi[index_] + 0.5 * Delta_x[index_] * dphidxb[index_];
      }

      if(!S_solve_phase_OK[index_]){ // S
	
	eps_val = eps_y[index_];
	
	Da = (1 - fn[index_])*Delta_yN[index_] + eps_val*Delta_yS[index_];
	
	my = Delta_y[index_] / Da;
	
	Fns = rho_cp * v[index_v];
	phi_m_ns = phi[index_] - 0.5 * Delta_y[index_] * dphidyb[index_];
      }

      if(!N_solve_phase_OK[index_]){ // N

	eps_val = eps_y[index_];
	
	Da = (1 - fs[index_])*Delta_yS[index_] + eps_val*Delta_yN[index_];
	
	my = Delta_y[index_] / Da;
	
	Fns = rho_cp * v[index_v+Nx];
	phi_m_ns = phi[index_] + 0.5 * Delta_y[index_] * dphidyb[index_];
      }

      if(!B_solve_phase_OK[index_]){ // B
	
	eps_val = eps_z[index_];
	
	Da = (1 - ft[index_])*Delta_zT[index_] + eps_val*Delta_zB[index_];
	
	mz = Delta_z[index_] / Da;
	
	Ftb = rho_cp * w[index_w];
	phi_m_tb = phi[index_] - 0.5 * Delta_z[index_] * dphidzb[index_];
      }

      if(!T_solve_phase_OK[index_]){ // T

	eps_val = eps_z[index_];
	
	Da = (1 - fb[index_])*Delta_zB[index_] + eps_val*Delta_zT[index_];
	
	mz = Delta_z[index_] / Da;
	
	Ftb = rho_cp * w[index_w+Nx*Ny];
	phi_m_tb = phi[index_] + 0.5 * Delta_z[index_] * dphidzb[index_];
      }

      /* Build the combined fluxes. It must be noted that there are different lengths
	 for the molecular and convective parts which are taken into account by 
	 the eps factors */
      Jxmol = - gamma * dphidxb[index_] * mx;
      Jxconv = Few * phi_m_ew;
      Jxconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jxconv : 0;

      Jymol = - gamma * dphidyb[index_] * my;
      Jyconv = Fns * phi_m_ns;
      Jyconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jyconv : 0;

      Jzmol = - gamma * dphidzb[index_] * mz;
      Jzconv = Ftb * phi_m_tb;
      Jzconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jzconv : 0;

      Jx = Jxmol + Jxconv;
      Jy = Jymol + Jyconv;
      Jz = Jzmol + Jzconv;
      
      SC_vol_m[index_] =
	(-1) * (1 - E_solve_phase_OK[index_]) * Jx * faces_x[index_u+1] +
	(-1) * (1 - N_solve_phase_OK[index_]) * Jy * faces_y[index_v+Nx] +
	(-1) * (1 - T_solve_phase_OK[index_]) * Jz * faces_z[index_w+Nx*Ny] +
	(1 - W_solve_phase_OK[index_]) * Jx * faces_x[index_u] +
	(1 - S_solve_phase_OK[index_]) * Jy * faces_y[index_v] +
	(1 - B_solve_phase_OK[index_]) * Jz * faces_z[index_w];
    } /* for(count=0; count<Isoflux_boundary_cell_count; count++) */
  }
  
  return;
}

/* SOLVE_PHI_TRANSIENT_3D */
void solve_phi_transient_3D(Data_Mem *data){

  /* Pointer to phi coeffs function */
  void (*coeffs_phi_3D)(Data_Mem *);

  Wins *wins = &(data->wins);
  Sigmas *sigmas = &(data->sigmas);
  
  const TEG *teg = &(data->teg);

  FILE *fp = NULL;
  FILE *teg_fp = NULL;
  FILE *current_fp = NULL;
  FILE *glob_res_fp = NULL;
  
  char filename[SIZE];
  char teg_filename[SIZE];
  char current_filename[SIZE];
  char msg[SIZE];

  const char *results_dir = data->results_dir;
  const char *glob_res_filename = data->glob_res_filename;

  int convergence_OK = 0, sub_convergence_OK = 0, Tol_OK = 0;
  int it_counter = 0, sub_it_counter = 0, total_it = 0;
  int transient_counter = 1;
  int gL_convergence_OK;
  
  int max_it = data->max_it;
  int max_sub_it = data->max_sub_it;

  const int phi_flow_scheme = data->phi_flow_scheme;

  const int TEG_OK_summary = ymy_teg_phase(teg);

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int it_ref_norm_phi = data->it_ref_norm_phi;
  const int it_upd_coeff_phi = data->it_upd_coeff_phi;

  const int interface_flux_update_it =
    data->interface_flux_update_it;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int animation_phi_OK = data->animation_phi_OK;
  const int animation_phi_update_it = data->animation_phi_update_it;

  const int res_sigma_iter = data->res_sigma_iter;
  const int write_transient_data_OK = data->write_transient_data_OK;

  u_int64_t time_i, time_f, timer;
  
  double norm_phi = 1, norm_phi_ref = 1;
  double gL_diff = 1, gL_diff_ref = 1;

  const double tol_phi = data->tol_phi;
  const double gL_tol = data->gL_tol;
  const double transient_save = data->write_transient_data_interval * 60;

  double *phi = data->phi;
  double *phi0 = data->phi0;
  double *gL = data->gL;
  double *gL_calc = data->gL_calc;
  double *gL_guess = data->gL_guess;
  double *dt = data->dt;
  double *sigmas_v= data->sigmas.values;
  double *glob_norm= data->sigmas.glob_norm;

  sigmas->time_f = &(time_f);
  
  solve_phi_pre_calc_jobs_3D(data);

  if(phi_flow_scheme == 1){
    coeffs_phi_3D = coeffs_phi_power_law_3D;
  }
  else if(phi_flow_scheme == 2){
    coeffs_phi_3D = coeffs_phi_TVD_3D;
  }
  else{
    coeffs_phi_3D = coeffs_phi_power_law_3D;
  }

  if(write_transient_data_OK){
    set_transient_sim_time_filename(filename, "sim_time",
				    0, results_dir);
    
    open_check_file(filename, &fp, wins, 0);
    fprintf(fp, "sim_time_var = [\n");          
    fclose(fp);

    if(TEG_OK_summary){
      set_transient_sim_time_filename(teg_filename, "teg_vars",
				      0, results_dir);
	
      open_check_file(teg_filename, &teg_fp, wins, 0);
      fprintf(teg_fp, "teg_var = [\n");	
      fclose(teg_fp);

      set_transient_sim_time_filename(current_filename, "IVP", 
				      0, results_dir);

      open_check_file(current_filename, &current_fp, wins, 0);
      fprintf(current_fp, "IVP_var = [\n");
      fclose(current_fp);

    }
  }

  if(!data->read_tmp_file_OK){
    dt[6] = 0;
  }
     
  /* Timer to measure it_time */
  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  /* Este es el ciclo principal de iteración */
  do{
    it_counter++;
    dt[6] = dt[6] + dt[3];

    sub_it_counter = 0;
    sub_convergence_OK = 0;
    
    norm_phi_ref = 1;
            
    if(solve_phase_change_OK){
      /* gL_diff Max difference between guess and 
	 calculated value of gL */
      gL_diff = 1;
      gL_diff_ref = 1;
      gL_convergence_OK = 0;
    }
    else{
      gL_convergence_OK = 1;
    }

    /* Store phi values */
    copy_array(phi, phi0, Nx, Ny, Nz);

    /* Saving transient data */
    if( (write_transient_data_OK) &&
	(dt[6] >= data->tsaves[2]) &&
	(dt[6] <= data->tsaves[3]) ){
      
      if( dt[6] >= data->tsaves[2] + transient_save * (transient_counter - 1) ){
	
	save_transient_data(data, filename);

	if(TEG_OK_summary){
	  /* Here compute electric current */ 
	  solve_current(data, current_filename);
	  save_transient_teg_data(data, teg_filename);
	}
	
	transient_counter++;
	
      }
    }
    
    /* Update coeffs before starting next outer iteration */
    if(solve_phase_change_OK){
      /* gL must be updated before average physical properties calculation */
      update_gL_guess_3D(data);
      avg_phys_prop_3D(data);
      compute_enthalpy_diff_3D(data);
    }

    /* Update coefficients and interface flow */
    coeffs_phi_3D(data);
    compute_phi_SC_at_interface_3D(data);
    
    /* Este es el subciclo de iteración */
    do{
      sub_it_counter++;
      total_it++;

      /* Call solve_flow_jobs with proper update flag */
      if(sub_it_counter % it_upd_coeff_phi == 0){

	/* gL must be updated before average physical properties calculation */
	if(solve_phase_change_OK){
	  update_gL_guess_3D(data);
	  avg_phys_prop_3D(data);
	  compute_enthalpy_diff_3D(data);
	}

	/* Update coeffs */
	coeffs_phi_3D(data);

	/* Computation of flow at interfaces must be after an update
	   of the coefficients */
	if(sub_it_counter % interface_flux_update_it == 0){
	  compute_phi_SC_at_interface_3D(data);
	}
      }

      /* Solve for phi */
      norm_phi = solve_phi_jobs_3D(data);

      /* Compute glob norm for stopping criteria */      
      glob_norm[7] = compute_norm_glob_res_3D(data->phi, &(data->coeffs_phi), Nx, Ny, Nz);
      
      if(solve_phase_change_OK){
	/* Update phase average properties */
	compute_gL_value_3D(data, gL_calc);

	/* Compute glob norm for stopping criteria */
	glob_norm[8] = compute_avg_gL_diff_3D(data);

	/* Legacy stopping criteria based on sigmas.
	 * We are adopting for transient the comparison with glob norm instead.
	 
	gL_diff = max_abs_matrix_diff_3D(gL_calc, gL_guess, Nx, Ny, Nz);

	if(sub_it_counter == it_ref_norm_phi){
	gL_diff_ref = (gL_diff > 1e-9) ? gL_diff : 1;
	}

	sigmas_v[8] = gL_diff / gL_diff_ref;
	
	if(sigmas_v[8] < gL_tol){
	gL_convergence_OK = 1;
	}

	if(sub_it_counter <= it_ref_norm_phi){
	gL_convergence_OK = 0;
	}

	*/
	
      }

      /* Legacy stopping criteria based on sigmas.
       * We are adopting for transient the comparison with glob norm instead.
      
      if(sub_it_counter == it_ref_norm_phi){
        norm_phi_ref = norm_phi;        
      }
      
      sigmas_v[7] = norm_phi / norm_phi_ref;
      Tol_OK = (sigmas_v[7] <= tol_phi) && gL_convergence_OK;

      */

      Tol_OK = (glob_norm[7] <= tol_phi) && gL_convergence_OK;
      
      if(sub_it_counter <= it_ref_norm_phi){
	Tol_OK = 0;
      }
      
      if(Tol_OK || (sub_it_counter >= max_sub_it)){
	sub_convergence_OK = 1;
      }
      
      /* Here process action upon signal raise */
      if(action_on_signal_OK){
	
	process_action_on_signal(data, &convergence_OK, &sub_convergence_OK);
	
	/* Update in case of modification the new values */
	max_it = data->max_it;
	max_sub_it = data->max_sub_it;
	
	action_on_signal_OK = 0;
        
      }

#ifdef DISPLAY_OK
      /* Back to ncurses */
      refresh();
#endif
      
    } while(!sub_convergence_OK);

    /* Update gL value */
    if(solve_phase_change_OK){
      copy_array(gL_guess, gL, Nx, Ny, Nz);
    }

    /* Compute normalized global residues */
    /* This was inside the sub iter loop (!?) under 
       if(sub_it_counter % interface_flux_update_it == 0){ */
    if(it_counter % res_sigma_iter == 0){
      glob_norm[7] = compute_norm_glob_res_3D(data->phi, &(data->coeffs_phi), Nx, Ny, Nz);
      sigmas_v[7] = glob_norm[7];

      if(solve_phase_change_OK){
	glob_norm[8] = compute_avg_gL_diff_3D(data);
	sigmas_v[8] = glob_norm[8];
      }
    }
      
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);
      memset(msg, '\0', sizeof(msg));
      snprintf(msg, SIZE, 
	       "solve_phi: it = "
	       "%7d; sphi = %9.3e; sgL = %9.3e", 
	       it_counter, sigmas_v[7], sigmas_v[8]);

      sigmas->it = it_counter;
      write_to_log_file(data, msg);
      
      timer_flag_OK = 0;
    }
	
    /* Updating the server */
    if(launch_server_OK &&
       (it_counter % server_update_it == 0)){
      update_server(data);
    }
      
    /* Updating the animation */
    if(animation_phi_OK &&
       (it_counter % animation_phi_update_it == 0)){
      update_anims((void *) data);	
    }
      
    time_i = time_f;
    time_f = time_f + elapsed_time(&timer);

    if(it_counter % res_sigma_iter == 0){
      sigmas->it = it_counter;
      sigmas->sub_it = sub_it_counter;

      print_phi_it_info(it_counter, max_it,
			time_i, time_f,
			sigmas,
			wins);
	  
      update_graph_window(data);
	  
      write_sfile(data);

      write_norm_glob_res(data);
    }
    
  } while((it_counter < max_it) && !convergence_OK);

  /* Save iteration */
  sigmas->phi_it = it_counter;
  solve_phi_post_calc_jobs(data);

  if(write_transient_data_OK){
      
    open_check_file(filename, &fp, wins, 1);
    fprintf(fp, "];\n");
    fclose(fp);

    if(TEG_OK_summary){

      open_check_file(teg_filename, &teg_fp, wins, 1);
      fprintf(teg_fp, "];\n");
      fclose(teg_fp);

      open_check_file(current_filename, &current_fp, wins, 1);
      fprintf(current_fp, "];\n");
      fclose(current_fp);

    }
    
  }
#ifdef DISPLAY_OK
  wprintw(wins->disp, "Simulation time (s) = %g\n", dt[6]);
  wrefresh(wins->disp);
#endif
  
  close_sfile(data);

  open_check_file(glob_res_filename, &glob_res_fp, wins, 1);
  fprintf(glob_res_fp, "];\n");
  fclose(glob_res_fp);  
  
  return;
}

/* SOLVE_PHI_STEADY */
/*
  Entry point
*/
/* SOLVE_PHI_STEADY_3D */
void solve_phi_steady_3D(Data_Mem *data){

  /* Pointer to phi coeffs function */
  void (*coeffs_phi_3D)(Data_Mem *);

  Wins *wins = &(data->wins);
  Sigmas *sigmas = &(data->sigmas);
    
  char msg[SIZE];

  FILE *glob_res_fp = NULL;

  const char *glob_res_filename = data->glob_res_filename;

  int convergence_OK = 0, Tol_OK = 0;
  int it_counter = 0;
  int gL_convergence_OK;
  
  int max_it = data->max_it;

  int it_ref_norm_phi;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int phi_flow_scheme = data->phi_flow_scheme;

  const int read_tmp_file_OK = data->read_tmp_file_OK;

  const int it_upd_coeff_phi = data->it_upd_coeff_phi;

  const int interface_flux_update_it =
    data->interface_flux_update_it;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int animation_phi_OK = data->animation_phi_OK;
  const int animation_phi_update_it = data->animation_phi_update_it;

  const int res_sigma_iter = data->res_sigma_iter;

  u_int64_t time_i, time_f, timer;
  
  double norm_phi = 1, norm_phi_ref = 1;
  double gL_diff = 1, gL_diff_ref = 1;

  const double tol_phi = data->tol_phi;
  const double gL_tol = data->gL_tol;

  double *gL = data->gL;
  double *gL_calc = data->gL_calc;
  double *gL_guess = data->gL_guess;
  double *sigmas_v= data->sigmas.values;
  double *refs = data->sigmas.refs;

  sigmas->time_f = &(time_f);
  
  solve_phi_pre_calc_jobs_3D(data);

  if(phi_flow_scheme == 1){
    coeffs_phi_3D = coeffs_phi_power_law_3D;
  }
  else if(phi_flow_scheme == 2){
    coeffs_phi_3D = coeffs_phi_TVD_3D;
  }
  else{
    coeffs_phi_3D = coeffs_phi_power_law_3D;
  }

  if(read_tmp_file_OK){
    it_ref_norm_phi = -1;
    norm_phi_ref = refs[7];
    gL_diff_ref = refs[8];
  }
  else{
    it_ref_norm_phi = data->it_ref_norm_phi;
  }
		
  /* Timer to measure it_time */
  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  if(solve_phase_change_OK){
    /* gL_diff Max difference between guess and 
       calculated value of gL */
    gL_diff = 1;
    gL_convergence_OK = 0;
  }
  else{
    gL_convergence_OK = 1;
  }
    
  /* Este es el ciclo de iteración */  
  do{
    it_counter++;

    /* Call solve_flow_jobs with proper update flag */
    if(it_counter % it_upd_coeff_phi == 0){
      coeffs_phi_3D(data);
    }
      
    /* Computation of flow at the interfaces 
       for the conjugate case must be accompanied with
       an update of coefficients */
    if(it_counter % interface_flux_update_it == 0){
      coeffs_phi_3D(data);
      compute_phi_SC_at_interface_3D(data);
    }
     
    if(solve_phase_change_OK){
      /* Update phase average properties */
      avg_phys_prop_3D(data);
      /* For steady state the source term is due to phase change is zero
	 compute_enthalpy_diff(data); */
	
      norm_phi = solve_phi_jobs_3D(data);
	
      compute_gL_value_3D(data, gL_calc);
      gL_diff = max_abs_matrix_diff_3D(gL_calc,
				       gL_guess, Nx, Ny, Nz);
      update_gL_guess_3D(data);

      if(it_counter == it_ref_norm_phi){
	
	gL_diff_ref = (gL_diff > 1e-9) ? gL_diff : 1;
	refs[8] = gL_diff_ref;
	
      }

      sigmas_v[8] = gL_diff / gL_diff_ref;
	
      if(sigmas_v[8] < gL_tol){
	gL_convergence_OK = 1;
      }
	
      if(it_counter <= it_ref_norm_phi){
	gL_convergence_OK = 0;
      }

    }
    else{
      /* No phase change */
      norm_phi = solve_phi_jobs_3D(data);
    }
	    
    /* Para adimensionalizar los residuos y poder 
       compararlos con tol_phi apropiadamente */
    if(it_counter == it_ref_norm_phi){
      norm_phi_ref = norm_phi;
      refs[7] = norm_phi_ref;
    }
      
    sigmas_v[7] = norm_phi / norm_phi_ref;
    Tol_OK = (sigmas_v[7] <= tol_phi);

    /* Comparison with tolerances must be with 
       normalized residuals */
    if(it_counter <= it_ref_norm_phi){
      Tol_OK = 0;
    }
    
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);
      memset(msg, '\0', sizeof(msg));
      snprintf(msg, SIZE, 
	       "solve_phi: it = "
	       "%7d; sphi = %9.3e; sgL = %9.3e", 
	       it_counter, sigmas_v[7], sigmas_v[8]);

      sigmas->it = it_counter;
      write_to_log_file(data, msg);
      
      timer_flag_OK = 0;
    }
    
    /* Updating the server */
    if(launch_server_OK &&
       (it_counter % server_update_it == 0)){
      update_server(data);
    }
	
    /* Updating the animation */
    if(animation_phi_OK &&
       (it_counter % animation_phi_update_it == 0)){

      update_anims((void *) data);
	  
    }
	
    time_i = time_f;
    time_f = time_f + elapsed_time(&timer);

    if(it_counter % res_sigma_iter == 0){
      sigmas->it = it_counter;

      print_phi_it_info(it_counter, max_it,
			time_i, time_f,
			sigmas,
			wins);
	  
      update_graph_window(data);
	  
      write_sfile(data);
    }	
  
    if((Tol_OK && gL_convergence_OK) ||
       (it_counter >= max_it)){
      convergence_OK = 1;
    }
      
    /* Here process action upon signal raise */
    if(action_on_signal_OK){
          
      process_action_on_signal(data, &convergence_OK, &convergence_OK);

      /* Update in case of modification the new values */
      max_it = data->max_it;

      action_on_signal_OK = 0;
        
    }

#ifdef DISPLAY_OK
    /* Back to ncurses */
    refresh();
#endif
   
    /* Update gL value */
    if(solve_phase_change_OK){
      copy_array(gL_guess, gL, Nx, Ny, Nz);
    }
   
  } while(!convergence_OK);

  /* Save iteration */
  sigmas->phi_it = it_counter;
  solve_phi_post_calc_jobs(data);

  close_sfile(data);
 
  open_check_file(glob_res_filename, &glob_res_fp, wins, 1);
  fprintf(glob_res_fp, "];\n");
  fclose(glob_res_fp);  
 
  return;
}

/* SOLVE_PHI_PRE_CALC_JOBS_3D */
void solve_phi_pre_calc_jobs_3D(Data_Mem *data){

  /* Pointer to phi coeffs function */
  void (*coeffs_phi_3D)(Data_Mem *);

  MATIOS *matios = &(data->matios);

  Sigmas *sigmas = &(data->sigmas);

  Wins *wins = &(data->wins);

  double *sigmas_v= data->sigmas.values;

  const int phi_flow_scheme = data->phi_flow_scheme;

  const int turb_model = data->turb_model;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int ss_OK = data->steady_state_OK;
  const int wtd_OK = data->write_transient_data_OK;
  const int tos = data->TEG_OK_summary;

  size_t dim[2], dims[4];

  if(phi_flow_scheme == 1){
    coeffs_phi_3D = coeffs_phi_power_law_3D;
  }
  else if(phi_flow_scheme == 2){
    coeffs_phi_3D = coeffs_phi_TVD_3D;
  }
  else{
    coeffs_phi_3D = coeffs_phi_power_law_3D;
  }

  if(wtd_OK && !ss_OK){
    dim[0] = 1;
    dim[1] = 1;
    matios->tphi = Mat_VarCreate("tphi", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->dt[6]),
				 MAT_F_DONT_COPY_DATA);

    if(tos){
      matios->current = Mat_VarCreate("current", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->teg.I),
				      MAT_F_DONT_COPY_DATA);
      matios->voltage = Mat_VarCreate("voltage", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->teg.V),
				      MAT_F_DONT_COPY_DATA);
      matios->power = Mat_VarCreate("power", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->teg.P),
				    MAT_F_DONT_COPY_DATA);
    }
    
    dims[0] = Nx;
    dims[1] = Ny;
    dims[2] = Nz;
    dims[3] = 1;
    
    /* Setting matio struct for transient data saving */
    matios->phi = Mat_VarCreate("phi_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims,
				data->phi, MAT_F_DONT_COPY_DATA);
    
    if(data->solve_phase_change_OK){
      matios->gL = Mat_VarCreate("gL_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims,
				 data->gL, MAT_F_DONT_COPY_DATA);
    }
  }
    
  /* Reset values of sigmas structure */
  sigmas_v[0] = NAN;
  sigmas_v[1] = NAN;
  sigmas_v[2] = NAN;
  sigmas_v[3] = NAN;
  sigmas_v[4] = NAN;
  sigmas_v[5] = NAN;
  sigmas_v[6] = NAN;
  sigmas_v[7] = 1;
  sigmas_v[8] = NAN;

  sigmas->it = 0;

  /* Set ncurses graphics for phi */
  set_it_graph_sizes(wins);
  update_graph_window(data);

  /* Add turbulent conductivity contribution */
  if(turb_model){
    add_gamma_turb_3D(data);
  }

  /* Initial coeffs computation. 
     Later is included in solve_phi_jobs */
  compute_phi_SC_at_interface_3D(data);
  coeffs_phi_3D(data);

  /* Write the table header for iterations without updating the window */
  table_header_phi_it(wins);
  write_sfile(data);
  write_norm_glob_res(data);

  if(!data->read_tmp_file_OK){
    data->dt[3] = data->dt[4];
  }

  mem_count(wins);

  return;
}

/* SOLVE_PHI_JOBS_3D */
/**
   Solves for phi given the coefficients and set of the boundary conditions. 
   Returns the normalized residues for phi.
   
   MODIFIED: 02-07-2020
   
   @param data
   
   @see set_bc_3D, ADI_solver_3D, compute_residues_3D
   
   @return double
   
   MODIFIED: 02-07-2020
*/
double solve_phi_jobs_3D(Data_Mem *data){
  
  double norm_phi = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  double *phi = data->phi;
  
  void (*ADI_solver)(double *, Coeffs *, const int, const int,
		     const int, ADI_Vars *) = data->ADI_solver;
  
  /* Setting boundary conditions */
  set_bc_3D(data);
  
  /* Solving algebraic system */
  (*ADI_solver)(data->phi, &(data->coeffs_phi), Nx, Ny, Nz,
		&(data->ADI_vars));
  
  /* Computing residues */
  norm_phi =
    compute_residues_3D(phi, &(data->coeffs_phi), Nx, Ny, Nz);
  
  return norm_phi;
}

/* SET_BC_3D */
/**
   Set the domain (not immersed boundaries) boundary conditions.
   
   @param data
   
   @see set_bc_3D, ADI_solver_3D, compute_residues_3D
   
   @return double
   
   MODIFIED: 03-07-2020
*/
void set_bc_3D(Data_Mem *data){

  int I, J, K, index_;

  int iper, jper, kper, count_seg;
  int Nperx, Npery, Nperz, Nparts;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *alpha = NULL;

  const int *dom_mat = data->domain_matrix;
  const int *P_solve_phase_OK = data->P_solve_phase_OK;

  double x, y, z;
    
  const double Lx = data->nodes_x[Nx-1];
  const double Ly = data->nodes_y[Nx*(Ny-1)];
  const double Lz = data->nodes_z[Nx*Ny*(Nz-1)];

  double *aP = data->coeffs_phi.aP;
  double *b = data->coeffs_phi.b;

  double *aE = data->coeffs_phi.aE;
  double *aW = data->coeffs_phi.aW;
  double *aN = data->coeffs_phi.aN;
  double *aS = data->coeffs_phi.aS;
  double *aT = data->coeffs_phi.aT;
  double *aB = data->coeffs_phi.aB;

  double *a_m = NULL;
  
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zT = data->Delta_zT;
  const double *Delta_zB = data->Delta_zB;

  const double *D_m = NULL;
  
  const double *gamma_m = data->gamma_m;

  const double *perx = NULL;
  const double *pery = NULL;
  const double *perz = NULL;
  
  const double *beta = NULL;
  const double *gamma = NULL;
  const double *phi_b = NULL;

  const double *phi0_v = data->phi0_v;

  Wins *wins = &(data->wins);
  
  /* WEST */

  Nperx = 1;
  Npery = data->west.Npery;
  Nperz = data->west.Nperz;

  Nparts = data->west.parts;

  if(Nperx * Npery * Nperz != Nparts){
    error_msg(wins, "Number of parts in boundary does not match");
  }
  
  perx = NULL;
  pery = data->west.pery;
  perz = data->west.perz;
  
  alpha = data->west.alpha;
  beta = data->west.beta;
  gamma = data->west.gamma;
  phi_b = data->west.phi_b;

  I = 0;
  a_m = aE;
  D_m = Delta_xE;

  kper = 0;
  count_seg = 0;

  for (K=1; K<Nz-1; K++){
    
    jper = 0;
    
    for (J=1; J<Ny-1; J++){
      
      index_ = K*Nx*Ny + J*Nx + I;

      y = nodes_y[index_];
      z = nodes_z[index_];

      if(z <= perz[kper] * Lz){
	/* Inside correct perz region */
    
	if(y <= pery[jper] * Ly){
	  /* inside correct pery region */

	  count_seg = kper*Npery + jper;

	  if(P_solve_phase_OK[index_]){
	  
	    aP[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_] +
	      beta[count_seg];
	    a_m[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_];
	    b[index_] =
	      beta[count_seg] * phi_b[count_seg] + gamma[count_seg];

	  }
	  else{
	    aP[index_] = 1;
	    b[index_] = phi0_v[ dom_mat[index_] ];
	  }
	}
	else {
	  /* Correct perz region but incorrect pery region
	     so lets advance one pery further */
	  J--;
	  jper++;
	 	 	  
	} // end if(y <= pery[jper] * Ly)
      }
      else{
	/* Incorrect perz region, advance one perz further */
	K--;
	kper++;
	break;
      } // end if(z <= perz[kper] * Lz){
      
    } // end for (J=1; J<Ny-1; J++){
    
  } // end for (K=1; K<Nz-1; K++){

  /* EAST */
  Nperx = 1;
  Npery = data->east.Npery;
  Nperz = data->east.Nperz;

  Nparts = data->east.parts;

  if(Nperx * Npery * Nperz != Nparts){
    error_msg(wins, "Number of parts in boundary does not match");
  }
  
  perx = NULL;
  pery = data->east.pery;
  perz = data->east.perz;
  
  alpha = data->east.alpha;
  beta = data->east.beta;
  gamma = data->east.gamma;
  phi_b = data->east.phi_b;

  I = Nx-1;
  a_m = aW;
  D_m = Delta_xW;

  kper = 0;
  count_seg = 0;

  for (K=1; K<Nz-1; K++){
    
    jper = 0;
    
    for (J=1; J<Ny-1; J++){
      
      index_ = K*Nx*Ny + J*Nx + I;

      y = nodes_y[index_];
      z = nodes_z[index_];

      if(z <= perz[kper] * Lz){
	/* Inside correct perz region */
    
	if(y <= pery[jper] * Ly){
	  /* inside correct pery region */

	  count_seg = kper*Npery + jper;

	  if(P_solve_phase_OK[index_]){
	  
	    aP[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_] +
	      beta[count_seg];
	    a_m[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_];
	    b[index_] =
	      beta[count_seg] * phi_b[count_seg] + gamma[count_seg];

	  }
	  else{
	    aP[index_] = 1;
	    b[index_] = phi0_v[ dom_mat[index_] ];
	  }
	  
	}
	else {
	  /* Correct perz region but incorrect pery region
	     so lets advance one pery further */
	  J--;
	  jper++;
	  
	} // end if(y <= pery[jper] * Ly)
      }
      else{
	/* Incorrect perz region, advance one perz further */
	K--;
	kper++;
	break;
      } // end if(z <= perz[kper] * Lz){
      
    } // end for (J=1; J<Ny-1; J++){
    
  } // end for (K=1; K<Nz-1; K++){

  /* SOUTH */
  Nperx = data->south.Nperx;
  Npery = 1;
  Nperz = data->south.Nperz;

  Nparts = data->south.parts;

  if(Nperx * Npery * Nperz != Nparts){
    error_msg(wins, "Number of parts in boundary does not match");
  }
  
  perx = data->south.perx;
  pery = NULL;
  perz = data->south.perz;
  
  alpha = data->south.alpha;
  beta = data->south.beta;
  gamma = data->south.gamma;
  phi_b = data->south.phi_b;

  J = 0;
  a_m = aN;
  D_m = Delta_yN;
  
  kper = 0;
  count_seg = 0;

  for (K=1; K<Nz-1; K++){
    
    iper = 0;
    
    for (I=1; I<Nx-1; I++){
      
      index_ = K*Nx*Ny + J*Nx + I;

      x = nodes_x[index_];
      z = nodes_z[index_];

      if(z <= perz[kper] * Lz){
	/* Inside correct perz region */
    
	if(x <= perx[iper] * Lx){
	  /* inside correct perx region */

	  count_seg = kper*Nperx + iper;

	  if(P_solve_phase_OK[index_]){
	  
	    aP[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_] +
	      beta[count_seg];
	    a_m[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_];
	    b[index_] =
	      beta[count_seg] * phi_b[count_seg] + gamma[count_seg];

	  }
	  else{
	    aP[index_] = 1;
	    b[index_] = phi0_v[ dom_mat[index_] ];
	  }
	  
	}
	else {
	  /* Correct perz region but incorrect perx region
	     so lets advance one perx further */
	  I--;
	  iper++;

	} // end if(x <= perx[iper] * Lx)
      }
      else{
	/* Incorrect perz region, advance one perz further */
	K--;
	kper++;
	break;
      } // end if(z <= perz[kper] * Lz){
      
    } // end for (I=1; J<Nx-1; I++){
    
  } // end for (K=1; K<Nz-1; K++){
  
  /* NORTH */
  Nperx = data->north.Nperx;
  Npery = 1;
  Nperz = data->north.Nperz;

  Nparts = data->north.parts;

  if(Nperx * Npery * Nperz != Nparts){
    error_msg(wins, "Number of parts in boundary does not match");
  }
  
  perx = data->north.perx;
  pery = NULL;
  perz = data->north.perz;
  
  alpha = data->north.alpha;
  beta = data->north.beta;
  gamma = data->north.gamma;
  phi_b = data->north.phi_b;

  J = Nx-1;
  a_m = aS;
  D_m = Delta_yS;

  kper = 0;
  count_seg = 0;

  for (K=1; K<Nz-1; K++){
    
    iper = 0;
    
    for (I=1; I<Nx-1; I++){
      
      index_ = K*Nx*Ny + J*Nx + I;

      x = nodes_x[index_];
      z = nodes_z[index_];

      if(z <= perz[kper] * Lz){
	/* Inside correct perz region */
    
	if(x <= perx[iper] * Lx){
	  /* inside correct perx region */

	  count_seg = kper*Nperx + iper;

	  if(P_solve_phase_OK[index_]){
	  
	    aP[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_] +
	      beta[count_seg];
	    a_m[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_];
	    b[index_] =
	      beta[count_seg] * phi_b[count_seg] + gamma[count_seg];

	  }
	  else{
	    aP[index_] = 1;
	    b[index_] = phi0_v[ dom_mat[index_] ];
	  }
	  
	}
	else {
	  /* Correct perz region but incorrect perx region
	     so lets advance one perx further */
	  I--;
	  iper++;

	} // end if(x <= perx[iper] * Lx)
      }
      else{
	/* Incorrect perz region, advance one perz further */
	K--;
	kper++;
	break;
      } // end if(z <= perz[kper] * Lz){
      
    } // end for (I=1; J<Nx-1; I++){
    
  } // end for (K=1; K<Nz-1; K++){
  
  /* BOTTOM */
  Nperx = data->bottom.Nperx;
  Npery = data->bottom.Npery;
  Nperz = 1;

  Nparts = data->bottom.parts;

  if(Nperx * Npery * Nperz != Nparts){
    error_msg(wins, "Number of parts in boundary does not match");
  }
  
  perx = data->bottom.perx;
  pery = data->bottom.pery;
  perz = NULL;
  
  alpha = data->bottom.alpha;
  beta = data->bottom.beta;
  gamma = data->bottom.gamma;
  phi_b = data->bottom.phi_b;

  K = 0;
  a_m = aT;
  D_m = Delta_zT;
  
  jper = 0;
  count_seg = 0;

  for (J=1; J<Ny-1; J++){
    
    iper = 0;
    
    for (I=1; I<Nx-1; I++){
      
      index_ = K*Nx*Ny + J*Nx + I;

      x = nodes_x[index_];
      y = nodes_y[index_];

      if(y <= pery[jper] * Ly){
	/* Inside correct pery region */
    
	if(x <= perx[iper] * Lx){
	  /* inside correct perx region */

	  count_seg = jper*Nperx + iper;

	  if(P_solve_phase_OK[index_]){
	  
	    aP[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_] +
	      beta[count_seg];
	    a_m[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_];
	    b[index_] =
	      beta[count_seg] * phi_b[count_seg] + gamma[count_seg];
	  }
	  else{
	    aP[index_] = 1;
	    b[index_] = phi0_v[ dom_mat[index_] ];
	  }
	  
	}
	else {
	  /* Correct pery region but incorrect perx region
	     so lets advance one perx further */
	  I--;
	  iper++;

	} // end if(x <= perx[iper] * Lx)
      }
      else{
	/* Incorrect pery region, advance one pery further */
	J--;
	jper++;
	break;
      } // end if(y <= pery[jper] * Ly){
      
    } // end for (I=1; J<Nx-1; I++){
    
  } // end for (J=1; J<Ny-1; J++){

    /* TOP */
  Nperx = data->top.Nperx;
  Npery = data->top.Npery;
  Nperz = 1;

  Nparts = data->top.parts;

  if(Nperx * Npery * Nperz != Nparts){
    error_msg(wins, "Number of parts in boundary does not match");
  }
  
  perx = data->top.perx;
  pery = data->top.pery;
  perz = NULL;
  
  alpha = data->top.alpha;
  beta = data->top.beta;
  gamma = data->top.gamma;
  phi_b = data->top.phi_b;

  K = Nz-1;
  a_m = aB;
  D_m = Delta_zB;
  
  jper = 0;
  count_seg = 0;

  for (J=1; J<Ny-1; J++){

    iper = 0;
    
    for (I=1; I<Nx-1; I++){
      
      index_ = K*Nx*Ny + J*Nx + I;

      x = nodes_x[index_];
      y = nodes_y[index_];

      if(y <= pery[jper] * Ly){
	/* Inside correct pery region */
    
	if(x <= perx[iper] * Lx){
	  /* inside correct perx region */

	  count_seg = jper*Nperx + iper;

	  if(P_solve_phase_OK[index_]){
	  
	    aP[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_] +
	      beta[count_seg];
	    a_m[index_] =
	      alpha[count_seg] * gamma_m[index_] / D_m[index_];
	    b[index_] =
	      beta[count_seg] * phi_b[count_seg] + gamma[count_seg];

	  }
	  else{
	    aP[index_] = 1;
	    b[index_] = phi0_v[ dom_mat[index_] ];
	  }
	  
	}
	else {
	  /* Correct pery region but incorrect perx region
	     so lets advance one perx further */
	  I--;
	  iper++;

	} // end if(x <= perx[iper] * Lx)
      }
      else{
	/* Incorrect pery region, advance one pery further */
	J--;
	jper++;
	break;
      } // end if(y <= pery[jper] * Ly){
      
    } // end for (I=1; J<Nx-1; I++){
    
  } // end for (J=1; J<Ny-1; J++){
  
  return;
}

/* COMPUTE_PHI_SC_AT_INTERFACE_DIRICHLET_3D */
/**
   For Dirichlet boundary condition
   Sets:
   Array (double, NxNyNz) SC_vol_m: source term due to Dirichlet boundary
   
   MODIFIED: 02-07-2020
   
   @param data
   
   @see __get_b_values_Dirichlet
   
   @return void
   
   MODIFIED: 17-07-2020
*/
void compute_phi_SC_at_interface_Dirichlet_3D(Data_Mem *data){
  
  int count, i, j, k, I, J, K;
  int index_, index_u, index_v, index_w;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int nx = data->nx;
  const int ny = data->ny;
  
  const int Dirichlet_boundary_cell_count =
    data->Dirichlet_boundary_cell_count;

  const int Dirichlet_Isoflux_convective_boundary_OK = data->Dirichlet_Isoflux_convective_boundary_OK;
  
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  const int *T_solve_phase_OK = data->T_solve_phase_OK;
  const int *B_solve_phase_OK = data->B_solve_phase_OK;
  
  const int *dom_mat = data->domain_matrix;

  const int *I_Dirichlet_v = data->I_Dirichlet_v;
  const int *J_Dirichlet_v = data->J_Dirichlet_v;
  const int *K_Dirichlet_v = data->K_Dirichlet_v;
    
  const int *Dirichlet_boundary_indexs =
    data->Dirichlet_boundary_indexs;

  /* phi at boundary and midpoint */
  double phi_b, phi_m;
  double dphidxb_, dphidyb_, dphidzb_;

  double eps_val, m, Da;

  double Fe, Fw, Fn, Fs, Ft, Fb;
  double Ax, Ay, Az;

  double Jxmol, Jxconv, Jx;
  double Jymol, Jyconv, Jy;
  double Jzmol, Jzconv, Jz;

  const double eps_dis_tol = data->eps_dist_tol;

  double *SC_vol_m = data->SC_vol_m;

  double *dphidxb = data->dphidxb;
  double *dphidyb = data->dphidyb;
  double *dphidzb = data->dphidzb;
  
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;

  const double *fw = data->fw;
  const double *fs = data->fs;
  const double *fb = data->fb;

  const double *fe = data->fe;
  const double *fn = data->fn;
  const double *ft = data->ft;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  const double *w = data->w_at_faces;

  const double *phi = data->phi;
  const double *phi_b_v = data->phi_b_v;

  const double *gamma_m = data->gamma_m;
  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;

  for(count=0; count<Dirichlet_boundary_cell_count; count++){
    
    index_ = Dirichlet_boundary_indexs[count];
      
    I = I_Dirichlet_v[count];
    J = J_Dirichlet_v[count];
    K = K_Dirichlet_v[count];
    
    i = I-1;
    j = J-1;
    k = K-1;
    
    index_u = K*nx*Ny + J*nx + i;
    index_v = K*Nx*ny + j*Nx + I;
    index_w = k*Nx*Ny + J*Nx + I;

    Ax = faces_x[index_u];
    Ay = faces_y[index_v];
    Az = faces_z[index_w];
    
    SC_vol_m[index_] = 0;

    if(!W_solve_phase_OK[index_]){
      /* There is solid (non-solvable) boundary covering W */
      
      eps_val  = eps_x[index_];

      Da = (1 - fe[index_])*Delta_xE[index_] + eps_val*Delta_xW[index_];
      
      m = Delta_x[index_] / Da;
      
      /* Convective parameter */
      Fw = rho_m[index_] * cp_m[index_] * u[index_u] * Ax;
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_-1] - 1 ];

      /* Get dphidxb for case W (-+-) : dir_fact == -1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidxb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_+1],
			       Delta_xW[index_], Delta_xE[index_], -1);

      dphidxb[index_] = dphidxb_;

      Jxmol = - gamma_m[index_] * dphidxb_ * Ax * m;
      Jxconv = Fw * phi_m;
      Jxconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jxconv : 0;
      
      Jx = Jxmol + Jxconv;
      
      /* Contribution to SC_vol_m (W: -+) */
      SC_vol_m[index_] += Jx;
    }
    
    if(!E_solve_phase_OK[index_]){
      
      eps_val = eps_x[index_];
      
      Da = (1 - fw[index_])*Delta_xW[index_] + eps_val*Delta_xE[index_];
      
      m = Delta_x[index_] / Da;
	  
      /* Convective parameter */
      Fe = rho_m[index_] * cp_m[index_] * u[index_u+1] * Ax;
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_+1] - 1 ];
      /* Get dphidxb for case E (+-+) : dir_fact == +1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidxb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_-1],
			       Delta_xE[index_], Delta_xW[index_], 1);

      dphidxb[index_] = dphidxb_;

      Jxmol = - gamma_m[index_] * dphidxb_ * Ax * m;
      Jxconv = Fe * phi_m;
      Jxconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jxconv : 0;
      
      Jx = Jxmol + Jxconv;

      /* Contribution to SC_vol_m (E: +-) */
      SC_vol_m[index_] +=  - Jx;
    }

    if(!S_solve_phase_OK[index_]){
      eps_val = eps_y[index_];

      Da = (1 - fn[index_])*Delta_yN[index_] + eps_val*Delta_yS[index_];
      
      m = Delta_y[index_] / Da;
      /* Convective parameter */
      Fs = rho_m[index_] * cp_m[index_] * v[index_v] * Ay;
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_-Nx] - 1 ];

      /* Get dphidyb for case S (-+-) : dir_fact == -1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidyb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_+Nx],
			       Delta_yS[index_], Delta_yN[index_], -1);

      dphidyb[index_] = dphidyb_;

      Jymol = - gamma_m[index_] * dphidyb_ * Ay * m;
      Jyconv = Fs * phi_m;
      Jyconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jyconv : 0;
      
      Jy = Jymol + Jyconv;

      /* Contribution to SC_vol_m (S: -+) */
      SC_vol_m[index_] += Jy;
    }

    if(!N_solve_phase_OK[index_]){
      eps_val = eps_y[index_];
      
      Da = (1 - fs[index_])*Delta_yS[index_] + eps_val*Delta_yN[index_];
      
      m = Delta_y[index_] / Da;
      /* Convective parameter */
      Fn = rho_m[index_] * cp_m[index_] * v[index_v+Nx] * Ay;
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_+Nx] - 1 ];

      /* Get dphidyb for case N (+-+) : dir_fact == +1
	 and phi_m */
      __get_b_values_Dirichlet(&dphidyb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_-Nx],
			       Delta_yN[index_], Delta_yS[index_], 1);

      dphidyb[index_] = dphidyb_;

      Jymol = - gamma_m[index_] * dphidyb_ * Ay * m;
      Jyconv = Fn * phi_m;
      Jyconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jyconv : 0;
      
      Jy = Jymol + Jyconv;

      /* Contribution to SC_vol_m (N: +-) */
      SC_vol_m[index_] +=  - Jy;
    }

    if(!B_solve_phase_OK[index_]){
      eps_val = eps_z[index_];

      Da = (1 - ft[index_])*Delta_zT[index_] + eps_val*Delta_zB[index_];
      
      m = Delta_z[index_] / Da;
      /* Convective parameter */
      Fb = rho_m[index_] * cp_m[index_] * w[index_w] * Az;
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_-Nx*Ny] - 1 ];

      /* Get dphidzb for case B (-+-) : dir_fact == -1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidzb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_+Nx*Ny],
			       Delta_zB[index_], Delta_zT[index_], -1);

      dphidzb[index_] = dphidzb_;

      Jzmol = - gamma_m[index_] * dphidzb_ * Az * m;
      Jzconv = Fb * phi_m;
      Jzconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jzconv : 0;
      
      Jz = Jzmol + Jzconv;

      /* Contribution to SC_vol_m (B: -+) */
      SC_vol_m[index_] +=  Jz;
    }

    if(!T_solve_phase_OK[index_]){
      eps_val = eps_z[index_];

      Da = (1 - fb[index_])*Delta_zB[index_] + eps_val*Delta_zT[index_];
      
      m = Delta_z[index_] / Da;
      /* Convective parameter */
      Ft = rho_m[index_] * cp_m[index_] * w[index_w+Nx*Ny] * Az;
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_+Nx*Ny] - 1 ];

      /* Get dphidzb for case T (+-+) : dir_fact == +1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidzb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_-Nx*Ny],
			       Delta_zT[index_], Delta_zB[index_], 1);

      dphidzb[index_] = dphidzb_;

      Jzmol = - gamma_m[index_] * dphidzb_ * Az * m;
      Jzconv = Ft * phi_m;
      Jzconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jzconv : 0;
      
      Jz = Jzmol + Jzconv;

      /* Contribution to SC_vol_m (T: +-) */
      SC_vol_m[index_] +=  - Jz;
    }
      
  }

  return;
}

/* COMPUTE_PHI_SC_AT_INTERFACE_3D */
/**
   Sets:
   Array (double, NxNyNz) SC_vol_m: source term on energy equation
   For conjugate heat transfer:
   - Diffusive part is calculated on function compute_near_boundary_molecular_flux_*
   - Convective part is calculated on function compute_near_boundary_convective_flux
   For Isoflux boundary condition:
   - Diffusive and convective parts are calculated on function compute_phi_SC_at_interface_Isoflux.
   For Dirichlet boundary condition:
   - Diffusive and convective parts are calculated on function compute_phi_SC_at_interface_Dirichlet.
   
   MODIFIED: 02-07-2020
   
   @param data
   
   @see compute_near_boundary_molecular_flux_Tsutsumi_3D, compute_near_boundary_molecular_flux_Tsutsumi_def_3D
   
   @see compute_near_boundary_molecular_flux_Sato_3D, compute_near_boundary_molecular_flux_Sato_def_3D
   
   @see compute_near_boundary_molecular_flux_experimental_3D, compute_phi_SC_at_interface_Dirichlet_3D
   
   @see compute_phi_SC_at_interface_Isoflux_3D
   
   @return void
   
   MODIFIED: 17-07-2020
*/
void compute_phi_SC_at_interface_3D(Data_Mem *data){
  
  int count, index_;
  
  const int interface_molecular_flux_model =
    data->interface_molecular_flux_model;
  const int Dirichlet_boundary_cell_count =
    data->Dirichlet_boundary_cell_count;
  const int Isoflux_boundary_cell_count =
    data->Isoflux_boundary_cell_count;
  const int Conjugate_boundary_cell_count =
    data->Conjugate_boundary_cell_count;
  
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;
  
  const int *dom_mat = data->domain_matrix;
  
  const int *Conjugate_boundary_indexs =
    data->Conjugate_boundary_indexs;

  double Ax, Ay, Az;
    
  const double alpha_SC_phi = data->alpha_SC_phi;

  double *SC_vol_m_0 = data->SC_vol_m_0;
  double *SC_vol_m = data->SC_vol_m;

  /* Only for conjugate transfer */
  const double *J_x = data->J_x;
  const double *J_y = data->J_y;
  const double *J_z = data->J_z;
  
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  
  /* Update fluxes at interface 
     in case of conjugate heat transfer */
  /* Only valid for fluid and solid interfaces as of yet */

  if(Conjugate_boundary_cell_count && (interface_molecular_flux_model < 6)){

    switch(interface_molecular_flux_model){
    case 1: compute_near_boundary_molecular_flux_Tsutsumi_3D(data);
      break;
      
    case 2: compute_near_boundary_molecular_flux_Tsutsumi_def_3D(data);
      break;
      
    case 3: compute_near_boundary_molecular_flux_Sato_3D(data);
      break;
      
    case 4: compute_near_boundary_molecular_flux_Sato_at_face_3D(data);
      break;
      
    case 5: compute_near_boundary_molecular_experimental_3D(data);
      break;
    }
   
    compute_near_boundary_convective_flux_3D(data);
    
    for(count=0; count<Conjugate_boundary_cell_count; count++){
      
      index_ = Conjugate_boundary_indexs[count];

      Ax = Delta_y[index_] * Delta_z[index_];
      Ay = Delta_x[index_] * Delta_z[index_];
      Az = Delta_x[index_] * Delta_y[index_];
	  
      if(dom_mat[index_] != 0){
	/* Solid phase */
	SC_vol_m[index_] = 
	  (-1) * E_is_fluid_OK[index_] * J_x[index_] * Ax +
	  (-1) * N_is_fluid_OK[index_] * J_y[index_] * Ay +
	  (-1) * T_is_fluid_OK[index_] * J_z[index_] * Az +
	  W_is_fluid_OK[index_] * J_x[index_] * Ax + 
	  S_is_fluid_OK[index_] * J_y[index_] * Ay +
	  B_is_fluid_OK[index_] * J_z[index_] * Az;
	    
	SC_vol_m[index_]  = alpha_SC_phi * SC_vol_m[index_] +
	  (1 - alpha_SC_phi) * SC_vol_m_0[index_];
	
      }
      else{
	/* Fluid phase */
	SC_vol_m[index_] = 
	  (-1) * (1 - E_is_fluid_OK[index_]) * J_x[index_] * Ax +
	  (-1) * (1 - N_is_fluid_OK[index_]) * J_y[index_] * Ay +
	  (-1) * (1 - T_is_fluid_OK[index_]) * J_z[index_] * Az +
	  (1 - W_is_fluid_OK[index_]) * J_x[index_] * Ax +
	  (1 - S_is_fluid_OK[index_]) * J_y[index_] * Ay +
	  (1 - B_is_fluid_OK[index_]) * J_z[index_] * Az;
	    
	SC_vol_m[index_]  = alpha_SC_phi * SC_vol_m[index_] +
	  (1 - alpha_SC_phi) * SC_vol_m_0[index_];
	
      }
    }
  } /* end if(Conjugate_boundary_cell_count) */
  
  /* Dirichlet and Isoflux cases include 
     the convection component and write directly into SC_vol */
  if(Dirichlet_boundary_cell_count){
    /* Now the Dirichlet case */
    compute_phi_SC_at_interface_Dirichlet_3D(data);
  }
  if(Isoflux_boundary_cell_count){
    /* Now the Isoflux case */
    compute_phi_SC_at_interface_Isoflux_3D(data);
  }
  
  return;
}

/* COEFFS_PHI_POWER_LAW_3D */
/**

   Sets:
   Structure coeffs_phi: coefficients of discretized equation for internal nodes
   
   @param data
   
   @see set_D_phi_3D, set_F_phi_3D
   
   @see compute_near_boundary_molecular_flux_experimental_SI, relax_coeffs_3D
   
   @return void
   
   MODIFIED: 17-07-2020
*/
void coeffs_phi_power_law_3D(Data_Mem *data){

  int I, J, K;
  int i, j, k;
  int index_;
  int index_u, index_v, index_w;
    
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;

  const int phi_dF_OK = data->phi_dF_OK;
  
  const int steady_state_OK = data->steady_state_OK;
  const int interface_molecular_flux_model = data->interface_molecular_flux_model;

  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  const int *T_solve_phase_OK = data->T_solve_phase_OK;
  const int *B_solve_phase_OK = data->B_solve_phase_OK;
  
  const int *W_coeff = data->W_coeff;
  const int *E_coeff = data->E_coeff;
  const int *S_coeff = data->S_coeff;
  const int *N_coeff = data->N_coeff;
  const int *B_coeff = data->B_coeff;
  const int *T_coeff = data->T_coeff;

  const int *dom_mat = data->domain_matrix;
  const int *curve_phase_change_OK = data->curve_phase_change_OK;
    
  double De, Dw, Dn, Ds, Dt, Db;
  double Fe, Fw, Fn, Fs, Ft, Fb;
  double Pe, Pw, Pn, Ps, Pt, Pb, PL_arg;
  double SP = 0, SC = 0, SPC = 0;
  
  double aP0 = 0;
  double dF = 0;

  double eps_val, m, Da;

  const double dt = data->dt[3] / 3; // For 3D case

  double *aE = data->coeffs_phi.aE;
  double *aW = data->coeffs_phi.aW;
  double *aN = data->coeffs_phi.aN;
  double *aS = data->coeffs_phi.aS;
  double *aT = data->coeffs_phi.aT;
  double *aB = data->coeffs_phi.aB;
  
  double *aP = data->coeffs_phi.aP;
  double *b = data->coeffs_phi.b;
  
  const double *phi0_v = data->phi0_v;
  
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  const double *eps_z = data->epsilon_z;
  
  const double *SC_vol_m = data->SC_vol_m;
  const double *phi0 = data->phi0;
  const double *cp_m = data->cp_m;
  const double *rho_m = data->rho_m;
  const double *enthalpy_diff = data->enthalpy_diff;
  const double *gL_guess = data->gL_guess;
  const double *gL = data->gL;
  const double *phase_change_vol = data->phase_change_vol;
  const double *vol = data->vol_uncut;
  
  const double *Dphi_x = data->Dphi_x;
  const double *Dphi_y = data->Dphi_y;
  const double *Dphi_z = data->Dphi_z;
  
  const double *Fphi_x = data->Fphi_x;
  const double *Fphi_y = data->Fphi_y;
  const double *Fphi_z = data->Fphi_z;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *Dxw = data->Delta_xw;
  const double *Dys = data->Delta_ys;
  const double *Dzb = data->Delta_zb;
  
  const double *Dxe = data->Delta_xe;
  const double *Dyn = data->Delta_yn;
  const double *Dzt = data->Delta_zt;

  set_D_phi_3D(data);
  set_F_phi_3D(data);

  /* Coefficients computation */
  
#pragma omp parallel for default(none)				\
  firstprivate(aP0)						\
  private(i, j, k, I, J, K, index_, index_u, index_v, index_w,	\
	  eps_val, m, Da, dF,					\
	  SC, SPC, SP,						\
	  De, Dw, Dn, Ds, Dt, Db,				\
	  Fe, Fw, Fn, Fs, Ft, Fb,				\
	  Pe, Pw, Pn, Ps, Pt, Pb, PL_arg)			\
  shared(dom_mat, P_solve_phase_OK,				\
	 E_solve_phase_OK, W_solve_phase_OK,			\
	 N_solve_phase_OK, S_solve_phase_OK,			\
	 T_solve_phase_OK, B_solve_phase_OK,			\
	 eps_x, eps_y, eps_z,					\
	 aP, aE, aW, aN, aS, aT, aB, b,				\
	 curve_phase_change_OK, enthalpy_diff,			\
	 gL, gL_guess, phase_change_vol,			\
	 SC_vol_m, phi0, vol,					\
	 Dphi_x, Dphi_y, Dphi_z, Fphi_x, Fphi_y, Fphi_z,	\
	 cp_m, rho_m, phi0_v,					\
	 E_coeff, W_coeff, N_coeff, S_coeff, T_coeff, B_coeff,	\
	 Delta_x, Delta_y, Delta_z,				\
	 Dxe, Dxw, Dyn, Dys, Dzt, Dzb,				\
	 Delta_xE, Delta_xW,					\
	 Delta_yN, Delta_yS,					\
	 Delta_zT, Delta_zB)
  
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	i = I-1;
	j = J-1;
	k = K-1;
	
	index_ = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;
	index_v = K*Nx*ny + j*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;
	
	SC = 0;
	SPC = 0;
	SP = 0;
	
	if(!steady_state_OK){
	  aP0 = rho_m[index_] * cp_m[index_] * vol[index_] / dt;
	}

	/* Diffusion parameters*/
	De = Dphi_x[index_u+1]     * E_coeff[index_];
	Dw = Dphi_x[index_u]       * W_coeff[index_];
	Dn = Dphi_y[index_v+Nx]    * N_coeff[index_];
	Ds = Dphi_y[index_v]       * S_coeff[index_];
	Dt = Dphi_z[index_w+Nx*Ny] * T_coeff[index_];
	Db = Dphi_z[index_w]       * B_coeff[index_];
	
	/* Corrections to parameters
	   in account for Dirichlet or Isoflux effects */
	if(!E_solve_phase_OK[index_]){
	  
	  eps_val = eps_x[index_];
	  
	  Da = Dxw[index_] + eps_val*Delta_xE[index_];
	  
	  m = Delta_x[index_] / Da;
	  
	  De = De * m;
	  Dw = Dw * m;
	}
	if(!W_solve_phase_OK[index_]){

	  eps_val = eps_x[index_];
	  
	  Da = Dxe[index_] + eps_val*Delta_xW[index_];
	  
	  m = Delta_x[index_] / Da;
	  
	  De = De * m;
	  Dw = Dw * m;
	}
		
	if(!N_solve_phase_OK[index_]){

	  eps_val = eps_y[index_];
      
	  Da = Dys[index_] + eps_val*Delta_yN[index_];
      
	  m = Delta_y[index_] / Da;
      
	  Dn = Dn * m;
	  Ds = Ds * m;
	}

	if(!S_solve_phase_OK[index_]){
	  
	  eps_val = eps_y[index_];
      
	  Da = Dyn[index_] + eps_val*Delta_yS[index_];
      
	  m = Delta_y[index_] / Da;
      
	  Dn = Dn * m;
	  Ds = Ds * m;
	}
	
	if(!T_solve_phase_OK[index_]){

	  eps_val = eps_z[index_];
      
	  Da = Dzb[index_] + eps_val*Delta_zT[index_];
      
	  m = Delta_z[index_] / Da;

	  Dt = Dt * m;
	  Db = Db * m;
	  
	}

	if(!B_solve_phase_OK[index_]){

	  eps_val = eps_z[index_];
	  
	  Da = Dzt[index_] + eps_val*Delta_zB[index_];
      
	  m = Delta_z[index_] / Da;

	  Dt = Dt * m;
	  Db = Db * m;
	  
	}
	
	/* Convective parameters */
	
	Fe = Fphi_x[index_u+1]     * E_coeff[index_];
	Fw = Fphi_x[index_u]       * W_coeff[index_];
	Fn = Fphi_y[index_v+Nx]    * N_coeff[index_];
	Fs = Fphi_y[index_v]       * S_coeff[index_];
	Ft = Fphi_z[index_w+Nx*Ny] * T_coeff[index_];
	Fb = Fphi_z[index_w]       * B_coeff[index_];
	
	if(!P_solve_phase_OK[index_]){
	  /* We are "not solving" this phase */
	  aP[index_] = 1;
	  aE[index_] = 0;
	  aW[index_] = 0;
	  aN[index_] = 0;
	  aS[index_] = 0;
	  aT[index_] = 0;
	  aB[index_] = 0;
	  b[index_] = phi0_v[ dom_mat[index_] ];
	}
	else{
	  /* This phase must be addressed */
	  if(dom_mat[index_] != 0){
	    /* The solid (no convection) case */
	    aE[index_] = De * E_solve_phase_OK[index_] * E_coeff[index_];
	    aW[index_] = Dw * W_solve_phase_OK[index_] * W_coeff[index_];
	    aN[index_] = Dn * N_solve_phase_OK[index_] * N_coeff[index_];
	    aS[index_] = Ds * S_solve_phase_OK[index_] * S_coeff[index_];
	    aT[index_] = Dt * T_solve_phase_OK[index_] * T_coeff[index_];
	    aB[index_] = Db * B_solve_phase_OK[index_] * B_coeff[index_];
	    
	    if((curve_phase_change_OK[ dom_mat[index_] - 1 ]) &&
	       !steady_state_OK){
	      /* EC 16 Voller  */
	      SPC = enthalpy_diff[index_] * 
		(gL[index_] - gL_guess[index_]) *
		phase_change_vol[index_] / dt;
	    }
	  }
	  else{
	    /* The fluid (convection) case */
	    /* Peclet number */
	    Pe = Fe/De; Pw = Fw/Dw;
	    Pn = Fn/Dn; Ps = Fs/Ds;
	    Pt = Ft/Dt; Pb = Fb/Db;
	    /* Power law */
	    PL_arg = powf(1.0 - 0.1 * fabs(Pe), 5.0);
	    aE[index_] = E_solve_phase_OK[index_] * E_coeff[index_] * De *
	      ( MAX(PL_arg, 0) + MAX(-Pe, 0) );
	    
	    PL_arg = powf(1.0 - 0.1 * fabs(Pw), 5.0);
	    aW[index_] = W_solve_phase_OK[index_] * W_coeff[index_] * Dw *
	      ( MAX(PL_arg, 0) + MAX(Pw, 0) );
	    
	    PL_arg = powf(1.0 - 0.1 * fabs(Pn), 5.0);
	    aN[index_] = N_solve_phase_OK[index_] * N_coeff[index_] * Dn *
	      ( MAX(PL_arg, 0) + MAX(-Pn, 0) );
	    
	    PL_arg = powf(1.0 - 0.1 * fabs(Ps), 5.0);
	    aS[index_] = S_solve_phase_OK[index_] * S_coeff[index_] * Ds *
	      ( MAX(PL_arg, 0) + MAX(Ps, 0) );

	    PL_arg = powf(1.0 - 0.1 * fabs(Pt), 5.0);
	    aT[index_] = T_solve_phase_OK[index_] * T_coeff[index_] * Dt *
	      ( MAX(PL_arg, 0) + MAX(-Pt, 0) );
	    
	    PL_arg = powf(1.0 - 0.1 * fabs(Pb), 5.0);
	    aB[index_] = B_solve_phase_OK[index_] * B_coeff[index_] * Db *
	      ( MAX(PL_arg, 0) + MAX(Pb, 0) );
	  }

	  /* Mainly for testing the two forms of the phi equation */
	  dF = Fe - Fw + Fn - Fs + Ft - Fb;
	  dF = phi_dF_OK ? dF : 0;
		
	  aP[index_] =
	    aE[index_] + aW[index_] +
	    aN[index_] + aS[index_] +
	    aT[index_] + aB[index_] +
	    aP0 - SP * vol[index_] + dF;
	  
	  /* With SC_m computes arrays of SC due to 
	     internal boundaries regardless of the case 
	     (Dirichlet, Isoflux, Conjugate) */
	  b[index_]  =
	    SC_vol_m[index_] + SC * vol[index_] + SPC + aP0 * phi0[index_];
	  
	} /* end else if(!P_solve_phase_OK[index_]) */
	
      } /* end for I */
    } /* end for J */
  } /* end for K */
  
  /* Conjugate heat transfer semi-implicit scheme */

  if(interface_molecular_flux_model == 6){
    //    compute_near_boundary_molecular_flux_experimental_SI_3D(data); <================================================= Falta arreglar esta función solamente para coeffs_phi_3D
  }

  /* Relax coefficients */
  relax_coeffs_3D(data->phi, P_solve_phase_OK, 1, data->coeffs_phi,
		  Nx, Ny, Nz, data->alpha_phi);
  
  return;
}

/* SET_F_PHI_3D */
/*****************************************************************************/
/**
   @param data

   @return void
  
   MODIFIED: 01-07-2020
*/
void set_F_phi_3D(Data_Mem *data){

  int I, J, K;
  int i, j, k;
  
  int index_;
  int index_face; // index_u, index_v, index_w

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  double rho_face, cp_face;

  double *Fphi_x = data->Fphi_x;
  double *Fphi_y = data->Fphi_y;
  double *Fphi_z = data->Fphi_z;

  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;
  
  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;
  
  const double *um = data->u_at_faces;
  const double *vm = data->v_at_faces;
  const double *wm = data->w_at_faces;

  /* Compute base convective parameters */
  /* Convective forces parameters 
     THESE INTERPOLATION ARE ACROSS PHASES, 
     CHECK IT OUT IF THIS WORKS */

  /* Fphi_x */

#pragma omp parallel for default(none)				\
  private(i, J, K, index_face, index_, rho_face, cp_face)	\
  shared(rho_m, cp_m, faces_x, um, Fphi_x)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	index_face = K*nx*Ny + J*nx + i;
	index_ = K*Nx*Ny + J*Nx + i;
	
	rho_face = 0.5 * (rho_m[index_] + rho_m[index_+1]);
	cp_face = 0.5 * (cp_m[index_] + cp_m[index_+1]);
	
	Fphi_x[index_face] = rho_face * cp_face * faces_x[index_face] * um[index_face];
	
      }
    }
  }
  
  /* Fphi_y */

#pragma omp parallel for default(none)				\
  private(I, j, K, index_face, index_, rho_face, cp_face)	\
  shared(rho_m, cp_m, faces_y, vm, Fphi_y)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
	
	index_face = K*Nx*ny + j*Nx + I;
	index_ = K*Nx*Ny + j*Nx + I;
      
	rho_face = 0.5 * (rho_m[index_] + rho_m[index_+Nx]);
	cp_face = 0.5 * (cp_m[index_] + cp_m[index_+Nx]);
	    
	Fphi_y[index_face] = rho_face * cp_face * faces_y[index_face] * vm[index_face];
	
      }
    }
  }
  
  /* Fphi_z */

#pragma omp parallel for default(none)				\
  private(I, J, k, index_face, index_, rho_face, cp_face)	\
  shared(rho_m, cp_m, faces_z, wm, Fphi_z)

  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_face = k*Nx*Ny + J*Nx + I;
	index_ = k*Nx*Ny + J*Nx + I;
      
	rho_face = 0.5 * (rho_m[index_] + rho_m[index_+Nx*Ny]);
	cp_face = 0.5 * (cp_m[index_] + cp_m[index_+Nx*Ny]);
	    
	Fphi_z[index_face] = rho_face * cp_face * faces_z[index_face] * wm[index_face];
	
      }
    }
  }
  
  return;
}

/* SET_D_PHI_3D */
/*****************************************************************************/
/**
   @param data

   @return void
  
   MODIFIED: 01-07-2020
*/

void set_D_phi_3D(Data_Mem *data){

  int I, J, K;
  int i, j, k;
  int index_;
  int index_face; // index_u, index_v, index_w

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  double gamma_face;

  double *Dphi_x = data->Dphi_x;
  double *Dphi_y = data->Dphi_y;
  double *Dphi_z = data->Dphi_z;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *fe = data->fe;
  const double *fn = data->fn;
  const double *ft = data->ft;

  const double *faces_x = data->faces_x;
  const double *faces_y = data->faces_y;
  const double *faces_z = data->faces_z;

  const double *gamma_m = data->gamma_m;
  
  /* Compute base diffusion parameters, 
     HERE IT IS ASSUMED NO VARIATION OF GAMMA WITH PHI */

  /* Dphi_x */

#pragma omp parallel for default(none)			\
  private(i, J, K, index_face, index_, gamma_face)	\
  shared(gamma_m, fe, faces_x, Delta_xE, Dphi_x)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	index_face = K*nx*Ny + J*nx + i;
	index_ = K*Nx*Ny + J*Nx + i;
	
	gamma_face = gamma_m[index_] * gamma_m[index_+1] /
	  ((1 - fe[index_]) * gamma_m[index_+1] + fe[index_] * gamma_m[index_]);
	
	Dphi_x[index_face] = gamma_face * faces_x[index_face] / Delta_xE[index_];
	
      }
    }
  }
  
  /* Dphi_y */

#pragma omp parallel for default(none) private(I, j, K, index_face, index_, gamma_face) \
  shared(gamma_m, fn, faces_y, Delta_yN, Dphi_y)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
	
	index_face = K*Nx*ny + j*Nx + I;
	index_ = K*Nx*Ny + j*Nx + I;
	
	gamma_face = gamma_m[index_] * gamma_m[index_+Nx] / 
	  ((1 - fn[index_]) * gamma_m[index_+Nx] + fn[index_] * gamma_m[index_]);
	
	Dphi_y[index_face] = gamma_face * faces_y[index_face] / Delta_yN[index_];
	
      }
    }
  }
  
  /* Dphi_z */

#pragma omp parallel for default(none) private(I, J, k, index_face, index_, gamma_face) \
  shared(gamma_m, ft, faces_z, Delta_zT, Dphi_z)

  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_face = k*Nx*Ny + J*Nx + I;
	index_ = k*Nx*Ny + J*Nx + I;
	
	gamma_face = gamma_m[index_] * gamma_m[index_+Nx*Ny] / 
	  ((1 - ft[index_]) * gamma_m[index_+Nx*Ny] + ft[index_] * gamma_m[index_]);
	
	Dphi_z[index_face] = gamma_face * faces_z[index_face] / Delta_zT[index_];
	
      }
    }
  }
  
  return;
}

/* __GET_B_VALUES_DIRICHLET */
/*****************************************************************************/
/**

   Get derivative at Dirichlet boundary type, assuming boundary value is phi_b, 
   using Eq 14 of Sato and diferent lengths for points.

   dir_fact == -1 for solid boundary at W || S || B (-+-)
   dir_fact == +1 for solid boundary at E || N || T (+-+)

   @return double

   MODIFIED: 13-07-2020
*/
void __get_b_values_Dirichlet(double *der_at_b, double *phi_m,
			      const double e_, const double e_tol,
			      const double phi_b, const double phi_c,
			      const double phi_f,
			      const double Dic, const double Dcf,
			      const int dir_fact){
  
  /* Distance to boundary from c */
  const double Dbc = Dic * e_;
  
  /* Taking care of small eps_x values*/
  if(e_ > e_tol){
    
    *der_at_b = __asymm_b_der(phi_b, phi_c, phi_f, Dbc, Dcf, dir_fact);
    
    /* Eq 11 Sato */
    /* Same Dic so no correction needed */
    *phi_m = phi_c * (e_ - 0.5) / e_ + phi_b * 0.5 / e_;
  }
  else{
    
    *der_at_b = dir_fact * (phi_b - phi_f) / (Dcf + Dbc);
    *phi_m = phi_c + 0.5 * Dic * (phi_b - phi_f) / (Dcf + Dbc);
    
  }
  
  return;
}
