#include "include/some_defs.h"

char * get_next_valid_line(FILE *fPtr, char *buff,
			   const int buff_size);

/* TEXT_PARSER */
int text_parser(Data_Mem *data, const char *config_filename){

  FILE *fPtr = NULL;

  char buff[SIZE];
  char *token = NULL;

  int ret_value = 0;
  
  int n_curves = 0, n_lines = 0;
  int i = 0, I = 0, J = 0, K = 0;

  int turb_model = 0;
  
  int mu_initial_iter_flow = 50;
  int mu_final_iter_flow = 50;
  
  int per_mu_initial_iter_flow = 50;
  int per_mu_final_iter_flow = 50;
  
  fPtr = fopen(config_filename, "r");
    
  if(fPtr == NULL){
    printf("Could not open the file");
    ret_value = 1;
    return ret_value;
  }

  /* UTILIZANDO FGETS/SSCANF */

  /* EXAMPLE OF CHECK OF PARAMETER VALUE READING */
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "coupled_physics_OK %d", &(data->coupled_physics_OK)) != 1){
    printf("Error reading coupled_physics_OK\n");
    return 1;
  };

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_to_start_coupled_physics %d", &(data->it_to_start_coupled_physics)) != 1){
    printf("Error reading it_to_start_coupled_physics\n");
    return 1;
  };
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "graphs_flow_OK %d", &(data->graphs_flow_OK)) != 1){
    printf("Error reading graphs_flow_OK\n");
    return 1;
  };
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "animation_flow_OK %d", &(data->animation_flow_OK)) != 1){
    printf("Error reading animation_flow_OK\n");
    return 1;
  };
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "animation_flow_update_it %d",
	    &(data->animation_flow_update_it)) != 1){
    printf("Error reading animation_flow_update\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "graphs_phi_OK %d", &(data->graphs_phi_OK)) != 1){
    printf("Error reading graphs_phi_OK\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "animation_phi_OK %d", &(data->animation_phi_OK)) != 1){
    printf("Error reading animation_phi_OK\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "animation_phi_update_it %d",
	    &(data->animation_phi_update_it)) != 1){
    printf("Error reading animation_phi_update\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "semilog_OK %d", &(data->wins.semilog_OK)) != 1){
    printf("Error reading semilog_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "launch_server_OK %d", &(data->launch_server_OK)) != 1){
    printf("Error reading launch_server_OK\n");
    return 1;
  }

  /* Server max clients */
  data->sdata.max_clients = MAX_CLIENTS;

  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "server_update_it %d", &(data->server_update_it));
  
  get_grid_info_from_file(&(data->x_side), fPtr);

  get_grid_info_from_file(&(data->y_side), fPtr);

  get_grid_info_from_file(&(data->z_side), fPtr);
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "Lx %lf", &(data->Lx)) != 1){
    printf("Error reading Lx\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "Ly %lf", &(data->Ly)) != 1){
    printf("Error reading Ly\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "Lz %lf", &(data->Lz)) != 1){
    printf("Error reading Lz\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "solve_3D_OK %d", &(data->solve_3D_OK)) != 1){  
    printf("Error reading solve_3D_OK\n");
    return 1;
  }
    
  data->Nx = count_nodes_in_gi(&(data->x_side));
  data->Ny = count_nodes_in_gi(&(data->y_side));
  data->Nz = count_nodes_in_gi(&(data->z_side));
  
  if(!data->solve_3D_OK){
      data->Nz = 1;
  }

  data->wins.Nx = data->Nx;
  data->wins.Ny = data->Ny;
  data->wins.Nz = data->Nz;

  /* This function computes dx and dy for every case and
     may change the grid size
     so it is needded to be called before get_memory */
  smooth_grid(data);

  if(data->solve_3D_OK){
    data->cell_size = MIN(MIN(data->Lx/(data->Nx-2), data->Ly/(data->Ny-2)), data->Lz/(data->Nz-2));
  }
  else{
    data->cell_size = MIN(data->Lx/(data->Nx-2), data->Ly/(data->Ny-2));
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "write_data_on_file_OK %d", &(data->write_data_on_file_OK)) != 1){
    printf("Error reading write_data_on_file_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "write_cut_cell_data_on_file_OK %d",
	    &(data->write_cut_cell_data_on_file_OK)) != 1){
    printf("Error reading write_cut_cell_data_on_file_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "write_coeffs_OK %d",
	    &(data->write_coeffs_OK)) != 1){
    printf("Error reading write_coeffs_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "read_cut_cell_data_OK %d",
	 &(data->read_cut_cell_data_OK));

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "res_sigma_iter %d", &(data->res_sigma_iter)) != 1){
    printf("Error reading res_sigma_iter\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "post_process_OK %d", &(data->post_process_OK)) != 1){
    printf("Error reading post_process_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "add_p_source_when_finish_OK %d", &(data->add_p_source_when_finish_OK)) != 1){
    printf("Error reading add_p_source_when_finish_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "read_tmp_file_OK %d", &(data->read_tmp_file_OK)) != 1){
    printf("Error reading read_tmp_file_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "read_MAT_file_OK %d", &(data->read_MAT_file_OK)) != 1){
    printf("Error reading read_MAT_file_OK\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "solve_flow_OK %d", &(data->solve_flow_OK)) != 1){
    printf("Error reading solve_flow_OK\n");
    return 1;
  }

  if((data->solve_flow_OK > 3) || (data->solve_flow_OK < 0)){
    printf("Invalid value for solve_flow_OK\n");
    return 1;
  }

  /* SOLVE_FLOW PARAMETERS */
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "pv_coupling_algorithm %d", &(data->pv_coupling_algorithm)) != 1){
    printf("Error reading pv_coupling_algorithm\n");
    return 1;
  }

  if((data->pv_coupling_algorithm < 0) || (data->pv_coupling_algorithm > 3)){
    printf("Invalid value for pv_coupling_algorithm\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "flow_scheme %d", &(data->flow_scheme)) != 1){
    printf("Error reading flow_scheme\n");
    return 1;
  }

  if((data->flow_scheme < 0) || (data->flow_scheme > 2)){
    printf("Invalid value for flow_scheme\n"); 
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_iter_flow %d", &(data->max_iter_flow)) != 1){
    printf("Error reading max_iter_flow\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_iter_ADI %d", &(data->max_iter_ADI)) != 1){
    printf("max_iter_ADI\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_sub_iter_flow %d", &(data->max_sub_iter_flow)) != 1){
    printf("Error reading max_sub_iter_flow\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_iter_uvp %d", &(data->max_iter_uvp)) != 1){
    printf("Error reading max_iter_uvp\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_iter_ke %d", &(data->max_iter_ke)) != 1){
    printf("Error reading max_iter_ke\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_for_update_coeffs %d", &(data->it_for_update_coeffs)) != 1){
    printf("Error reading it_for_update_coeffs\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_for_update_k_coeffs %d", &(data->it_for_update_k_coeffs)) != 1){
    printf("Error reading it_for_update_k_coeffs\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_for_update_eps_coeffs %d", &(data->it_for_update_eps_coeffs)) != 1){
    printf("Error reading it_for_udpate_eps_coeffs\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "flow_steady_state_OK %d", &(data->flow_steady_state_OK)) != 1){
    printf("Error reading flow_steady_state_OK\n");
    return 1;
  }

  if((data->flow_steady_state_OK < 0) || (data->flow_steady_state_OK > 1)){
    printf("Invalid value for flow_steady_state_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "write_flow_transient_data_OK %d", &(data->write_flow_transient_data_OK)) != 1){
    printf("Error reading write_flow_transient_data_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "write_flow_transient_data_interval %lf", &(data->write_flow_transient_data_interval)) != 1){
    printf("Error reading write_flow_transient_data_interval\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "t_min t_max %lf %lf", &(data->tsaves[0]), &(data->tsaves[1]));

  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "dt_min dt_max %lf %lf", &(data->dt[1]),	&(data->dt[2]));
  data->dt[0] = 0;
  data->dt[3] = data->dt[1];

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "per_mu_initial_iter_flow %d", &per_mu_initial_iter_flow) != 1){
    printf("Error reading per_mu_initial_iter_flow\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "per_mu_final_iter_flow %d", &per_mu_final_iter_flow) != 1){
    printf("Error reading per_mu_final_iter_flow\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "mu_initial %lf", &(data->mu_initial)) != 1){
    printf("Error reading mu_initial\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_u %lf", &(data->alpha_u)) != 1){
    printf("Error reading alpha_u\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_v %lf", &(data->alpha_v)) != 1){
    printf("Error reading alpha_v\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_w %lf", &(data->alpha_w)) != 1){
    printf("Error reading alpha_w\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_p_corr %lf", &(data->alpha_p_corr)) != 1){
    printf("Error reading alpha_p_corr\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_p_coeff %lf", &(data->alpha_p_coeff)) != 1){
    printf("Error alpha_p_coeff\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "pol_tol %lf", &(data->pol_tol)) != 1){
    printf("Error reading pol_tol\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "eps_dist_tol %lf", &(data->eps_dist_tol)) != 1){
    printf("Error reading eps_dist_tol\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "kappa %lf", &(data->kappa)) != 1){
    printf("Error reading kappa\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "tol_flow %lf", &(data->tol_flow)) != 1){
    printf("Error reading tol_flow\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "tol_ADI %lf", &(data->tol_ADI)) != 1){
    printf("Error reading tol_ADI\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "consider_wall_shear_OK %d", &(data->consider_wall_shear_OK)) != 1){
    printf("Error reading consider_wall_shear_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "add_momentum_source_OK %d", &(data->add_momentum_source_OK)) != 1){
    printf("Error reading add_momentum_source_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "turb_model %d", &turb_model) != 1){
    printf("Error reading turb_model\n");
    return 1;
  }

  if(turb_model == 0){
    //info_msg(wins, "No turbulence model selected");
  }
  else if(turb_model == 1){
    //    info_msg(wins, "AKN turbulence model selected");
    data->C_mu = 0.09;
    data->C_1 = 1.5;
    data->C_2 = 1.9;
    data->C_sigma_k = 1.4;
    data->C_sigma_eps = 1.3;
  }
  else if(turb_model == 2){
    //   info_msg(wins, "CG turbulence model selected");
    data->C_mu = 0.09;
    data->C_1 = 1.44;
    data->C_2 = 1.92;
    data->C_sigma_k = 1;
    data->C_sigma_eps = 1.3;
  }
  else{
    turb_model = 0;
    //    alert_msg(wins, "Invalid turbulence model selected");
  }
  data->turb_model = turb_model;

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "read_y_wall_OK %d", &(data->read_y_wall_OK)) != 1){
    printf("Error reading read_y_wall_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_to_start_turb %d", &(data->it_to_start_turb)) != 1){
    printf("Error reading it_to_start_turb\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "use_y_wall_OK %d", &(data->use_y_wall_OK)) != 1){
    printf("Error reading use_y_wall_OK\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "mu_eff0 %lf", &(data->mu_eff0)) != 1){
    printf("Error reading mu_eff0\n");
    return 1;
  };

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_mix_l %lf", &(data->max_mix_l)) != 1){
    printf("Error reading max_mix_l\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "l0 %lf", &(data->l0)) != 1){
    printf("Error reading l0\n");
    return 1;
  }

  if(turb_model){
    if((data->max_mix_l > data->Lx) || (data->max_mix_l > data->Ly) ||
       (data->max_mix_l > data->Lz)){
      printf("Maximum mixing length value is incorrect\n");
      return -1;
    }
    
    if(data->l0 > data->max_mix_l){
      printf("Default mixing length value is incorrect\n");
      return -1;
    }
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "cbc %lf", &(data->cbc)) != 1){
    printf("Error reading cbc\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_k %lf", &(data->alpha_k)) != 1){
    printf("Error reading alpha_k\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_eps %lf", &(data->alpha_eps)) != 1){
    printf("Error reading alpha_eps\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_P_k %lf", &(data->alpha_P_k)) != 1){
    printf("Error reading alpha_P_k\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_mu_turb %lf", &(data->alpha_mu_turb)) != 1){
    printf("Error reading alpha_mu_turb\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_S_CG_eps %lf", &(data->alpha_S_CG_eps)) != 1){
    printf("Error alpha_S_CG_eps\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_SC_k %lf", &(data->alpha_SC_k)) != 1){
    printf("Error reading alpha_SC_k\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_SC_eps %lf", &(data->alpha_SC_eps)) != 1){
    printf("Error reading alpha_SC_eps\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_to_update_y_plus %d", &(data->it_to_update_y_plus)) != 1){
    printf("Error reading it_to_update_y_plus\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "y_plus_limit %lf", &(data->y_plus_limit)) != 1){
    printf("Error reading y_plus_limit\n");
    return 1;
  }

  /* SOLVE_PHI PARAMETERS */
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "interface_molecular_flux_model %d",
	    &(data->interface_molecular_flux_model)) != 1){
    printf("Error reading interface_molecular_flux_model\n");
    return 1;
  }

  if((data->interface_molecular_flux_model < 1) || (data->interface_molecular_flux_model > 6)){
    printf("Invalid value for interface_molecular_flux_model\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "phi_flow_scheme %d",
	    &(data->phi_flow_scheme)) != 1){
    printf("Error reading phi_flow_scheme\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "phi_dF_OK %d",
	    &(data->phi_dF_OK)) != 1){
    printf("Error reading phi_dF_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "Dirichlet_Isoflux_convective_boundary_OK %d",
	    &(data->Dirichlet_Isoflux_convective_boundary_OK)) != 1){
    printf("Error reading Dirichlet_Isoflux_convective_boundary_OK\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "interface_flux_update_it %d",
	    &(data->interface_flux_update_it)) != 1){
    printf("Error reading interface_flux_update_it\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_it %d", &(data->max_it)) != 1){
    printf("Error reading max_it\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "max_sub_it %d", &(data->max_sub_it)) != 1){
    printf("Error reading max_sub_it\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_upd_coeff_phi %d", &(data->it_upd_coeff_phi)) != 1){
    printf("Error reading it_upd_coeff_phi\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "it_ref_norm_phi %d", &(data->it_ref_norm_phi)) != 1){
    printf("Error reading it_ref_norm_phi\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "steady_state_OK %d", &(data->steady_state_OK)) != 1){
    printf("Error reading steady_state_OK\n");
    return 1;
  }

  if((data->steady_state_OK < 0) || (data->steady_state_OK > 1)){
    printf("Invalid value for steady_state_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "write_transient_data_OK %d", &(data->write_transient_data_OK)) != 1){
    printf("Error reading write_transient_data_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "write_transient_data_interval %lf", &(data->write_transient_data_interval)) != 1){
    printf("Error reading write_transient_data_interval\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "t_min t_max %lf %lf", &(data->tsaves[2]), &(data->tsaves[3]));
  
  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "dt_min dt_max %lf %lf", &(data->dt[4]), &(data->dt[5]));
  data->dt[6] = 0;

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_phi %lf", &(data->alpha_phi)) != 1){
    printf("Error reading alpha_phi\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_SC_phi %lf", &(data->alpha_SC_phi)) != 1){
    printf("Error reading alpha_SC_phi\n");
    return 1;
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "tol_phi %lf", &(data->tol_phi)) != 1){
    printf("Error reading tol_phi\n");
    return 1;
  }
  
  /* Phase change solver parameters */
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "solve_phase_change_OK %d",
	    &(data->solve_phase_change_OK)) != 1){
    printf("Error reading solve_phase_change_OK\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "alpha_gL %lf", &(data->alpha_gL)) != 1){
    printf("Error reading alpha_gL\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "gL_tol %lf", &(data->gL_tol)) != 1){
    printf("Error reading gL_tol\n");
    return 1;
  }

  /* Getting boundary condition info */
  if(get_bc_flow_info_from_file(&(data->bc_flow_west), fPtr)){
    ret_value = 1;
    return ret_value;
  }

  if(get_bc_flow_info_from_file(&(data->bc_flow_east), fPtr)){
    ret_value = 1;
    return ret_value;
  }

  if(get_bc_flow_info_from_file(&(data->bc_flow_south), fPtr)){
    ret_value = 1;
    return ret_value;
  }
  
  if(get_bc_flow_info_from_file(&(data->bc_flow_north), fPtr)){
    ret_value = 1;
    return ret_value;
  } 

  if(get_bc_flow_info_from_file(&(data->bc_flow_bottom), fPtr)){
    ret_value = 1;
    return ret_value;
  }

  if(get_bc_flow_info_from_file(&(data->bc_flow_top), fPtr)){
    ret_value = 1;
    return ret_value;
  }
  
  get_bc_info_from_file(&(data->west), fPtr, data->solve_3D_OK);
  get_bc_info_from_file(&(data->east), fPtr, data->solve_3D_OK);
  get_bc_info_from_file(&(data->south), fPtr, data->solve_3D_OK);
  get_bc_info_from_file(&(data->north), fPtr, data->solve_3D_OK);
  get_bc_info_from_file(&(data->bottom), fPtr, data->solve_3D_OK);
  get_bc_info_from_file(&(data->top), fPtr, data->solve_3D_OK);

  /* Curves parameters */
  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "curves %d", &(data->curves));

  n_curves = data->curves;

  /* Initialized to TRUE */
  data->solve_this_phase_OK = create_array_int(n_curves + 1, 1, 1, 1);

  data->rho_v = create_array(n_curves + 1, 1, 1, 0);
  data->cp_v = create_array(n_curves + 1, 1, 1, 0);
  data->gamma_v = create_array(n_curves + 1, 1, 1, 0);
  data->phi0_v = create_array(n_curves + 1, 1, 1, 0);
  
  if(n_curves < 0){
    printf("The configuration file does not contain valid "
	   "objects to draw");
    ret_value = -1;
  }

  if(n_curves > 0){
    data->curve_is_solid_OK = create_array_int(n_curves, 1, 1, 0);
    data->Isoflux_OK = create_array_int(n_curves, 1, 1, 1);
    data->phi_b_v = create_array(n_curves, 1, 1, 0);
    /* If true lets do normal quadric construction, if false it should be a
       direct poly_coeffs completition */
    data->quadric_OK = create_array_int(n_curves, 1, 1, 0);
	  
    data->lambda_1 = create_array(n_curves, 1, 1, 0);
    data->lambda_2 = create_array(n_curves, 1, 1, 0);
    data->lambda_3 = create_array(n_curves, 1, 1, 0);
    data->angle_x = create_array(n_curves, 1, 1, 0);
    data->angle_y = create_array(n_curves, 1, 1, 0);
    data->angle_z = create_array(n_curves, 1, 1, 0);
    data->tcompx = create_array(n_curves, 1, 1, 0);
    data->tcompy = create_array(n_curves, 1, 1, 0);
    data->tcompz = create_array(n_curves, 1, 1, 0);
    data->dphidNb_v = create_array(n_curves, 1, 1, 0);
    data->constant_d = create_array(n_curves, 1, 1, 0);
    data->rho_l = create_array(n_curves, 1, 1, 0);
    data->cp_l = create_array(n_curves, 1, 1, 0);
    data->gamma_l = create_array(n_curves, 1, 1, 0);
    data->curve_phase_change_OK = create_array_int(n_curves, 1, 1, 0);
    data->T_phase_change = create_array(n_curves, 1, 1, 0);
    data->dT_phase_change = create_array(n_curves, 1, 1, 0);
    data->T_ref = create_array(n_curves, 1, 1, 0);
    data->dH_S_L = create_array(n_curves, 1, 1, 0);

    data->poly_coeffs = create_array(16, n_curves, 1, 0);

  }

  /* Get the quadric_OK flags */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");  
  for(J=0; J<n_curves; J++){
    token = strtok(NULL, " ");
    if(sscanf(token, "%d", &data->quadric_OK[J]) != 1){
      printf("Error reading quadric_OK flags for curve %d\n", J);
      return 1;
    }
  }
  
  /* Here put limits.  No error checking sadly for strtod function, but we
     need to parse inf values */
  /* xmin */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");  
  for(J=0; J<n_curves; J++){
    token = strtok(NULL, " ");
    if(sscanf(token, "%lf", &data->poly_coeffs[J*16 + 10]) != 1){
      printf("Error reading xmin values for curve %d\n", J);
      return 1;
    }
  }

  /* xmax */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(J=0; J<n_curves; J++){
    token = strtok(NULL, " ");
    if(sscanf(token, "%lf", &data->poly_coeffs[J*16 + 11]) != 1){
      printf("Error reading xmax values for curve %d\n", J);
      return 1;
    }
  }

  /* ymin */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(J=0; J<n_curves; J++){
    token = strtok(NULL, " ");
    if(sscanf(token, "%lf", &data->poly_coeffs[J*16 + 12]) != 1){
      printf("Error reading ymin values for curve %d\n", J);
      return 1;
    }
  }

  /* ymax */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(J=0; J<n_curves; J++){
    token = strtok(NULL, " ");
    if(sscanf(token, "%lf", &data->poly_coeffs[J*16 + 13]) != 1){
      printf("Error reading ymax values for curve %d\n", J);
      return 1;
    }
  }

  /* zmin */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(J=0; J<n_curves; J++){
    token = strtok(NULL, " ");
    if(sscanf(token, "%lf", &data->poly_coeffs[J*16 + 14]) != 1){
      printf("Error reading zmin values for curve %d\n", J);
      return 1;
    }
  }

  /* zmax */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(J=0; J<n_curves; J++){
    token = strtok(NULL, " ");
    if(sscanf(token, "%lf", &data->poly_coeffs[J*16 + 15]) != 1){
      printf("Error reading zmax values for curve %d\n", J);
      return 1;
    }
  }
  
  for(I=0; I<27; I++){
      
    get_next_valid_line(fPtr, buff, SIZE);
    token = strtok(buff, " "); /* First token (NAME) */
      
    for(J=0; J<n_curves+1; J++){
      if(J>0){ /* Skip coeff name */
		  
	if(I == 0){
	  if(sscanf(token, "%d", &data->curve_is_solid_OK[J - 1]) != 1){
	    printf("Error reading curve_is_solid_OK for curve %i\n", J);
	    return 1;	    
	  }
	}
	if(I == 1){
	  if(sscanf(token, "%d", &data->solve_this_phase_OK[J]) != 1){
	    printf("Error reading solve_this_phase_OK for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 2){
	  if(sscanf(token, "%d", &data->Isoflux_OK[J - 1]) != 1){
	    printf("Error reading Isoflux_OK for curve %i\n", J);
	    return 1;	    
	  }
	}
	if(I == 3){
	  if(sscanf(token, "%lf", &data->lambda_1[J - 1]) != 1){
	    printf("Error reading lambda_1 for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 4){
	  if(sscanf(token, "%lf", &data->lambda_2[J - 1]) != 1){
	    printf("Error reading lambda_2 for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 5){
	  if(sscanf(token, "%lf", &data->lambda_3[J - 1]) != 1){
	    printf("Error reading lambda_3 for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 6){
	  if(sscanf(token, "%lf", &data->angle_x[J - 1]) != 1){
	    printf("Error reading angle_x for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 7){
	  if(sscanf(token, "%lf", &data->angle_y[J - 1]) != 1){
	    printf("Error reading angle_y for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 8){
	  if(sscanf(token, "%lf", &data->angle_z[J - 1]) != 1){
	    printf("Error reading angle_z for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 9){
	  if(sscanf(token, "%lf", &data->tcompx[J - 1]) != 1){
	    printf("Error reading tcompx for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 10){
	  if(sscanf(token, "%lf", &data->tcompy[J - 1]) != 1){
	    printf("Error reading tcompy for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 11){
	  if(sscanf(token, "%lf", &data->tcompz[J - 1]) != 1){
	    printf("Error reading tcompz for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 12){
	  if(sscanf(token, "%lf", &data->constant_d[J - 1]) != 1){
	    printf("Error reading constant_d for curve %i\n", J);
	    return 1;
	  }
	}
	if(I == 13){
	  if(sscanf(token, "%lf", &data->dphidNb_v[J - 1]) != 1){
	    printf("Error reading dphidNb_v for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 14){
	  if(sscanf(token, "%lf", &data->phi_b_v[J - 1]) != 1){
	    printf("Error reading phi_b_v for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 15){
	  if(sscanf(token, "%lf", &data->phi0_v[J]) != 1){
	    printf("Error reading phi0_v for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 16){
	  if(sscanf(token, "%lf", &data->rho_v[J]) != 1){
	    printf("Error reading rho_v for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 17){
	  if(sscanf(token, "%lf", &data->cp_v[J]) != 1){
	    printf("Error reading cp_v for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 18){
	  if(sscanf(token, "%lf", &data->gamma_v[J]) != 1){
	    printf("Error reading gamma_v for curve %i\n", J);
	    return 1;	    
	  }
	}
	if(I == 19){
	  if(sscanf(token, "%lf", &data->rho_l[J - 1]) != 1){
	    printf("Error reading rho_l for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 20){
	  if(sscanf(token, "%lf", &data->cp_l[J - 1]) != 1){
	    printf("Error reading cp_l for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 21){
	  if(sscanf(token, "%lf", &data->gamma_l[J - 1]) != 1){
	    printf("Error reading gamma_l for curve %i\n", J);
	    return 1;	    
	  }
	}
	if(I == 22){
	  if(sscanf(token, "%d", &data->curve_phase_change_OK[J - 1]) != 1){
	    printf("Error reading curve_phase_change_OK for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 23){
	  if(sscanf(token, "%lf", &data->T_phase_change[J - 1]) != 1){
	    printf("Error reading T_phase_change for curve %i\n", J);
	    return 1;	    
	  }
	}
	if(I == 24){
	  if(sscanf(token, "%lf", &data->dT_phase_change[J - 1]) != 1){
	    printf("Error reading dT_phase_change for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 25){	  
	  if(sscanf(token, "%lf", &data->T_ref[J - 1]) != 1){
	    printf("Error reading T_ref for curve %i\n", J);
	    return 1;	    	    
	  }
	}
	if(I == 26){	  
	  if(sscanf(token, "%lf", &data->dH_S_L[J - 1]) != 1){
	    printf("Error reading dH_S_L for curve %i\n", J);
	    return 1;	    	    
	  }
	}
      }
      token = strtok(NULL, " ");
    }
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "rho_f %lf", &(data->rho_v[0]));

  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "mu %lf", &(data->mu_final));
  
  if((per_mu_final_iter_flow < 0 && per_mu_final_iter_flow > 100) ||
     (per_mu_initial_iter_flow < 0 && per_mu_initial_iter_flow > 100) ||
     (per_mu_initial_iter_flow >= per_mu_final_iter_flow) ||
     (per_mu_final_iter_flow == 0) ||
     (data->mu_initial == data->mu_final)){
    
    mu_initial_iter_flow = 0;
    mu_final_iter_flow = 0;
    
  }
  else{
    
    mu_initial_iter_flow = (int)round(data->max_iter_flow * per_mu_initial_iter_flow / 100.0);
    mu_final_iter_flow = (int)round(data->max_iter_flow * per_mu_final_iter_flow / 100.0);
    
  }
  
  data->mu_initial_iter_flow = mu_initial_iter_flow;
  data->mu_final_iter_flow = mu_final_iter_flow;
  
  data->mu = data->mu_final;
  
  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "cp_f %lf", &(data->cp_v[0]));
  
  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "gamma_f %lf", &(data->gamma_v[0]));
  
  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "phi0_f %lf", &(data->phi0_v[0]));

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "u0 %lf", &(data->u0_v)) != 1){
    printf("Error reading u0\n");
    return 1;
  }
	
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "v0 %lf", &(data->v0_v)) != 1){
    printf("Error reading v0\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "w0 %lf", &(data->w0_v)) != 1){
    printf("Error reading w0\n");
    return 1;
  }

  get_next_valid_line(fPtr, buff, SIZE);

  if(sscanf(buff, "p_gradient_direction %i", &(data->p_gradient_direction)) != 1){
    printf("Error reading p_gradient_direction\n");
    return 1;
  }

  if((data->p_gradient_direction < 0) || (data->p_gradient_direction > 5)){
    printf("Invalid p_gradient_direction value\n");
    return 1;
  }
  	
  get_next_valid_line(fPtr, buff, SIZE);

  sscanf(buff, "(p0, pL, rise_to) %lf %lf %lf",
	 &(data->p0), &(data->pL), &(data->rise_to));
    
  if((data->rise_to > 1) || (data->rise_to < 0)){
    printf("Invalid rise_to value\n");
    return 1;
  }

  /* THERMOELECTRICITY */
  TEG *teg = &(data->teg);

  teg->curves = n_curves;
  
  /* Memory request */
  teg->TE_OK = create_array_int(n_curves, 1, 1, 0);
  teg->ntppm = create_array_int(n_curves, 1, 1, 0);

  teg->QL = create_array(n_curves, 1, 1, 0);
  teg->Q0 = create_array(n_curves, 1, 1, 0);
  
  teg->TfH = create_array(n_curves, 1, 1, 0);
  teg->TfC = create_array(n_curves, 1, 1, 0);

  teg->hL = create_array(n_curves, 1, 1, 0);
  teg->h0 = create_array(n_curves, 1, 1, 0);

  teg->TL = create_array(n_curves, 1, 1, 0);
  teg->T0 = create_array(n_curves, 1, 1, 0);

  teg->ALPHA = create_array(n_curves, 1, 1, 0);
  teg->K = create_array(n_curves, 1, 1, 0);
  teg->Re = create_array(n_curves, 1, 1, 0);

  teg->RHOe = create_array(n_curves, 1, 1, 0);
  teg->k = create_array(n_curves, 1, 1, 0);
  teg->A = create_array(n_curves, 1, 1, 0);
  teg->L = create_array(n_curves, 1, 1, 0);
  
  for(I = 0; I < 10; I++){
      
    get_next_valid_line(fPtr, buff, SIZE);
    token = strtok(buff, " "); /* First token (NAME) */
      
    for(J = 0; J < n_curves + 1; J++){
      if(J > 0){ /* Skip coeff name */
		  
	if(I == 0){
	  sscanf(token, "%d", &teg->TE_OK[J - 1]);
	}
	if(I == 1){
	  sscanf(token, "%d", &teg->ntppm[J - 1]);
	}
	if(I == 2){
	  sscanf(token, "%lf", &teg->A[J - 1]);
	}
	if(I == 3){
	  sscanf(token, "%lf", &teg->L[J - 1]);
	}
	if(I == 4){
	  sscanf(token, "%lf", &teg->ALPHA[J - 1]);
	}
	if(I == 5){
	  sscanf(token, "%lf", &teg->RHOe[J - 1]);
	}
	if(I == 6){
	  sscanf(token, "%lf", &teg->k[J - 1]);
	}
	if(I == 7){
	  sscanf(token, "%lf", &teg->TfC[J - 1]);
	}
	if(I == 8){
	  sscanf(token, "%lf", &teg->hL[J - 1]);
	}
	if(I == 9){
	  sscanf(token, "%lf", &teg->h0[J - 1]);
	}
      }
      
      token = strtok(NULL, " ");
    }
  }

  /* Set physical properties of TEMs */
  set_TEM_properties(teg);

  /* Lines */
  get_next_valid_line(fPtr, buff, SIZE);
  sscanf(buff, "lines %d", &n_lines);
  data->lines = n_lines;
  
  if(n_lines < 0){
    printf("The configuration file does not contain "
	   "valid objects to draw");
    ret_value = -1;
  }
  
  if(n_lines > 0){
    data->line_points = create_array(2*n_lines, 2, 1, 0);
  }
  
  for(I = 0; I < n_lines; I++){
    get_next_valid_line(fPtr, buff, SIZE);
    token = strtok(buff, " "); 
    for(J = 0; J < 5; J++){
      if(J == 3){
	i = 1;
      }
      if(J != 0){
	if(K == 0){
	  sscanf(token, "%lf",
		 &data->line_points[(2*K*n_lines) + (2*I + i)]);
	  K = 1;
	}
	else{
	  sscanf(token, "%lf",
		 &data->line_points[(2*K*n_lines) + (2*I + i)]);
	  K = 0;
	}
      }
      token = strtok(NULL, " ");
    }
    i = 0;
  }
  
  fclose(fPtr);
  
  return ret_value;
}

/* GET_BC_INFO_FROM_FILE */
int get_bc_info_from_file(Boundary_Info *bi, FILE *fPtr, const int solve_3D_OK){
    
  char buff[256];
  char *token = NULL;
  
  int parts, count_parts;
  int ret_value = 0;

  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  token = strtok(NULL, " ");
  sscanf(token, "%d", &parts);
    
  bi->parts = parts;

  if(parts > 0){
    bi->per = create_array(parts, 1, 1, 0);
    
    bi->perx = create_array(1, 1, 1, 0);
    bi->pery = create_array(1, 1, 1, 0);
    bi->perz = create_array(1, 1, 1, 0);
	
    bi->alpha = create_array_int(parts, 1, 1, 0);
    bi->beta = create_array(parts, 1, 1, 0);
    bi->gamma = create_array(parts, 1, 1, 0);
    bi->phi_b = create_array(parts, 1, 1, 0);
  }
  else{
    printf("Boundary conditions for the domain are necessary\n");
    ret_value = 1;
    return ret_value;
  }
    
  /* Getting the field information */
  get_next_valid_line(fPtr, buff, SIZE);
  /* Ignore if 3D case */
  if(!solve_3D_OK){
    
    token = strtok(buff, " ");
    for(count_parts = 0; count_parts < parts; count_parts++){
      token = strtok(NULL, " ");
      sscanf(token, "%lf", &bi->per[count_parts]);
    }
    
  }
  
  bi->Nperx = get_per_parts_bc(bi->perx, fPtr);
  bi->Npery = get_per_parts_bc(bi->pery, fPtr);
  bi->Nperz = get_per_parts_bc(bi->perz, fPtr);
  
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(count_parts = 0; count_parts < parts; count_parts++){
    token = strtok(NULL, " ");
    sscanf(token, "%d", &bi->alpha[count_parts]);
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(count_parts = 0; count_parts < parts; count_parts++){
    token = strtok(NULL, " ");
    sscanf(token, "%lf", &bi->beta[count_parts]);
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(count_parts = 0; count_parts < parts; count_parts++){
    token = strtok(NULL, " ");
    sscanf(token, "%lf", &bi->gamma[count_parts]);
  }
    
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(count_parts = 0; count_parts < parts; count_parts++){
    token = strtok(NULL, " ");
    sscanf(token, "%lf", &bi->phi_b[count_parts]);
  }

  return ret_value;
}

/* GET_NEXT_VALID_LINE */
char * get_next_valid_line(FILE *fPtr, char *buff, const int buff_size){
    
  char first_char;
    
  fgets(buff, buff_size - 1, fPtr);
  first_char = buff[0];
    
  while(first_char == '%'){
    fgets(buff, buff_size - 1, fPtr);
    first_char = buff[0];
        
  }

  return buff;
}

/* GET_GRID_INFO_FROM_FILE */
void get_grid_info_from_file(Grid_Info *gi, FILE *fPtr){
    
  char buff[256];
  char *token = NULL;
  int smooth_OK, parts, count_parts;

  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  token = strtok(NULL, " ");
  sscanf(token, "%d", &smooth_OK);

  gi->smooth_OK = smooth_OK;
    
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  token = strtok(NULL, " ");
  sscanf(token, "%d", &parts);

  gi->seg = parts;

  gi->per = create_array(parts, 1, 1, 0);
  gi->N = create_array_int(parts, 1, 1, 0);
    
  /* Getting boundary segmentation information */
  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(count_parts = 1; count_parts < parts + 1; count_parts++){
    token = strtok(NULL, " ");
    sscanf(token, "%lf", &gi->per[count_parts - 1]);
  }

  get_next_valid_line(fPtr, buff, SIZE);
  token = strtok(buff, " ");
  for(count_parts = 1; count_parts < parts + 1; count_parts++){
    token = strtok(NULL, " ");
    sscanf(token, "%d", &gi->N[count_parts - 1]);
  }

  return;
}

/* GET_BC_FLOW_INFO_FROM_FILE */
int get_bc_flow_info_from_file(b_cond_flow *bc, FILE *fPtr){

  static int bc_flow_counter = 0;
  
  char buff[256];

  int ret_value = 0;
   
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "type %d", &(bc->type)) != 1){
    printf("Error reading type at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;    
  }

  if((bc->type > 5) || (bc->type < -2)){
    printf("Type out of range at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "parabolize_profile_OK %d", &(bc->parabolize_profile_OK)) != 1){
    printf("Error reading parabolize_profile_OK at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;    
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "v_value[0] v_value[1] v_value[2] %lf %lf %lf",
	    &(bc->v_value[0]), &(bc->v_value[1]), &(bc->v_value[2])) != 3){
    printf("Error reading v_value at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;    
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "k_value %lf", &(bc->k_value)) != 1){
    printf("Error reading k_value at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;    
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "eps_value %lf", &(bc->eps_value)) != 1){
    printf("Error reding eps_value at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;    
  }

  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "ref_p_node_OK %d", &(bc->ref_p_node_OK)) != 1){
    printf("Error reading ref_p_node_OK at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;    
  }
  
  get_next_valid_line(fPtr, buff, SIZE);
  if(sscanf(buff, "p_value %lf", &(bc->p_value)) != 1){
    printf("Error reading p_value at flow bc number %i\n", bc_flow_counter);
    ret_value = 1;
    return ret_value;    
  }

  bc_flow_counter += 1;
  
  return ret_value;
}


/* GET_PER_PARTS_BC */
int get_per_parts_bc(double *perx, FILE *fPtr){
    
  char buff[256];
  char *token = NULL;

  int i = 0;

  /* perx */
  get_next_valid_line(fPtr, buff, SIZE);
  /* Here is the name in token */
  token = strtok(buff, " ");
  /* Now a number or NULL */
  token = strtok(NULL, " ");
  
  if (token != NULL){
    /* Store that number */
    sscanf(token, "%lf", &perx[i]);
    
    /* Let us see if there are more */
    token = strtok(NULL, " ");
    i++;
    
    while( token != NULL ){
      /* Put some memory in it */
      perx = (double *)realloc(perx, (i+1)*sizeof(double));
      verify_alloc(perx);
      
      /* Store that number */
      sscanf(token, "%lf", &perx[i]);
      
      /* Get next token */
      token = strtok(NULL, " ");
      i++;
    }
    
  }
  
  return i;
  
}
