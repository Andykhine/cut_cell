#include "include/some_defs.h"

/* ADI_OMP_3D */
void ADI_omp_3D(double *phi, Coeffs *coeffs, const int Nx, const int Ny, const int Nz, ADI_Vars *ADI_vars){

  int I, J, K;
  int index_;
  
  double *a_T = ADI_vars->a_T;
  double *b_T = ADI_vars->b_T;
  double *c_T = ADI_vars->c_T;
  double *d_T = ADI_vars->d_T;
  double *c_p = ADI_vars->c_p;
  double *d_p = ADI_vars->d_p;

  const double *aP = coeffs->aP;
  const double *aE = coeffs->aE;
  const double *aW = coeffs->aW;
  const double *aN = coeffs->aN;
  const double *aS = coeffs->aS;
  const double *aT = coeffs->aT;
  const double *aB = coeffs->aB;
  const double *b = coeffs->b;

  /* First ADI step: y */
#pragma omp parallel private(I,J,K,index_) shared(a_T,b_T,c_T,d_T,aP,aE,aW,aN,aS,aT,aB,phi)
  {
#pragma omp for
    for(K=1; K<(Nz-1); K++){
      for(I=1; I<(Nx-1); I++){
	/* Boundary condition for a_T, b_T, c_T, d_T */
	J = 0;
	index_ = K*Nx*Ny + J*Nx + I;
	
	a_T[index_]    = (-1)*aN[index_];
	b_T[index_]    = (-1)*aS[index_];
	c_T[index_]    = b[index_];
	d_T[index_]    = aP[index_];
	
	J = Ny-1;
	index_ = K*Nx*Ny + J*Nx + I;
	
	a_T[index_] = (-1)*aN[index_];
	b_T[index_] = (-1)*aS[index_];
	c_T[index_] = b[index_];       
	d_T[index_] = aP[index_];
        
	for (J=1; J<(Ny-1); J++){
	  
	  index_ = K*Nx*Ny + J*Nx + I;
	  
	  /* Setting values for a_T, b_T, c_T, d_T array */
	  a_T[index_] = (-1)*aN[index_];
	  b_T[index_] = (-1)*aS[index_];
	  c_T[index_] = b[index_] +  phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	  
	  // 	  if((I == (Nx-2)) && ((J == 0) || (J == (Ny-1)) || (K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_] = b[index_] +  phi[index_ - 1] * aW[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_]; 
	  // 	  }
	  // 	  else if((I == 1) && ((J == 0) || (J == (Ny-1))) && ((K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_] = b[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	  // 	  }
	  // 	  else if((K == (Nz-2)) && ((J == 0) || (J == (Ny-1))) && ((I == 0) || (I == (Nx-1)))){
	  // 	    c_T[index_] = b[index_] +  phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - (Nx*Ny)]*aB[index_];
	  // 	  }
	  // 	  else if((K == 1) && ((J == 0) || (J == (Ny-1))) && ((I == 0) || (I == (Nx-1)))){
	  // 	    c_T[index_] = b[index_] +  phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	  // 	  }
	  
	  d_T[index_] = aP[index_];
	}
      }
    } 
  }

#pragma omp parallel private(I,J,K,index_) shared(c_p,d_p,a_T,b_T,c_T,d_T)
  {
#pragma omp for
    /* Setting values for c_p and d_p */
    for(K=1; K<(Nz-1); K++){
      for(I=1; I<(Nx-1); I++){
	/* Boundary 0 */
	J = 0;
	index_ = K*Nx*Ny + J*Nx + I;
	
	c_p[index_] = c_T[index_];
	d_p[index_] = d_T[index_];
	
	for(J=1; J<Ny; J++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-Nx])/d_p[index_-Nx]);
	  c_p[index_] = c_T[index_] - ((c_p[index_-Nx]*b_T[index_])/d_p[index_-Nx]);
	}
      }
    }
  }
  
#pragma omp parallel private(I,J,K,index_) shared(phi,a_T,c_p,d_T)
  {
#pragma omp for
    /* Coefficients "backwards" */
    for(K=1; K<(Nz-1); K++){
      for(I=1; I<(Nx-1); I++){
	/* Boundary  Ny-1 */
	J = Ny-1;
	index_ = K*Nx*Ny + J*Nx + I;
	phi[index_] = c_p[index_]/d_p[index_];
	
	for(J=(Ny-2); J>=0; J--){
	  index_ = K*Nx*Ny + J*Nx + I;
	  
	  phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + Nx]) / (d_p[index_]);
	}
      }
    }
  }

  /* Second ADI step X */
  
#pragma omp parallel private(I,J,K,index_) shared(a_T,b_T,c_T,d_T,aP,aE,aW,aN,aS,aT,aB,phi)
  {
#pragma omp for
    for(K=1; K<(Nz-1); K++){
      for(J=1; J<(Ny-1); J++){
	/* Boundary condition for a_T, b_T, c_T, d_T */
	I = 0;
	index_ = K*Nx*Ny + J*Nx + I;
	
	a_T[index_]       = (-1)*aE[index_];
	b_T[index_]       = (-1)*aW[index_];
	c_T[index_]       = b[index_];
	d_T[index_]       = aP[index_];

	I = Nx - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	
	a_T[index_]  = (-1)*aE[index_];
	b_T[index_]  = (-1)*aW[index_];
	c_T[index_]  = b[index_];
	d_T[index_]  = aP[index_];
	
	for(I=1; I<(Nx-1); I++){
	  
	  index_ = K*Nx*Ny + J*Nx + I;

	  /* Setting values for a_T, b_T, c_T, d_T array */
	  a_T[index_] = (-1)*aE[index_];
	  b_T[index_] = (-1)*aW[index_];
	  c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_] + phi[index_ + Nx]*aN[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	  
	  // 	  if((J == (Ny-2)) && ((I == 0) || (I == (Nx-1)) || (K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	  // 	  }
	  // 	  else if((J == 1) && ((I == 0) || (I == (Nx-1))) && ((K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_] = b[index_] + phi[index_ + Nx]*aN[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	  // 	  }
	  // 	  else if((K == (Nz-2)) && ((I == 0) || (I == (Nx-1))) && ((J == 0) || (J == (Ny-1)))){
	  // 	    c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_] + phi[index_ + Nx]*aN[index_] + phi[index_ - (Nx*Ny)]*aB[index_];
	  // 	  }
	  // 	  else if((K == 1) && ((I == 0) || (I == (Nx-1))) && ((J == 0) || (J == (Ny-1)))){
	  // 	    c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_] + phi[index_ + Nx]*aN[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	  // 	  }
	  
	  d_T[index_] = aP[index_];
	}
      }
    }
  }

#pragma omp parallel private(I,J,K,index_) shared(c_p,d_p,a_T,b_T,c_T,d_T)
  {
#pragma omp for
    /* Setting values for c_p and d_p */
	
    for(K=1; K<(Nz-1); K++){
      for(J=1; J<(Ny-1); J++){
	I = 0;
	index_ = K*Nx*Ny + J*Nx + I;

	c_p[index_] = c_T[index_];
	d_p[index_] = d_T[index_];
    
	for(I=1; I<Nx; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
	  d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-1])/d_p[index_-1]);
	  c_p[index_] = c_T[index_] - ((c_p[index_-1]*b_T[index_])/d_p[index_-1]);
	}
      }
    }
  }

#pragma omp parallel private(I,J,K,index_) shared(phi,a_T,c_p,d_T)
  {
#pragma omp for
    /* Coefficients "backwards" */
    for(K=1; K<(Nz-1); K++){
      for(J=1; J<(Ny-1); J++){
	/* Boundary  Nx-1 */
	I = Nx-1;
	index_ = K*Nx*Ny + J*Nx + I;
	phi[index_] = c_p[index_]/d_p[index_];
	
	for(I=(Nx-2); I>=0; I--){
	  index_ = K*Nx*Ny + J*Nx + I;
	  
	  phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + 1]) / (d_p[index_]);
	}
      }
    }
  }

  /* Third ADI step Z */
#pragma omp parallel private(I,J,K,index_) shared(a_T,b_T,c_T,d_T,aP,aE,aW,aN,aS,aT,aB,phi)
  {
#pragma omp for
    for(I=1; I < (Nx-1); I++){
      for(J=1; J < (Ny-1); J++){
	K = 0;
	index_ = K*Nx*Ny + J*Nx + I;
	
	/* Boundary condition for a_T, b_T, c_T, d_T */
	a_T[index_]       = (-1)*aT[index_];
	b_T[index_]       = (-1)*aB[index_];
	c_T[index_]       = b[index_];
	d_T[index_]       = aP[index_];

	K = Nz-1;
	index_ = K*Nx*Ny + J*Nx + I;
	
	a_T[index_]    = (-1)*aT[index_];
	b_T[index_]    = (-1)*aB[index_];
	c_T[index_]    = b[index_];
	d_T[index_]    = aP[index_];
	
	for(K=1; K<(Nz-1); K++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  /* Setting values for a_T, b_T, c_T, d_T array */
	  a_T[index_]     = (-1)*aT[index_];
	  b_T[index_]     = (-1)*aB[index_];
	  c_T[index_]     = b[index_] + phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - Nx] * aS[index_] + phi[index_ + Nx] * aN[index_];
	  
	  // 	  if((I == (Nx-2)) && ((J == 0) || (J == (Ny-1)) || (K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_]     = b[index_] + phi[index_ - 1] * aW[index_] + phi[index_ - Nx] * aS[index_] + phi[index_ + Nx] * aN[index_];
	  // 	  }
	  // 	  else if((I == 1) && ((J == 0) || (J == (Nx-1))) && ((K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_]     = b[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - Nx] * aS[index_] + phi[index_ + Nx] * aN[index_];
	  // 	  }
	  // 	  else if((J == (Ny-2)) && ((I == 0) || (I == (Nx-1))) && ((K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_]     = b[index_] + phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - Nx] * aS[index_];
	  // 	  }
	  // 	  else if((J == 1) && ((I == 0) || (I == (Nx-1))) && ((K == 0) || (K == (Nz-1)))){
	  // 	    c_T[index_]     = b[index_] + phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ + Nx] * aN[index_];
	  // 	  }
	  
	  d_T[index_]     = aP[index_];
	}
      }
    }
  }

#pragma omp parallel private(I,J,K,index_) shared(c_p,d_p,a_T,b_T,c_T,d_T)
  {
#pragma omp for
    /* Setting values for c_p and d_p */
	
    for(I=1; I<(Nx-1); I++){
      for(J=1; J<(Ny-1); J++){
	K = 0;
	index_ = K*Nx*Ny + J*Nx + I;

	c_p[index_] = c_T[index_];
	d_p[index_] = d_T[index_];
    
	for(K=1; K<Nz; K++){
	  index_ = K*Nx*Ny + J*Nx + I;
	
	  d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-Nx*Ny])/d_p[index_-Nx*Ny]);
	  c_p[index_] = c_T[index_] - ((c_p[index_-Nx*Ny]*b_T[index_])/d_p[index_-Nx*Ny]);
	}
      }
    }
  }

#pragma omp parallel private(I,J,K,index_) shared(phi,a_T,c_p,d_T)
  {
#pragma omp for
    /* Coefficients "backwards" */
    for(I=1; I<(Nx-1); I++){
      for(J=1; J<(Ny-1); J++){
	/* Boundary  Nx-1 */
	K = Nz-1;
	index_ = K*Nx*Ny + J*Nx + I;
	phi[index_] = c_p[index_]/d_p[index_];
	
	for(K=(Nz-2); K>=0; K--){
	  index_ = K*Nx*Ny + J*Nx + I;
	  
	  phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + Ny*Nx]) / (d_p[index_]);
	}
      }
    }
  }

  return;
}

/* ADI_SEQUENTIAL_3D */
void ADI_sequential_3D(double *phi, Coeffs *coeffs, const int Nx, const int Ny, const int Nz, ADI_Vars *ADI_vars){

  int I, J, K;
  int index_;
  
  double *a_T = ADI_vars->a_T;
  double *b_T = ADI_vars->b_T;
  double *c_T = ADI_vars->c_T;
  double *d_T = ADI_vars->d_T;
  double *c_p = ADI_vars->c_p;
  double *d_p = ADI_vars->d_p;

  const double *aP = coeffs->aP;
  const double *aE = coeffs->aE;
  const double *aW = coeffs->aW;
  const double *aN = coeffs->aN;
  const double *aS = coeffs->aS;
  const double *aT = coeffs->aT;
  const double *aB = coeffs->aB;
  const double *b = coeffs->b;

  /* First ADI step: y */
  for(K=1; K<(Nz-1); K++){
    for(I=1; I<(Nx-1); I++){
      /* Boundary condition for a_T, b_T, c_T, d_T */
      J = 0;
      index_ = K*Nx*Ny + J*Nx + I;
	
      a_T[index_]    = (-1)*aN[index_];
      b_T[index_]    = (-1)*aS[index_];
      c_T[index_]    = b[index_];
      d_T[index_]    = aP[index_];
	
      J = Ny-1;
      index_ = K*Nx*Ny + J*Nx + I;
	
      a_T[index_] = (-1)*aN[index_];
      b_T[index_] = (-1)*aS[index_];
      c_T[index_] = b[index_];       
      d_T[index_] = aP[index_];
        
      for (J=1; J<(Ny-1); J++){
	  
	index_ = K*Nx*Ny + J*Nx + I;
	  
	/* Setting values for a_T, b_T, c_T, d_T array */
	a_T[index_] = (-1)*aN[index_];
	b_T[index_] = (-1)*aS[index_];
	c_T[index_] = b[index_] +  phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	d_T[index_] = aP[index_];
      }
    }
  } 
    
  /* Setting values for c_p and d_p */
  for(K=1; K<(Nz-1); K++){
    for(I=1; I<(Nx-1); I++){
      /* Boundary 0 */
      J = 0;
      index_ = K*Nx*Ny + J*Nx + I;
	
      c_p[index_] = c_T[index_];
      d_p[index_] = d_T[index_];
	
      for(J=1; J<Ny; J++){
	index_ = K*Nx*Ny + J*Nx + I;
	d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-Nx])/d_p[index_-Nx]);
	c_p[index_] = c_T[index_] - ((c_p[index_-Nx]*b_T[index_])/d_p[index_-Nx]);
      }
    }
  }
  
  
  /* Coefficients "backwards" */
  for(K=1; K<(Nz-1); K++){
    for(I=1; I<(Nx-1); I++){
      /* Boundary  Ny-1 */
      J = Ny-1;
      index_ = K*Nx*Ny + J*Nx + I;
      phi[index_] = c_p[index_]/d_p[index_];
	
      for(J=(Ny-2); J>=0; J--){
	index_ = K*Nx*Ny + J*Nx + I;
	  
	phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + Nx]) / (d_p[index_]);
      }
    }
  }
  

  /* Second ADI step X */
  
  for(K=1; K<(Nz-1); K++){
    for(J=1; J<(Ny-1); J++){
      /* Boundary condition for a_T, b_T, c_T, d_T */
      I = 0;
      index_ = K*Nx*Ny + J*Nx + I;
	
      a_T[index_]       = (-1)*aE[index_];
      b_T[index_]       = (-1)*aW[index_];
      c_T[index_]       = b[index_];
      d_T[index_]       = aP[index_];

      I = Nx - 1;
      index_ = K*Nx*Ny + J*Nx + I;
	
      a_T[index_]  = (-1)*aE[index_];
      b_T[index_]  = (-1)*aW[index_];
      c_T[index_]  = b[index_];
      d_T[index_]  = aP[index_];
	
      for(I=1; I<(Nx-1); I++){
	  
	index_ = K*Nx*Ny + J*Nx + I;

	/* Setting values for a_T, b_T, c_T, d_T array */
	a_T[index_] = (-1)*aE[index_];
	b_T[index_] = (-1)*aW[index_];
	c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_] + phi[index_ + Nx]*aN[index_] + phi[index_ - (Nx*Ny)]*aB[index_] + phi[index_ + (Nx*Ny)]*aT[index_];
	d_T[index_] = aP[index_];
      }
    }
  }
  

  /* Setting values for c_p and d_p */
	
  for(K=1; K<(Nz-1); K++){
    for(J=1; J<(Ny-1); J++){
      I = 0;
      index_ = K*Nx*Ny + J*Nx + I;

      c_p[index_] = c_T[index_];
      d_p[index_] = d_T[index_];
    
      for(I=1; I<Nx; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-1])/d_p[index_-1]);
	c_p[index_] = c_T[index_] - ((c_p[index_-1]*b_T[index_])/d_p[index_-1]);
      }
    }
  }
  


  /* Coefficients "backwards" */
  for(K=1; K<(Nz-1); K++){
    for(J=1; J<(Ny-1); J++){
      /* Boundary  Nx-1 */
      I = Nx-1;
      index_ = K*Nx*Ny + J*Nx + I;
      phi[index_] = c_p[index_]/d_p[index_];
	
      for(I=(Nx-2); I>=0; I--){
	index_ = K*Nx*Ny + J*Nx + I;
	  
	phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + 1]) / (d_p[index_]);
      }
    }
  }
 

  /* Third ADI step Z */
  
  for(I=1; I < (Nx-1); I++){
    for(J=1; J < (Ny-1); J++){
      K = 0;
      index_ = K*Nx*Ny + J*Nx + I;
	
      /* Boundary condition for a_T, b_T, c_T, d_T */
      a_T[index_]       = (-1)*aT[index_];
      b_T[index_]       = (-1)*aB[index_];
      c_T[index_]       = b[index_];
      d_T[index_]       = aP[index_];

      K = Nz-1;
      index_ = K*Nx*Ny + J*Nx + I;
	
      a_T[index_]    = (-1)*aT[index_];
      b_T[index_]    = (-1)*aB[index_];
      c_T[index_]    = b[index_];
      d_T[index_]    = aP[index_];
	
      for(K=1; K<(Nz-1); K++){

	index_ = K*Nx*Ny + J*Nx + I;

	/* Setting values for a_T, b_T, c_T, d_T array */
	a_T[index_]     = (-1)*aT[index_];
	b_T[index_]     = (-1)*aB[index_];
	c_T[index_]     = b[index_] + phi[index_ - 1] * aW[index_] + phi[index_ + 1] *aE[index_] + phi[index_ - Nx] * aS[index_] + phi[index_ + Nx] * aN[index_];
	d_T[index_]     = aP[index_];
      }
    }
  }
  

  /* Setting values for c_p and d_p */
	
  for(I=1; I<(Nx-1); I++){
    for(J=1; J<(Ny-1); J++){
      K = 0;
      index_ = K*Nx*Ny + J*Nx + I;

      c_p[index_] = c_T[index_];
      d_p[index_] = d_T[index_];
    
      for(K=1; K<Nz; K++){
	index_ = K*Nx*Ny + J*Nx + I;
	
	d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-Nx*Ny])/d_p[index_-Nx*Ny]);
	c_p[index_] = c_T[index_] - ((c_p[index_-Nx*Ny]*b_T[index_])/d_p[index_-Nx*Ny]);
      }
    }
  }
  

  /* Coefficients "backwards" */
  for(I=1; I<(Nx-1); I++){
    for(J=1; J<(Ny-1); J++){
      /* Boundary  Nx-1 */
      K = Nz-1;
      index_ = K*Nx*Ny + J*Nx + I;
      phi[index_] = c_p[index_]/d_p[index_];
      
      for(K=(Nz-2); K>=0; K--){
	index_ = K*Nx*Ny + J*Nx + I;
	
	phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + Ny*Nx]) / (d_p[index_]);
      }
    }
  }
  
  return;
}













/* ADI_OMP_2D */
void ADI_omp_2D(double *phi, Coeffs *coeffs, const int Nx, const int Ny, const int dummy, ADI_Vars *ADI_vars){
  /* Declaring Variables */
  int I, J;
  int index_;
  
  double *a_T = ADI_vars->a_T;
  double *b_T = ADI_vars->b_T;
  double *c_T = ADI_vars->c_T;
  double *d_T = ADI_vars->d_T;
  double *c_p = ADI_vars->c_p;
  double *d_p = ADI_vars->d_p;

  const double *aP = coeffs->aP;
  const double *aE = coeffs->aE;
  const double *aW = coeffs->aW;
  const double *aN = coeffs->aN;
  const double *aS = coeffs->aS;
  const double *b = coeffs->b;

  /* ADI Y */
  /* First ADI step */

#pragma omp parallel default(none) private(I,J,index_) shared(a_T,b_T,c_T,d_T,c_p,d_p,aP,aE,aW,aN,aS,b,phi)
  {
#pragma omp for
    for(I=1; I<(Nx-1); I++) {

      /* Boundary condition for a_T, b_T, c_T, d_T */
      J = 0;
      index_ = J*Nx + I;
      a_T[index_] = (-1)*aN[index_];
      b_T[index_] = (-1)*aS[index_];
      c_T[index_] = b[index_];
      d_T[index_] = aP[index_];
      
      J = Ny-1;
      index_ = J*Nx + I;
      a_T[index_] = (-1)*aN[index_];
      b_T[index_] = (-1)*aS[index_];
      c_T[index_] = b[index_];
      d_T[index_] = aP[index_];
   

      for(J=1; J<(Ny-1); J++) {

	index_ = J*Nx + I;

	/* Setting values for a_T, b_T, c_T, d_T array */
	
	a_T[index_] = (-1)*aN[index_];
	b_T[index_] = (-1)*aS[index_];
	c_T[index_] = b[index_] + phi[index_ - 1]*aW[index_] + phi[index_ + 1]*aE[index_];
	if((I == (Nx-2)) && ((J == 0) || (J == (Ny-1)))){
	  c_T[index_] = b[index_] + phi[index_ - 1]*aW[index_];
	}
	else if((I == 1) && ((J == 0) || (J == (Ny-1)))){
	  c_T[index_] = b[index_] + phi[index_ + 1]*aE[index_];
	}
	d_T[index_] = aP[index_];
      }
    }
  }

#pragma omp barrier

#pragma omp parallel default(none) private(I, J, index_) shared(c_T, d_T, c_p, d_p, b_T, a_T)
  {
#pragma omp for
    /* Setting values for c_p and d_p */
    for(I=1; I<(Nx-1); I++){
      /* Boundary 0 */
      J = 0;
      index_ = J*Nx + I;
    
      c_p[index_] = c_T[index_];
      d_p[index_] = d_T[index_];
      for(J=1; J<Ny; J++){
	index_ = J*Nx + I;
	d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-Nx])/d_p[index_-Nx]);
	c_p[index_] = c_T[index_] - ((c_p[index_-Nx]*b_T[index_])/d_p[index_-Nx]);
      }
    }
  }
#pragma omp barrier

#pragma omp parallel default(none) private(I, J, index_) shared(c_p, d_p, phi, a_T)
  {
#pragma omp for
    /* Coefficients "backwards" */
    /* Boundary Ny-1 */
    for(I=1; I<(Nx-1); I++){

      J = Ny-1;
      index_ = J*Nx + I;

      phi[index_] = c_p[index_]/d_p[index_];

      for(J=(Ny-2); J>=0; J--){
	index_ = J*Nx + I;
	phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + Nx]) / (d_p[index_]);
      }
    }
  }
#pragma omp barrier

  /* Update */
  // copy_array(phi_next, phi, Nx, Ny); /* Necessary to respect boundary conditions!! */

  /* ADI X*/
  /* AUX vars for solving with TDMA: Second ADI step */

#pragma omp parallel default(none) private(I, J, index_) shared(a_T, b_T, c_T, d_T, aP, aE, aW, aN, aS, phi, b)
  {
#pragma omp for
    /* Second ADI step */
    for(J=1; J<(Ny-1); J++) {
      /* Boundary condition for a_T, b_T, c_T, d_T */
    
      I = 0;
      index_ = J*Nx + I;
    
      a_T[index_] = (-1)*aE[index_];
      b_T[index_] = (-1)*aW[index_];
      c_T[index_] = b[index_];
      d_T[index_] = aP[index_];

      I = Nx-1;
      index_ = J*Nx + I;

      a_T[index_] = (-1)*aE[index_];
      b_T[index_] = (-1)*aW[index_];
      c_T[index_] = b[index_];
      d_T[index_] = aP[index_];
      
      for(I=1; I<(Nx-1); I++) {

	index_ = J*Nx + I;
	/* Setting values for a_T, b_T, c_T, d_T array */
	a_T[index_] = (-1)*aE[index_];
	b_T[index_] = (-1)*aW[index_];
	c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_] + phi[index_ + Nx]*aN[index_];
	if((J == (Ny-2)) && ((I == 0) || (I == (Nx-1)))){
	  c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_];
	}
	else if((J == 1) && ((I == 0) || (I == (Nx-1)))){
	  c_T[index_] = b[index_] + phi[index_ + Nx]*aN[index_];
	}
	d_T[index_] = aP[index_];
      }
    }
  }

#pragma omp barrier

#pragma omp parallel default(none) private(I,J,index_) shared(a_T,b_T,c_T,d_T,c_p,d_p)
  {
#pragma omp for
    /* Setting values for c_p and d_p */
    for(J=1; J<(Ny-1); J++){
      I = 0;
      index_ = J*Nx + I;
    
      c_p[index_] = c_T[index_];
      d_p[index_] = d_T[index_];
    
      for(I=1; I<Nx; I++){
	index_ = J*Nx + I;
	d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-1])/d_p[index_-1]);
	c_p[index_] = c_T[index_] - ((c_p[index_-1]*b_T[index_])/d_p[index_-1]);
      }
    }
  }

#pragma omp barrier

#pragma omp parallel default(none) private(I,J,index_) shared(c_p,d_p,a_T,phi)
  {
#pragma omp for
    /* Boundary Nx-1 */
    /* Coefficientes "backwards" */
    for(J=1; J<(Ny-1); J++){
      I = Nx-1;
      index_ = J*Nx + I;
    
      phi[index_] = c_p[index_]/d_p[index_];
    
      for(I=(Nx-2); I>=0; I--){
	index_ = J*Nx + I;
	phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + 1])/(d_p[index_]);
      }
    }      
  }

  return;
}

/* ADI_SEQUENTIAL_2D */
void ADI_sequential_2D(double *phi, Coeffs *coeffs, const int Nx, const int Ny, const int dummy, ADI_Vars *ADI_vars){
  /* Declaring Variables */
  int I, J;
  int index_;

  double *a_T = ADI_vars->a_T;
  double *b_T = ADI_vars->b_T;
  double *c_T = ADI_vars->c_T;
  double *d_T = ADI_vars->d_T;
  double *c_p = ADI_vars->c_p;
  double *d_p = ADI_vars->d_p;

  const double *aP = coeffs->aP;
  const double *aE = coeffs->aE;
  const double *aW = coeffs->aW;
  const double *aN = coeffs->aN;
  const double *aS = coeffs->aS;
  const double *b = coeffs->b;

  /* ADI Y */
  /* First ADI step */

  for(I=1; I<(Nx-1); I++) {

    /* Boundary condition for a_T, b_T, c_T, d_T */
    J = 0;
    index_ = J*Nx + I;
    a_T[index_] = (-1)*aN[index_];
    b_T[index_] = (-1)*aS[index_];
    c_T[index_] = b[index_];
    d_T[index_] = aP[index_];
			  
    J = Ny-1;
    index_ = J*Nx + I;
    a_T[index_] = (-1)*aN[index_];
    b_T[index_] = (-1)*aS[index_];
    c_T[index_] = b[index_];
    d_T[index_] = aP[index_];
   
    for(J=1; J<(Ny-1); J++) {

      index_ = J*Nx + I;

      /* Setting values for a_T, b_T, c_T, d_T array */
	
      a_T[index_] = (-1)*aN[index_];
      b_T[index_] = (-1)*aS[index_];
      c_T[index_] = b[index_] + phi[index_ - 1]*aW[index_] + phi[index_ + 1]*aE[index_];
      if((I == (Nx-2)) && ((J == 0) || (J == (Ny-1)))){
	c_T[index_] = b[index_] + phi[index_ - 1]*aW[index_];
      }
      else if((I == 1) && ((J == 0) || (J == (Ny-1)))){
	c_T[index_] = b[index_] + phi[index_ + 1]*aE[index_];
      }
      d_T[index_] = aP[index_];
    }
  }

  /* Setting values for c_p and d_p */
  for(I=1; I<(Nx-1); I++){
    /* Boundary 0 */
    J= 0;
    index_ = J*Nx + I;
    
    c_p[index_] = c_T[index_];
    d_p[index_] = d_T[index_];

    for(J=1; J<Ny; J++){
      index_ = J*Nx + I;
      d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-Nx])/d_p[index_-Nx]);
      c_p[index_] = c_T[index_] - ((c_p[index_-Nx]*b_T[index_])/d_p[index_-Nx]);
    }
  }
  

  /* Coefficients "backwards" */
  /* Boundary Ny-1 */
  for(I=1; I<(Nx-1); I++){
    J = Ny-1;
    index_ = J*Nx + I;

    phi[index_] = c_p[index_]/d_p[index_];

    for(J=(Ny-2); J>=0; J--){
      index_ = J*Nx + I;
      phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + Nx]) / (d_p[index_]);
    }
  }
  
  /* Update */
  // copy_array(phi_next, phi, Nx, Ny); /* Necessary to respect boundary conditions!! */

  /* ADI x */
  /* AUX vars for solving with TDMA: Second ADI step */
  
  /* Second ADI step */
  for(J=1; J<(Ny-1); J++) {
    /* Boundary condition for a_T, b_T, c_T, d_T */

    I = 0;
    index_ = J*Nx + I;
    
    a_T[index_] = (-1)*aE[index_];
    b_T[index_] = (-1)*aW[index_];
    c_T[index_] = b[index_];
    d_T[index_] = aP[index_];

    I = Nx-1;
    index_ = J*Nx + I;

    a_T[index_] = (-1)*aE[index_];
    b_T[index_] = (-1)*aW[index_];
    c_T[index_] = b[index_];
    d_T[index_] = aP[index_];
      
    for(I=1; I<(Nx-1); I++) {

      index_ = J*Nx + I;
      /* Setting values for a_T, b_T, c_T, d_T array */
      a_T[index_] = (-1)*aE[index_];
      b_T[index_] = (-1)*aW[index_];
      c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_] + phi[index_ + Nx]*aN[index_];
      if((J == (Ny-2)) && ((I == 0) || (I == (Nx-1)))){
	c_T[index_] = b[index_] + phi[index_ - Nx]*aS[index_];
      }
      else if((J == 1) && ((I == 0) || (I == (Nx-1)))){
	c_T[index_] = b[index_] + phi[index_ + Nx]*aN[index_];
      }
      d_T[index_] = aP[index_];
    }
  }
  
  /* Setting values for c_p and d_p */
  for(J=1; J<(Ny-1); J++){
    I = 0;
    index_ = J*Nx + I;
    
    c_p[index_] = c_T[index_];
    d_p[index_] = d_T[index_];
    
    for(I=1; I<Nx; I++){
      index_ = J*Nx + I;
      d_p[index_] = d_T[index_] - ((b_T[index_]*a_T[index_-1])/d_p[index_-1]);
      c_p[index_] = c_T[index_] - ((c_p[index_-1]*b_T[index_])/d_p[index_-1]);
    }
  }
  
  /* Boundary Nx-1 */
  /* Coefficientes "backwards" */
  for(J=1; J<(Ny-1); J++){
    I = Nx-1;
    index_ = J*Nx + I;
    phi[index_] = c_p[index_]/d_p[index_];
    
    for(I=(Nx-2); I>=0; I--){
      index_ = J*Nx + I;
      phi[index_] = (c_p[index_] - a_T[index_]*phi[index_ + 1])/(d_p[index_]);
    }
  }      
  
  return;
}
