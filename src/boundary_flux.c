#include "include/some_defs.h"
/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_EXPERIMENTAL */
/**

   This scheme uses the gradients accross phases to estimate dphidN and dphidS, 
   which are evaluated at the faces (ewns).

   Consequently it does not call to compute_solid_der_at_interface.

   It does not uses the Lagrangian interpolation.

   It uses the harmonic and arithmetic transport property with the volume fraction
   computed at the displaced (velocity) cell, i.e., for an E fluid cell it uses
   alpha = v_fraction_x[index_+1];

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data
  
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_near_boundary_molecular_flux_experimental(Data_Mem *data){

  int count, index_;

  const int Nx = data->Nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;
  const int *dom_mat = data->domain_matrix;
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nyny;
  
  /* Extrapolado en la direccion x e y, respectivamente */
  double dphidSface_x, dphidSface_y; 
  double dphidxface, dphidyface;

  double dphi0, dphi1, f01;

  const double *fw = data->fw;
  const double *fs = data->fs;

  const double *fe = data->fe;
  const double *fn = data->fn;
  
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  
  /* These are the molecular flux components measured in W/m2 if they were energy.
   * These are computed at the face ==> must compute velocity if applicable.
   * They are computed from the solid phase.
   * Must be stored in the solid and fluid nodes which share the interface.
   */
  
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  
  const double *v_fraction_x = data->v_fraction_x;
  const double *v_fraction_y = data->v_fraction_y;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  
  /* Compute derivatives at the solid interfaces */
  compute_solid_der_at_interface(data);
  
  for(count=0; count<solid_boundary_cell_count; count++){
           
    index_ = sf_boundary_solid_indexs[count];

    /* East fluid cell ==> Internal boundary */
    if (dom_mat[index_+1] == 0){

      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];

      /* Volume fraction */
      alpha = v_fraction_x[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	
      /* Gradient estimated at the face using values of phi across phases */
      dphidxface = (phi[index_+1] - phi[index_]) / Delta_xE[index_];

      f01 = fe[index_];
      
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+1], phi[index_-Nx+1], Delta_yN[index_+1], Delta_yS[index_+1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxnx + dphidyface * nxny);
      
      /* J_par (only x component) */
      dphidSface_x = (1 - nxnx) * dphidxface - nxny * dphidyface;
      J_par[index_] = -gamma_a * dphidSface_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_+1] = J_x[index_];

    }
      
    /* West fluid cell */
    if (dom_mat[index_-1] == 0){

      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];

      /* Volume fraction */
      alpha = v_fraction_x[index_-1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidxface = (phi[index_] - phi[index_-1]) / Delta_xW[index_];

      f01 = fw[index_];
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx-1], phi[index_-1], phi[index_-Nx-1], Delta_yN[index_-1], Delta_yS[index_-1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxnx + dphidyface * nxny);

      /* J_par (only x component) */
      dphidSface_x = (1 - nxnx) * dphidxface - nxny * dphidyface;
      J_par[index_] = -gamma_a * dphidSface_x;

      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_-1] = J_x[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (dom_mat[index_+Nx] == 0){

      nxny = norm_vec_y_x[index_+Nx] * norm_vec_y_y[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_+Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidyface = (phi[index_+Nx] - phi[index_]) / Delta_yN[index_];

      f01 = fn[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxny + dphidyface * nyny);
      
      /* J_par (only y component) */
      dphidSface_y = -nxny * dphidxface + (1 - nyny) * dphidyface;
      J_par[index_] = -gamma_a * dphidSface_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_+Nx] = J_y[index_];
      
    }
    
    /* South (Fluid cell located at south position from solid domain) */
    if (dom_mat[index_-Nx] == 0){

      nxny = norm_vec_y_x[index_-Nx] * norm_vec_y_y[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_-Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidyface = (phi[index_] - phi[index_-Nx]) / Delta_yS[index_];

      f01 = fs[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxny + dphidyface * nyny);

      /* J_par (only y component) */
      dphidSface_y = -nxny * dphidxface + (1 - nyny) * dphidyface;
      J_par[index_] = -gamma_a * dphidSface_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_-Nx] = J_y[index_];
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_TSUTSUMI */
/**

   This scheme uses the gradients accross phases to estimate dphidN and dphidS, 
   across phases in a simplifyed manner, i.e.
   dphidx = (phiE - phiW]) / (2 * Delta_x[index_]);
   dphidy = (phiN - phiS]) / (2 * Delta_y[index_]);

   Moreover, it uses a tensorial construction common for all cases (EWNS)
   at the boundary fluid cell
   nxnx = norm_x[index_] * norm_x[index_];
   nxny = norm_x[index_] * norm_y[index_];
   nyny = norm_y[index_] * norm_y[index_];

   Consequently it does not call to compute_solid_der_at_interface.
   It does not uses the Lagrangian interpolation.
   It uses the harmonic and arithmetic transport property, for an E fluid cell it uses
   alpha = v_fraction[index_]; (no 'xyz')

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data
  
   @return void
   
   MODIFIED: 20-07-2020
*/
void compute_near_boundary_molecular_flux_Tsutsumi(Data_Mem *data){

  int count, index_;

  const int Nx = data->Nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nyny;
  
  double dphidx, dphidy;
  double dphidS_x, dphidS_y;
  
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
    
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;
  
  const double *v_fraction = data->v_fraction;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_x = data->norm_x;
  const double *norm_y = data->norm_y;
  
  for(count=0; count<solid_boundary_cell_count; count++){
      
    index_ = sf_boundary_solid_indexs[count];

    /* Derivatives */
    dphidx = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
    dphidy = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);

    /* East fluid cell ==> Internal boundary */
    if (E_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_+1];
      
      nxnx = norm_x[index_+1] * norm_x[index_+1];
      nxny = norm_x[index_+1] * norm_y[index_+1];
      nyny = norm_y[index_+1] * norm_y[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny);
	  
      /* x component of tangential flux */
      dphidS_x = (1-nxnx) * dphidx - nxny * dphidy;
      
      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_+1] = J_x[index_];

    }
      
    /* West fluid cell */
    if (W_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_-1];
      
      nxnx = norm_x[index_-1] * norm_x[index_-1];
      nxny = norm_x[index_-1] * norm_y[index_-1];
      nyny = norm_y[index_-1] * norm_y[index_-1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny);

      /* x component of tangential flux */
      dphidS_x = (1-nxnx) * dphidx - nxny * dphidy;

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_x;

      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_-1] = J_x[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (N_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_+Nx];
      
      nxnx = norm_x[index_+Nx] * norm_x[index_+Nx];
      nxny = norm_x[index_+Nx] * norm_y[index_+Nx];
      nyny = norm_y[index_+Nx] * norm_y[index_+Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxny + dphidy * nyny);

      dphidS_y = -nxny * dphidx + (1-nyny) * dphidy;

      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_+Nx] = J_y[index_];
      
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (S_is_fluid_OK[index_]){

      /* Volume fraction */
      alpha = v_fraction[index_-Nx];
      
      nxnx = norm_x[index_-Nx] * norm_x[index_-Nx];
      nxny = norm_x[index_-Nx] * norm_y[index_-Nx];
      nyny = norm_y[index_-Nx] * norm_y[index_-Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxny + dphidy * nyny);

      dphidS_y = -nxny * dphidx + (1-nyny) * dphidy;
      
      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_-Nx] = J_y[index_];
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_TSUTSUMI_DEF */
/**

   This is coded following the derivations from the paper of Tsutsumi 2014.
   It gives better results than Sato for more than gamma_s/gamma_f = 100.
   Difference being in the normal vector wich must be extracted from the ij cell.
   This must be corrected, or announced as a new method :).

   This scheme uses the gradients accross phases to estimate dphidN and dphidS, 
   across phases in a simplifyed manner, i.e.
   dphidx = (phiE - phiW]) / (2 * Delta_x[index_]);
   dphidy = (phiN - phiS]) / (2 * Delta_y[index_]);
   
   It is DEFerred in the sense that the tensor used is on the fluid side. 
   For E fluid is
   nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
   nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];
   
   Consequently it does not call to compute_solid_der_at_interface.
   It does not uses the Lagrangian interpolation.
   It uses the harmonic and arithmetic transport property, for an E fluid cell it uses
   alpha = v_fraction[index_]; (no 'xyz')

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data
  
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_near_boundary_molecular_flux_Tsutsumi_def(Data_Mem *data){
  
  int count, index_;

  const int Nx = data->Nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *dom_mat = data->domain_matrix;
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nyny;
  
  double dphidx, dphidy;
  double dphidS_x, dphidS_y;
  
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
    
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;
  
  const double *v_fraction = data->v_fraction;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  
  for(count=0; count<solid_boundary_cell_count; count++){
      
    index_ = sf_boundary_solid_indexs[count];
    
    /* Derivatives */
    dphidx = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
    dphidy = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
    
    /* Volume fraction */
    alpha = v_fraction[index_];

    /* East fluid cell ==> Internal boundary */
    if (dom_mat[index_+1] == 0){

      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny);
	  
      /* x component of tangential flux */
      dphidS_x = (1-nxnx) * dphidx - nxny * dphidy;

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_+1] = J_x[index_];
	  
    }
      
    /* West fluid cell */
    if (dom_mat[index_-1] == 0){

      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];

      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidx * nxnx + dphidy * nxny);

      /* x component of tangential flux */
      dphidS_x = (1-nxnx) * dphidx - nxny * dphidy;

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_x;

      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_-1] = J_x[index_];

    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (dom_mat[index_+Nx] == 0){

      nxny = norm_vec_y_x[index_+Nx] * norm_vec_y_y[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxny + dphidy * nyny);

      dphidS_y = -nxny * dphidx + (1-nyny) * dphidy;

      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_+Nx] = J_y[index_];
      
    }
    
    /* South (Fluid cell located at south position from solid domain) */
    if (dom_mat[index_-Nx] == 0){

      nxny = norm_vec_y_x[index_-Nx] * norm_vec_y_y[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];

      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidx * nxny + dphidy * nyny);

      dphidS_y = -nxny * dphidx + (1-nyny) * dphidy;
      
      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_-Nx] = J_y[index_];

    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_SATO */
/**

   Original implementation of Sato et al. (2016) with correction on its Eqs (41)-(42) 
   since it must be multiplied by the aritmethic mean transport property (gamma_a) 
   and NOT the phase alone property.

   Uses the solid phase derivatives for increased stability.

   There is an apparent confusion since there are combination of magnitudes 
   evaluated at the face and the interface:
   - dphidNface evaluated at the face (ewnstb). 
   - dphidS_iface, evaluated at the INTERface with Eq 45 Sato (2016).

   The derivatives stored in local arrays dphidx, dphidy are in a solid domain.

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at interface.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data

   @see compute_solid_der_at_interface
   
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_near_boundary_molecular_flux_Sato(Data_Mem *data){

  char cx, cy;

  const char *case_s_type_x = data->case_s_type_x;
  const char *case_s_type_y = data->case_s_type_y;
  
  int count, index_;

  const int Nx = data->Nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;
  const int *dom_mat = data->domain_matrix;
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nyny;

  double es;
  double es_x, es_y;
  double d, d10, d21;
  double L0, L1, L2;

  double dphi0, dphi1, f01;
  
  /* [0]: (I,J), [1]: (I+-1,J), [2]: (I,J+-1) */
  double dphidx[3] = {0}, dphidy[3] = {0}, dphidS[3] = {0};
  /* Extrapolado en la direccion x e y, respectivamente */
  double dphidS_iface_x, dphidS_iface_y; 
  double dphidxface, dphidyface;
  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  
  /* These are the molecular flux components measured in W/m2 if they were energy.
   * These are computed at the face ==> must compute velocity if applicable.
   * They are computed from the solid phase.
   * Must be stored in the solid and fluid nodes which share the interface.
   */
  
  double *J_x = data->J_x;
  double *J_y = data->J_y;
  
  const double *dphidxs = data->dphidxs;
  const double *dphidys = data->dphidys;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;

  const double *fw = data->fw;
  const double *fs = data->fs;

  const double *fe = data->fe;
  const double *fn = data->fn;
  
  const double *v_fraction_x = data->v_fraction_x;
  const double *v_fraction_y = data->v_fraction_y;
  
  const double *eps_x_sol = data->epsilon_x_sol;
  const double *eps_y_sol = data->epsilon_y_sol;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  
  /* Compute derivatives at the solid interfaces */
  compute_solid_der_at_interface(data);
  
  for(count=0; count<solid_boundary_cell_count; count++){
      
    index_ = sf_boundary_solid_indexs[count];
	
    cx = case_s_type_x[index_];
    cy = case_s_type_y[index_];

    es_x = eps_x_sol[index_];
    es_y = eps_y_sol[index_];
    
    /* dphidx_(I,J,K) */
    dphidx[0] = dphidxs[index_];
    /* dphidy_(I,J,K) */
    dphidy[0] = dphidys[index_];
		
    /* East fluid cell ==> Internal boundary */
    if (dom_mat[index_+1] == 0){

      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];

      /* Volume fraction */
      alpha = v_fraction_x[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	
      /* Gradient estimated at the face using values of phi across phases */
      dphidxface = (phi[index_+1] - phi[index_]) / Delta_xE[index_];

      f01 = fe[index_];
      
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+1], phi[index_-Nx+1], Delta_yN[index_+1], Delta_yS[index_+1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxnx + dphidyface * nxny);
	
      /* These derivatives must be in the solid phase */
      /* dphi_dx_(I-1,J,K), cases abc */
      dphidx[1] = dphidxs[index_-1];
      /* dphi_dx_(I-2,J,K), cases ab */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2];
	
      /* dphidy_(I-1,J,K), cases abc */
      dphidy[1] = dphidys[index_-1];
      /* dphidy_(I-2,J,K), cases ab */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2];
	  
      /* x component of tangential flux */
      dphidS[0] = (1 - nxnx) * dphidx[0] - nxny * dphidy[0];
      dphidS[1] = (1 - nxnx) * dphidx[1] - nxny * dphidy[1];
      dphidS[2] = (1 - nxnx) * dphidx[2] - nxny * dphidy[2];
	
      /* Lagrangian extrapolation to interface */
      /* Al copiar tener cuidado con indices y _x _xW */
      d = Delta_xE[index_];
      d10 = Delta_xW[index_];
      d21 = Delta_xW[index_-1];
      es = es_x;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_iface_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_+1] = J_x[index_];
  
    }
      
    /* West fluid cell */
    if (dom_mat[index_-1] == 0){

      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];

      /* Volume fraction */
      alpha = v_fraction_x[index_-1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidxface = (phi[index_] - phi[index_-1]) / Delta_xW[index_];

      f01 = fw[index_];
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx-1], phi[index_-1], phi[index_-Nx-1], Delta_yN[index_-1], Delta_yS[index_-1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxnx + dphidyface * nxny);
	
      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I+1, J) */
      dphidx[1] = dphidxs[index_+1];
      /* dphi/dx_(I+2, J) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2];

      /* dphi/dy_(I+1, J) */
      dphidy[1] = dphidys[index_+1];
      /* dphi/dy_(I+2, J) */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2];

      /* x component of tangential flux */
      dphidS[0] = (1 - nxnx) * dphidx[0] - nxny * dphidy[0];
      dphidS[1] = (1 - nxnx) * dphidx[1] - nxny * dphidy[1];
      dphidS[2] = (1 - nxnx) * dphidx[2] - nxny * dphidy[2];

      /* Lagrangian extrapolation to interface */
      /* Al copiar tener cuidado con indices y _x _xW */
      d = Delta_xW[index_];
      d10 = Delta_xE[index_];
      d21 = Delta_xE[index_+1];
      es = es_x;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidS_iface_x;

      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_-1] = J_x[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (dom_mat[index_+Nx] == 0){

      nxny = norm_vec_y_x[index_+Nx] * norm_vec_y_y[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_+Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidyface = (phi[index_+Nx] - phi[index_]) / Delta_yN[index_];

      f01 = fn[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxny + dphidyface * nyny);
	
      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J-1,K) */
      dphidx[1] = dphidxs[index_-Nx];
      /* dphi/dx_(I,J-2,K) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2*Nx];
      
      dphidy[1] = dphidys[index_-Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2*Nx];

      dphidS[0] = -nxny * dphidx[0] + (1 - nyny) * dphidy[0];
      dphidS[1] = -nxny * dphidx[1] + (1 - nyny) * dphidy[1];
      dphidS[2] = -nxny * dphidx[2] + (1 - nyny) * dphidy[2];
      
      /* Lagrangian extrapolation to interface */
      /* Al copiar tener cuidado con indices y _x _xW */
      d = Delta_yN[index_];
      d10 = Delta_yS[index_];
      d21 = Delta_yS[index_-Nx];
      es = es_y;
      
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }
      
      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_iface_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_+Nx] = J_y[index_];
            
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (dom_mat[index_-Nx] == 0){

      nxny = norm_vec_y_x[index_-Nx] * norm_vec_y_y[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_-Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidyface = (phi[index_] - phi[index_-Nx]) / Delta_yS[index_];

      f01 = fs[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxny + dphidyface * nyny);
	
      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I, J+1) */
      dphidx[1] = dphidxs[index_+Nx];
      /* dphi/dx_(I, J+2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2*Nx];
      
      dphidy[1] = dphidys[index_+Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2*Nx];

      dphidS[0] = -nxny * dphidx[0] + (1 - nyny) * dphidy[0];
      dphidS[1] = -nxny * dphidx[1] + (1 - nyny) * dphidy[1];
      dphidS[2] = -nxny * dphidx[2] + (1 - nyny) * dphidy[2];
            
      /* Lagrangian extrapolation to interface */
      /* Al copiar tener cuidado con indices y _x _xW */
      d = Delta_yS[index_];
      d10 = Delta_yN[index_];
      d21 = Delta_yN[index_+Nx];
      es = es_y;
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidS_iface_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }
      
      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidS_iface_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_-Nx] = J_y[index_];
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_SATO_AT_FACE */
/**

   Original implementation of Sato et al. (2016) with correction on its Eqs (41)-(42) 
   since it must be multiplied by the aritmethic mean transport property (gamma_a) 
   and NOT the phase alone property.

   Uses the solid phase derivatives for increased stability.

   This Sato at face takes de dphidS to the face instead of the interface.

   The derivatives stored in local arrays dphidx, dphidy are in a solid domain.

   Sets:

   Array (double, NxNy): Data_Mem::J_norm: diffusive flux in the direction normal to the interface.
   Array (double, NxNy): Data_Mem::J_par: indem par.
   Array (double, NxNy): Data_Mem::J_x: diffusive flux in x direction at face.
   Array (double, NxNy): Data_Mem::J_y: idem y.
     
   @param data

   @see compute_solid_der_at_interface
   
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_near_boundary_molecular_flux_Sato_at_face(Data_Mem *data){

  char cx, cy;

  const char *case_s_type_x = data->case_s_type_x;
  const char *case_s_type_y = data->case_s_type_y;

  int count, index_;

  const int Nx = data->Nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *dom_mat = data->domain_matrix;
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;
  
  double gamma_f, gamma_s;

  double alpha;
  
  /* Products of normal vector components */
  double nxnx, nxny, nyny;

  double d, d10, d21;
  double L0, L1, L2;

  double dphi0, dphi1, f01;
  
  /* Extrapolado en la direccion x e y, respectivamente */
  double dphidSface_x, dphidSface_y; 
  double dphidxface, dphidyface;

  /* [0]: (I,J), [1]: (I+-1,J), [2]: (I,J+-1) */
  double dphidx[3] = {0}, dphidy[3] = {0}, dphidS[3] = {0};

  const double es = 0.5;

  double *J_norm = data->J_norm;
  double *J_par = data->J_par;
  
  /* These are the molecular flux components measured in W/m2 if they were energy.
   * These are computed at the face ==> must compute velocity if applicable.
   * They are computed from the solid phase.
   * Must be stored in the solid and fluid nodes which share the interface.
   */
  
  double *J_x = data->J_x;
  double *J_y = data->J_y;

  const double *dphidxs = data->dphidxs;
  const double *dphidys = data->dphidys;
  
  const double *phi = data->phi;
  const double *gamma_m = data->gamma_m;
  
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;

  const double *fw = data->fw;
  const double *fs = data->fs;

  const double *fe = data->fe;
  const double *fn = data->fn;
  
  const double *v_fraction_x = data->v_fraction_x;
  const double *v_fraction_y = data->v_fraction_y;
  
  /* These are normalized (dimensionless) vector components */
  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  
  /* Compute derivatives at the solid interfaces */
  compute_solid_der_at_interface(data);
  
  for(count=0; count<solid_boundary_cell_count; count++){
            
    index_ = sf_boundary_solid_indexs[count];
    
    cx = case_s_type_x[index_];
    cy = case_s_type_y[index_];
	
    /* dphidx_(I,J) */
    dphidx[0] = dphidxs[index_];
    /* dphidy_(I,J) */
    dphidy[0] = dphidys[index_];
		
    /* East fluid cell ==> Internal boundary */
    if (dom_mat[index_+1] == 0){
    
      nxnx = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
      nxny = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];

      /* Volume fraction */
      alpha = v_fraction_x[index_+1];

      /* Phase properties */
      gamma_f = gamma_m[index_+1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;
	
      /* Gradient estimated at the face using values of phi across phases */
      dphidxface = (phi[index_+1] - phi[index_]) / Delta_xE[index_];

      f01 = fe[index_];
      
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+1], phi[index_-Nx+1], Delta_yN[index_+1], Delta_yS[index_+1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxnx + dphidyface * nxny);
	
      /* These derivatives must be in the solid phase */
      /* dphi_dx_(I-1,J,K), cases abc */
      dphidx[1] = dphidxs[index_-1];
      /* dphi_dx_(I-2,J,K), cases ab */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2];
	
      /* dphidy_(I-1,J,K), cases abc */
      dphidy[1] = dphidys[index_-1];
      /* dphidy_(I-2,J,K), cases ab */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2];
	  
      /* x component of tangential flux */
      dphidS[0] = (1 - nxnx) * dphidx[0] - nxny * dphidy[0];
      dphidS[1] = (1 - nxnx) * dphidx[1] - nxny * dphidy[1];
      dphidS[2] = (1 - nxnx) * dphidx[2] - nxny * dphidy[2];
	
      /* Take everything to the face */
      d = Delta_xE[index_];
      d10 = Delta_xW[index_];
      d21 = Delta_xW[index_-1];
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidSface_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidSface_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidSface_x;
	  
      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_+1] = J_x[index_];	  
    }
      
    /* West fluid cell */
    if (dom_mat[index_-1] == 0){
     
      nxnx = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
      nxny = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];

      /* Volume fraction */
      alpha = v_fraction_x[index_-1];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-1];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidxface = (phi[index_] - phi[index_-1]) / Delta_xW[index_];

      f01 = fw[index_];
      dphi0 = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      dphi1 = __central_der(phi[index_+Nx-1], phi[index_-1], phi[index_-Nx-1], Delta_yN[index_-1], Delta_yS[index_-1]);

      dphidyface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only x component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxnx + dphidyface * nxny);
	
      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I+1, J) */
      dphidx[1] = dphidxs[index_+1];
      /* dphi/dx_(I+2, J) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2];

      /* dphi/dy_(I+1, J) */
      dphidy[1] = dphidys[index_+1];
      /* dphi/dy_(I+2, J) */
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2];

      /* x component of tangential flux */
      dphidS[0] = (1 - nxnx) * dphidx[0] - nxny * dphidy[0];
      dphidS[1] = (1 - nxnx) * dphidx[1] - nxny * dphidy[1];
      dphidS[2] = (1 - nxnx) * dphidx[2] - nxny * dphidy[2];

      d = Delta_xW[index_];
      d10 = Delta_xE[index_];
      d21 = Delta_xE[index_+1];
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidSface_x = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidSface_x = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      /* J_par (only x component) */
      J_par[index_] = -gamma_a * dphidSface_x;

      J_x[index_] = J_norm[index_] + J_par[index_];
      J_x[index_-1] = J_x[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (dom_mat[index_+Nx] == 0){

      nxny = norm_vec_y_x[index_+Nx] * norm_vec_y_y[index_+Nx];
      nyny = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_+Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_+Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidyface = (phi[index_+Nx] - phi[index_]) / Delta_yN[index_];

      f01 = fn[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxny + dphidyface * nyny);
	
      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I,J-1,K) */
      dphidx[1] = dphidxs[index_-Nx];
      /* dphi/dx_(I,J-2,K) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_-2*Nx];
      
      dphidy[1] = dphidys[index_-Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_-2*Nx];

      dphidS[0] = -nxny * dphidx[0] + (1 - nyny) * dphidy[0];
      dphidS[1] = -nxny * dphidx[1] + (1 - nyny) * dphidy[1];
      dphidS[2] = -nxny * dphidx[2] + (1 - nyny) * dphidy[2];
	  
      d = Delta_yN[index_];
      d10 = Delta_yS[index_];
      d21 = Delta_yS[index_-Nx];
      
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidSface_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidSface_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }

      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidSface_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_+Nx] = J_y[index_];
            
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (dom_mat[index_-Nx] == 0){

      nxny = norm_vec_y_x[index_-Nx] * norm_vec_y_y[index_-Nx];
      nyny = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];

      /* Volume fraction */
      alpha = v_fraction_y[index_-Nx];
      
      /* Phase properties */
      gamma_f = gamma_m[index_-Nx];
      gamma_s = gamma_m[index_];

      /* Eq 40 Sato */
      gamma_h = gamma_s * gamma_f / ((1 - alpha) * gamma_s + alpha * gamma_f);
      gamma_a = (1 - alpha) * gamma_f + alpha * gamma_s;

      dphidyface = (phi[index_] - phi[index_-Nx]) / Delta_yS[index_];

      f01 = fs[index_];

      dphi0 = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      dphi1 = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
      
      dphidxface = (1 - f01)*dphi1 + f01*dphi0;
	  
      /* J_norm (only y component) */
      J_norm[index_] = -gamma_h * (dphidxface * nxny + dphidyface * nyny);
	
      /* These derivatives must be in the solid phase */
      /* dphi/dx_(I, J+1) */
      dphidx[1] = dphidxs[index_+Nx];
      /* dphi/dx_(I, J+2) */
      dphidx[2] = (cx == 'c') ? NAN : dphidxs[index_+2*Nx];
      
      dphidy[1] = dphidys[index_+Nx];
      dphidy[2] = (cy == 'c') ? NAN : dphidys[index_+2*Nx];

      dphidS[0] = -nxny * dphidx[0] + (1 - nyny) * dphidy[0];
      dphidS[1] = -nxny * dphidx[1] + (1 - nyny) * dphidy[1];
      dphidS[2] = -nxny * dphidx[2] + (1 - nyny) * dphidy[2];
	  
      d = Delta_yS[index_];
      d10 = Delta_yN[index_];
      d21 = Delta_yN[index_+Nx];
  
      if( isnan(dphidS[2]) ){
	/* Eq 46 Sato case NAN, only 2 points for extrapolation */
	L0 = (d10 + es*d) / d10;
	L1 = es*d/d10;
	
	dphidSface_y = L0 * dphidS[0] + L1 * dphidS[1];
      }
      else{
	/* Eq 46 Sato case normal */
	L0 = ((d10 + es*d) / d10) * ((d10 + es*d + d21) / (d21 + d10));
	L1 = (es*d / d10) * ((d10 + es*d + d21) / d21);
	L2 = (es*d / (d10 + d21)) * ((d10 + es*d) / d21);
	
	/* Eq 45 Sato, extrapolation to INTERface */
	dphidSface_y = L0 * dphidS[0] + L1 * dphidS[1] + L2 * dphidS[2];
      }
      
      /* J_par (only y component) */
      J_par[index_] = -gamma_a * dphidSface_y;

      J_y[index_] = J_norm[index_] + J_par[index_];
      J_y[index_-Nx] = J_y[index_];
    }
    
  } /* end for to solid_boundary_cell_count */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_CONVECTIVE_FLUX */
void compute_near_boundary_convective_flux(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: compute_near_boundary_convective_flux
    For conjugate heat transfer and nodes that are neighbours of sf_boundary_solid_indexs
    Sets:
    Array (double, NxNy) J_x: adds convective heat transport on x direction at interface
    to diffusive contribution calculated previously on function compute_near_boundary_molecular_flux_*.
    Array (double, NxNy) J_y: adds convective heat transport on y direction at interface
    to diffusive contribution calculated previously on function compute_near_boundary_molecular_flux_*.

    RETURNS: ret_value not implemented.
  
    MODIFIED: 08-02-2019
  */

  int I, J, i, j, count;
  int index_, index_u, index_v;

  const int Nx = data->Nx;
  const int nx = data->nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;
  
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *I_bm = data->I_bm;
  const int *J_bm = data->J_bm;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;

  /* Fluid side gradient at the boundary or face for evaluation of phi at face */
  double dphidxf, dphidyf;
  double phie, phiw, phin, phis;

  /* Storing the derivative for TVD scheme computations */
  double *dphidxb = data->dphidxb;
  double *dphidyb = data->dphidyb;

  double *J_x = data->J_x;
  double *J_y = data->J_y;

  const double *phi = data->phi;

  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;
  const double *gamma_m = data->gamma_m;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;

  
  for(count=0; count<solid_boundary_cell_count; count++){

    index_ = sf_boundary_solid_indexs[count];

    I = I_bm[count];
    J = J_bm[count];

    i = I-1;
    j = J-1;

    index_u = J*nx+i;
    index_v = j*Nx+I;

    if(E_is_fluid_OK[index_]){
      dphidxf = -J_x[index_] / (gamma_m[index_+1]);
      dphidxb[index_+1] = dphidxf;
      phie = phi[index_+1] - (Delta_x[index_+1] / 2) * dphidxf;
      J_x[index_+1] += rho_m[index_+1] * cp_m[index_+1] * u[index_u+1] * (phie - phi[index_+1]);
    }

    if(W_is_fluid_OK[index_]){
      dphidxf = -J_x[index_] / (gamma_m[index_-1]);
      dphidxb[index_-1] = dphidxf;
      phiw = phi[index_-1] + (Delta_x[index_-1] / 2) * dphidxf;
      J_x[index_-1] += rho_m[index_-1] * cp_m[index_-1] * u[index_u] * (phiw - phi[index_-1]);
    }

    if(N_is_fluid_OK[index_]){
      dphidyf = -J_y[index_] / (gamma_m[index_+Nx]);
      dphidyb[index_+Nx] = dphidyf;
      phin = phi[index_+Nx] - (Delta_y[index_+Nx] / 2) * dphidyf;
      J_y[index_+Nx] += rho_m[index_+Nx] * cp_m[index_+Nx] * v[index_v+Nx] * (phin - phi[index_+Nx]);
    }

    if(S_is_fluid_OK[index_]){
      dphidyf = -J_y[index_] / (gamma_m[index_-Nx]);
      dphidyb[index_-Nx] = dphidyf;
      phis = phi[index_-Nx] + (Delta_y[index_-Nx] / 2) * dphidyf;
      J_y[index_-Nx] += rho_m[index_-Nx] * cp_m[index_-Nx] * v[index_v] * (phis - phi[index_-Nx]);
    }
    
  }

  return;
}

/* COMPUTE_SOLID_DER_AT_INTERFACE */
/**
   Compute derivatives at solid points near the interface
   
   Sets:

   Array (double, NxNy): Data_Mem::dphidxs: derivatives in x direction up to two points within solid body, using only values of phi on solid domain.
   Array (double, NxNy): Data_Mem::dphidys: idem y.  
   
   @param data
   
   @return void
   
   MODIFIED: 11-07-2020
*/
void compute_solid_der_at_interface(Data_Mem *data){

  char cx, cy;

  const char *case_s_type_x = data->case_s_type_x;
  const char *case_s_type_y = data->case_s_type_y;
  
  int count, index_;

  const int Nx = data->Nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;

  double *dphidxs = data->dphidxs;
  double *dphidys = data->dphidys;
  
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;

  const double *phi = data->phi;
  
  for(count=0; count<solid_boundary_cell_count; count++){
    
    index_ = sf_boundary_solid_indexs[count];

    cx = case_s_type_x[index_];
    cy = case_s_type_y[index_];
		
    /* EW Block */
    if(cx == 'a'){
      if(E_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1], 1);
	dphidxs[index_-1] = __central_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1]);
	dphidxs[index_-2] = __central_der(phi[index_-1], phi[index_-2], phi[index_-3], Delta_xW[index_-1], Delta_xW[index_-2]);
      }
      else if(W_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_+1], phi[index_+2], Delta_xE[index_], Delta_xE[index_+1], -1);
	dphidxs[index_+1] = __central_der(phi[index_+2], phi[index_+1], phi[index_], Delta_xE[index_+1], Delta_xE[index_]);
	dphidxs[index_+2] = __central_der(phi[index_+3], phi[index_+2], phi[index_+1], Delta_xE[index_+2], Delta_xE[index_+1]);
      }
      else{
	dphidxs[index_] = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      }
    }

    if(cx == 'b'){
      if(E_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1], 1);
	dphidxs[index_-1] = __central_der(phi[index_], phi[index_-1], phi[index_-2], Delta_xW[index_], Delta_xW[index_-1]);
	dphidxs[index_-2] = (phi[index_-1] - phi[index_-2]) / Delta_xW[index_-1];
      }
      else if(W_is_fluid_OK[index_]){
	dphidxs[index_] = __asymm_der(phi[index_], phi[index_+1], phi[index_+2], Delta_xE[index_], Delta_xE[index_+1], -1);
	dphidxs[index_+1] = __central_der(phi[index_+2], phi[index_+1], phi[index_], Delta_xE[index_+1], Delta_xE[index_]);
	dphidxs[index_+2] = (phi[index_+2] - phi[index_+1]) / Delta_xE[index_+1];
      }
      else{
	dphidxs[index_] = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      }
    }
    
    if(cx == 'c'){
      if(E_is_fluid_OK[index_]){
	dphidxs[index_] = (phi[index_] - phi[index_-1]) / Delta_xW[index_];
      }
      else if(W_is_fluid_OK[index_]){
	dphidxs[index_] = (phi[index_+1] - phi[index_]) / Delta_xE[index_];
      }
      else{
	dphidxs[index_] = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
      }
    }
    
    /* N-S Block */
    if(cy == 'a'){
      if(N_is_fluid_OK[index_]){
	dphidys[index_] = __asymm_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx], 1);
	dphidys[index_-Nx] = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx]);
	dphidys[index_-2*Nx] = __central_der(phi[index_-Nx], phi[index_-2*Nx], phi[index_-3*Nx], Delta_yS[index_-Nx], Delta_yS[index_-2*Nx]);
      }
      else if(S_is_fluid_OK[index_]){
	dphidys[index_] = __asymm_der(phi[index_], phi[index_+Nx], phi[index_+2*Nx], Delta_yN[index_], Delta_yN[index_+Nx], -1);
	dphidys[index_+Nx] = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_], Delta_yN[index_+Nx], Delta_yN[index_]);
	dphidys[index_+2*Nx] = __central_der(phi[index_+3*Nx], phi[index_+2*Nx], phi[index_+Nx], Delta_yN[index_+2*Nx], Delta_yN[index_+Nx]);
      }
      else{
	dphidys[index_] = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      }
    }

    if(cy == 'b'){
      if(N_is_fluid_OK[index_]){
	dphidys[index_] = __asymm_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx], 1);
	dphidys[index_-Nx] = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx], Delta_yS[index_], Delta_yS[index_-Nx]);
	dphidys[index_-2*Nx] = (phi[index_-Nx] - phi[index_-2*Nx]) / Delta_yS[index_-Nx];
      }
      else if(S_is_fluid_OK[index_]){
	/* Eq 44 Sato */
	dphidys[index_] = __asymm_der(phi[index_], phi[index_+Nx], phi[index_+2*Nx], Delta_yN[index_], Delta_yN[index_+Nx], -1);
	dphidys[index_+Nx] = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_], Delta_yN[index_+Nx], Delta_yN[index_]);
	dphidys[index_+2*Nx] = (phi[index_+2*Nx] - phi[index_+Nx]) / Delta_yN[index_+Nx];
      }
      else{
	dphidys[index_] = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      }
    }

    if(cy == 'c'){
      if(N_is_fluid_OK[index_]){
	dphidys[index_] = (phi[index_] - phi[index_-Nx]) / Delta_yS[index_];
      }
      else if(S_is_fluid_OK[index_]){
	dphidys[index_] = (phi[index_+Nx] - phi[index_]) / Delta_yN[index_];
      }
      else{
	dphidys[index_] = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
      }
    }
	
  } /* end for(I=0; I<solid_boundary_cell_count; I++) */

  return;
}

/* COMPUTE_NEAR_BOUNDARY_MOLECULAR_FLUX_EXPERIMENTAL_SI */
void compute_near_boundary_molecular_flux_experimental_SI(Data_Mem *data){

  /* Semi-implicit version of experimental scheme for calculating 
     the conjugate heat transfer at interfaces */
  /*****************************************************************************/
  /*
    FUNCTION: compute_near_boundary_molecular_flux_experimental_SI
    For conjugate heat transfer
    Sets:
    Array (double, NxNy) coeffs_phi near boundary.

    RETURNS: ret_value not implemented.
  
    MODIFIED: 11-06-2019
  */

  int count, index_, index_u, index_v;
  int I, J, i, j;

  const int Nx = data->Nx;
  const int nx = data->nx;
  const int solid_boundary_cell_count = data->solid_boundary_cell_count;

  const int *dom_mat = data->domain_matrix;
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;
  const int *I_bm = data->I_bm;
  const int *J_bm = data->J_bm;
  
  /* Harmonic */
  double gamma_h;
  /* Arithmetic */
  double gamma_a;

  /* Diffusive like contribution */
  double D;
  /* Explicit source contribution */
  double exp_src;

  double exp_gamma, imp_gamma;
  double dphidxface, dphidyface;
  double phi_face;
  
  double *aW = data->coeffs_phi.aW;
  double *aE = data->coeffs_phi.aE;
  double *aS = data->coeffs_phi.aS;
  double *aN = data->coeffs_phi.aN;
  double *aP = data->coeffs_phi.aP;
  double *b = data->coeffs_phi.b;
  
  const double *phi = data->phi0;
  const double *gamma_m = data->gamma_m;
  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  
  const double *v_fraction_x = data->v_fraction_x;
  const double *v_fraction_y = data->v_fraction_y;

  /* Products of normal vector components */  
  const double *nxnx_x = data->nxnx_x;
  const double *nxny_x = data->nxny_x;
  const double *nyny_y = data->nyny_y;
  const double *nxny_y = data->nxny_y;

  const double *u = data->u;
  const double *v = data->v;

    
  for(count=0; count<solid_boundary_cell_count; count++){
           
    index_ = sf_boundary_solid_indexs[count];
    I = I_bm[count];
    J = J_bm[count];
    i = I-1;
    j = J-1;
    index_u = J*nx+i;
    index_v = j*Nx+I;

    /* East fluid cell */
    if (dom_mat[index_+1] == 0){

      /* Eq 40 Sato, index is shifted to the E because magnitudes are defined for the fluid */
      gamma_h = gamma_m[index_] * gamma_m[index_+1] / 
	((1 - v_fraction_x[index_+1]) * gamma_m[index_] + v_fraction_x[index_+1] * gamma_m[index_+1]);
      gamma_a = (1 - v_fraction_x[index_+1]) * gamma_m[index_+1] + v_fraction_x[index_+1] * gamma_m[index_];
	
      /* Gradient estimated at the face using values of phi across phases */
      dphidyface = (phi[index_+Nx+1] + phi[index_+Nx] - (phi[index_-Nx+1] + phi[index_-Nx])) / (4 * Delta_y[index_]);

      exp_gamma = (gamma_h - gamma_a) * nxny_x[count];
      imp_gamma = gamma_h * nxnx_x[count] + gamma_a * (1 - nxnx_x[count]);
	        
      /* Derivative in x direction: implicit, derivative in y direction: explicit */

      D = imp_gamma * Delta_y[index_] / Delta_x[index_];
      exp_src = exp_gamma * dphidyface * Delta_y[index_];

      aE[index_] += D;
      aP[index_] += D;
      b[index_] += exp_src;

      aW[index_+1] += D;
      aP[index_+1] += D;
      b[index_+1] -= exp_src;

      /* Convective contribution */
      phi_face = 0.5 * (phi[index_+1] + phi[index_]);
      b[index_+1] += rho_m[index_+1] * cp_m[index_+1] * u[index_u+1] * (phi_face - phi[index_+1]) * Delta_y[index_];

    }
      
    /* West fluid cell */
    if (dom_mat[index_-1] == 0){

      gamma_h = gamma_m[index_] * gamma_m[index_-1] / 
	((1 - v_fraction_x[index_-1]) * gamma_m[index_] + v_fraction_x[index_-1] * gamma_m[index_-1]);
      gamma_a = (1 - v_fraction_x[index_-1]) * gamma_m[index_-1] + v_fraction_x[index_-1] * gamma_m[index_];

      dphidyface = (phi[index_+Nx] + phi[index_-1+Nx] - (phi[index_-Nx] + phi[index_-1-Nx])) / (4 * Delta_y[index_]);

      exp_gamma = (gamma_h - gamma_a) * nxny_x[count];
      imp_gamma = gamma_h * nxnx_x[count] + gamma_a * (1 - nxnx_x[count]);
      
      /* Derivative in x direction: implicit, derivative in y direction: explicit */

      D = imp_gamma * Delta_y[index_] / Delta_x[index_];
      exp_src = exp_gamma * dphidyface * Delta_y[index_];

      aW[index_] += D;
      aP[index_] += D;
      b[index_] -= exp_src;

      aE[index_-1] += D;
      aP[index_-1] += D;
      b[index_-1] += exp_src;      

      /* Convective contribution */
      phi_face = 0.5 * (phi[index_-1] + phi[index_]);
      b[index_-1] -= rho_m[index_-1] * cp_m[index_-1] * u[index_u] * (phi_face - phi[index_-1]) * Delta_y[index_];
    }
      
    /* North (Fluid cell located at north position from solid domain) */
    if (dom_mat[index_+Nx] == 0){

      gamma_h = gamma_m[index_] * gamma_m[index_+Nx] / 
	((1 - v_fraction_y[index_+Nx]) * gamma_m[index_] + v_fraction_y[index_+Nx] * gamma_m[index_+Nx]);
      gamma_a = (1 - v_fraction_y[index_+Nx]) * gamma_m[index_+Nx] + v_fraction_y[index_+Nx] * gamma_m[index_];

      dphidxface = (phi[index_+1] + phi[index_+Nx+1] - (phi[index_-1] + phi[index_+Nx-1])) / (4 * Delta_x[index_]);
	  
      exp_gamma = (gamma_h - gamma_a) * nxny_y[count];
      imp_gamma = gamma_h * nyny_y[count] + gamma_a * (1 - nyny_y[count]);

      /* Derivative in y direction: implicit, derivative in x direction: explicit */

      D = imp_gamma * Delta_x[index_] / Delta_y[index_];
      exp_src = exp_gamma * dphidxface * Delta_x[index_];

      aN[index_] += D;
      aP[index_] += D;
      b[index_] += exp_src;

      aS[index_+Nx] += D;
      aP[index_+Nx] += D;
      b[index_+Nx] -= exp_src;      

      /* Convective contribution */
      phi_face = 0.5 * (phi[index_+Nx] + phi[index_]);
      b[index_+Nx] += rho_m[index_+Nx] * cp_m[index_+Nx] * v[index_v+Nx] * (phi_face - phi[index_+Nx]) * Delta_x[index_];
      
    }
	
    /* South (Fluid cell located at south position from solid domain) */
    if (dom_mat[index_-Nx] == 0){

      gamma_h = gamma_m[index_] * gamma_m[index_-Nx] / 
	((1 - v_fraction_y[index_-Nx]) * gamma_m[index_] + v_fraction_y[index_-Nx] * gamma_m[index_-Nx]);
      gamma_a = (1 - v_fraction_y[index_-Nx]) * gamma_m[index_-Nx] + v_fraction_y[index_-Nx] * gamma_m[index_];

      dphidxface = (phi[index_+1] + phi[index_-Nx+1] - (phi[index_-1] + phi[index_-Nx-1])) / (4 * Delta_x[index_]);

      exp_gamma = (gamma_h - gamma_a) * nxny_y[count];
      imp_gamma = gamma_h * nyny_y[count] + gamma_a * (1 - nyny_y[count]);

      /* Derivative in y direction: implicit, derivative in x direction: explicit */

      D = imp_gamma * Delta_x[index_] / Delta_y[index_];
      exp_src = exp_gamma * dphidxface * Delta_x[index_];

      aS[index_] += D;
      aP[index_] += D;
      b[index_] -= exp_src;

      aN[index_-Nx] += D;
      aP[index_-Nx] += D;
      b[index_-Nx] += exp_src;      

      /* Convective contribution */
      phi_face = 0.5 * (phi[index_-Nx] + phi[index_]);
      b[index_-Nx] -= rho_m[index_-Nx] * cp_m[index_-Nx] * v[index_v] * (phi_face - phi[index_-Nx]) * Delta_x[index_];

    }
  } /* end for to solid_boundary_cell_count */
    

  return;
}
