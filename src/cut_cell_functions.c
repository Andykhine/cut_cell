#include "include/some_defs.h"

/* CUT_CELL_JOBS */
void cut_cell_jobs(Data_Mem *data){

  char msg[SIZE];
  
  const char *geom_filename = data->geom_filename;

  int compute_cut_cell_jobs_OK = 1;

  const int read_cut_cell_data_OK = data->read_cut_cell_data_OK;
  const int turb_model = data->turb_model;
  const int solve_3D_OK = data->solve_3D_OK;

  Wins *wins = &(data->wins);

  progress_msg(wins, "cut cell", 0.0);
  
  if(read_cut_cell_data_OK){

    compute_cut_cell_jobs_OK = read_from_geom_file(data);         

  }

  if(compute_cut_cell_jobs_OK){

    /* If pressure node is on fluid domain then enable 
       computation of u, v through arrays 
       u_dom_matrix, v_dom_matrix.
       Sets:
       - u_dom_matrix (nxNyNz)
       - v_dom_matrix (NxnyNz)
       - w_dom_matrix (NxNynz) (3D case)
    */
    available_vel_nodes(data);

    /* Adds the pressure nodes that are on the boundary
       creating the array p_dom_matrix.
       Sets: 
       - p_dom_matrix (Expanded dom_matrix including cut_cells, NxNyNz)
    */
    add_boundary_p_nodes(data);
    
    /* Computes vertical and horizontal areas for each 
       staggered cut_cell.
       Sets: 
       - u_cut_matrix (!=0 if u cell is cut, nxNyNz)
       - v_cut_matrix (!=0 if v cell is cut, NxnyNz)
       - w_cut_matrix (!=0 if w cell is cut, NxNynz) (3D case)
       - u_cut_face_x (!=0 if face_x of u cell is cut, NxNyNz)
       - u_cut_face_y (!=0 if face_y of u cell is cut, nxnyNz)
       - u_cut_face_z (!=0 if face_z of u cell is cut, nxNynz) (3D case)        
       - v_cut_face_x (!=0 if face_x of v cell is cut, nxnyNz)
       - v_cut_face_y (!=0 if face_y of v cell is cut, NxNyNz)
       - v_cut_face_y (!=0 if face_z of v cell is cut, Nxnynz) (3D case)        
       - w_cut_face_x (!=0 if face_x of w cell is cut, nxNynz) (3D case)
       - w_cut_face_y (!=0 if face_y of w cell is cut, Nxnynz) (3D case)
       - w_cut_face_z (!=0 if face_z of w cell is cut, NxNyNz) (3D case)
       - u_faces_x (Effective area perp to x axis for u velocity component, NxNyNz)
       - u_faces_y (Effective area perp to y axis for u velocity component, nxnyNz)
       - u_faces_z (Effective area perp to z axis for u velocity component, nxNynz) (3D case)
       - v_faces_x (Effective area perp to x axis for v velocity component, nxnyNz)
       - v_faces_y (Effective area perp to y axis for v velocity component, NxNyNz)
       - v_faces_z (Effective area perp to z axis for v velocity component, Nxnynz) (3D case)
       - w_faces_x (Effective area perp to x axis for w velocity component, nxNynz) (3D case)
       - w_faces_y (Effective area perp to y axis for w velocity component, Nxnynz) (3D case)
       - w_faces_z (Effective area perp to z axis for w velocity component, NxNyNz) (3D case)

    */
    if(solve_3D_OK){
      if(set_cut_matrix_v_cells(data) != 0){
	error_msg(wins, "Velocity cut cells");
      }
      if(face_area_3D(data) != 0){
	error_msg(wins, "Velocity cells face area");
      }
    }
    else{
      if(face_area(data) != 0){
	error_msg(wins, "Velocity cells face area");
      }
    }

    progress_msg(wins, "cut cell", 20.0);
    
    /* Set coordinates for the velocities at the 
       center of the face cut by the polynomials and set
       pressure areas to 0 if they are within the solid or 
       to close to the interface.
       Sets:
       - p_cut_face_x (True values if x faces are cut, nxNyNz)
       - p_cut_face_y (True values if y faces are cut, NxnyNz)
       - p_cut_face_z (True values if z faces are cut, NxNynz) (3D case)
       - u_dom_matrix (Phase arrays for u cells, nxNyNz)
       - v_dom_matrix (Phase arrays for v cells, NxnyNz)
       - w_dom_matrix (Phase arrays for v cells, NxNynz) (3D case)
       - nodes_y_u (y coordinate of u nodes, nxNyNz)
       - nodes_z_u (z coordinate of u nodes, nxNyNz) (3D case)
       - nodes_x_v (x coordinate of v nodes, NxnyNz)
       - nodes_z_v (z coordinate of v nodes, NxnyNz) (3D case)
       - nodes_x_w (x coordinate of w nodes, NxNynz) (3D case)
       - nodes_y_w (y coordinate of w nodes, NxNynz) (3D case)
       - p_faces_x (Effective area perp to x in main grid, nxNyNz)
       - p_faces_y (Effective area perp to y in main grid, NxnyNz)
       - p_faces_z (Effective area perp to z in main grid, NxNynz) (3D case)
       - Delta_yN_u (Distance to N node from P in u cut_cell, nxNyNz)
       - Delta_yS_u (Distance to S node from P in u cut_cell, nxNyNz)
       - Delta_yn_u (Distance to n face from P in u cut_cell, nxNyNz)
       - Delta_ys_u (Distance to s face from P in u cut_cell, nxNyNz)
       - Delta_yT_u (Distance to T node from P in u cut_cell, nxNyNz) (3D case)
       - Delta_yB_u (Distance to B node from P in u cut_cell, nxNyNz) (3D case)
       - Delta_yt_u (Distance to t face from P in u cut_cell, nxNyNz) (3D case)
       - Delta_yb_u (Distance to b face from P in u cut_cell, nxNyNz) (3D case)
       - Delta_xE_v (Distance to E node from P in v cut_cell, NxnyNz)
       - Delta_xW_v (Distance to W node from P in v cut_cell, NxnyNz)
       - Delta_xe_v (Distance to e face from P in v cut_cell, NxnyNz)
       - Delta_xw_v (Distance to w face from P in v cut_cell, NxnyNz)
       - Delta_xT_v (Distance to T node from P in v cut_cell, NxnyNz) (3D case)
       - Delta_xB_v (Distance to B node from P in v cut_cell, NxnyNz) (3D case)
       - Delta_xt_v (Distance to t face from P in v cut_cell, NxnyNz) (3D case)
       - Delta_xb_v (Distance to b face from P in v cut_cell, NxnyNz) (3D case)
       - Delta_xE_w (Distance to E node from P in w cut_cell, NxnyNz) (3D case)
       - Delta_xW_w (Distance to W node from P in w cut_cell, NxnyNz) (3D case)
       - Delta_xe_w (Distance to e face from P in w cut_cell, NxnyNz) (3D case)
       - Delta_xw_w (Distance to w face from P in w cut_cell, NxnyNz) (3D case)
       - Delta_xN_w (Distance to N node from P in w cut_cell, NxnyNz) (3D case)
       - Delta_xS_w (Distance to S node from P in w cut_cell, NxnyNz) (3D case)
       - Delta_xn_w (Distance to n face from P in w cut_cell, NxnyNz) (3D case)
       - Delta_xs_w (Distance to s face from P in w cut_cell, NxnyNz) (3D case)

    */
    if(solve_3D_OK){
      set_cut_faces_p_cells(data);
      face_area_p_cells(data);
    }
    else{
      displace_vel_nodes(data);
    }
    
    /* Modifies p_dom_matrix assigning it cut_matrix values 
       whenever p_ver_lower_OK || p_hor_lower_OK.
       Sets:
       - p_dom_matrix
    */
    if(solve_3D_OK){
      find_small_pressure_cells_3D(data);
    }
    else{
      find_small_pressure_cells(data);
    }

  }
  
  /* Up to here can data be placed in geom.dat files safely */

  /* Explores the array p_dom_matrix within the fluid
     putting the corresponding slave cells.
     Sets:
     - u_slave_nodes: List of u velocity nodes in fluid that are set to zero.
     - v_slave_nodes: List of v velocity nodes in fluid that are set to zero.
     - w_slave_nodes: List of w velocity nodes in fluid that are set to zero (3D case).
  */
  if(solve_3D_OK){
    set_slave_nodes_3D(data);
  }
  else{
    set_slave_nodes(data);
  }
  
  /* Displays information of cut cells */
  cut_cell_count(data);

  progress_msg(wins, "cut cell", 40.0);

  /* Explores the array p_dom_matrix within the fluid
     putting the corresponding master cells.
     Sets:
     - u_master_nodes: List of master nodes for corresponding u_slave_nodes.
     - v_master_nodes: List of master nodes for corresponding v_slave_nodes.

  set_master_nodes(data);  */

  /* Determine correction factors in the vicinity of interface
     Sets:
     - alpha_u_x: Correction factor for u_e and u_w in momentum balance for u cell.
     - alpha_u_y: Correction factor for u_n and u_s in momentum balance for u cell.
     - alpha_v_x: Correction factor for v_e and v_w in momentum balance for v cell.
     - alpha_v_y: Correction factor for v_n and v_s in momentum balance for v cell.
     - u_cut_fw: Weight factor for determination of u_e and u_w in momentum balance for u cell.
     - u_cut_fe: Weight factor for determination of u_e and u_w in momentum balance for u cell.
     - v_cut_fs: Weight factor for determination of v_n and v_s in momentum balance for v cell.
     - v_cut_fn: Weight factor for determination of v_n and v_s in momentum balance for v cell.
     - diff_factor_u_face_x: Correction factor for diffusive momentum flux at w and e faces in momentum balance for u cell.
     - diff_factor_v_face_y: Correction factor for diffusive momentum flux at s and n faces in momentum balance for v cell.
  */
  if(solve_3D_OK){
    compute_alphas_3D(data);
  }
  else{
    compute_alphas(data);
  }

  /* Determine correction factors in the vicinity of interface
     Sets:
     - u_sol_factor: Factor due to stress on solid boundary on u cells.
     - u_p_factor: Factor due to pressure on solid boundary on u cells.
     - v_sol_factor: Factor due to stress on solid boundary on v cells.
     - v_p_factor: Factor due to pressure on solid boundary on v cells.
  */

  if(solve_3D_OK){
    if(compute_solid_factors_3D(data) != 0){
      error_msg(wins, "Solid factors");
    }    
  }
  else{
    if(compute_solid_factors(data) != 0){
      error_msg(wins, "Solid factors");
    }
  }

  progress_msg(wins, "cut cell", 60.0);
  
  /* Set p nodes located in solid domain to zero. */
  set_p_nodes_in_solid_to_zero(data);

  /* Set nodes corresponding to open boundary 
     Sets:
     - w_J: J indexes on west boundary that correspond to fluid.
     - w_K: K indexes on west boundary that correspond to fluid (3D case).
     - e_J: J indexes on east boundary that correspond to fluid.
     - e_K: K indexes on east boundary that correspond to fluid (3D case).
     - s_I: I indexes on south boundary that correspond to fluid.
     - s_K: K indexes on south boundary that correspond to fluid (3D case).
     - n_I: I indexes on north boundary that correspond to fluid.
     - n_K: K indexes on north boundary that correspond to fluid (3D case).
     - b_I: I indexes on bottom boundary that correspond to fluid (3D case).
     - b_J: J indexes on bottom boundary that correspond to fluid (3D case).
     - t_I: I indexes on top boundary that correspond to fluid (3D case).
     - t_J: J indexes on top boundary that correspond to fluid (3D case).
  */
  if(solve_3D_OK){
    open_bnd_indx_3D(data);
  }
  else{
    open_bnd_indx(data);
  }

  /* Calculate interpolation factor for values of transport properties at face 
     Sets: 
     - fe_u: Normalized distance between E neighbour and e face for u cell.
     - fw_u: Normalized distance between W neighbour and w face for u cell.
     - fn_u: Normalized distance between N neighbour and n face for u cell.
     - fs_u: Normalized distance between S neighbour and s face for u cell.
     - ft_u: Normalized distance between T neighbour and t face for u cell.
     - fb_u: Normalized distance between B neighbour and b face for u cell.
     - fe_v: Normalized distance between E neighbour and e face for v cell.
     - fw_v: Normalized distance between W neighbour and w face for v cell.
     - fn_v: Normalized distance between N neighbour and n face for v cell.
     - fs_v: Normalized distance between S neighbour and s face for v cell.
     - ft_v: Normalized distance between T neighbour and t face for v cell.
     - fb_v: Normalized distance between B neighbour and b face for v cell.
     - fe_w: Normalized distance between E neighbour and e face for w cell (3D case).
     - fw_w: Normalized distance between W neighbour and w face for w cell (3D case).
     - fn_w: Normalized distance between N neighbour and n face for w cell (3D case).
     - fs_w: Normalized distance between S neighbour and s face for w cell (3D case).
     - ft_w: Normalized distance between T neighbour and t face for w cell (3D case).
     - fb_w: Normalized distance between B neighbour and b face for w cell (3D case).

  */

  if(compute_fnsew_u_v(data) != 0){
    error_msg(wins, "Interpolation factors for velocity cells");
  }

  /* Compute v_fractions to use with periodic boundary conditions
     Sets:
     - vf_x : Solid fraction at u cells.
     - vf_y : Solid fraction at v cells.
     - vf_z : Solid fraction at w cells (3D case).
     - vol_x: Fluid volume at u cells.
     - vol_y: Fluid volume at v cells.
     - vol_z: Fluid volume at w cells (3D case).

  */

  if(solve_3D_OK){
    if(fluid_fraction_v_cells_3D(data) != 0){
      error_msg(wins, "Fluid fractions for velocity cells");
    }    
  }
  else{
    if(fluid_fraction_v_cells(data) != 0){
      error_msg(wins, "Fluid fractions for velocity cells");
    }
  }

  progress_msg(wins, "cut cell", 80.0);

  /* Compute interpolation factors to calculate Fu_y and Fv_x at cut cells 
     Sets:
     - u_cut_face_Fx_index
     - u_cut_face_Fx_W
     - u_cut_face_Fx_E
     - u_cut_face_Fx_index_v
     - v_cut_face_Fy_index
     - v_cut_face_Fy_S
     - v_cut_face_Fy_N
     - v_cut_face_Fy_index_u
  */


  if(solve_3D_OK){
    if(compute_fnsewtb_u_v_w_cut_cells(data) != 0){
      error_msg(wins, "Interpolation factors for convective fluxes at cut cells");
    }    
  }
  else{
    if(compute_fnsew_u_v_cut_cells(data) != 0){
      error_msg(wins, "Interpolation factors for convective fluxes at cut cells");
    }
  }

  progress_msg(wins, "cut cell", 90.0);

  if(turb_model){
  
    /* Find fluid pressure nodes without mu_turb_values 
       Sets:
       - p_nodes_in_solid_indx
       - p_nodes_in_solid_count
    */

    if(solve_3D_OK){
      p_nodes_in_solid_3D(data);
    }
    else{
      p_nodes_in_solid(data);
    }

    /* Interpolates mu_turb values to p_nodes_in_solid nodes */
    /* Called here because the required p_nodes_in_solid_indx array is not available
       yet when set_turb_init_values is called on main function. */
    interp_mu_turb_to_p_nodes_in_solid(data);

    /* mu_turb values at velocity nodes */
    if(solve_3D_OK){
      compute_mu_turb_at_vel_nodes_3D(data);
    }
    else{
      compute_mu_turb_at_vel_nodes(data);      
    }
    
    /* Boundary cells required for turbulence model solution
       Sets:
       - turb_boundary_indexs
       - turb_boundary_I
       - turb_boundary_J
       - turb_boundary_K
       - turb_boundary_cell_count
    */
    if(solve_3D_OK){
      if(turb_boundary_nodes_3D(data) != 0){
	error_msg(wins, "Turbulence boundary nodes computed");
      }      
    }
    else{
      if(turb_boundary_nodes(data) != 0){
	error_msg(wins, "Turbulence boundary nodes computed");
      }
    }
  } /* if(turb_model) */


  if(data->flow_scheme !=0){
    if(compute_cdist(data) != 0){
      error_msg(wins, "CDISTs computed");
    }
  }
  
  if(read_cut_cell_data_OK && !compute_cut_cell_jobs_OK){
    sprintf(msg, "cut_cell data read from %s", geom_filename);
    info_msg(wins, msg);
  }

  /* WRITE GEOMETRIC DATA */
  /* Sets flags in u,v,w_sol_type arrays for classification of cells near solid boundaries */
  set_sol_factor_type_cells(data);

  progress_msg(wins, "cut cell", 100.0);
  
  write_to_geom_file(data);

  /* Prints cut cell statistics into cut_cell_info.dat file */
  print_cut_cell_info(data);

  return;
}

/* AVAILABLE_VEL_NODES */
/*****************************************************************************/
/**

  Sets:

  Array (int, nxNyNz) u_dom_matrix: 0 if located on face of pressure node on fluid domain.

  Array (int, NxnyNz) v_dom_matrix: 0 if located on face of pressure node on fluid domain.

  Array (int, NxNynz) w_dom_matrix: 0 if located on face of pressure node on fluid domain.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 16-07-2019
*/
void available_vel_nodes(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_, index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int solve_3D_OK = data->solve_3D_OK;

  int *u_dom_matrix = data->u_dom_matrix;
  int *v_dom_matrix = data->v_dom_matrix;
  int *w_dom_matrix = data->w_dom_matrix;
  
  const int *dom_matrix = data->domain_matrix;

  if(solve_3D_OK){

#pragma omp parallel for default(none) private(I, J, K, index_, i, j, k, index_u, index_v, index_w) \
  shared(dom_matrix, u_dom_matrix, v_dom_matrix, w_dom_matrix)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;

	  if(dom_matrix[index_] == 0){

	    i = I-1;
	    j = J-1;
	    k = K-1;

	    index_u = K*nx*Ny + J*nx + i;
	    index_v = K*Nx*ny + j*Nx + I;
	    index_w = k*Nx*Ny + J*Nx + I;

	    u_dom_matrix[index_u] = 0;
	    u_dom_matrix[index_u+1] = 0;
	    v_dom_matrix[index_v] = 0;
	    v_dom_matrix[index_v+Nx] = 0;
	    w_dom_matrix[index_w] = 0;
	    w_dom_matrix[index_w+Nx*Ny] = 0;
	  }
	}
      }
    }
    
  }
  else{
  
#pragma omp parallel for default(none) private(I, J, index_, i, j, index_u, index_v) \
  shared(dom_matrix, u_dom_matrix, v_dom_matrix)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;

	if(dom_matrix[index_] == 0){

	  i = I-1;
	  j = J-1;

	  index_u = J*nx + i;
	  index_v = j*Nx + I;

	  u_dom_matrix[index_u] = 0;
	  u_dom_matrix[index_u+1] = 0;
	  v_dom_matrix[index_v] = 0;
	  v_dom_matrix[index_v+Nx] = 0;
	}
      }
    }

  }

  return;
}

/* ADD_BOUNDARY_P_NODES */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, NxNyNz) p_dom_matrix: 0 for nodes classified as fluid in domain_matrix
  and nodes on cut_matrix that have a fluid neighbour.
  
  @param data

  @return ret_value not implemented.
  
  MODIFIED: 17-07-2019
*/
void add_boundary_p_nodes(Data_Mem *data){
  
  int I, J, K;
  int index_;

  int fluid_nb_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int solve_3D_OK = data->solve_3D_OK;

  int *p_dom_matrix = data->p_dom_matrix;

  const int *dom_matrix = data->domain_matrix;
  const int *cut_matrix = data->cut_matrix;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *T_is_fluid_OK = data->T_is_fluid_OK;
  const int *B_is_fluid_OK = data->B_is_fluid_OK;

  copy_array_int(dom_matrix, p_dom_matrix, Nx, Ny, Nz);

  if(solve_3D_OK){
  
#pragma omp parallel for default(none) private(I, J, K, index_, fluid_nb_OK) \
  shared(E_is_fluid_OK, W_is_fluid_OK, N_is_fluid_OK, S_is_fluid_OK, T_is_fluid_OK, \
	 B_is_fluid_OK, dom_matrix, cut_matrix, p_dom_matrix)

    for(K=0; K<Nz; K++){
      for(J=0; J<Ny; J++){
	for(I=0; I<Nx; I++){

	  index_ = K*Nx*Ny + J*Nx + I;

	  fluid_nb_OK = E_is_fluid_OK[index_] || W_is_fluid_OK[index_] || 
	    N_is_fluid_OK[index_] || S_is_fluid_OK[index_] ||
	    T_is_fluid_OK[index_] || B_is_fluid_OK[index_];

	  if(fluid_nb_OK){
	    if(dom_matrix[index_] && cut_matrix[index_]){
	      p_dom_matrix[index_] = 0;
	    }
	  }

	}
      }
    }

  }
  else{

#pragma omp parallel for default(none) private(I, J, index_, fluid_nb_OK) \
  shared(E_is_fluid_OK, W_is_fluid_OK, N_is_fluid_OK, S_is_fluid_OK, dom_matrix, cut_matrix, p_dom_matrix)

    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){

	index_ = J*Nx + I;

	fluid_nb_OK = E_is_fluid_OK[index_] || W_is_fluid_OK[index_] || 
	  N_is_fluid_OK[index_] || S_is_fluid_OK[index_];

	if(fluid_nb_OK){
	  if(dom_matrix[index_] && cut_matrix[index_]){
	    p_dom_matrix[index_] = 0;
	  }
	}

      }
    }    
    
  }

  return;
}

/* FACE_AREA */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, nxNy) u_cut_matrix: set to curv_ind + 1 if u cell is cut.

  Array (int, Nxny) v_cut_matrix: set to curv_ind + 1 if v cell is cut.

  Array (int, NxNy) u_cut_face_x: set to curv_ind + 1 if face_x of u
  cell is cut.

  Array (int, nxny) u_cut_face_y: set to curv_ind + 1 if face_y of u
  cell is cut.

  Array (int, nxny) v_cut_face_x: set to curv_ind + 1 if face_x of v
  cell is cut.

  Array (int, NxNy) v_cut_face_y: set to curv_ind + 1 if face_y of v
  cell is cut.

  Array (double, NxNy) u_faces_x: area of u cell perpendicular to x axis
  in fluid domain.

  Array (double, nxny) u_faces_y: area of u cell perpendicular to y axis
  in fluid domain.

  Array (double, nxny) v_faces_x: area of v cell perpendicular to x axis
  in fluid domain.

  Array (double, NxNy) v_faces_y: area of v cell perpendicular to y axis
  in fluid domain.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 05-02-2019
*/
int face_area(Data_Mem *data){

  int curv_ind;
  int i, j;

  int I, J, index_, index_face, index_u, index_v;
  int ret_value = 0;

  const int curves = data->curves;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  
  int *u_cut_matrix = data->u_cut_matrix;
  int *v_cut_matrix = data->v_cut_matrix;
  int *u_cut_face_x = data->u_cut_face_x;
  int *u_cut_face_y = data->u_cut_face_y;
  int *v_cut_face_x = data->v_cut_face_x;
  int *v_cut_face_y = data->v_cut_face_y;
  
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double f0, f1;
  double curve_factor;
  double x0[3] = {0};
  double x1[3] = {0};
  double norm_v[3] = {0};
  double pc_temp[16];
  double x_int[3] = {0};
  double x_middle[3] = {0};

  const double pol_tol = data->pol_tol;
  const double cell_size = data->cell_size;

  double *u_faces_x = data->u_faces_x;
  double *u_faces_y = data->u_faces_y;
  double *v_faces_x = data->v_faces_x;
  double *v_faces_y = data->v_faces_y;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *poly_coeffs = data->poly_coeffs;

  /* Initialization of areas of u and v cells */
  
  // u_faces_x
#pragma omp parallel for default(none) private(I, J, index_) shared(u_faces_x, Delta_y)
  for(J=0; J<Ny; J++){
    for(I=0; I<Nx; I++){
      index_ = J*Nx+I;
      u_faces_x[index_] = Delta_y[index_];
    }
  }

  // u_faces_y
#pragma omp parallel for default(none) private(i, j, I, J, index_, index_face) \
  shared(u_faces_y, Delta_x)
  for(j=0; j<ny; j++){
    for(i=0; i<nx; i++){
      I = i+1;
      J = j+1;
      index_ = J*Nx+I;
      index_face = j*nx+i;
      u_faces_y[index_face] = 0.5 * (Delta_x[index_] + Delta_x[index_-1]);
    }
  }

  // v_faces_x
#pragma omp parallel for default(none) private(i, j, I, J, index_, index_face) \
  shared(v_faces_x, Delta_y)
  for(j=0; j<ny; j++){
    for(i=0; i<nx; i++){
      I = i+1;
      J = j+1;
      index_ = J*Nx+I;
      index_face = j*nx+i;
      v_faces_x[index_face] = 0.5 * (Delta_y[index_] + Delta_y[index_-Nx]);
    }
  }

  // v_faces_y
#pragma omp parallel for default(none) private(I, J, index_) shared(v_faces_y, Delta_x)
  for(J=0; J<Ny; J++){
    for(I=0; I<Nx; I++){
      index_ = J*Nx+I;
      v_faces_y[index_] = Delta_x[index_];
    }
  }

  /* Computing areas of cut cells and setting u_cut_matrix and v_cut_matrix */
  for(curv_ind=0; curv_ind<curves; curv_ind++){
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

    /* u_faces_x */
    norm_v[0] = 0;

#pragma omp parallel for default(none) firstprivate(x0, x1, norm_v, x_middle) \
  private(i, j, I, J, index_, index_u, index_v, f0, f1, x_int)		\
  shared(curve_factor, pc_temp, nodes_x_v, nodes_y_v, u_faces_x, u_cut_matrix, curv_ind, u_cut_face_x)

    for(J=1; J<Ny-1; J++){
      for(I=0; I<Nx; I++){

	i = I-1;
	j = J-1;

	index_ = J*Nx+I;
	index_u = J*nx+i;
	index_v = j*Nx+I;

	x0[0] = nodes_x_v[index_v];
	x1[0] = x0[0];
	x0[1] = nodes_y_v[index_v];
	x1[1] = nodes_y_v[index_v+Nx];

	f0 = polyn(x0, pc_temp) * curve_factor;
	f1 = polyn(x1, pc_temp) * curve_factor;

	if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	  x_middle[0] = 0.5 * (x0[0] + x1[0]);
	  x_middle[1] = 0.5 * (x0[1] + x1[1]);
	  if(polyn(x_middle, pc_temp) * curve_factor < 0){
	    u_faces_x[index_] = 0;
	    u_cut_face_x[index_] = curv_ind + 1;	    
	  }
	}
	else if((fabs(f0) < pol_tol) && (f1 < 0)){
	  u_faces_x[index_] = 0;
	  u_cut_face_x[index_] = curv_ind + 1;	    
	}
	else if((fabs(f1) < pol_tol) && (f0 < 0)){
	  u_faces_x[index_] = 0;
	  u_cut_face_x[index_] = curv_ind + 1;	    
	}
	else if(f0*f1 < 0){
	  if(f0 < 0){
	    norm_v[1] = 1 * curve_factor;
	    int_curv(x_int, x1, norm_v, pc_temp, cell_size);
	    x_int[1] = MIN(x1[1], MAX(x0[1], x_int[1]));
	    u_faces_x[index_] = x1[1] - x_int[1];
	  }
	  else{
	    norm_v[1] = -1 * curve_factor;
	    int_curv(x_int, x0, norm_v, pc_temp, cell_size);
	    x_int[1] = MIN(x1[1], MAX(x0[1], x_int[1]));
	    u_faces_x[index_] = x_int[1] - x0[1];
	  }
	  u_cut_face_x[index_] = curv_ind + 1;	    
	}
	else if((f0 < 0) && (f1 < 0))
	  u_faces_x[index_] = 0;

	if(u_cut_face_x[index_] == curv_ind + 1){
	  if(I == 0){
#pragma omp critical
	    {
	      u_cut_matrix[index_u+1] = curv_ind + 1;
	    }
	  }
	  else if(I == Nx - 1){
#pragma omp critical
	    {
	      u_cut_matrix[index_u] = curv_ind + 1;
	    }
	  }
	  else{
#pragma omp critical
	    {
	      u_cut_matrix[index_u] = curv_ind + 1;
	      u_cut_matrix[index_u+1] = curv_ind + 1;
	    }
	  }
	}
      }
    }

    /* u_faces_y */
    norm_v[1] = 0;

#pragma omp parallel for default(none) firstprivate(x0, x1, norm_v, x_middle) \
  private(i, j, I, J, index_face, index_u, index_v, f0, f1, x_int)	\
  shared(nodes_x_v, nodes_y_v, pc_temp, curve_factor, u_faces_y, u_cut_matrix, curv_ind, u_cut_face_y) 

    for(j=0; j<ny; j++){
      for(i=0; i<nx; i++){ 

	I = i+1;
	J = j+1;

	index_face = j*nx+i;
	index_u = J*nx+i;
	index_v = j*Nx+I;

	x0[0] = nodes_x_v[index_v-1];
	x1[0] = nodes_x_v[index_v];
	x0[1] = nodes_y_v[index_v];
	x1[1] = x0[1];

	f0 = polyn(x0, pc_temp) * curve_factor;
	f1 = polyn(x1, pc_temp) * curve_factor;
	
	if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	  x_middle[0] = 0.5 * (x0[0] + x1[0]);
	  x_middle[1] = 0.5 * (x0[1] + x1[1]);
	  if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	    u_faces_y[index_face] = 0;
	    u_cut_face_y[index_face] = curv_ind + 1;
	  }
	}
	else if((fabs(f0) < pol_tol) && (f1 < 0)){
	  u_faces_y[index_face] = 0;
	  u_cut_face_y[index_face] = curv_ind + 1;
	}
	else if((fabs(f1) < pol_tol) && (f0 < 0)){
	  u_faces_y[index_face] = 0;
	  u_cut_face_y[index_face] = curv_ind + 1;
	}
	else if(f0*f1 < 0){
	  if(f0 < 0){
	    norm_v[0] = 1 * curve_factor;
	    int_curv(x_int, x1, norm_v, pc_temp, cell_size);
	    x_int[0] = MIN(x1[0], MAX(x0[0], x_int[0]));
	    u_faces_y[index_face] = x1[0] - x_int[0];
	  }
	  else{
	    norm_v[0] = -1 * curve_factor;
	    int_curv(x_int, x0, norm_v, pc_temp, cell_size);
	    x_int[0] = MIN(x1[0], MAX(x0[0], x_int[0]));
	    u_faces_y[index_face] = x_int[0] - x0[0];
	  }
	  u_cut_face_y[index_face] = curv_ind + 1;
	}
	else if((f0 < 0) && (f1 < 0))
	  u_faces_y[index_face] = 0;
	
	if(u_cut_face_y[index_face] == curv_ind + 1){
#pragma omp critical
	  {
	    u_cut_matrix[index_u] = curv_ind + 1;
	    u_cut_matrix[index_u-nx] = curv_ind + 1;
	  }
	}
      }
    }

    /* v_faces_x */
    norm_v[0] = 0;

#pragma omp parallel for default(none) firstprivate(x0, x1, norm_v, x_middle) \
  private(i, j, I, J, index_face, index_u, index_v, f0, f1, x_int)	\
  shared(pc_temp, v_faces_x, v_cut_matrix, curv_ind, curve_factor, nodes_x_u, nodes_y_u, v_cut_face_x)

    for(j=0; j<ny; j++){
      for(i=0; i<nx; i++){

	I = i+1;
	J = j+1;

	index_face = j*nx+i;
	index_u = J*nx+i;
	index_v = j*Nx+I;

	x0[0] = nodes_x_u[index_u];
	x1[0] = x0[0];
	x0[1] = nodes_y_u[index_u-nx];
	x1[1] = nodes_y_u[index_u];

	f0 = polyn(x0, pc_temp) * curve_factor;
	f1 = polyn(x1, pc_temp) * curve_factor;

	if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	  x_middle[0] = 0.5 * (x0[0] + x1[0]);
	  x_middle[1] = 0.5 * (x0[1] + x1[1]);
	  if(polyn(x_middle, pc_temp) * curve_factor < 0){
	    v_faces_x[index_face] = 0;
	    v_cut_face_x[index_face]  = curv_ind + 1;
	  }
	}
	else if((fabs(f0) < pol_tol) && (f1 < 0)){
	  v_faces_x[index_face] = 0;
	  v_cut_face_x[index_face]  = curv_ind + 1;
	}
	else if((fabs(f1) < pol_tol) && (f0 < 0)){
	  v_faces_x[index_face] = 0;
	  v_cut_face_x[index_face]  = curv_ind + 1;
	}
	else if(f0*f1 < 0){
	  if(f0 < 0){
	    norm_v[1] = 1 * curve_factor;
	    int_curv(x_int, x1, norm_v, pc_temp, cell_size);
	    x_int[1] = MIN(x1[1], MAX(x0[1], x_int[1]));
	    v_faces_x[index_face] = x1[1] - x_int[1];
	  }
	  else{
	    norm_v[1] = -1 * curve_factor;
	    int_curv(x_int, x0, norm_v, pc_temp, cell_size);
	    x_int[1] = MIN(x1[1], MAX(x0[1], x_int[1]));
	    v_faces_x[index_face] = x_int[1] - x0[1];
	  }  
	  v_cut_face_x[index_face]  = curv_ind + 1;
	}
	else if((f0 < 0) && (f1 < 0))
	  v_faces_x[index_face] = 0;

	if(v_cut_face_x[index_face] == curv_ind + 1){
#pragma omp critical
	  {
	    v_cut_matrix[index_v] = curv_ind + 1;
	    v_cut_matrix[index_v-1] = curv_ind + 1;
	  }
	}
      }
    }

    /* v_faces_y */
    norm_v[1] = 0;

#pragma omp parallel for default(none) firstprivate(x0, x1, norm_v, x_middle) \
  private(i, j, I, J, index_, index_u, index_v, f0, f1, x_int)		\
  shared(nodes_x_u, nodes_y_u, pc_temp, curve_factor, v_faces_y, v_cut_matrix, curv_ind, v_cut_face_y)

    for(J=0; J<Ny; J++){
      for(I=1; I<Nx-1; I++){

	i = I-1;
	j = J-1;

	index_ = J*Nx+I;
	index_u = J*nx+i;
	index_v = j*Nx+I;

	x0[0] = nodes_x_u[index_u];
	x1[0] = nodes_x_u[index_u+1];
	x0[1] = nodes_y_u[index_u];
	x1[1] = x0[1];

	f0 = polyn(x0, pc_temp) * curve_factor;
	f1 = polyn(x1, pc_temp) * curve_factor;

	if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	  x_middle[0] = 0.5 * (x0[0] + x1[0]);
	  x_middle[1] = 0.5 * (x0[1] + x1[1]);
	  if(polyn(x_middle, pc_temp) * curve_factor < 0){
	    v_faces_y[index_] = 0;
	    v_cut_face_y[index_] = curv_ind + 1;
	  }
	}
	else if((fabs(f0) < pol_tol) && (f1 < 0)){
	  v_faces_y[index_] = 0;
	  v_cut_face_y[index_] = curv_ind + 1;
	}
	else if((fabs(f1) < pol_tol) && (f0 < 0)){
	  v_faces_y[index_] = 0;
	  v_cut_face_y[index_] = curv_ind + 1;
	}
	else if(f0*f1 < 0){
	  if(f0 < 0){
	    norm_v[0] = 1 * curve_factor;
	    int_curv(x_int, x1, norm_v, pc_temp, cell_size);
	    x_int[0] = MIN(x1[0], MAX(x0[0], x_int[0]));
	    v_faces_y[index_] = x1[0] - x_int[0];
	  }
	  else{
	    norm_v[0] = -1 * curve_factor;
	    int_curv(x_int, x0, norm_v, pc_temp, cell_size);
	    x_int[0] = MIN(x1[0], MAX(x0[0], x_int[0]));
	    v_faces_y[index_] = x_int[0] - x0[0];
	  }
	  v_cut_face_y[index_] = curv_ind + 1;
	}
	else if((f0 < 0) && (f1 < 0))
	  v_faces_y[index_] = 0;

	if(v_cut_face_y[index_] == curv_ind + 1){
	  if(J == 0){
#pragma omp critical
	    {
	      v_cut_matrix[index_v+Nx] = curv_ind + 1;
	    }
	  }
	  else if(J == Ny - 1){
#pragma omp critical
	    {
	      v_cut_matrix[index_v] = curv_ind + 1;
	    }
	  }
	  else{
#pragma omp critical
	    {
	      v_cut_matrix[index_v] = curv_ind + 1;
	      v_cut_matrix[index_v+Nx] = curv_ind + 1;
	    }
	  }
	}
      }
    }
  } // for(curv_ind=0; curv_ind<curves; curv_ind++)

  return ret_value;
}

/* DISPLACE_VEL_NODES */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, nxNy) p_cut_face_x: set to curv_ind + 1 if face_x of p cell
  is cut.

  Array (int, Nxny) p_cut_face_y: set to curv_ind + 1 if face_y of p cell
  is cut.

  Array (int, nxNy) u_dom_matrix: set u cells that are inside curve to curv_ind + 1.

  Array (int, Nxny) v_dom_matrix: set v cells that are inside curve to curv_ind + 1.

  Array (double, nxNy) nodes_y_u: modify values corresponding to u displaced nodes.

  Array (double, Nxny) nodes_x_v: modify values corresponding to v displaced nodes.

  Array (double, nxNy) p_faces_x: area of p cell perpendicular to x axis in fluid domain.

  Array (double, Nxny) p_faces_y: area of p cell perpendicular to y axis in fluid domain.

  Array (double, nxNy) Delta_yN_u: modify values corresponding to u displaced nodes.

  Array (double, nxNy) Delta_yS_u: modify values corresponding to u displaced nodes.

  Array (double, nxNy) Delta_yn_u: modify values corresponding to u displaced nodes.

  Array (double, nxNy) Delta_ys_u: modify values corresponding to u displaced nodes.

  Array (double, Nxny) Delta_xE_v: modify values corresponding to v displaced nodes.

  Array (double, Nxny) Delta_xW_v: modify values corresponding to v displaced nodes.

  Array (double, Nxny) Delta_xe_v: modify values corresponding to v displaced nodes.

  Array (double, Nxny) Delta_xw_v: modify values corresponding to v displaced nodes.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 05-02-2019
*/
void displace_vel_nodes(Data_Mem *data){
  int I, J, i, j;
  int index_, index_u, index_v;
  int curv_ind;
  int fluid_nb_OK;
  int fluid_nb_W_OK, fluid_nb_E_OK;
  int fluid_nb_S_OK, fluid_nb_N_OK;
  int int_curv_ret;

  const int Nx = data->Nx; 
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int curves = data->curves;

  int *p_cut_face_x = data->p_cut_face_x;
  int *p_cut_face_y = data->p_cut_face_y;
  int *u_dom_matrix = data->u_dom_matrix;
  int *v_dom_matrix = data->v_dom_matrix;
  
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  /* Variables to store polynomial values */
  double f0, f1; 
  double curve_factor;
  /* Variables to store locations for polynomial evaluation */
  double x0[3] = {0};
  double x1[3] = {0};
  double x2[3] = {0};
  double pc_temp[16];
  double norm_v[3] = {0};
  double x_int[3];

  const double cell_size = data->cell_size;
  const double pol_tol = data->pol_tol;

  double *nodes_x_v = data->nodes_x_v;
  double *nodes_y_u = data->nodes_y_u;
 
  double *p_faces_x = data->p_faces_x;
  double *p_faces_y = data->p_faces_y;
  
  double *Delta_yN_u = data->Delta_yN_u;
  double *Delta_yS_u = data->Delta_yS_u;
  
  double *Delta_yn_u = data->Delta_yn_u;
  double *Delta_ys_u = data->Delta_ys_u;

  double *Delta_xE_v = data->Delta_xE_v;
  double *Delta_xW_v = data->Delta_xW_v;
  
  double *Delta_xe_v = data->Delta_xe_v;  
  double *Delta_xw_v = data->Delta_xw_v;

  const double *poly_coeffs = data->poly_coeffs;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

#pragma omp parallel for default(none) private(I, J, i, index_, index_u) \
  shared(p_faces_x, Delta_y)
  
  for(J=0; J<Ny; J++){
    for(I=1; I<Nx; I++){

      i = I-1;

      index_ = J*Nx + I;
      index_u = J*nx + i;

      p_faces_x[index_u] = Delta_y[index_];
    }
  }

#pragma omp parallel for default(none) private(I, J, j, index_, index_v) \
  shared(p_faces_y, Delta_x)
  
  for(J=1; J<Ny; J++){
    for(I=0; I<Nx; I++){

      j = J-1;

      index_ = J*Nx + I;
      index_v = j*Nx + I;

      p_faces_y[index_v] = Delta_x[index_];
    }
  }

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }

    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
    /* Displace u velocity component */  
    norm_v[0] = 0;

#pragma omp parallel for default(none) firstprivate(x0, x1, x2, norm_v) \
  private(i, I, J, index_, index_u, fluid_nb_OK, f0, f1, x_int,		\
	  fluid_nb_W_OK, fluid_nb_E_OK, fluid_nb_S_OK, fluid_nb_N_OK,	\
	  int_curv_ret)							\
  shared(nodes_x, nodes_y, Delta_x, Delta_y, curve_factor, pc_temp,	\
	 p_faces_x, p_cut_face_x,					\
	 nodes_y_u, u_dom_matrix, Delta_yn_u, Delta_ys_u, curv_ind)
    
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){

	I = i + 1;

	index_ = J*Nx + I;
	index_u = J*nx + i;

	/* These u_dom_matrix are polyns on the middlepoint of uncut faces (so far)  */
	/* Potential race condition here?, better to use omp critical */
	if(i > 0){
	  fluid_nb_W_OK = (u_dom_matrix[index_u-1] == 0);
	}
	else{
	  fluid_nb_W_OK = 0;
	}

	if(i < nx-1){
	  fluid_nb_E_OK = (u_dom_matrix[index_u+1] == 0);
	}
	else{
	  fluid_nb_E_OK = 0;
	}

	fluid_nb_N_OK = (u_dom_matrix[index_u+nx] == 0);
	fluid_nb_S_OK = (u_dom_matrix[index_u-nx] == 0);
	
	fluid_nb_OK = fluid_nb_N_OK || fluid_nb_S_OK || fluid_nb_E_OK || fluid_nb_W_OK;
	
	// Displace velocity node only if located in the vicinity of fluid

	x0[0] = nodes_x[index_] - 0.5 * Delta_x[index_];
	x1[0] = x0[0];
	x0[1] = nodes_y[index_] - 0.5 * Delta_y[index_];
	x1[1] = nodes_y[index_] + 0.5 * Delta_y[index_];

	f0 = polyn(x0, pc_temp) * curve_factor; 
	f1 = polyn(x1, pc_temp) * curve_factor;

	if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	  /* f0, f1 values are close to line
	     evaluate middle point */
	  // x0[0], x1[0] are equal
	  avg_vec(x2, x0, x1);
	  
	  if(polyn(x2, pc_temp) * curve_factor < 0){
	    // Middlepoint is within solid
	    p_faces_x[index_u] = 0;
	    u_dom_matrix[index_u] = curv_ind + 1;
	    /* u node is located on solid surface, so there is no need to solve for it */	    
	  }

	}
	else if((fabs(f0) < pol_tol) && (f1 < 0)){
	  p_cut_face_x[index_u] = curv_ind + 1;
	  p_faces_x[index_u] = 0;
	  nodes_y_u[index_u] = x0[1];
	  u_dom_matrix[index_u] = curv_ind + 1;
	  /* u node is located on solid surface, so there is no need to solve for it */
	}
	else if((fabs(f1) < pol_tol) && (f0 < 0)){
	  p_cut_face_x[index_u] = curv_ind + 1;
	  p_faces_x[index_u] = 0;
	  nodes_y_u[index_u] = x1[1];
	  u_dom_matrix[index_u] = curv_ind + 1;
	  /* u node is located on solid surface, so there is no need to solve for it */
	}
	else if((f0 * f1 < 0)){
	  p_cut_face_x[index_u] = curv_ind + 1;
	  /* Add node to available list, 
	     takes care of nodes displaced to fluid region, 
	     not labeled as available based on p_node location criteria */
	  if(fluid_nb_OK){
	    u_dom_matrix[index_u] = 0; 

	    if(f0 < 0){
	      norm_v[1] = 1 * curve_factor;
	      int_curv_ret = int_curv(x_int, x1, norm_v, pc_temp, cell_size);
	      /* Constraint intersection value to correct range */
	      x_int[1] = MIN(x1[1], MAX(x_int[1], x0[1]));
	      nodes_y_u[index_u] = 0.5 * (x1[1] + x_int[1]);
	      p_faces_x[index_u] = x1[1] - x_int[1];
	    }
	    else{
	      norm_v[1] = -1 * curve_factor;
	      int_curv_ret = int_curv(x_int, x0, norm_v, pc_temp, cell_size);
	      x_int[1] = MIN(x1[1], MAX(x_int[1], x0[1]));
	      nodes_y_u[index_u] = 0.5 * (x0[1] + x_int[1]);
	      p_faces_x[index_u] = x_int[1] - x0[1];
	    }
	  }
	}
	else if((f0 < 0) && (f1 < 0)){
	  /* Este es el caso de una cara completamente sumergida pero con al
	     menos un vecino fluido, me parece sería precisamente la
	     diferencia entre las propuestas interpolativas. */
	  p_faces_x[index_u] = 0;
	  u_dom_matrix[index_u] = curv_ind + 1;
	  /* No modifica la posición del nodo velocidad, solo anula su cómputo
	     con la matriz de dominio correspondiente. Se extrae que la
	     distancia en la misma línea (EW) requiere recalcularse, como se
	     hace en la función de alphas, pero que hay de las otras distancias
	     (NSTB).... deberían computarse mediante diferencias para estar
	     del lado seguro. */
	  /* u node is located on solid surface, so there is no need to solve for it */
	}

	/* These deltas can be calculated here without race conditions because
	   neighbour nodes coordinates are not used. */
	if(p_cut_face_x[index_u] == curv_ind + 1){
	  Delta_yn_u[index_u] = x1[1] - nodes_y_u[index_u];
	  Delta_ys_u[index_u] = nodes_y_u[index_u] - x0[1];
	}

      } // end for i
    } // end for J

    /* Displace v velocity component */
    norm_v[1] = 0;

#pragma omp parallel for default(none) firstprivate(x0, x1, x2, norm_v) \
  private(I, j, J, index_, index_v, fluid_nb_OK, fluid_nb_S_OK, fluid_nb_N_OK, f0, f1, x_int, int_curv_ret) \
  shared(nodes_x, nodes_y, Delta_x, Delta_y, p_faces_y, p_cut_face_y, nodes_x_v, v_dom_matrix, pc_temp, Delta_xe_v, \
	 Delta_xw_v, curve_factor, curv_ind)

    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){

	J = j + 1;

	index_ = J*Nx + I;
	index_v = j*Nx + I;

	if(j > 0){
	  fluid_nb_S_OK = (v_dom_matrix[index_v-Nx] == 0);
	}
	else{
	  fluid_nb_S_OK = 0;
	}

	if(j < ny-1){
	  fluid_nb_N_OK = (v_dom_matrix[index_v+Nx] == 0);
	}
	else{
	  fluid_nb_N_OK = 0;
	}

	fluid_nb_OK = fluid_nb_N_OK || fluid_nb_S_OK ||
	  (v_dom_matrix[index_v+1] == 0) || (v_dom_matrix[index_v-1] == 0);
	// Displace velocity node only if located in the vicinity of fluid

	x0[0] = nodes_x[index_] - 0.5 * Delta_x[index_];
	x1[0] = nodes_x[index_] + 0.5 * Delta_x[index_];
	x0[1] = nodes_y[index_] - 0.5 * Delta_y[index_];
	x1[1] = x0[1];

	f0 = polyn(x0, pc_temp) * curve_factor; 
	f1 = polyn(x1, pc_temp) * curve_factor;

	if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){

	  avg_vec(x2, x0, x1);

	  if(polyn(x2, pc_temp) * curve_factor < 0){
	    p_faces_y[index_v] = 0;
	    v_dom_matrix[index_v] = curv_ind + 1;
	  }
	}
	else if((fabs(f0) < pol_tol) && (f1 < 0)){
	  p_cut_face_y[index_v] = curv_ind + 1;
	  p_faces_y[index_v] = 0;
	  nodes_x_v[index_v] = x0[0];
	  v_dom_matrix[index_v] = curv_ind + 1;
	}
	else if((fabs(f1) < pol_tol) && (f0 < 0)){
	  p_cut_face_y[index_v] = curv_ind + 1;
	  p_faces_y[index_v] = 0;
	  nodes_x_v[index_v] = x1[0];
	  v_dom_matrix[index_v] = curv_ind + 1;
	}
	else if((f0 * f1 < 0)){
	  p_cut_face_y[index_v] = curv_ind + 1;

	  if(fluid_nb_OK){
	    v_dom_matrix[index_v] = 0; /* Add node to available list, takes care of nodes displaced to fluid region,
					  not labeled as available based on p_node location criteria */

	    if(f0 < 0){
	      norm_v[0] = 1 * curve_factor;
	      int_curv_ret = int_curv(x_int, x1, norm_v, pc_temp, cell_size);
	      x_int[0] = MIN(x1[0], MAX(x_int[0], x0[0]));
	      nodes_x_v[index_v] = 0.5 * (x1[0] + x_int[0]);
	      p_faces_y[index_v] = x1[0] - x_int[0];
	    }
	    else{
	      norm_v[0] = -1 * curve_factor;
	      int_curv_ret = int_curv(x_int, x0, norm_v, pc_temp, cell_size);
	      x_int[0] = MIN(x1[0], MAX(x_int[0], x0[0]));
	      nodes_x_v[index_v] = 0.5 * (x0[0] + x_int[0]);
	      p_faces_y[index_v] = x_int[0] - x0[0];
	    }
	  }
	}
	else if((f0 < 0) && (f1 < 0)){
	  p_faces_y[index_v] = 0;
	  v_dom_matrix[index_v] = curv_ind + 1;
	}

	/* These deltas can be calculated here without race conditions because
	   neighbour nodes coordinates are not used. */
	if(p_cut_face_y[index_v] == curv_ind + 1){
	  Delta_xe_v[index_v] = x1[0] - nodes_x_v[index_v];
	  Delta_xw_v[index_v] = nodes_x_v[index_v] - x0[0];
	}	
      }	  
    }

  } // for(curv_ind=0; curv_ind<curves; curv_ind++)


  /* Computing deltas for u nodes */
  
#pragma omp parallel for default(none) private(i, J, index_u) \
  shared(p_cut_face_x, Delta_yN_u, Delta_yS_u, nodes_y_u)
  
  for(J=1; J<Ny-1; J++){
    for(i=0; i<nx; i++){

      index_u = J*nx + i;

      /* If a x face is cut the u velocity node was displaced */
      if(p_cut_face_x[index_u] != 0){
#pragma omp critical
	{
	  Delta_yN_u[index_u] = nodes_y_u[index_u+nx] - nodes_y_u[index_u];
	  Delta_yS_u[index_u+nx] = Delta_yN_u[index_u];
	  Delta_yS_u[index_u] = nodes_y_u[index_u] - nodes_y_u[index_u - nx];
	  Delta_yN_u[index_u-nx] = Delta_yS_u[index_u];
	}
      }
    }
  }

  /* Computing deltas for v nodes */

#pragma omp parallel for default(none) private(I, j, index_v) \
  shared(p_cut_face_y, Delta_xE_v, nodes_x_v, Delta_xW_v)

  for(j=0; j<ny; j++){
    for(I=1; I<Nx-1; I++){

      index_v = j*Nx + I;

      /* If a y face is cut the u velocity node was displaced */
      if(p_cut_face_y[index_v] != 0){
#pragma omp critical
	{
	  Delta_xE_v[index_v] = nodes_x_v[index_v+1] - nodes_x_v[index_v];
	  Delta_xW_v[index_v+1] = Delta_xE_v[index_v];
	  Delta_xW_v[index_v] = nodes_x_v[index_v] - nodes_x_v[index_v-1];
	  Delta_xE_v[index_v-1] = Delta_xW_v[index_v];
	}
      }
    }
  }

  return;
}

/* FIND_SMALL_PRESSURE_CELLS */
/*****************************************************************************/
/**

  Sets:

  Array (int, NxNy) p_dom_maxtrix: set to curv_ind + 1 for small pressure cells.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 05-02-2019
*/
void find_small_pressure_cells(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;
  int p_x_lower_OK = 0, p_y_lower_OK = 0;
  int u_x_lower_OK, v_y_lower_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;

  int *p_dom_matrix = data->p_dom_matrix;

  const int *cut_matrix = data->cut_matrix;

  const double kappa = data->kappa;

  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;

#pragma omp parallel for default(none)				\
  private(I, J, i, j, index_, index_u, index_v,			\
	  p_x_lower_OK, p_y_lower_OK,				\
	  u_x_lower_OK, v_y_lower_OK)				\
  shared(cut_matrix, p_dom_matrix, p_faces_x, p_faces_y,	\
	 Delta_y, Delta_x, u_faces_x, v_faces_y)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      i = I-1;
      j = J-1;

      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;

      if((cut_matrix[index_]) && (p_dom_matrix[index_] == 0)){
	// Border cells
	p_x_lower_OK = (p_faces_x[index_u] < kappa * Delta_y[index_]) && 
	  (p_faces_x[index_u+1] < kappa * Delta_y[index_]);
	p_y_lower_OK = (p_faces_y[index_v] < kappa * Delta_x[index_]) &&
	  (p_faces_y[index_v+Nx] < kappa * Delta_x[index_]);
	
	u_x_lower_OK = (u_faces_x[index_] < kappa * Delta_y[index_]);
	v_y_lower_OK = (v_faces_y[index_] < kappa * Delta_x[index_]);
	
	if (u_x_lower_OK && v_y_lower_OK){
	  // Do not compute since its enclosing x or y faces are too small
	  p_dom_matrix[index_] = cut_matrix[index_];
	}
	else if(u_x_lower_OK && p_y_lower_OK){
	  p_dom_matrix[index_] = cut_matrix[index_];
	}
	else if(v_y_lower_OK && p_x_lower_OK){
	  p_dom_matrix[index_] = cut_matrix[index_];
	}
	else if(p_y_lower_OK && p_x_lower_OK){
	  p_dom_matrix[index_] = cut_matrix[index_];
	}
      }
    }
  }

  return;
}

/* SET_SLAVE_NODES */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, u_slave_count) u_slave_nodes: u cells classified as slaves.

  Array (int, v_slave_count) v_slave_nodes: v cells classified as slaves. 

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 05-02-2019
*/
void set_slave_nodes(Data_Mem *data){

  int I, J, i, j;
  int index_, index_u, index_v;
  int count = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  
  const int *p_dom_matrix = data->p_dom_matrix;

  /* u slave velocity nodes */
  count = 0;

#pragma omp parallel for default(none) private(i, I, J, index_, index_u) \
  shared(u_dom_matrix, p_dom_matrix) reduction(+: count)
  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      I = i+1;

      index_ = J*Nx + I;
      index_u = J*nx + i;

      if(u_dom_matrix[index_u] == 0){
	// Fluid domain u cell
	if((p_dom_matrix[index_-1]) || (p_dom_matrix[index_])){
	  // Not having 2 pressure nodes to make the gradient in x direction
	  count++;
	}
      }
    }
  }

  if(count > 0){

    data->u_slave_count = count;
    // Saves index_u of nodes
    data->u_slave_nodes = create_array_int(count, 1, 1, 0);
    data->u_slave_at_E_of_p_OK = create_array_int(count, 1, 1, 0);
    data->u_slave_i = create_array_int(count, 1, 1, 0);
    data->u_slave_J = create_array_int(count, 1, 1, 0);
  
    int *u_slave_nodes = data->u_slave_nodes;
    int *u_slave_at_E_of_p_OK = data->u_slave_at_E_of_p_OK;
    int *u_slave_i = data->u_slave_i;
    int *u_slave_J = data->u_slave_J;
  
    count = 0; 
  
#pragma omp parallel for default(none) private(i, I, J, index_, index_u) \
  shared(u_dom_matrix, p_dom_matrix, u_slave_nodes, u_slave_at_E_of_p_OK, count, u_slave_i, u_slave_J)
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
      
	I = i+1;
      
	index_ = J*Nx + I;
	index_u = J*nx + i;
      
	if(u_dom_matrix[index_u] == 0){
	
	  if((p_dom_matrix[index_-1]) || (p_dom_matrix[index_])){
	  
#pragma omp critical
	    {
	      u_slave_nodes[count] = index_u;
	      u_slave_i[count] = i;
	      u_slave_J[count] = J;

	      if(p_dom_matrix[index_]){
		/* 1: u node is at east position relative to fluid pressure node */
		u_slave_at_E_of_p_OK[count] = 1;
	      }

	      count++;
	  
	    }
	  }
	}
      }
    }
  }

  /* v slave velocity nodes */
  count = 0;

#pragma omp parallel for default(none) private(I, j, J, index_, index_v) \
  shared(v_dom_matrix, p_dom_matrix) reduction(+:count)
  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){

      J = j+1;

      index_ = J*Nx+I;
      index_v = j*Nx+I;

      if(v_dom_matrix[index_v] == 0){
	if((p_dom_matrix[index_-Nx]) || (p_dom_matrix[index_])){
	  count++;
	}
      }
    }
  }

  if(count > 0){

    data->v_slave_count = count;
    data->v_slave_nodes = create_array_int(count, 1, 1, 0);
    data->v_slave_at_N_of_p_OK = create_array_int(count, 1, 1, 0);
    data->v_slave_I = create_array_int(count, 1, 1, 0);
    data->v_slave_j = create_array_int(count, 1, 1, 0);

    int *v_slave_nodes = data->v_slave_nodes;
    int *v_slave_at_N_of_p_OK = data->v_slave_at_N_of_p_OK;
    int *v_slave_I = data->v_slave_I;
    int *v_slave_j = data->v_slave_j;

    count = 0;

#pragma omp parallel for default(none) private(I, j, J, index_, index_v) \
  shared(v_dom_matrix, p_dom_matrix, v_slave_nodes, v_slave_at_N_of_p_OK, count, v_slave_I, v_slave_j)

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){

	J = j + 1;

	index_ = J*Nx + I;
	index_v = j*Nx + I;

	if(v_dom_matrix[index_v] == 0){
	  if((p_dom_matrix[index_-Nx]) || (p_dom_matrix[index_])){
#pragma omp critical
	    {
	      v_slave_nodes[count] = index_v;
	      v_slave_I[count] = I;
	      v_slave_j[count] = j;

	      if(p_dom_matrix[index_]){
		/* 1: v node is at north position relative to fluid pressure node */
		v_slave_at_N_of_p_OK[count] = 1;
	      }
	      count++;
	    }
	  }
	}
      }
    }
  }

  return;
}

/* COMPUTE_ALPHAS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) Data_Mem::alpha_u_x: Correction factor for u_e and u_w in momentum
  balance for u cell. 

  Array (double, nxny) Data_Mem::alpha_u_y: Correction factor for u_n and u_s in momentum
  balance for u cell.

  Array (double, nxny) Data_Mem::alpha_v_x: Correction factor for v_e and v_w in momentum 
  balance for v cell.

  Array (double, NxNy) Data_Mem::alpha_v_y: Correction factor for v_n and v_s in momentum 
  balance for v cell.

  Array (double, NxNy) Data_Mem::u_cut_fw: Weight factor for determination of u_e and u_w
  in momentum balance for u cell.

  Array (double, NxNy) Data_Mem::u_cut_fe: Weight factor for determination of u_e and u_w
  in momentum balance for u cell.

  Array (double, NxNy) Data_Mem::v_cut_fs: Weight factor for determination of v_n and v_s
  in momentum balance for v cell.

  Array (double, NxNy) Data_Mem::v_cut_fn: Weight factor for determination of v_n and v_s
  in momentum balance for v cell.

  Array (double, diff_u_face_x_count) Data_Mem::diff_factor_u_face_x: Correction factor for
  diffusive momentum flux at w and e faces in momentum balance for u cell.

  Array (double, diff_v_face_y_count) Data_Mem::diff_factor_v_face_y: Correction factor for
  diffusive momentum flux at s and n faces in momentum balance for v cell.

  Data_Mem::alpha_u_x, Data_Mem::alpha_v_y, Data_Mem::alpha_w_z: Only internal values are needed.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 19-02-2019
*/
void compute_alphas(Data_Mem *data){
  
  int i, I, j, J;
  int index_, index_u, index_v;
  int index_p;
  int curv_ind;
  int test_is_solid;
  int some_face_cut_OK;
  int W_is_solid_OK, E_is_solid_OK;
  int S_is_solid_OK, N_is_solid_OK;

  int diff_u_face_x_count = 0;
  int diff_v_face_y_count = 0;

  const int Ny = data->Ny;
  const int Nx = data->Nx;
  const int nx = data->nx;
  const int ny = data->ny;
  const int curves = data->curves;
  const int flow_scheme = data->flow_scheme;

  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;
  const int *u_cut_face_x = data->u_cut_face_x;
  const int *u_cut_face_y = data->u_cut_face_y;
  const int *v_cut_face_x = data->v_cut_face_x;
  const int *v_cut_face_y = data->v_cut_face_y;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double curve_factor;
  double dist_e, dist_w;
  double dist_s, dist_n;
  double fW, fE;
  double fS, fN;
  double x_W, x_E;
  double y_S, y_N;
  double dist_f;
  double delta_h;

  double den;

  double x_test[3] = {0};
  double pc_temp[16];
  double norm_v[3] = {0};
  double S[2] = {0};
  double E_coord[3] = {0};
  double W_coord[3] = {0};
  double N_coord[3] = {0};
  double S_coord[3] = {0};
  double face_center_coord[3] = {0};
  double face_coord[3] = {0};

  const double tol = 1e-5;
  const double pol_tol = data->pol_tol;
  const double cell_size = data->cell_size;

  double *alpha_u_x = data->alpha_u_x;
  double *alpha_u_y = data->alpha_u_y;
  double *alpha_v_x = data->alpha_v_x;
  double *alpha_v_y = data->alpha_v_y;
  double *u_cut_fw = data->u_cut_fw;
  double *u_cut_fe = data->u_cut_fe;
  double *v_cut_fs = data->v_cut_fs;
  double *v_cut_fn = data->v_cut_fn;

  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_yN_u = data->Delta_yN_u;
  const double *Delta_yn_u = data->Delta_yn_u;
  const double *Delta_xe_v = data->Delta_xe_v;
  const double *Delta_xE_v = data->Delta_xE_v;
  const double *poly_coeffs = data->poly_coeffs;

  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;

  /* alpha_u_x */

#pragma omp parallel for default(none)					\
  private(I, J, i, index_, index_u, index_p)				\
  shared(u_faces_x, Delta_y, u_cut_fe, u_cut_fw, nodes_x, nodes_x_u)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      i = I - 1;
      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_p = J*Nx + I;

      if(u_faces_x[index_p] > tol * Delta_y[index_p]){
	/* Default values */
	u_cut_fe[index_] = (nodes_x[index_p] - nodes_x_u[index_u]) / 
	  (nodes_x_u[index_u+1] - nodes_x_u[index_u]);
	u_cut_fw[index_] = 1 - u_cut_fe[index_];	  
      }
      else{
	/* Interior face */
	u_cut_fw[index_] = 0;
	u_cut_fe[index_] = 0;
      }
    }
  }

  for(curv_ind=0; curv_ind<curves; curv_ind++){

    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }

    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none)					\
  firstprivate(x_test, E_coord, W_coord, face_center_coord)		\
  private(i, j, I, J, index_, index_u, index_p, index_v, test_is_solid, dist_e, dist_w, dist_f, fE, fW, \
	  some_face_cut_OK, W_is_solid_OK, E_is_solid_OK, x_W, x_E, den) \
  shared(u_faces_x, Delta_x, Delta_y, u_cut_face_x, p_cut_face_x, nodes_x, nodes_y, pc_temp, curve_factor, \
	 u_dom_matrix, nodes_x_u, nodes_y_u, alpha_u_x, v_faces_y, p_faces_y, curv_ind, u_cut_fw, u_cut_fe)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	j = J - 1;

	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_p = J*Nx + I;
	index_v = j*Nx + I;

	if(u_faces_x[index_p] > tol * Delta_y[index_p]){

	  /* Any of these conditions imply that the interpolated velocity is not 
	     aligned with the velocity nodes used for interpolation */

	  some_face_cut_OK = (u_cut_face_x[index_p] == curv_ind + 1) || (p_cut_face_x[index_u] == curv_ind + 1) 
	    || (p_cut_face_x[index_u+1] == curv_ind + 1);
	  W_is_solid_OK = (u_dom_matrix[index_u] == curv_ind + 1);
	  E_is_solid_OK = (u_dom_matrix[index_u+1] == curv_ind + 1);

      
	  if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK){
	    /* Need to calculate an alpha_value */

	    /* Determine if inferior limit of face is located inside the solid */

	    x_test[0] = nodes_x[index_p];
	    x_test[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p];
	    
	    if(fabs(polyn(x_test, pc_temp)) < pol_tol){
	      x_test[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];
	      test_is_solid = (polyn(x_test, pc_temp) * curve_factor > 0);
	    }
	    else{
	      test_is_solid = (polyn(x_test, pc_temp) * curve_factor < 0);	  
	    }

	    if((!W_is_solid_OK) && (!E_is_solid_OK)){
	      /* Both W and E nodes in fluid domain */

	      /* East neighbour */
	      E_coord[0] = nodes_x_u[index_u+1];
	      E_coord[1] = nodes_y_u[index_u+1];

	      /* West neighbour */
	      W_coord[0] = nodes_x_u[index_u];
	      W_coord[1] = nodes_y_u[index_u];

	      u_cut_fe[index_] = (nodes_x[index_p] - nodes_x_u[index_u]) / 
		(nodes_x_u[index_u+1] - nodes_x_u[index_u]);
	      u_cut_fw[index_] = 1 - u_cut_fe[index_];	      
	    
	    }
	    else if (u_cut_face_x[index_] == 0){
	      /* Interpolate in horizontal direction to solid surface */
	      if(E_is_solid_OK){

		x_E = nodes_x[index_p] + v_faces_y[index_p] - 0.5 * Delta_x[index_p];
		  
		/* East neighbour: point on solid surface */
		E_coord[0] = x_E;
		E_coord[1] = nodes_y[index_p];

		/* West neighbour */
		W_coord[0] = nodes_x_u[index_u];
		W_coord[1] = nodes_y_u[index_u];

		u_cut_fw[index_] = 1 - (nodes_x[index_p] - nodes_x_u[index_u]) /
		  (x_E - nodes_x_u[index_u]);
		u_cut_fe[index_] = 0;
		
	      }	    
	      else{
		
		x_W = nodes_x[index_p] - v_faces_y[index_p] + 0.5 * Delta_x[index_p];
		
		/* East neighbour */
		E_coord[0] = nodes_x_u[index_u+1];
		E_coord[1] = nodes_y_u[index_u+1];
	      
		/* West neighbour: point on solid surface */
		W_coord[0] = x_W;
		W_coord[1] = nodes_y[index_p];

		u_cut_fw[index_] = 0;
		u_cut_fe[index_] = (nodes_x[index_p] - x_W) /
		  (nodes_x_u[index_u+1] - x_W);

	      }
	    }
	    else{
	      /* Interpolate to solid surface */
	      if(E_is_solid_OK){

		u_cut_fe[index_] = 0;

		/* West neighbour */
		W_coord[0] = nodes_x_u[index_u];
		W_coord[1] = nodes_y_u[index_u];

		/* East neighbour: point on solid surface */
		if(test_is_solid){		
		  x_E = nodes_x[index_p] + p_faces_y[index_v+Nx] - 0.5 * Delta_x[index_p];
		  E_coord[0] = x_E;
		  E_coord[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];
		}
		else{
		  x_E = nodes_x[index_p] + p_faces_y[index_v] - 0.5 * Delta_x[index_p];
		  E_coord[0] = x_E;
		  E_coord[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p];
		}
		
		u_cut_fw[index_] = 1 - (nodes_x[index_p] - nodes_x_u[index_u]) /
		  (x_E - nodes_x_u[index_u]);

	      }
	      else{

		u_cut_fw[index_] = 0;

		/* East neighbour */
		E_coord[0] = nodes_x_u[index_u+1];
		E_coord[1] = nodes_y_u[index_u+1];
	      
		/* West neighbour: point on solid surface */
		if(test_is_solid){
		  x_W = nodes_x[index_p] - p_faces_y[index_v+Nx] + 0.5 * Delta_x[index_p];
		  W_coord[0] = x_W;
		  W_coord[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];
		}
		else{
		  x_W = nodes_x[index_p] - p_faces_y[index_v] + 0.5 * Delta_x[index_p];
		  W_coord[0] = x_W;
		  W_coord[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p];
		}

		u_cut_fe[index_] = (nodes_x[index_p] - x_W) /
		  (nodes_x_u[index_u+1] - x_W);
		
	      }	    
	    }

#ifdef OLDINT_OK
	    /* East neighbour */
	    E_coord[0] = nodes_x_u[index_u+1];
	    E_coord[1] = nodes_y_u[index_u+1];

	    /* West neighbour */
	    W_coord[0] = nodes_x_u[index_u];
	    W_coord[1] = nodes_y_u[index_u];

	    u_cut_fe[index_] = (nodes_x[index_p] - nodes_x_u[index_u]) / 
	      (nodes_x_u[index_u+1] - nodes_x_u[index_u]);
	    u_cut_fw[index_] = 1 - u_cut_fe[index_];	      	    
#endif	    

	    /* Common for all three cases */

	    // Distance from east neighbour to the curve
	    dist_e = calc_dist(E_coord, pc_temp, cell_size);
	    // Distance from west neighbour to the curve
	    dist_w = calc_dist(W_coord, pc_temp, cell_size);

	    /* This is supposed to be the node in between the u_W and u_E nodes */

	    face_center_coord[0] = nodes_x[index_p];

	    if(test_is_solid){
	      // x_test falls within the solid body
	      face_center_coord[1] = nodes_y[index_p] + 0.5 * (Delta_y[index_p] - u_faces_x[index_p]);
	    }
	    else{
	      // x_test is in the fluid region
	      face_center_coord[1] = nodes_y[index_p] + 0.5 * (-Delta_y[index_p] + u_faces_x[index_p]);
	    }
	  
	    dist_f = calc_dist(face_center_coord, pc_temp, cell_size);
	  
	    fE = (face_center_coord[0] - W_coord[0]) / (E_coord[0] - W_coord[0]);
	    fW = 1 - fE;

	    /* Build alpha based on distances computed, see eqs 18 to 21 Kirkpatrick
	       This affects convective terms only */
	    den = dist_e * fE + dist_w * fW;
	    alpha_u_x[index_] = (den <= 0) ? 1 : dist_f / den;
	    //	    alpha_u_x[index_] = dist_f / den;

	  } /* 	    if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK) */
	} /* 	if(u_faces_x[index_p] > tol * Delta_y[index_p]) */
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++) */


  /* diff_factor_u_face_x */

  for(curv_ind=0; curv_ind<curves; curv_ind++){

#pragma omp parallel for default(none)					\
  private(I, J, i, index_, index_u, W_is_solid_OK, E_is_solid_OK, some_face_cut_OK) \
  shared(u_faces_x, Delta_y, u_dom_matrix, curv_ind, u_cut_face_x, p_cut_face_x) \
  reduction(+: diff_u_face_x_count)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	i = I - 1;
	index_ = J*Nx + I;
	index_u = J*nx + i;

	if(u_faces_x[index_] > tol * Delta_y[index_]){

	  W_is_solid_OK = (u_dom_matrix[index_u] == curv_ind + 1);
	  E_is_solid_OK = (u_dom_matrix[index_u+1] == curv_ind + 1);
	  some_face_cut_OK = (u_cut_face_x[index_] == curv_ind + 1) || (p_cut_face_x[index_u] == curv_ind + 1) 
	    || (p_cut_face_x[index_u+1] == curv_ind + 1);

	  if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK){
	    diff_u_face_x_count++;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)


  if(diff_u_face_x_count > 0){

    data->diff_u_face_x_index = create_array_int(diff_u_face_x_count, 1, 1, 0);
    data->diff_u_face_x_index_u = create_array_int(diff_u_face_x_count, 1, 1, 0);
    data->diff_u_face_x_I = create_array_int(diff_u_face_x_count, 1, 1, 0);
    data->diff_u_face_x_Delta = create_array(diff_u_face_x_count, 1, 1, 0);
    data->diff_factor_u_face_x = create_array(diff_u_face_x_count, 1, 1, 0);

    data->diff_u_face_x_count = diff_u_face_x_count;

    diff_u_face_x_count = 0;

    int *diff_u_face_x_index = data->diff_u_face_x_index;
    int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
    int *diff_u_face_x_I = data->diff_u_face_x_I;
    double *diff_u_face_x_Delta = data->diff_u_face_x_Delta;
    double *diff_factor_u_face_x = data->diff_factor_u_face_x;

    for(curv_ind=0; curv_ind<curves; curv_ind++){
      for(i=0; i<16; i++){
	pc_temp[i] = poly_coeffs[curv_ind*16 + i];
      }

      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) firstprivate(face_coord, norm_v, S, W_coord, E_coord, x_test) \
  private(I, J, i, j, index_, index_p, index_u, index_v, some_face_cut_OK, delta_h, W_is_solid_OK, E_is_solid_OK, test_is_solid) \
  shared(u_cut_face_x, p_cut_face_x, curv_ind, nodes_x_u, nodes_y_u, pc_temp, \
	 diff_factor_u_face_x, u_faces_x, curve_factor, Delta_y, v_faces_y, Delta_x, p_faces_y, \
	 u_dom_matrix, nodes_x, nodes_y, diff_u_face_x_index, diff_u_face_x_index_u, \
	 diff_u_face_x_Delta, diff_u_face_x_count, diff_u_face_x_I)

      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  i = I - 1;
	  j = J - 1;
	  index_p = J*Nx + I;
	  index_ = J*Nx + I;
	  index_u = J*nx + i;
	  index_v = j*Nx + I;

	  if(u_faces_x[index_] > tol * Delta_y[index_]){

	    W_is_solid_OK = (u_dom_matrix[index_u] == curv_ind + 1);
	    E_is_solid_OK = (u_dom_matrix[index_u+1] == curv_ind + 1);
	    some_face_cut_OK = (u_cut_face_x[index_] == curv_ind + 1) || (p_cut_face_x[index_u] == curv_ind + 1) 
	      || (p_cut_face_x[index_u+1] == curv_ind + 1);

	    if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK){
	      /* Determine if inferior limit of face is located inside the solid */

	      x_test[0] = nodes_x[index_p];
	      x_test[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p];
	    
	      if(fabs(polyn(x_test, pc_temp)) < pol_tol){
		x_test[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];
		test_is_solid = (polyn(x_test, pc_temp) * curve_factor > 0);
	      }
	      else{
		test_is_solid = (polyn(x_test, pc_temp) * curve_factor < 0);	  
	      }

	      if((!W_is_solid_OK) && (!E_is_solid_OK)){
		/* East neighbour */
		E_coord[0] = nodes_x_u[index_u+1];
		E_coord[1] = nodes_y_u[index_u+1];

		/* West neighbour */
		W_coord[0] = nodes_x_u[index_u];
		W_coord[1] = nodes_y_u[index_u];	    
	      }
	      else if (u_cut_face_x[index_] == 0){
		/* Interpolate in horizontal direction to solid surface */
		if(E_is_solid_OK){
		  /* East neighbour: point on solid surface */
		  E_coord[0] = nodes_x[index_p] + v_faces_y[index_p] - 0.5 * Delta_x[index_p];
		  E_coord[1] = nodes_y[index_p];

		  /* West neighbour */
		  W_coord[0] = nodes_x_u[index_u];
		  W_coord[1] = nodes_y_u[index_u];		
		}	    
		else{
		  /* East neighbour */
		  E_coord[0] = nodes_x_u[index_u+1];
		  E_coord[1] = nodes_y_u[index_u+1];
	      
		  /* West neighbour: point on solid surface */
		  W_coord[0] = nodes_x[index_p] - v_faces_y[index_p] + 0.5 * Delta_x[index_p];
		  W_coord[1] = nodes_y[index_p];
		}
	      }
	      else{
		/* Interpolate to solid surface */
		if(E_is_solid_OK){
		  /* West neighbour */
		  W_coord[0] = nodes_x_u[index_u];
		  W_coord[1] = nodes_y_u[index_u];

		  /* East neighbour: point on solid surface */
		  if(test_is_solid){		
		    E_coord[0] = nodes_x[index_p] + p_faces_y[index_v+Nx] - 0.5 * Delta_x[index_p];
		    E_coord[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];
		  }
		  else{
		    E_coord[0] = nodes_x[index_p] + p_faces_y[index_v] - 0.5 * Delta_x[index_p];
		    E_coord[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p];
		  }
		}
		else{
		  /* East neighbour */
		  E_coord[0] = nodes_x_u[index_u+1];
		  E_coord[1] = nodes_y_u[index_u+1];
	      
		  /* West neighbour: point on solid surface */
		  if(test_is_solid){
		    W_coord[0] = nodes_x[index_p] - p_faces_y[index_v+Nx] + 0.5 * Delta_x[index_p];
		    W_coord[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];
		  }
		  else{
		    W_coord[0] = nodes_x[index_p] - p_faces_y[index_v] + 0.5 * Delta_x[index_p];
		    W_coord[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p];
		  }
		}	    
	      }
	    
#ifdef OLDINT_OK
	      E_coord[0] = nodes_x_u[index_u+1];
	      E_coord[1] = nodes_y_u[index_u+1];
	      W_coord[0] = nodes_x_u[index_u];
	      W_coord[1] = nodes_y_u[index_u];
#endif

	      /* Coordinates of w face of u cut cell, "linked coordinate"
		 This is not the face center necessarily, see fig 4 Kirkpatrick */

	      face_coord[0] = nodes_x[index_p];
	      face_coord[1] = W_coord[1] + (face_coord[0] - W_coord[0]) * 
		(E_coord[1] - W_coord[1]) / (E_coord[0] - W_coord[0]);
	      S[0] = E_coord[0] - W_coord[0];
	      S[1] = E_coord[1] - W_coord[1];

	      if(fabs(polyn(face_coord, pc_temp)) > pol_tol){
		// face linked coordinate distance to the curve falls within the fluid
		calc_dist_norm(face_coord, pc_temp, &delta_h, norm_v, cell_size);
		// Displacement vector components (2D only, must expand to 3D!!)
#pragma omp critical
		{
		  diff_u_face_x_index[diff_u_face_x_count] = index_;
		  diff_u_face_x_index_u[diff_u_face_x_count] = index_u;
		  diff_u_face_x_I[diff_u_face_x_count] = I;
		  diff_u_face_x_Delta[diff_u_face_x_count] = S[0];
		  // This factor can be seen from eq 32 of Kirkpatrick
		  diff_factor_u_face_x[diff_u_face_x_count] = u_faces_x[index_] * S[1] * norm_v[1] * curve_factor /
		    (S[0] * delta_h);
		  diff_u_face_x_count++;
		}
	      }
	      else{
		// face_coord located on solid, delta_h->0, diff_factor arbitrarily set to zero
#pragma omp critical
		{
		  diff_u_face_x_index[diff_u_face_x_count] = index_;
		  diff_u_face_x_index_u[diff_u_face_x_count] = index_u;
		  diff_u_face_x_I[diff_u_face_x_count] = I;		  
		  diff_u_face_x_Delta[diff_u_face_x_count] = S[0];
		  diff_factor_u_face_x[diff_u_face_x_count] = 0;
		  diff_u_face_x_count++;
		}
	      }
	    } // (some_face_cut_OK)
	  } // (u_faces_x[index_] > tol * Delta_y[index_])
	}
      }
    }
  } //   if(diff_u_face_x_count > 0)

  /* alpha_u_y just needed for QUICK scheme */

  if(flow_scheme == 1){
    
    for(curv_ind=0; curv_ind<curves; curv_ind++){
      
      for(i=0; i<16; i++){
	pc_temp[i] = poly_coeffs[curv_ind*16 + i];
      }

      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) firstprivate(S_coord, N_coord, x_test, face_center_coord) \
  private(i, j, index_, index_u, index_p, dist_n, dist_s, dist_f, some_face_cut_OK, den) \
  shared(u_cut_face_y, curv_ind, p_cut_face_x, nodes_x_u, nodes_y_u, pc_temp, nodes_x, nodes_y, \
	 Delta_y, curve_factor, u_faces_y, alpha_u_y, Delta_yn_u, Delta_yN_u, Delta_x)

      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){
	
	  index_ = j*nx + i;
	  index_u = j*nx + i;
	  index_p = j*Nx + i;

	  if(u_faces_y[index_] > tol * 0.5 * (Delta_x[index_p] + Delta_x[index_p+1])){

	    some_face_cut_OK = (u_cut_face_y[index_] == curv_ind + 1) || (p_cut_face_x[index_u] == curv_ind + 1) 
	      || (p_cut_face_x[index_u+nx] == curv_ind + 1);
	
	    if(some_face_cut_OK){

	      // South neighbour
	      S_coord[0] = nodes_x_u[index_u];
	      S_coord[1] = nodes_y_u[index_u];
	      dist_s = calc_dist(S_coord, pc_temp, cell_size);

	      // North neighbour
	      N_coord[0] = nodes_x_u[index_u+nx];
	      N_coord[1] = nodes_y_u[index_u+nx];
	      dist_n = calc_dist(N_coord, pc_temp, cell_size);

	      x_test[0] = nodes_x[index_p];
	      x_test[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];

	      face_center_coord[1] = x_test[1];
	  
	      if(fabs(polyn(x_test, pc_temp)) < pol_tol){
		
		x_test[0] = nodes_x[index_p+1];
		
		if(polyn(x_test, pc_temp) * curve_factor > 0){
		  face_center_coord[0] = nodes_x[index_p+1] - 0.5 * u_faces_y[index_];
		}
		else{
		  face_center_coord[0] = nodes_x[index_p] + 0.5 * u_faces_y[index_];
		}	    
	      }
	      else{
		if(polyn(x_test, pc_temp) * curve_factor < 0){
		  face_center_coord[0] = nodes_x[index_p+1] - 0.5 * u_faces_y[index_];
		}
		else{
		  face_center_coord[0] = nodes_x[index_p] + 0.5 * u_faces_y[index_];
		}
	      }

	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_n * Delta_yn_u[index_u] + dist_s * (Delta_yN_u[index_u] - Delta_yn_u[index_u]);
	      
	      alpha_u_y[index_] = (den <= 0) ? 1 : dist_f * Delta_yN_u[index_u] / den;

	    }
	  } // if(u_faces_y[index_] > tol * 0.5 * (Delta_x[index_p] + Delta_x[index_p+1]))
	}// for(i=0; i<nx; i++)
      }// for(j=0; j<ny; j++)
    }// for(curv_ind=0; curv_ind<curves; curv_ind++)
  
  }// if(flow_scheme == 1)

  /* alpha_v_y */

#pragma omp parallel for default(none) private(I, J, j, index_, index_p, index_v) \
  shared(v_faces_y, Delta_x, v_cut_fn, v_cut_fs, nodes_y, nodes_y_v)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      j = J - 1;
      index_ = J*Nx + I;
      index_p = J*Nx + I;
      index_v = j*Nx + I;

      if(v_faces_y[index_p] > tol * Delta_x[index_p]){

	/* Default values */	    
	v_cut_fn[index_] = (nodes_y[index_p] - nodes_y_v[index_v]) / 
	  (nodes_y_v[index_v+Nx] - nodes_y_v[index_v]);
	v_cut_fs[index_] = 1 - v_cut_fn[index_];

      }
      else{
	/* Interior face */
	v_cut_fs[index_] = 0;
	v_cut_fn[index_] = 0;
      }
    }
  }

  for(curv_ind=0; curv_ind<curves; curv_ind++){

    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }

    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none)					\
  firstprivate(x_test, N_coord, S_coord, face_center_coord)		\
  private(i, j, I, J, index_, index_u, index_p, index_v, test_is_solid, dist_n, dist_s, dist_f, fN, fS, \
	  some_face_cut_OK, S_is_solid_OK, N_is_solid_OK, y_S, y_N, den) \
  shared(v_faces_y, Delta_x, Delta_y, v_cut_face_y, p_cut_face_y, nodes_x, nodes_y, pc_temp, curve_factor, \
	 v_dom_matrix, nodes_x_v, nodes_y_v, alpha_v_y, u_faces_x, p_faces_x, curv_ind, v_cut_fs, v_cut_fn)
    
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	j = J - 1;

	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_p = J*Nx + I;
	index_v = j*Nx + I;

	if(v_faces_y[index_p] > tol * Delta_x[index_p]){

	  /* Any of these conditions imply that the interpolated velocity is not 
	     aligned with the velocity nodes used for interpolation */

	  some_face_cut_OK = (v_cut_face_y[index_p] == curv_ind + 1) || (p_cut_face_y[index_v] == curv_ind + 1) 
	    || (p_cut_face_y[index_v+Nx] == curv_ind + 1);
	  S_is_solid_OK = (v_dom_matrix[index_v] == curv_ind + 1);
	  N_is_solid_OK = (v_dom_matrix[index_v+Nx] == curv_ind + 1);

      
	  if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK){
	    /* Need to calculate an alpha_value */

	    /* Determine if left limit of face is located inside the solid */

	    x_test[0] = nodes_x[index_p] - 0.5 * Delta_x[index_p];
	    x_test[1] = nodes_y[index_p];
	    
	    if(fabs(polyn(x_test, pc_temp)) < pol_tol){
	      x_test[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
	      test_is_solid = (polyn(x_test, pc_temp) * curve_factor > 0);		
	    }
	    else{
	      test_is_solid = (polyn(x_test, pc_temp) * curve_factor < 0);	  
	    }

	    if((!S_is_solid_OK) && (!N_is_solid_OK)){

	      /* North neighbour */
	      N_coord[0] = nodes_x_v[index_v+Nx];
	      N_coord[1] = nodes_y_v[index_v+Nx];

	      /* South neighbour */
	      S_coord[0] = nodes_x_v[index_v];
	      S_coord[1] = nodes_y_v[index_v];

	      /* This should be 1/2 so it does not matter */
	      v_cut_fn[index_] = (nodes_y[index_p] - nodes_y_v[index_v]) / 
		(nodes_y_v[index_v+Nx] - nodes_y_v[index_v]);
	      v_cut_fs[index_] = 1 - v_cut_fn[index_];
	    
	    }
	    else if (v_cut_face_y[index_] == 0){
	      /* Interpolate in vertical direction to solid surface */
	      if(N_is_solid_OK){

		y_N = nodes_y[index_p] - 0.5 * Delta_y[index_p] + u_faces_x[index_p];

		/* North neighbour: point on solid surface */
		N_coord[0] = nodes_x[index_p];
		N_coord[1] = y_N;

		/* South neighbour */
		S_coord[0] = nodes_x_v[index_v];
		S_coord[1] = nodes_y_v[index_v];

		v_cut_fs[index_] = 1 - (nodes_y[index_p] - nodes_y_v[index_v]) /
		  (y_N - nodes_y_v[index_v]);
		v_cut_fn[index_] = 0;
		
	      }	    
	      else{

		y_S = nodes_y[index_p] + 0.5 * Delta_y[index_p] - u_faces_x[index_p];

		/* North neighbour */
		N_coord[0] = nodes_x_v[index_v+Nx];
		N_coord[1] = nodes_y_v[index_v+Nx];
	      
		/* South neighbour: point on solid surface */
		S_coord[0] = nodes_x[index_p];
		S_coord[1] = y_S;

		v_cut_fs[index_] = 0;
		v_cut_fn[index_] = (nodes_y[index_p] - y_S) /
		  (nodes_y_v[index_v+Nx] - y_S);
	      }
	    }
	    else{
	      /* Interpolate to solid surface */
	      if(N_is_solid_OK){

		v_cut_fn[index_] = 0;

		/* South neighbour */
		S_coord[0] = nodes_x_v[index_v];
		S_coord[1] = nodes_y_v[index_v];

		/* North neighbour: point on solid surface */
		if(test_is_solid){		
		  y_N = nodes_y[index_p] - 0.5 * Delta_y[index_p] + p_faces_x[index_u+1];
		  N_coord[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
		  N_coord[1] = y_N;
		}
		else{
		  y_N = nodes_y[index_p] - 0.5 * Delta_y[index_p] + p_faces_x[index_u];
		  N_coord[0] = nodes_x[index_p] - 0.5 * Delta_x[index_p];
		  N_coord[1] = y_N;
		}

		v_cut_fs[index_] = 1 - (nodes_y[index_p] - nodes_y_v[index_v]) /
		  (y_N - nodes_y_v[index_v]);

	      }
	      else{

		v_cut_fs[index_] = 0;

		/* North neighbour */
		N_coord[0] = nodes_x_v[index_v+Nx];
		N_coord[1] = nodes_y_v[index_v+Nx];
	      
		/* South neighbour: point on solid surface */
		if(test_is_solid){
		  y_S = nodes_y[index_p] + 0.5 * Delta_y[index_p] - p_faces_x[index_u+1];
		  S_coord[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
		  S_coord[1] = y_S;
		}
		else{
		  y_S = nodes_y[index_p] + 0.5 * Delta_y[index_p] - p_faces_x[index_u];
		  S_coord[0] = nodes_x[index_p] - 0.5 * Delta_x[index_p];
		  S_coord[1] = y_S;
		}
		
		v_cut_fn[index_] = (nodes_y[index_p] - y_S) /
		  (nodes_y_v[index_v+Nx] - y_S);
	      }
	    }

#ifdef OLDINT_OK
	    /* North neighbour */
	    N_coord[0] = nodes_x_v[index_v+Nx];
	    N_coord[1] = nodes_y_v[index_v+Nx];

	    /* South neighbour */
	    S_coord[0] = nodes_x_v[index_v];
	    S_coord[1] = nodes_y_v[index_v];

	    v_cut_fn[index_] = (nodes_y[index_p] - nodes_y_v[index_v]) / 
	      (nodes_y_v[index_v+Nx] - nodes_y_v[index_v]);
	    v_cut_fs[index_] = 1 - v_cut_fn[index_];
#endif

	    /* Common for all three cases */

	    // Distance from north neighbour to the curve
	    dist_n = calc_dist(N_coord, pc_temp, cell_size);
	    // Distance from south neighbour to the curve
	    dist_s = calc_dist(S_coord, pc_temp, cell_size);

	    /* This is supposed to be the node in between the u_P and u_E nodes */

	    face_center_coord[1] = nodes_y[index_p];

	    if(test_is_solid){
	      // x_test falls within the solid body
	      face_center_coord[0] = nodes_x[index_p] + 0.5 * (Delta_y[index_p] - v_faces_y[index_p]);
	    }
	    else{
	      // x_test is in the fluid region
	      face_center_coord[0] = nodes_x[index_p] + 0.5 * (-Delta_y[index_p] + v_faces_y[index_p]);
	    }
	  
	    dist_f = calc_dist(face_center_coord, pc_temp, cell_size);
	  
	    fN = (face_center_coord[1] - S_coord[1]) / (N_coord[1] - S_coord[1]);
	    fS = 1 - fN;

	    /* Build alpha based on distances computed, see eqs 18 to 21 Kirkpatrick
	       This affects convective terms only */
	    den = dist_n * fN + dist_s * fS;
	    
	    alpha_v_y[index_] = (den <= 0) ? 1 : dist_f / den;

	  } /* 	if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK) */
	} /* 	if(v_faces_y[index_p] > tol * Delta_x[index_p]) */
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++) */


  /* diff_factor_v_face_y */

  for(curv_ind=0; curv_ind<curves; curv_ind++){

#pragma omp parallel for default(none)					\
  private(I, J, j, index_, index_v, S_is_solid_OK, N_is_solid_OK, some_face_cut_OK) \
  shared(v_faces_y, Delta_x, v_dom_matrix, curv_ind, v_cut_face_y, p_cut_face_y) \
  reduction(+: diff_v_face_y_count)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	j = J - 1;
	index_ = J*Nx + I;
	index_v = j*Nx + I;

	if(v_faces_y[index_] > tol * Delta_x[index_]){

	  S_is_solid_OK = (v_dom_matrix[index_v] == curv_ind + 1);
	  N_is_solid_OK = (v_dom_matrix[index_v+Nx] == curv_ind + 1);
	  some_face_cut_OK = (v_cut_face_y[index_] == curv_ind + 1) || (p_cut_face_y[index_v] == curv_ind + 1) 
	    || (p_cut_face_y[index_v+Nx] == curv_ind + 1);

	  if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK){
	    diff_v_face_y_count++;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)


  if(diff_v_face_y_count > 0){
      
    data->diff_v_face_y_index = create_array_int(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_index_v = create_array_int(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_J = create_array_int(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_Delta = create_array(diff_v_face_y_count, 1, 1, 0);
    data->diff_factor_v_face_y = create_array(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_count = diff_v_face_y_count;

    diff_v_face_y_count = 0;

    int *diff_v_face_y_index = data->diff_v_face_y_index;
    int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
    int *diff_v_face_y_J = data->diff_v_face_y_J;
    double *diff_v_face_y_Delta = data->diff_v_face_y_Delta;
    double *diff_factor_v_face_y = data->diff_factor_v_face_y;
    

    for(curv_ind=0; curv_ind<curves; curv_ind++){
      for(i=0; i<16; i++){
	pc_temp[i] = poly_coeffs[curv_ind*16 + i];
      }

      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) firstprivate(face_coord, S, norm_v, N_coord, S_coord, x_test) \
  private(I, J, i, j, index_, index_p, index_u, index_v, some_face_cut_OK, delta_h, test_is_solid, N_is_solid_OK, S_is_solid_OK) \
  shared(v_cut_face_y, p_cut_face_y, curv_ind, nodes_x_v, nodes_y_v, pc_temp, curve_factor, \
	 v_faces_y, diff_factor_v_face_y, Delta_x, nodes_x, nodes_y, Delta_y, u_faces_x, p_faces_x, v_dom_matrix, \
	 diff_v_face_y_index, diff_v_face_y_index_v, diff_v_face_y_Delta, diff_v_face_y_count, diff_v_face_y_J)

      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  j = J - 1;
	  i = I - 1;
	  index_ = J*Nx + I;
	  index_p = J*Nx + I;
	  index_u = J*nx + i;
	  index_v = j*Nx + I;

	  if(v_faces_y[index_] > tol * Delta_x[index_]){

	    S_is_solid_OK = (v_dom_matrix[index_v] == curv_ind + 1);
	    N_is_solid_OK = (v_dom_matrix[index_v+Nx] == curv_ind + 1);
	    some_face_cut_OK = (v_cut_face_y[index_] == curv_ind + 1) || (p_cut_face_y[index_v] == curv_ind + 1) 
	      || (p_cut_face_y[index_v+Nx] == curv_ind + 1);

	    if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK){

	      x_test[0] = nodes_x[index_p] - 0.5 * Delta_x[index_p];
	      x_test[1] = nodes_y[index_p];
	    
	      if(fabs(polyn(x_test, pc_temp)) < pol_tol){
		x_test[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
		test_is_solid = (polyn(x_test, pc_temp) * curve_factor > 0);		
	      }
	      else{
		test_is_solid = (polyn(x_test, pc_temp) * curve_factor < 0);	  
	      }

	      if((!S_is_solid_OK) && (!N_is_solid_OK)){
		/* North neighbour */
		N_coord[0] = nodes_x_v[index_v+Nx];
		N_coord[1] = nodes_y_v[index_v+Nx];

		/* South neighbour */
		S_coord[0] = nodes_x_v[index_v];
		S_coord[1] = nodes_y_v[index_v];
	      }
	      else if (v_cut_face_y[index_] == 0){
		/* Interpolate in vertical direction to solid surface */
		if(N_is_solid_OK){
		  /* North neighbour: point on solid surface */
		  N_coord[0] = nodes_x[index_p];
		  N_coord[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p] + u_faces_x[index_p];

		  /* South neighbour */
		  S_coord[0] = nodes_x_v[index_v];
		  S_coord[1] = nodes_y_v[index_v];		
		}	    
		else{
		  /* North neighbour */
		  N_coord[0] = nodes_x_v[index_v+Nx];
		  N_coord[1] = nodes_y_v[index_v+Nx];
	      
		  /* South neighbour: point on solid surface */
		  S_coord[0] = nodes_x[index_p];
		  S_coord[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p] - u_faces_x[index_p];
		}
	      }
	      else{
		/* Interpolate to solid surface */
		if(N_is_solid_OK){
		  /* South neighbour */
		  S_coord[0] = nodes_x_v[index_v];
		  S_coord[1] = nodes_y_v[index_v];

		  /* North neighbour: point on solid surface */
		  if(test_is_solid){
		    N_coord[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
		    N_coord[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p] + p_faces_x[index_u+1];
		  }
		  else{
		    N_coord[0] = nodes_x[index_p] - 0.5 * Delta_x[index_p];
		    N_coord[1] = nodes_y[index_p] - 0.5 * Delta_y[index_p] + p_faces_x[index_u];
		  }
		}
		else{
		  /* North neighbour */
		  N_coord[0] = nodes_x_v[index_v+Nx];
		  N_coord[1] = nodes_y_v[index_v+Nx];
	      
		  /* South neighbour: point on solid surface */
		  if(test_is_solid){
		    S_coord[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
		    S_coord[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p] - p_faces_x[index_u+1];
		  }
		  else{
		    S_coord[0] = nodes_x[index_p] - 0.5 * Delta_x[index_p];
		    S_coord[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p] - p_faces_x[index_u];
		  }
		}	    
	      }

#ifdef OLDINT_OK
	      N_coord[0] = nodes_x_v[index_v+Nx];
	      N_coord[1] = nodes_y_v[index_v+Nx];
	      S_coord[0] = nodes_x_v[index_v];
	      S_coord[1] = nodes_y_v[index_v];
#endif

	      face_coord[1] = nodes_y[index_p];
	      face_coord[0] = S_coord[0] + (face_coord[1] - S_coord[1]) * (N_coord[0] - S_coord[0]) /
		(N_coord[1] - S_coord[1]);
	      S[0] = N_coord[0] - S_coord[0];
	      S[1] = N_coord[1] - S_coord[1];

	      if(fabs(polyn(face_coord, pc_temp)) > pol_tol){
		calc_dist_norm(face_coord, pc_temp, &delta_h, norm_v, cell_size);
#pragma omp critical
		{
		  diff_v_face_y_index[diff_v_face_y_count] = index_;
		  diff_v_face_y_index_v[diff_v_face_y_count] = index_v;
		  diff_v_face_y_J[diff_v_face_y_count] = J;
		  diff_v_face_y_Delta[diff_v_face_y_count] = S[1];
		  diff_factor_v_face_y[diff_v_face_y_count] = v_faces_y[index_] * S[0] * norm_v[0] * curve_factor /
		    (S[1] * delta_h);
		  diff_v_face_y_count++;
		}
	      }
	      else{
#pragma omp critical
		{		
		  diff_v_face_y_index[diff_v_face_y_count] = index_;
		  diff_v_face_y_index_v[diff_v_face_y_count] = index_v;
		  diff_v_face_y_J[diff_v_face_y_count] = J;
		  diff_v_face_y_Delta[diff_v_face_y_count] = S[1];
		  diff_factor_v_face_y[diff_v_face_y_count] = 0;
		  diff_v_face_y_count++;		  
		}
	      }
	    } // (some_face_cut_OK)
	  } // if(v_faces_y[index_] > tol * Delta_x[index_])
	}
      }
    }
  }

  /* alpha_v_x just needed for QUICK scheme */

  if(flow_scheme == 1){

    for(curv_ind=0; curv_ind<curves; curv_ind++){

      for(i=0; i<16; i++){
	pc_temp[i] = poly_coeffs[curv_ind*16 + i];
      }
      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none)					\
  firstprivate(W_coord, E_coord, x_test, face_center_coord)		\
  private(i, j, index_, index_v, index_p, some_face_cut_OK, dist_w, dist_e, dist_f, den) \
  shared(v_cut_face_x, p_cut_face_y, nodes_x_v, nodes_y_v, nodes_x, nodes_y, v_faces_x, \
	 alpha_v_x, Delta_xE_v, Delta_xe_v, curv_ind, pc_temp, Delta_x, curve_factor, Delta_y)

      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){
	
	  index_ = j*nx + i;
	  index_v = j*Nx + i;
	  index_p = j*Nx + i;

	  if(v_faces_x[index_] > tol * 0.5 * (Delta_y[index_p] + Delta_y[index_p+Nx])){

	    some_face_cut_OK = (v_cut_face_x[index_] == curv_ind + 1) || (p_cut_face_y[index_v] == curv_ind + 1) 
	      || (p_cut_face_y[index_v+1] == curv_ind + 1);

	    if(some_face_cut_OK){
	      // West neighbour
	      W_coord[0] = nodes_x_v[index_v];
	      W_coord[1] = nodes_y_v[index_v];
	      dist_w = calc_dist(W_coord, pc_temp, cell_size);

	      // East neighbour

	      E_coord[0] = nodes_x_v[index_v+1];
	      E_coord[1] = nodes_y_v[index_v+1];
	      dist_e = calc_dist(E_coord, pc_temp, cell_size);

	      x_test[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
	      x_test[1] = nodes_y[index_p];
	      face_center_coord[0] = x_test[0];

	      if(fabs(polyn(x_test, pc_temp)) < pol_tol){
		x_test[1] = nodes_y[index_p+Nx];
		if(polyn(x_test, pc_temp) * curve_factor > 0){
		  face_center_coord[1] = nodes_y[index_p+Nx] - 0.5 * v_faces_x[index_];
		}
		else{
		  face_center_coord[1] = nodes_y[index_p] + 0.5 * v_faces_x[index_];
		}	    
	      }
	      else{
		if(polyn(x_test, pc_temp) * curve_factor < 0){
		  face_center_coord[1] = nodes_y[index_p+Nx] - 0.5 * v_faces_x[index_];
		}
		else{
		  face_center_coord[1] = nodes_y[index_p] + 0.5 * v_faces_x[index_];
		}
	      }
	  
	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_e * Delta_xe_v[index_v] + dist_w * (Delta_xE_v[index_v] - Delta_xe_v[index_v]);
	      
	      alpha_v_x[index_] = (den <= 0) ? 1 : dist_f * Delta_xE_v[index_v] / den;
	
	    }
	  }
	}// for(i=0; i<nx; i++)
      }// for(j=0; j<ny; j++)
    }// for(curv_ind=0; curv_ind<curves; curv_ind++)
  }// if(flow_scheme == 1)

  return;
}

/* COMPUTE_SOLID_FACTORS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, Data_Mem::u_sol_factor_count) Data_Mem::u_sol_factor: Factor due to stress on solid
  boundary on u cells.

  Array (double, Data_Mem::u_sol_factor_count) Data_Mem::u_p_factor: Factor due to pressure on solid
  boundary on u cells.

  Array (double, Data_Mem::v_sol_factor_count) Data_Mem::v_sol_factor: Factor due to stress on solid
  boundary on v cells.

  Array (double, Data_Mem::v_sol_factor_count) Data_Mem::v_p_factor: Factor due to pressure on solid
  boundary on v cells.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 19-02-2019
*/
int compute_solid_factors(Data_Mem *data){

  int i, j, I, J;
  int index_, index_u, index_v;
  int curv_ind;
  int flag_int0, flag_int1;

  int u_sol_factor_count = 0;
  int v_sol_factor_count = 0;
  int ret_value = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int curves = data->curves;

  const int *u_cut_matrix = data->u_cut_matrix;
  const int *v_cut_matrix = data->v_cut_matrix;
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double curve_factor, delta_h;
  double f0, f1;
  
  double pc_temp[16];
  double x0[3] = {0};
  double x1[3] = {0};
  double norm_v[3] = {0};
  double x_int0[3] = {0};
  double x_int1[3] = {0};
  double x_test[3] = {0};
  
  const double cell_size = data->cell_size;
  const double solid_factor_tol = 1e10;

  const double *poly_coeffs = data->poly_coeffs;
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

  /** Factors corresponding to the presence of the solid boundary and associated forces
      calculated at velociy cells cut as indicated on u_cut_matrix, v_cut_matrix, w_cut_matrix. */

  if(curves > 0){

#pragma omp parallel for default(none) private(i, J, index_u) shared(u_cut_matrix) \
  reduction(+: u_sol_factor_count)
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){
	index_u = J*nx + i;
	if(u_cut_matrix[index_u]){
	  u_sol_factor_count++;
	}
      }
    }
  }
  
  if(u_sol_factor_count > 0){

    data->u_sol_factor = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_p_factor = create_array(u_sol_factor_count, 1, 1, 0);    
    data->u_sol_factor_index_u = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_index = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_i = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_J = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_K = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_factor_delta_h = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_area = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_inters_x = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_inters_y = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_inters_z = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_p_factor_W = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_count = u_sol_factor_count;

    u_sol_factor_count = 0;

    double *u_sol_factor = data->u_sol_factor;
    double *u_p_factor = data->u_p_factor;
    double *u_factor_delta_h = data->u_factor_delta_h;
    double *u_factor_area = data->u_factor_area;
    double *u_factor_inters_x = data->u_factor_inters_x;
    double *u_factor_inters_y = data->u_factor_inters_y;
    double *u_factor_inters_z = data->u_factor_inters_z;
    double *u_p_factor_W = data->u_p_factor_W;
    int *u_sol_factor_index_u = data->u_sol_factor_index_u;
    int *u_sol_factor_index = data->u_sol_factor_index;
    int *u_sol_factor_i = data->u_sol_factor_i;
    int *u_sol_factor_J = data->u_sol_factor_J;
    int *u_sol_factor_K = data->u_sol_factor_K;

    for(curv_ind=0; curv_ind<curves; curv_ind++){
      for(i=0; i<16; i++){
	pc_temp[i] = poly_coeffs[curv_ind*16 + i];
      }

      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

      // u
#pragma omp parallel for default(none) firstprivate(x0, x1, norm_v, x_test, x_int0, x_int1) \
  private(i, I, J, index_, index_u, flag_int0, flag_int1, f0, f1, delta_h) \
  shared(pc_temp, curv_ind, u_cut_matrix, nodes_x, nodes_y, Delta_y, curve_factor, \
	 nodes_x_u, nodes_y_u, u_sol_factor, u_p_factor, u_sol_factor_index_u, u_sol_factor_index, u_sol_factor_i, \
	 u_sol_factor_J, u_sol_factor_K, u_sol_factor_count, u_factor_delta_h, u_factor_area, \
	 u_factor_inters_x, u_factor_inters_y, u_factor_inters_z, u_p_factor_W)

      for(J=1; J<Ny-1; J++){      
	for(i=1; i<nx-1; i++){

	  I = i+1;

	  index_ = J*Nx + I;
	  index_u = J*nx + i;

	  /* Flag true for x_int found */
	  flag_int0 = 0;
	  flag_int1 = 0;

	  if(u_cut_matrix[index_u] == curv_ind + 1){
	    // Within a u cut cell

	    // West side
	    x0[0] = nodes_x[index_-1];
	    x0[1] = nodes_y[index_-1] - 0.5 * Delta_y[index_-1];
	    x1[0] = x0[0];
	    x1[1] = nodes_y[index_-1] + 0.5 * Delta_y[index_-1];

	    f0 = polyn(x0, pc_temp) * curve_factor;
	    f1 = polyn(x1, pc_temp) * curve_factor;

	    if(f0*f1 < 0){
	      // u cell is cut
	      flag_int0 = 1;
	      norm_v[0] = 0;

	      if(f0 < 0){
		// x0 is within the solid
		norm_v[1] = 1 * curve_factor;
		// Search for the intersection between x1 and the curve
		// Point on the curve would have x_int0 coordinates
		int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		x_int0[1] = MIN(x1[1], MAX(x0[1], x_int0[1]));
	      }
	      else{
		// x1 is within the solid
		norm_v[1] = -1 * curve_factor;
		// Search for the intersection between x0 and the curve
		// Point on the curve would have x_int0 coordinates
		int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		x_int0[1] = MIN(x1[1], MAX(x0[1], x_int0[1]));
	      }
	    }

	    // North side
	    // Swing over the x0 coordinate to analyze the N face of the u cut cell
	    x0[0] = nodes_x[index_];
	    x0[1] = x1[1];

	    f0 = polyn(x0, pc_temp) * curve_factor;

	    if(f0*f1 < 0){
	      // N face is cut
	      norm_v[1] = 0;
	      if(f0 < 0){
		// x0 within solid
		norm_v[0] = -1 * curve_factor;
		if (flag_int0){
		  // This is executed if W face is cut
		  // setting the value of x_int1
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x0[0], MAX(x1[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  // Otherwise, the value of x_int0 is overwritten
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x0[0], MAX(x1[0], x_int0[0]));
		  // and the flag is set
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[0] = 1 * curve_factor;
		if (flag_int0){
		  // This is executed in case of the W face is not cut
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x0[0], MAX(x1[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x0[0], MAX(x1[0], x_int0[0]));
		  flag_int0 = 1;
		}
	      }
	    }

	    // East side
	    x1[0] = x0[0];
	    x1[1] = nodes_y[index_] - 0.5 * Delta_y[index_];

	    f1 = polyn(x1, pc_temp) * curve_factor;

	    if(f0*f1 < 0){
	      norm_v[0] = 0;
	      if(f0 < 0){
		norm_v[1] = -1 * curve_factor;
		if(flag_int0){
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  x_int1[1] = MIN(x0[1], MAX(x1[1], x_int1[1]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  x_int0[1] = MIN(x0[1], MAX(x1[1], x_int0[1]));
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[1] = 1 * curve_factor;
		if(flag_int0){
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  x_int1[1] = MIN(x0[1], MAX(x1[1], x_int1[1]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  x_int0[1] = MIN(x0[1], MAX(x1[1], x_int0[1]));
		  flag_int0 = 1;
		}
	      }
	    }

	    // South side
	    x0[0] = nodes_x[index_-1];
	    x0[1] = x1[1];

	    f0 = polyn(x0, pc_temp) * curve_factor;

	    if(f0*f1 < 0){
	      norm_v[1] = 0;
	      if(f0 < 0){
		norm_v[0] = 1 * curve_factor;
		if(flag_int0){
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x1[0], MAX(x0[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x1[0], MAX(x0[0], x_int0[0]));
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[0] = -1 * curve_factor;
		if(flag_int0){
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x1[0], MAX(x0[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x1[0], MAX(x0[0], x_int0[0]));
		  flag_int0 = 1;
		}
	      }
	    }

	    // u cell coordinates
	    x0[0] = nodes_x_u[index_u];
	    x0[1] = nodes_y_u[index_u];
	    // Distance from u node to the curve
	    delta_h = calc_dist(x0, pc_temp, cell_size);

	    /* Due to tolerance used, for some cut_cell an intersection may not be found */
	    if(flag_int0 && flag_int1){
	      avg_vec(x_test, x_int0, x_int1);
	      normal(norm_v, x_test, pc_temp);
	    }

#pragma omp critical
	    {
	      if(flag_int0 && flag_int1){
		// See eq 35 Kirkpatrick, this term corresponds to solid area/solid distance
		u_sol_factor[u_sol_factor_count] = MIN(vec_dist(x_int0, x_int1) / delta_h,
						       solid_factor_tol);

		// This is for the perpendicular component of the force, attributed to the pressure
		u_p_factor[u_sol_factor_count] = norm_v[0] * vec_dist(x_int0, x_int1) * curve_factor;
		u_factor_area[u_sol_factor_count] = vec_dist(x_int0, x_int1);
		u_factor_inters_x[u_sol_factor_count] = x_test[0];
		u_factor_inters_y[u_sol_factor_count] = x_test[1];
		u_factor_inters_z[u_sol_factor_count] = x_test[2];
		u_p_factor_W[u_sol_factor_count] = 1 - (0.5 * (x_int0[0] + x_int1[0]) - nodes_x[index_-1]) / 
		  (nodes_x[index_] - nodes_x[index_-1]);
	      }
	      else{
		/* No intersection was found: Default values */
		u_sol_factor[u_sol_factor_count] = 0;
		u_p_factor[u_sol_factor_count] = 0;
		u_factor_area[u_sol_factor_count] = 0;
		u_factor_inters_x[u_sol_factor_count] = NAN;
		u_factor_inters_y[u_sol_factor_count] = NAN;
		u_factor_inters_z[u_sol_factor_count] = NAN;
		u_p_factor_W[u_sol_factor_count] = 0.5;
	      }
	      u_factor_delta_h[u_sol_factor_count] = delta_h;
	      u_sol_factor_i[u_sol_factor_count] = i;
	      u_sol_factor_J[u_sol_factor_count] = J;
	      u_sol_factor_K[u_sol_factor_count] = 0;
	      u_sol_factor_index_u[u_sol_factor_count] = index_u;
	      u_sol_factor_index[u_sol_factor_count] = index_;
	      u_sol_factor_count++;	      
	    }
	  }
	}// for(i=1; i<nx-1; i++)
      }// for(J=1; J<Ny-1; J++)
    }// for(curv_ind=0; curv_ind<curves; curv_ind++)
  }// if(u_sol_factor_count > 0)


  if(curves > 0){

#pragma omp parallel for default(none) private(i, I, index_v) shared(v_cut_matrix) \
  reduction(+: v_sol_factor_count)
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	index_v = j*Nx + I;
	if(v_cut_matrix[index_v]){
	  v_sol_factor_count++;
	}
      }
    }
  }
  
  if(v_sol_factor_count > 0){  

    data->v_sol_factor = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_p_factor = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_index_v = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_index = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_I = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_j = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_K = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_factor_delta_h = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_area = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_inters_x = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_inters_y = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_inters_z = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_p_factor_S = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_count = v_sol_factor_count;
    v_sol_factor_count = 0;

    double *v_sol_factor = data->v_sol_factor;
    double *v_p_factor = data->v_p_factor;
    double *v_factor_delta_h = data->v_factor_delta_h;
    double *v_factor_area = data->v_factor_area;
    double *v_factor_inters_x = data->v_factor_inters_x;
    double *v_factor_inters_y = data->v_factor_inters_y;
    double *v_factor_inters_z = data->v_factor_inters_z;
    double *v_p_factor_S = data->v_p_factor_S;
    int *v_sol_factor_index_v = data->v_sol_factor_index_v;
    int *v_sol_factor_index = data->v_sol_factor_index;
    int *v_sol_factor_I = data->v_sol_factor_I;
    int *v_sol_factor_j = data->v_sol_factor_j;
    int *v_sol_factor_K = data->v_sol_factor_K;

    for(curv_ind=0; curv_ind<curves; curv_ind++){
      for(i=0; i<16; i++){
	pc_temp[i] = poly_coeffs[curv_ind*16 + i];
      }
      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

      // v
#pragma omp parallel for default(none) firstprivate(x0, x1, norm_v, x_int0, x_int1, x_test) \
  private(I, j, J, index_, index_v, flag_int0, flag_int1, f0, f1, delta_h) \
  shared(curv_ind, curve_factor, v_cut_matrix, nodes_x, nodes_y, Delta_x, pc_temp, \
	 nodes_x_v, nodes_y_v, v_sol_factor, v_p_factor, v_sol_factor_index_v, v_sol_factor_index, v_sol_factor_I, \
	 v_sol_factor_j, v_sol_factor_K, v_sol_factor_count, v_factor_delta_h, v_factor_area, \
	 v_factor_inters_x, v_factor_inters_y, v_factor_inters_z, v_p_factor_S)

      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){

	  J = j+1;

	  index_ = J*Nx + I;
	  index_v = j*Nx + I;

	  /* Flag true for x_int found */
	  flag_int0 = 0;
	  flag_int1 = 0;

	  if (v_cut_matrix[index_v] == curv_ind + 1){

	    // West side
	    x0[0] = nodes_x[index_-Nx] - 0.5 * Delta_x[index_-Nx];
	    x0[1] = nodes_y[index_-Nx];
	    x1[0] = x0[0];
	    x1[1] = nodes_y[index_];
	  
	    f0 = polyn(x0, pc_temp) * curve_factor;
	    f1 = polyn(x1, pc_temp) * curve_factor;
	  
	    if(f0*f1 < 0){
	      flag_int0 = 1;
	      norm_v[0] = 0;
	      if(f0 < 0){
		norm_v[1] = 1 * curve_factor;
		int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		x_int0[1] = MIN(x1[1], MAX(x0[1], x_int0[1]));
	      }
	      else{
		norm_v[1] = -1 * curve_factor;
		int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		x_int0[1] = MIN(x1[1], MAX(x0[1], x_int0[1]));
	      }
	    }
	    // North side
	    x0[0] = nodes_x[index_-Nx] + 0.5 * Delta_x[index_-Nx];
	    x0[1] = x1[1];
	  
	    f0 = polyn(x0, pc_temp) * curve_factor;
	    f1 = polyn(x1, pc_temp) * curve_factor;
	  
	    if(f0*f1 < 0){
	      norm_v[1] = 0;
	      if(f0 < 0){
		norm_v[0] = -1 * curve_factor;	    
		if(flag_int0){
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x0[0], MAX(x1[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x0[0], MAX(x1[0], x_int0[0]));
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[0] = 1 * curve_factor;
		if(flag_int0){
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x0[0], MAX(x1[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x0[0], MAX(x1[0], x_int0[0]));
		  flag_int0 = 1;
		}
	      }
	    }
	    // East side
	    x1[0] = x0[0];
	    x1[1] = nodes_y[index_-Nx];
	  
	    f0 = polyn(x0, pc_temp) * curve_factor;
	    f1 = polyn(x1, pc_temp) * curve_factor;
	  
	    if(f0*f1 < 0){
	      norm_v[0] = 0;
	      if(f0 < 0){
		norm_v[1] = -1 * curve_factor;
		if(flag_int0){
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  x_int1[1] = MIN(x0[1], MAX(x1[1], x_int1[1]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  x_int0[1] = MIN(x0[1], MAX(x1[1], x_int0[1]));
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[1] = 1 * curve_factor;
		if(flag_int0){
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  x_int1[1] = MIN(x0[1], MAX(x1[1], x_int1[1]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  x_int0[1] = MIN(x0[1], MAX(x1[1], x_int0[1]));
		  flag_int0 = 1;
		}
	      }
	    }
	    // South side
	    x0[0] = nodes_x[index_-Nx] - 0.5 * Delta_x[index_-Nx];
	    x0[1] = x1[1];
	  
	    f0 = polyn(x0, pc_temp) * curve_factor;
	    f1 = polyn(x1, pc_temp) * curve_factor;
	  
	    if(f0*f1 < 0){
	      norm_v[1] = 0;
	      if (f0 < 0){
		norm_v[0] = 1 * curve_factor;
		if (flag_int0){
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x1[0], MAX(x0[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x1[0], MAX(x0[0], x_int0[0]));
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[0] = -1 * curve_factor;
		if (flag_int0){		      
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  x_int1[0] = MIN(x1[0], MAX(x0[0], x_int1[0]));
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  x_int0[0] = MIN(x1[0], MAX(x0[0], x_int0[0]));
		  flag_int0 = 1;
		}
	      }
	    }

	    x0[0] = nodes_x_v[index_v];
	    x0[1] = nodes_y_v[index_v];
	    delta_h = calc_dist(x0, pc_temp, cell_size);
	    if(flag_int0 && flag_int1){
	      avg_vec(x_test, x_int0, x_int1);
	      normal(norm_v, x_test, pc_temp);
	    }

#pragma omp critical
	    {         
	      if(flag_int0 && flag_int1){
		v_sol_factor[v_sol_factor_count] = MIN(vec_dist(x_int0, x_int1) / delta_h,
						       solid_factor_tol);
		v_p_factor[v_sol_factor_count] = norm_v[1] * vec_dist(x_int0, x_int1) * curve_factor;
		v_factor_area[v_sol_factor_count] = vec_dist(x_int0, x_int1);
		v_factor_inters_x[v_sol_factor_count] = x_test[0];
		v_factor_inters_y[v_sol_factor_count] = x_test[1];
		v_factor_inters_z[v_sol_factor_count] = x_test[2];
		v_p_factor_S[v_sol_factor_count] = 1 - (0.5 * (x_int0[1] + x_int1[1]) - nodes_y[index_-Nx]) /
		  (nodes_y[index_] - nodes_y[index_-Nx]);
	      }
	      else{
		v_sol_factor[v_sol_factor_count] = 0;
		v_p_factor[v_sol_factor_count] = 0;
		v_factor_area[v_sol_factor_count] = 0;
		v_factor_inters_x[v_sol_factor_count] = NAN;
		v_factor_inters_y[v_sol_factor_count] = NAN;
		v_factor_inters_z[v_sol_factor_count] = NAN;
		v_p_factor_S[v_sol_factor_count] = 0.5;
	      }
	      v_factor_delta_h[v_sol_factor_count] = delta_h;
	      v_sol_factor_I[v_sol_factor_count] = I;
	      v_sol_factor_j[v_sol_factor_count] = j;
	      v_sol_factor_K[v_sol_factor_count] = 0;
	      v_sol_factor_index_v[v_sol_factor_count] = index_v;
	      v_sol_factor_index[v_sol_factor_count] = index_;
	      v_sol_factor_count++;
	    }
	  }
	}// for(I=1; I<Nx-1; I++)
      }// for(j=1; j<ny-1; j++)
    }// for(curv_ind=0; curv_ind<curves; curv_ind++)
  }// if(v_sol_factor_count > 0)


  return ret_value;
}

/* SET_P_NODES_IN_SOLID_TO_ZERO */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) p: set p values in solid to zero. Needed if p_dom_matrix
  from file is different from current one.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 08-08-2019
*/
void set_p_nodes_in_solid_to_zero(Data_Mem *data){

  int index_;
  int I, J, K;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *p_dom_matrix =  data->p_dom_matrix;

  double *p = data->p;

#pragma omp parallel for default(none) private(I, J, K, index_) shared(p_dom_matrix, p)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	if(p_dom_matrix[index_] != 0)
	  p[index_] = 0;
      }
    }
  }

  return;
}

/* COMPUTE_FNSEW_U_V*/
/*****************************************************************************/
/**
  
  Sets:

  Array (int, nxNyNz) fe_u: Normalized distance between E neighbour and e face for u cell.

  Array (int, nxNyNz) fw_u: Normalized distance between W neighbour and w face for u cell.

  Array (int, nxNyNz) fn_u: Normalized distance between N neighbour and n face for u cell.

  Array (int, nxNyNz) fs_u: Normalized distance between S neighbour and s face for u cell.

  Array (int, nxNyNz) ft_u: Normalized distance between T neighbour and t face for u cell.

  Array (int, nxNyNz) fb_u: Normalized distance between B neighbour and b face for u cell.

  Array (int, NxnyNz) fe_v: Normalized distance between E neighbour and e face for v cell.

  Array (int, NxnyNz) fw_v: Normalized distance between W neighbour and w face for v cell.

  Array (int, NxnyNz) fn_v: Normalized distance between N neighbour and n face for v cell.

  Array (int, NxnyNz) fs_v: Normalized distance between S neighbour and s face for v cell.

  Array (int, NxnyNz) ft_v: Normalized distance between T neighbour and t face for v cell.

  Array (int, NxnyNz) fb_v: Normalized distance between B neighbour and b face for v cell.

  Array (int, NxNynz) fe_w: Normalized distance between E neighbour and e face for w cell.

  Array (int, NxNynz) fw_w: Normalized distance between W neighbour and w face for w cell.

  Array (int, NxNynz) fn_w: Normalized distance between N neighbour and n face for w cell.

  Array (int, NxNynz) fs_w: Normalized distance between S neighbour and s face for w cell.

  Array (int, NxNynz) ft_w: Normalized distance between T neighbour and t face for w cell.

  Array (int, NxNynz) fb_w: Normalized distance between B neighbour and b face for w cell.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 12-08-2019
*/
int compute_fnsew_u_v(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_u, index_v, index_w;
  int ret_value = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int solve_3D_OK = data->solve_3D_OK;

  double *fe_u = data->fe_u;
  double *fw_u = data->fw_u;
  double *fn_u = data->fn_u;
  double *fs_u = data->fs_u;
  double *ft_u = data->ft_u;
  double *fb_u = data->fb_u;
  
  double *fe_v = data->fe_v;
  double *fw_v = data->fw_v;
  double *fn_v = data->fn_v;
  double *fs_v = data->fs_v;
  double *ft_v = data->ft_v;
  double *fb_v = data->fb_v;

  double *fe_w = data->fe_w;
  double *fw_w = data->fw_w;
  double *fn_w = data->fn_w;
  double *fs_w = data->fs_w;
  double *ft_w = data->ft_w;
  double *fb_w = data->fb_w;  

  const double *Delta_xe_u = data->Delta_xe_u;
  const double *Delta_xw_u = data->Delta_xw_u;
  const double *Delta_yn_u = data->Delta_yn_u;
  const double *Delta_ys_u = data->Delta_ys_u;
  const double *Delta_zt_u = data->Delta_zt_u;
  const double *Delta_zb_u = data->Delta_zb_u;
  
  const double *Delta_xE_u = data->Delta_xE_u;
  const double *Delta_xW_u = data->Delta_xW_u;
  const double *Delta_yN_u = data->Delta_yN_u;
  const double *Delta_yS_u = data->Delta_yS_u;
  const double *Delta_zT_u = data->Delta_zT_u;
  const double *Delta_zB_u = data->Delta_zB_u;  

  const double *Delta_xe_v = data->Delta_xe_v;
  const double *Delta_xw_v = data->Delta_xw_v;
  const double *Delta_yn_v = data->Delta_yn_v;
  const double *Delta_ys_v = data->Delta_ys_v;
  const double *Delta_zt_v = data->Delta_zt_v;
  const double *Delta_zb_v = data->Delta_zb_v;
  
  const double *Delta_xE_v = data->Delta_xE_v;
  const double *Delta_xW_v = data->Delta_xW_v;
  const double *Delta_yN_v = data->Delta_yN_v;
  const double *Delta_yS_v = data->Delta_yS_v;
  const double *Delta_zT_v = data->Delta_zT_v;
  const double *Delta_zB_v = data->Delta_zB_v;  

  const double *Delta_xe_w = data->Delta_xe_w;
  const double *Delta_xw_w = data->Delta_xw_w;
  const double *Delta_yn_w = data->Delta_yn_w;
  const double *Delta_ys_w = data->Delta_ys_w;
  const double *Delta_zt_w = data->Delta_zt_w;
  const double *Delta_zb_w = data->Delta_zb_w;
  
  const double *Delta_xE_w = data->Delta_xE_w;
  const double *Delta_xW_w = data->Delta_xW_w;
  const double *Delta_yN_w = data->Delta_yN_w;
  const double *Delta_yS_w = data->Delta_yS_w;
  const double *Delta_zT_w = data->Delta_zT_w;
  const double *Delta_zB_w = data->Delta_zB_w;  
  

#pragma omp parallel for default(none) private(i, J, K, index_u)		\
  shared(fe_u, fw_u, fn_u, fs_u, Delta_xe_u, Delta_xw_u, Delta_yn_u, Delta_ys_u, \
	 Delta_xE_u, Delta_xW_u, Delta_yN_u, Delta_yS_u)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
      
	index_u = K*nx*Ny + J*nx + i;

	fe_u[index_u] = 1 - Delta_xe_u[index_u] / Delta_xE_u[index_u];
	fw_u[index_u] = 1 - Delta_xw_u[index_u] / Delta_xW_u[index_u];
	fn_u[index_u] = 1 - Delta_yn_u[index_u] / Delta_yN_u[index_u];
	fs_u[index_u] = 1 - Delta_ys_u[index_u] / Delta_yS_u[index_u];
      
      }
    }
  }

#pragma omp parallel for default(none) private(I, j, K, index_v)		\
  shared(fe_v, fw_v, fn_v, fs_v, Delta_xe_v, Delta_xw_v, Delta_yn_v, Delta_ys_v, \
	 Delta_xE_v, Delta_xW_v, Delta_yN_v, Delta_yS_v)

  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	
	index_v = K*Nx*ny + j*Nx + I;
      
	fe_v[index_v] = 1 - Delta_xe_v[index_v] / Delta_xE_v[index_v];
	fw_v[index_v] = 1 - Delta_xw_v[index_v] / Delta_xW_v[index_v];
	fn_v[index_v] = 1 - Delta_yn_v[index_v] / Delta_yN_v[index_v];
	fs_v[index_v] = 1 - Delta_ys_v[index_v] / Delta_yS_v[index_v];
      }
    }
  }

  if(solve_3D_OK){

#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(ft_u, fb_u, Delta_zt_u, Delta_zb_u, Delta_zT_u, Delta_zB_u) 
    
    for(K=0; K<Nz; K++){
      for(J=0; J<Ny; J++){
	for(i=0; i<nx; i++){
      
	  index_u = K*nx*Ny + J*nx + i;

	  ft_u[index_u] = 1 - Delta_zt_u[index_u] / Delta_zT_u[index_u];
	  fb_u[index_u] = 1 - Delta_zb_u[index_u] / Delta_zB_u[index_u];      
	}
      }
    }

#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(ft_v, fb_v, Delta_zt_v, Delta_zb_v, Delta_zT_v, Delta_zB_v)

    for(K=0; K<Nz; K++){
      for(j=0; j<ny; j++){
	for(I=0; I<Nx; I++){
	  
	  index_v = K*Nx*ny + j*Nx + I;
      
	  ft_v[index_v] = 1 - Delta_zt_v[index_v] / Delta_zT_v[index_v];
	  fb_v[index_v] = 1 - Delta_zb_v[index_v] / Delta_zB_v[index_v];
	}
      }
    }

#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(fe_w, fw_w, fn_w, fs_w, ft_w, fb_w, Delta_xe_w, Delta_xw_w, Delta_yn_w, Delta_ys_w, Delta_zt_w, Delta_zb_w, \
	 Delta_xE_w, Delta_xW_w, Delta_yN_w, Delta_yS_w, Delta_zT_w, Delta_zB_w)

    for(k=0; k<nz; k++){
      for(J=0; J<Ny; J++){
	for(I=0; I<Nx; I++){
	  
	  index_w = k*Nx*Ny + J*Nx + I;
      
	  fe_w[index_w] = 1 - Delta_xe_w[index_w] / Delta_xE_w[index_w];
	  fw_w[index_w] = 1 - Delta_xw_w[index_w] / Delta_xW_w[index_w];
	  fn_w[index_w] = 1 - Delta_yn_w[index_w] / Delta_yN_w[index_w];
	  fs_w[index_w] = 1 - Delta_ys_w[index_w] / Delta_yS_w[index_w];
	  ft_w[index_w] = 1 - Delta_zt_w[index_w] / Delta_zT_w[index_w];
	  fb_w[index_w] = 1 - Delta_zb_w[index_w] / Delta_zB_w[index_w];
	}
      }
    }    
    
  }

  return ret_value;

}

/* FLUID_FRACTION_V_CELLS*/
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNy) vf_x : Solid fraction at u cells.

  Array (double, Nxny) vf_y : Solid fraction at v cells.

  Array (double, nxNy) vol_x: Fluid volume at u cells.

  Array (double, Nxny) vol_y: Fluid volume at v cells.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 05-02-2019
*/
int fluid_fraction_v_cells(Data_Mem *data){

  int I, J, i, j;
  int index_u, index_v, index_p;
  int curv_ind, curve_is_solid;
  int ret_value = 0;

  const int curves = data->curves;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *u_cut_matrix = data->u_cut_matrix;
  const int *v_cut_matrix = data->v_cut_matrix;

  double pc_temp[16] = {0};
  double c0[3] = {0};
  double c1[3] = {0};

  double *vf_x = data->vf_x;
  double *vf_y = data->vf_y;
  double *vol_x = data->vol_x;
  double *vol_y = data->vol_y;

  const double *poly_coeffs = data->poly_coeffs;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_x_u = data->Delta_x_u;
  const double *Delta_y_u = data->Delta_y_u;
  const double *Delta_x_v = data->Delta_x_v;
  const double *Delta_y_v = data->Delta_y_v;
  
  /* U */

  /* Default values */

#pragma omp parallel for default(none) private(i, J, index_u)	\
  shared(u_dom_matrix, vf_x, Delta_x_u, Delta_y_u, vol_x)

  for(J=0; J<Ny; J++){
    for(i=0; i<nx; i++){
      index_u = J*nx + i;

      if(u_dom_matrix[index_u] == 0){
	vf_x[index_u] = 0;
	vol_x[index_u] = Delta_x_u[index_u] * Delta_y_u[index_u];
      }
      else{
	vf_x[index_u] = 1;
	vol_x[index_u] = 0;
      }
    }
  }

  /* Cut cell values */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_is_solid = curve_is_solid_OK[curv_ind];


#pragma omp parallel for default(none) firstprivate(c0, c1) private(i, J, index_u, index_p) \
  shared(curve_is_solid, u_dom_matrix, curv_ind, nodes_x, nodes_y, Delta_x, \
	 Delta_y, pc_temp, u_cut_matrix, vf_x, Delta_x_u, Delta_y_u, vol_x)

    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	index_u = J*nx + i;
	if(u_cut_matrix[index_u] == curv_ind + 1){
	  index_p = J*Nx + i + 1;
	  c0[0] = nodes_x[index_p-1];
	  c0[1] = nodes_y[index_p-1] - 0.5 * Delta_y[index_p-1];
	  c1[0] = nodes_x[index_p];
	  c1[1] = nodes_y[index_p] + 0.5 * Delta_y[index_p];
	  vf_x[index_u] = vol_fraction_pol(c0, c1, pc_temp, curve_is_solid);
	  vol_x[index_u] = Delta_x_u[index_u] * Delta_y_u[index_u] * (1 - vf_x[index_u]);
	}
      }
    }
  }

  /* V */

  /* Default values */

#pragma omp parallel for default(none) private(I, j, index_v)	\
  shared(v_dom_matrix, vf_y, Delta_x_v, Delta_y_v, vol_y)

  for(j=0; j<ny; j++){
    for(I=0; I<Nx; I++){
      index_v = j*Nx + I;

      if(v_dom_matrix[index_v] == 0){
	vf_y[index_v] = 0;
	vol_y[index_v] = Delta_x_v[index_v] * Delta_y_v[index_v];
      }
      else{
	vf_y[index_v] = 1;
	vol_y[index_v] = 0;
      }
    }
  }

  /* Cut cell values */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_is_solid = curve_is_solid_OK[curv_ind];


#pragma omp parallel for default(none) firstprivate(c0, c1) private(I, j, index_v, index_p) \
  shared(curve_is_solid, v_dom_matrix, curv_ind, nodes_x, nodes_y, Delta_x, \
	 Delta_y, pc_temp, v_cut_matrix, vf_y, Delta_x_v, Delta_y_v, vol_y)

    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	index_v = j*Nx + I;

	if(v_cut_matrix[index_v] == curv_ind + 1){
	  index_p = (j + 1) * Nx + I;
	  c0[0] = nodes_x[index_p-Nx] - 0.5 * Delta_x[index_p-Nx];
	  c0[1] = nodes_y[index_p-Nx];
	  c1[0] = nodes_x[index_p] + 0.5 * Delta_x[index_p];
	  c1[1] = nodes_y[index_p];
	  vf_y[index_v] = vol_fraction_pol(c0, c1, pc_temp, curve_is_solid);
	  vol_y[index_v] = Delta_x_v[index_v] * Delta_y_v[index_v] * (1 - vf_y[index_v]);
	}
      }
    }
  }

  return ret_value;
}

/* COMPUTE_FNSEW_U_V_CUT_CELLS */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_index: face_y on u cell where 
  Data_Mem::Fu_y is interpolated near solid.

  Array (int, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_index_v: v cell nodes used for
  interpolation of Data_Mem::Fu_y near solid.

  Array (double, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_W: weight used for interpolation
  of Data_Mem::Fu_y near solid.

  Array (double, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_E: weight used for interpolation 
  of Data_Mem::Fu_y near solid.

  Array (int, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_index: face_x on v cell where 
  Data_Mem::Fv_x  is interpolated near solid.

  Array (int, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_index_u: u cell nodes used for 
  interpolation of Data_Mem::Fv_x near solid.

  Array (double, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_S: weight used for interpolation 
  of Data_Mem::Fv_x near solid.

  Array (double, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_N: weight used for interpolation
  of Data_Mem::Fv_x near solid.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 03-09-2020
*/
int compute_fnsew_u_v_cut_cells(Data_Mem *data){

  int i, j, index_p, index_u, index_v;
  int index_face;
  int u_cut_face_Fy_count = 0;
  int v_cut_face_Fx_count = 0;
  int ret_value = 0;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int Nx = data->Nx;

  double x_interp, y_interp;

  const double tol = 1e-5;

  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;

  const double *u_faces_y = data->u_faces_y;
  const double *v_faces_x = data->v_faces_x;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_u = data->nodes_y_u;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;


  /* Interpolation factors for Fu_y */

#pragma omp parallel for default(none) private(i, j, index_v)	\
  shared(p_cut_face_y) reduction(+: u_cut_face_Fy_count)
  
  for(j=0; j<ny; j++){
    for(i=1; i<nx-1; i++){

      index_v = j*Nx + i;

      if(p_cut_face_y[index_v] || p_cut_face_y[index_v+1]){
	u_cut_face_Fy_count++;
      }
    }
  }

  if(u_cut_face_Fy_count){

    data->u_cut_face_Fy_W = create_array(1, u_cut_face_Fy_count, 1, 0);
    data->u_cut_face_Fy_E = create_array(1, u_cut_face_Fy_count, 1, 0);
    data->u_cut_face_Fy_index = create_array_int(1, u_cut_face_Fy_count, 1, 0);
    data->u_cut_face_Fy_index_v = create_array_int(1, u_cut_face_Fy_count, 1, 0);
    data->u_cut_face_Fy_count = u_cut_face_Fy_count;

    double *u_cut_face_Fy_W = data->u_cut_face_Fy_W;
    double *u_cut_face_Fy_E = data->u_cut_face_Fy_E;
    int *u_cut_face_Fy_index = data->u_cut_face_Fy_index;
    int *u_cut_face_Fy_index_v = data->u_cut_face_Fy_index_v;

    u_cut_face_Fy_count = 0;

#pragma omp parallel for default(none) private(i, j, index_face, index_v, index_p, x_interp) \
  shared(p_cut_face_y, u_cut_face_Fy_count, u_faces_y, v_dom_matrix, nodes_x, nodes_x_v, \
	 u_cut_face_Fy_index, u_cut_face_Fy_W, u_cut_face_Fy_E, Delta_x, u_cut_face_Fy_index_v)
  
    for(j=0; j<ny; j++){
      for(i=1; i<nx-1; i++){

	index_face = j*nx + i;
	index_v = j*Nx + i;
	index_p = j*Nx + i;

	if(p_cut_face_y[index_v] || p_cut_face_y[index_v+1]){

#pragma omp critical
	  {

	    u_cut_face_Fy_index[u_cut_face_Fy_count] = index_face;
	    u_cut_face_Fy_index_v[u_cut_face_Fy_count] = index_v;

	    if(u_faces_y[index_face] > tol * (Delta_x[index_p] + Delta_x[index_p+1])){
	  
	      if(p_cut_face_y[index_v]){
	    
		if(v_dom_matrix[index_v+1]){
		  /* Solid: east, vW only used for interpolation */
		  x_interp = nodes_x[index_p] + 0.5 * u_faces_y[index_face];
		  u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (x_interp - nodes_x_v[index_v]) / 
		    (nodes_x[index_p] + u_faces_y[index_face]  - nodes_x_v[index_v]);
		  u_cut_face_Fy_E[u_cut_face_Fy_count] = 0;
		  
#ifdef OLDINT_OK
		  u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (x_interp - nodes_x_v[index_v]) / 
		    (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		  u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];	    			  
#endif
		}
		else{
		  /* Solid: west, both vW and vE used for interpolation */
		  x_interp = nodes_x[index_p+1] - 0.5 * u_faces_y[index_face];
		  u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (x_interp - nodes_x_v[index_v]) / 
		    (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		  u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];	    	      
		}
	      }
	      else{
		if(v_dom_matrix[index_v]){
		  /* Solid: west, vE only used for interpolation */
		  x_interp = nodes_x[index_p+1] - 0.5 * u_faces_y[index_face];
		  u_cut_face_Fy_W[u_cut_face_Fy_count] = 0;
		  u_cut_face_Fy_E[u_cut_face_Fy_count] = 0.5 * u_faces_y[index_face] / 
		    (nodes_x_v[index_v+1] - (nodes_x[index_p+1] - u_faces_y[index_face]));
		  
#ifdef OLDINT_OK		  
		  u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (x_interp - nodes_x_v[index_v]) / 
		    (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		  u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];	    	    	 		  
#endif
		}
		else{
		  /* Solid: east, both vW and vE used for interpolation */ 
		  x_interp = nodes_x[index_p] + 0.5 * u_faces_y[index_face];
		  u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (x_interp - nodes_x_v[index_v]) / 
		    (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		  u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];	    	    	      
		}
	      }
	    }
	    else{
	      /* Interior face */
	      u_cut_face_Fy_W[u_cut_face_Fy_count] = 0;
	      u_cut_face_Fy_E[u_cut_face_Fy_count] = 0;
	    }

	    u_cut_face_Fy_count++;

	  } /* critical */
	}
      }
    }
  } /* if(u_cut_face_Fy_count) */


  /* Interpolation factors for Fv_x */

#pragma omp parallel for default(none) private(i, j, index_u)	\
  shared(p_cut_face_x) reduction(+: v_cut_face_Fx_count)

  for(j=1; j<ny-1; j++){
    for(i=0; i<nx; i++){

      index_u = j*nx + i;
            
      if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx]){
	v_cut_face_Fx_count++;
      }
    }
  }

  
  if(v_cut_face_Fx_count){

    data->v_cut_face_Fx_S = create_array(1, v_cut_face_Fx_count, 1, 0);
    data->v_cut_face_Fx_N = create_array(1, v_cut_face_Fx_count, 1, 0);
    data->v_cut_face_Fx_index = create_array_int(1, v_cut_face_Fx_count, 1, 0);
    data->v_cut_face_Fx_index_u = create_array_int(1, v_cut_face_Fx_count, 1, 0);
    data->v_cut_face_Fx_count = v_cut_face_Fx_count;

    double *v_cut_face_Fx_S = data->v_cut_face_Fx_S;
    double *v_cut_face_Fx_N = data->v_cut_face_Fx_N;
    int *v_cut_face_Fx_index = data->v_cut_face_Fx_index;
    int *v_cut_face_Fx_index_u = data->v_cut_face_Fx_index_u;

    v_cut_face_Fx_count = 0;

#pragma omp parallel for default(none) private(i, j, index_face, index_u, index_p, y_interp) \
  shared(p_cut_face_x, nodes_y, v_faces_x, u_dom_matrix, v_cut_face_Fx_count, \
	 v_cut_face_Fx_S, v_cut_face_Fx_N, nodes_y_u, v_cut_face_Fx_index, Delta_y, v_cut_face_Fx_index_u)

    for(j=1; j<ny-1; j++){
      for(i=0; i<nx; i++){

	index_face = j*nx + i;
	index_u = j*nx + i;
	index_p = j*Nx + i;

	if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx]){


#pragma omp critical
	  {

	    v_cut_face_Fx_index[v_cut_face_Fx_count] = index_face;
	    v_cut_face_Fx_index_u[v_cut_face_Fx_count] = index_u;

	    if(v_faces_x[index_face] > tol * (Delta_y[index_p] + Delta_y[index_p+Nx])){
	  
	      if(p_cut_face_x[index_u]){
		if(u_dom_matrix[index_u+nx]){
		  /* Solid: north, uS only used for interpolation */
		  y_interp = nodes_y[index_p] + 0.5 * v_faces_x[index_face];
		  v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (y_interp - nodes_y_u[index_u]) / 
		    (nodes_y[index_p] + v_faces_x[index_face] - nodes_y_u[index_u]);
		  v_cut_face_Fx_N[v_cut_face_Fx_count] = 0;
		  
#ifdef OLDINT_OK
		  v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (y_interp - nodes_y_u[index_u]) / 
		    (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		  v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
#endif		  
		}
		else{
		  /* Solid: south, uS and uN used for interpolation */
		  y_interp = nodes_y[index_p+Nx] - 0.5 * v_faces_x[index_face];
		  v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (y_interp - nodes_y_u[index_u]) / 
		    (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		  v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
		}
	      }
	      else{
		if(u_dom_matrix[index_u]){
		  /* Solid: south, uN only used for interpolation */
		  y_interp = nodes_y[index_p+Nx] - 0.5 * v_faces_x[index_face];
		  v_cut_face_Fx_S[v_cut_face_Fx_count] = 0;
		  v_cut_face_Fx_N[v_cut_face_Fx_count] = 0.5 * v_faces_x[index_face] 
		    / (nodes_y_u[index_u+nx] - (nodes_y[index_p+Nx] - v_faces_x[index_face]));
		  
#ifdef OLDINT_OK
		  v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (y_interp - nodes_y_u[index_u]) / 
		    (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		  v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
#endif
		}
		else{
		  /* Solid: north, uS and uN used for interpolation */
		  y_interp = nodes_y[index_p] + 0.5 * v_faces_x[index_face];
		  v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (y_interp - nodes_y_u[index_u]) / 
		    (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		  v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
		}
	      }
	    }
	    else{
	      /* Interior face */
	      v_cut_face_Fx_S[v_cut_face_Fx_count] = 0;
	      v_cut_face_Fx_N[v_cut_face_Fx_count] = 0;
	    }

	    v_cut_face_Fx_count++;

	  } /* critical*/
	}
      }
    }
  } /* if(v_cut_face_Fx_count) */

  return ret_value;
}


/* P_NODES_IN_SOLID */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, p_nodes_in_solid_count) p_nodes_in_solid_indx: pressure cells 
  considered fluid on p_dom_matrix and solid on domain_matrix

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 05-02-2019
*/
void p_nodes_in_solid(Data_Mem *data){

  int I, J, index_;
  int p_nodes_in_solid_count = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *dom_matrix = data->domain_matrix;
  const int *p_dom_matrix = data->p_dom_matrix;


#pragma omp parallel for default(none) private(I, J, index_)		\
  shared(p_dom_matrix, dom_matrix) reduction(+: p_nodes_in_solid_count)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;

      if((p_dom_matrix[index_] == 0) && (dom_matrix[index_])){
	p_nodes_in_solid_count++;
      }
    }
  }

  if(p_nodes_in_solid_count > 0){
    
    data->p_nodes_in_solid_indx = create_array_int(p_nodes_in_solid_count, 1, 1, 0);
    data->p_nodes_in_solid_count = p_nodes_in_solid_count;

    int *p_nodes_in_solid_indx = data->p_nodes_in_solid_indx;

    p_nodes_in_solid_count = 0;
    
#pragma omp parallel for default(none) private(I, J, index_)		\
  shared(p_dom_matrix, dom_matrix, p_nodes_in_solid_indx, p_nodes_in_solid_count)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;

	if((p_dom_matrix[index_] == 0) && (dom_matrix[index_])){
#pragma omp critical
	  {
	    p_nodes_in_solid_indx[p_nodes_in_solid_count] = index_;
	    p_nodes_in_solid_count++;
	  }
	}
      }
    }
  }


  return;
}

/* SET_CUT_MATRIX_V_CELLS */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, nxNyNz) u_cut_matrix: set to curv_ind + 1 if u cell is cut.

  Array (int, NxnyNz) v_cut_matrix: set to curv_ind + 1 if v cell is cut.

  Array (int, NxNynz) w_cut_matrix: set to curv_ind + 1 if w cell is cut.

  Array (int, NxNyNz) u_cut_face_x: set to curv_ind + 1 if face_x of u
  cell is cut.

  Array (int, nxnyNz) u_cut_face_y: set to curv_ind + 1 if face_y of u
  cell is cut.

  Array (int, nxNynz) u_cut_face_z: set to curv_ind + 1 if face_z of u
  cell is cut.

  Array (int, nxnyNz) v_cut_face_x: set to curv_ind + 1 if face_x of v
  cell is cut.

  Array (int, NxNyNz) v_cut_face_y: set to curv_ind + 1 if face_y of v
  cell is cut.

  Array (int, Nxnynz) v_cut_face_z: set to curv_ind + 1 if face_z of v
  cell is cut.

  Array (int, nxNynz) w_cut_face_x: set to curv_ind + 1 if face_x of w
  cell is cut.

  Array (int, Nxnynz) w_cut_face_y: set to curv_ind + 1 if face_y of w
  cell is cut.

  Array (int, NxNyNz) w_cut_face_z: set to curv_ind + 1 if face_z of w
  cell is cut.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 29-07-2019
*/
int set_cut_matrix_v_cells(Data_Mem *data){

  int I, J, K;
  int i, j, k;
  int index_u, index_v, index_w;
  int vertex_cut;
  int index_face_x;
  int index_face_y;
  int index_face_z;
  int curv_ind;
  int w_face, e_face, s_face;
  int n_face, b_face, t_face;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int curves = data->curves;

  int *u_cut_face_x = data->u_cut_face_x;
  int *u_cut_face_y = data->u_cut_face_y;
  int *u_cut_face_z = data->u_cut_face_z;
  int *v_cut_face_x = data->v_cut_face_x;
  int *v_cut_face_y = data->v_cut_face_y;
  int *v_cut_face_z = data->v_cut_face_z;
  int *w_cut_face_x = data->w_cut_face_x;
  int *w_cut_face_y = data->w_cut_face_y;
  int *w_cut_face_z = data->w_cut_face_z;
  int *u_cut_matrix = data->u_cut_matrix;
  int *v_cut_matrix = data->v_cut_matrix;
  int *w_cut_matrix = data->w_cut_matrix;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double curve_factor;
  double f0, f1;
  
  double x0[3] = {0};
  double x1[3] = {0};
  double x_middle[3] = {0};
  double pc_temp[16];

  const double pol_tol = data->pol_tol;
  
  const double *poly_coeffs = data->poly_coeffs;
  
  const double *vertex_x_u = data->vertex_x_u;
  const double *vertex_y_v = data->vertex_y_v;
  const double *vertex_z_w = data->vertex_z_w;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;

  /* Determination of cut_faces for u cells */

  /* Edges parallel to x axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_y, index_face_z) \
  shared(vertex_x_u, vertex_y, vertex_z, pc_temp, curve_factor, u_cut_face_y, u_cut_face_z, curv_ind)
    
    for(k=0; k<nz; k++){
      for(j=0; j<ny; j++){
	for(I=0; I<Nx-1; I++){

	  vertex_cut = 0;
	  
	  x0[0] = vertex_x_u[I];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];
	  x1[0] = vertex_x_u[I+1];
	  x1[1] = x0[1];
	  x1[2] = x0[2];

	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }
	  
	  if(vertex_cut){
	    i = I - 1;
	    J = j + 1;
	    K = k + 1;
	    index_face_y = K*nx*ny + j*nx + i + 1;
	    index_face_z = k*nx*Ny + J*nx + i + 1;
#pragma omp critical
	    {
	      u_cut_face_y[index_face_y] = curv_ind + 1;
	      u_cut_face_y[index_face_y-nx*ny] = curv_ind + 1;
	      u_cut_face_z[index_face_z] = curv_ind + 1;
	      u_cut_face_z[index_face_z-nx] = curv_ind + 1;
	    }
	  } /* if(vertex_cut) */	  
	}
      }
    }
  } /* (curv_ind=0; curv_ind<curves; curv_ind++) */


  /* Edges parallel to y axis */
  
  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_x, index_face_z) \
  shared(vertex_x_u, vertex_y, vertex_z, pc_temp, curve_factor, u_cut_face_x, u_cut_face_z, curv_ind)
    
    for(k=0; k<nz; k++){
      for(j=0; j<ny-1; j++){
	for(I=0; I<Nx; I++){

	  vertex_cut = 0;

	  x0[0] = vertex_x_u[I];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];
	  x1[0] = x0[0];
	  x1[1] = vertex_y[j+1];
	  x1[2] = x0[2];

	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }

	  if(vertex_cut){
	    i = I - 1;
	    J = j + 1;
	    K = k + 1;
	    index_face_x = K*Nx*Ny + J*Nx + I;
	    index_face_z = k*nx*Ny + J*nx + i;
#pragma omp critical
	    {
	      u_cut_face_x[index_face_x] = curv_ind + 1;
	      u_cut_face_x[index_face_x-Nx*Ny] = curv_ind + 1;
	      if(i < nx-1){
		u_cut_face_z[index_face_z+1] = curv_ind + 1;
	      }
	      if(i > 0){
		u_cut_face_z[index_face_z] = curv_ind + 1;
	      }
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /* for(curv_ind=0; curv_ind<curves; curv_ind++) */

  /* Edges parallel to z axis */
  
  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_x, index_face_y) \
  shared(vertex_x_u, vertex_y, vertex_z, pc_temp, curve_factor, u_cut_face_x, u_cut_face_y, curv_ind)    
  
    for(k=0; k<nz-1; k++){
      for(j=0; j<ny; j++){
	for(I=0; I<Nx; I++){
	  
	  vertex_cut = 0;

	  x0[0] = vertex_x_u[I];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];
	  x1[0] = x0[0];
	  x1[1] = x0[1];
	  x1[2] = vertex_z[k+1];

	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }	  
	  
	  if(vertex_cut){
	    i = I - 1;
	    J = j + 1;	    
	    K = k + 1;
	    index_face_x = K*Nx*Ny + J*Nx + I;
	    index_face_y = K*nx*ny + j*nx + i;
	    
#pragma omp critical
	    {
	      u_cut_face_x[index_face_x] = curv_ind + 1;
	      u_cut_face_x[index_face_x-Nx] = curv_ind + 1;
	      if(i < nx-1){
		u_cut_face_y[index_face_y+1] = curv_ind + 1;
	      }
	      if(i > 0){
		u_cut_face_y[index_face_y] = curv_ind + 1;
	      }
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /* for(curv_ind=0; curv_ind<curves; curv_ind++) */  


  /* Determination of cut_faces for v cells */
  
  /* Edges parallel to x axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_y, index_face_z) \
  shared(vertex_x, vertex_y_v, vertex_z, pc_temp, curve_factor, v_cut_face_y, v_cut_face_z, curv_ind)      
  
    for(k=0; k<nz; k++){
      for(J=0; J<Ny; J++){
	for(i=0; i<nx-1; i++){
	  
	  vertex_cut = 0;

	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y_v[J];
	  x0[2] = vertex_z[k];
	  x1[0] = vertex_x[i+1];
	  x1[1] = x0[1];
	  x1[2] = x0[2];


	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }	  
	  
	  if(vertex_cut){
	    I = i + 1;
	    j = J - 1;	    
	    K = k + 1;
	    index_face_y = K*Nx*Ny + J*Nx + I;
	    index_face_z = k*Nx*ny + j*Nx + I;	  
	
#pragma omp critical
	    {
	      v_cut_face_y[index_face_y] = curv_ind + 1;
	      v_cut_face_y[index_face_y-Nx*Ny] = curv_ind + 1;
	      if(j < ny-1){
		v_cut_face_z[index_face_z+Nx] = curv_ind + 1;
	      }
	      if(j > 0){
		v_cut_face_z[index_face_z] = curv_ind + 1;
	      }
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++) */

  /* Edges parallel to the y axis */
  
  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_x, index_face_z) \
  shared(vertex_x, vertex_y_v, vertex_z, pc_temp, curve_factor, v_cut_face_x, v_cut_face_z, curv_ind)      
  
    for(k=0; k<nz; k++){
      for(J=0; J<Ny-1; J++){
	for(i=0; i<nx; i++){
	  
	  vertex_cut = 0;

	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y_v[J];
	  x0[2] = vertex_z[k];
	  x1[0] = x0[0];
	  x1[1] = vertex_y_v[J+1];
	  x1[2] = x0[2];


	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }	  
	  
	  if(vertex_cut){
	    I = i + 1;
	    j = J - 1;
	    K = k + 1;
	    index_face_x = K*nx*ny + (j+1)*nx + i;
	    index_face_z = k*Nx*ny + (j+1)*Nx + I;	  
	
#pragma omp critical
	    {
	      v_cut_face_x[index_face_x] = curv_ind + 1;
	      v_cut_face_x[index_face_x-nx*ny] = curv_ind + 1;
	      v_cut_face_z[index_face_z] = curv_ind + 1;
	      v_cut_face_z[index_face_z-1] = curv_ind + 1;
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++) */

  /* Edges parallel to the z axis */
  
  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_x, index_face_y) \
  shared(vertex_x, vertex_y_v, vertex_z, pc_temp, curve_factor, v_cut_face_x, v_cut_face_y, curv_ind)      
  
    for(k=0; k<nz-1; k++){
      for(J=0; J<Ny; J++){
	for(i=0; i<nx; i++){
	  
	  vertex_cut = 0;

	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y_v[J];
	  x0[2] = vertex_z[k];
	  x1[0] = x0[0];
	  x1[1] = x0[1];
	  x1[2] = vertex_z[k+1];


	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }	  
	  
	  if(vertex_cut){
	    j = J - 1;
	    K = k + 1;
	    I = i + 1;
	    index_face_x = K*nx*ny + j*nx + i;
	    index_face_y = K*Nx*Ny + J*Nx + I;	  
	
#pragma omp critical
	    {
	      if(j < ny-1){		
		v_cut_face_x[index_face_x+nx] = curv_ind + 1;
	      }
	      if(j > 0){
		v_cut_face_x[index_face_x] = curv_ind + 1;
	      }
	      v_cut_face_y[index_face_y] = curv_ind + 1;
	      v_cut_face_y[index_face_y-1] = curv_ind + 1;
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++) */

  /* Determination of cut_faces for w cells */
  
  /* Edges parallel to x axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_y, index_face_z) \
  shared(vertex_x, vertex_y, vertex_z_w, pc_temp, curve_factor, w_cut_face_y, w_cut_face_z, curv_ind)      
  
    for(K=0; K<Nz; K++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx-1; i++){
	  
	  vertex_cut = 0;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z_w[K];
	  x1[0] = vertex_x[i+1];
	  x1[1] = x0[1];
	  x1[2] = x0[2];


	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }	  
	  
	  if(vertex_cut){
	    I = i + 1;
	    J = j + 1;
	    k = K - 1;
	    index_face_y = k*Nx*ny + j*Nx + I;
	    index_face_z = K*Nx*Ny + J*Nx + I;	  
	
#pragma omp critical
	    {
	      if(k < nz-1){		
		w_cut_face_y[index_face_y+Nx*ny] = curv_ind + 1;
	      }
	      if(k > 0){
		w_cut_face_y[index_face_y] = curv_ind + 1;
	      }
	      w_cut_face_z[index_face_z] = curv_ind + 1;
	      w_cut_face_z[index_face_z-Nx] = curv_ind + 1;
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++) */


  /* Edges parallel to y axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_x, index_face_z) \
  shared(vertex_x, vertex_y, vertex_z_w, pc_temp, curve_factor, w_cut_face_x, w_cut_face_z, curv_ind)      
  
    for(K=0; K<Nz; K++){
      for(j=0; j<ny-1; j++){
	for(i=0; i<nx; i++){
	  
	  vertex_cut = 0;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z_w[K];
	  x1[0] = x0[0];
	  x1[1] = vertex_y[j+1];
	  x1[2] = x0[2];


	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }	  
	  
	  if(vertex_cut){
	    I = i + 1;
	    J = j + 1;
	    k = K - 1;
	    index_face_x = k*nx*Ny + J*nx + i;
	    index_face_z = K*Nx*Ny + J*Nx + I;	  
	
#pragma omp critical
	    {
	      if(i < nx-1){		
		w_cut_face_x[index_face_x+nx*Ny] = curv_ind + 1;
	      }
	      if(i > 0){
		w_cut_face_x[index_face_x] = curv_ind + 1;
	      }
	      w_cut_face_z[index_face_z] = curv_ind + 1;
	      w_cut_face_z[index_face_z-1] = curv_ind + 1;
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++)  */

  /* Edges parallel to z axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(I, J, K, i, j, k, x0, x1, x_middle, f0, f1, \
					       vertex_cut, index_face_x, index_face_y) \
  shared(vertex_x, vertex_y, vertex_z_w, pc_temp, curve_factor, w_cut_face_x, w_cut_face_y, curv_ind)      
  
    for(K=0; K<Nz-1; K++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){
	  
	  vertex_cut = 0;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z_w[K];
	  x1[0] = x0[0];
	  x1[1] = x0[1];
	  x1[2] = vertex_z_w[K+1];

	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;
	  }	  
	  
	  if(vertex_cut){
	    I = i + 1;
	    J = j + 1;
	    k = K - 1;
	    index_face_x = (k+1)*nx*Ny + J*nx + i;
	    index_face_y = (k+1)*Nx*ny + j*Nx + I;	  
	
#pragma omp critical
	    {
	      w_cut_face_x[index_face_x] = curv_ind + 1;
	      w_cut_face_x[index_face_x-nx] = curv_ind + 1;
	      w_cut_face_y[index_face_y] = curv_ind + 1;
	      w_cut_face_y[index_face_y-1] = curv_ind + 1;
	    }
	  } /* if(vertex_cut) */
	}
      }
    }
  } /*   for(curv_ind=0; curv_ind<curves; curv_ind++)  */


  /* It is assumed that every cell is cut by at most one surface */

  /* Evaluate if u cell is cut */  
  
#pragma omp parallel for default(none) private(I, J, K, i, j, k, e_face, w_face, n_face, s_face, \
					       t_face, b_face, index_u, index_face_x, index_face_y, index_face_z) \
  shared(u_cut_face_x, u_cut_face_y, u_cut_face_z, u_cut_matrix)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	I = i+1;
	j = J-1;
	k = K-1;

	e_face = 0;
	w_face = 0;
	n_face = 0;
	s_face = 0;
	t_face = 0;
	b_face = 0;

	index_u = K*nx*Ny + J*nx + i;
	index_face_x = K*Nx*Ny + J*Nx + I;
	index_face_y = K*nx*ny + j*nx + i;
	index_face_z = k*nx*Ny + J*nx + i;

	e_face = u_cut_face_x[index_face_x];
	w_face = u_cut_face_x[index_face_x-1];
	  
	if(J<Ny-1){
	  n_face = u_cut_face_y[index_face_y+nx];
	}
	if(J>0){	    
	  s_face = u_cut_face_y[index_face_y];
	}
	if(K<Nz-1){	    
	  t_face = u_cut_face_z[index_face_z+nx*Ny];
	}
	if(K>0){
	  b_face = u_cut_face_z[index_face_z];
	}

	if(e_face != 0){
	  u_cut_matrix[index_u] = e_face;
	}
	else if(w_face != 0){
	  u_cut_matrix[index_u] = w_face;
	}
	else if(n_face != 0){
	  u_cut_matrix[index_u] = n_face;
	}
	else if(s_face != 0){
	  u_cut_matrix[index_u] = s_face;
	}
	else if(t_face != 0){
	  u_cut_matrix[index_u] = t_face;
	}
	else if(b_face != 0){
	  u_cut_matrix[index_u] = b_face;
	}	    	  
      }
    }
  }
    

  /* Evaluate if v cell is cut */

#pragma omp parallel for default(none) private(I, J, K, i, j, k, e_face, w_face, n_face, s_face, \
					       t_face, b_face, index_v, index_face_x, index_face_y, index_face_z) \
  shared(v_cut_face_x, v_cut_face_y, v_cut_face_z, v_cut_matrix)
    
  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	i = I-1;
	J = j+1;
	k = K-1;

	e_face = 0;
	w_face = 0;
	n_face = 0;
	s_face = 0;
	t_face = 0;
	b_face = 0;

	index_v = K*Nx*ny + j*Nx + I;
	index_face_x = K*nx*ny + j*nx + i;
	index_face_y = K*Nx*Ny + J*Nx + I;
	index_face_z = k*Nx*ny + j*Nx + I;

	n_face = v_cut_face_y[index_face_y];
	s_face = v_cut_face_y[index_face_y-Nx];
	  	  
	if(I<Nx-1){
	  e_face = v_cut_face_x[index_face_x+1];
	}
	if(I>0){	    
	  w_face = v_cut_face_x[index_face_x];
	}
	if(K<Nz-1){	    
	  t_face = v_cut_face_z[index_face_z+Nx*ny];
	}
	if(K>0){
	  b_face = v_cut_face_z[index_face_z];
	}

	if(e_face != 0){
	  v_cut_matrix[index_v] = e_face;
	}
	else if(w_face != 0){
	  v_cut_matrix[index_v] = w_face;
	}
	else if(n_face != 0){
	  v_cut_matrix[index_v] = n_face;
	}
	else if(s_face != 0){
	  v_cut_matrix[index_v] = s_face;
	}
	else if(t_face != 0){
	  v_cut_matrix[index_v] = t_face;
	}
	else if(b_face != 0){
	  v_cut_matrix[index_v] = b_face;
	}	    	
      }
    }
  }
    

  /* Evaluate if w cell is cut */

#pragma omp parallel for default(none) private(I, J, K, i, j, k, e_face, w_face, n_face, s_face, \
					       t_face, b_face, index_w, index_face_x, index_face_y, index_face_z) \
  shared(w_cut_face_x, w_cut_face_y, w_cut_face_z, w_cut_matrix)
    
  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	i = I-1;
	j = J-1;
	K = k+1;

	e_face = 0;
	w_face = 0;
	n_face = 0;
	s_face = 0;
	t_face = 0;
	b_face = 0;

	index_w = k*Nx*Ny + J*Nx + I;
	index_face_x = k*nx*Ny + J*nx + i;
	index_face_y = k*Nx*ny + j*Nx + I;
	index_face_z = K*Nx*Ny + J*Nx + I;

	t_face = w_cut_face_z[index_face_z];
	b_face = w_cut_face_z[index_face_z-Nx*Ny];
	  	  	  
	if(I<Nx-1){
	  e_face = w_cut_face_x[index_face_x+1];
	}
	if(I>0){	    
	  w_face = w_cut_face_x[index_face_x];
	}
	if(J<Ny-1){	    
	  n_face = w_cut_face_y[index_face_y+Nx];
	}
	if(J>0){
	  s_face = w_cut_face_y[index_face_y];
	}

	if(e_face != 0){
	  w_cut_matrix[index_w] = e_face;
	}
	else if(w_face != 0){
	  w_cut_matrix[index_w] = w_face;
	}
	else if(n_face != 0){
	  w_cut_matrix[index_w] = n_face;
	}
	else if(s_face != 0){
	  w_cut_matrix[index_w] = s_face;
	}
	else if(t_face != 0){
	  w_cut_matrix[index_w] = t_face;
	}
	else if(b_face != 0){
	  w_cut_matrix[index_w] = b_face;
	}	    	
      }
    }
  }    

  return 0;  
}

/* FACE_AREA_3D */
/*****************************************************************************/
/**
  
  Sets:
  Array (double, NxNyNz) u_faces_x: area of u cell perpendicular to x axis
  in fluid domain.

  Array (double, nxnyNz) u_faces_y: area of u cell perpendicular to y axis
  in fluid domain.

  Array (double, nxNynz) u_faces_z: area of u cell perpendicular to z axis
  in fluid domain.

  Array (double, nxnyNz) v_faces_x: area of v cell perpendicular to x axis
  in fluid domain.

  Array (double, NxNyNz) v_faces_y: area of v cell perpendicular to y axis
  in fluid domain.

  Array (double, Nxnynz) v_faces_z: area of v cell perpendicular to z axis 
  in fluid domain.

  Array (double, nxNynz) w_faces_x: area of w cell perpendicular to x axis
  in fluid domain.

  Array (double, Nxnynz) w_faces_y: area of w cell perpendicular to y axis
  in fluid domain.

  Array (double, NxNyNz) w_faces_z: area of w cell perpendicular to z axis
  in fluid domain.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 31-07-2019
*/
int face_area_3D(Data_Mem *data){

  int I, J, K;
  int i, j, k;
  int curv_ind, coeffs_ind;
  int index_face;
  int index_u, index_v, index_w;
  int index_face_x, index_face_y, index_face_z;
  int curve_is_solid_flag;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int curves = data->curves;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;
  const int *u_cut_face_x = data->u_cut_face_x;
  const int *u_cut_face_y = data->u_cut_face_y;
  const int *u_cut_face_z = data->u_cut_face_z;
  const int *v_cut_face_x = data->v_cut_face_x;
  const int *v_cut_face_y = data->v_cut_face_y;
  const int *v_cut_face_z = data->v_cut_face_z;
  const int *w_cut_face_x = data->w_cut_face_x;
  const int *w_cut_face_y = data->w_cut_face_y;
  const int *w_cut_face_z = data->w_cut_face_z;

  double curve_factor;
  double f0, f1, f2, f3;
  
  double x0[3] = {0};
  double x1[3] = {0};
  double x_test[3] = {0};
  double pc_temp[16];
    
  double *u_faces_x = data->u_faces_x;
  double *u_faces_y = data->u_faces_y;
  double *u_faces_z = data->u_faces_z;
  double *v_faces_x = data->v_faces_x;
  double *v_faces_y = data->v_faces_y;
  double *v_faces_z = data->v_faces_z;
  double *w_faces_x = data->w_faces_x;
  double *w_faces_y = data->w_faces_y;
  double *w_faces_z = data->w_faces_z;

  const double *poly_coeffs = data->poly_coeffs;
  
  const double *Delta_x_u = data->Delta_x_u;
  const double *Delta_y_u = data->Delta_y_u;
  const double *Delta_z_u = data->Delta_z_u;
  const double *Delta_x_v = data->Delta_x_v;
  const double *Delta_y_v = data->Delta_y_v;
  const double *Delta_z_v = data->Delta_z_v;
  const double *Delta_x_w = data->Delta_x_w;
  const double *Delta_y_w = data->Delta_y_w;
  const double *Delta_z_w = data->Delta_z_w;  

  const double *vertex_x_u = data->vertex_x_u;
  const double *vertex_y_v = data->vertex_y_v;
  const double *vertex_z_w = data->vertex_z_w;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;

  
  /* Area values initialization */
  
  /* u_faces_x */
  
#pragma omp parallel for default(none) private(I, J, K, index_u, index_face) \
  shared(u_faces_x, Delta_y_u, Delta_z_u)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx-1; I++){
	index_u = K*nx*Ny + J*nx + I;
	index_face = K*Nx*Ny + J*Nx + I;
	u_faces_x[index_face] = Delta_y_u[index_u] * Delta_z_u[index_u];
      }
    }
  }

  I = Nx - 1;

#pragma omp parallel for default(none) private(J, K, index_u, index_face) \
  shared(u_faces_x, Delta_y_u, Delta_z_u, I)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      index_u = K*nx*Ny + J*nx + I - 1;
      index_face = K*Nx*Ny + J*Nx + I;
      u_faces_x[index_face] = Delta_y_u[index_u] * Delta_z_u[index_u];
    }
  }

  /* u_faces_y */
  
#pragma omp parallel for default(none) private(i, j, K, index_u, index_face) \
  shared(u_faces_y, Delta_x_u, Delta_z_u)

  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(i=0; i<nx; i++){
	index_u = K*nx*Ny + j*nx + i;
	index_face = K*nx*ny + j*nx + i;
	u_faces_y[index_face] = Delta_x_u[index_u] * Delta_z_u[index_u];
      }
    }
  }

  /* u_faces_z */
  
#pragma omp parallel for default(none) private(i, J, j, index_u, index_face) \
  shared(u_faces_z, Delta_x_u, Delta_y_u)
  
  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	index_u = k*nx*Ny + J*nx + i;
	index_face = k*nx*Ny + J*nx + i; 
	u_faces_z[index_face] = Delta_x_u[index_u] * Delta_y_u[index_u];
      }
    }
  }

  /* v_faces_x */

#pragma omp parallel for default(none) private(i, j, K, index_v, index_face) \
  shared(v_faces_x, Delta_y_v, Delta_z_v)
  
  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(i=0; i<nx; i++){
	index_v = K*Nx*ny + j*Nx + i;
	index_face = K*nx*ny + j*nx + i;
	v_faces_x[index_face] = Delta_y_v[index_v] * Delta_z_v[index_v];
      }
    }
  }

  /* v_faces_y */

#pragma omp parallel for default(none) private(I, J, K, index_v, index_face) \
  shared(v_faces_y, Delta_x_v, Delta_z_v)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny-1; J++){
      for(I=0; I<Nx; I++){
	index_v = K*Nx*ny + J*Nx + I;
	index_face = K*Nx*Ny + J*Nx + I;
	v_faces_y[index_face] = Delta_x_v[index_v] * Delta_z_v[index_v];
      }
    }
  }

  J = Ny - 1;

#pragma omp parallel for default(none) private(I, K, index_v, index_face) \
  shared(J, v_faces_y, Delta_x_v, Delta_z_v)
  
  for(K=0; K<Nz; K++){
    for(I=0; I<Nx; I++){
      index_v = K*Nx*ny + (J-1)*Nx + I;
      index_face = K*Nx*Ny + J*Nx + I;
      v_faces_y[index_face] = Delta_x_v[index_v] * Delta_z_v[index_v];
    }
  }  

  /* v_faces_z */

#pragma omp parallel for default(none) private(k, j, I, index_v, index_face) \
  shared(v_faces_z, Delta_x_v, Delta_y_v)
  
  for(k=0; k<nz; k++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	index_v = k*Nx*ny + j*Nx + I;
	index_face = k*Nx*ny + j*Nx + I;
	v_faces_z[index_face] = Delta_x_v[index_v] * Delta_y_v[index_v];
      }
    }
  }

  /* w_faces_x */

#pragma omp parallel for default(none) private(i, J, k, index_w, index_face) \
  shared(w_faces_x, Delta_y_w, Delta_z_w)
  
  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	index_w = k*Nx*Ny + J*Nx + i;
	index_face = k*nx*Ny + J*nx + i;
	w_faces_x[index_face] = Delta_y_w[index_w] * Delta_z_w[index_w];
      }
    }
  }

  /* w_faces_y */

#pragma omp parallel for default(none) private(I, j, k, index_w, index_face) \
  shared(w_faces_y, Delta_x_w, Delta_z_w)
  
  for(k=0; k<nz; k++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	index_w = k*Nx*Ny + j*Nx + I;
	index_face = k*Nx*ny + j*Nx + I;
	w_faces_y[index_face] = Delta_x_w[index_w] * Delta_z_w[index_w];
      }
    }
  }

  /* w_faces_z */

#pragma omp parallel for default(none) private(I, J, K, index_w, index_face) \
  shared(w_faces_z, Delta_x_w, Delta_y_w)
  
  for(K=0; K<Nz-1; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	index_w = K*Nx*Ny + J*Nx + I;
	index_face = K*Nx*Ny + J*Nx + I;
	w_faces_z[index_face] = Delta_x_w[index_w] * Delta_y_w[index_w];
      }
    }
  }

  K = Nz - 1;

#pragma omp parallel for default(none) private(I, J, index_w, index_face) \
  shared(w_faces_z, Delta_x_w, Delta_y_w, K)
  
  for(J=0; J<Ny; J++){
    for(I=0; I<Nx; I++){
      index_w = (K-1)*Nx*Ny + J*Nx + I;
      index_face = K*Nx*Ny + J*Nx + I;
      w_faces_z[index_face] = Delta_x_w[index_w] * Delta_y_w[index_w];
    }
  }


  /* Set u cells internal faces to zero */

  /* u_faces_x */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, J, K, j, k, index_face_x, x_test, f0, f1, f2, f3) \
  shared(vertex_x_u, vertex_y, vertex_z, pc_temp, curve_factor, u_faces_x)
  
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=0; I<Nx; I++){
	  j = J-1;
	  k = K-1;
	  index_face_x = K*Nx*Ny+ J*Nx + I;
	
	  x_test[0] = vertex_x_u[I];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[1] = vertex_y[j+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;
		
	  x_test[2] = vertex_z[k+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[1] = vertex_y[j];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    u_faces_x[index_face_x] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* u_faces_y */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, K, i, j, k, index_face_y, x_test, f0, f1, f2, f3) \
  shared(vertex_x_u, vertex_y, vertex_z, pc_temp, curve_factor, u_faces_y)
  
    for(K=1; K<Nz-1; K++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){
	  I = i+1;
	  k = K-1;
	  index_face_y = K*nx*ny+ j*nx + i;
	
	  x_test[0] = vertex_x_u[I-1];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x_u[I];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[2] = vertex_z[k+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x_u[I-1];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    u_faces_y[index_face_y] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

    /* u_faces_z */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, J, i, j, k, index_face_z, x_test, f0, f1, f2, f3) \
  shared(vertex_x_u, vertex_y, vertex_z, pc_temp, curve_factor, u_faces_z)
  
    for(k=0; k<nz; k++){
      for(J=1; J<Ny-1; J++){
	for(i=0; i<nx; i++){
	  I = i+1;
	  j = J-1;
	  index_face_z = k*nx*Ny+ J*nx + i;
	
	  x_test[0] = vertex_x_u[I-1];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x_u[I];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[1] = vertex_y[j+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x_u[I-1];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    u_faces_z[index_face_z] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)    
    
  /* Set v cells internal faces to zero */

  /* v_faces_x */
    
  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(J, K, i, j, k, index_face_x, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y_v, vertex_z, pc_temp, curve_factor, v_faces_x)
  
    for(K=1; K<Nz-1; K++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){
	  J = j+1;
	  k = K-1;
	  index_face_x = K*nx*ny+ j*nx + i;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y_v[J-1];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[1] = vertex_y_v[J];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[2] = vertex_z[k+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[1] = vertex_y_v[J-1];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    v_faces_x[index_face_x] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* v_faces_y */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, J, K, i, k, index_face_y, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y_v, vertex_z, pc_temp, curve_factor, v_faces_y)
  
    for(K=1; K<Nz-1; K++){
      for(J=0; J<Ny; J++){
	for(I=1; I<Nx-1; I++){
	  i = I-1;
	  k = K-1;
	  index_face_y = K*Nx*Ny+ J*Nx + I;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y_v[J];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x[i+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[2] = vertex_z[k+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x[i];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    v_faces_y[index_face_y] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

    /* v_faces_z */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, J, i, j, k, index_face_z, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y_v, vertex_z, pc_temp, curve_factor, v_faces_z)
  
    for(k=0; k<nz; k++){
      for(j=0; j<ny; j++){
	for(I=1; I<Nx-1; I++){
	  i = I-1;
	  J = j+1;
	  index_face_z = k*Nx*ny+ j*Nx + I;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y_v[J-1];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x[i+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[1] = vertex_y_v[J];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x[i];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    v_faces_z[index_face_z] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* Set w cells internal faces to zero */
  
    /* w_faces_x */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(J, K, i, j, k, index_face_x, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y, vertex_z_w, pc_temp, curve_factor, w_faces_x)
  
    for(k=0; k<nz; k++){
      for(J=1; J<Ny-1; J++){
	for(i=0; i<nx; i++){
	  j = J-1;
	  K = k+1;
	  index_face_x = k*nx*Ny+ J*nx + i;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z_w[K-1];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[1] = vertex_y[j+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[2] = vertex_z_w[K];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[1] = vertex_y[j];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    w_faces_x[index_face_x] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

    /* w_faces_y */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, K, i, j, k, index_face_y, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y, vertex_z_w, pc_temp, curve_factor, w_faces_y)
  
    for(k=0; k<nz; k++){
      for(j=0; j<ny; j++){
	for(I=1; I<Nx-1; I++){
	  i = I-1;
	  K = k+1;
	  index_face_y = k*Nx*ny+ j*Nx + I;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z_w[K-1];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x[i+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[2] = vertex_z_w[K];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x[i];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    w_faces_y[index_face_y] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* w_faces_z */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, J, K, i, j, index_face_z, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y, vertex_z_w, pc_temp, curve_factor, w_faces_z)
  
    for(K=1; K<Nz; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  i = I-1;
	  j = J-1;
	  index_face_z = K*Nx*Ny+ J*Nx + I;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z_w[K];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x[i+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[1] = vertex_y[j+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x[i];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    w_faces_z[index_face_z] = 0;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

    /* Set areas for cut faces */

  /* u_faces_x */
      
#pragma omp parallel for default(none) private(I, J, K, j, k, index_face_x, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x_u, vertex_y, vertex_z, u_faces_x, u_cut_face_x, curve_is_solid_OK, poly_coeffs)
  
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=0; I<Nx; I++){

	index_face_x = K*Nx*Ny+ J*Nx + I;

	if(u_cut_face_x[index_face_x]){

	  curv_ind = u_cut_face_x[index_face_x] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	    

	  j = J-1;
	  k = K-1;
	  
	  x0[0] = vertex_x_u[I];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];

	  x1[0] = vertex_x_u[I];
	  x1[1] = vertex_y[j+1];
	  x1[2] = vertex_z[k+1];
	    
	  u_faces_x[index_face_x] = u_faces_x[index_face_x] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 0);
	}
      }
    }
  }


  /* u_faces_y */
    
#pragma omp parallel for default(none) private(I, K, i, j, k, index_face_y, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x_u, vertex_y, vertex_z, u_faces_y, u_cut_face_y, poly_coeffs, curve_is_solid_OK)
  
  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(i=0; i<nx; i++){

	index_face_y = K*nx*ny+ j*nx + i;
	
	if(u_cut_face_y[index_face_y]){

	  curv_ind = u_cut_face_y[index_face_y] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];

	  I = i+1;
	  k = K-1;
	  
	  x0[0] = vertex_x_u[I-1];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];

	  x1[0] = vertex_x_u[I];
	  x1[1] = vertex_y[j];
	  x1[2] = vertex_z[k+1];
	    
	  u_faces_y[index_face_y] = u_faces_y[index_face_y] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 1);
	}
      }
    }
  }


  /* u_faces_z */
    
#pragma omp parallel for default(none) private(I, J, i, j, k, index_face_z, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x_u, vertex_y, vertex_z, u_faces_z, u_cut_face_z, poly_coeffs, curve_is_solid_OK)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	  
	index_face_z = k*nx*Ny+ J*nx + i;

	if(u_cut_face_z[index_face_z]){

	  curv_ind = u_cut_face_z[index_face_z] - 1;
    
	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	    

	  I = i+1;
	  j = J-1;
	  
	  x0[0] = vertex_x_u[I-1];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];
	  	 
	  x1[0] = vertex_x_u[I];
	  x1[1] = vertex_y[j+1];
	  x1[2] = vertex_z[k];
	    
	  u_faces_z[index_face_z] = u_faces_z[index_face_z] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 2);
	}	  
      }
    }
  }
    

  /* v_faces_x */
          
#pragma omp parallel for default(none) private(J, K, i, j, k, index_face_x, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x, vertex_y_v, vertex_z, v_faces_x, v_cut_face_x, poly_coeffs, curve_is_solid_OK)
  
  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(i=0; i<nx; i++){

	index_face_x = K*nx*ny+ j*nx + i;

	if(v_cut_face_x[index_face_x]){

	  curv_ind = v_cut_face_x[index_face_x] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	  

	  J = j+1;
	  k = K-1;
	
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y_v[J-1];
	  x0[2] = vertex_z[k];

	  x1[0] = vertex_x[i];
	  x1[1] = vertex_y_v[J];
	  x1[2] = vertex_z[k+1];
	    
	  v_faces_x[index_face_x] = v_faces_x[index_face_x] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 0);
	}	  
      }
    }
  }


  /* v_faces_y */
  
#pragma omp parallel for default(none) private(I, J, K, i, k, index_face_y, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x, vertex_y_v, vertex_z, v_faces_y, v_cut_face_y, poly_coeffs, curve_is_solid_OK)
  
  for(K=1; K<Nz-1; K++){
    for(J=0; J<Ny; J++){
      for(I=1; I<Nx-1; I++){

	index_face_y = K*Nx*Ny+ J*Nx + I;

	if(v_cut_face_y[index_face_y]){

	  curv_ind = v_cut_face_y[index_face_y] - 1;
	  
	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	    

	  i = I-1;
	  k = K-1;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y_v[J];
	  x0[2] = vertex_z[k];
	  
	  x1[0] = vertex_x[i+1];
	  x1[1] = vertex_y_v[J];
	  x1[2] = vertex_z[k+1];
	    
	  v_faces_y[index_face_y] = v_faces_y[index_face_y] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 1);
	}	  	  
      }
    }
  }


    /* v_faces_z */

      
#pragma omp parallel for default(none) private(I, J, i, j, k, index_face_z, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x, vertex_y_v, vertex_z, v_faces_z, v_cut_face_z, poly_coeffs, curve_is_solid_OK)
  
  for(k=0; k<nz; k++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){

	index_face_z = k*Nx*ny+ j*Nx + I;

	if(v_cut_face_z[index_face_z]){

	  curv_ind = v_cut_face_z[index_face_z] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	    

	  i = I-1;
	  J = j+1;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y_v[J-1];
	  x0[2] = vertex_z[k];

	  x1[0] = vertex_x[i+1];
	  x1[1] = vertex_y_v[J];
	  x1[2] = vertex_z[k];
	    
	  v_faces_z[index_face_z] = v_faces_z[index_face_z] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 2);
	}	  	  	  	  
      }
    }
  }


    /* w_faces_x */
      
#pragma omp parallel for default(none) private(J, K, i, j, k, index_face_x, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x, vertex_y, vertex_z_w, w_faces_x, w_cut_face_x, poly_coeffs, curve_is_solid_OK)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){

	index_face_x = k*nx*Ny+ J*nx + i;

	if(w_cut_face_x[index_face_x]){

	  curv_ind = w_cut_face_x[index_face_x] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	    

	  j = J-1;
	  K = k+1;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z_w[K-1];

	  x1[0] = vertex_x[i];
	  x1[1] = vertex_y[j+1];
	  x1[2] = vertex_z_w[K];
	    
	  w_faces_x[index_face_x] = w_faces_x[index_face_x] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 0);
	}	  	  	  	  
      }
    }
  }

  /* w_faces_y */
  
#pragma omp parallel for default(none) private(I, K, i, j, k, index_face_y, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x, vertex_y, vertex_z_w, w_faces_y, w_cut_face_y, poly_coeffs, curve_is_solid_OK)
  
  for(k=0; k<nz; k++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){

	index_face_y = k*Nx*ny+ j*Nx + I;

	if(w_cut_face_y[index_face_y]){

	  curv_ind = w_cut_face_y[index_face_y] - 1;
	  
	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	    

	  i = I-1;
	  K = k+1;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z_w[K-1];
	    
	  x1[0] = vertex_x[i+1];
	  x1[1] = vertex_y[j];
	  x1[2] = vertex_z_w[K];
	    
	  w_faces_y[index_face_y] = w_faces_y[index_face_y] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 1);
	}	    
      }
    }
  }


  /* w_faces_z */
      
#pragma omp parallel for default(none) private(I, J, K, i, j, index_face_z, x0, x1, pc_temp, \
					       curve_is_solid_flag, curv_ind, coeffs_ind) \
  shared(vertex_x, vertex_y, vertex_z_w, w_faces_z, w_cut_face_z, poly_coeffs, curve_is_solid_OK)
  
  for(K=1; K<Nz; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_face_z = K*Nx*Ny+ J*Nx + I;

	if(w_cut_face_z[index_face_z]){

	  curv_ind = w_cut_face_z[index_face_z] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];	    

	  i = I-1;
	  j = J-1;
	  
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z_w[K];

	  x1[0] = vertex_x[i+1];
	  x1[1] = vertex_y[j+1];
	  x1[2] = vertex_z_w[K];
	    
	  w_faces_z[index_face_z] = w_faces_z[index_face_z] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 2);
	}	    	  
      }
    }
  }

    
  return 0;
}

/* FACE_AREA_P_CELLS */
/*****************************************************************************/
/**

  Sets:

  Array (double, nxNyNz) p_faces_x: area of p cell perpendicular to x axis
  in fluid domain.

  Array (double, NxnyNz) p_faces_y: area of p cell perpendicular to y axis
  in fluid domain.

  Array (double, NxNynz) p_faces_z: area of p cell perpendicular to z axis
  in fluid domain.

  Array (int, nxNyNz) u_dom_matrix: set u cells that are inside curve to curv_ind + 1.

  Array (int, NxnyNz) v_dom_matrix: set v cells that are inside curve to curv_ind + 1.

  Array (int, NxNynz) w_dom_matrix: set w cells that are inside curve to curv_ind + 1.   

  Array (double, nxNyNz) nodes_y_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) nodes_z_u: modify values corresponding to u displaced nodes.

  Array (double, NxnyNz) nodes_x_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) nodes_z_v: modify values correspondinf to v displaced nodes.

  Array (double, NxNynz) nodes_x_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) nodes_y_w: modify valyes corresponding to w displaced nodes.

  Array (double, nxNyNz) Delta_yN_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) Delta_yS_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) Delta_zT_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) Delta_zB_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) Delta_yn_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) Delta_ys_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) Delta_zt_u: modify values corresponding to u displaced nodes.

  Array (double, nxNyNz) Delta_zb_u: modify values corresponding to u displaced nodes.

  Array (double, NxnyNz) Delta_xE_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) Delta_xW_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) Delta_zT_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) Delta_zB_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) Delta_xe_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) Delta_xw_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) Delta_zt_v: modify values corresponding to v displaced nodes.

  Array (double, NxnyNz) Delta_zb_v: modify values corresponding to v displaced nodes.

  Array (double, NxNynz) Delta_xE_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) Delta_xW_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) Delta_yN_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) Delta_yS_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) Delta_xe_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) Delta_xw_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) Delta_yn_w: modify values corresponding to w displaced nodes.

  Array (double, NxNynz) Delta_ys_w: modify values corresponding to w displaced nodes.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 01-08-2019
*/  
int face_area_p_cells(Data_Mem *data){

  int I, J, K;
  int i, j, k;

  int index_, index_face;
  int curv_ind;
  int index_face_x, index_face_y, index_face_z;
  int curve_is_solid_flag;
  int coeffs_ind;
  int index_u, index_v, index_w;
  int fluid_nb_OK, fluid_nb_W_OK, fluid_nb_E_OK;
  int fluid_nb_S_OK, fluid_nb_N_OK, fluid_nb_B_OK;
  int fluid_nb_T_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int curves = data->curves;

  int *u_dom_matrix = data->u_dom_matrix;
  int *v_dom_matrix = data->v_dom_matrix;
  int *w_dom_matrix = data->w_dom_matrix;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;
  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;
  const int *p_cut_face_z = data->p_cut_face_z;

  double curve_factor;
  double f0, f1, f2, f3;
  
  double pc_temp[16];
  double x_test[3];
  double x0[3];
  double x1[3];
  double x_centroid[3];

  double *p_faces_x = data->p_faces_x;
  double *p_faces_y = data->p_faces_y;
  double *p_faces_z = data->p_faces_z;

  double *nodes_y_u = data->nodes_y_u;
  double *nodes_z_u = data->nodes_z_u;
  double *nodes_x_v = data->nodes_x_v;
  double *nodes_z_v = data->nodes_z_v;
  double *nodes_x_w = data->nodes_x_w;
  double *nodes_y_w = data->nodes_y_w;

  double *Delta_yN_u = data->Delta_yN_u;
  double *Delta_yS_u = data->Delta_yS_u;
  double *Delta_zT_u = data->Delta_zT_u;
  double *Delta_zB_u = data->Delta_zB_u;
  double *Delta_yn_u = data->Delta_yn_u;
  double *Delta_ys_u = data->Delta_ys_u;
  double *Delta_zt_u = data->Delta_zt_u;
  double *Delta_zb_u = data->Delta_zb_u;
  double *Delta_xE_v = data->Delta_xE_v;
  double *Delta_xW_v = data->Delta_xW_v;
  double *Delta_zT_v = data->Delta_zT_v;
  double *Delta_zB_v = data->Delta_zB_v;
  double *Delta_xe_v = data->Delta_xe_v;
  double *Delta_xw_v = data->Delta_xw_v;
  double *Delta_zt_v = data->Delta_zt_v;
  double *Delta_zb_v = data->Delta_zb_v;
  double *Delta_xE_w = data->Delta_xE_w;
  double *Delta_xW_w = data->Delta_xW_w;
  double *Delta_yN_w = data->Delta_yN_w;
  double *Delta_yS_w = data->Delta_yS_w;
  double *Delta_xe_w = data->Delta_xe_w;
  double *Delta_xw_w = data->Delta_xw_w;
  double *Delta_yn_w = data->Delta_yn_w;
  double *Delta_ys_w = data->Delta_ys_w;
  
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;
  const double *poly_coeffs = data->poly_coeffs;

  /* Initialization of areas */

  /* p_faces_x */
  
#pragma omp parallel for default(none) private(I, J, K, i, index_, index_face) \
  shared(p_faces_x, Delta_y, Delta_z)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	I = i + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_face = K*nx*Ny + J*nx + i;
	p_faces_x[index_face] = Delta_y[index_] * Delta_z[index_];	
      }
    }
  }

  /* p_faces_y */

#pragma omp parallel for default(none) private(I, J, K, j, index_, index_face) \
  shared(p_faces_y, Delta_x, Delta_z)
  
  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	J = j + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_face = K*Nx*ny + j*Nx + I;
	p_faces_y[index_face] = Delta_x[index_] * Delta_z[index_];
      }
    }
  }

  /* p_faces_z */
  
#pragma omp parallel for default(none) private(I, J, K, k, index_, index_face) \
  shared(p_faces_z, Delta_x, Delta_y)

  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	K = k + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_face = k*Nx*Ny + J*Nx + I;
	p_faces_z[index_face] = Delta_x[index_] * Delta_y[index_];
      }
    }
  }

  /* Set area of internal faces to zero */

  /* p_faces_x */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(J, K, i, j, k, index_face_x, index_u, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y, vertex_z, pc_temp, curve_factor, p_faces_x, curv_ind, u_dom_matrix)
  
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=0; i<nx; i++){
	  
	  j = J-1;
	  k = K-1;
	  index_face_x = K*nx*Ny + J*nx + i;
	  index_u = K*nx*Ny + J*nx + i;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[1] = vertex_y[j+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[2] = vertex_z[k+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[1] = vertex_y[j];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    p_faces_x[index_face_x] = 0;
	    u_dom_matrix[index_u] = curv_ind + 1;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* p_faces_y */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, K, i, j, k, index_face_y, index_v, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y, vertex_z, pc_temp, curve_factor, p_faces_y, curv_ind, v_dom_matrix)
  
    for(K=1; K<Nz-1; K++){
      for(j=0; j<ny; j++){
	for(I=1; I<Nx-1; I++){
	  
	  i = I-1;
	  k = K-1;
	  index_face_y = K*Nx*ny + j*Nx + I;
	  index_v = K*Nx*ny + j*Nx + I;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x[i+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[2] = vertex_z[k+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x[i];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    p_faces_y[index_face_y] = 0;
	    v_dom_matrix[index_v] = curv_ind + 1;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* p_faces_z */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
  
#pragma omp parallel for default(none) private(I, J, i, j, k, index_face_z, index_w, x_test, f0, f1, f2, f3) \
  shared(vertex_x, vertex_y, vertex_z, pc_temp, curve_factor, p_faces_z, curv_ind, w_dom_matrix)
  
    for(k=0; k<nz; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  
	  i = I-1;
	  j = J-1;
	  index_face_z = k*Nx*Ny + J*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
	
	  x_test[0] = vertex_x[i];
	  x_test[1] = vertex_y[j];
	  x_test[2] = vertex_z[k];
	  f0 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[0] = vertex_x[i+1];
	  f1 = polyn(x_test, pc_temp) * curve_factor;

	  x_test[1] = vertex_y[j+1];
	  f2 = polyn(x_test, pc_temp) * curve_factor;
	
	  x_test[0] = vertex_x[i];
	  f3 = polyn(x_test, pc_temp) * curve_factor;

	  /* All face corners are located inside the curve */
	  if((f0<0) && (f1<0) && (f2<0) && (f3<0)){
	    p_faces_z[index_face_z] = 0;
	    w_dom_matrix[index_w] = curv_ind + 1;
	  }
	}
      }
    }
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* Area of cut faces */

  /* p_faces_x */
  
#pragma omp parallel for default(none) private(J, K, i, j, k, index_face_x, index_u, x0, x1, x_centroid, \
					       curv_ind, pc_temp, curve_is_solid_flag, coeffs_ind, \
					       fluid_nb_W_OK, fluid_nb_E_OK, fluid_nb_OK) \
  shared(vertex_x, vertex_y, vertex_z, p_faces_x, p_cut_face_x, poly_coeffs, curve_is_solid_OK, \
	 u_dom_matrix, nodes_y_u, nodes_z_u)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){

	index_face_x = K*nx*Ny+ J*nx + i;

	if(p_cut_face_x[index_face_x] != 0){

	  curv_ind = p_cut_face_x[index_face_x] - 1;
    
	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }
	  
	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];
	  
	  j = J-1;
	  k = K-1;
	
	  x0[0] = vertex_x[i];
	  x0[1] = (J>0)? vertex_y[j] : vertex_y[0];
	  x0[2] = (K>0)? vertex_z[k] : vertex_z[0];
	  	 
	  x1[0] = x0[0];
	  x1[1] = (J<Ny-1)? vertex_y[j+1] : vertex_y[ny-1];
	  x1[2] = (K<Nz-1)? vertex_z[k+1] : vertex_z[nz-1];

	  /* Set area of p cell */
	  p_faces_x[index_face_x] = p_faces_x[index_face_x] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 0);

	  /* Displace u velocity node */
	  index_u = K*nx*Ny + J*nx + i;
	  
	  fluid_nb_W_OK = (i>0)?  (u_dom_matrix[index_u-1] == 0) : 0;
	  fluid_nb_E_OK = (i<nx-1)? (u_dom_matrix[index_u+1] == 0) : 0;

	  fluid_nb_OK = fluid_nb_W_OK || fluid_nb_E_OK || (u_dom_matrix[index_u-nx] == 0) ||
	    (u_dom_matrix[index_u+nx] == 0) || (u_dom_matrix[index_u-nx*Ny] == 0) ||
	    (u_dom_matrix[index_u+nx*Ny] == 0);

	  if(fluid_nb_OK){
	    u_dom_matrix[index_u] = 0;
	    face_centroid(x_centroid, x0, x1, pc_temp, curve_is_solid_flag, 0);	  
	    nodes_y_u[index_u] = x_centroid[1];
	    nodes_z_u[index_u] = x_centroid[2];
	  }

	}	  
      }
    }
  }

  /* p_faces_y */

#pragma omp parallel for default(none) private(I, K, i, j, k, index_face_y, index_v, x0, x1, x_centroid, \
					       curv_ind, pc_temp, curve_is_solid_flag, coeffs_ind, \
					       fluid_nb_S_OK, fluid_nb_N_OK, fluid_nb_OK) \
  shared(vertex_x, vertex_y, vertex_z, p_faces_y, p_cut_face_y, poly_coeffs, curve_is_solid_OK, \
	 v_dom_matrix, nodes_x_v, nodes_z_v)
  
  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){

	index_face_y = K*Nx*ny+ j*Nx + I;

	if(p_cut_face_y[index_face_y] != 0){

	  curv_ind = p_cut_face_y[index_face_y] - 1;
    
	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];
	  
	  i = I-1;
	  k = K-1;
	
	  x0[0] = (I>0)? vertex_x[i] : vertex_x[0];
	  x0[1] = vertex_y[j];
	  x0[2] = (K>0)? vertex_z[k] : vertex_z[0];
	  	 
	  x1[0] = (I<Nx-1)? vertex_x[i+1] : vertex_x[nx-1];
	  x1[1] = x0[1];
	  x1[2] = (K<Nz-1)? vertex_z[k+1] : vertex_z[nz-1];

	  /* Set area of p cell */
	  p_faces_y[index_face_y] = p_faces_y[index_face_y] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 1);

	  /* Displace v velocity node */
	  index_v = K*Nx*ny + j*Nx + I;
	  
	  fluid_nb_S_OK = (j>0)? (v_dom_matrix[index_v-Nx] == 0) : 0;
	  fluid_nb_N_OK = (j<ny-1)? (v_dom_matrix[index_v+Nx] == 0) : 0;

	  fluid_nb_OK = fluid_nb_S_OK || fluid_nb_N_OK || (v_dom_matrix[index_v-1] == 0) ||
	    (v_dom_matrix[index_v+1] == 0) || (v_dom_matrix[index_v-Nx*ny] == 0) ||
	    (v_dom_matrix[index_v+Nx*ny] == 0);

	  if(fluid_nb_OK){
	    v_dom_matrix[index_v] = 0;
	    face_centroid(x_centroid, x0, x1, pc_temp, curve_is_solid_flag, 1);
	    nodes_x_v[index_v] = x_centroid[0];
	    nodes_z_v[index_v] = x_centroid[2];
	  }
	}	  
      }
    }
  }

  /* p_faces_z */

#pragma omp parallel for default(none) private(I, J, i, j, k, index_face_z, index_w, x0, x1, x_centroid, \
					       curv_ind, pc_temp, curve_is_solid_flag, coeffs_ind, \
					       fluid_nb_B_OK, fluid_nb_T_OK, fluid_nb_OK) \
  shared(vertex_x, vertex_y, vertex_z, p_faces_z, p_cut_face_z, poly_coeffs, curve_is_solid_OK, \
	 w_dom_matrix, nodes_x_w, nodes_y_w)
  
  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){

	index_face_z = k*Nx*Ny+ J*Nx + I;

	if(p_cut_face_z[index_face_z] != 0){

	  curv_ind = p_cut_face_z[index_face_z] - 1;
    
	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }

	  curve_is_solid_flag = curve_is_solid_OK[curv_ind];
	  
	  i = I-1;
	  j = J-1;
	
	  x0[0] = (I>0)? vertex_x[i] : vertex_x[0];
	  x0[1] = (J>0)? vertex_y[j] : vertex_y[0];
	  x0[2] = vertex_z[k];
	  	 
	  x1[0] = (I<Nx-1)? vertex_x[i+1] : vertex_x[nx-1];
	  x1[1] = (J<Ny-1)? vertex_y[j+1] : vertex_y[ny-1];
	  x1[2] = x0[2];

	  /* Set area of p cell */
	  p_faces_z[index_face_z] = p_faces_z[index_face_z] *
	    area_fraction_pol(x0, x1, pc_temp, curve_is_solid_flag, 2);

	  /* Displace w velocity node */
	  index_w = k*Nx*Ny + J*Nx + I;
	  
	  fluid_nb_B_OK = (k>0)? (w_dom_matrix[index_w-Nx*Ny] == 0) : 0;
	  fluid_nb_T_OK = (k<nz-1)? (w_dom_matrix[index_w+Nx*Ny] == 0) : 0;

	  fluid_nb_OK = fluid_nb_B_OK || fluid_nb_T_OK || (w_dom_matrix[index_w-1] == 0) ||
	    (w_dom_matrix[index_w+1] == 0) || (w_dom_matrix[index_w-Nx] == 0) ||
	    (w_dom_matrix[index_w+Nx] == 0);

	  if(fluid_nb_OK){
	    w_dom_matrix[index_w] = 0;
	    face_centroid(x_centroid, x0, x1, pc_temp, curve_is_solid_flag, 2);	  
	    nodes_x_w[index_w] = x_centroid[0];
	    nodes_y_w[index_w] = x_centroid[1];
	  }
	}	  
      }
    }
  }  


  /* Updating deltas for u nodes */
  
#pragma omp parallel for default(none) private(i, j, k, J, K, index_u)	\
  shared(p_cut_face_x, Delta_yN_u, Delta_yS_u, Delta_zT_u, Delta_zB_u, nodes_y_u, \
	 nodes_z_u, vertex_y, vertex_z, Delta_yn_u ,Delta_ys_u, Delta_zt_u, Delta_zb_u)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){

	index_u = K*nx*Ny + J*nx + i;
	j = J - 1;
	k = K - 1;

	if(p_cut_face_x[index_u] != 0){
#pragma omp critical
	  {
	    Delta_yN_u[index_u] = nodes_y_u[index_u+nx] - nodes_y_u[index_u];
	    Delta_yS_u[index_u+nx] = Delta_yN_u[index_u];
	    Delta_yS_u[index_u] = nodes_y_u[index_u] - nodes_y_u[index_u-nx];
	    Delta_yN_u[index_u-nx] = Delta_yS_u[index_u];
	    Delta_zT_u[index_u] = nodes_z_u[index_u+nx*Ny] - nodes_z_u[index_u];
	    Delta_zB_u[index_u+nx*Ny] = Delta_zT_u[index_u];
	    Delta_zB_u[index_u] = nodes_z_u[index_u] - nodes_z_u[index_u-nx*Ny];
	    Delta_zT_u[index_u-nx*Ny] = Delta_zB_u[index_u];
	  }
	  Delta_yn_u[index_u] = vertex_y[j+1] - nodes_y_u[index_u];
	  Delta_ys_u[index_u] = nodes_y_u[index_u] - vertex_y[j];
	  Delta_zt_u[index_u] = vertex_z[k+1] - nodes_z_u[index_u];
	  Delta_zb_u[index_u] = nodes_z_u[index_u] - vertex_z[k];	    
	}
      }
    }
  }

  /* Updating deltas for v nodes */
  
#pragma omp parallel for default(none) private(i, j, k, I, K, index_v)	\
  shared(p_cut_face_y, Delta_xE_v, Delta_xW_v, Delta_zT_v, Delta_zB_v, nodes_x_v, \
	 nodes_z_v, vertex_x, vertex_z, Delta_xe_v, Delta_xw_v, Delta_zt_v, Delta_zb_v)
  
  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){

	index_v = K*Nx*ny + j*Nx + I;
	i = I - 1;
	k = K - 1;

	if(p_cut_face_y[index_v] != 0){
#pragma omp critical
	  {
	    Delta_xE_v[index_v] = nodes_x_v[index_v+1] - nodes_x_v[index_v];
	    Delta_xW_v[index_v+1] = Delta_xE_v[index_v];
	    Delta_xW_v[index_v] = nodes_x_v[index_v] - nodes_x_v[index_v-1];
	    Delta_xE_v[index_v-1] = Delta_xW_v[index_v];
	    Delta_zT_v[index_v] = nodes_z_v[index_v+Nx*ny] - nodes_z_v[index_v];
	    Delta_zB_v[index_v+Nx*ny] = Delta_zT_v[index_v];
	    Delta_zB_v[index_v] = nodes_z_v[index_v] - nodes_z_v[index_v-Nx*ny];
	    Delta_zT_v[index_v-Nx*ny] = Delta_zB_v[index_v];
	  }
	  Delta_xe_v[index_v] = vertex_x[i+1] - nodes_x_v[index_v];
	  Delta_xw_v[index_v] = nodes_x_v[index_v] - vertex_x[i];
	  Delta_zt_v[index_v] = vertex_z[k+1] - nodes_z_v[index_v];
	  Delta_zb_v[index_v] = nodes_z_v[index_v] - vertex_z[k];
	}
      }
    }
  }

  /* Updating deltas for w nodes */

#pragma omp parallel for default(none) private(i, j, k, I, J, index_w) \
  shared(p_cut_face_z, Delta_xE_w, Delta_xW_w, Delta_yN_w, Delta_yS_w, nodes_x_w, \
	 nodes_y_w, vertex_x, vertex_y, Delta_xe_w, Delta_xw_w, Delta_yn_w, Delta_ys_w)

  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_w = k*Nx*Ny + J*Nx + I;
	i = I - 1;
	j = J - 1;

	if(p_cut_face_z[index_w] != 0){
#pragma omp critical
	  {
	    Delta_xE_w[index_w] = nodes_x_w[index_w+1] - nodes_x_w[index_w];
	    Delta_xW_w[index_w+1] = Delta_xE_w[index_w];
	    Delta_xW_w[index_w] = nodes_x_w[index_w] - nodes_x_w[index_w-1];
	    Delta_xE_w[index_w-1] = Delta_xW_w[index_w];
	    Delta_yN_w[index_w] = nodes_y_w[index_w+Nx] - nodes_y_w[index_w];
	    Delta_yS_w[index_w+Nx] = Delta_yN_w[index_w];
	    Delta_yS_w[index_w] = nodes_y_w[index_w] - nodes_y_w[index_w-Nx];
	    Delta_yN_w[index_w-Nx] = Delta_yS_w[index_w];	    
	  }
	  Delta_xe_w[index_w] = vertex_x[i+1] - nodes_x_w[index_w];
	  Delta_xw_w[index_w] = nodes_x_w[index_w] - vertex_x[i];
	  Delta_yn_w[index_w] = vertex_y[j+1] - nodes_y_w[index_w];
	  Delta_ys_w[index_w] = nodes_y_w[index_w] - vertex_y[j];
	}
      }
    }
  }  
  
  return 0;
}

/* SET_CUT_FACES_P_CELLS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNyNz) p_cut_face_x: equal to (curv_ind + 1) if p_face_x is cut.

  Array (double, NxnyNz) p_cut_face_y: equal to (curv_ind + 1) if p_face_y is cut.

  Array (double, NxNynz) p_cut_face_z: equal to (curv_ind + 1) if p_face_z is cut.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 29-07-2019
*/  
int set_cut_faces_p_cells(Data_Mem *data){

  int i, j, k;
  int I, J, K;
  int vertex_cut;
  int index_face_x;
  int index_face_y;
  int index_face_z;
  int curv_ind;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int curves = data->curves;

  int *p_cut_face_x = data->p_cut_face_x;
  int *p_cut_face_y = data->p_cut_face_y;
  int *p_cut_face_z = data->p_cut_face_z;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double f0, f1;
  double curve_factor;

  double pc_temp[16];
  double x0[3] = {0};
  double x1[3] = {0};
  double x_middle[3] = {0};
  const double pol_tol = data->pol_tol;
  
  const double *poly_coeffs = data->poly_coeffs;
  
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;

  /* Determination of cut faces */

  /* Edges parallel to x axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(i, j, k, I, J, K, x0, x1, f0, f1, vertex_cut, \
					       x_middle, index_face_y, index_face_z) \
  shared(vertex_x, vertex_y, vertex_z, curve_factor, pc_temp, p_cut_face_y, p_cut_face_z, curv_ind)
    
    for(k=0; k<nz; k++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx-1; i++){
	  vertex_cut = 0;
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];
	  x1[0] = vertex_x[i+1];
	  x1[1] = x0[1];
	  x1[2] = x0[2];

	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;	  
	  }
	  
	  if(vertex_cut){
	    I = i+1;
	    J = j+1;
	    K = k+1;
	    index_face_y = K*Nx*ny + j*Nx + I;
	    index_face_z = k*Nx*Ny + J*Nx + I;
#pragma omp critical
	    {
	      p_cut_face_y[index_face_y] = curv_ind + 1;
	      p_cut_face_y[index_face_y-Nx*ny] = curv_ind + 1;
	      p_cut_face_z[index_face_z] = curv_ind + 1;
	      p_cut_face_z[index_face_z-Nx] = curv_ind + 1;
	    }
	  }	
	}
      }
    }

  } // for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* Edges parallel to y axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(i, j, k, I, J, K, x0, x1, f0, f1, vertex_cut, \
					       x_middle, index_face_x, index_face_z) \
  shared(vertex_x, vertex_y, vertex_z, curve_factor, pc_temp, p_cut_face_x, p_cut_face_z, curv_ind)    

    for(k=0; k<nz; k++){
      for(j=0; j<ny-1; j++){
	for(i=0; i<nx; i++){
	  vertex_cut = 0;
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];
	  x1[0] = x0[0];
	  x1[1] = vertex_y[j+1];
	  x1[2] = x0[2];

	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;	  
	  }
	  
	  if(vertex_cut){
	    I = i+1;
	    J = j+1;
	    K = k+1;
	    index_face_x = K*nx*Ny + J*nx + i;
	    index_face_z = k*Nx*Ny + J*Nx + I;
#pragma omp critical
	    {
	      p_cut_face_x[index_face_x] = curv_ind + 1;
	      p_cut_face_x[index_face_x-nx*Ny] = curv_ind + 1;
	      p_cut_face_z[index_face_z] = curv_ind + 1;
	      p_cut_face_z[index_face_z-1] = curv_ind + 1;
	    }
	  }	
	}
      }
    }

  } // for(curv_ind=0; curv_ind<curves; curv_ind++)

  /* Edges parallel to z axis */

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    
    for(i=0; i<16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(i, j, k, I, J, K, x0, x1, f0, f1, vertex_cut, \
					       x_middle, index_face_x, index_face_y) \
  shared(vertex_x, vertex_y, vertex_z, curve_factor, pc_temp, p_cut_face_x, p_cut_face_y, curv_ind)    

    for(k=0; k<nz-1; k++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){
	  vertex_cut = 0;
	  x0[0] = vertex_x[i];
	  x0[1] = vertex_y[j];
	  x0[2] = vertex_z[k];
	  x1[0] = x0[0];
	  x1[1] = x0[1];
	  x1[2] = vertex_z[k+1];

	  f0 = polyn(x0, pc_temp) * curve_factor;
	  f1 = polyn(x1, pc_temp) * curve_factor;

	  if((fabs(f0) < pol_tol) && (fabs(f1) < pol_tol)){
	    avg_vec(x_middle, x0, x1);
	    if(polyn(x_middle, pc_temp) * curve_factor < 0){ 
	      vertex_cut = 1;
	    }
	  }
	  else if((fabs(f0) < pol_tol) && (f1 < 0)){
	    vertex_cut = 1;
	  }
	  else if((fabs(f1) < pol_tol) && (f0 < 0)){
	    vertex_cut = 1;
	  }
	  else if(f0*f1 < 0){
	    vertex_cut = 1;	  
	  }
	  
	  if(vertex_cut){
	    I = i+1;
	    J = j+1;
	    K = k+1;
	    index_face_x = K*nx*Ny + J*nx + i;
	    index_face_y = K*Nx*ny + j*Nx + I;
#pragma omp critical
	    {
	      p_cut_face_x[index_face_x] = curv_ind + 1;
	      p_cut_face_x[index_face_x-nx] = curv_ind + 1;
	      p_cut_face_y[index_face_y] = curv_ind + 1;
	      p_cut_face_y[index_face_y-1] = curv_ind + 1;
	    }
	  }	
	}
      }
    }

  } // for(curv_ind=0; curv_ind<curves; curv_ind++)
  
  return 0;
}

/* FIND_SMALL_PRESSURE_CELLS_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, NxNyNz) p_dom_maxtrix: set to (curv_ind + 1) for small pressure cells.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 02-08-2019
*/
void find_small_pressure_cells_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  int p_x_lower_OK, p_y_lower_OK, p_z_lower_OK;
  int u_x_lower_OK, v_y_lower_OK, w_z_lower_OK;
  int x_faces_small_OK, y_faces_small_OK, z_faces_small_OK;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;

  int *p_dom_matrix = data->p_dom_matrix;

  const int *cut_matrix = data->cut_matrix;

  double area_x, area_y, area_z;
  
  const double kappa = data->kappa;

  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;
  const double *p_faces_z = data->p_faces_z;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  const double *u_faces_x = data->u_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *w_faces_z = data->w_faces_z;

#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_u, index_v, index_w,		\
	  p_x_lower_OK, p_y_lower_OK, p_z_lower_OK,			\
	  u_x_lower_OK, v_y_lower_OK, w_z_lower_OK,			\
	  x_faces_small_OK, y_faces_small_OK, z_faces_small_OK,		\
	  area_x, area_y, area_z)					\
  shared(cut_matrix, p_dom_matrix, p_faces_x, p_faces_y, p_faces_z,	\
	 Delta_y, Delta_x, Delta_z, u_faces_x, v_faces_y, w_faces_z)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if((cut_matrix[index_]) && (p_dom_matrix[index_] == 0)){

	  i = I-1;
	  j = J-1;
	  k = K-1;
	  index_u = K*nx*Ny + J*nx + i;
	  index_v = K*Nx*ny + j*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
	  area_x = kappa * Delta_y[index_] * Delta_z[index_];
	  area_y = kappa * Delta_x[index_] * Delta_z[index_];
	  area_z = kappa * Delta_x[index_] * Delta_y[index_];
	  
	  /* Cell borders */
	  p_x_lower_OK = (p_faces_x[index_u] < area_x) && 
	    (p_faces_x[index_u+1] < area_x);
	  p_y_lower_OK = (p_faces_y[index_v] < area_y) &&
	    (p_faces_y[index_v+Nx] < area_y);
	  p_z_lower_OK = (p_faces_z[index_w] < area_z) &&
	    (p_faces_z[index_w+Nx*Ny] < area_z);
	
	  u_x_lower_OK = (u_faces_x[index_] < area_x);
	  v_y_lower_OK = (v_faces_y[index_] < area_y);
	  w_z_lower_OK = (w_faces_z[index_] < area_z);

	  x_faces_small_OK = (p_x_lower_OK || u_x_lower_OK);
	  y_faces_small_OK = (p_y_lower_OK || v_y_lower_OK);
	  z_faces_small_OK = (p_z_lower_OK || w_z_lower_OK);
	  
	  if(x_faces_small_OK && y_faces_small_OK && z_faces_small_OK){
	    // Do not compute since its enclosing x or y faces are too small
	    p_dom_matrix[index_] = cut_matrix[index_];
	  }	  
	}
      }
    }
  }

  return;
}

/* SET_SLAVE_NODES_3D */
/*****************************************************************************/
/**

  Sets:

  Array (int, u_slave_count) u_slave_nodes: u cells classified as slaves.

  Array (int, v_slave_count) v_slave_nodes: v cells classified as slaves. 

  Arrat (int, w_slave_count) w_slave_nodes: w cells classified as slaves.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 02-08-2019
*/
void set_slave_nodes_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  int count = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;
  
  const int *p_dom_matrix = data->p_dom_matrix;

  /* u slave velocity nodes */
  count = 0;

#pragma omp parallel for default(none) private(i, I, J, K, index_, index_u) \
  shared(u_dom_matrix, p_dom_matrix) reduction(+: count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){

	I = i + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;

	if(u_dom_matrix[index_u] == 0){
	  // Fluid domain u cell
	  if((p_dom_matrix[index_-1]) || (p_dom_matrix[index_])){
	    // Not having 2 pressure nodes to make the gradient in x direction
	    count++;
	  }
	}
      }
    }
  }

  if(count > 0){

    data->u_slave_count = count;
    // Saves index_u of nodes
    data->u_slave_nodes = create_array_int(count, 1, 1, 0);
    data->u_slave_at_E_of_p_OK = create_array_int(count, 1, 1, 0);
    data->u_slave_i = create_array_int(count, 1, 1, 0);
    data->u_slave_J = create_array_int(count, 1, 1, 0);
    data->u_slave_K = create_array_int(count, 1, 1, 0);
    
    int *u_slave_nodes = data->u_slave_nodes;
    int *u_slave_at_E_of_p_OK = data->u_slave_at_E_of_p_OK;
    int *u_slave_i = data->u_slave_i;
    int *u_slave_J = data->u_slave_J;
    int *u_slave_K = data->u_slave_K;

    count = 0; 
  
#pragma omp parallel for default(none) private(i, I, J, K, index_, index_u) \
  shared(u_dom_matrix, p_dom_matrix, u_slave_nodes, u_slave_at_E_of_p_OK, \
	 count, u_slave_i, u_slave_J, u_slave_K)

    for(K=1; K<Nz-1; K++){    
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
      
	  I = i + 1;      
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_u = K*nx*Ny + J*nx + i;
      
	  if(u_dom_matrix[index_u] == 0){
	
	    if((p_dom_matrix[index_-1]) || (p_dom_matrix[index_])){
	  
#pragma omp critical
	      {
		u_slave_nodes[count] = index_u;
		u_slave_i[count] = i;
		u_slave_J[count] = J;
		u_slave_K[count] = K;

		if(p_dom_matrix[index_]){
		  /* 1: u node is at east position relative to fluid pressure node */
		  u_slave_at_E_of_p_OK[count] = 1;
		}

		count++;	  
	      }
	    }
	  }
	}
      }
    }
  } //   if(count > 0)

  /* v slave velocity nodes */
  count = 0;

#pragma omp parallel for default(none) private(I, j, J, K, index_, index_v) \
  shared(v_dom_matrix, p_dom_matrix) reduction(+:count)

  for(K=1; K<Nz-1; K++){
    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){

	J = j + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_v = K*Nx*ny + j*Nx + I;

	if(v_dom_matrix[index_v] == 0){
	  if((p_dom_matrix[index_-Nx]) || (p_dom_matrix[index_])){
	    count++;
	  }
	}
      }
    }
  }

  if(count > 0){

    data->v_slave_count = count;
    data->v_slave_nodes = create_array_int(count, 1, 1, 0);
    data->v_slave_at_N_of_p_OK = create_array_int(count, 1, 1, 0);
    data->v_slave_I = create_array_int(count, 1, 1, 0);
    data->v_slave_j = create_array_int(count, 1, 1, 0);
    data->v_slave_K = create_array_int(count, 1, 1, 0);
    
    int *v_slave_nodes = data->v_slave_nodes;
    int *v_slave_at_N_of_p_OK = data->v_slave_at_N_of_p_OK;
    int *v_slave_I = data->v_slave_I;
    int *v_slave_j = data->v_slave_j;
    int *v_slave_K = data->v_slave_K;

    count = 0;

#pragma omp parallel for default(none) private(I, j, J, K, index_, index_v) \
  shared(v_dom_matrix, p_dom_matrix, v_slave_nodes, v_slave_at_N_of_p_OK, \
	 count, v_slave_I, v_slave_j, v_slave_K)

    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){

	  J = j + 1;
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_v = K*Nx*ny + j*Nx + I;

	  if(v_dom_matrix[index_v] == 0){
	    if((p_dom_matrix[index_-Nx]) || (p_dom_matrix[index_])){
#pragma omp critical
	      {
		v_slave_nodes[count] = index_v;
		v_slave_I[count] = I;
		v_slave_j[count] = j;
		v_slave_K[count] = K;

		if(p_dom_matrix[index_]){
		  /* 1: v node is at north position relative to fluid pressure node */
		  v_slave_at_N_of_p_OK[count] = 1;
		}
		count++;
	      }
	    }
	  }
	}
      }
    }
  } //   if(count > 0)

  /* w slave velocity nodes */
  count = 0;

#pragma omp parallel for default(none) private(I, J, K, k, index_, index_w) \
  shared(w_dom_matrix, p_dom_matrix) reduction(+:count)

  for(k=1; k<nz-1; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	K = k + 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	if(w_dom_matrix[index_w] == 0){
	  if((p_dom_matrix[index_-Nx*Ny]) || (p_dom_matrix[index_])){
	    count++;
	  }
	}
      }
    }
  }

  if(count > 0){

    data->w_slave_count = count;
    data->w_slave_nodes = create_array_int(count, 1, 1, 0);
    data->w_slave_at_T_of_p_OK = create_array_int(count, 1, 1, 0);
    data->w_slave_I = create_array_int(count, 1, 1, 0);
    data->w_slave_J = create_array_int(count, 1, 1, 0);
    data->w_slave_k = create_array_int(count, 1, 1, 0);
    
    int *w_slave_nodes = data->w_slave_nodes;
    int *w_slave_at_T_of_p_OK = data->w_slave_at_T_of_p_OK;
    int *w_slave_I = data->w_slave_I;
    int *w_slave_J = data->w_slave_J;
    int *w_slave_k = data->w_slave_k;

    count = 0;

#pragma omp parallel for default(none) private(I, J, K, k, index_, index_w) \
  shared(w_dom_matrix, p_dom_matrix, w_slave_nodes, w_slave_at_T_of_p_OK, \
	 count, w_slave_I, w_slave_J, w_slave_k)

    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  K = k + 1;
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;

	  if(w_dom_matrix[index_w] == 0){
	    if((p_dom_matrix[index_-Nx*Ny]) || (p_dom_matrix[index_])){
#pragma omp critical
	      {
		w_slave_nodes[count] = index_w;
		w_slave_I[count] = I;
		w_slave_J[count] = J;
		w_slave_k[count] = k;

		if(p_dom_matrix[index_]){
		  /* 1: v node is at north position relative to fluid pressure node */
		  w_slave_at_T_of_p_OK[count] = 1;
		}
		count++;
	      }
	    }
	  }
	}
      }
    }
  } //   if(count > 0)  

  return;
}

/* FLUID_FRACTION_V_CELLS_3D*/
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNyNz) vf_x : Solid fraction at u cells.

  Array (double, NxnyNz) vf_y : Solid fraction at v cells.

  Array (double, NxNynz) vf_z : Solid fraction at w cells.

  Array (double, nxNyNz) vol_x: Fluid volume at u cells.

  Array (double, NxnyNz) vol_y: Fluid volume at v cells.

  Array (double, NxNynz) vol_z: Fluid volume at w cells.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 12-08-2019
*/
int fluid_fraction_v_cells_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_u, index_v, index_w;
  int curv_ind, curve_is_solid;
  int coeffs_ind;
  int ret_value = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;
  const int *u_cut_matrix = data->u_cut_matrix;
  const int *v_cut_matrix = data->v_cut_matrix;
  const int *w_cut_matrix = data->w_cut_matrix;

  double pc_temp[16] = {0};
  double c0[3] = {0};
  double c1[3] = {0};

  double *vf_x = data->vf_x;
  double *vf_y = data->vf_y;
  double *vf_z = data->vf_z;
  double *vol_x = data->vol_x;
  double *vol_y = data->vol_y;
  double *vol_z = data->vol_z;

  const double *poly_coeffs = data->poly_coeffs;
  const double *Delta_x_u = data->Delta_x_u;
  const double *Delta_y_u = data->Delta_y_u;
  const double *Delta_z_u = data->Delta_z_u;
  const double *Delta_x_v = data->Delta_x_v;
  const double *Delta_y_v = data->Delta_y_v;
  const double *Delta_z_v = data->Delta_z_v;
  const double *Delta_x_w = data->Delta_x_w;
  const double *Delta_y_w = data->Delta_y_w;
  const double *Delta_z_w = data->Delta_z_w;
  const double *vertex_x_u = data->vertex_x_u;
  const double *vertex_y_v = data->vertex_y_v;
  const double *vertex_z_w = data->vertex_z_w;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;
  
  /* U */

  /* Default values */

#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(u_dom_matrix, vf_x, Delta_x_u, Delta_y_u, Delta_z_u, vol_x)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	
	index_u = K*nx*Ny + J*nx + i;

	if(u_dom_matrix[index_u] == 0){
	  vf_x[index_u] = 0;
	  vol_x[index_u] = Delta_x_u[index_u] * Delta_y_u[index_u] * Delta_z_u[index_u];
	}
	else{
	  vf_x[index_u] = 1;
	  vol_x[index_u] = 0;
	}
      }
    }
  }

  /* Cut cell values */

  
#pragma omp parallel for default(none) private(i, J, K, coeffs_ind, index_u, curve_is_solid, curv_ind, pc_temp, c0, c1) \
  shared(u_dom_matrix, vertex_x_u, vertex_y, vertex_z, u_cut_matrix, vf_x, Delta_x_u, \
	 Delta_y_u, Delta_z_u, vol_x, poly_coeffs, curve_is_solid_OK)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	index_u = K*nx*Ny + J*nx + i;
	
	if(u_cut_matrix[index_u]){

	  curv_ind = u_cut_matrix[index_u] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }
	  
	  curve_is_solid = curve_is_solid_OK[curv_ind];	  
	  
	  c0[0] = vertex_x_u[i];
	  c0[1] = vertex_y[J-1];
	  c0[2] = vertex_z[K-1];
	  c1[0] = vertex_x_u[i+1];
	  c1[1] = vertex_y[J];
	  c1[2] = vertex_z[K];
	  vf_x[index_u] = vol_fraction_pol_3D(c0, c1, pc_temp, curve_is_solid);
	  vol_x[index_u] = Delta_x_u[index_u] * Delta_y_u[index_u] * Delta_z_u[index_u] * (1 - vf_x[index_u]);
	}
      }
    }
  }
  

  /* V */

  /* Default values */

#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(v_dom_matrix, vf_y, Delta_x_v, Delta_y_v, Delta_z_v, vol_y)

  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	
	index_v = K*Nx*ny + j*Nx + I;

	if(v_dom_matrix[index_v] == 0){
	  vf_y[index_v] = 0;
	  vol_y[index_v] = Delta_x_v[index_v] * Delta_y_v[index_v] * Delta_z_v[index_v];
	}
	else{
	  vf_y[index_v] = 1;
	  vol_y[index_v] = 0;
	}
      }
    }
  }

  /* Cut cell values */

#pragma omp parallel for default(none) private(I, j, K, coeffs_ind, index_v, curve_is_solid, curv_ind, pc_temp, c0, c1) \
  shared(v_dom_matrix, vertex_x, vertex_y_v, vertex_z, v_cut_matrix, vf_y, Delta_x_v, \
	 Delta_y_v, Delta_z_v, vol_y, poly_coeffs, curve_is_solid_OK)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
	
	index_v = K*Nx*ny + j*Nx + I;

	if(v_cut_matrix[index_v]){

	  curv_ind = v_cut_matrix[index_v] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }
	  curve_is_solid = curve_is_solid_OK[curv_ind];
	  
	  c0[0] = vertex_x[I-1];
	  c0[1] = vertex_y_v[j];
	  c0[2] = vertex_z[K-1];
	  c1[0] = vertex_x[I];
	  c1[1] = vertex_y_v[j+1];
	  c1[2] = vertex_z[K];
	  vf_y[index_v] = vol_fraction_pol_3D(c0, c1, pc_temp, curve_is_solid);
	  vol_y[index_v] = Delta_x_v[index_v] * Delta_y_v[index_v] * Delta_z_v[index_v] * (1 - vf_y[index_v]);
	}
      }
    }
  }

  /* W */

  /* Default values */

#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(w_dom_matrix, vf_z, Delta_x_w, Delta_y_w, Delta_z_w, vol_z)

  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_w = k*Nx*Ny + J*Nx + I;

	if(w_dom_matrix[index_w] == 0){
	  vf_z[index_w] = 0;
	  vol_z[index_w] = Delta_x_w[index_w] * Delta_y_w[index_w] * Delta_z_w[index_w];
	}
	else{
	  vf_z[index_w] = 1;
	  vol_z[index_w] = 0;
	}
      }
    }
  }

  /* Cut cell values */

#pragma omp parallel for default(none) private(I, J, k, coeffs_ind, index_w, curve_is_solid, curv_ind, pc_temp, c0, c1) \
  shared(w_dom_matrix, vertex_x, vertex_y, vertex_z_w, w_cut_matrix, vf_z, Delta_x_w, \
	 Delta_y_w, Delta_z_w, vol_z, poly_coeffs, curve_is_solid_OK)

  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_w = k*Nx*Ny + J*Nx + I;

	if(w_cut_matrix[index_w]){

	  curv_ind = w_cut_matrix[index_w] - 1;

	  for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	    pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	  }
	  curve_is_solid = curve_is_solid_OK[curv_ind];
	  
	  c0[0] = vertex_x[I-1];
	  c0[1] = vertex_y[J-1];
	  c0[2] = vertex_z_w[k];
	  c1[0] = vertex_x[I];
	  c1[1] = vertex_y[J];
	  c1[2] = vertex_z_w[k+1];
	  vf_z[index_w] = vol_fraction_pol_3D(c0, c1, pc_temp, curve_is_solid);
	  vol_z[index_w] = Delta_x_w[index_w] * Delta_y_w[index_w] * Delta_z_w[index_w] * (1 - vf_z[index_w]);
	}
      }
    }
  }
  
  return ret_value;
}

/* P_NODES_IN_SOLID_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, p_nodes_in_solid_count) p_nodes_in_solid_indx: pressure cells 
  considered fluid on p_dom_matrix and solid on domain_matrix

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 13-08-2019
*/
void p_nodes_in_solid_3D(Data_Mem *data){

  int I, J, K, index_;
  int p_nodes_in_solid_count = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *dom_matrix = data->domain_matrix;
  const int *p_dom_matrix = data->p_dom_matrix;


#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(p_dom_matrix, dom_matrix) reduction(+: p_nodes_in_solid_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = K*Nx*Ny + J*Nx + I;

	if((p_dom_matrix[index_] == 0) && (dom_matrix[index_])){
	  p_nodes_in_solid_count++;
	}
      }
    }
  }

  if(p_nodes_in_solid_count > 0){
    
    data->p_nodes_in_solid_indx = create_array_int(p_nodes_in_solid_count, 1, 1, 0);
    data->p_nodes_in_solid_count = p_nodes_in_solid_count;

    int *p_nodes_in_solid_indx = data->p_nodes_in_solid_indx;

    p_nodes_in_solid_count = 0;
    
#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(p_dom_matrix, dom_matrix, p_nodes_in_solid_indx, p_nodes_in_solid_count)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_ = K*Nx*Ny + J*Nx + I;

	  if((p_dom_matrix[index_] == 0) && (dom_matrix[index_])){
#pragma omp critical
	    {
	      p_nodes_in_solid_indx[p_nodes_in_solid_count] = index_;
	      p_nodes_in_solid_count++;
	    }
	  }
	}
      }
    }
  } //   if(p_nodes_in_solid_count > 0)


  return;
}

/* COMPUTE_SOLID_FACTORS_3D */
/*****************************************************************************/
/**

  Sets:

  Array (double, u_sol_factor_count) u_sol_factor: Factor due to stress on solid
  boundary on u cells.

  Array (double, u_sol_factor_count) u_p_factor: Factor due to pressure on solid
  boundary on u cells.

  Array (double, v_sol_factor_count) v_sol_factor: Factor due to stress on solid
  boundary on v cells.

  Array (double, v_sol_factor_count) v_p_factor: Factor due to pressure on solid
  boundary on v cells.

  Array (double, w_sol_factor_count) w_sol_factor: Factor due to stress on solid 
  boundary on w cells.

  Array (double, w_sol_factor_count) w_p_factor: Factor due to pressure on solid 
  boundary on w cells.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 16-08-2019
*/
int compute_solid_factors_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_, index_u, index_v, index_w;
  int curv_ind, coeffs_ind;
  int index_face_x, index_face_y, index_face_z;
  int compute_factors;

  int u_sol_factor_count = 0;
  int v_sol_factor_count = 0;
  int w_sol_factor_count = 0;
  int ret_value = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int curves = data->curves;

  const int *u_cut_matrix = data->u_cut_matrix;
  const int *v_cut_matrix = data->v_cut_matrix;
  const int *w_cut_matrix = data->w_cut_matrix;

  double delta_h;
  double area_x, area_y, area_z;
  double solid_area, max_area, avg_edge;
  
  double pc_temp[16];
  double norm_v[3] = {0};
  double fluid_norm_v[3] = {0};
  double x_vel_node[3] = {0};
  double x_test[3] = {0};
  
  const double cell_size = data->cell_size;
  const double solid_factor_tol = 1e10;
  /* Keep consistency with find_solid_area_function */
  const double min_area_crit = 0.001;

  const double *poly_coeffs = data->poly_coeffs;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;
  const double *nodes_x_w = data->nodes_x_w;
  const double *nodes_y_w = data->nodes_y_w;
  const double *nodes_z_w = data->nodes_z_w;  
  const double *Delta_x_u = data->Delta_x_u;
  const double *Delta_y_u = data->Delta_y_u;
  const double *Delta_z_u = data->Delta_z_u;
  const double *Delta_x_v = data->Delta_x_v;
  const double *Delta_y_v = data->Delta_y_v;
  const double *Delta_z_v = data->Delta_z_v;
  const double *Delta_x_w = data->Delta_x_w;
  const double *Delta_y_w = data->Delta_y_w;
  const double *Delta_z_w = data->Delta_z_w;  
  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  const double *u_faces_z = data->u_faces_z;
  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *v_faces_z = data->v_faces_z;
  const double *w_faces_x = data->w_faces_x;
  const double *w_faces_y = data->w_faces_y;
  const double *w_faces_z = data->w_faces_z;

  /* Factors corresponding to the presence of the solid boundary and associated forces */

  if(curves > 0){

#pragma omp parallel for default(none) private(i, J, K, index_u) shared(u_cut_matrix) \
  reduction(+: u_sol_factor_count)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){
	  
	  index_u = K*nx*Ny + J*nx + i;
	  
	  if(u_cut_matrix[index_u]){
	    u_sol_factor_count++;
	  }
	}
      }
    }
    
  } /* if(curves > 0) */

  
  if(u_sol_factor_count > 0){

    data->u_sol_factor = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_p_factor = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_index_u = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_index = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_i = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_J = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_K = create_array_int(u_sol_factor_count, 1, 1, 0);
    data->u_factor_delta_h = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_area = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_inters_x = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_inters_y = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_factor_inters_z = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_p_factor_W = create_array(u_sol_factor_count, 1, 1, 0);
    data->u_sol_factor_count = u_sol_factor_count;

    u_sol_factor_count = 0;

    double *u_sol_factor = data->u_sol_factor;
    double *u_p_factor = data->u_p_factor;
    double *u_factor_delta_h = data->u_factor_delta_h;
    double *u_factor_area = data->u_factor_area;
    double *u_factor_inters_x = data->u_factor_inters_x;
    double *u_factor_inters_y = data->u_factor_inters_y;
    double *u_factor_inters_z = data->u_factor_inters_z;
    double *u_p_factor_W = data->u_p_factor_W;
    int *u_sol_factor_index_u = data->u_sol_factor_index_u;
    int *u_sol_factor_index = data->u_sol_factor_index;
    int *u_sol_factor_i = data->u_sol_factor_i;
    int *u_sol_factor_J = data->u_sol_factor_J;
    int *u_sol_factor_K = data->u_sol_factor_K;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_, index_u, x_vel_node, curv_ind, coeffs_ind, \
					       pc_temp, delta_h, index_face_x, index_face_y, index_face_z, \
					       area_x, area_y, area_z, avg_edge, max_area, norm_v, solid_area, \
					       x_test, compute_factors, fluid_norm_v) \
  shared(u_cut_matrix, nodes_x_u, nodes_y_u, nodes_z_u, poly_coeffs, u_sol_factor, u_p_factor, u_factor_delta_h, \
	 u_factor_area, u_factor_inters_x, u_factor_inters_y, u_factor_inters_z, u_p_factor_W, u_sol_factor_index_u, \
	 u_sol_factor_index, u_sol_factor_i, u_sol_factor_J, u_sol_factor_K, u_sol_factor_count, u_faces_x, u_faces_y, \
	 u_faces_z, Delta_x_u, Delta_y_u, Delta_z_u, nodes_x)
    
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(i=1; i<nx-1; i++){ 

	  index_u = K*nx*Ny + J*nx + i;

	  if(u_cut_matrix[index_u]){

	    I = i + 1;
	    j = J - 1;
	    k = K - 1;

	    index_ = K*Nx*Ny + J*Nx + I;
	    
	    /* u node coordinates */

	    x_vel_node[0] = nodes_x_u[index_u];
	    x_vel_node[1] = nodes_y_u[index_u];
	    x_vel_node[2] = nodes_z_u[index_u];

	    /* Set polynomial coefficients */
	    curv_ind = u_cut_matrix[index_u] - 1;

	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    /* Distance from u node to the curve */
	    delta_h = calc_dist(x_vel_node, pc_temp, cell_size);

	    index_face_x = K*Nx*Ny + J*Nx + I;
	    index_face_y = K*nx*ny + j*nx + i;
	    index_face_z = k*nx*Ny + J*nx + i;

	    area_x = u_faces_x[index_face_x-1] - u_faces_x[index_face_x];
	    area_y = u_faces_y[index_face_y] - u_faces_y[index_face_y+nx];
	    area_z = u_faces_z[index_face_z] - u_faces_z[index_face_z+nx*Ny];

	    avg_edge = (Delta_x_u[index_u] + Delta_y_u[index_u] + Delta_z_u[index_u]) / 3;
	    max_area = 2 * avg_edge * avg_edge;

	    compute_factors =
	      (fabs(area_x) > min_area_crit * max_area) ||
	      (fabs(area_y) > min_area_crit * max_area) ||
	      (fabs(area_z) > min_area_crit * max_area);

	    if(compute_factors){
	      /* Find solid area and normal vector */	      
	      find_solid_area(fluid_norm_v, &solid_area, area_x, area_y, area_z, max_area);
	      /* fluid_norm_v is perpendicular to fluid boundary */
	      copy_vec(norm_v, fluid_norm_v);
	      scale_vec(norm_v, -1);
	      
	      /* Find approximate center of solid area */	     
	      int_curv(x_test, x_vel_node, norm_v, pc_temp, cell_size);
	    }
	    	    
#pragma omp critical
	    {
	      if(compute_factors){
		u_sol_factor[u_sol_factor_count] = MIN(solid_area / delta_h, solid_factor_tol);
		u_p_factor[u_sol_factor_count] = solid_area * norm_v[0];
		u_factor_area[u_sol_factor_count] = solid_area;
		u_factor_inters_x[u_sol_factor_count] = x_test[0];
		u_factor_inters_y[u_sol_factor_count] = x_test[1];
		u_factor_inters_z[u_sol_factor_count] = x_test[2];
		u_p_factor_W[u_sol_factor_count] = 1 - (x_test[0] - nodes_x[index_-1]) / (nodes_x[index_] - nodes_x[index_-1]);
	      }
	      else{
		u_sol_factor[u_sol_factor_count] = 0;
		u_p_factor[u_sol_factor_count] = 0;
		u_factor_area[u_sol_factor_count] = 0;
		u_factor_inters_x[u_sol_factor_count] = NAN;
		u_factor_inters_y[u_sol_factor_count] = NAN;
		u_factor_inters_z[u_sol_factor_count] = NAN;
		u_p_factor_W[u_sol_factor_count] = 0.5;		
	      }
	      u_factor_delta_h[u_sol_factor_count] = delta_h;
	      u_sol_factor_index_u[u_sol_factor_count] = index_u;
	      u_sol_factor_index[u_sol_factor_count] = index_;
	      u_sol_factor_i[u_sol_factor_count] = i;
	      u_sol_factor_J[u_sol_factor_count] = J;
	      u_sol_factor_K[u_sol_factor_count] = K;
	      u_sol_factor_count++;
	    }
	  }
	} /* for(i=1; i<nx-1; i++) */
      } /* for(J=1; J<Ny-1; J++) */
    } /* for(K=1; K<Nz-1; K++) */
      
  } /* if(u_sol_factor_count > 0) */


  if(curves > 0){

#pragma omp parallel for default(none) private(I, j, K, index_v) shared(v_cut_matrix) \
  reduction(+: v_sol_factor_count)

    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){
	
	  index_v = K*Nx*ny + j*Nx + I;
	
	  if(v_cut_matrix[index_v]){
	    v_sol_factor_count++;
	  }
	}
      }
    }
  }
  
  if(v_sol_factor_count > 0){

    data->v_sol_factor = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_p_factor = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_index_v = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_index = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_I = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_j = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_K = create_array_int(v_sol_factor_count, 1, 1, 0);
    data->v_factor_delta_h = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_area = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_inters_x = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_inters_y = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_factor_inters_z = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_p_factor_S = create_array(v_sol_factor_count, 1, 1, 0);
    data->v_sol_factor_count = v_sol_factor_count;
    v_sol_factor_count = 0;

    double *v_sol_factor = data->v_sol_factor;
    double *v_p_factor = data->v_p_factor;
    double *v_factor_delta_h = data->v_factor_delta_h;
    double *v_factor_area = data->v_factor_area;
    double *v_factor_inters_x = data->v_factor_inters_x;
    double *v_factor_inters_y = data->v_factor_inters_y;
    double *v_factor_inters_z = data->v_factor_inters_z;
    double *v_p_factor_S = data->v_p_factor_S;
    int *v_sol_factor_index_v = data->v_sol_factor_index_v;
    int *v_sol_factor_index = data->v_sol_factor_index;
    int *v_sol_factor_I = data->v_sol_factor_I;
    int *v_sol_factor_j = data->v_sol_factor_j;
    int *v_sol_factor_K = data->v_sol_factor_K;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_, index_v, x_vel_node, curv_ind, coeffs_ind, \
					       pc_temp, delta_h, index_face_x, index_face_y, index_face_z, \
					       area_x, area_y, area_z, avg_edge, max_area, norm_v, solid_area, \
					       x_test, compute_factors, fluid_norm_v) \
  shared(v_cut_matrix, nodes_x_v, nodes_y_v, nodes_z_v, poly_coeffs, v_sol_factor, v_p_factor, v_factor_delta_h, \
	 v_factor_area, v_factor_inters_x, v_factor_inters_y, v_factor_inters_z, v_p_factor_S, v_sol_factor_index_v, \
	 v_sol_factor_index, v_sol_factor_I, v_sol_factor_j, v_sol_factor_K, v_sol_factor_count, v_faces_x, v_faces_y, \
	 v_faces_z, Delta_x_v, Delta_y_v, Delta_z_v, nodes_y)
    
    for(K=1; K<Nz-1; K++){
      for(j=1; j<ny-1; j++){
	for(I=1; I<Nx-1; I++){

	  index_v = K*Nx*ny + j*Nx + I;

	  if (v_cut_matrix[index_v]){

	    i = I - 1;
	    J = j + 1;
	    k = K - 1;

	    index_ = K*Nx*Ny + J*Nx + I;

	    /* v node coordinates */

	    x_vel_node[0] = nodes_x_v[index_v];
	    x_vel_node[1] = nodes_y_v[index_v];
	    x_vel_node[2] = nodes_z_v[index_v];

	    /* Set polynomial coefficients */
	    curv_ind = v_cut_matrix[index_v] - 1;

	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    /* Distance from v node to the curve */
	    delta_h = calc_dist(x_vel_node, pc_temp, cell_size);

	    index_face_x = K*nx*ny + j*nx + i;
	    index_face_y = K*Nx*Ny + J*Nx + I;
	    index_face_z = k*Nx*ny + j*Nx + I;

	    area_x = v_faces_x[index_face_x] - v_faces_x[index_face_x+1];
	    area_y = v_faces_y[index_face_y-Nx] - v_faces_y[index_face_y];
	    area_z = v_faces_z[index_face_z] - v_faces_z[index_face_z+Nx*ny];

	    avg_edge = (Delta_x_v[index_v] + Delta_y_v[index_v] + Delta_z_v[index_v]) / 3;
	    max_area = 2 * avg_edge * avg_edge;

	    compute_factors =
	      (fabs(area_x) > min_area_crit * max_area) ||
	      (fabs(area_y) > min_area_crit * max_area) ||
	      (fabs(area_z) > min_area_crit * max_area);

	    if(compute_factors){
	      /* Find solid area and normal vector */
	      find_solid_area(fluid_norm_v, &solid_area, area_x, area_y, area_z, max_area);
	      /* fluid_norm_v is perpendicular to fluid domain */
	      copy_vec(norm_v, fluid_norm_v);
	      scale_vec(norm_v, -1);
	      
	      /* Find approximate center of solid area */
	      int_curv(x_test, x_vel_node, norm_v, pc_temp, cell_size);
	    }
	    
#pragma omp critical
	    {
	      if(compute_factors){
		v_sol_factor[v_sol_factor_count] = MIN(solid_area / delta_h, solid_factor_tol);	
		v_p_factor[v_sol_factor_count] = solid_area * norm_v[1];
		v_factor_area[v_sol_factor_count] = solid_area;
		v_factor_inters_x[v_sol_factor_count] = x_test[0];
		v_factor_inters_y[v_sol_factor_count] = x_test[1];
		v_factor_inters_z[v_sol_factor_count] = x_test[2];
		v_p_factor_S[v_sol_factor_count] = 1 - (x_test[1] - nodes_y[index_-Nx]) / (nodes_y[index_] - nodes_y[index_-Nx]);
	      }
	      else{
		v_sol_factor[v_sol_factor_count] = 0;
		v_p_factor[v_sol_factor_count] = 0;
		v_factor_area[v_sol_factor_count] = 0;
		v_factor_inters_x[v_sol_factor_count] = NAN;
		v_factor_inters_y[v_sol_factor_count] = NAN;
		v_factor_inters_z[v_sol_factor_count] = NAN;
		v_p_factor_S[v_sol_factor_count] = 0.5;		
	      }
	      v_factor_delta_h[v_sol_factor_count] = delta_h;	      
	      v_sol_factor_index_v[v_sol_factor_count] = index_v;
	      v_sol_factor_index[v_sol_factor_count] = index_;
	      v_sol_factor_I[v_sol_factor_count] = I;
	      v_sol_factor_j[v_sol_factor_count] = j;
	      v_sol_factor_K[v_sol_factor_count] = K;
	      v_sol_factor_count++;
	    }
	  }
	} /* for(I=1; I<Nx-1; I++) */
      } /* for(j=1; j<ny-1; j++) */
    } /* for(K=1; K<Nz-1; K++) */
      
  } /* if(v_sol_factor_count > 0) */

  
  if(curves > 0){

#pragma omp parallel for default(none) private(I, J, k, index_w) shared(w_cut_matrix) \
  reduction(+: w_sol_factor_count)

    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  
	  index_w = k*Nx*Ny + J*Nx + I;
	  
	  if(w_cut_matrix[index_w]){
	    w_sol_factor_count++;
	  }
	}
      }
    }
    
  } /*   if(curves > 0) */
  
  if(w_sol_factor_count > 0){

    data->w_sol_factor = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_p_factor = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_sol_factor_index_w = create_array_int(w_sol_factor_count, 1, 1, 0);
    data->w_sol_factor_index = create_array_int(w_sol_factor_count, 1, 1, 0);
    data->w_sol_factor_I = create_array_int(w_sol_factor_count, 1, 1, 0);
    data->w_sol_factor_J = create_array_int(w_sol_factor_count, 1, 1, 0);
    data->w_sol_factor_k = create_array_int(w_sol_factor_count, 1, 1, 0);
    data->w_factor_delta_h = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_factor_area = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_factor_inters_x = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_factor_inters_y = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_factor_inters_z = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_p_factor_B = create_array(w_sol_factor_count, 1, 1, 0);
    data->w_sol_factor_count = w_sol_factor_count;

    w_sol_factor_count = 0;

    double *w_sol_factor = data->w_sol_factor;
    double *w_p_factor = data->w_p_factor;
    double *w_factor_delta_h = data->w_factor_delta_h;
    double *w_factor_area = data->w_factor_area;
    double *w_factor_inters_x = data->w_factor_inters_x;
    double *w_factor_inters_y = data->w_factor_inters_y;
    double *w_factor_inters_z = data->w_factor_inters_z;
    double *w_p_factor_B = data->w_p_factor_B;
    int *w_sol_factor_index_w = data->w_sol_factor_index_w;
    int *w_sol_factor_index = data->w_sol_factor_index;
    int *w_sol_factor_I = data->w_sol_factor_I;
    int *w_sol_factor_J = data->w_sol_factor_J;
    int *w_sol_factor_k = data->w_sol_factor_k;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_, index_w, x_vel_node, curv_ind, coeffs_ind, \
					       pc_temp, delta_h, index_face_x, index_face_y, index_face_z, \
					       area_x, area_y, area_z, avg_edge, max_area, norm_v, solid_area, \
					       x_test, compute_factors, fluid_norm_v) \
  shared(w_cut_matrix, nodes_x_w, nodes_y_w, nodes_z_w, poly_coeffs, w_sol_factor, w_p_factor, w_factor_delta_h, \
	 w_factor_area, w_factor_inters_x, w_factor_inters_y, w_factor_inters_z, w_p_factor_B, w_sol_factor_index_w, \
	 w_sol_factor_index, w_sol_factor_I, w_sol_factor_J, w_sol_factor_k, w_sol_factor_count, w_faces_x, w_faces_y, \
	 w_faces_z, Delta_x_w, Delta_y_w, Delta_z_w, nodes_z)
    
    for(k=1; k<nz-1; k++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_w = k*Nx*Ny + J*Nx + I;

	  if(w_cut_matrix[index_w]){

	    i = I - 1;
	    j = J - 1;
	    K = k + 1;

	    index_ = K*Nx*Ny + J*Nx + I;

	    /* w node coordinates */

	    x_vel_node[0] = nodes_x_w[index_w];
	    x_vel_node[1] = nodes_y_w[index_w];
	    x_vel_node[2] = nodes_z_w[index_w];

	    /* Set polynomial coefficients */
	    curv_ind = w_cut_matrix[index_w] - 1;

	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    /* Distance from w node to the curve */
	    delta_h = calc_dist(x_vel_node, pc_temp, cell_size);

	    index_face_x = k*nx*Ny + J*nx + i;
	    index_face_y = k*Nx*ny + j*Nx + I;
	    index_face_z = K*Nx*Ny + J*Nx + I;

	    area_x = w_faces_x[index_face_x] - w_faces_x[index_face_x+1];
	    area_y = w_faces_y[index_face_y] - w_faces_y[index_face_y+Nx];
	    area_z = w_faces_z[index_face_z-Nx*Ny] - w_faces_z[index_face_z];

	    avg_edge = (Delta_x_w[index_w] + Delta_y_w[index_w] + Delta_z_w[index_w]) / 3;
	    max_area = 2 * avg_edge * avg_edge;

	    compute_factors =
	      (fabs(area_x) > min_area_crit * max_area) ||
	      (fabs(area_y) > min_area_crit * max_area) ||
	      (fabs(area_z) > min_area_crit * max_area);
	    
	    if(compute_factors){
	      /* Find solid area and normal vector */	    
	      find_solid_area(fluid_norm_v, &solid_area, area_x, area_y, area_z, max_area);
	      /* fluid_norm_v is perpendicular to fluid domain */
	      copy_vec(norm_v, fluid_norm_v);
	      scale_vec(norm_v, -1);
	      /* Find approximate center of solid area */
	      int_curv(x_test, x_vel_node, norm_v, pc_temp, cell_size);
	    }
	    
#pragma omp critical
	    {
	      if(compute_factors){
		w_sol_factor[w_sol_factor_count] = MIN(solid_area / delta_h, solid_factor_tol);
		w_p_factor[w_sol_factor_count] = solid_area * norm_v[2];
		w_factor_area[w_sol_factor_count] = solid_area;
		w_factor_inters_x[w_sol_factor_count] = x_test[0];
		w_factor_inters_y[w_sol_factor_count] = x_test[1];
		w_factor_inters_z[w_sol_factor_count] = x_test[2];
		w_p_factor_B[w_sol_factor_count] = 1 - (x_test[2] - nodes_z[index_-Nx*Ny]) / (nodes_z[index_] - nodes_z[index_-Nx*Ny]);
	      }
	      else{
		w_sol_factor[w_sol_factor_count] = 0;
		w_p_factor[w_sol_factor_count] = 0;
		w_factor_area[w_sol_factor_count] = 0;
		w_factor_inters_x[w_sol_factor_count] = NAN;
		w_factor_inters_y[w_sol_factor_count] = NAN;
		w_factor_inters_z[w_sol_factor_count] = NAN;
		w_p_factor_B[w_sol_factor_count] = 0.5;		
	      }
	      w_factor_delta_h[w_sol_factor_count] = delta_h;	      
	      w_sol_factor_index_w[w_sol_factor_count] = index_w;
	      w_sol_factor_index[w_sol_factor_count] = index_;
	      w_sol_factor_I[w_sol_factor_count] = I;
	      w_sol_factor_J[w_sol_factor_count] = J;
	      w_sol_factor_k[w_sol_factor_count] = k;
	      w_sol_factor_count++;
	    }
	  }
	} /* for(I=1; I<Nx-1; I++) */ 
      } /* for(J=1; J<Ny-1; J++) */
    } /* for(k=1; k<nz-1; k++) */
      
  } /* if(w_sol_factor_count > 0) */

  
  return ret_value;
}

/* COMPUTE_ALPHAS_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) alpha_u_x: Correction factor for u_e and u_w in momentum
  balance for u cell.

  Array (double, nxnyNz) alpha_u_y: Correction factor for u_n and u_s in momentum
  balance for u cell.

  Array (double, nxNynz) alpha_u_z: Correction factor for u_t and u_b in momentum 
  balance for u cell.

  Array (double, nxnyNz) alpha_v_x: Correction factor for v_e and v_w in momentum 
  balance for v cell.

  Array (double, NxNyNz) alpha_v_y: Correction factor for v_n and v_s in momentum 
  balance for v cell.

  Array (double, Nxnynz) alpha_v_z: Correction factor for v_t and v_b in momentum
  balance for v cell.

  Array (double, nxNynz) alpha_w_x: Correction factor for w_e and w_w in momentum
  balance for w cell.

  Array (double, Nxnynz) alpha_w_y: Correction factor for w_n and w_s in momentum 
  balance for w cell.

  Array (double, NxNyNz) alpha_w_z: Correction factor for w_t and w_b in momentum
  balance for w cell.

  Array (double, NxNyNz) u_cut_fw: Weight factor for determination of u_e and u_w
  in momentum balance for u cell.

  Array (double, NxNyNz) u_cut_fe: Weight factor for determination of u_e and u_w
  in momentum balance for u cell.

  Array (double, NxNyNz) v_cut_fs: Weight factor for determination of v_n and v_s
  in momentum balance for v cell.

  Array (double, NxNyNz) v_cut_fn: Weight factor for determination of v_n and v_s
  in momentum balance for v cell.

  Array (double, NxNyNz) w_cut_fb: Weight factor for determination of w_t and w_b
  in momentum balance for w cell.

  Array (double, NxNyNz) w_cut_ft: Weight factor for determination of w_t and w_b
  in momentum balance for w cell.

  Array (double, diff_u_face_x_count) diff_factor_u_face_x: Correction factor for
  diffusive momentum flux at w and e faces in momentum balance for u cell.

  Array (double, diff_v_face_y_count) diff_factor_v_face_y: Correction factor for
  diffusive momentum flux at s and n faces in momentum balance for v cell.

  Array (double, diff_w_face_z_count) diff_factor_w_face_z: Correction factor for
  diffusive momentum flux at b and t faces in momentum balance for w cell.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 03-09-2020
*/
void compute_alphas_3D(Data_Mem *data){
  
  int i, I, j, J, k, K;
  int index_, index_u, index_v, index_w;
  int curv_ind;
  int some_face_cut_OK;
  int W_is_solid_OK, E_is_solid_OK;
  int S_is_solid_OK, N_is_solid_OK;
  int B_is_solid_OK, T_is_solid_OK;
  int coeffs_ind;

  int diff_u_face_x_count = 0;
  int diff_v_face_y_count = 0;
  int diff_w_face_z_count = 0;

  const int Ny = data->Ny;
  const int Nx = data->Nx;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int flow_scheme = data->flow_scheme;

  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;
  const int *p_cut_face_z = data->p_cut_face_z;
  const int *u_cut_face_x = data->u_cut_face_x;
  const int *u_cut_face_y = data->u_cut_face_y;
  const int *u_cut_face_z = data->u_cut_face_z;
  const int *v_cut_face_x = data->v_cut_face_x;
  const int *v_cut_face_y = data->v_cut_face_y;
  const int *v_cut_face_z = data->v_cut_face_z;
  const int *w_cut_face_x = data->w_cut_face_x;
  const int *w_cut_face_y = data->w_cut_face_y;
  const int *w_cut_face_z = data->w_cut_face_z;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double curve_factor;
  double dist_e, dist_w;
  double dist_s, dist_n;
  double dist_b, dist_t;
  double fW, fE;
  double fS, fN;
  double fB, fT;
  double dist_f;
  double delta_h;

  double den;

  double pc_temp[16];
  double norm_v[3] = {0};
  double S[3] = {0};
  double E_coord[3] = {0};
  double W_coord[3] = {0};
  double N_coord[3] = {0};
  double S_coord[3] = {0};
  double T_coord[3] = {0};
  double B_coord[3] = {0};
  double face_center_coord[3] = {0};
  double face_coord[3] = {0};
  double x_int[3] = {0};
  double x_centroid[3] = {0};
  double face_lower_corner[3] = {0};
  double face_upper_corner[3] = {0};

  const double tol = 1e-5;
  const double pol_tol = data->pol_tol;
  const double cell_size = data->cell_size;

  double *alpha_u_x = data->alpha_u_x;
  double *alpha_u_y = data->alpha_u_y;
  double *alpha_u_z = data->alpha_u_z;
  double *alpha_v_x = data->alpha_v_x;
  double *alpha_v_y = data->alpha_v_y;
  double *alpha_v_z = data->alpha_v_z;
  double *alpha_w_x = data->alpha_w_x;
  double *alpha_w_y = data->alpha_w_y;
  double *alpha_w_z = data->alpha_w_z;
  double *u_cut_fw = data->u_cut_fw;
  double *u_cut_fe = data->u_cut_fe;
  double *v_cut_fs = data->v_cut_fs;
  double *v_cut_fn = data->v_cut_fn;
  double *w_cut_fb = data->w_cut_fb;
  double *w_cut_ft = data->w_cut_ft;

  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;
  const double *nodes_x_w = data->nodes_x_w;
  const double *nodes_y_w = data->nodes_y_w;
  const double *nodes_z_w = data->nodes_z_w;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;
  const double *Delta_xE_u = data->Delta_xE_u;
  const double *Delta_yN_u = data->Delta_yN_u;
  const double *Delta_zT_u = data->Delta_zT_u;
  const double *Delta_yn_u = data->Delta_yn_u;
  const double *Delta_zt_u = data->Delta_zt_u;
  const double *Delta_xE_v = data->Delta_xE_v;
  const double *Delta_yN_v = data->Delta_yN_v;
  const double *Delta_zT_v = data->Delta_zT_v;
  const double *Delta_xe_v = data->Delta_xe_v;
  const double *Delta_zt_v = data->Delta_zt_v;
  const double *Delta_xE_w = data->Delta_xE_w;
  const double *Delta_yN_w = data->Delta_yN_w;
  const double *Delta_xe_w = data->Delta_xe_w;
  const double *Delta_yn_w = data->Delta_yn_w;
  const double *Delta_zT_w = data->Delta_zT_w;
  const double *poly_coeffs = data->poly_coeffs;
  const double *Delta_x_u = data->Delta_x_u;
  const double *Delta_y_u = data->Delta_y_u;
  const double *Delta_z_u = data->Delta_z_u;
  const double *Delta_x_v = data->Delta_x_v;
  const double *Delta_y_v = data->Delta_y_v;
  const double *Delta_z_v = data->Delta_z_v;
  const double *Delta_x_w = data->Delta_x_w;
  const double *Delta_y_w = data->Delta_y_w;
  const double *Delta_z_w = data->Delta_z_w;

  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  const double *u_faces_z = data->u_faces_z;
  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *v_faces_z = data->v_faces_z;
  const double *w_faces_x = data->w_faces_x;
  const double *w_faces_y = data->w_faces_y;
  const double *w_faces_z = data->w_faces_z;
  const double *vertex_x_u = data->vertex_x_u;
  const double *vertex_y_v = data->vertex_y_v;
  const double *vertex_z_w = data->vertex_z_w;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;

  /* u cut cells */
  
  /* alpha_u_x */

#pragma omp parallel for default(none) private(I, J, K, i, index_, index_u) \
  shared(u_faces_x, Delta_y_u, Delta_z_u, u_cut_fe, u_cut_fw, nodes_x, nodes_x_u, Delta_xE_u)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;

	if(u_faces_x[index_] > tol * Delta_y_u[index_u] * Delta_z_u[index_u]){
	  /* Default values */
	  u_cut_fe[index_] = (nodes_x[index_] - nodes_x_u[index_u]) / Delta_xE_u[index_u];
	  u_cut_fw[index_] = 1 - u_cut_fe[index_];	  
	}
	else{
	  /* Interior face */
	  u_cut_fw[index_] = 0;
	  u_cut_fe[index_] = 0;
	}
      }
    }
  }
    
#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_u, dist_e, dist_w, dist_f, fE, fW, \
	  some_face_cut_OK, W_is_solid_OK, E_is_solid_OK, pc_temp,	\
	  curv_ind, coeffs_ind, curve_factor, x_centroid,		\
	  face_lower_corner, face_upper_corner, x_int, E_coord,		\
	  W_coord, face_center_coord, norm_v, den)			\
  shared(u_faces_x, Delta_y_u, Delta_z_u, u_cut_face_x, p_cut_face_x, nodes_x, u_dom_matrix, nodes_x_u, \
	 nodes_y_u, nodes_z_u, alpha_u_x, u_cut_fw, u_cut_fe, poly_coeffs, curve_is_solid_OK, \
	 vertex_x_u, vertex_y, vertex_z, Delta_xE_u)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	j = J - 1;
	k = K - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;

	if(u_faces_x[index_] > tol * Delta_y_u[index_u] * Delta_z_u[index_u]){
	  /* Any of these conditions imply that the interpolated velocity is not 
	     aligned with the velocity nodes used for interpolation */

	  some_face_cut_OK = (u_cut_face_x[index_] || p_cut_face_x[index_u] 
			      || p_cut_face_x[index_u+1]);
	  W_is_solid_OK = u_dom_matrix[index_u];
	  E_is_solid_OK = u_dom_matrix[index_u+1];
     
	  if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK){
	    /* Need to calculate an alpha_value */

	    curv_ind = (some_face_cut_OK)? (some_face_cut_OK-1) : (W_is_solid_OK)? (W_is_solid_OK - 1) : (E_is_solid_OK - 1);
	    
	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	    /* Face corners */
	    face_lower_corner[0] = vertex_x_u[I];
	    face_lower_corner[1] = vertex_y[j];
	    face_lower_corner[2] = vertex_z[k];

	    face_upper_corner[0] = face_lower_corner[0];
	    face_upper_corner[1] = vertex_y[j+1];
	    face_upper_corner[2] = vertex_z[k+1];	    

	    if((!W_is_solid_OK) && (!E_is_solid_OK)){
	      /* Both W and E nodes in fluid domain */

	      /* East neighbour */
	      E_coord[0] = nodes_x_u[index_u+1];
	      E_coord[1] = nodes_y_u[index_u+1];
	      E_coord[2] = nodes_z_u[index_u+1];

	      /* West neighbour */
	      W_coord[0] = nodes_x_u[index_u];
	      W_coord[1] = nodes_y_u[index_u];
	      W_coord[2] = nodes_z_u[index_u];

	      u_cut_fe[index_] = (nodes_x[index_] - nodes_x_u[index_u]) / Delta_xE_u[index_u];
	      u_cut_fw[index_] = 1 - u_cut_fe[index_];	      
	    
	    }
	    else if (u_cut_face_x[index_] == 0){
	      /* Interpolate on x axis direction */
	      if(E_is_solid_OK){

		/* West neighbour */
		W_coord[0] = nodes_x_u[index_u];
		W_coord[1] = nodes_y_u[index_u];
		W_coord[2] = nodes_z_u[index_u];

		/* Find intersection with solid body along x axis */
		norm_v[0] = -1 * curve_factor;
		norm_v[1] = 0;
		norm_v[2] = 0;
		int_curv(x_int, W_coord, norm_v, pc_temp, cell_size);
				  
		/* East neighbour: point on solid surface */
		copy_vec(E_coord, x_int);

		u_cut_fw[index_] = 1 - (nodes_x[index_] - W_coord[0]) / (E_coord[0] - W_coord[0]);
		u_cut_fe[index_] = 0;		
		
	      }
	      else{		
		/* East neighbour */
		E_coord[0] = nodes_x_u[index_u+1];
		E_coord[1] = nodes_y_u[index_u+1];
		E_coord[2] = nodes_z_u[index_u+1];

		/* Find intersection with solid body along x axis */
		norm_v[0] = 1 * curve_factor;
		norm_v[1] = 0;
		norm_v[2] = 0;		
		int_curv(x_int, E_coord, norm_v, pc_temp, cell_size);		
	     
		/* West neighbour: point on solid surface */
		copy_vec(W_coord, x_int);

		u_cut_fw[index_] = 0;
		u_cut_fe[index_] = (nodes_x[index_] - W_coord[0]) / (E_coord[0] - W_coord[0]);

	      }
	    }
	    else{
	      /* Interpolate to solid surface */
	      if(E_is_solid_OK){

		/* West neighbour */
		W_coord[0] = nodes_x_u[index_u];
		W_coord[1] = nodes_y_u[index_u];
		W_coord[2] = nodes_z_u[index_u];

		/* Find centroid of cut face */      
		
		face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 0);

		diff_vec(norm_v, W_coord, x_centroid);
		scale_vec(norm_v, curve_factor);
		normalize_vec(norm_v);
		int_curv(x_int, W_coord, norm_v, pc_temp, cell_size);

		/* East neighbour: point on solid surface */
		copy_vec(E_coord, x_int);

		u_cut_fw[index_] = 1 - (nodes_x[index_] - W_coord[0]) / (E_coord[0] - W_coord[0]);
		u_cut_fe[index_] = 0;		
		
	      }
	      else{

		/* East neighbour */
		E_coord[0] = nodes_x_u[index_u+1];
		E_coord[1] = nodes_y_u[index_u+1];
		E_coord[2] = nodes_z_u[index_u+1];

		/* Find centroid of cut face */      
		
		face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 0);

		diff_vec(norm_v, E_coord, x_centroid);
		scale_vec(norm_v, curve_factor);
		normalize_vec(norm_v);
		int_curv(x_int, E_coord, norm_v, pc_temp, cell_size);

		/* West neighbour: point on solid surface */
		copy_vec(W_coord, x_int);

		u_cut_fe[index_] = (nodes_x[index_] - W_coord[0]) / (E_coord[0] - W_coord[0]);
		u_cut_fw[index_] = 0;
		
	      }	    
	    }

#ifdef OLDINT_OK
	    /* East neighbour */
	    E_coord[0] = nodes_x_u[index_u+1];
	    E_coord[1] = nodes_y_u[index_u+1];
	    E_coord[2] = nodes_z_u[index_u+1];

	    /* West neighbour */
	    W_coord[0] = nodes_x_u[index_u];
	    W_coord[1] = nodes_y_u[index_u];
	    W_coord[2] = nodes_z_u[index_u];

	    u_cut_fe[index_] = (nodes_x[index_] - W_coord[0]) /
	      (E_coord[0] - W_coord[0]);
	    u_cut_fw[index_] = 1 - u_cut_fe[index_];
#endif
	    
	    /* Common for all three cases */

	    /* Distance from east neighbour to the curve */
	    dist_e = calc_dist(E_coord, pc_temp, cell_size);
	    /* Distance from west neighbour to the curve */
	    dist_w = calc_dist(W_coord, pc_temp, cell_size);

	    /* This is supposed to be the node in between the u_W and u_E nodes */

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 0);	    
	  
	    dist_f = calc_dist(face_center_coord, pc_temp, cell_size);
	  
	    fE = (face_center_coord[0] - W_coord[0]) / (E_coord[0] - W_coord[0]);
	    fW = 1 - fE;	
	    
	    /* Build alpha based on distances computed, see eqs 18 to 21 Kirkpatrick
	       This affects convective terms only */
	    den = dist_e * fE + dist_w * fW;
	    
	    alpha_u_x[index_] = (den <= 0) ? 1 : dist_f / den;

	  } /* 	    if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK) */
	} /* 	if(u_faces_x[index_] > tol * Delta_y_u[index_u] * Delta_z_u[index_u]) */
      }
    }
  }

  
  /* diff_factor_u_face_x */

#pragma omp parallel for default(none)					\
  private(I, J, K, i, index_, index_u, W_is_solid_OK, E_is_solid_OK, some_face_cut_OK) \
  shared(u_faces_x, Delta_y_u, Delta_z_u, u_dom_matrix, u_cut_face_x, p_cut_face_x) \
  reduction(+: diff_u_face_x_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	i = I - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_u = K*nx*Ny + J*nx + i;

	if(u_faces_x[index_] > tol * Delta_y_u[index_u] * Delta_z_u[index_u]){

	  W_is_solid_OK = u_dom_matrix[index_u];
	  E_is_solid_OK = u_dom_matrix[index_u+1];
	  some_face_cut_OK = u_cut_face_x[index_] || p_cut_face_x[index_u] 
	    || p_cut_face_x[index_u+1];

	  if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK){
	    diff_u_face_x_count++;
	  }
	}
      }
    }
  }
  
  if(diff_u_face_x_count > 0){

    data->diff_u_face_x_index = create_array_int(diff_u_face_x_count, 1, 1, 0);
    data->diff_u_face_x_index_u = create_array_int(diff_u_face_x_count, 1, 1, 0);
    data->diff_u_face_x_I = create_array_int(diff_u_face_x_count, 1, 1, 0);
    data->diff_u_face_x_Delta = create_array(diff_u_face_x_count, 1, 1, 0);
    data->diff_factor_u_face_x = create_array(diff_u_face_x_count, 1, 1, 0);

    data->diff_u_face_x_count = diff_u_face_x_count;

    diff_u_face_x_count = 0;

    int *diff_u_face_x_index = data->diff_u_face_x_index;
    int *diff_u_face_x_index_u = data->diff_u_face_x_index_u;
    int *diff_u_face_x_I = data->diff_u_face_x_I;
    double *diff_u_face_x_Delta = data->diff_u_face_x_Delta;
    double *diff_factor_u_face_x = data->diff_factor_u_face_x;


#pragma omp parallel for default(none) \
  private(I, J, K, i, j, k, index_, index_u, some_face_cut_OK, delta_h, W_is_solid_OK, E_is_solid_OK, \
	  coeffs_ind, curv_ind, pc_temp, curve_factor, face_lower_corner, face_upper_corner, \
	  x_centroid, face_coord, norm_v, S, W_coord, E_coord, x_int)	\
  shared(u_cut_face_x, p_cut_face_x, nodes_x_u, nodes_y_u, nodes_z_u,	\
	 diff_factor_u_face_x, u_faces_x, Delta_y_u, Delta_z_u, u_dom_matrix, \
	 nodes_x, diff_u_face_x_index, diff_u_face_x_index_u, diff_u_face_x_Delta, \
	 diff_u_face_x_count, poly_coeffs, vertex_x_u, vertex_y, vertex_z, curve_is_solid_OK, diff_u_face_x_I)

    for(K=1; K<Nz-1; K++){ 
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  i = I - 1;
	  j = J - 1;
	  k = K - 1;
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_u = K*nx*Ny + J*nx + i;

	  if(u_faces_x[index_] > tol * Delta_y_u[index_u] * Delta_z_u[index_u]){

	    W_is_solid_OK = u_dom_matrix[index_u];
	    E_is_solid_OK = u_dom_matrix[index_u+1];
	    some_face_cut_OK = u_cut_face_x[index_] || p_cut_face_x[index_u] 
	      || p_cut_face_x[index_u+1];

	    if(some_face_cut_OK || W_is_solid_OK || E_is_solid_OK){

	      curv_ind = (some_face_cut_OK)? (some_face_cut_OK - 1) : (W_is_solid_OK)? (W_is_solid_OK - 1) : (E_is_solid_OK - 1);	      

	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);	      

	      /* Face corners */
	      face_lower_corner[0] = vertex_x_u[I];
	      face_lower_corner[1] = vertex_y[j];
	      face_lower_corner[2] = vertex_z[k];

	      face_upper_corner[0] = face_lower_corner[0];
	      face_upper_corner[1] = vertex_y[j+1];
	      face_upper_corner[2] = vertex_z[k+1];
	      
	      if((!W_is_solid_OK) && (!E_is_solid_OK)){
		/* East neighbour */
		E_coord[0] = nodes_x_u[index_u+1];
		E_coord[1] = nodes_y_u[index_u+1];
		E_coord[2] = nodes_z_u[index_u+1];

		/* West neighbour */
		W_coord[0] = nodes_x_u[index_u];
		W_coord[1] = nodes_y_u[index_u];
		W_coord[2] = nodes_z_u[index_u];
	      }
	      else if (u_cut_face_x[index_] == 0){
		/* Interpolate on x axis direction */
		if(E_is_solid_OK){
		  /* West neighbour */
		  W_coord[0] = nodes_x_u[index_u];
		  W_coord[1] = nodes_y_u[index_u];
		  W_coord[2] = nodes_z_u[index_u];

		  /* Find intersection with solid body along x axis */
		  norm_v[0] = -1 * curve_factor;
		  norm_v[1] = 0;
		  norm_v[2] = 0;
		  int_curv(x_int, W_coord, norm_v, pc_temp, cell_size);
		  
		  /* East neighbour: point on solid surface */
		  copy_vec(E_coord, x_int);
		  
		}	    
		else{
		  /* East neighbour */
		  E_coord[0] = nodes_x_u[index_u+1];
		  E_coord[1] = nodes_y_u[index_u+1];
		  E_coord[2] = nodes_z_u[index_u+1];

		  /* Find intersection with solid body along x axis */
		  norm_v[0] = 1 * curve_factor;
		  norm_v[1] = 0;
		  norm_v[2] = 0;
		  int_curv(x_int, E_coord, norm_v, pc_temp, cell_size);
		  
		  /* West neighbour: point on solid surface */
		  copy_vec(W_coord, x_int);

		}
	      }
	      else{
		/* Interpolate to solid surface */
		if(E_is_solid_OK){
		  /* West neighbour */
		  W_coord[0] = nodes_x_u[index_u];
		  W_coord[1] = nodes_y_u[index_u];
		  W_coord[2] = nodes_z_u[index_u];		  

		  /* Find centroid of cut face */

		  face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 0);

		  diff_vec(norm_v, W_coord, x_centroid);
		  scale_vec(norm_v, curve_factor);
		  normalize_vec(norm_v);
		  int_curv(x_int, W_coord, norm_v, pc_temp, cell_size);
		  
		  /* East neighbour: point on solid surface */
		  copy_vec(E_coord, x_int);

		}
		else{
		  /* East neighbour */
		  E_coord[0] = nodes_x_u[index_u+1];
		  E_coord[1] = nodes_y_u[index_u+1];
		  E_coord[2] = nodes_z_u[index_u+1];

		  /* Find centroid of cut face */

		  face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 0);		  

		  diff_vec(norm_v, E_coord, x_centroid);
		  scale_vec(norm_v, curve_factor);
		  normalize_vec(norm_v);
		  int_curv(x_int, E_coord, norm_v, pc_temp, cell_size);

		/* West neighbour: point on solid surface */
		copy_vec(W_coord, x_int);		  

		}	    
	      }

#ifdef OLDINT_OK
	      E_coord[0] = nodes_x_u[index_u+1];
	      E_coord[1] = nodes_y_u[index_u+1];
	      E_coord[2] = nodes_z_u[index_u+1];
	      
	      W_coord[0] = nodes_x_u[index_u];
	      W_coord[1] = nodes_y_u[index_u];
	      W_coord[2] = nodes_z_u[index_u];
#endif
	      
	      /* Coordinates of w face of u cut cell, "linked coordinate"
		 This is not the face center necessarily, see fig 4 Kirkpatrick */
	      
	      face_coord[0] = nodes_x[index_];
	      face_coord[1] = W_coord[1] + (face_coord[0] - W_coord[0]) * 
		(E_coord[1] - W_coord[1]) / (E_coord[0] - W_coord[0]);
	      face_coord[2] = W_coord[2] + (face_coord[0] - W_coord[0]) *
		(E_coord[2] - W_coord[2]) / (E_coord[0] - W_coord[0]);

	      diff_vec(S, E_coord, W_coord);
	      
	      if(fabs(polyn(face_coord, pc_temp)) > pol_tol){
		calc_dist_norm(face_coord, pc_temp, &delta_h, norm_v, cell_size);

#pragma omp critical
		{
		  diff_u_face_x_index[diff_u_face_x_count] = index_;
		  diff_u_face_x_index_u[diff_u_face_x_count] = index_u;
		  diff_u_face_x_I[diff_u_face_x_count] = I;
		  diff_u_face_x_Delta[diff_u_face_x_count] = S[0];
		  // This factor can be seen from eq 32 of Kirkpatrick
		  diff_factor_u_face_x[diff_u_face_x_count] = u_faces_x[index_] * (S[1] * norm_v[1] + S[2] * norm_v[2]) *
		    curve_factor / (S[0] * delta_h);
		  diff_u_face_x_count++;
		}
	      }
	      else{
		// face_coord located on curve, delta_h->0, diff_factor arbitrarily set to zero
#pragma omp critical
		{
		  diff_u_face_x_index[diff_u_face_x_count] = index_;
		  diff_u_face_x_index_u[diff_u_face_x_count] = index_u;
		  diff_u_face_x_I[diff_u_face_x_count] = I;
		  diff_u_face_x_Delta[diff_u_face_x_count] = S[0];
		  diff_factor_u_face_x[diff_u_face_x_count] = 0;
		  diff_u_face_x_count++;
		}
	      }
	    } // (some_face_cut_OK)
	  } // if(u_faces_x[index_] > tol * Delta_y_u[index_u] * Delta_z_u[index_u])
	} // for(I=1; I<Nx-1; I++)
      } // for(J=1; J<Ny-1; J++)
    } // for(K=1; K<Nz-1; K++)
  } // if(diff_u_face_x_count > 0)

  
  /* alpha_u_y just needed for QUICK scheme */

  if(flow_scheme == 1){  

#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_u, dist_n, dist_s, dist_f, some_face_cut_OK, \
	  coeffs_ind, curv_ind, pc_temp, face_lower_corner, face_upper_corner, \
	  S_coord, N_coord, face_center_coord, den)			\
  shared(u_cut_face_y, p_cut_face_x, nodes_x_u, nodes_y_u, nodes_z_u,	\
	 Delta_x_u, Delta_z_u, curve_factor, u_faces_y, alpha_u_y, Delta_yn_u, Delta_yN_u, \
	 poly_coeffs, curve_is_solid_OK, vertex_x_u, vertex_y, vertex_z)

    for(K=1; K<Nz-1; K++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){

	  I = i + 1;
	  J = j  + 1;
	  k = K - 1;
	  index_ = K*nx*ny + j*nx + i;
	  index_u = K*nx*Ny + J*nx + i;

	  if(u_faces_y[index_] > tol * Delta_x_u[index_u] * Delta_z_u[index_u]){

	    some_face_cut_OK = u_cut_face_y[index_] || p_cut_face_x[index_u]
	      || p_cut_face_x[index_u-nx];
	
	    if(some_face_cut_OK){

	      curv_ind = some_face_cut_OK - 1;

	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      /* South neighbour */
	      S_coord[0] = nodes_x_u[index_u-nx];
	      S_coord[1] = nodes_y_u[index_u-nx];
	      S_coord[2] = nodes_z_u[index_u-nx];
	      dist_s = calc_dist(S_coord, pc_temp, cell_size);

	      /* North neighbour */
	      N_coord[0] = nodes_x_u[index_u];
	      N_coord[1] = nodes_y_u[index_u];
	      N_coord[2] = nodes_z_u[index_u];
	      dist_n = calc_dist(N_coord, pc_temp, cell_size);

	      face_lower_corner[0] = vertex_x_u[I-1];
	      face_lower_corner[1] = vertex_y[j];
	      face_lower_corner[2] = vertex_z[k];

	      face_upper_corner[0] = vertex_x_u[I];
	      face_upper_corner[1] = face_lower_corner[1];
	      face_upper_corner[2] = vertex_z[k+1];				      

	      face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);	    

	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_n * Delta_yn_u[index_u-nx] + dist_s * (Delta_yN_u[index_u-nx] - Delta_yn_u[index_u-nx]);

	      alpha_u_y[index_] = (den <= 0) ? 1 : dist_f * Delta_yN_u[index_u-nx] / den;
	      
	    }
	  } /* if(u_faces_y[index_] > tol * Delta_x_u[index_u] * Delta_z_u[index_u]) */
	} /* for(i=0; i<nx; i++) */
      } /* for(j=0; j<ny; j++) */
    } /* for(K=1; K<Nz-1; K++) */
  
  } /* if(flow_scheme == 1) */

  /* alpha_u_z just needed for QUICK scheme */

  if(flow_scheme == 1){  

#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_u, dist_t, dist_b, dist_f, some_face_cut_OK, \
	  coeffs_ind, curv_ind, pc_temp, face_lower_corner, face_upper_corner, \
	  B_coord, T_coord, face_center_coord, den)			\
  shared(u_cut_face_z, p_cut_face_x, nodes_x_u, nodes_y_u, nodes_z_u,	\
	 Delta_x_u, Delta_y_u, curve_factor, u_faces_z, alpha_u_z, Delta_zt_u, Delta_zT_u, \
	 poly_coeffs, curve_is_solid_OK, vertex_x_u, vertex_y, vertex_z)

    for(k=0; k<nz; k++){
      for(J=1; J<Ny-1; J++){
	for(i=0; i<nx; i++){

	  I = i + 1;
	  j = J - 1;
	  K = k + 1;
	  index_ = k*nx*Ny + J*nx + i;
	  index_u = K*nx*Ny + J*nx + i;

	  if(u_faces_z[index_] > tol * Delta_x_u[index_u] * Delta_y_u[index_u]){

	    some_face_cut_OK = u_cut_face_z[index_] || p_cut_face_x[index_u]
	      || p_cut_face_x[index_u-nx*Ny];

	    if(some_face_cut_OK){

	      curv_ind = some_face_cut_OK - 1;
	    
	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      /* Bottom neighbour */
	      B_coord[0] = nodes_x_u[index_u-nx*Ny];
	      B_coord[1] = nodes_y_u[index_u-nx*Ny];
	      B_coord[2] = nodes_z_u[index_u-nx*Ny];
	      dist_b = calc_dist(B_coord, pc_temp, cell_size);

	      /* Top neighbour */
	      T_coord[0] = nodes_x_u[index_u];
	      T_coord[1] = nodes_y_u[index_u];
	      T_coord[2] = nodes_z_u[index_u];
	      dist_t = calc_dist(T_coord, pc_temp, cell_size);

	      face_lower_corner[0] = vertex_x_u[I-1];
	      face_lower_corner[1] = vertex_y[j];
	      face_lower_corner[2] = vertex_z[k];

	      face_upper_corner[0] = vertex_x_u[I];
	      face_upper_corner[1] = vertex_y[j+1];
	      face_upper_corner[2] = face_lower_corner[2];

	      face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);

	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_t * Delta_zt_u[index_u-nx*Ny] + dist_b * (Delta_zT_u[index_u-nx*Ny] - Delta_zt_u[index_u-nx*Ny]);

	      alpha_u_z[index_] = (den <= 0) ? 1 : dist_f * Delta_zT_u[index_u-nx*Ny] / den;
	    
	    }
	  }
	}
      }
    }

  }
  
  /* v cut cells */
  
  /* alpha_v_y */

#pragma omp parallel for default(none)					\
  private(I, J, K, j, index_, index_v)					\
  shared(v_faces_y, Delta_x_v, Delta_z_v, v_cut_fn, v_cut_fs, nodes_y, nodes_y_v)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	j = J - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_v = K*Nx*ny + j*Nx + I;

	if(v_faces_y[index_] > tol * Delta_x_v[index_v] * Delta_z_v[index_v]){

	  /* Default values */	    
	  v_cut_fn[index_] = (nodes_y[index_] - nodes_y_v[index_v]) / 
	    (nodes_y_v[index_v+Nx] - nodes_y_v[index_v]);
	  v_cut_fs[index_] = 1 - v_cut_fn[index_];

	}
	else{
	  /* Interior face */
	  v_cut_fs[index_] = 0;
	  v_cut_fn[index_] = 0;
	}
      }
    }
  }

#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_u, index_v, dist_n, dist_s, dist_f, fN, fS, \
	  some_face_cut_OK, S_is_solid_OK, N_is_solid_OK, pc_temp,	\
	  curv_ind, coeffs_ind, curve_factor, x_centroid,		\
	  face_lower_corner, face_upper_corner, x_int, N_coord,		\
	  S_coord, face_center_coord, norm_v, den)			\
  shared(v_faces_y, Delta_x_v, Delta_z_v, v_cut_face_y, p_cut_face_y, nodes_y, v_dom_matrix, nodes_x_v, \
	 nodes_y_v, nodes_z_v, alpha_v_y, v_cut_fs, v_cut_fn, poly_coeffs, curve_is_solid_OK, \
	 vertex_x, vertex_y_v, vertex_z, Delta_yN_v)
  
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	j = J - 1;
	k = K - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_v = K*Nx*ny + j*Nx + I;	

	if(v_faces_y[index_] > tol * Delta_x_v[index_v] * Delta_z_v[index_v]){
	  /* Any of these conditions imply that the interpolated velocity is not 
	     aligned with the velocity nodes used for interpolation */

	  some_face_cut_OK = (v_cut_face_y[index_] || p_cut_face_y[index_v]
			      || p_cut_face_y[index_v+Nx]);
	  S_is_solid_OK = v_dom_matrix[index_v];
	  N_is_solid_OK = v_dom_matrix[index_v+Nx];
	  
	  if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK){
	    /* Need to calculate an alpha_value */

	    curv_ind = (some_face_cut_OK)? (some_face_cut_OK-1) : (S_is_solid_OK)? (S_is_solid_OK-1) : (N_is_solid_OK-1);
	    
	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	    
	    /* Face corners */

	    face_lower_corner[0] = vertex_x[i];
	    face_lower_corner[1] = vertex_y_v[J];
	    face_lower_corner[2] = vertex_z[k];

	    face_upper_corner[0] = vertex_x[i+1];
	    face_upper_corner[1] = face_lower_corner[1];
	    face_upper_corner[2] = vertex_z[k+1];

	    if((!S_is_solid_OK) && (!N_is_solid_OK)){
	      /* Both B and T nodes in fluid domain */

	      /* North neighbour */
	      N_coord[0] = nodes_x_v[index_v+Nx];
	      N_coord[1] = nodes_y_v[index_v+Nx];
	      N_coord[2] = nodes_z_v[index_v+Nx];

	      /* South neighbour */
	      S_coord[0] = nodes_x_v[index_v];
	      S_coord[1] = nodes_y_v[index_v];
	      S_coord[2] = nodes_z_v[index_v];

	      v_cut_fn[index_] = (nodes_y[index_] - nodes_y_v[index_v]) / Delta_yN_v[index_v];
	      v_cut_fs[index_] = 1 - v_cut_fn[index_];
	    
	    }
	    else if (v_cut_face_y[index_] == 0){
	      /* Interpolate on y axis direction */
	      if(N_is_solid_OK){

		/* South neighbour */
		S_coord[0] = nodes_x_v[index_v];
		S_coord[1] = nodes_y_v[index_v];
		S_coord[2] = nodes_z_v[index_v];
		
		/* Find intersection with solid body along y axis */
		norm_v[0] = 0;
		norm_v[1] = -1 * curve_factor;
		norm_v[2] = 0;
		int_curv(x_int, S_coord, norm_v, pc_temp, cell_size); 
	      
		/* North neighbour: point on solid surface */
		copy_vec(N_coord, x_int);

		v_cut_fs[index_] = 1 - (nodes_y[index_] - S_coord[1]) / (N_coord[1] - S_coord[1]);
		v_cut_fn[index_] = 0;
		
	      }	    
	      else{
		
		/* North neighbour */
		N_coord[0] = nodes_x_v[index_v+Nx];
		N_coord[1] = nodes_y_v[index_v+Nx];
		N_coord[2] = nodes_z_v[index_v+Nx];

		/* Find intersection with solid body along y axis */
		
		norm_v[0] = 0;
		norm_v[1] = 1 * curve_factor;
		norm_v[2] = 0;
		int_curv(x_int, N_coord, norm_v, pc_temp, cell_size);
				
		/* South neighbour: point on solid surface */
		copy_vec(S_coord, x_int);

		v_cut_fs[index_] = 0;
		v_cut_fn[index_] = (nodes_y[index_] - S_coord[1]) / (N_coord[1] - S_coord[1]);
	      }
	    }
	    else{
	      /* Interpolate to solid surface */
	      if(N_is_solid_OK){

		/* South neighbour */
		S_coord[0] = nodes_x_v[index_v];
		S_coord[1] = nodes_y_v[index_v];
		S_coord[2] = nodes_z_v[index_v];

		/* Find centroid of cut face */

		face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);

		diff_vec(norm_v, S_coord, x_centroid);
		scale_vec(norm_v, curve_factor);
		normalize_vec(norm_v);
		int_curv(x_int, S_coord, norm_v, pc_temp, cell_size);

		/* N neighbour: point on solid surface */
		copy_vec(N_coord, x_int);
		
		v_cut_fs[index_] = 1 - (nodes_y[index_] - S_coord[1]) /(N_coord[1] - S_coord[1]);
		v_cut_fn[index_] = 0;

	      }
	      else{

		/* North neighbour */
		N_coord[0] = nodes_x_v[index_v+Nx];
		N_coord[1] = nodes_y_v[index_v+Nx];
		N_coord[2] = nodes_z_v[index_v+Nx];

		/* Find centroid of cut face */

		face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);

		diff_vec(norm_v, N_coord, x_centroid);
		scale_vec(norm_v, curve_factor);
		normalize_vec(norm_v);
		int_curv(x_int, N_coord, norm_v, pc_temp, cell_size);
		
		/* South neighbour: point on solid surface */
		copy_vec(S_coord, x_int);

		v_cut_fn[index_] = (nodes_y[index_] - S_coord[1]) / (N_coord[1] - S_coord[1]);
		v_cut_fs[index_] = 0;
	      }	    
	    }

#ifdef OLDINT_OK
	    /* North neighbour */
	    N_coord[0] = nodes_x_v[index_v+Nx];
	    N_coord[1] = nodes_y_v[index_v+Nx];
	    N_coord[2] = nodes_z_v[index_v+Nx];

	    /* South neighbour */
	    S_coord[0] = nodes_x_v[index_v];
	    S_coord[1] = nodes_y_v[index_v];
	    S_coord[2] = nodes_z_v[index_v];

	    v_cut_fn[index_] = (nodes_y[index_] - S_coord[1]) /
	      (N_coord[1] - S_coord[1]);
	    v_cut_fs[index_] = 1 - v_cut_fn[index_];
#endif
	    
	    /* Common for all three cases */

	    /* Distance from north neighbour to the curve */
	    dist_n = calc_dist(N_coord, pc_temp, cell_size);
	    /* Distance from south neighbour to the curve */
	    dist_s = calc_dist(S_coord, pc_temp, cell_size);

	    /* This is supposed to be the node in between the u_P and u_E nodes */

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);
	  
	    dist_f = calc_dist(face_center_coord, pc_temp, cell_size);
	  
	    fN = (face_center_coord[1] - S_coord[1]) / (N_coord[1] - S_coord[1]);
	    fS = 1 - fN;

	    /* Build alpha based on distances computed, see eqs 18 to 21 Kirkpatrick
	       This affects convective terms only */
	    den = dist_n * fN + dist_s * fS;
	    alpha_v_y[index_] = (den <= 0) ? 1 : dist_f / den;

	  } /* 	if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK) */
	} /* 	if(v_faces_y[index_] > tol * Delta_x_v[index_v] * Delta_z_v[index_v]) */
      } /* for(I=1; I<Nx-1; I++) */
    } /* for(J=1; J<Ny-1; J++) */
  } /* for(K=1; K<Nz-1; K++) */

  /* diff_factor_v_face_y */

#pragma omp parallel for default(none)					\
  private(I, J, K, j, index_, index_v, S_is_solid_OK, N_is_solid_OK, some_face_cut_OK) \
  shared(v_faces_y, Delta_x_v, Delta_z_v, v_dom_matrix, v_cut_face_y, p_cut_face_y) \
  reduction(+: diff_v_face_y_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	j = J - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_v = K*Nx*ny + j*Nx + I;

	if(v_faces_y[index_] > tol * Delta_x_v[index_v] * Delta_z_v[index_v]){

	  S_is_solid_OK = v_dom_matrix[index_v];
	  N_is_solid_OK = v_dom_matrix[index_v+Nx];
	  some_face_cut_OK = v_cut_face_y[index_] || p_cut_face_y[index_v] 
	    || p_cut_face_y[index_v+Nx];

	  if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK){
	    diff_v_face_y_count++;
	  }
	}
      }
    }
  }

  if(diff_v_face_y_count > 0){
      
    data->diff_v_face_y_index = create_array_int(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_index_v = create_array_int(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_J = create_array_int(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_Delta = create_array(diff_v_face_y_count, 1, 1, 0);
    data->diff_factor_v_face_y = create_array(diff_v_face_y_count, 1, 1, 0);
    data->diff_v_face_y_count = diff_v_face_y_count;

    diff_v_face_y_count = 0;

    int *diff_v_face_y_index = data->diff_v_face_y_index;
    int *diff_v_face_y_index_v = data->diff_v_face_y_index_v;
    int *diff_v_face_y_J = data->diff_v_face_y_J;
    double *diff_v_face_y_Delta = data->diff_v_face_y_Delta;
    double *diff_factor_v_face_y = data->diff_factor_v_face_y;
    
#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_v, some_face_cut_OK, delta_h, N_is_solid_OK, S_is_solid_OK, \
	  coeffs_ind, curv_ind, pc_temp, curve_factor, face_lower_corner, face_upper_corner, \
	  x_centroid, face_coord, norm_v, S, N_coord, S_coord, x_int)	\
  shared(v_cut_face_y, p_cut_face_y, nodes_x_v, nodes_y_v, nodes_z_v,	\
	 diff_factor_v_face_y, v_faces_y, Delta_x_v, Delta_z_v, v_dom_matrix, \
	 nodes_y, diff_v_face_y_index, diff_v_face_y_index_v, diff_v_face_y_Delta, \
	 diff_v_face_y_count, poly_coeffs, vertex_x, vertex_y_v, vertex_z, curve_is_solid_OK, diff_v_face_y_J)

    for(K=1; K<Nz-1; K++){    
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  
	  i = I - 1;
	  j = J - 1;
	  k = K - 1;
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_v = K*Nx*ny + j*Nx + I;

	  if(v_faces_y[index_] > tol * Delta_x_v[index_v] * Delta_z_v[index_v]){

	    S_is_solid_OK = v_dom_matrix[index_v];
	    N_is_solid_OK = v_dom_matrix[index_v+Nx];
	    some_face_cut_OK = v_cut_face_y[index_] || p_cut_face_y[index_v] 
	      || p_cut_face_y[index_v+Nx];

	    if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK){

	      curv_ind = (some_face_cut_OK)? (some_face_cut_OK - 1) : (S_is_solid_OK)? (S_is_solid_OK - 1) : (N_is_solid_OK - 1);
	      
	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      /* Face corners */
	      face_lower_corner[0] = vertex_x[i];
	      face_lower_corner[1] = vertex_y_v[J];
	      face_lower_corner[2] = vertex_z[k];

	      face_upper_corner[0] = vertex_x[i+1];
	      face_upper_corner[1] = face_lower_corner[1];
	      face_upper_corner[2] = vertex_z[k+1];
	      
	      if((!S_is_solid_OK) && (!N_is_solid_OK)){
		/* North neighbour */
		N_coord[0] = nodes_x_v[index_v+Nx];
		N_coord[1] = nodes_y_v[index_v+Nx];
		N_coord[2] = nodes_z_v[index_v+Nx];

		/* South neighbour */
		S_coord[0] = nodes_x_v[index_v];
		S_coord[1] = nodes_y_v[index_v];
		S_coord[2] = nodes_z_v[index_v];
	      }
	      else if (v_cut_face_y[index_] == 0){
		/* Interpolate on y axis direction */
		if(N_is_solid_OK){
		  /* South neighbour */
		  S_coord[0] = nodes_x_v[index_v];
		  S_coord[1] = nodes_y_v[index_v];
		  S_coord[2] = nodes_z_v[index_v];

		  /* Find intersection with solid body along y axis */
		  norm_v[0] = 0;
		  norm_v[1] = -1 * curve_factor;
		  norm_v[2] = 0;
		  int_curv(x_int, S_coord, norm_v, pc_temp, cell_size);

		  /* North neighbour: point on solid surface */
		  copy_vec(N_coord, x_int);
		}	    
		else{
		  /* North neighbour */
		  N_coord[0] = nodes_x_v[index_v+Nx];
		  N_coord[1] = nodes_y_v[index_v+Nx];
		  N_coord[2] = nodes_z_v[index_v+Nx];

		  /* Find intersection with solid body along y axis */
		  norm_v[0] = 0;
		  norm_v[1] = 1 * curve_factor;
		  norm_v[2] = 0;
		  int_curv(x_int, N_coord, norm_v, pc_temp, cell_size);
	      
		  /* South neighbour: point on solid surface */
		  copy_vec(S_coord, x_int);
		}
	      }
	      else{
		/* Interpolate to solid surface */
		if(N_is_solid_OK){
		  /* South neighbour */
		  S_coord[0] = nodes_x_v[index_v];
		  S_coord[1] = nodes_y_v[index_v];
		  S_coord[2] = nodes_z_v[index_v];

		  /* Find centroid of cut face */

		  face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);

		  diff_vec(norm_v, S_coord, x_centroid);
		  scale_vec(norm_v, curve_factor);
		  normalize_vec(norm_v);
		  int_curv(x_int, S_coord, norm_v, pc_temp, cell_size);

		  /* North neighbour: point on solid surface */
		  copy_vec(N_coord, x_int);
		}
		else{
		  /* North neighbour */
		  N_coord[0] = nodes_x_v[index_v+Nx];
		  N_coord[1] = nodes_y_v[index_v+Nx];
		  N_coord[2] = nodes_z_v[index_v+Nx];

		  /* Find centroid of cut face */
		  face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);

		  diff_vec(norm_v, N_coord, x_centroid);
		  scale_vec(norm_v, curve_factor);
		  normalize_vec(norm_v);
		  int_curv(x_int, N_coord, norm_v, pc_temp, cell_size);

		  /* South neighbour: point on solid surface */
		  copy_vec(S_coord, x_int);
		}	    
	      }

#ifdef OLDINT_OK
	      N_coord[0] = nodes_x_v[index_v+Nx];
	      N_coord[1] = nodes_y_v[index_v+Nx];
	      N_coord[2] = nodes_z_v[index_v+Nx];

	      S_coord[0] = nodes_x_v[index_v];
	      S_coord[1] = nodes_y_v[index_v];
	      S_coord[2] = nodes_z_v[index_v];	      
#endif
	      
	      face_coord[1] = nodes_y[index_];
	      face_coord[0] = S_coord[0] + (face_coord[1] - S_coord[1]) *
		(N_coord[0] - S_coord[0]) / (N_coord[1] - S_coord[1]);
	      face_coord[2] = S_coord[2] + (face_coord[1] - S_coord[1]) *
		(N_coord[2] - S_coord[2]) / (N_coord[1] - S_coord[1]);

	      diff_vec(S, N_coord, S_coord);

	      if(fabs(polyn(face_coord, pc_temp)) > pol_tol){
		calc_dist_norm(face_coord, pc_temp, &delta_h, norm_v, cell_size);
#pragma omp critical
		{
		  diff_v_face_y_index[diff_v_face_y_count] = index_;
		  diff_v_face_y_index_v[diff_v_face_y_count] = index_v;
		  diff_v_face_y_J[diff_v_face_y_count] = J;
		  diff_v_face_y_Delta[diff_v_face_y_count] = S[1];
		  diff_factor_v_face_y[diff_v_face_y_count] = v_faces_y[index_] * (S[0] * norm_v[0] + S[2] * norm_v[2]) *
		    curve_factor / (S[1] * delta_h);
		  diff_v_face_y_count++;
		}
	      }
	      else{
#pragma omp critical
		{		
		  diff_v_face_y_index[diff_v_face_y_count] = index_;
		  diff_v_face_y_index_v[diff_v_face_y_count] = index_v;
		  diff_v_face_y_J[diff_v_face_y_count] = J;
		  diff_v_face_y_Delta[diff_v_face_y_count] = S[1];
		  diff_factor_v_face_y[diff_v_face_y_count] = 0;
		  diff_v_face_y_count++;		  
		}
	      }
	    } // (some_face_cut_OK)
	  } // if(v_faces_y[index_] > tol * Delta_x[index_] * Delta_z[index_])
	}
      }
    }
  } // if(diff_v_face_y_count > 0)


  /* alpha_v_x just needed for QUICK scheme */

  if(flow_scheme == 1){  

#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_v, dist_w, dist_e, dist_f, some_face_cut_OK, \
	  coeffs_ind, curv_ind, pc_temp, face_lower_corner, face_upper_corner, \
	  W_coord, E_coord, face_center_coord, den)			\
  shared(v_cut_face_x, p_cut_face_y, nodes_x_v, nodes_y_v, nodes_z_v,	\
	 Delta_y_v, Delta_z_v, curve_factor, v_faces_x, alpha_v_x, Delta_xE_v, Delta_xe_v, \
	 poly_coeffs, curve_is_solid_OK, vertex_x, vertex_y_v, vertex_z)

    for(K=1; K<Nz-1; K++){
      for(j=0; j<ny; j++){
	for(i=0; i<nx; i++){

	  I = i + 1;
	  J = j + 1;
	  k = K - 1;	
	  index_ = K*nx*ny + j*nx + i;
	  index_v = K*Nx*ny + j*Nx + I;

	  if(v_faces_x[index_] > tol * Delta_y_v[index_v] * Delta_z_v[index_v]){

	    some_face_cut_OK = v_cut_face_x[index_] || p_cut_face_y[index_v]
	      || p_cut_face_y[index_v-1];

	    if(some_face_cut_OK){

	      curv_ind = some_face_cut_OK - 1;
	    
	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }
	    
	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	    
	      /* West neighbour */
	      W_coord[0] = nodes_x_v[index_v-1];
	      W_coord[1] = nodes_y_v[index_v-1];
	      W_coord[2] = nodes_z_v[index_v-1];
	      dist_w = calc_dist(W_coord, pc_temp, cell_size);

	      /* East neighbour */
	      E_coord[0] = nodes_x_v[index_v];
	      E_coord[1] = nodes_y_v[index_v];
	      E_coord[2] = nodes_z_v[index_v];
	      dist_e = calc_dist(E_coord, pc_temp, cell_size);

	      face_lower_corner[0] = vertex_x[i];
	      face_lower_corner[1] = vertex_y_v[J-1];
	      face_lower_corner[2] = vertex_z[k];

	      face_upper_corner[0] = face_lower_corner[0];
	      face_upper_corner[1] = vertex_y_v[J];
	      face_upper_corner[2] = vertex_z[k+1];

	      face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 0);
	  
	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_e * Delta_xe_v[index_v-1] + dist_w * (Delta_xE_v[index_v-1] - Delta_xe_v[index_v-1]);
	    
	      alpha_v_x[index_] = (den <= 0) ? 1 : dist_f * Delta_xE_v[index_v-1] / den;

	    }
	  } /* if(v_faces_x[index_] > tol * Delta_y_v[index_v] * Delta_z_v[index_v]) */
	} /* for(i=0; i<nx; i++) */
      } /* for(j=0; j<ny; j++) */
    } /* for(K=1; K<Nz-1; K++) */
  
  }

  /* alpha_v_z just needed for QUICK scheme */

  if(flow_scheme == 1){
    
#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_v, dist_t, dist_b, dist_f, some_face_cut_OK, \
	  coeffs_ind, curv_ind, pc_temp, face_lower_corner, face_upper_corner, \
	  B_coord, T_coord, face_center_coord, den)			\
  shared(v_cut_face_z, p_cut_face_y, nodes_x_v, nodes_y_v, nodes_z_v,	\
	 Delta_x_v, Delta_y_v, curve_factor, v_faces_z, alpha_v_z, Delta_zt_v, Delta_zT_v, \
	 poly_coeffs, curve_is_solid_OK, vertex_x, vertex_y_v, vertex_z)
  
    for(k=0; k<nz; k++){
      for(j=0; j<ny; j++){
	for(I=1; I<Nx-1; I++){

	  i = I - 1;
	  J = j + 1;
	  K = k + 1;
	  index_ = k*Nx*ny + j*Nx + I;
	  index_v = K*Nx*ny + j*Nx + I;

	  if(v_faces_z[index_] > tol * Delta_x_v[index_v] * Delta_y_v[index_v]){

	    some_face_cut_OK = v_cut_face_z[index_] || p_cut_face_y[index_v]
	      || p_cut_face_y[index_v-Nx*ny];

	    if(some_face_cut_OK){

	      curv_ind = some_face_cut_OK - 1;	    

	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      // Bottom neighbour
	      B_coord[0] = nodes_x_v[index_v-Nx*ny];
	      B_coord[1] = nodes_y_v[index_v-Nx*ny];
	      B_coord[2] = nodes_z_v[index_v-Nx*ny];
	      dist_b = calc_dist(B_coord, pc_temp, cell_size);

	      // Top neighbour
	      T_coord[0] = nodes_x_v[index_v];
	      T_coord[1] = nodes_y_v[index_v];
	      T_coord[2] = nodes_z_v[index_v];
	      dist_t = calc_dist(T_coord, pc_temp, cell_size);

	      face_lower_corner[0] = vertex_x[i];
	      face_lower_corner[1] = vertex_y_v[J-1];
	      face_lower_corner[2] = vertex_z[k];

	      face_upper_corner[0] = vertex_x[i+1];
	      face_upper_corner[1] = vertex_y_v[J];
	      face_upper_corner[2] = face_lower_corner[2];

	      face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);
			       
	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_t * Delta_zt_v[index_v-Nx*ny] + dist_b * (Delta_zT_v[index_v-Nx*ny] - Delta_zt_v[index_v-Nx*ny]);

	      alpha_v_z[index_] = (den <= 0) ? 1 : dist_f * Delta_zT_v[index_v-Nx*ny] / den;
	      
	    }	  
	  } /* if(v_faces_z[index_] > tol * Delta_x_v[index_v] * Delta_y_v[index_v]) */
	} /* for(I=1; I<Nx-1; I++) */
      } /* for(j=0; j<ny; j++) */
    } /* for(k=0; k<nz; k++) */

  }

  
  /* w cut cells */

  /* alpha_w_z */

#pragma omp parallel for default(none) private(I, J, K, k, index_, index_w) \
  shared(w_faces_z, Delta_x_w, Delta_y_w, w_cut_ft, w_cut_fb, nodes_z, nodes_z_w)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	k = K - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	if(w_faces_z[index_] > tol * Delta_x_w[index_w] * Delta_y_w[index_w]){

	  /* Default values */	    
	  w_cut_ft[index_] = (nodes_z[index_] - nodes_z_w[index_w]) / 
	    (nodes_z_w[index_w+Nx*Ny] - nodes_z_w[index_w]);
	  w_cut_fb[index_] = 1 - w_cut_ft[index_];

	}
	else{
	  /* Interior face */
	  w_cut_ft[index_] = 0;
	  w_cut_fb[index_] = 0;
	}
      }
    }
  }

#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_w, index_v, dist_t, dist_b, dist_f, fT, fB, \
	  some_face_cut_OK, B_is_solid_OK, T_is_solid_OK, pc_temp,	\
	  curv_ind, coeffs_ind, curve_factor, x_centroid,		\
	  face_lower_corner, face_upper_corner, x_int, B_coord,		\
	  T_coord, face_center_coord, norm_v, den)			\
  shared(w_faces_z, Delta_x_w, Delta_y_w, w_cut_face_z, p_cut_face_z, nodes_z, \
	 w_dom_matrix, nodes_x_w,					\
	 nodes_y_w, nodes_z_w, alpha_w_z, w_cut_fb, w_cut_ft, poly_coeffs, curve_is_solid_OK, \
	 vertex_x, vertex_y, vertex_z_w, Delta_zT_w)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	i = I - 1;
	j = J - 1;
	k = K - 1;
	index_w = k*Nx*Ny + J*Nx + I;	
	index_ = K*Nx*Ny + J*Nx + I;

	if(w_faces_z[index_] > tol * Delta_x_w[index_w] * Delta_y_w[index_w]){
	  /* Any of these conditions imply that the interpolated velocity is not 
	     aligned with the velocity nodes used for interpolation */

	  some_face_cut_OK = (w_cut_face_z[index_] || p_cut_face_z[index_w]
			      || p_cut_face_z[index_w+Nx*Ny]);
	  B_is_solid_OK = w_dom_matrix[index_w];
	  T_is_solid_OK = w_dom_matrix[index_w+Nx*Ny];
	  
	  if(some_face_cut_OK || B_is_solid_OK || T_is_solid_OK){
	    /* Need to calculate an alpha_value */

	    curv_ind = (some_face_cut_OK)? (some_face_cut_OK-1) : (B_is_solid_OK)? (B_is_solid_OK-1) : (T_is_solid_OK-1);
	    
	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	    
	    /* Face corners */
	    
	    face_lower_corner[0] = vertex_x[i];
	    face_lower_corner[1] = vertex_y[j];
	    face_lower_corner[2] = vertex_z_w[K];

	    face_upper_corner[0] = vertex_x[i+1];
	    face_upper_corner[1] = vertex_y[j+1];
	    face_upper_corner[2] = face_lower_corner[2];

	    if((!B_is_solid_OK) && (!T_is_solid_OK)){
	      /* Both B and T nodes in fluid domain */

	      /* North neighbour */
	      T_coord[0] = nodes_x_w[index_w+Nx*Ny];
	      T_coord[1] = nodes_y_w[index_w+Nx*Ny];
	      T_coord[2] = nodes_z_w[index_w+Nx*Ny];

	      /* South neighbour */
	      B_coord[0] = nodes_x_w[index_w];
	      B_coord[1] = nodes_y_w[index_w];
	      B_coord[2] = nodes_z_w[index_w];

	      w_cut_ft[index_] = (nodes_z[index_] - nodes_z_w[index_w]) / Delta_zT_w[index_w];
	      w_cut_fb[index_] = 1 - w_cut_ft[index_];
	    
	    }
	    else if (w_cut_face_z[index_] == 0){
	      /* Interpolate on y axis direction */
	      if(T_is_solid_OK){

		/* South neighbour */
		B_coord[0] = nodes_x_w[index_w];
		B_coord[1] = nodes_y_w[index_w];
		B_coord[2] = nodes_z_w[index_w];
		
		/* Find intersection with solid body along z axis */
		norm_v[0] = 0;
		norm_v[1] = 0;
		norm_v[2] = -1 * curve_factor;
		int_curv(x_int, B_coord, norm_v, pc_temp, cell_size); 
	      
		/* North neighbour: point on solid surface */
		copy_vec(T_coord, x_int);

		w_cut_fb[index_] = 1 - (nodes_z[index_] - B_coord[2]) / (T_coord[2] - B_coord[2]);
		w_cut_ft[index_] = 0;
		
	      }	    
	      else{
		
		/* North neighbour */
		T_coord[0] = nodes_x_w[index_w+Nx*Ny];
		T_coord[1] = nodes_y_w[index_w+Nx*Ny];
		T_coord[2] = nodes_z_w[index_w+Nx*Ny];

		/* Find intersection with solid body along y axis */
		
		norm_v[0] = 0;
		norm_v[1] = 0;
		norm_v[2] = 1 * curve_factor;
		int_curv(x_int, T_coord, norm_v, pc_temp, cell_size);
				
		/* South neighbour: point on solid surface */
		copy_vec(B_coord, x_int);

		w_cut_fb[index_] = 0;
		w_cut_ft[index_] = (nodes_z[index_] - B_coord[2]) / (T_coord[2] - B_coord[2]);
	      }
	    }
	    else{
	      /* Interpolate to solid surface */
	      if(T_is_solid_OK){

		/* South neighbour */
		B_coord[0] = nodes_x_w[index_w];
		B_coord[1] = nodes_y_w[index_w];
		B_coord[2] = nodes_z_w[index_w];

		/* Find centroid of cut face */

		face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);

		diff_vec(norm_v, B_coord, x_centroid);
		scale_vec(norm_v, curve_factor);
		normalize_vec(norm_v);
		int_curv(x_int, B_coord, norm_v, pc_temp, cell_size);

		/* N neighbour: point on solid surface */
		copy_vec(T_coord, x_int);
		
		w_cut_fb[index_] = 1 - (nodes_z[index_] - B_coord[2]) /(T_coord[2] - B_coord[2]);
		w_cut_ft[index_] = 0;

	      }
	      else{

		/* North neighbour */
		T_coord[0] = nodes_x_w[index_w+Nx*Ny];
		T_coord[1] = nodes_y_w[index_w+Nx*Ny];
		T_coord[2] = nodes_z_w[index_w+Nx*Ny];

		/* Find centroid of cut face */

		face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);

		diff_vec(norm_v, T_coord, x_centroid);
		scale_vec(norm_v, curve_factor);
		normalize_vec(norm_v);
		int_curv(x_int, T_coord, norm_v, pc_temp, cell_size);
		
		/* South neighbour: point on solid surface */
		copy_vec(B_coord, x_int);

		w_cut_ft[index_] = (nodes_z[index_] - B_coord[2]) / (T_coord[2] - B_coord[2]);
		w_cut_fb[index_] = 0;
	      }	    
	    }

#ifdef OLDINT_OK
	    /* Top neighbour */
	    T_coord[0] = nodes_x_w[index_w+Nx*Ny];
	    T_coord[1] = nodes_y_w[index_w+Nx*Ny];
	    T_coord[2] = nodes_z_w[index_w+Nx*Ny];

	    /* Bottom neighbour */
	    B_coord[0] = nodes_x_w[index_w];
	    B_coord[1] = nodes_y_w[index_w];
	    B_coord[2] = nodes_z_w[index_w];

	    w_cut_ft[index_] = (nodes_z[index_] - B_coord[2]) /
	      (T_coord[2] - B_coord[2]);
	    w_cut_fb[index_] = 1 - w_cut_ft[index_];
	    
#endif

	    /* Common for all three cases */

	    /* Distance from north neighbour to the curve */
	    dist_t = calc_dist(T_coord, pc_temp, cell_size);
	    /* Distance from south neighbour to the curve */
	    dist_b = calc_dist(B_coord, pc_temp, cell_size);

	    /* This is supposed to be the node in between the u_P and u_E nodes */

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);
	  
	    dist_f = calc_dist(face_center_coord, pc_temp, cell_size);
	  
	    fT = (face_center_coord[2] - B_coord[2]) / (T_coord[2] - B_coord[2]);
	    fB = 1 - fT;

	    /* Build alpha based on distances computed, see eqs 18 to 21 Kirkpatrick
	       This affects convective terms only */
	    den = dist_t * fT + dist_b * fB;
	    
	    alpha_w_z[index_] = (den <= 0) ? 1 : dist_f / den;

	  } /* 	if(some_face_cut_OK || S_is_solid_OK || N_is_solid_OK) */
	} /* 	if(v_faces_y[index_] > tol * Delta_x[index_]) */
      }
    }
  }

  /* diff_factor_w_face_z */

#pragma omp parallel for default(none)					\
  private(I, J, K, k, index_, index_w, B_is_solid_OK, T_is_solid_OK, some_face_cut_OK) \
  shared(w_faces_z, Delta_x_w, Delta_y_w, w_dom_matrix, w_cut_face_z, p_cut_face_z) \
  reduction(+: diff_w_face_z_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	k = K - 1;
	index_ = K*Nx*Ny + J*Nx + I;
	index_w = k*Nx*Ny + J*Nx + I;

	if(w_faces_z[index_] > tol * Delta_x_w[index_w] * Delta_y_w[index_w]){

	  B_is_solid_OK = w_dom_matrix[index_w];
	  T_is_solid_OK = w_dom_matrix[index_w+Nx*Ny];
	  some_face_cut_OK = w_cut_face_z[index_] || p_cut_face_z[index_w] 
	    || p_cut_face_z[index_w+Nx*Ny];

	  if(some_face_cut_OK || B_is_solid_OK || T_is_solid_OK){
	    diff_w_face_z_count++;
	  }
	}
      }
    }
  }

  if(diff_w_face_z_count > 0){

    data->diff_w_face_z_index = create_array_int(diff_w_face_z_count, 1, 1, 0);
    data->diff_w_face_z_index_w = create_array_int(diff_w_face_z_count, 1, 1, 0);
    data->diff_w_face_z_K = create_array_int(diff_w_face_z_count, 1, 1, 0);
    data->diff_w_face_z_Delta = create_array(diff_w_face_z_count, 1, 1, 0);
    data->diff_factor_w_face_z = create_array(diff_w_face_z_count, 1, 1, 0);

    data->diff_w_face_z_count = diff_w_face_z_count;

    diff_w_face_z_count = 0;

    int *diff_w_face_z_index = data->diff_w_face_z_index;
    int *diff_w_face_z_index_w = data->diff_w_face_z_index_w;
    int *diff_w_face_z_K = data->diff_w_face_z_K;
    double *diff_w_face_z_Delta = data->diff_w_face_z_Delta;
    double *diff_factor_w_face_z = data->diff_factor_w_face_z;    
    
#pragma omp parallel for default(none) \
  private(I, J, K, k, i, j, index_, index_w, some_face_cut_OK, delta_h, T_is_solid_OK, B_is_solid_OK, \
	  coeffs_ind, curv_ind, pc_temp, curve_factor, face_lower_corner, face_upper_corner, \
	  x_centroid, face_coord, norm_v, S, T_coord, B_coord, x_int)	\
  shared(w_cut_face_z, p_cut_face_z, nodes_x_w, nodes_y_w, nodes_z_w,	\
	 diff_factor_w_face_z, w_faces_z, Delta_x_w, Delta_y_w, w_dom_matrix, \
	 nodes_z, diff_w_face_z_index, diff_w_face_z_index_w, diff_w_face_z_Delta, \
	 diff_w_face_z_count, poly_coeffs, vertex_x, vertex_y, vertex_z_w, curve_is_solid_OK, diff_w_face_z_K)

    for(K=1; K<Nz-1; K++){    
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  i = I - 1;
	  j = J - 1;
	  k = K - 1;
	  index_ = K*Nx*Ny + J*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;

	  if(w_faces_z[index_] > tol * Delta_x_w[index_w] * Delta_y_w[index_w]){

	    B_is_solid_OK = w_dom_matrix[index_w];
	    T_is_solid_OK = w_dom_matrix[index_w+Nx*Ny];
	    some_face_cut_OK = w_cut_face_z[index_] || p_cut_face_z[index_w] 
	      || p_cut_face_z[index_w+Nx*Ny];

	    if(some_face_cut_OK || B_is_solid_OK || T_is_solid_OK){

	      curv_ind = (some_face_cut_OK)? (some_face_cut_OK - 1) : (B_is_solid_OK)? (B_is_solid_OK - 1) : (T_is_solid_OK - 1);
	      
	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      /* Face corners */
	      face_lower_corner[0] = vertex_x[i];
	      face_lower_corner[1] = vertex_y[j];
	      face_lower_corner[2] = vertex_z_w[K];

	      face_upper_corner[0] = vertex_x[i+1];
	      face_upper_corner[1] = vertex_y[j+1];
	      face_upper_corner[2] = face_lower_corner[2];
	      
	      if((!B_is_solid_OK) && (!T_is_solid_OK)){
		/* Top neighbour */
		T_coord[0] = nodes_x_w[index_w+Nx*Ny];
		T_coord[1] = nodes_y_w[index_w+Nx*Ny];
		T_coord[2] = nodes_z_w[index_w+Nx*Ny];

		/* Bottom neighbour */
		B_coord[0] = nodes_x_w[index_w];
		B_coord[1] = nodes_y_w[index_w];
		B_coord[2] = nodes_z_w[index_w];
	      }
	      else if (w_cut_face_z[index_] == 0){
		/* Interpolate on z axis direction */
		if(T_is_solid_OK){
		  /* Bottom neighbour */
		  B_coord[0] = nodes_x_w[index_w];
		  B_coord[1] = nodes_y_w[index_w];
		  B_coord[2] = nodes_z_w[index_w];

		  /* Find intersection with solid body along y axis */
		  norm_v[0] = 0;
		  norm_v[1] = 0;
		  norm_v[2] = -1 * curve_factor;
		  int_curv(x_int, B_coord, norm_v, pc_temp, cell_size);

		  /* Top neighbour: point on solid surface */
		  copy_vec(T_coord, x_int);
		}	    
		else{
		  /* Top neighbour */
		  T_coord[0] = nodes_x_w[index_w+Nx*Ny];
		  T_coord[1] = nodes_y_w[index_w+Nx*Ny];
		  T_coord[2] = nodes_z_w[index_w+Nx*Ny];

		  /* Find intersection with solid body along y axis */
		  norm_v[0] = 0;
		  norm_v[1] = 0;
		  norm_v[2] = 1 * curve_factor;
		  int_curv(x_int, T_coord, norm_v, pc_temp, cell_size);
	      
		  /* Bottom neighbour: point on solid surface */
		  copy_vec(B_coord, x_int);
		}
	      }
	      else{
		/* Interpolate to solid surface */
		if(T_is_solid_OK){
		  /* Bottom neighbour */
		  B_coord[0] = nodes_x_w[index_w];
		  B_coord[1] = nodes_y_w[index_w];
		  B_coord[2] = nodes_z_w[index_w];

		  /* Find centroid of cut face */

		  face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);

		  diff_vec(norm_v, B_coord, x_centroid);
		  scale_vec(norm_v, curve_factor);
		  normalize_vec(norm_v);
		  int_curv(x_int, B_coord, norm_v, pc_temp, cell_size);

		  /* Top neighbour: point on solid surface */
		  copy_vec(T_coord, x_int);
		}
		else{
		  /* Top neighbour */
		  T_coord[0] = nodes_x_w[index_w+Nx*Ny];
		  T_coord[1] = nodes_y_w[index_w+Nx*Ny];
		  T_coord[2] = nodes_z_w[index_w+Nx*Ny];
	      
		  /* Find centroid of cut face */
		  face_centroid(x_centroid, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);

		  diff_vec(norm_v, T_coord, x_centroid);
		  scale_vec(norm_v, curve_factor);
		  normalize_vec(norm_v);
		  int_curv(x_int, T_coord, norm_v, pc_temp, cell_size);

		  /* South neighbour: point on solid surface */
		  copy_vec(B_coord, x_int);
		}	    
	      }

#ifdef OLDINT_OK
	      T_coord[0] = nodes_x_w[index_w+Nx*Ny];
	      T_coord[1] = nodes_y_w[index_w+Nx*Ny];
	      T_coord[2] = nodes_z_w[index_w+Nx*Ny];

	      B_coord[0] = nodes_x_w[index_w];
	      B_coord[1] = nodes_y_w[index_w];
	      B_coord[2] = nodes_z_w[index_w];
#endif
	      

	      face_coord[2] = nodes_z[index_];
	      face_coord[0] = B_coord[0] + (face_coord[2] - B_coord[2]) *
		(T_coord[0] - B_coord[0]) / (T_coord[2] - B_coord[2]);
	      face_coord[1] = B_coord[1] + (face_coord[2] - B_coord[2]) *
		(T_coord[1] - B_coord[1]) / (T_coord[2] - B_coord[2]);

	      diff_vec(S, T_coord, B_coord);

	      if(fabs(polyn(face_coord, pc_temp)) > pol_tol){
		calc_dist_norm(face_coord, pc_temp, &delta_h, norm_v, cell_size);
#pragma omp critical
		{
		  diff_w_face_z_index[diff_w_face_z_count] = index_;
		  diff_w_face_z_index_w[diff_w_face_z_count] = index_w;
		  diff_w_face_z_K[diff_w_face_z_count] = K;
		  diff_w_face_z_Delta[diff_w_face_z_count] = S[2];
		  diff_factor_w_face_z[diff_w_face_z_count] = w_faces_z[index_] * ( S[0] * norm_v[0] + S[1] * norm_v[1]) *
		    curve_factor / (S[2] * delta_h);
		  diff_w_face_z_count++;
		}
	      }
	      else{
#pragma omp critical
		{		
		  diff_w_face_z_index[diff_w_face_z_count] = index_;
		  diff_w_face_z_index_w[diff_w_face_z_count] = index_w;
		  diff_w_face_z_K[diff_w_face_z_count] = K;
		  diff_w_face_z_Delta[diff_w_face_z_count] = S[2];
		  diff_factor_w_face_z[diff_w_face_z_count] = 0;
		  diff_w_face_z_count++;		  
		}
	      }
	    } // (some_face_cut_OK)
	  } // if(w_faces_z[index_] > tol * Delta_x_w[index_w] * Delta_y_w[index_w])
	}
      }
    }
  } // if(diff_w_face_z_count > 0)

  /* alpha_w_x just needed for QUICK scheme */

  if(flow_scheme == 1){
  
#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_w, dist_e, dist_w, dist_f, some_face_cut_OK, \
	  coeffs_ind, curv_ind, pc_temp, face_lower_corner, face_upper_corner, \
	  W_coord, E_coord, face_center_coord, den)			\
  shared(w_cut_face_x, p_cut_face_z, nodes_x_w, nodes_y_w, nodes_z_w,	\
	 Delta_y_w, Delta_z_w, curve_factor, w_faces_x, alpha_w_x, Delta_xe_w, Delta_xE_w, \
	 poly_coeffs, curve_is_solid_OK, vertex_x, vertex_y, vertex_z_w)

    for(k=0; k<nz; k++){
      for(J=1; J<Ny-1; J++){
	for(i=0; i<nx; i++){

	  I = i + 1;
	  j = J - 1;
	  K = k + 1;
	  index_ = k*nx*Ny + J*nx + i;
	  index_w = k*Nx*Ny + J*Nx + I;

	  if(w_faces_x[index_] > tol * Delta_y_w[index_w] * Delta_z_w[index_w]){

	    some_face_cut_OK = w_cut_face_x[index_] || p_cut_face_z[index_w]
	      || p_cut_face_z[index_w-1];

	    if(some_face_cut_OK){

	      curv_ind = some_face_cut_OK - 1;

	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      /* West neighbour */
	      W_coord[0] = nodes_x_w[index_w-1];
	      W_coord[1] = nodes_y_w[index_w-1];
	      W_coord[2] = nodes_z_w[index_w-1];
	      dist_w = calc_dist(W_coord, pc_temp, cell_size);

	      /* East neighbour */
	      E_coord[0] = nodes_x_w[index_w];
	      E_coord[1] = nodes_y_w[index_w];
	      E_coord[2] = nodes_z_w[index_w];
	      dist_e = calc_dist(E_coord, pc_temp, cell_size);

	      face_lower_corner[0] = vertex_x[i];
	      face_lower_corner[1] = vertex_y[j];
	      face_lower_corner[2] = vertex_z_w[K-1];

	      face_upper_corner[0] = face_lower_corner[0];
	      face_upper_corner[1] = vertex_y[j+1];
	      face_upper_corner[2] = vertex_z_w[K];

	      face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 0);

	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_e * Delta_xe_w[index_w-1] + dist_w * (Delta_xE_w[index_w-1] - Delta_xe_w[index_w-1]);

	      alpha_w_x[index_] = (den <= 0) ? 1 : dist_f * Delta_xE_w[index_w-1] / den;
	      
	    }
	  } /* if(w_faces_x[index_] > tol * Delta_y_w[index_w] * Delta_z_w[index_w]) */
	} /* for(i=0; i<nx; i++) */
      } /* for(J=1; J<Ny-1; J++) */
    } /* for(k=0; k<nz; k++) */

  }

  /* alpha_w_y just needed for QUICK scheme */

  if(flow_scheme == 1){
  
#pragma omp parallel for default(none)					\
  private(i, j, k, I, J, K, index_, index_w, dist_s, dist_n, dist_f, some_face_cut_OK, \
	  coeffs_ind, curv_ind, pc_temp, face_lower_corner, face_upper_corner, \
	  S_coord, N_coord, face_center_coord, den)			\
  shared(w_cut_face_y, p_cut_face_z, nodes_x_w, nodes_y_w, nodes_z_w,	\
	 Delta_x_w, Delta_z_w, curve_factor, w_faces_y, alpha_w_y, Delta_yn_w, Delta_yN_w, \
	 poly_coeffs, curve_is_solid_OK, vertex_x, vertex_y, vertex_z_w)

    for(k=0; k<nz; k++){
      for(j=0; j<ny; j++){
	for(I=1; I<Nx-1; I++){
	
	  i = I - 1;
	  J = j + 1;
	  K = k + 1;
	  index_ = k*Nx*ny + j*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;

	  if(w_faces_y[index_] > tol * Delta_x_w[index_w] * Delta_z_w[index_w]){

	    some_face_cut_OK = w_cut_face_y[index_] || p_cut_face_z[index_w]
	      || p_cut_face_z[index_w-Nx];

	    if(some_face_cut_OK){

	      curv_ind = some_face_cut_OK - 1;

	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      /* South neighbour */
	      S_coord[0] = nodes_x_w[index_w-Nx];
	      S_coord[1] = nodes_y_w[index_w-Nx];
	      S_coord[2] = nodes_z_w[index_w-Nx];
	      dist_s = calc_dist(S_coord, pc_temp, cell_size);

	      /* North neighbour */
	      N_coord[0] = nodes_x_w[index_w];
	      N_coord[1] = nodes_y_w[index_w];
	      N_coord[2] = nodes_z_w[index_w];
	      dist_n = calc_dist(N_coord, pc_temp, cell_size);

	      face_lower_corner[0] = vertex_x[i];
	      face_lower_corner[1] = vertex_y[j];
	      face_lower_corner[2] = vertex_z_w[K-1];

	      face_upper_corner[0] = vertex_x[i+1];
	      face_upper_corner[1] = face_lower_corner[1];
	      face_upper_corner[2] = vertex_z_w[K];

	      face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);

	      dist_f = calc_dist(face_center_coord, pc_temp, cell_size);

	      den = dist_n * Delta_yn_w[index_w-Nx] + dist_s * (Delta_yN_w[index_w-Nx] - Delta_yn_w[index_w-Nx]);
	      
	      alpha_w_y[index_] = (den <= 0) ? 1 : dist_f * Delta_yN_w[index_w-Nx] / den;
	      
	    }
	  } /* if(w_faces_y[index_] > tol * Delta_x_w[index_w] * Delta_z_w[index_w]) */
	} /* for(I=1; I<Nx-1; I++) */
      } /* for(j=0; j<ny; j++) */
    } /* for(k=0; k<nz; k++) */

  }
  
  return;
}

/* COMPUTE_FNSEWTB_U_V_W_CUT_CELLS_3D */
/*****************************************************************************/
/**

  Sets: 

  Array (int, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_index: face_y on u cell where 
  Data_Mem::Fu_y is interpolated near solid.

  Array (int, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_index_v: v cell nodes used for
  interpolation of Data_Mem::Fu_y near solid.

  Array (double, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_W: weight used for interpolation
  of Data_Mem::Fu_y near solid.

  Array (double, Data_Mem::u_cut_face_Fy_count) Data_Mem::u_cut_face_Fy_E: weight used for interpolation 
  of Data_Mem::Fu_y near solid.

  Array (int, Data_Mem::u_cut_face_Fz_count) Data_Mem::u_cut_face_Fz_index: face_z on u cell where
  Data_Mem::Fu_z is interpolated near solid.

  Array (int, Data_Mem::u_cut_face_Fz_count) Data_Mem::u_cut_face_Fz_index_w: w cell nodes used for 
  interpolation of Data_Mem::Fu_z near solid.

  Array (double, Data_Mem::u_cut_face_Fz_count) Data_Mem::u_cut_face_Fz_W: weight used for interpolation 
  of Data_Mem::Fu_z near solid.

  Array (double, Data_Mem::u_cut_face_Fz_count) Data_Mem::u_cut_face_Fz_E: weight used for interpolation
  of Data_Mem::Fu_z near solid.

  Array (int, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_index: face_x on v cell where 
  Data_Mem::Fv_x  is interpolated near solid.

  Array (int, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_index_u: u cell nodes used for 
  interpolation of Data_Mem::Fv_x near solid.

  Array (double, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_S: weight used for interpolation 
  of Data_Mem::Fv_x near solid.

  Array (double, Data_Mem::v_cut_face_Fx_count) Data_Mem::v_cut_face_Fx_N: weight used for interpolation
  of Data_Mem::Fv_x near solid.

  Array (int, Data_Mem::v_cut_face_Fz_count) Data_Mem::v_cut_face_Fz_index: face_z on v cell where
  Data_Mem::Fv_z is interpolated near solid.

  Array (int, Data_Mem::v_cut_face_Fz_count) Data_Mem::v_cut_face_Fz_index_w: w cell nodes used for
  interpolation of Data_Mem::Fv_z near solid.

  Array (double, Data_Mem::v_cut_face_Fz_count) Data_Mem::v_cut_face_Fz_S: weight used for interpolation
  of Data_Mem::Fv_z near solid.

  Array (double, Data_Mem::v_cut_face_Fz_count) Data_Mem::v_cut_face_Fz_N: weight used for interpolation
  of Data_Mem::Fv_z near solid.

  Array (int, Data_Mem::w_cut_face_Fx_count) Data_Mem::w_cut_face_Fx_index: face_x on w cell where 
  Data_Mem::Fw_x is interpolated near solid.

  Array (int, Data_Mem::w_cut_face_Fx_count) Data_Mem::w_cut_face_Fx_index_u: u cell nodes used for 
  interpolation of Data_Mem::Fw_x near solid.

  Array (double, Data_Mem::w_cut_face_Fx_count) Data_Mem::w_cut_face_Fx_B: weight used for interpolation 
  of Data_Mem::Fw_x near solid.

  Array (double, Data_Mem::w_cut_face_Fx_count) Data_Mem::w_cut_face_Fx_T: weight used for interpolation
  of Data_Mem::Fw_x near solid.

  Array (int, Data_Mem::w_cut_face_Fy_count) Data_Mem::w_cut_face_Fy_index: face_y on w cell where
  Data_Mem::Fw_y is interpolated near solid.

  Array (int, Data_Mem::w_cut_face_Fy_count) Data_Mem::w_cut_face_Fy_index_v: v cell nodes used for
  interpolation of Data_Mem::Fw_y near solid.

  Array (double, Data_Mem::w_cut_face_Fy_count) Data_Mem::w_cut_face_Fy_B: weight used for interpolation
  of Data_Mem::Fw_y near solid.

  Array (double, Data_Mem::w_cut_face_Fy_count) Data_Mem::w_cut_face_Fy_T: weight used for interpolation
  of Data_Mem::Fw_y near solid.

  @param data

  @return 0 (ret_value not fully implemented).
  
  MODIFIED: 03-09-2020
*/
int compute_fnsewtb_u_v_w_cut_cells(Data_Mem *data){

  int I, J, K;
  int i, j, k;
  int index_u, index_v, index_w;
  int index_face, curv_ind, coeffs_ind;
  int u_cut_face_Fy_count = 0;
  int u_cut_face_Fz_count = 0;
  int v_cut_face_Fx_count = 0;
  int v_cut_face_Fz_count = 0;
  int w_cut_face_Fx_count = 0;
  int w_cut_face_Fy_count = 0;  
  int ret_value = 0;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const double tol = 1e-5;

  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;
  const int *p_cut_face_z = data->p_cut_face_z;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;
  const int *curve_is_solid_OK = data->curve_is_solid_OK;  

  double curve_factor;
  double face_lower_corner[3];
  double face_upper_corner[3];
  double face_center_coord[3];
  double v_node[3];
  double norm_v[3];
  double x_int[3];
  double pc_temp[16];

  const double cell_size = data->cell_size;

  const double *poly_coeffs = data->poly_coeffs;
  const double *u_faces_y = data->u_faces_y;
  const double *u_faces_z = data->u_faces_z;
  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_z = data->v_faces_z;
  const double *w_faces_x = data->w_faces_x;
  const double *w_faces_y = data->w_faces_y;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;  
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;
  const double *nodes_x_w = data->nodes_x_w;
  const double *nodes_y_w = data->nodes_y_w;
  const double *nodes_z_w = data->nodes_z_w;  
  const double *Delta_x_u = data->Delta_x_u;
  const double *Delta_y_u = data->Delta_y_u;
  const double *Delta_z_u = data->Delta_z_u;
  const double *Delta_y_v = data->Delta_y_v;
  const double *Delta_x_v = data->Delta_x_v;
  const double *Delta_z_v = data->Delta_z_v;
  const double *Delta_x_w = data->Delta_x_w;
  const double *Delta_y_w = data->Delta_y_w;
  const double *Delta_z_w = data->Delta_z_w;
  const double *vertex_x_u = data->vertex_x_u;
  const double *vertex_y_v = data->vertex_y_v;
  const double *vertex_z_w = data->vertex_z_w;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;

  /* Interpolation factors for Fu_y */

#pragma omp parallel for default(none) private(I, j, K, index_v)	\
  shared(p_cut_face_y) reduction(+: u_cut_face_Fy_count)

  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-2; I++){

	index_v = K*Nx*ny + j*Nx + I;

	if(p_cut_face_y[index_v] || p_cut_face_y[index_v+1]){
	  u_cut_face_Fy_count++;
	}
      }
    }
  }

  if(u_cut_face_Fy_count){

    data->u_cut_face_Fy_W = create_array(u_cut_face_Fy_count, 1, 1, 0);
    data->u_cut_face_Fy_E = create_array(u_cut_face_Fy_count, 1, 1, 0);
    data->u_cut_face_Fy_index = create_array_int(u_cut_face_Fy_count, 1, 1, 0);
    data->u_cut_face_Fy_index_v = create_array_int(u_cut_face_Fy_count, 1, 1, 0);
    data->u_cut_face_Fy_count = u_cut_face_Fy_count;

    double *u_cut_face_Fy_W = data->u_cut_face_Fy_W;
    double *u_cut_face_Fy_E = data->u_cut_face_Fy_E;
    int *u_cut_face_Fy_index = data->u_cut_face_Fy_index;
    int *u_cut_face_Fy_index_v = data->u_cut_face_Fy_index_v;

    u_cut_face_Fy_count = 0;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_u, index_face, index_v, face_lower_corner, \
					       face_upper_corner, face_center_coord, v_node, curv_ind, coeffs_ind, \
					       pc_temp, curve_factor, norm_v, x_int) \
  shared(p_cut_face_y, u_cut_face_Fy_count, u_faces_y, v_dom_matrix, u_cut_face_Fy_index, u_cut_face_Fy_W, \
	 u_cut_face_Fy_E, Delta_x_u, Delta_z_u, u_cut_face_Fy_index_v, vertex_x_u, vertex_y, vertex_z, \
	 nodes_x_v, nodes_y_v, nodes_z_v, poly_coeffs, curve_is_solid_OK)

    for(K=1; K<Nz-1; K++){
      for(j=0; j<ny; j++){
	for(I=1; I<Nx-2; I++){

	  i = I - 1;
	  J = j + 1;
	  k = K - 1;
	  index_face = K*nx*ny + j*nx + i + 1;
	  index_v = K*Nx*ny + j*Nx + I;
	  index_u = K*nx*Ny + J*nx + i + 1;

	  if(p_cut_face_y[index_v] || p_cut_face_y[index_v+1]){

	    face_lower_corner[0] = vertex_x_u[I];
	    face_lower_corner[1] = vertex_y[j];
	    face_lower_corner[2] = vertex_z[k];
	    face_upper_corner[0] = vertex_x_u[I+1];
	    face_upper_corner[1] = face_lower_corner[1];
	    face_upper_corner[2] = vertex_z[k+1];

	    curv_ind = (p_cut_face_y[index_v])? p_cut_face_y[index_v] - 1 : p_cut_face_y[index_v+1] - 1;	    
	    
	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);			    

#pragma omp critical
	    {

	      u_cut_face_Fy_index[u_cut_face_Fy_count] = index_face;
	      u_cut_face_Fy_index_v[u_cut_face_Fy_count] = index_v;

	      if(u_faces_y[index_face] > tol * Delta_x_u[index_u] * Delta_z_u[index_u]){
		if(p_cut_face_y[index_v]){
		  	    
		  if(v_dom_matrix[index_v+1]){
		    /* Solid: east, vW only used for interpolation */
		    v_node[0] = nodes_x_v[index_v];
		    v_node[1] = nodes_y_v[index_v];
		    v_node[2] = nodes_z_v[index_v];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);
		  		  
		    u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (face_center_coord[0] - v_node[0]) / 
		      (x_int[0]  - v_node[0]);
		    u_cut_face_Fy_E[u_cut_face_Fy_count] = 0;

#ifdef OLDINT_OK
		    u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (face_center_coord[0] - nodes_x_v[index_v]) /
		      (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		    u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];
#endif
		  }
		  else{
		    /* Both vW and vE used for interpolation */
		    u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (face_center_coord[0] - nodes_x_v[index_v]) / 
		      (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		    u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];	    	      
		  }
		}
		else{
		  if(v_dom_matrix[index_v]){
		    /* Solid: west, vE only used for interpolation */
		    v_node[0] = nodes_x_v[index_v+1];
		    v_node[1] = nodes_y_v[index_v+1];
		    v_node[2] = nodes_z_v[index_v+1];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);
		    
		    u_cut_face_Fy_W[u_cut_face_Fy_count] = 0;
		    u_cut_face_Fy_E[u_cut_face_Fy_count] = face_center_coord[0] / 
		      (v_node[0] - x_int[0]);

#ifdef OLDINT_OK
		    u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (face_center_coord[0] - nodes_x_v[index_v]) /
		      (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		    u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];
#endif
		  }
		  else{
		    /* Both vW and vE used for interpolation */ 
		    u_cut_face_Fy_W[u_cut_face_Fy_count] = 1 - (face_center_coord[0] - nodes_x_v[index_v]) / 
		      (nodes_x_v[index_v+1] - nodes_x_v[index_v]);
		    u_cut_face_Fy_E[u_cut_face_Fy_count] = 1 - u_cut_face_Fy_W[u_cut_face_Fy_count];	    	    	      
		  }
		}
	      }
	      else{
		/* Interior face */
		u_cut_face_Fy_W[u_cut_face_Fy_count] = 0;
		u_cut_face_Fy_E[u_cut_face_Fy_count] = 0;
	      }

	      u_cut_face_Fy_count++;

	    } /* critical */
	  } /* if(p_cut_face_y[index_v] || p_cut_face_y[index_v+1]) */
	}
      }
    } 
  } /* if(u_cut_face_Fy_count) */

  
  /* Interpolation factors for Fu_z */

#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(p_cut_face_z) reduction(+: u_cut_face_Fz_count)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-2; I++){
	
	index_w = k*Nx*Ny + J*Nx + I;

	if(p_cut_face_z[index_w] || p_cut_face_z[index_w+1]){
	  u_cut_face_Fz_count++;
	}
      }
    }
  }

  if(u_cut_face_Fz_count){

    data->u_cut_face_Fz_W = create_array(u_cut_face_Fz_count, 1, 1, 0);
    data->u_cut_face_Fz_E = create_array(u_cut_face_Fz_count, 1, 1, 0);
    data->u_cut_face_Fz_index = create_array_int(u_cut_face_Fz_count, 1, 1, 0);
    data->u_cut_face_Fz_index_w = create_array_int(u_cut_face_Fz_count, 1, 1, 0);
    data->u_cut_face_Fz_count = u_cut_face_Fz_count;

    double *u_cut_face_Fz_W = data->u_cut_face_Fz_W;
    double *u_cut_face_Fz_E = data->u_cut_face_Fz_E;
    int *u_cut_face_Fz_index = data->u_cut_face_Fz_index;
    int *u_cut_face_Fz_index_w = data->u_cut_face_Fz_index_w;

    u_cut_face_Fz_count = 0;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_u, index_face, index_w, face_lower_corner, \
					       face_upper_corner, face_center_coord, v_node, curv_ind, coeffs_ind, \
					       pc_temp, curve_factor, norm_v, x_int) \
  shared(p_cut_face_z, u_cut_face_Fz_count, u_faces_z, w_dom_matrix, u_cut_face_Fz_index, u_cut_face_Fz_W, \
	 u_cut_face_Fz_E, Delta_x_u, Delta_y_u, u_cut_face_Fz_index_w, vertex_x_u, vertex_y, vertex_z, \
	 nodes_x_w, nodes_y_w, nodes_z_w, poly_coeffs, curve_is_solid_OK)

      for(k=0; k<nz; k++){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-2; I++){

	    i = I - 1;
	    j = J - 1;
	    K = k + 1;
	    index_face = k*nx*Ny + J*nx + i + 1;
	    index_w = k*Nx*Ny + J*Nx + I;
	    index_u = K*nx*Ny + J*nx + i + 1;

	    if(p_cut_face_z[index_w] || p_cut_face_z[index_w+1]){
	      
	      face_lower_corner[0] = vertex_x_u[I];
	      face_lower_corner[1] = vertex_y[j];
	      face_lower_corner[2] = vertex_z[k];
	      face_upper_corner[0] = vertex_x_u[I+1];
	      face_upper_corner[1] = vertex_y[j+1];
	      face_upper_corner[2] = face_lower_corner[2];

	      curv_ind = (p_cut_face_z[index_w])? p_cut_face_z[index_w] - 1 : p_cut_face_z[index_w+1] - 1;

	      for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
		pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	      }

	      curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	      face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);

#pragma omp critical
	      {
		u_cut_face_Fz_index[u_cut_face_Fz_count] = index_face;
		u_cut_face_Fz_index_w[u_cut_face_Fz_count] = index_w;

		if(u_faces_z[index_face] > tol * Delta_x_u[index_u] * Delta_y_u[index_u]){
		  if(p_cut_face_z[index_w]){
		    if(w_dom_matrix[index_w+1]){
		      /* Solid: east, wW only used for interpolation */
		      v_node[0] = nodes_x_w[index_w];
		      v_node[1] = nodes_y_w[index_w];
		      v_node[2] = nodes_z_w[index_w];

		      diff_vec(norm_v, v_node, face_center_coord);
		      scale_vec(norm_v, curve_factor);
		      normalize_vec(norm_v);
		      int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		      u_cut_face_Fz_W[u_cut_face_Fz_count] = 1 - (face_center_coord[0] - v_node[0]) /
			(x_int[0] - v_node[0]);
		      u_cut_face_Fz_E[u_cut_face_Fz_count] = 0;

#ifdef OLDINT_OK
		      u_cut_face_Fz_W[u_cut_face_Fz_count] = 1 - (face_center_coord[0] - nodes_x_w[index_w]) /
			(nodes_x_w[index_w+1] - nodes_x_w[index_w]);
		      u_cut_face_Fz_E[u_cut_face_Fz_count] = 1 - u_cut_face_Fz_W[u_cut_face_Fz_count];
#endif
		    }
		    else{
		      /* Both wW and wE used for interpolation */
		      u_cut_face_Fz_W[u_cut_face_Fz_count] = 1 - (face_center_coord[0] - nodes_x_w[index_w]) /
			(nodes_x_w[index_w+1] - nodes_x_w[index_w]);
		      u_cut_face_Fz_E[u_cut_face_Fz_count] = 1 - u_cut_face_Fz_W[u_cut_face_Fz_count];
		    }
		  }
		  else{
		    if(w_dom_matrix[index_w]){
		      /* Solid: west, wE only used for interpolation */
		      v_node[0] = nodes_x_w[index_w+1];
		      v_node[1] = nodes_y_w[index_w+1];
		      v_node[2] = nodes_z_w[index_w+1];

		      diff_vec(norm_v, v_node, face_center_coord);
		      scale_vec(norm_v, curve_factor);
		      normalize_vec(norm_v);
		      int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		      u_cut_face_Fz_W[u_cut_face_Fz_count] = 0;
		      u_cut_face_Fz_E[u_cut_face_Fz_count] = face_center_coord[0] / (v_node[0] - x_int[0]);

#ifdef OLDINT_OK
		      u_cut_face_Fz_W[u_cut_face_Fz_count] = 1 - (face_center_coord[0] - nodes_x_w[index_w]) /
			(nodes_x_w[index_w+1] - nodes_x_w[index_w]);
		      u_cut_face_Fz_E[u_cut_face_Fz_count] = 1 - u_cut_face_Fz_W[u_cut_face_Fz_count];
#endif		      
		    }
		    else{
		      /* Both wW and wE used for interpolation */
		      u_cut_face_Fz_W[u_cut_face_Fz_count] = 1 - (face_center_coord[0] - nodes_x_w[index_w]) /
			(nodes_x_w[index_w+1] - nodes_x_w[index_w]);
		      u_cut_face_Fz_E[u_cut_face_Fz_count] = 1 - u_cut_face_Fz_W[u_cut_face_Fz_count];		      
		    }
		  }		  
		}
		else{
		  /* Interior face */
		  u_cut_face_Fz_W[u_cut_face_Fz_count] = 0;
		  u_cut_face_Fz_E[u_cut_face_Fz_count] = 0;		      	   
		}

		u_cut_face_Fz_count++;
	      } /* critical */	      
	    } /* if(p_cut_face_z[index_w] || p_cut_face_z[index_w+1]) */
	  }
	}
      }
  } /* if(u_cut_face_Fz_count) */
  
   
  /* Interpolation factors for Fv_x */

#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(p_cut_face_x) reduction(+: v_cut_face_Fx_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-2; J++){
      for(i=0; i<nx; i++){

	index_u = J*nx + i;
            
	if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx]){
	  v_cut_face_Fx_count++;
	}
      }
    }
  }

  
  if(v_cut_face_Fx_count){

    data->v_cut_face_Fx_S = create_array(v_cut_face_Fx_count, 1, 1, 0);
    data->v_cut_face_Fx_N = create_array(v_cut_face_Fx_count, 1, 1, 0);
    data->v_cut_face_Fx_index = create_array_int(v_cut_face_Fx_count, 1, 1, 0);
    data->v_cut_face_Fx_index_u = create_array_int(v_cut_face_Fx_count, 1, 1, 0);
    data->v_cut_face_Fx_count = v_cut_face_Fx_count;
    
    double *v_cut_face_Fx_S = data->v_cut_face_Fx_S;
    double *v_cut_face_Fx_N = data->v_cut_face_Fx_N;
    int *v_cut_face_Fx_index = data->v_cut_face_Fx_index;
    int *v_cut_face_Fx_index_u = data->v_cut_face_Fx_index_u;

    v_cut_face_Fx_count = 0;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_u, index_face, index_v, face_lower_corner, \
					       face_upper_corner, face_center_coord, v_node, curv_ind, coeffs_ind, \
					       pc_temp, curve_factor, norm_v, x_int) \
  shared(p_cut_face_x, v_cut_face_Fx_count, v_faces_x, u_dom_matrix, v_cut_face_Fx_index, v_cut_face_Fx_S, \
	 v_cut_face_Fx_N, Delta_y_v, Delta_z_v, v_cut_face_Fx_index_u, vertex_x, vertex_y_v, vertex_z, \
	 nodes_x_u, nodes_y_u, nodes_z_u, poly_coeffs, curve_is_solid_OK)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-2; J++){
	for(i=0; i<nx; i++){

	  I = i + 1;
	  j = J - 1;
	  k = K - 1;	
	  index_face = K*nx*ny + (j+1)*nx + i;
	  index_v = K*Nx*ny + (j+1)*Nx + I;
	  index_u = K*nx*Ny + J*nx + i;	  

	  if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx]){

	    face_lower_corner[0] = vertex_x[i];
	    face_lower_corner[1] = vertex_y_v[J];
	    face_lower_corner[2] = vertex_z[k];
	    face_upper_corner[0] = face_lower_corner[0];
	    face_upper_corner[1] = vertex_y_v[J+1];
	    face_upper_corner[2] = vertex_z[k+1];

	    curv_ind = (p_cut_face_x[index_u])? p_cut_face_x[index_u] - 1 : p_cut_face_x[index_u+nx] - 1;

	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);
	    
#pragma omp critical
	    {

	      v_cut_face_Fx_index[v_cut_face_Fx_count] = index_face;
	      v_cut_face_Fx_index_u[v_cut_face_Fx_count] = index_u;

	      if(v_faces_x[index_face] > tol * Delta_y_v[index_v] * Delta_z_v[index_v]){
	  
		if(p_cut_face_x[index_u]){
		  
		  if(u_dom_matrix[index_u+nx]){
		    /* Solid: north, uS only used for interpolation */
		    v_node[0] = nodes_x_u[index_u];
		    v_node[1] = nodes_y_u[index_u];
		    v_node[2] = nodes_z_u[index_u];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		    v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (face_center_coord[1] - v_node[1]) / 
		      (x_int[1] - v_node[1]);
		    v_cut_face_Fx_N[v_cut_face_Fx_count] = 0;

#ifdef OLDINT_OK
		    v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (face_center_coord[1] - nodes_y_u[index_u]) /
		      (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		    v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
#endif		    
		  }
		  else{
		    /* Both uS and uN used for interpolation */
		    v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (face_center_coord[1] - nodes_y_u[index_u]) / 
		      (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		    v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
		  }
		}
		else{
		  if(u_dom_matrix[index_u]){
		    /* Solid: south, uN only used for interpolation */
		    v_node[0] = nodes_x_u[index_u+nx];
		    v_node[1] = nodes_y_u[index_u+nx];
		    v_node[2] = nodes_z_u[index_u+nx];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);
		    
		    v_cut_face_Fx_S[v_cut_face_Fx_count] = 0;
		    v_cut_face_Fx_N[v_cut_face_Fx_count] = face_center_coord[1] /
		      (v_node[1] - x_int[1]);

#ifdef OLDINT_OK
		    v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (face_center_coord[1] - nodes_y_u[index_u]) /
		      (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		    v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
#endif		    		    
		  }
		  else{
		    /* Both uS and uN used for interpolation */
		    v_cut_face_Fx_S[v_cut_face_Fx_count] = 1 - (face_center_coord[1] - nodes_y_u[index_u]) /
		      (nodes_y_u[index_u+nx] - nodes_y_u[index_u]);
		    v_cut_face_Fx_N[v_cut_face_Fx_count] = 1 - v_cut_face_Fx_S[v_cut_face_Fx_count];
		  }
		}
	      }
	      else{
		/* Interior face */
		v_cut_face_Fx_S[v_cut_face_Fx_count] = 0;
		v_cut_face_Fx_N[v_cut_face_Fx_count] = 0;
	      }

	      v_cut_face_Fx_count++;

	    } /* critical*/
	  } /* if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx]) */
	}
      }
    }
  } /* if(v_cut_face_Fx_count) */
  
  /* Interpolation factors for Fv_z */

#pragma omp parallel for default(none) private(I, J, k, index_w)	\
  shared(p_cut_face_z) reduction(+: v_cut_face_Fz_count)
  
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-2; J++){
      for(I=1; I<Nx-1; I++){

	index_w = k*Nx*Ny + J*Nx + I;
	
	if(p_cut_face_z[index_w] || p_cut_face_z[index_w+Nx]){
	  v_cut_face_Fz_count++;
	}
      }
    }
  }

  if(v_cut_face_Fz_count){
    
    data->v_cut_face_Fz_S = create_array(v_cut_face_Fz_count, 1, 1, 0);
    data->v_cut_face_Fz_N = create_array(v_cut_face_Fz_count, 1, 1, 0);
    data->v_cut_face_Fz_index = create_array_int(v_cut_face_Fz_count, 1, 1, 0);
    data->v_cut_face_Fz_index_w = create_array_int(v_cut_face_Fz_count, 1, 1, 0);
    data->v_cut_face_Fz_count = v_cut_face_Fz_count;

    double *v_cut_face_Fz_S = data->v_cut_face_Fz_S;
    double *v_cut_face_Fz_N = data->v_cut_face_Fz_N;
    int *v_cut_face_Fz_index = data->v_cut_face_Fz_index;
    int *v_cut_face_Fz_index_w = data->v_cut_face_Fz_index_w;
    
    v_cut_face_Fz_count = 0;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_v, index_face, index_w, face_lower_corner, \
					       face_upper_corner, face_center_coord, v_node, curv_ind, coeffs_ind, \
					       pc_temp, curve_factor, norm_v, x_int) \
  shared(p_cut_face_z, v_cut_face_Fz_count, v_faces_z, w_dom_matrix, v_cut_face_Fz_index, v_cut_face_Fz_S, \
	 v_cut_face_Fz_N, Delta_x_v, Delta_y_v, v_cut_face_Fz_index_w, vertex_x, vertex_y_v, vertex_z, \
	 nodes_x_w, nodes_y_w, nodes_z_w, poly_coeffs, curve_is_solid_OK)
    
    for(k=0; k<nz; k++){
      for(J=1; J<Ny-2; J++){
	for(I=1; I<Nx-1; I++){

	  i = I - 1;
	  j = J - 1;
	  K = k + 1;
	  index_face = k*Nx*ny + (j+1)*Nx + I;
	  index_w = k*Nx*Ny + J*Nx + I;
	  index_v = K*Nx*ny + (j+1)*Nx + I;

	  if(p_cut_face_z[index_w] || p_cut_face_z[index_w+Nx]){

	    face_lower_corner[0] = vertex_x[i];
	    face_lower_corner[1] = vertex_y_v[J];
	    face_lower_corner[2] = vertex_z[k];
	    face_upper_corner[0] = vertex_x[i+1];
	    face_upper_corner[1] = vertex_y_v[J+1];
	    face_upper_corner[2] = face_lower_corner[2];

 	    curv_ind = p_cut_face_z[index_w]? p_cut_face_z[index_w] - 1 : p_cut_face_z[index_w+Nx] - 1;

	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);	    

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 2);

#pragma omp critical
	    {
	      v_cut_face_Fz_index[v_cut_face_Fz_count] = index_face;
	      v_cut_face_Fz_index_w[v_cut_face_Fz_count] = index_w;

	      if(v_faces_z[index_face] > tol * Delta_x_v[index_v] * Delta_y_v[index_v]){
		if(p_cut_face_z[index_w]){
		  if(w_dom_matrix[index_w+Nx]){
		    /* Solid: north, wS only used for interpolation */
		    v_node[0] = nodes_x_w[index_w];
		    v_node[1] = nodes_y_w[index_w];
		    v_node[2] = nodes_z_w[index_w];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		    v_cut_face_Fz_S[v_cut_face_Fz_count] = 1 - (face_center_coord[1] - v_node[1]) /
		      (x_int[1] - v_node[1]);
		    v_cut_face_Fz_N[v_cut_face_Fz_count] = 1 - v_cut_face_Fz_S[v_cut_face_Fz_count];

#ifdef OLDINT_OK
		    v_cut_face_Fz_S[v_cut_face_Fz_count] = 1 - (face_center_coord[1] - nodes_y_w[index_w]) /
		      (nodes_y_w[index_w+Nx] - nodes_y_w[index_w]);
		    v_cut_face_Fz_N[v_cut_face_Fz_count] = 1 - v_cut_face_Fz_S[v_cut_face_Fz_count];
#endif
		  }
		  else{
		    /* Both wS and wN used for interpolation */
		    v_cut_face_Fz_S[v_cut_face_Fz_count] = 1 - (face_center_coord[1] - nodes_y_w[index_w]) /
		      (nodes_y_w[index_w+Nx] - nodes_y_w[index_w]);
		    v_cut_face_Fz_N[v_cut_face_Fz_count] = 1 - v_cut_face_Fz_S[v_cut_face_Fz_count];
		  }	      
		}
		else{
		  if(w_dom_matrix[index_w]){
		    /* Solid: south, wN only used for interpolation */
		    v_node[0] = nodes_x_w[index_w+Nx];
		    v_node[1] = nodes_y_w[index_w+Nx];
		    v_node[2] = nodes_z_w[index_w+Nx];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		    v_cut_face_Fz_S[v_cut_face_Fz_count] = 0;
		    v_cut_face_Fz_N[v_cut_face_Fz_count] = face_center_coord[1] /
		      (v_node[1] - x_int[1]);

#ifdef OLDINT_OK
		    v_cut_face_Fz_S[v_cut_face_Fz_count] = 1 - (face_center_coord[1] - nodes_y_w[index_w]) /
		      (nodes_y_w[index_w+Nx] - nodes_y_w[index_w]);
		    v_cut_face_Fz_N[v_cut_face_Fz_count] = 1 - v_cut_face_Fz_S[v_cut_face_Fz_count];
#endif		    
		  }
		  else{
		    /* Both wS and wN used for interpolation */
		    v_cut_face_Fz_S[v_cut_face_Fz_count] = 1 - (face_center_coord[1] - nodes_y_w[index_w]) /
		      (nodes_y_w[index_w+Nx] - nodes_y_w[index_w]);
		    v_cut_face_Fz_N[v_cut_face_Fz_count] = 1 - v_cut_face_Fz_S[v_cut_face_Fz_count];
		  }
		}	    
	      }
	      else{
		/* Interior face */
		v_cut_face_Fz_S[v_cut_face_Fz_count] = 0;
		v_cut_face_Fz_N[v_cut_face_Fz_count] = 0;
	      }
	      v_cut_face_Fz_count++;
	    } /* critical */
	  } /* if(p_cut_face_z[index_w] || p_cut_face_z[index_w+Nx]) */
	}
      }
    }
  } /* if(v_cut_face_Fz_count) */

  /* Interpolation factors for Fz_x */

#pragma omp parallel for default(none) private(i, J, K, index_u)	\
  shared(p_cut_face_x) reduction(+: w_cut_face_Fx_count)

  for(K=1; K<Nz-2; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	index_u  = K*nx*Ny + J*nx + i;

	if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx*Ny]){
	  w_cut_face_Fx_count++;
	}
      }
    }
  }

  if(w_cut_face_Fx_count){
    
    data->w_cut_face_Fx_B = create_array(w_cut_face_Fx_count, 1, 1, 0);
    data->w_cut_face_Fx_T = create_array(w_cut_face_Fx_count, 1, 1, 0);
    data->w_cut_face_Fx_index = create_array_int(w_cut_face_Fx_count, 1, 1, 0);
    data->w_cut_face_Fx_index_u = create_array_int(w_cut_face_Fx_count, 1, 1, 0);
    data->w_cut_face_Fx_count = w_cut_face_Fx_count;

    double *w_cut_face_Fx_B = data->w_cut_face_Fx_B;
    double *w_cut_face_Fx_T = data->w_cut_face_Fx_T;
    int *w_cut_face_Fx_index = data->w_cut_face_Fx_index;
    int *w_cut_face_Fx_index_u =  data->w_cut_face_Fx_index_u;
    
    w_cut_face_Fx_count = 0;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_u, index_face, index_w, face_lower_corner, \
					       face_upper_corner, face_center_coord, v_node, curv_ind, coeffs_ind, \
					       pc_temp, curve_factor, norm_v, x_int) \
  shared(p_cut_face_x, w_cut_face_Fx_count, w_faces_x, u_dom_matrix, w_cut_face_Fx_index, w_cut_face_Fx_B, \
	 w_cut_face_Fx_T, Delta_y_w, Delta_z_w, w_cut_face_Fx_index_u, vertex_x, vertex_y, vertex_z_w, \
	 nodes_x_u, nodes_y_u, nodes_z_u, poly_coeffs, curve_is_solid_OK)
    
    for(K=1; K<Nz-2; K++){
      for(J=1; J<Ny-1; J++){
	for(i=0; i<nx; i++){

	  I = i + 1;
	  j = J - 1;
	  k = K - 1;
	  index_face = (k+1)*nx*Ny + J*nx + i;
	  index_w = (k+1)*Nx*Ny + J*Nx + I;
	  index_u = K*nx*Ny + J*nx + i;

	  if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx*Ny]){
	    
	    face_lower_corner[0] = vertex_x[i];
	    face_lower_corner[1] = vertex_y[j];
	    face_lower_corner[2] = vertex_z_w[K];
	    face_upper_corner[0] = face_lower_corner[0];
	    face_upper_corner[1] = vertex_y[j+1];
	    face_upper_corner[2] = vertex_z_w[K+1];

	    curv_ind = (p_cut_face_x[index_u])? p_cut_face_x[index_u] - 1 : p_cut_face_x[index_u+nx*Ny] - 1;

	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);

#pragma omp critical
	    {
	      w_cut_face_Fx_index[w_cut_face_Fx_count] = index_face;
	      w_cut_face_Fx_index_u[w_cut_face_Fx_count] = index_u;

	      if(w_faces_x[index_face] > tol * Delta_y_w[index_w] * Delta_z_w[index_w]){
		if(p_cut_face_x[index_u]){
		  if(u_dom_matrix[index_u+nx*Ny]){
		    /* Solid: top, uB only used for interpolation */
		    v_node[0] = nodes_x_u[index_u];
		    v_node[1] = nodes_y_u[index_u];
		    v_node[2] = nodes_z_u[index_u];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		    w_cut_face_Fx_B[w_cut_face_Fx_count] = 1 - (face_center_coord[2] - v_node[2]) /
		      (x_int[2] - v_node[2]);
		    w_cut_face_Fx_T[w_cut_face_Fx_count] = 0;

#ifdef OLDINT_OK
		    w_cut_face_Fx_B[w_cut_face_Fx_count] = 1 - (face_center_coord[2] - nodes_z_u[index_u]) /
		      (nodes_z_u[index_u+nx*Ny] - nodes_z_u[index_u]);
		    w_cut_face_Fx_T[w_cut_face_Fx_count] = 1 - w_cut_face_Fx_B[w_cut_face_Fx_count];
#endif
		  }
		  else{
		    /* Both uB and uT used for interpolation */
		    w_cut_face_Fx_B[w_cut_face_Fx_count] = 1 - (face_center_coord[2] - nodes_z_u[index_u]) /
		      (nodes_z_u[index_u+nx*Ny] - nodes_z_u[index_u]);
		    w_cut_face_Fx_T[w_cut_face_Fx_count] = 1 - w_cut_face_Fx_B[w_cut_face_Fx_count];
		  }
		}
		else{
		  if(u_dom_matrix[index_u]){
		    /* Solid: bottom, uT only used for interpolation */
		    v_node[0] = nodes_x_u[index_u+nx*Ny];
		    v_node[1] = nodes_y_u[index_u+nx*Ny];
		    v_node[2] = nodes_z_u[index_u+nx*Ny];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		    w_cut_face_Fx_B[w_cut_face_Fx_count] = 0;
		    w_cut_face_Fx_T[w_cut_face_Fx_count] = face_center_coord[2] / (v_node[2] - x_int[2]);

#ifdef OLDINT_OK
		    w_cut_face_Fx_B[w_cut_face_Fx_count] = 1 - (face_center_coord[2] - nodes_z_u[index_u]) /
		      (nodes_z_u[index_u+nx*Ny] - nodes_z_u[index_u]);
		    w_cut_face_Fx_T[w_cut_face_Fx_count] = 1 - w_cut_face_Fx_B[w_cut_face_Fx_count];
#endif		    
		  }
		  else{
		    /* Both uB and uT used for interpolation */
		    w_cut_face_Fx_B[w_cut_face_Fx_count] = 1 - (face_center_coord[2] - nodes_z_u[index_u]) /
		      (nodes_z_u[index_u+nx*Ny] - nodes_z_u[index_u]);
		    w_cut_face_Fx_T[w_cut_face_Fx_count] = 1 - w_cut_face_Fx_B[w_cut_face_Fx_count];  
		  }
		}
	      }
	      else{
		/* Interior face */
		w_cut_face_Fx_B[w_cut_face_Fx_count] = 0;
		w_cut_face_Fx_T[w_cut_face_Fx_count] = 0;  		
	      }

	      w_cut_face_Fx_count++;
	    } /* critical */
	  } /* if(p_cut_face_x[index_u] || p_cut_face_x[index_u+nx*Ny]) */
	}
      }
    }    
  } /* if(w_cut_face_Fx_count) */

  /* Interpolation factors for Fw_y */

#pragma omp parallel for default(none) private(I, j, K, index_v) \
  shared(p_cut_face_y) reduction(+: w_cut_face_Fy_count)
  
  for(K=1; K<Nz-2; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){

	index_v = K*Nx*ny + j*Nx + I;

	if(p_cut_face_y[index_v] || p_cut_face_y[index_v+Nx*ny]){
	  w_cut_face_Fy_count++;
	}	
      }
    }
  }

  if(w_cut_face_Fy_count){

    data->w_cut_face_Fy_B = create_array(w_cut_face_Fy_count, 1, 1, 0);
    data->w_cut_face_Fy_T = create_array(w_cut_face_Fy_count, 1, 1, 0);
    data->w_cut_face_Fy_index = create_array_int(w_cut_face_Fy_count, 1, 1, 0);
    data->w_cut_face_Fy_index_v = create_array_int(w_cut_face_Fy_count, 1, 1, 0);
    data->w_cut_face_Fy_count = w_cut_face_Fy_count;

    double *w_cut_face_Fy_B = data->w_cut_face_Fy_B;
    double *w_cut_face_Fy_T = data->w_cut_face_Fy_T;
    int *w_cut_face_Fy_index = data->w_cut_face_Fy_index;
    int *w_cut_face_Fy_index_v = data->w_cut_face_Fy_index_v;
    
    w_cut_face_Fy_count = 0;

#pragma omp parallel for default(none) private(i, j, k, I, J, K, index_w, index_face, index_v, face_lower_corner, \
					       face_upper_corner, face_center_coord, v_node, curv_ind, coeffs_ind, \
					       pc_temp, curve_factor, norm_v, x_int) \
  shared(p_cut_face_y, w_cut_face_Fy_count, w_faces_y, v_dom_matrix, w_cut_face_Fy_index, w_cut_face_Fy_B, \
	 w_cut_face_Fy_T, Delta_x_w, Delta_z_w, w_cut_face_Fy_index_v, vertex_x, vertex_y, vertex_z_w, \
	 nodes_x_v, nodes_y_v, nodes_z_v, poly_coeffs, curve_is_solid_OK)

    for(K=1; K<Nz-2; K++){
      for(j=0; j<ny; j++){
	for(I=1; I<Nx-1; I++){
	  
	  i = I - 1;
	  J = j + 1;
	  k = K - 1;
	  
	  index_face = (k+1)*Nx*ny + j*Nx + I;
	  index_w = (k+1)*Nx*Ny + J*Nx + I;
	  index_v = K*Nx*ny + j*Nx + I;

	  if(p_cut_face_y[index_v] || p_cut_face_y[index_v+Nx*ny]){

	    face_lower_corner[0] = vertex_x[i];
	    face_lower_corner[1] = vertex_y[j];
	    face_lower_corner[2] = vertex_z_w[K];

	    face_upper_corner[0] = vertex_x[i+1];
	    face_upper_corner[1] = face_lower_corner[1];
	    face_upper_corner[2] = vertex_z_w[K+1];

	    curv_ind = p_cut_face_y[index_v]? p_cut_face_y[index_v] - 1 : p_cut_face_y[index_v+Nx*ny] - 1;

	    for(coeffs_ind=0; coeffs_ind<16; coeffs_ind++){
	      pc_temp[coeffs_ind] = poly_coeffs[curv_ind*16 + coeffs_ind];
	    }

	    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	    face_centroid(face_center_coord, face_lower_corner, face_upper_corner, pc_temp, curve_is_solid_OK[curv_ind], 1);

#pragma omp critical
	    {
	      w_cut_face_Fy_index[w_cut_face_Fy_count] = index_face;
	      w_cut_face_Fy_index_v[w_cut_face_Fy_count] = index_v;

	      if(w_faces_y[index_face] > tol * Delta_x_w[index_w] * Delta_z_w[index_w]){
		if(p_cut_face_y[index_v]){
		  if(v_dom_matrix[index_v+Nx*ny]){
		    /* Solid: top, vB only used for interpolation */
		    v_node[0] = nodes_x_v[index_v];
		    v_node[1] = nodes_y_v[index_v];
		    v_node[2] = nodes_z_v[index_v];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		    w_cut_face_Fy_B[w_cut_face_Fy_count] = 1 - (face_center_coord[2] - v_node[2]) /
		      (x_int[2] - v_node[2]);
		    w_cut_face_Fy_T[w_cut_face_Fy_count] = 1 - w_cut_face_Fy_B[w_cut_face_Fy_count];

#ifdef OLDINT_OK
		    w_cut_face_Fy_B[w_cut_face_Fy_count] = 1 - (face_center_coord[2] - nodes_z_v[index_v]) /
		      (nodes_z_v[index_v+Nx*ny] - nodes_z_v[index_v]);
		    w_cut_face_Fy_T[w_cut_face_Fy_count] = 1 - w_cut_face_Fy_B[w_cut_face_Fy_count];
#endif
		  }
		  else{
		    /* Both vB and vT used for interpolation */
		    w_cut_face_Fy_B[w_cut_face_Fy_count] = 1 - (face_center_coord[2] - nodes_z_v[index_v]) /
		      (nodes_z_v[index_v+Nx*ny] - nodes_z_v[index_v]);
		    w_cut_face_Fy_T[w_cut_face_Fy_count] = 1 - w_cut_face_Fy_B[w_cut_face_Fy_count];
		  }
		}
		else{
		  if(v_dom_matrix[index_v]){
		    /* Solid: bottom, vT only used for interpolation */
		    v_node[0] = nodes_x_v[index_v+Nx*ny];
		    v_node[1] = nodes_y_v[index_v+Nx*ny];
		    v_node[2] = nodes_z_v[index_v+Nx*ny];

		    diff_vec(norm_v, v_node, face_center_coord);
		    scale_vec(norm_v, curve_factor);
		    normalize_vec(norm_v);
		    int_curv(x_int, v_node, norm_v, pc_temp, cell_size);

		    w_cut_face_Fy_B[w_cut_face_Fy_count] = 0;
		    w_cut_face_Fy_T[w_cut_face_Fy_count] = face_center_coord[2] / (v_node[2] - x_int[2]);

#ifdef OLDINT_OK
		    w_cut_face_Fy_B[w_cut_face_Fy_count] = 1 - (face_center_coord[2] - nodes_z_v[index_v]) /
		      (nodes_z_v[index_v+Nx*ny] - nodes_z_v[index_v]);
		    w_cut_face_Fy_T[w_cut_face_Fy_count] = 1 - w_cut_face_Fy_B[w_cut_face_Fy_count];
#endif		    
		  }
		  else{
		    /* Both vB and vT used for interpolation */
		    w_cut_face_Fy_B[w_cut_face_Fy_count] = 1 - (face_center_coord[2] - nodes_z_v[index_v]) /
		      (nodes_z_v[index_v+Nx*ny] - nodes_z_v[index_v]);
		    w_cut_face_Fy_T[w_cut_face_Fy_count] = 1 - w_cut_face_Fy_B[w_cut_face_Fy_count];
		  }
		}
	      }
	      else{
		/* Interior face */
		w_cut_face_Fy_B[w_cut_face_Fy_count] = 0;
		w_cut_face_Fy_T[w_cut_face_Fy_count] = 0;
	      }
	      w_cut_face_Fy_count++;
	    } /* critical */
	  }
	}	  
      }
    }
  } /* if(w_cut_face_Fy_count) */   
    
  
  return ret_value;
}

int compute_cdist(Data_Mem *data){

  int ret_value = 0;

  const int Nx = data->Nx; 
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int curves = data->curves;
  const int solve_3D_OK = data->solve_3D_OK;

  const int *u_dom = data->u_dom_matrix;
  const int *v_dom = data->v_dom_matrix;
  const int *w_dom = data->w_dom_matrix;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  const double cell_size = data->cell_size;

  double *cudE = data->cudE;
  double *cudW = data->cudW;
  double *cudN = data->cudN;
  double *cudS = data->cudS;
  double *cudT = data->cudT;
  double *cudB = data->cudB;

  double *cvdE = data->cvdE;
  double *cvdW = data->cvdW;
  double *cvdN = data->cvdN;
  double *cvdS = data->cvdS;
  double *cvdT = data->cvdT;
  double *cvdB = data->cvdB;

  double *cwdE = data->cwdE;
  double *cwdW = data->cwdW;
  double *cwdN = data->cwdN;
  double *cwdS = data->cwdS;
  double *cwdT = data->cwdT;
  double *cwdB = data->cwdB;

  const double *x_u = data->nodes_x_u;
  const double *y_u = data->nodes_y_u;
  const double *z_u = data->nodes_z_u;

  const double *x_v = data->nodes_x_v;
  const double *y_v = data->nodes_y_v;
  const double *z_v = data->nodes_z_v;

  const double *x_w = data->nodes_x_w;
  const double *y_w = data->nodes_y_w;
  const double *z_w = data->nodes_z_w;
  
  const double *poly_coeffs = data->poly_coeffs;

  /* Initialize distances */
  if(solve_3D_OK){
    
    copy_array(data->Delta_xE_u, cudE, nx, Ny, Nz);
    copy_array(data->Delta_xW_u, cudW, nx, Ny, Nz);
    copy_array(data->Delta_yN_u, cudN, nx, Ny, Nz);
    copy_array(data->Delta_yS_u, cudS, nx, Ny, Nz);
    copy_array(data->Delta_zT_u, cudT, nx, Ny, Nz);
    copy_array(data->Delta_zB_u, cudB, nx, Ny, Nz);
    
    copy_array(data->Delta_xE_v, cvdE, Nx, ny, Nz);
    copy_array(data->Delta_xW_v, cvdW, Nx, ny, Nz);
    copy_array(data->Delta_yN_v, cvdN, Nx, ny, Nz);
    copy_array(data->Delta_yS_v, cvdS, Nx, ny, Nz);
    copy_array(data->Delta_zT_v, cvdT, Nx, ny, Nz);
    copy_array(data->Delta_zB_v, cvdB, Nx, ny, Nz);

    copy_array(data->Delta_xE_w, cwdE, Nx, Ny, nz);
    copy_array(data->Delta_xW_w, cwdW, Nx, Ny, nz);
    copy_array(data->Delta_yN_w, cwdN, Nx, Ny, nz);
    copy_array(data->Delta_yS_w, cwdS, Nx, Ny, nz);
    copy_array(data->Delta_zT_w, cwdT, Nx, Ny, nz);
    copy_array(data->Delta_zB_w, cwdB, Nx, Ny, nz);
  }
  else{
    
    copy_array(data->Delta_xE_u, cudE, nx, Ny, 1);
    copy_array(data->Delta_xW_u, cudW, nx, Ny, 1);
    copy_array(data->Delta_yN_u, cudN, nx, Ny, 1);
    copy_array(data->Delta_yS_u, cudS, nx, Ny, 1);
    
    copy_array(data->Delta_xE_v, cvdE, Nx, ny, 1);
    copy_array(data->Delta_xW_v, cvdW, Nx, ny, 1);
    copy_array(data->Delta_yN_v, cvdN, Nx, ny, 1);
    copy_array(data->Delta_yS_v, cvdS, Nx, ny, 1);
  }

  /* Correct distances */
  if(solve_3D_OK){
    
    /* u */
    __compute_cdist_3D(cudE, cudW, cudN, cudS, cudT, cudB,
		       nx, Ny, Nz, curves,
		       u_dom, curve_is_solid_OK,
		       x_u, y_u, z_u,
		       poly_coeffs, cell_size);
    /* v */
    __compute_cdist_3D(cvdE, cvdW, cvdN, cvdS, cvdT, cvdB,
		       Nx, ny, Nz, curves,
		       v_dom, curve_is_solid_OK,
		       x_v, y_v, z_v,
		       poly_coeffs, cell_size);
    /* w */
    __compute_cdist_3D(cwdE, cwdW, cwdN, cwdS, cwdT, cwdB,
		       Nx, Ny, nz, curves,
		       w_dom, curve_is_solid_OK,
		       x_w, y_w, z_w,
		       poly_coeffs, cell_size);
  }
  else{
    
    /* u */
    __compute_cdist(cudE, cudW, cudN, cudS,
		    nx, Ny, curves,
		    u_dom, curve_is_solid_OK,
		    x_u, y_u,
		    poly_coeffs, cell_size);
    /* v */
    __compute_cdist(cvdE, cvdW, cvdN, cvdS,
		    Nx, ny, curves,
		    v_dom, curve_is_solid_OK,
		    x_v, y_v,
		    poly_coeffs, cell_size);
  }
  
  return ret_value;
}

/**
  
   Computes distances to neighborg or solid, since it is generally programmed
   index_uv are referred just as index_

   This function is a complement which should follow after an initialization of
   arrays cuvdEWNS to Delta_xE_u, etc.
   
   Great improvements if we could count with the indexs of the boundaries for uv.
   
   @param (double, NxNy) cdEWNS: Distance to EWNS neighborg or solid traveling through xy.
   
   @param (int, NxNy) dom: Domain matrix.
   
   @param (double, NxNy) x,y: Coordinates of DISPLACED cut velocities.
   
   @param (double) poly_coeffs: Array of curves
   
   @return ret_value not implemented.
   
   MODIFIED: 25-09-2020
*/
int __compute_cdist(double *cdE, double *cdW, double *cdN, double *cdS,
		    const int Nx, const int Ny, const int curves,
		    const int* dom, const int *curve_is_solid_OK,
		    const double *x, const double *y,
		    const double *poly_coeffs, const double cell_size){
  
  int ret_value = 0;
  
  int I, J, i;
  int index_;
  int curv_ind;
  int fluid_P_OK;
  int solid_W_OK, solid_E_OK;
  int solid_S_OK, solid_N_OK;

  /* Variables to store polynomial values */
  double cf;
  double x0[3] = {0};
  double pc[16];
  double norm[3] = {0};
  double xint[3] = {0};

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    for(i=0; i<16; i++){
      pc[i] = poly_coeffs[curv_ind*16 + i];
    }

    cf = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
    
#pragma omp parallel for default(none)		\
  firstprivate(curv_ind, cf)			\
  private(I, J, index_,				\
	  x0, xint, norm,			\
	  fluid_P_OK,				\
	  solid_W_OK, solid_E_OK,		\
	  solid_S_OK, solid_N_OK)		\
  shared(pc, dom, x, y,				\
	 cdE, cdW, cdN, cdS)
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_ = J*Nx + I;

	fluid_P_OK = (dom[index_] == 0);
	
	/* Correction only for cases with solid neighborg */
	if(fluid_P_OK){
	  
	  solid_E_OK = (dom[index_+1] == curv_ind + 1);
	  solid_W_OK = (dom[index_-1] == curv_ind + 1);
	  solid_N_OK = (dom[index_+Nx] == curv_ind + 1);
	  solid_S_OK = (dom[index_-Nx] == curv_ind + 1);
	  
	  x0[0] = x[index_];
	  x0[1] = y[index_];
	  
	  if(solid_E_OK){
	    /* Normal vector defined from the solid phase to the fluid phase */
	    norm[0] = -1;
	    norm[1] = 0;
	    	    
	    if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
	      exit(EXIT_FAILURE);
	    }
	    
	    cdE[index_] = xint[0] - x0[0];
	  }
	  
	  if(solid_W_OK){
	    /* Normal vector defined from the solid phase to the fluid phase */
	    norm[0] = +1;
	    norm[1] = 0;
	    
	    if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
	      exit(EXIT_FAILURE);
	    }
	    
	    cdW[index_] = x0[0] - xint[0];
	  }
	  
	  if(solid_N_OK){
	    /* Normal vector defined from the solid phase to the fluid phase */
	    norm[0] = 0;
	    norm[1] = -1;
	    
	    if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
	      exit(EXIT_FAILURE);
	    }
	    
	    cdN[index_] = xint[1] - x0[1];
	  }
	  
	  if(solid_S_OK){
	    /* Normal vector defined from the solid phase to the fluid phase */
	    norm[0] = 0;
	    norm[1] = +1;
	    
	    if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
	      exit(EXIT_FAILURE);
	    }
	    
	    cdS[index_] = x0[1] - xint[1];
	  }

	} //	  if(fluid_P_OK){
	  
      }
    }
    
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++){

  return ret_value;
}

/**
  
   Computes distances to neighborg or solid, since it is generally programmed
   index_uvw are referred just as index_

   This function is a complement which should follow after an initialization of
   arrays cuvwdEWNSTB to Delta_xE_u, etc.
   
   Great improvements if we could count with the indexs of the boundaries for uvw.
   
   @param (double, NxNyNz) cdEWNSTB: Distance to EWNSTB neighborg or solid traveling through xyz.
   
   @param (int, NxNyNz) dom: Domain matrix.
   
   @param (double, NxNyNz) x,y,z: Coordinates of DISPLACED cut velocities.
   
   @param (double) poly_coeffs: Array of curves
   
   @return ret_value not implemented.
   
   MODIFIED: 25-09-2020
*/
int __compute_cdist_3D(double *cdE, double *cdW, double *cdN, double *cdS, double *cdT, double *cdB,
		       const int Nx, const int Ny, const int Nz, const int curves,
		       const int* dom, const int *curve_is_solid_OK,
		       const double *x, const double *y, const double *z,
		       const double *poly_coeffs, const double cell_size){
  
  int ret_value = 0;
  
  int I, J, K, i;
  int index_;
  int curv_ind;
  int fluid_P_OK;
  int solid_W_OK, solid_E_OK;
  int solid_S_OK, solid_N_OK;
  int solid_B_OK, solid_T_OK;

  /* Variables to store polynomial values */
  double cf;
  double x0[3] = {0};
  double pc[16];
  double norm[3] = {0};
  double xint[3] = {0};

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    for(i=0; i<16; i++){
      pc[i] = poly_coeffs[curv_ind*16 + i];
    }

    cf = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none)		\
  firstprivate(curv_ind, cf)			\
  private(I, J, K, index_,			\
	  x0, xint, norm,			\
	  fluid_P_OK,				\
	  solid_W_OK, solid_E_OK,		\
	  solid_S_OK, solid_N_OK,		\
  	  solid_B_OK, solid_T_OK)		\
	  shared(pc, dom, x, y, z,		\
		 cdE, cdW, cdN, cdS, cdT, cdB)
    for(K=1; K<Nz-1; K++){    
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  
  	  index_ = K*Nx*Ny + J*Nx + I;

	  fluid_P_OK = (dom[index_] == 0);

	  /* Correction only for cases with solid neighborg */
	  if(fluid_P_OK){

	    solid_E_OK = (dom[index_+1] == curv_ind + 1);
	    solid_W_OK = (dom[index_-1] == curv_ind + 1);
	    solid_N_OK = (dom[index_+Nx] == curv_ind + 1);
	    solid_S_OK = (dom[index_-Nx] == curv_ind + 1);
	    solid_T_OK = (dom[index_+Nx*Ny] == curv_ind + 1);
	    solid_B_OK = (dom[index_-Nx*Ny] == curv_ind + 1);

	    x0[0] = x[index_];
	    x0[1] = y[index_];
	    x0[2] = z[index_];
	    
	    if(solid_E_OK){
	      /* Normal vector defined from the solid phase to the fluid phase */
	      norm[0] = -1;
	      norm[1] = 0;
	      norm[2] = 0;

	      if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
		exit(EXIT_FAILURE);
	      }
	      
	      cdE[index_] = xint[0] - x0[0];
	    }

	    if(solid_W_OK){
	      /* Normal vector defined from the solid phase to the fluid phase */
	      norm[0] = +1;
	      norm[1] = 0;
	      norm[2] = 0;

	      if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
		exit(EXIT_FAILURE);
	      }
	      
	      cdW[index_] = x0[0] - xint[0];
	    }

	    if(solid_N_OK){
	      /* Normal vector defined from the solid phase to the fluid phase */
	      norm[0] = 0;
	      norm[1] = -1;
	      norm[2] = 0;

	      if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
		exit(EXIT_FAILURE);
	      }
	      
	      cdN[index_] = xint[1] - x0[1];
	    }

	    if(solid_S_OK){
	      /* Normal vector defined from the solid phase to the fluid phase */
	      norm[0] = 0;
	      norm[1] = +1;
	      norm[2] = 0;

	      if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
		exit(EXIT_FAILURE);
	      }
	      
	      cdS[index_] = x0[1] - xint[1];
	    }

	    if(solid_T_OK){
	      /* Normal vector defined from the solid phase to the fluid phase */
	      norm[0] = 0;
	      norm[1] = 0;
	      norm[2] = -1;

	      if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
		exit(EXIT_FAILURE);
	      }
	      
	      cdT[index_] = xint[2] - x0[2];
	    }

	    if(solid_B_OK){
	      /* Normal vector defined from the solid phase to the fluid phase */
	      norm[0] = 0;
	      norm[1] = 0;
	      norm[2] = +1;

	      if(int_curv_cf(xint, x0, norm, pc, cell_size, cf) != 0){
		exit(EXIT_FAILURE);
	      }
	      
	      cdB[index_] = x0[2] - xint[2];
	    }

	  } //	  if(fluid_P_OK){
	  
	}
      }
    }
    
  } //   for(curv_ind=0; curv_ind<curves; curv_ind++){

  return ret_value;
}


/* SET_SOL_FACTOR_TYPE_CELLS */
/* Sets int arrays u,v,w_sol_factor_type to 
   0: regular fluid cell, 
   1: slave cell, 
   2: solid cell 
   for use with u,v,w_sol_factor_index_u,v,w
*/
void set_sol_factor_type_cells(Data_Mem *data){

  const int u_slave_count = data->u_slave_count;
  const int v_slave_count = data->v_slave_count;
  const int w_slave_count = data->w_slave_count;

  const int u_sol_factor_count = data->u_sol_factor_count;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int w_sol_factor_count = data->w_sol_factor_count;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;

  const int *u_slave_nodes = data->u_slave_nodes;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *w_slave_nodes = data->w_slave_nodes;

  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *w_sol_factor_index_w = data->w_sol_factor_index_w;

  /* U */
  if(u_sol_factor_count > 0){
    /* Get memory (Memory freed in free_memory when its count is > 0) */
    data->u_sol_type = create_array_int(u_sol_factor_count, 1, 1, 0);
    
    /* Classify cells */
    _set_sol_factor_type_cells(data->u_sol_type,
			       u_slave_nodes, u_sol_factor_index_u,
			       u_dom_matrix,
			       u_slave_count, u_sol_factor_count);
  }
  
  /* V */
  if(v_sol_factor_count > 0){
    /* Get memory (Memory freed in free_memory when its count is > 0) */
    data->v_sol_type = create_array_int(v_sol_factor_count, 1, 1, 0);
    
    /* Classify cells */
    _set_sol_factor_type_cells(data->v_sol_type,
			       v_slave_nodes, v_sol_factor_index_v,
			       v_dom_matrix,
			       v_slave_count, v_sol_factor_count);
  }
  
  /* W */
  if(w_sol_factor_count > 0){
    /* Get memory (Memory freed in free_memory when its count is > 0) */
    data->w_sol_type = create_array_int(w_sol_factor_count, 1, 1, 0);
    
    /* Classify cells */
    _set_sol_factor_type_cells(data->w_sol_type,
			       w_slave_nodes, w_sol_factor_index_w,
			       w_dom_matrix,
			       w_slave_count, w_sol_factor_count);
  }
  
  return;
  
}


void _set_sol_factor_type_cells(int *_sol_type,
				const int *_slave_nodes, const int *_sol_factor_index_,
				const int *_dom_matrix,
				const int _slave_count, const int _sol_factor_count){

  int k = 0, z = 0;
  int slave_found = 0;
  int index_ = 0;

  /* Slave seeker */
  for(z=0; z<_slave_count; z++){
    
    index_ = _slave_nodes[z];
    
    k = 0;
    
    slave_found = 0;
    
    while((k<_sol_factor_count) && (!slave_found)){
      
      if(_sol_factor_index_[k] == index_){
	
	slave_found = 1;
	_sol_type[k] = 1;
      }
      k++;
    }
  }

  /* Solid seeker */
  for(z=0; z<_sol_factor_count; z++){
    index_ = _sol_factor_index_[z];
    
    if(_dom_matrix[index_]){
      _sol_type[z] = 2;
    }
  }
  
  return;
  
}

/* PRINT_CUT_CELL_INFO */
/*****************************************************************************/
/**
  
  Print cut_cell info to cut_cell_info.dat file

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 06-12-2019
*/
void print_cut_cell_info(Data_Mem *data){

  FILE *fp = NULL;
  
  Wins *wins = &(data->wins);

  const char *results_dir = data->results_dir;

  char cut_cell_info_file[SIZE];

  const int write_in_GPU_OK = 0;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int u_slave_count = data->u_slave_count;
  const int v_slave_count = data->v_slave_count;
  const int w_slave_count = data->w_slave_count;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int w_sol_factor_count = data->w_sol_factor_count;
  const int solve_3D_OK = data->solve_3D_OK;

  const int nu = nx*Ny*Nz;
  const int nv = Nx*ny*Nz;
  const int nw = solve_3D_OK ? Nx*Ny*nz : 1;
  
  if(results_dir == NULL && !write_in_GPU_OK){
    snprintf(cut_cell_info_file, SIZE, "./data/CPU/%s",
	     "cut_cell_info.dat");

  }
  else if(results_dir == NULL && write_in_GPU_OK){
    snprintf(cut_cell_info_file, SIZE, "./data/GPU/%s",
	     "cut_cell_info.dat");
  }
  else{
    snprintf(cut_cell_info_file, SIZE, "%s%s",
	     results_dir, "cut_cell_info.dat");
  }

  fp = fopen(cut_cell_info_file, "w");

  if(fp == NULL){
    error_msg(wins, "Could not create/open cut_cell_info_file file\n");
  }

  fprintf(fp, "%s\n",
	  "% kappa total_p total_u total_v total_w cc_u cc_v cc_w sc_u sc_v sc_w p_sc_u p_sc_v p_sc_w");

   fprintf(fp, "%.2f %d %d %d %d %d %d %d %d %d %d %.2f %.2f %.2f\n",
	  data->kappa,
	  Nx*Ny*Nz,
	  nu, nv, nw,
	  u_sol_factor_count, v_sol_factor_count, w_sol_factor_count,
	  u_slave_count, v_slave_count, w_slave_count,
	  (float) u_slave_count / u_sol_factor_count * 100,
	  (float) v_slave_count / v_sol_factor_count * 100,
	  (float) w_slave_count / w_sol_factor_count * 100);

   /* Writing coordinates for tikz (paper cells) */
   fprintf(fp, "\n");
   
   const int usfc = data->u_sol_factor_count;
   const int vsfc = data->v_sol_factor_count;
   int k;
   int idx;
   /* Nodos frontera? */
   const int *usfiu = data->u_sol_factor_index_u;
   const int *vsfiv = data->v_sol_factor_index_v;
   
   const int *ust = data->u_sol_type;
   const int *vst = data->v_sol_type;
      
   const double *xu = data->nodes_x_u;
   const double *yu = data->nodes_y_u;

   const double *xv = data->nodes_x_v;
   const double *yv = data->nodes_y_v;

   fprintf(fp, "%s\n", "% _sol_type: 0: regular fluid node, 1: slave node, 2: solid node\n");

   fprintf(fp, "%s\n", "% u cells (x-momentum)");
   
   fprintf(fp, "%s\n", "% Fluid nodes");
   for(k=0; k<usfc; k++){

     if(ust[k] == 0){
       idx = usfiu[k];
       fprintf(fp, "%s%8.4f, %8.4f%s\n",
	       "\\draw[regularNodeStyle] (",
	       xu[idx]*100, yu[idx]*100,
	       ") circle [radius = \\vNodeRadius];");
     }
   }


   fprintf(fp, "%s\n", "% Slave nodes");
   for(k=0; k<usfc; k++){

     if(ust[k] == 1){
       idx = usfiu[k];
       fprintf(fp, "%s%8.4f, %8.4f%s\n",
	       "\\draw[slaveNodeStyle] (",
	       xu[idx]*100, yu[idx]*100,
	       ") circle [radius = \\vNodeRadius];");
     }
   }

   fprintf(fp, "%s\n", "% Solid nodes");
   for(k=0; k<usfc; k++){

     if(ust[k] == 2){
       idx = usfiu[k];
       fprintf(fp, "%s%8.4f, %8.4f%s\n",
	       "\\draw[internalNodeStyle] (",
	       xu[idx]*100, yu[idx]*100,
	       ") circle [radius = \\vNodeRadius];");
     }
   }

   fprintf(fp, "\n");

   fprintf(fp, "%s\n", "% v cells (y-momentum)");
   
   fprintf(fp, "%s\n", "% Fluid nodes");
   for(k=0; k<vsfc; k++){

     if(vst[k] == 0){
       idx = vsfiv[k];
       fprintf(fp, "%s%8.4f, %8.4f%s\n",
	       "\\draw[regularNodeStyle] (",
	       xv[idx]*100, yv[idx]*100,
	       ") circle [radius = \\vNodeRadius];");
     }
   }


   fprintf(fp, "%s\n", "% Slave nodes");
   for(k=0; k<vsfc; k++){

     if(vst[k] == 1){
       idx = vsfiv[k];
       fprintf(fp, "%s%8.4f, %8.4f%s\n",
	       "\\draw[slaveNodeStyle] (",
	       xv[idx]*100, yv[idx]*100,
	       ") circle [radius = \\vNodeRadius];");
     }
   }

   fprintf(fp, "%s\n", "% Solid nodes");
   for(k=0; k<vsfc; k++){

     if(vst[k] == 2){
       idx = vsfiv[k];
       fprintf(fp, "%s%8.4f, %8.4f%s\n",
	       "\\draw[internalNodeStyle] (",
	       xv[idx]*100, yv[idx]*100,
	       ") circle [radius = \\vNodeRadius];");
     }
   }

   fclose(fp);

  return;
}
