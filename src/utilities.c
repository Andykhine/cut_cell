#include "include/some_defs.h"
extern int count_double;
extern int count_int;
extern int count_char;

/* SET_ETA_ARRAY */
void set_eta_array(char *eta, const int size, const u_int64_t delta){

  const u_int64_t delta_secs = delta / 1000000;
  const u_int64_t hours    = delta_secs / 3600;
  const u_int64_t minutes  = (delta_secs - hours * 3600) / 60;
  const u_int64_t seconds  = (delta_secs - hours * 3600 - minutes * 60);
  // const u_int64_t mseconds = (delta / 100000) % 10;

  snprintf(eta, size, "ETA: %4luh %2lum %2lus", hours, minutes, seconds);
  
  return;
}

/* MAKE_BL_PROFILE */
void make_bl_profile(Data_Mem *data){

  const int nx = data->nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int w_type = data->bc_flow_west.type;
  /* const int e_type = data->bc_flow_east.type; */

  double phib, d;
  
  /* double *aP_u = data->coeffs_u.aP; */
  /* double *aS_u = data->coeffs_u.aS; */
  /* double *aN_u = data->coeffs_u.aN; */
  /* double *aW_u = data->coeffs_u.aW; */
  /* double *aE_u = data->coeffs_u.aE; */
  /* double *aB_u = data->coeffs_u.aB; */
  /* double *aT_u = data->coeffs_u.aT; */
  double *b_u = data->coeffs_u.b;
  
  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  //  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);

  // Solid at B, flow W->E
  if(w_type == -1){

    phib = bc_flow_west->v_value[0];
    d = bc_flow_west->p_value;
    
    _make_bl_profile(b_u, phib,
		     'w', 'b',
		     0,
		     data->nodes_x_u, data->nodes_y_u, data->nodes_z_u,
		     data->u_dom_matrix, 0,
		     d,
		     nx, Ny, Nz,
		     0, data->Lx,
		     0, data->Ly,
		     0, data->Lz);
  }
  
  return;
}

/* _MAKE_BL_PROFILE */
/**

  Sets phi with a boundary layer profile as a boundary condition in bc_face as
  long as there is a solid_at_face which is perpendicular to bc_face.

  dM domain matrix for phi field, phib free stream value of field, d BL
  thickness, dir > 0 treats space from wref forwards else wref backwards until
  it reaches d.
    
  @return void.
  
  MODIFIED: 01-May-2021
*/
void _make_bl_profile(double *phi, const double phib,
		      const char bc_face, const char solid_at_face,
		      const double wref,
		      const double *x, const double *y, const double *z,
		      const int *dM, const int dM_value,
		      const double d,
		      const int Nx, const int Ny, const int Nz,
		      const double xmin, const double xmax,
		      const double ymin, const double ymax,
		      const double zmin, const double zmax){
  
  int I, J, K, index_;
  double pos, d_pos;

  // B is solid -> Flow W<->E (BC)
  if( (solid_at_face == 'B') || (solid_at_face == 'b') ){
    
    if((bc_face == 'E') || (bc_face == 'e')){
      I = Nx-1;
    }
    else{
      I = 0;
    }
    
#pragma omp parallel for default(none) \
  firstprivate(I)		       \
  private(J, K, index_, pos, d_pos)    \
  shared(phi, x, y, z, dM)
    for(K=0; K<Nz; K++){ // since solid is at B face
      for(J=0; J<Ny; J++){
	
	index_ = K*Nx*Ny + J*Nx + I;
	
	pos = z[index_];
	d_pos = fabs(pos - wref);

	if((dM[index_] == dM_value) && // Fluid
	   (ymin <= y[index_]) && (y[index_] <= ymax) &&
	   (zmin <= pos) && (pos <= zmax) && (wref <= pos) // Positions
	   ){

	  if(d_pos <= d){ // within BL
	    phi[index_] = (2*phib/(d*d)) * (d*d_pos - d_pos*d_pos/2);
	  }
	  else{
	    phi[index_] = phib;
	  }
	    
	  }
	
      }
    }
  } // if solid_at_face
  
  return;
}

/* SET_ARRAY_PROFILE_FOR_DEBUG */
void set_array_profile_for_debug(double *phi, Data_Mem *data, const int radial_OK){
    
  int I, J, K, index_;
    
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
    
  const int *dom_matrix = data->domain_matrix;
    
  double x, y, z, r, theta;
    
  const double tx = data->tcompx[0];
  const double ty = data->tcompy[0];
  const double tz = data->tcompz[0];
  
  const double r0 = sqrt((-1)*data->constant_d[0]);
  
  const double *x_ = data->nodes_x;
  const double *y_ = data->nodes_y;
  const double *z_ = data->nodes_z;

#pragma omp parallel for default(none)		\
  private(I, J, K, index_, x, y, z, r, theta)	\
  shared(x_, y_, z_, phi, dom_matrix)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_ = K*Ny*Nx + J*Nx + I;
	
	if(dom_matrix[index_] == 0){
	  
	  x = x_[index_];
	  y = y_[index_];
	  z = z_[index_];

	  x = x - tx;
	  y = y - ty;
	  z = z - tz;
	  
	  r = sqrt(x*x + y*y + z*z);
	  theta = atan(sqrt(x*x + y*y)/z);

	  if(radial_OK){
	    phi[index_] = (r-r0) * cos(theta);
	  }
	  else{
	    phi[index_] = 1;
	  }
	  
	} 
      }
    }
  }
  
  return;
  
}

/* SET_UVW_PROFILE_FOR_DEBUG */
void set_uvw_profile_for_debug(Data_Mem *data, const int radial_OK){

  int i, j, k, I, J, K;
  int index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;

  double x, y, z;

  double r, theta, phi;
  
  const double tx = data->tcompx[0];
  const double ty = data->tcompy[0];
  const double tz = data->tcompz[0];
  
  const double r0 = sqrt((-1)*data->constant_d[0]);
  
  double *u = data->u;
  double *v = data->v;
  double *w = data->w;

  const double *x_u = data->nodes_x_u;
  const double *y_u = data->nodes_y_u;
  const double *z_u = data->nodes_z_u;
  
  const double *x_v = data->nodes_x_v;
  const double *y_v = data->nodes_y_v;
  const double *z_v = data->nodes_z_v;

  const double *x_w = data->nodes_x_w;
  const double *y_w = data->nodes_y_w;
  const double *z_w = data->nodes_z_w;

#pragma omp parallel for default(none)			\
  private(i, J, K, index_u, x, y, z, r, theta, phi)	\
  shared(x_u, y_u, z_u, u, u_dom_matrix)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	
	index_u = K*Ny*nx + J*nx + i;
	
	if(u_dom_matrix[index_u] == 0){
	  
	  x = x_u[index_u];
	  y = y_u[index_u];
	  z = z_u[index_u];

	  x = x - tx;
	  y = y - ty;
	  z = z - tz;
	  
	  r = sqrt(x*x + y*y + z*z);
	  theta = atan(sqrt(x*x + y*y)/z);
	  phi = atan(y/x);
      
	  if(radial_OK){	  
	    u[index_u] = (r-r0) * sin(theta) * cos(phi);
	  }
	  else{
	    u[index_u] = 1;
	  }
	  
	} 
      }
    }
  }
  
#pragma omp parallel for default(none)			\
  private(I, j, K, index_v, x, y, z, r, theta, phi)	\
  shared(x_v, y_v, z_v, v, v_dom_matrix)
  
  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){
	
	index_v = K*ny*Nx + j*Nx + I;
	
	if(v_dom_matrix[index_v] == 0){
	  x = x_v[index_v];
	  y = y_v[index_v];
	  z = z_v[index_v];

	  x = x - tx;
	  y = y - ty;
	  z = z - tz;
	  
	  r = sqrt(x*x + y*y + z*z);
	  theta = atan(sqrt(x*x + y*y)/z);
	  phi = atan(y/x);
	  
	  if(radial_OK){
	    v[index_v] = (r-r0) * sin(theta) * sin(phi);
	  }
	  else{
	    v[index_v] = 1;
	  }
	  
	}
	
      }
    }
  }

#pragma omp parallel for default(none)		\
  private(I, J, k, index_w, x, y, z, r, theta)	\
  shared(x_w, y_w, z_w, w, w_dom_matrix)
  
  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_w = k*Ny*Nx + J*Nx + I;
	
	if(w_dom_matrix[index_w] == 0){
	  
	  x = x_w[index_w];
	  y = y_w[index_w];
	  z = z_w[index_w];

	  x = x - tx;
	  y = y - ty;
	  z = z - tz;
	  
	  r = sqrt(x*x + y*y + z*z);
	  theta = atan(sqrt(x*x + y*y)/z);

	  if(radial_OK){
	    w[index_w] = (r-r0) * cos(theta);
	  }
	  else{
	    w[index_w] = 1;
	  }
	  
	} 
      }
    }
  }
  
  return;
}

/* MEM_COUNT */
void mem_count(Wins *wins){

  char msg[SIZE];

  float mem = 0;

  mem = count_double * sizeof(double) + count_int * sizeof(int);
  mem = mem * 1e-6;

#ifdef DISPLAY_OK
  snprintf(msg, SIZE, "%.1f MB", mem);
  wmove(wins->upds, 2, 20);
  wclrtoeol(wins->upds);
  wprintw(wins->upds, "%s\n", msg);
  wrefresh(wins->upds);
#else
  snprintf(msg, SIZE, "MEM_COUNT: %.1f MB", mem);
  printf("%s\n", msg);
#endif
  
  return;
}

/* GET_MATFP */
inline mat_t * get_matfp(const char *filename, const int v5_OK, 
                         Wins *wins){

  char msg[SIZE];

  mat_t *matfp = NULL;
  enum mat_ft version;


  if(v5_OK){
    version = MAT_FT_MAT5;
  }
  else{
    version = MAT_FT_MAT73;
  }
  
  /* Check if file is already there */
  if(access(filename, F_OK) != 0){
    /* File does not exists and we need to create it */
    matfp = Mat_CreateVer(filename, NULL, version);
  }
  else{
    /* File exists */
    matfp = Mat_Open(filename, MAT_ACC_RDWR);
    snprintf(msg, SIZE, "%s exist. I will overwrite it", filename);
    alert_msg(wins, msg);
  }

  if(matfp == NULL){
    memset(msg, '\0', sizeof(msg));
    snprintf(msg, SIZE, "Could not create/open %s", filename);
    error_msg(wins, msg);
  }
  
  return matfp;
}

/* WRITE_TO_LOG_FILE */
int write_to_log_file(Data_Mem *data, const char *log_entry){

  Wins *wins = &(data->wins);
  
  char time_string[40];
  char eta[ETA_MSG];

  const char *filename = data->log_filename;

  FILE *log_file = NULL;

  struct timeval tv;
  // This pointer should not be freed!!
  struct tm *ptm = NULL;

  const int it = data->sigmas.it;
  const int max_iter_flow = data->max_iter_flow;
  const int max_iter_phi = data->max_it;
  const int coupled_physics_OK = data->coupled_physics_OK;
  const int flow_done_OK = data->flow_done_OK;

  
  const double fraction =  (flow_done_OK) ?
    (it + 1.0) / (max_iter_phi + 1.0) : (it + 1.0) / (max_iter_flow + 1.0);
  
  const u_int64_t elapsed = *(data->sigmas.time_f);
  const u_int64_t estimated_total = elapsed / fraction;
  const u_int64_t delta = estimated_total - elapsed;

  set_eta_array(eta, ETA_MSG, delta);
  
  log_file = fopen(filename, "a");

  /* Obtain the time of day, and convert it to a tm struct. */
  gettimeofday(&tv, NULL);
  ptm = localtime(&tv.tv_sec);
  /* Format the date and time, down to a single second. */
  strftime(time_string, sizeof(time_string),
	   "%d-%m-%Y %H:%M:%S", ptm);
  
  /* Print the log. */
  if(log_file != NULL){
    fprintf(log_file, "%s: %s - %s\n", time_string, log_entry, eta);
    update_upds_window(wins, 3);
  }
  else{
    printf("%s: %s - %s\n\n", time_string, log_entry, eta);
    printf("\n");
    return 1;
  }
  
  return 0;
}

/* READ_INT_ARRAY_FROM_MAT_FILE */
/*****************************************************************************/
/**
  
  Reads array (int, NxNyNz) from MAT file with file descriptor matfp. 
  Requires libmatio-devel >= 1.5.12.

  @return 0 on success.\n
  +1 on failure to assign matvar_t.\n 
  -1 on failure to access data from matvar_t.\n
  -2 not correct data class.\n
  -3 not correct data type.\n
  -4 data size not equal to destination.
  
  MODIFIED: 08-07-2019
*/
int read_int_array_from_MAT_file(int *array, const char *var_name,
				 const unsigned int Nx, const unsigned int Ny,
				 const unsigned int Nz, mat_t *matfp){

  matvar_t *matvar = NULL;
  const int *values = NULL;
  
  matvar = Mat_VarRead(matfp, var_name);
  
  if(matvar == NULL){
    return 1;
  }
  
  values = (int *)matvar->data;

  if(values == NULL){
    return -1;
  }

  if(matvar->class_type != MAT_C_INT32){
    return -2;
  }
  
  if(matvar->data_type != MAT_T_INT32){
    return -3;
  }

  if(matvar->dims[0] != Nx || matvar->dims[1] != Ny || matvar->dims[2] != Nz){
    return -4;
  }
  
  copy_array_int(values, array, Nx, Ny, Nz);
  
  Mat_VarFree(matvar);
  
  return 0;
}

/* READ_ARRAY_FROM_MAT_FILE */
/*****************************************************************************/
/**

  Reads array (double, NxNyNz) from MAT file with file descriptor matfp. 
  Requires libmatio-devel >= 1.5.12.

  @return -0 on success.\n
  1 on failure to assign matvar_t.\n
  -1 on failure to access data from matvar_t.\n
  -2 not correct data class.\n
  -3 not correct data type.\n
  -4 data size not equal to destination.
  
  MODIFIED: 08-07-2019
*/
int read_array_from_MAT_file(double *array, const char *var_name,
			     const unsigned int Nx, const unsigned int Ny,
			     const unsigned int Nz, mat_t *matfp){

  matvar_t *matvar = NULL;
  const double *values = NULL;
  
  matvar = Mat_VarRead(matfp, var_name);
  
  if(matvar == NULL){
    return 1;
  }

  values = (double *)matvar->data;

  if(values == NULL){
    return -1;
  }

  if(matvar->class_type != MAT_C_DOUBLE){
    return -2;
  }
  
  if(matvar->data_type != MAT_T_DOUBLE){
    return -3;
  }

  if(matvar->dims[0] != Nx || matvar->dims[1] != Ny || matvar->dims[2] != Nz){
    return -4;
  }
  
  copy_array(values, array, Nx, Ny, Nz);
  
  Mat_VarFree(matvar);
    
  return 0;
}

/* WRITE_CHAR_ARRAY_IN_MAT_FILE */
/*****************************************************************************/
/**

  Saves array (char, NxNyNz) in MAT file with file descriptor matfp. 
  Requires libmatio-devel >= 1.5.12.
    
  @return 0 on success.\n
  -1 on NULL matfp or matvar.\n
  1 matvar exists.
  
  MODIFIED: 08-07-2019
*/
int write_char_array_in_MAT_file(char *array, const char *var_name,
				 const int Nx, const int Ny, const int Nz, mat_t *matfp){

  matvar_t *matvar = NULL;

  size_t dims[3];

  int ret_value = 0;

  dims[0] = Nx;
  dims[1] = Ny;
  dims[2] = Nz;

  /* Check if var_name exists */
  matvar = Mat_VarRead(matfp, var_name);
  
  if(matvar != NULL){
    /* var_name exists so we delete it from the MAT file */
    Mat_VarDelete(matfp, var_name);
  }

  /* We create the variable now (otherwise it will no overwrite it) */
  matvar = Mat_VarCreate(var_name, MAT_C_CHAR, MAT_T_UTF8, 3, dims, array,
			 MAT_F_DONT_COPY_DATA);
  
  if(matvar != NULL){
    ret_value = Mat_VarWrite(matfp, matvar, MAT_COMPRESSION_ZLIB);
    Mat_VarFree(matvar);
  }
  else{
    ret_value = -1;
  }
  
  return ret_value;
}

/* WRITE_INT_ARRAY_IN_MAT_FILE */
/*****************************************************************************/
/**

  Saves array (int, NxNyNz) in MAT file with file descriptor matfp. 
  Requires libmatio-devel >= 1.5.12.
    
  @return 0 on success.\n
  -1 on NULL matfp or matvar.\n
  1 matvar exists.
  
  MODIFIED: 08-07-2019
*/
int write_int_array_in_MAT_file(int *array, const char *var_name,
				const int Nx, const int Ny, const int Nz, mat_t *matfp){

  matvar_t *matvar = NULL;

  size_t dims[3];

  int ret_value = 0;

  dims[0] = Nx;
  dims[1] = Ny;
  dims[2] = Nz;

  /* Check if var_name exists */
  matvar = Mat_VarRead(matfp, var_name);
  
  if(matvar != NULL){
    /* var_name exists so we delete it from the MAT file */
    Mat_VarDelete(matfp, var_name);
  }

  /* We create the variable now (otherwise it will no overwrite it) */
  matvar = Mat_VarCreate(var_name, MAT_C_INT32, MAT_T_INT32, 3, dims, array,
			 MAT_F_DONT_COPY_DATA);
  
  if(matvar != NULL){
    ret_value = Mat_VarWrite(matfp, matvar, MAT_COMPRESSION_ZLIB);
    Mat_VarFree(matvar);
  }
  else{
    ret_value = -1;
  }
  
  return ret_value;
}

/* WRITE_ARRAY_IN_MAT_FILE */
/*****************************************************************************/
/**

   Saves array (double, NxNyNz) in MAT file with file descriptor matfp. 
   Requires libmatio-devel >= 1.5.12.

   @return 0 on success.\n
   -1 on NULL matfp or matvar.\n
   1 matvar exists.
  
   MODIFIED: 08-07-2019
*/
int write_array_in_MAT_file(double *array, const char *var_name,
			    const int Nx, const int Ny, const int Nz, mat_t *matfp){

  matvar_t *matvar = NULL;

  size_t dims[3];

  int ret_value = 0;

  dims[0] = Nx;
  dims[1] = Ny;
  dims[2] = Nz;

  /* Check if var_name exists */
  matvar = Mat_VarRead(matfp, var_name);
  
  if(matvar != NULL){
    /* var_name exists so we delete it from the MAT file */
    Mat_VarDelete(matfp, var_name);
  }

  /* We create the variable now (otherwise it will not overwrite it) */
  matvar = Mat_VarCreate(var_name, MAT_C_DOUBLE, MAT_T_DOUBLE, 3, dims, array,
			 MAT_F_DONT_COPY_DATA);
  
  if(matvar != NULL){
    ret_value = Mat_VarWrite(matfp, matvar, MAT_COMPRESSION_ZLIB);
    Mat_VarFree(matvar);
  }  
  else{
    ret_value = -1;
  }
  
  return ret_value;
}

/* SAVE_DATA_IN_MAT_FILE */
/*****************************************************************************/
/**
  Saves data in MAT file.

  Requires libmatio-devel >= 1.5.12.
   
  @param data
 
  @return 0 on success.\n
  -1 on failure if can not open the MAT file.

  @see read_from_MAT_file

  MODIFIED: 15-05-2021

  Data written:
    
*/
int save_data_in_MAT_file(Data_Mem *data){

  Wins *wins = &(data->wins);

  mat_t *matfp = NULL;
  
  const char *res_filename = data->res_filename;

  int ret_value = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int turb_OK = data->turb_model;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int solve_3D_OK = data->solve_3D_OK;
  
  matfp = Mat_Open(res_filename, MAT_ACC_RDWR);

  if(matfp == NULL){
    alert_msg(wins, "Could not write MAT file");
    return -1;
  }

  /** Data_Mem::nodes_x, Data_Mem::nodes_y, Data_Mem::nodes_z (if Data_Mem::solve_3D_OK) */
  checkCall(write_array_in_MAT_file(data->nodes_x, "x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->nodes_y, "y", Nx, Ny, Nz, matfp), wins);
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->nodes_z, "z", Nx, Ny, Nz, matfp), wins);
  }
  /** Data_Mem::U, Data_Mem::V, Data_Mem::W (if Data_Mem::solve_3D_OK), Data_Mem::p */
  checkCall(write_array_in_MAT_file(data->U, "U", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->V, "V", Nx, Ny, Nz, matfp), wins);
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->W, "W", Nx, Ny, Nz, matfp), wins);
  }
  checkCall(write_array_in_MAT_file(data->p, "p", Nx, Ny, Nz, matfp), wins);
  
  /** if Data_Mem::turb_model:\n Data_Mem::k_turb, Data_Mem::eps_turb, Data_Mem::mu_turb */
  if(turb_OK){
    checkCall(write_array_in_MAT_file(data->k_turb, "k_turb", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->eps_turb, "eps_turb", Nx, Ny, Nz, matfp),
	      wins);
    checkCall(write_array_in_MAT_file(data->mu_turb, "mu_turb", Nx, Ny, Nz, matfp), wins);
    if(data->turb_boundary_cell_count > 0){
      /** Data_Mem::y_plus (if Data_Mem::turb_boundary_cell_count > 0) */
      checkCall(write_array_in_MAT_file(data->y_plus, "y_plus", data->turb_boundary_cell_count, 1, 1, matfp), wins);
      checkCall(write_int_array_in_MAT_file(data->turb_boundary_indexs, "turb_boundary_indexs", data->turb_boundary_cell_count, 1, 1, matfp), wins);
    }
  }
  /** end if */

  /** Data_Mem::phi */
  checkCall(write_array_in_MAT_file(data->phi, "phi", Nx, Ny, Nz, matfp), wins);

  /** Data_Mem::gL (if Data_Mem::solve_phase_change_OK) */
  if(solve_phase_change_OK){
    checkCall(write_array_in_MAT_file(data->gL, "gL", Nx, Ny, Nz, matfp), wins);
  }

  /* Flow data */
  checkCall(write_int_array_in_MAT_file(&(data->max_iter_flow),
					"max_it_flow", 1, 1, 1, matfp), wins);
  checkCall(write_int_array_in_MAT_file(&(data->sigmas.flow_it),
					"it_flow", 1, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(&(data->tol_flow),
				    "tol_flow", 1, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(data->sigmas.flow_values,
				    "sigmas_flow", 7, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(data->sigmas.flow_glob_norm,
				    "glob_norm_flow", 7, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(&(data->cputime_flow_solver),
				    "cputime_flow_solver", 1, 1, 1, matfp), wins);
  /* Phi data */
  checkCall(write_int_array_in_MAT_file(&(data->max_it),
					"max_it_phi", 1, 1, 1, matfp), wins);
  checkCall(write_int_array_in_MAT_file(&(data->sigmas.phi_it),
					"phi_it", 1, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(&(data->tol_phi),
				    "tol_phi", 1, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(data->sigmas.phi_values,
				    "sigmas_phi", 2, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(data->sigmas.phi_glob_norm,
				    "glob_norm_phi", 2, 1, 1, matfp), wins);
  checkCall(write_array_in_MAT_file(&(data->cputime_phi_solver),
				    "cputime_phi_solver", 1, 1, 1, matfp), wins);
  
  Mat_Close(matfp);
  
  update_upds_window(wins, 4);
  
  return ret_value;
}

/* COMPUTE_VOLUMETRIC_AVERAGE_PHASE_PHI */
/*****************************************************************************/
/**
 
  Computes volumetric average value of phi for pertaining phase using the 
  solid volume fraction for the main grid.

  @param data

  @param phase: index of phase where average is taken.

  @return (double, 1), volumetric phase average value of phi.
  NAN, if no phase point is encountered.
  
  MODIFIED: 01-09-2020
*/
double compute_volumetric_average_phase_phi(Data_Mem *data, const int phase){

  int I, J, K, index_;
  
  const int Nx = data->Nx; 
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *dom_matrix = data->domain_matrix;

  double phiav = NAN;
  double vol = 0;
  double num = 0, den = 0;
  
  const double *svf = data->v_fraction;
  const double *dx = data->Delta_x;
  const double *dy = data->Delta_y;
  const double *dz = data->Delta_z;
  const double *phi = data->phi;

#pragma omp parallel for default(none)		\
  private(I, J, K, index_, vol)			\
  shared(dom_matrix, dx, dy, dz, svf, phi)	\
  reduction(+:num)				\
  reduction(+:den)
  
  for(K=1; K<Nz; K++){
    for(J=1; J<Ny; J++){
      for(I=1; I<Nx; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;
	
	if(dom_matrix[index_] == phase){
	  vol = dx[index_]*dy[index_]*dz[index_] * svf[index_];
	  num = num + phi[index_]*vol;
	  den = den + vol;
	}
	
      }
    }
  }

  if(den != 0){
    phiav = num/den;
  }
  
  return phiav;
}

/* COMPUTE_SURFACE_AVERAGE_PHASE_PHI */
/*****************************************************************************/
/**
 
  Computes surface average value of phi for pertaining phase using the 
  solid volume fraction for the main grid at z = 0 for the moment.

  @param data

  @param phase: index of phase where average is taken.

  @return (double, 1), surface phase average value of phi.
  NAN, if no phase point is encountered.
  
  MODIFIED: 01-09-2020
*/
double compute_surface_average_phase_phi(Data_Mem *data, const int phase){

  int I, J, index_;

  const int K = 0;
  const int dz = 1;
  
  const int Nx = data->Nx; 
  const int Ny = data->Ny;

  const int *dom_matrix = data->domain_matrix;

  double phiav = NAN;
  double vol = 0;
  double num = 0, den = 0;
  
  const double *svf = data->v_fraction;
  const double *dx = data->Delta_x;
  const double *dy = data->Delta_y;
  const double *phi = data->phi;

#pragma omp parallel for default(none)		\
  private(I, J, index_, vol)			\
  shared(dom_matrix, dx, dy, svf, phi)		\
  reduction(+:num)				\
  reduction(+:den)
  
  for(J=1; J<Ny; J++){
    for(I=1; I<Nx; I++){
      
      index_ = K*Nx*Ny + J*Nx + I;
      
      if(dom_matrix[index_] == phase){
	vol = dx[index_]*dy[index_]*dz * svf[index_];
	num = num + phi[index_]*vol;
	den = den + vol;
      }
      
    }
  }
  

  if(den != 0){
    phiav = num/den;
  }
  
  return phiav;
}

/* COMPUTE_COURANT_NUMBER */
int compute_courant_number(Data_Mem *data){

  const char *results_dir = data->results_dir;

  int ret_value = 0;

  int I, J, i, j;
  int index_, index_u, index_v;

  const int Nx = data->Nx; 
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;  

  double dt_x = 0, dt_y = 0;
  double dx = 1, dy = 1;

  const double rho = data->rho_v[0];

  const double alpha_u = data->alpha_u;
  const double alpha_v = data->alpha_v;

  double *c_x = data->c_x;
  double *c_y = data->c_y;

  const double *u = data->u;
  const double *v = data->v;
  
  const double *vol_x = data->vol_x;
  const double *vol_y = data->vol_y;

  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;

  const double *vf_x = data->vf_x;
  const double *vf_y = data->vf_y;

  // In what state of relaxation are these coefficients??
  // We assume that they are ALREADY relaxed, e.g., aP = aP/alpha was performed
  const double *aP_u = data->coeffs_u.aP;
  const double *aP_v = data->coeffs_v.aP;
  
  if(alpha_u != 1){
#pragma omp parallel for default(none)					\
  private(I, J, i, index_, index_u, dt_x, dx)				\
  shared(vol_x, aP_u, vf_x, eps_x, Delta_xW, u, c_x, u_dom_matrix)

    for(J=1; J<Ny-1; J++){
      for(i=1; i<nx-1; i++){

	I = i+1;

	index_ = J*Nx + I;
	index_u = J*nx + i;

	if(u_dom_matrix[index_u] == 0){

	  dt_x = (rho * vol_x[index_u] / aP_u[index_u]) * (1 / (1 - alpha_u));
	  
	  if(vf_x[index_u] > 0){
	    
	    dx = MAX(eps_x[index_-1], eps_x[index_]) * Delta_xW[index_];
	    
	  }
	  else{
	    dx = Delta_xW[index_];
	  }
	  
	  c_x[index_u] = dt_x * u[index_u] / dx;
	}
      }
    }
    write_double_to_file(c_x, nx, Ny, "c_x", 1, results_dir);
  }
  else{
    ret_value = -1;
  }

  if(alpha_v != 0){
#pragma omp parallel for default(none) private(I, J, j, index_, index_v, dt_y, dy) \
  shared(vol_y, aP_v, vf_y, eps_y, Delta_yS, v, c_y, v_dom_matrix)    

    for(j=1; j<ny-1; j++){
      for(I=1; I<Nx-1; I++){
	
	J = j+1;
	
	index_ = J*Nx + I;
	index_v = j*Nx + I;

	if(v_dom_matrix[index_v] == 0){
	   
	  dt_y = (rho * vol_y[index_v] / aP_v[index_v]) * (1 / (1 - alpha_v));
	   
	  if(vf_y[index_v] > 0){
	     
	    dy = MAX(eps_y[index_-Nx], eps_y[index_]) * Delta_yS[index_];
	     
	  }
	  else{
	    dy = Delta_yS[index_];
	  }
	   
	  c_y[index_v] = dt_y * v[index_v] / dy;
	}
      }
    }

    write_double_to_file(c_y, Nx, ny, "c_y", 1, results_dir);
  }
  else{
    ret_value = -2;
  }
  
  return ret_value;
  
}

/* CLOSE_SFILE */
void close_sfile(Data_Mem *data){

  Wins *wins = &(data->wins);

  const char *sigmas_filename = data->sigmas_filename;

  FILE *fp = NULL;

  fp = fopen(sigmas_filename, "a");

  if(fp == NULL){
    error_msg(wins, "Could not create/open file for sigmas");
  }
  
  fprintf(fp, "];\n");
  
  fclose(fp);
  
  return;
}

/* WRITE_SFILE */
/*****************************************************************************/
/**

   Writes sigma values to sigmas_filename file. Write flow values
   if Data_Mem::flow_done_OK is False, and phi values otherwise.

   @param data

   @return void
 */
void write_sfile(Data_Mem *data){

  const char *sigmas_filename = data->sigmas_filename;

  const int flow_done_OK = data->flow_done_OK;
  
  const Sigmas *sigmas = &(data->sigmas);
  
  Wins *wins = &(data->wins);
  
  FILE *fp = NULL;

  static int count_flow = 0;
  static int count_phi = 0;
  
  fp = fopen(sigmas_filename, "a");

  if(fp == NULL){
    error_msg(wins, "Could not create/open file for sigmas");
  }

  if(!flow_done_OK){
    if(count_flow == 0){

      fprintf(fp, "%% %9s %11s %11s %11s %11s %11s %11s %11s\n",
	      "iter",
	      "u", "v", "w", "c", "k", "eps", "mass_io");       
      fprintf(fp, "sflow_cpu_var = [\n");
      
    }
    else{
      fprintf(fp, "%11d %11.3e %11.3e %11.3e %11.3e %11.3e %11.3e %11.3e;\n",
	      sigmas->it,
	      sigmas->values[0], sigmas->values[1], sigmas->values[2],
	      sigmas->values[3], sigmas->values[4],
	      sigmas->values[5], sigmas->values[6]);
    }
    count_flow++;
  }
  else{

    if(count_phi == 0){
      
      fprintf(fp, "sphi_cpu_var = [\n");
      
    }
    else{
      if(data->steady_state_OK){
	fprintf(fp, "%11d %11.3e %11.3e;\n",
		sigmas->it,
		sigmas->values[7], sigmas->values[8]);
      }
      else{
	fprintf(fp, "%11d %8d %11.3e %11.3e;\n",
		sigmas->it, sigmas->sub_it,
		sigmas->values[7], sigmas->values[8]);
      }
    }
    
    count_phi++;
  }
  
  fclose(fp);
  
  return;  
}

/* WRITE_GNRES_SFILE */
/*****************************************************************************/
/**
   Write glob_norm values to sigmas_filename at the end of the flow calculation.

   @param data

   @return void
 */
void write_gnres_sfile(Data_Mem *data){

  const int turb_model = data->turb_model;

  const double *glob_norm = data->sigmas.glob_norm;

  const char *sigmas_filename = data->sigmas_filename;
  
  FILE *fp = NULL;

  Wins *wins = &(data->wins);
  
  fp = fopen(sigmas_filename, "a");

  if(fp == NULL){
    error_msg(wins, "Could not create/open file for sigmas");
  }
  else{
    fprintf(fp, "glob_norm_res_var = [ ");
    if(turb_model){
     
      fprintf(fp, "%11.3e %11.3e %11.3e %11.3e %11.3e];\n",
	      glob_norm[0], glob_norm[1], glob_norm[2],
	      glob_norm[4], glob_norm[5]);
    }
    else{

      fprintf(fp, "%11.3e %11.3e %11.3e];\n",
	      glob_norm[0], glob_norm[1], glob_norm[2]);
    }
  }

  fclose(fp);
  
  return;
}

/* PROCESS_SFILE NOT WORKING!! */
void process_sfile(Data_Mem *data){

  const char *sigmas_filename = data->sigmas_filename;
  const Sigmas *sigmas = &(data->sigmas);
  Wins *wins = &(data->wins);
  
  FILE *fp = NULL;

  static int count = 0;
  static long int pos = 0;

  fp = fopen(sigmas_filename, "a+");

  if(fp == NULL){
    error_msg(wins, "Could not create/open file for sigmas");
  }

  if(count == 0){
    
    printf("First time writting here\n\n");
    /* This is the first time we open the file */
    fprintf(fp, "sigmas = [\n");
    
  }
  else{
    
    /* Not the first time here so we need to go to the correct position */
    if(fseek(fp, pos, SEEK_SET) != 0){
      error_msg(wins, "fseek error");
    }
    printf("pos %ld so I go to %ld\n\n", pos, ftell(fp));
    
    fprintf(fp, "%6d; %11.3e; %11.3e; %11.3e; %11.3e; %11.3e; %11.3e;\n",
	    sigmas->it,
	    sigmas->values[0], sigmas->values[1], sigmas->values[2],
	    sigmas->values[3], sigmas->values[4],
	    sigmas->values[5]);
  }

  /* Save position in file descriptor */
  pos = ftell(fp);
  pos = pos + 1;
  fprintf(fp, "%85s\n", "];");
  printf("My current position is %ld, while pos is %ld\n\n", ftell(fp), pos);
  
  fclose(fp);
  
  count++;

  return;  
}

/* WRITE_TO_GEOM_FILE */
/*****************************************************************************/
/**

   Requires libmatio-devel >= 1.5.12.

   @param data

   @return void

   @see read_from_geom_file

   Data written:
   
 */
void write_to_geom_file(Data_Mem *data){
  
  mat_t *matfp = NULL;
  
  Wins *wins = &(data->wins);
    
  char msg[SIZE];

  const char *geom_filename = data->geom_filename;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int turb_model = data->turb_model;
  const int solve_3D_OK = data->solve_3D_OK;

  matfp = get_matfp(geom_filename, 1, wins);

  /** Data_Mem::y_wall (if Data_Mem::turb_model). */
  if(turb_model){
    checkCall(write_array_in_MAT_file(data->y_wall, "y_wall", Nx, Ny, Nz, matfp), wins);
  }

  /** Data_Mem::u_dom_matrix, Data_Mem::v_dom_matrix. */
  checkCall(write_int_array_in_MAT_file(data->u_dom_matrix, "u_dom_matrix",
					nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->v_dom_matrix, "v_dom_matrix",
					Nx, ny, Nz, matfp), wins);

  /** if Data_Mem::solve_3D_OK Data_Mem::w_dom_matrix. */
  if(solve_3D_OK){
    checkCall(write_int_array_in_MAT_file(data->w_dom_matrix, "w_dom_matrix",
					  Nx, Ny, nz, matfp), wins);
  }

  /** Data_Mem::p_dom_matrix */
  checkCall(write_int_array_in_MAT_file(data->p_dom_matrix, "p_dom_matrix",
					Nx, Ny, Nz, matfp), wins);
  
  /** Data_Mem::u_cut_matrix, Data_Mem::v_cut_matrix. */
  checkCall(write_int_array_in_MAT_file(data->u_cut_matrix, "u_cut_matrix",
					nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->v_cut_matrix, "v_cut_matrix",
					Nx, ny, Nz, matfp), wins);
  /** if Data_Mem::solve_3D_OK Data_Mem::w_cut_matrix. */
  if(solve_3D_OK){
    checkCall(write_int_array_in_MAT_file(data->w_cut_matrix, "w_cut_matrix",
					  Nx, Ny, nz, matfp), wins);
  }

  /** Data_Mem::u_faces_x, Data_Mem::u_faces_y. */
  checkCall(write_array_in_MAT_file(data->u_faces_x, "u_faces_x",
				    Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->u_faces_y, "u_faces_y",
				    nx, ny, Nz, matfp), wins);
  /** if Data_Mem::solve_3D_OK Data_Mem::u_faces_z. */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->u_faces_z, "u_faces_z",
				      nx, Ny, nz, matfp), wins);
  }

  /** Data_Mem::v_faces_x, Data_Mem::v_faces_y. */
  checkCall(write_array_in_MAT_file(data->v_faces_x, "v_faces_x",
				    nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_faces_y, "v_faces_y",
				    Nx, Ny, Nz, matfp), wins);
  /** if Data_Mem::solve_3D_OK Data_Mem::v_faces_z. */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->v_faces_z, "v_faces_z",
				      Nx, ny, nz, matfp), wins);			 
  }

  /** if Data_Mem::solve_3D_OK Data_Mem::w_faces_x, Data_Mem::w_faces_y, Data_Mem::w_faces_z */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->w_faces_x, "w_faces_x",
				      nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->w_faces_y, "w_faces_y",
				      Nx, ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->w_faces_z, "w_faces_z",
				      Nx, Ny, Nz, matfp), wins);
  }
  
  /** Data_Mem::p_cut_face_x, Data_Mem::p_cut_face_y. */
  checkCall(write_int_array_in_MAT_file(data->p_cut_face_x, "p_cut_face_x",
					nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->p_cut_face_y, "p_cut_face_y",
					Nx, ny, Nz, matfp), wins);
  /** if Data_Mem::solve_3D_OK Data_Mem::p_cut_face_z. */
  if(solve_3D_OK){
    checkCall(write_int_array_in_MAT_file(data->p_cut_face_z, "p_cut_face_z",
					  Nx, Ny, nz, matfp), wins);
  }

  /** Displaced coordinates: */
  /** Data_Mem::nodes_y_u, Data_Mem::nodes_x_v. */
  checkCall(write_array_in_MAT_file(data->nodes_y_u, "nodes_y_u",
				    nx, Ny, Nz, matfp), wins);  
  checkCall(write_array_in_MAT_file(data->nodes_x_v, "nodes_x_v",
				    Nx, ny, Nz, matfp), wins);
  /** if Data_Mem::solve_3D_OK Data_Mem::nodes_z_u, Data_Mem::nodes_z_v,
      Data_Mem::nodes_x_w, Data_Mem::nodes_y_w. */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->nodes_z_u, "nodes_z_u",
				      nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nodes_z_v, "nodes_z_v",
				      Nx, ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nodes_x_w, "nodes_x_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nodes_y_w, "nodes_y_w",
				      Nx, Ny, nz, matfp), wins);
  }
  

  /** Data_Mem::p_faces_x, Data_Mem::p_faces_y. */
  checkCall(write_array_in_MAT_file(data->p_faces_x, "p_faces_x",
				    nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->p_faces_y, "p_faces_y",
				    Nx, ny, Nz, matfp), wins);
  /** Data_Mem::solve_3D_OK Data_Mem::p_faces_z. */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->p_faces_z, "p_faces_z",
				      Nx, Ny, nz, matfp), wins);
  }

  /** Distance to neighbours for displaced coordinates: */
  /** Data_Mem::Delta_yN_u, Data_Mem::Delta_yS_u, Data_Mem::Delta_yn_u, Data_Mem::Delta_ys_u. */
  checkCall(write_array_in_MAT_file(data->Delta_yN_u, "Delta_yN_u",
				    nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_yS_u, "Delta_yS_u",
				    nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_yn_u, "Delta_yn_u",
				    nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_ys_u, "Delta_ys_u",
				    nx, Ny, Nz, matfp), wins);

  /** if Data_Mem::solve_3D_OK Data_Mem::Delta_zT_u, Data_Mem::Delta_zB_u, 
      Data_Mem::Delta_zt_u, Data_Mem::Delta_zb_u. */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->Delta_zT_u, "Delta_zT_u",
				      nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_zB_u, "Delta_zB_u",
				      nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_zt_u, "Delta_zt_u",
				      nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_zb_u, "Delta_zb_u",
				      nx, Ny, Nz, matfp), wins);
  }

  /** Data_Mem::Delta_xE_v, Data_Mem::Delta_xW_v, Data_Mem::Delta_xe_v, Data_Mem::Delta_xw_v. */
  checkCall(write_array_in_MAT_file(data->Delta_xE_v, "Delta_xE_v",
				    Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_xW_v, "Delta_xW_v",
				    Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_xe_v, "Delta_xe_v",
				    Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_xw_v, "Delta_xw_v",
				    Nx, ny, Nz, matfp), wins);

  /** if Data_Mem::solve_3D_OK Data_Mem::Delta_zT_v, Data_Mem::Delta_zB_v,
      Data_Mem::Delta_zt_v, Data_Mem::Delta_zb_v. */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->Delta_zT_v, "Delta_zT_v",
				      Nx, ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_zB_v, "Delta_zB_v",
				      Nx, ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_zt_v, "Delta_zt_v",
				      Nx, ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_zb_v, "Delta_zb_v",
				      Nx, ny, Nz, matfp), wins);
  }

  /** if Data_Mem::solve_3D_OK Data_Mem::Delta_xE_w, Data_Mem::Delta_xW_w,
      Data_Mem::Delta_xe_w, Data_Mem::Delta_xw_w, Data_Mem::Delta_yN_w, 
      Data_Mem::Delta_yS_w, Data_Mem::Delta_yn_w, Data_Mem::Delta_ys_w. */
  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->Delta_xE_w, "Delta_xE_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_xW_w, "Delta_xW_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_xe_w, "Delta_xe_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_xw_w, "Delta_xw_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_yN_w, "Delta_yN_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_yS_w, "Delta_yS_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_yn_w, "Delta_yn_w",
				      Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Delta_ys_w, "Delta_ys_w",
				      Nx, Ny, nz, matfp), wins); 
  }
  
  Mat_Close(matfp);

  sprintf(msg, "Data written to %s", geom_filename);
  info_msg(wins, msg);
  
  return;   
}

/* CONSTRUCT_QUADRICS */
void construct_quadrics(Data_Mem *data){

  int count;
  const int curves = data->curves;
  
  if(curves > 0){
    for(count = 0; count < curves; count++){
      if(data->quadric_OK[count]){
	/* Construct quadric surface */
	make_surface(data, 
		     data->angle_x[count], data->angle_y[count], 
		     data->angle_z[count], 
		     data->lambda_1[count], data->lambda_2[count], 
		     data->lambda_3[count], 
		     data->constant_d[count], 
		     data->tcompx[count], data->tcompy[count], 
		     data->tcompz[count], 
		     data->poly_coeffs, count);
      }
      else{
	/* Direct polynomial assignment */
	data->poly_coeffs[count*16 + 0] = data->lambda_1[count];
	data->poly_coeffs[count*16 + 1] = data->lambda_2[count];
	data->poly_coeffs[count*16 + 2] = data->lambda_3[count];
	
	data->poly_coeffs[count*16 + 3] = data->angle_x[count];
	data->poly_coeffs[count*16 + 4] = data->angle_y[count];
	data->poly_coeffs[count*16 + 5] = data->angle_z[count];

	data->poly_coeffs[count*16 + 6] = data->tcompx[count];
	data->poly_coeffs[count*16 + 7] = data->tcompy[count];
	data->poly_coeffs[count*16 + 8] = data->tcompz[count];

	data->poly_coeffs[count*16 + 9] = data->constant_d[count];

      }
    }
  }
  
  return;
}

/* SET_BORDERS_TO NAN */
void set_nsew_to_value(double *A_, const int Nx, const int Ny,
		       const double value){

  set_bc_value(A_, value, 'N', Nx, Ny);
  set_bc_value(A_, value, 'S', Nx, Ny);
  set_bc_value(A_, value, 'E', Nx, Ny);
  set_bc_value(A_, value, 'W', Nx, Ny);

  return;
}

/* SET_TRANSIENT_SIM_TIME_FILENAME */
/*****************************************************************************/
/**

   Sets name of file that stores simulation time values corresponding to transient results file.

   @param *filename: pointer to char array where filename is returned.

   @param *msg: pointer to name of simulation times file (without directory information).

   @param flag: if results_dir is NULL, set simulation times file directory to ./data/CPU/ for flag == 0
   and to ./data/GPU/ for flag != 0.

   @param *results_dir: pointer to directory where simulation times file is to be stored.
 */
void set_transient_sim_time_filename(char *filename,
				     const char *msg,
				     const int flag,
				     const char *results_dir){

  if(results_dir == NULL){
    
    if (flag == 0){
      sprintf(filename, "%s", "./data/CPU/");
      strncat(filename, msg, SIZE);
    }
    else{
      sprintf(filename, "%s", "./data/GPU/");
      strncat(filename, msg, SIZE);
    }
    
    strncat(filename, ".dat", 5);
    
  }
  
  else{
    
    snprintf(filename, SIZE, "%s%s", results_dir, msg);
    strncat(filename, ".dat", 5);
  }
  
  return;
}

/* SAVE_TRANSIENT_DATA */
void save_transient_data(Data_Mem * data, const char* tr_filename){

  MATIOS *matios = &(data->matios);

  Wins *wins = &(data->wins);
  
  FILE *fp = NULL;

  mat_t *matfp = matios->matfp_tr;
  
  const int solve_phase_change_OK = data->solve_phase_change_OK;

  const int coupled_physics_OK = data->coupled_physics_OK;
  const int flow_done_OK = data->flow_done_OK;
  const int still_computing_coupled_physics_OK = coupled_physics_OK && flow_done_OK;
  const int tos = data->TEG_OK_summary;

  double time = 0;

  time = still_computing_coupled_physics_OK ? data->dt[0] : data->dt[6];

  fp = fopen(tr_filename, "a");
  fprintf(fp, "%.1f\n", time);
  fclose(fp);

  if(matfp == NULL){
    error_msg(wins, "Could not open transient MAT file");
  }
  
  checkCall(Mat_VarWriteAppend(matfp, matios->phi, MAT_COMPRESSION_ZLIB, 4), wins);

  if(solve_phase_change_OK){
    checkCall(Mat_VarWriteAppend(matfp, matios->gL, MAT_COMPRESSION_ZLIB, 4), wins);
  }

  checkCall(Mat_VarWriteAppend(matfp, matios->tphi, MAT_COMPRESSION_ZLIB, 2), wins);

  if(tos){
    checkCall(Mat_VarWriteAppend(matfp, matios->current, MAT_COMPRESSION_ZLIB, 2), wins);
    checkCall(Mat_VarWriteAppend(matfp, matios->voltage, MAT_COMPRESSION_ZLIB, 2), wins);
    checkCall(Mat_VarWriteAppend(matfp, matios->power, MAT_COMPRESSION_ZLIB, 2), wins);
  }
  
#ifndef DISPLAY_OK
  info_msg(wins, "Transient data saved");
#endif
  update_upds_window(wins, 6);
  
  return;
}

/* SAVE_FLOW_TRANSIENT_DATA */
void save_flow_transient_data(Data_Mem * data, const char* tr_filename){

  MATIOS *matios = &(data->matios);
  
  Wins *wins = &(data->wins);
  
  FILE *fp = NULL;

  mat_t *matfp = matios->matfp_tr;
  
  const int turb_model = data->turb_model;
  
  fp = fopen(tr_filename, "a");
  fprintf(fp, "%.4f\n", data->dt[0]);
  fclose(fp);

  if(matfp == NULL){
    error_msg(wins, "Could not open transient MAT file");
  }

  /* Saving centered velocity components */
  center_vel_components(data);
  compute_vel_at_main_cells(data);
  
  checkCall(Mat_VarWriteAppend(matfp, matios->U, MAT_COMPRESSION_ZLIB, 4), wins);
  checkCall(Mat_VarWriteAppend(matfp, matios->V, MAT_COMPRESSION_ZLIB, 4), wins);
  checkCall(Mat_VarWriteAppend(matfp, matios->W, MAT_COMPRESSION_ZLIB, 4), wins);
  checkCall(Mat_VarWriteAppend(matfp, matios->p, MAT_COMPRESSION_ZLIB, 4), wins);

  checkCall(Mat_VarWriteAppend(matfp, matios->tflow, MAT_COMPRESSION_ZLIB, 2), wins);
    
  if(turb_model){
    checkCall(Mat_VarWriteAppend(matfp, matios->k, MAT_COMPRESSION_ZLIB, 4), wins);
    checkCall(Mat_VarWriteAppend(matfp, matios->e, MAT_COMPRESSION_ZLIB, 4), wins);
  }
  
#ifndef DISPLAY_OK
  info_msg(wins, "Flow transient data saved");
#endif
  update_upds_window(wins, 6);
  
  return;
}

/* SAVE_TRANSIENT_ARRAY */
int save_transient_array(const double *A_,
			 const int Nx, const int Ny,
			 const int count,
			 const char *msg, const int flag,
			 const char *results_dir){

  FILE *fp;
  char filename[SIZE];
  int I, J;
  int ret_value = 0;

  if(results_dir == NULL){

    if (strnlen(msg, SIZE) < 1){
      // error_msg(wins, "Give a correct name for variable to print\n");
      ret_value = 1;
      exit(EXIT_FAILURE);
    }
    
    if (flag == 0){
      sprintf(filename, "%s", "./data/CPU/");
      strncat(filename, msg, SIZE-1);
    }
    else{
      sprintf(filename, "%s", "./data/GPU/");
      strncat(filename, msg, SIZE-1);
    }
    
    strncat(filename, ".dat", 5);
    
    fp = fopen(filename, "a");
    
    if (fp == NULL){
      //      error_msg(wins, "Could not open file, permissions??...");
      ret_value = -1;
    }
    
    if (flag == 0){
      fprintf(fp, "%s_cpu_var(:,:,%d) = [\n", msg, count);
    }
    else{
      fprintf(fp, "%s_gpu_var(:,:,%d) = [\n", msg, count);
    }
  }
  
  else{
    snprintf(filename, SIZE, "%s%s", results_dir, msg);
    strncat(filename, ".dat", 5);
    
    fp = fopen(filename, "a");
    
    if (fp == NULL){
      //      error_msg(wins, "Could not open file, permissions??...");
      ret_value = -1;
      exit(EXIT_FAILURE);
    }
    
    fprintf(fp, "%s_cpu_var(:,:,%d) = [\n", msg, count);
  }
  
  for (I = 0; I < Nx; I++) {  
    for (J = 0; J < Ny; J++){
      fprintf(fp, "%+14.7g ", A_[J*Nx + I]);
    }
    if (I == Nx-1){
      fprintf(fp, "\n");
    }
    else {
      fprintf(fp, ";\n");
    }
  }
  
  fprintf(fp, "];\n");
  
  fclose(fp);
 
  return ret_value;
}

/* FILTER_SMALL_VALUES_OMP */
/*****************************************************************************/
/**

   Set array values that satisfy (value < small_number) to small_number or zero.

   @param *A_: pointer to array.
   
   @param Nx, Ny, Nz: array size.

   @param small_number

   @param filter_to_zero_OK: if True, values are set to zero.

   @return void

   MODIFIED: 17-07-2020
 */
void filter_small_values_omp(double *A_, const int Nx, const int Ny, const int Nz,
			     const double small_number, const int filter_to_zero_OK){

  int I, J, K, index_;

  if(filter_to_zero_OK){
#pragma omp parallel private(I,J,K,index_) shared(A_)
    {
#pragma omp for
      for(K=0; K<Nz; K++){
	for(J=0; J<Ny; J++){
	  for(I=0; I<Nx; I++){
	    index_ = K*Ny*Nx + J*Nx + I;
	    if(A_[index_] < small_number){
	      A_[index_] = 0.0;
	    }
	  }
	}
      }
    }
  }
  else{
#pragma omp parallel private(I,J,K,index_) shared(A_)
    {
#pragma omp for
      for(K=0; K<Nz; K++){
	for(J=0; J<Ny; J++){
	  for(I=0; I<Nx; I++){
	    index_ = K*Ny*Nx + J*Nx + I;
	    if(A_[index_] < small_number){
	      A_[index_] = small_number;
	    }
	  }
	}
      }
    }
  }
  
  return;
}

/* SUM RELAX ARRAY */
/*****************************************************************************/
/**
   Set array C values to alpha * A + (1 - alpha) * B

   @param *A_, *B_, *C_: pointers to arrays.

   @param alpha: relaxation factor.

   @param Nx, Ny, Nz: arrays' size.

   @return void

   MODIFIED: 17-07-2020
 */
void sum_relax_array(const double *A_, const double *B_, double *C_, 
		     const double alpha, const int Nx, const int Ny, const int Nz){
  int I, J, K, index_;
  
#pragma omp parallel private(I,J,K,index_) shared(A_,B_,C_)
  {
#pragma omp for
    for(K=0; K<Nz; K++){
      for(J=0; J<Ny; J++){
	for(I=0; I<Nx; I++){
	  index_ = K*Ny*Nx + J*Nx + I;
	  C_[index_] = alpha*A_[index_] + (1-alpha)*B_[index_];
	}
      }
    }
  }
  
  return;
}

/* PRINT_TENSOR_AT_NODE */
void print_tensor_at_node(const double *tensor, const int I, const int J, 
			  const int Nx, const int Ny, const char *msg){
  int index, offset;
  
  if ( I<0 || I>Nx-1 || J<0 || J>Ny-1){
    // error_msg(wins, "Tensor index exceeds dimension");
    exit(EXIT_FAILURE);
  }
  
  index = J*Nx + I;
  offset = Nx*Ny;
  
  printf("\n%s(%d, %d) = \n", msg, I, J);
  printf("%+14g\t%+14g\n", tensor[index+0*offset], tensor[index+1*offset]);
  printf("%+14g\t%+14g\n", tensor[index+2*offset], tensor[index+3*offset]);
  printf("\n");
  
  return;
}

/* MAKE_PARABOLIC_PROFILE */
/*****************************************************************************/
/**
  
  Sets:

  Array (1, Nx||Ny) i_ii_non_uni: set values at open section of boundary, 
  when parabolize_profile_OK is set to true.

  i : u, v, k, eps

  ii : E, W, N, S
  
  @return void
  
  MODIFIED: 05-03-2019
*/
void make_parabolic_profile(Data_Mem *data, const int l_, const int u_, 
			    const char face){

  int I, J, i, j;
  int index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  double B, pos_offset, x;
  double vel_max;
  
  double vel_avg;

  double *u_E_non_uni = data->u_E_non_uni;
  double *u_W_non_uni = data->u_W_non_uni;
  double *v_N_non_uni = data->v_N_non_uni;
  double *v_S_non_uni = data->v_S_non_uni;
  double *k_E_non_uni = data->k_E_non_uni;
  double *k_W_non_uni = data->k_W_non_uni;
  double *k_N_non_uni = data->k_N_non_uni;
  double *k_S_non_uni = data->k_S_non_uni;
  double *eps_E_non_uni = data->eps_E_non_uni;
  double *eps_W_non_uni = data->eps_W_non_uni;
  double *eps_N_non_uni = data->eps_N_non_uni;
  double *eps_S_non_uni = data->eps_S_non_uni;

  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_u = data->nodes_y_u;

  const b_cond_flow *bc_flow_west = &(data->bc_flow_west);
  const b_cond_flow *bc_flow_east = &(data->bc_flow_east);
  const b_cond_flow *bc_flow_south = &(data->bc_flow_south);
  const b_cond_flow *bc_flow_north = &(data->bc_flow_north);
   
  if((face == 'E') || (face == 'e')){
    i = nx-1;

    vel_avg = bc_flow_east->v_value[0];
    vel_max = 0;
    pos_offset = nodes_y_u[l_*nx+i];
    
    /* Symmetry at north boundary */
    if((u_ == Ny-1) && (bc_flow_north->type == 2)){
      B = 2 * (nodes_y_u[u_*nx+i] - pos_offset);
    }
    /* Symmetry at south boundary */
    else if((l_ == 0) && (bc_flow_south->type == 2)){
      B = 2 * (nodes_y_u[u_*nx+i] - pos_offset);
      pos_offset -= 0.5 * B;
    }
    /* Default */
    else{
      B = nodes_y_u[u_*nx+i] - pos_offset; 
    }

    for(J=l_; J<u_+1; J++){

      index_u = J*nx+i;

      x = nodes_y_u[index_u] - pos_offset;

      u_E_non_uni[J] = vel_avg * (6/B) * x * (1 - (x/B));
      vel_max = MAX(vel_max, fabs(u_E_non_uni[J]));
    }

    for(J=l_; J<u_+1; J++){
      k_E_non_uni[J] = bc_flow_east->k_value * fabs(u_E_non_uni[J] / vel_max);
      eps_E_non_uni[J] = bc_flow_east->eps_value * fabs(u_E_non_uni[J] / vel_max);
    }
    
  }
  
  if((face == 'W') || (face == 'w')){
    i = 0;

    vel_avg = bc_flow_west->v_value[0];
    vel_max = 0;
    pos_offset = nodes_y_u[l_*nx+i];

    /* Symmetry at north boundary */
    if((u_ == Ny-1) && (bc_flow_north->type == 2)){
      B = 2 * (nodes_y_u[u_*nx+i] - pos_offset);      
    }
    /* Symmetry at south boundary */
    else if((l_ == 0) && (bc_flow_south->type == 2)){
      B = 2 * (nodes_y_u[u_*nx+i] - pos_offset);      
      pos_offset -= 0.5 * B;
    }
    /* Default */
    else{
      B = nodes_y_u[u_*nx+i] - pos_offset;
    }

    for(J=l_; J<u_+1; J++){

      index_u = J*nx+i;

      x = nodes_y_u[index_u] - pos_offset;

      u_W_non_uni[J] = vel_avg * (6/B) * x * (1 - (x/B));
      vel_max = MAX(vel_max, fabs(u_W_non_uni[J]));
    }

    for(J=l_; J<u_+1; J++){
      k_W_non_uni[J] = bc_flow_west->k_value * fabs(u_W_non_uni[J] / vel_max);
      eps_W_non_uni[J] = bc_flow_west->eps_value * fabs(u_W_non_uni[J] / vel_max);
    }
  }
  
  if((face == 'N') || (face == 'n')){

    j = ny-1;

    vel_avg = bc_flow_north->v_value[1];
    vel_max = 0;
    pos_offset = nodes_x_v[j*Nx+l_];

    /* Symmetry at east boundary */
    if((u_ == Nx-1) && (bc_flow_east->type == 2)){
      B = 2 * (nodes_x_v[j*Nx+u_] - pos_offset);
    }
    /* Symmetry at west boundary */
    else if((l_ == 0) && (bc_flow_west->type == 2)){
      B = 2 * (nodes_x_v[j*Nx+u_] - pos_offset);
      pos_offset -= 0.5 * B;
    }
    /* Default */
    else{
      B = nodes_x_v[j*Nx+u_] - pos_offset;
    }

    for(I=l_; I<u_+1; I++){

      index_v = j*Nx+I;

      x = nodes_x_v[index_v] - pos_offset;

      v_N_non_uni[I] = vel_avg * (6/B) * x * (1 - (x/B));
      vel_max = MAX(vel_max, fabs(v_N_non_uni[I]));
    }

    for(I=l_; I<u_+1; I++){
      k_N_non_uni[I] = bc_flow_north->k_value * fabs(v_N_non_uni[I] / vel_max);
      eps_N_non_uni[I] = bc_flow_north->eps_value * fabs(v_N_non_uni[I] / vel_max);
    }    
  }
  
  if((face == 'S') || (face == 's')){

    j = 0;

    vel_avg = bc_flow_south->v_value[1];
    vel_max = 0;
    pos_offset = nodes_x_v[j*Nx+l_];

    /* Symmetry at east boundary */
    if((u_ == Nx-1) && (bc_flow_east->type == 2)){
      B = 2 * (nodes_x_v[j*Nx+u_] - pos_offset);
    }
    /* Symmetry at west boundary */
    else if((l_ == 0) && (bc_flow_west->type == 2)){
      B = 2 * (nodes_x_v[j*Nx+u_] - pos_offset);
      pos_offset -= 0.5 * B;
    }
    /* Default */
    else{
      B = nodes_x_v[j*Nx+u_] - pos_offset;
    }

    for(I=l_; I<u_+1; I++){

      index_v = j*Nx+I;

      x = nodes_x_v[index_v] - pos_offset;

      v_S_non_uni[I] = vel_avg * (6/B) * x * (1 - (x/B));
      vel_max = MAX(vel_max, fabs(v_S_non_uni[I]));
    }

    for(I=l_; I<u_+1; I++){
      k_S_non_uni[I] = bc_flow_south->k_value * fabs(v_S_non_uni[I] / vel_max);
      eps_S_non_uni[I] = bc_flow_south->eps_value * fabs(v_S_non_uni[I] / vel_max);
    }    
  }

  return;
}

/* DIRICHLET_BOUNDARY_INDEXS_FINDER */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Dirichlet_boundary_cell_count) Dirichlet_boundary_indexs: Cells 
  that are solved (based on P_solve_phase_OK) and  have a non-solved neighbour, 
  this neighbour pertaining to a domain defined by a curve with a Dirichlet 
  boundary condition.

  @param data
  
  @return void
  
  MODIFIED: 15-07-2019
*/
void Dirichlet_boundary_indexs_finder(Data_Mem *data){

  int I, J, index_;
  int Dirichlet_boundary_cell_count = 0;
  int cell_OK = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *dom_mat = data->domain_matrix;
  const int *Isoflux_OK = data->Isoflux_OK;
  
  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  
#pragma omp parallel for default(none) private(I, J, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, P_solve_phase_OK, Isoflux_OK, dom_mat) \
  reduction(+:Dirichlet_boundary_cell_count)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
        
      index_ = J*Nx + I;

      cell_OK = 
	(!E_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+1] - 1 ]) ||
	(!W_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-1] - 1 ]) ||
	(!N_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+Nx] - 1 ]) ||
	(!S_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-Nx] - 1 ]);

      if(P_solve_phase_OK[index_] && cell_OK){
	Dirichlet_boundary_cell_count++;
      }
    }
  }
  
  data->Dirichlet_boundary_indexs = 
    create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->I_Dirichlet_v = create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->J_Dirichlet_v = create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->K_Dirichlet_v = create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->Dirichlet_boundary_cell_count = Dirichlet_boundary_cell_count;

  int *Dirichlet_boundary_indexs = data->Dirichlet_boundary_indexs;
  int *I_Dirichlet_v = data->I_Dirichlet_v;
  int *J_Dirichlet_v = data->J_Dirichlet_v;
  int *K_Dirichlet_v = data->K_Dirichlet_v;
  
  
  Dirichlet_boundary_cell_count = 0;

#pragma omp parallel for default(none) private(I, J, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, P_solve_phase_OK, Isoflux_OK, dom_mat, \
	 Dirichlet_boundary_indexs, I_Dirichlet_v, J_Dirichlet_v, K_Dirichlet_v, Dirichlet_boundary_cell_count) 

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
        
      index_ = J*Nx + I;

      cell_OK = 
	(!E_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+1] - 1 ]) ||
	(!W_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-1] - 1 ]) ||
	(!N_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+Nx] - 1 ]) ||
	(!S_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-Nx] - 1 ]);

      if(P_solve_phase_OK[index_] && cell_OK){
#pragma omp critical
	{
	  Dirichlet_boundary_indexs[Dirichlet_boundary_cell_count] = index_;
	  I_Dirichlet_v[Dirichlet_boundary_cell_count] = I;
	  J_Dirichlet_v[Dirichlet_boundary_cell_count] = J;
	  K_Dirichlet_v[Dirichlet_boundary_cell_count] = 0;
	  Dirichlet_boundary_cell_count++;
	}
      }
      
    }
  }
  
  return;
}

/* ISOFLUX_BOUNDARY_INDEXS_FINDER */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Isoflux_boundary_cell_count) Dirichlet_boundary_indexs: Cells 
  that are solved(based on P_solve_phase_OK) and have a non-solved neighbour, 
  this neighbour pertaining to a domain defined by a curve with an Isoflux
  boundary condition.

  @param data
  
  @return void
  
  MODIFIED: 16-07-2019
*/
void Isoflux_boundary_indexs_finder(Data_Mem *data){

  int I, J, index_;
  int cell_OK = 0;
  int Isoflux_boundary_cell_count = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int *dom_mat = data->domain_matrix;
  const int *Isoflux_OK = data->Isoflux_OK;

  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  
#pragma omp parallel for default(none) private(I, J, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, P_solve_phase_OK, Isoflux_OK, dom_mat) \
  reduction(+:Isoflux_boundary_cell_count)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
	  
      cell_OK = 
	(!E_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+1] -1 ]) ||
	(!W_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-1] -1 ]) ||
	(!N_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+Nx] -1 ]) ||
	(!S_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-Nx] -1 ]);

      if(P_solve_phase_OK[index_] && cell_OK){
	Isoflux_boundary_cell_count++;
      }
    }
  }
   
  data->Isoflux_boundary_indexs = 
    create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->I_Isoflux_v = create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->J_Isoflux_v = create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->K_Isoflux_v = create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->Isoflux_boundary_cell_count = Isoflux_boundary_cell_count;
    
  int *Isoflux_boundary_indexs = data->Isoflux_boundary_indexs;
  int *I_Isoflux_v = data->I_Isoflux_v;
  int *J_Isoflux_v = data->J_Isoflux_v;
  int *K_Isoflux_v = data->K_Isoflux_v;

  Isoflux_boundary_cell_count = 0;
    
#pragma omp parallel for default(none) private(I, J, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, P_solve_phase_OK, Isoflux_OK, dom_mat, \
	 Isoflux_boundary_cell_count, Isoflux_boundary_indexs, I_Isoflux_v, J_Isoflux_v, K_Isoflux_v)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
          
      cell_OK = 
	(!E_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+1] -1 ]) ||
	(!W_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-1] -1 ]) ||
	(!N_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+Nx] -1 ]) ||
	(!S_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-Nx] -1 ]);

      if(P_solve_phase_OK[index_] && cell_OK){
#pragma omp critical
	{
	  Isoflux_boundary_indexs[Isoflux_boundary_cell_count] = index_;
	  I_Isoflux_v[Isoflux_boundary_cell_count] = I;
	  J_Isoflux_v[Isoflux_boundary_cell_count] = J;
	  K_Isoflux_v[Isoflux_boundary_cell_count] = 0;
	  Isoflux_boundary_cell_count++;
	}
      }
    }
  }
 
  return;
}

/* CONJUGATE_BOUNDARY_INDEXS_FINDER */
/*****************************************************************************/
/**

  Sets:

  Array (int, Conjugate_boundary_cell_count) Conjugate_boundary_indexs: Cells 
  that are solved (based on P_solve_phase_OK) that have a neighbour that is 
  also solved, if domain index of neighbour cell is different to domain
  index of P cell.
  
  @param data

  @return void
  
  MODIFIED: 15-07-2019
*/
void Conjugate_boundary_indexs_finder(Data_Mem *data){

  int I, J, index_;
  int valid_cell_OK = 0;
  int Conjugate_boundary_cell_count = 0;
  
  int current_domain;
  int E_domain, W_domain, N_domain, S_domain;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int *domain_matrix = data->domain_matrix;
  
  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  
#pragma omp parallel for default(none)				\
  private(I, J, index_, current_domain,				\
	  E_domain, W_domain, N_domain, S_domain,		\
	  valid_cell_OK)					\
  shared(domain_matrix, E_solve_phase_OK, W_solve_phase_OK,	\
	 N_solve_phase_OK, S_solve_phase_OK, P_solve_phase_OK)	\
  reduction(+:Conjugate_boundary_cell_count)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
      
      current_domain = domain_matrix[index_];
      
      E_domain = domain_matrix[index_+1];
      W_domain = domain_matrix[index_-1];
      N_domain = domain_matrix[index_+Nx];
      S_domain = domain_matrix[index_-Nx];
	  
      valid_cell_OK = 
	(E_solve_phase_OK[index_] && (current_domain != E_domain)) || 
	(W_solve_phase_OK[index_] && (current_domain != W_domain)) || 
	(N_solve_phase_OK[index_] && (current_domain != N_domain)) || 
	(S_solve_phase_OK[index_] && (current_domain != S_domain));
    
      if(P_solve_phase_OK[index_] && valid_cell_OK){
	Conjugate_boundary_cell_count++;
      }
    }
  }
    
  data->Conjugate_boundary_indexs = 
    create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->I_Conjugate_v = create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->J_Conjugate_v = create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->K_Conjugate_v = create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->Conjugate_boundary_cell_count = Conjugate_boundary_cell_count;  

  int *Conjugate_boundary_indexs = data->Conjugate_boundary_indexs;
  int *I_Conjugate_v = data->I_Conjugate_v;
  int *J_Conjugate_v = data->J_Conjugate_v;
  int *K_Conjugate_v = data->K_Conjugate_v;
  
  Conjugate_boundary_cell_count = 0;
    
#pragma omp parallel for default(none) private(I, J, index_, current_domain, E_domain, \
					       W_domain, N_domain, S_domain, valid_cell_OK) \
  shared(domain_matrix, E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, \
	 P_solve_phase_OK, Conjugate_boundary_indexs, I_Conjugate_v, J_Conjugate_v,  K_Conjugate_v, \
	 Conjugate_boundary_cell_count) 

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
          
      current_domain = domain_matrix[index_];
      
      E_domain = domain_matrix[index_+1];
      W_domain = domain_matrix[index_-1];
      N_domain = domain_matrix[index_+Nx];
      S_domain = domain_matrix[index_-Nx];
	  
      valid_cell_OK = 
	(E_solve_phase_OK[index_] && (current_domain != E_domain)) || 
	(W_solve_phase_OK[index_] && (current_domain != W_domain)) || 
	(N_solve_phase_OK[index_] && (current_domain != N_domain)) || 
	(S_solve_phase_OK[index_] && (current_domain != S_domain));
    
      if(P_solve_phase_OK[index_] && valid_cell_OK){
#pragma omp critical
	{
	  Conjugate_boundary_indexs[Conjugate_boundary_cell_count] = index_;
	  I_Conjugate_v[Conjugate_boundary_cell_count] = I;
	  J_Conjugate_v[Conjugate_boundary_cell_count] = J;
	  K_Conjugate_v[Conjugate_boundary_cell_count] = 0;
	  Conjugate_boundary_cell_count++;
	}
      }
    }
  }

  return;
}

/* INITIALIZE_PHI_IN_DOMAIN */
/*****************************************************************************/
/**
   Set Data_Mem::phi values to the corresponding value on Data_Mem::phi0_v vector.

   @param data

   @return void
 */
void initialize_phi_in_domain(Data_Mem *data){
	
  int I, J, K;
  int index_;
	
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int solve_3D_OK = data->solve_3D_OK;
  
  const int *dom_matrix = data->domain_matrix;

  double *phi = data->phi;
  
  const double *phi0_v = data->phi0_v;

  if(solve_3D_OK){  
  
#pragma omp parallel for default(none)		\
  private(I, J, K, index_)			\
  shared(phi, phi0_v, dom_matrix)
    
    for(K=1; K<(Nz-1); K++){
      for(J=1; J<(Ny-1); J++){
	for(I=1; I<(Nx-1); I++){
      
	  index_ = K*Nx*Ny + J*Nx + I;

	  phi[index_] = phi0_v[ dom_matrix[index_] ];
      
	}
      }
    }
  }
  else{

#pragma omp parallel for default(none)		\
  private(I, J, index_)				\
  shared(phi, phi0_v, dom_matrix)

    for(J=1; J<(Ny-1); J++){
      for(I=1; I<(Nx-1); I++){
      
	index_ = J*Nx + I;

	phi[index_] = phi0_v[ dom_matrix[index_] ];
      
      }
    }

  }
  
  return;	
}

/* RELAX_COEFFS */
/*****************************************************************************/
/**
   Set b coeff to (b + ((1 - alpha) * aP / alpha) * field)

   Set aP coeff to aP / alpha

   @param *field: pointer to field.

   @param *sel_mat: pointer to array used to select which coeffs are relaxed.

   @param coeffs_: coeffs structure.

   @param Nx, Ny: array size.

   @param sel_val: coeffs relaxed if sel_mat(index_) == sel_val.

   @param alpha: relaxation factor.
   
   @return void

   MODIFIED: 17-07-2020
 */
void relax_coeffs(const double *field, const int *sel_mat, const int sel_val, 
		  Coeffs coeffs_, const int Nx, const int Ny, const double alpha){
  
  int I, J, index_;

  double *aP = coeffs_.aP;
  double *b  = coeffs_.b;

#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(sel_mat, b, aP, field)
  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
      if(sel_mat[index_] == sel_val){
	b[index_] = b[index_] + ((1 - alpha) * aP[index_] / alpha) * field[index_];
	aP[index_] = aP[index_] / alpha;
      }
    }
  }

  return;
}

/* COMPUTE_RESIDUES */
/*****************************************************************************/
/**
   
   Compute maximum value of residual abs(b + aE*phi_N + aW*phi_W + aN*phi_N
   aS*phi_S - aP*phi).
   
   @return norm_phi

   MODIFIED: 17-07-2020
 */
double compute_residues(double *phi, Coeffs *coeffs, 
			const int Nx, const int Ny){

  int I, J, index_;
  double norm_phi = 0;
  double r_ABS;
	
  double *aP = coeffs->aP;
  double *aE = coeffs->aE;
  double *aW = coeffs->aW;
  double *aN = coeffs->aN;
  double *aS = coeffs->aS;
  double *b = coeffs->b;
  
#pragma omp parallel for default(none)		\
  private(I, J, index_, r_ABS)			\
  shared(b, aE, aW, aN, aS, aP, phi)		\
  reduction(max: norm_phi)

  for(J=1; J<(Ny-1); J++){
    for(I=1; I<(Nx-1); I++){
      index_ = J*Nx + I;

      r_ABS = fabs(b[index_] + 
		   aE[index_]*phi[index_+1] + aW[index_]*phi[index_-1] + 
		   aN[index_]*phi[index_+Nx] + aS[index_]*phi[index_-Nx] - 
		   aP[index_]*phi[index_]);
      
      if (r_ABS > norm_phi){
	norm_phi = r_ABS;
      }
    }
  }
	
  return norm_phi;	
}

/* SCALE_VELOCITY_SELF */
/*****************************************************************************/
/**
  
  Gets the absolute maximum value of the velocity fields and scale the 
  velocities with it.

  @return void
    
  MODIFIED: 11-08-2019
*/
void scale_velocity_self(double *U, double *V, double *W,
			 const int Nx, const int Ny, const int Nz){

  int I, J, K, index_;

  double min = 0, max = 0;
  
  min = get_min(U, Nx, Ny, Nz);
  min = MIN( min, get_min(V, Nx, Ny, Nz) );
  if(Nz > 1){ min = MIN( min, get_min(W, Nx, Ny, Nz) ); }
	
  max = get_max(U, Nx, Ny, Nz);
  max = MAX( max, get_max(V, Nx, Ny, Nz) );
  if(Nz > 1){ max = MAX( max, get_max(W, Nx, Ny, Nz) ); }
	
  max = MAX( max, abs(min) );

  if(max != 0){	

#pragma omp parallel for default(none)		\
  private(I, J, K, index_) shared(U, V, W, max)
    for(K=0; K<Nz; K++){
      for(J=0; J<Ny; J++){
	for(I=0; I<Nx; I++){
	  
	  index_ = K*Ny*Nx + J*Nx + I;
	  
	  U[index_] = U[index_] / max;
	  V[index_] = V[index_] / max;
	  if(Nz > 1){ W[index_] = W[index_] / max ; }
	  
	}		
      }
    }
  }
  
  return;	
}

/* MAKE_P_INIT_PROFILE */
int make_p_init_profile(Data_Mem *data){
  
  int ret_value = 0;
  int I, J, index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int p_gradient_direction = data->p_gradient_direction;

  double lmin, lmax;

  double p_aux;
  double p0 = data->p0;
  double pL = data->pL;
  const double rise_to = data->rise_to;

  double *p = data->p;

  const double *x = data->nodes_x;
  const double *y = data->nodes_y;
  
  Wins *wins = &(data->wins);


  if((p_gradient_direction == 0) || (p_gradient_direction == 1)){

    lmin = y[1*Nx + 1];
    lmax = y[(Ny-2)*Nx + 1];

    if(p_gradient_direction == 1){
      p_aux = p0;
      p0 = pL;
      pL = p_aux;
    }

#pragma omp parallel for default(none)		\
  private(I, J, index_)				\
  shared(y, p, lmin, lmax, p0, pL)
  
    for(J=1; J<Ny-1; J++){
      for(I=0; I<Nx; I++){
      
	index_ = J*Nx + I;
      
	p[index_] = p0 + (pL - p0) * pow((lmin - y[index_]) / 
					 (lmin - lmax), rise_to);
      
      }
    }

    J = 0;
    
    for(I=1; I<Nx-1; I++){
      
      index_ = J*Nx + I;
      
      p[index_] = p0;
      
    }

    J = Ny-1;
  
    for(I=1; I<Nx-1; I++){
    
      index_ = J*Nx + I;
    
      p[index_] = pL;
    
    }

    if(p_gradient_direction == 0){
      info_msg(wins, "Pressure initialized for S ==> N flow");
    }
    else{
      info_msg(wins, "Pressure initialized for N ==> S flow");      
    }
  
  }
  
  if((p_gradient_direction == 2) || (p_gradient_direction == 3)){    
    
    lmin = x[1*Nx + 1];
    lmax = x[1*Nx + Nx-2];

    if(p_gradient_direction == 3){
      p_aux = p0;
      p0 = pL;
      pL = p_aux;
    }
    
#pragma omp parallel for default(none)		\
  private(I, J, index_)				\
  shared(x, p, lmin, lmax, p0, pL)
  
    for(J=0; J<Ny; J++){
      for(I=1; I<Nx-1; I++){
      
	index_ = J*Nx + I;
      
	p[index_] = p0 + (pL - p0) * pow((lmin - x[index_]) / 
					 (lmin - lmax), rise_to);
      
      }
    }

    I = 0;
    
    for(J=1; J<Ny-1; J++){
      
      index_ = J*Nx + I;
      
      p[index_] = p0;
      
    }
    
    I = Nx-1;
  
    for(J=1; J<Ny-1; J++){
    
      index_ = J*Nx + I;
    
      p[index_] = pL;
    
    }

    if(p_gradient_direction == 2){
      info_msg(wins, "Pressure initialized for W ==> E flow");
    }
    else{  
      info_msg(wins, "Pressure initialized for E ==> W flow");
    }
  
  }
  
  return ret_value;
}

/* GET_MEMORY */
void get_memory(Data_Mem *data){

  const char *res_filename = data->res_filename;
  const char *res_tr_filename = data->res_tr_filename;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int solve_3D_OK = data->solve_3D_OK;

  const int fss_OK = data->flow_steady_state_OK;
  const int ss_OK = data->steady_state_OK;
    
  const int wftd_OK = data->write_flow_transient_data_OK;
  const int wtd_OK = data->write_transient_data_OK;
    
  const double u0_v = data->u0_v;
  const double v0_v = data->v0_v;
  const double w0_v = data->w0_v;

  /* For initialization of phi arrays to the value of phi at fluid phase */
  const double phi0f = data->phi0_v[0];

  Wins *wins = &(data->wins);

  mat_t *matfp = NULL;

  /* Check for RESULTS file */
  matfp = get_matfp(res_filename, 1, wins);
  /* Ok that was it, we close it for now */
  /* It is OK since it is a v5 file */
  Mat_Close(matfp);

  /* This should be only called when want to save transient results */
  /* It is not a v5 type of file (v5_OK = 0), it is a v7.3 */
  if ((!fss_OK || !ss_OK) && (wftd_OK || wtd_OK)){
    data->matios.matfp_tr = get_matfp(res_tr_filename, 0, wins);
  }
  
  data->u_ps = create_array(nx, Ny, Nz, 0);
  data->v_ps = create_array(Nx, ny, Nz, 0);
  data->w_ps = create_array(Nx, Ny, nz, 0);

  data->c_x = create_array(nx, Ny, Nz, 0);
  data->c_y = create_array(Nx, ny, Nz, 0);
  
  data->vol = create_array(Nx, Ny, Nz, 0);
  data->vol_uncut = create_array(Nx, Ny, Nz, 0);
  data->vol_x = create_array(nx, Ny, Nz, 0);
  data->vol_y = create_array(Nx, ny, Nz, 0);
  data->vol_z = create_array(Nx, Ny, nz, 0);

  data->vf_x = create_array(nx, Ny, Nz, 0);
  data->vf_y = create_array(Nx, ny, Nz, 0);
  data->vf_z = create_array(Nx, Ny, nz, 0);

  data->nodes_x = create_array(Nx, Ny, Nz, 0);
  data->nodes_y = create_array(Nx, Ny, Nz, 0);
  data->nodes_z = create_array(Nx, Ny, Nz, 0);
  data->Delta_xe = create_array(Nx, Ny, Nz, 0);
  data->Delta_xE = create_array(Nx, Ny, Nz, 0);
  data->Delta_xw = create_array(Nx, Ny, Nz, 0);
  data->Delta_xW = create_array(Nx, Ny, Nz, 0);
  data->Delta_yn = create_array(Nx, Ny, Nz, 0);
  data->Delta_yN = create_array(Nx, Ny, Nz, 0);
  data->Delta_ys = create_array(Nx, Ny, Nz, 0);
  data->Delta_yS = create_array(Nx, Ny, Nz, 0);
  data->Delta_zt = create_array(Nx, Ny, Nz, 0);
  data->Delta_zb = create_array(Nx, Ny, Nz, 0);
  data->Delta_zT = create_array(Nx, Ny, Nz, 0);
  data->Delta_zB = create_array(Nx, Ny, Nz, 0);
  data->Delta_x = create_array(Nx, Ny, Nz, 0);
  data->Delta_y = create_array(Nx, Ny, Nz, 0);
  data->Delta_z = create_array(Nx, Ny, Nz, 0);
  data->vertex_x = create_array(nx, 1, 1, 0);
  data->vertex_y = create_array(ny, 1, 1, 0);
  data->vertex_z = create_array(nz, 1, 1, 0);

  data->fn = create_array(Nx, Ny, Nz, 0);
  data->fs = create_array(Nx, Ny, Nz, 0);
  data->fe = create_array(Nx, Ny, Nz, 0);
  data->fw = create_array(Nx, Ny, Nz, 0);
  data->ft = create_array(Nx, Ny, Nz, 0);
  data->fb = create_array(Nx, Ny, Nz, 0);
  
  /* U GRID */
  data->nodes_x_u = create_array(nx, Ny, Nz, 0);
  data->nodes_y_u = create_array(nx, Ny, Nz, 0);
  data->nodes_z_u = create_array(nx, Ny, Nz, 0);
  data->Delta_xe_u = create_array(nx, Ny, Nz, 0);
  data->Delta_xE_u = create_array(nx, Ny, Nz, 0);
  data->Delta_xw_u = create_array(nx, Ny, Nz, 0);
  data->Delta_xW_u = create_array(nx, Ny, Nz, 0);
  data->Delta_yn_u = create_array(nx, Ny, Nz, 0);
  data->Delta_yN_u = create_array(nx, Ny, Nz, 0);
  data->Delta_ys_u = create_array(nx, Ny, Nz, 0);
  data->Delta_yS_u = create_array(nx, Ny, Nz, 0);
  data->Delta_zt_u = create_array(nx, Ny, Nz, 0);
  data->Delta_zT_u = create_array(nx, Ny, Nz, 0);
  data->Delta_zb_u = create_array(nx, Ny, Nz, 0);
  data->Delta_zB_u = create_array(nx, Ny, Nz, 0);
  data->Delta_x_u = create_array(nx, Ny, Nz, 0);
  data->Delta_y_u = create_array(nx, Ny, Nz, 0);
  data->Delta_z_u = create_array(nx, Ny, Nz, 0);
  data->vertex_x_u = create_array(Nx, 1, 1, 0);
  
  /* V GRID */
  data->nodes_x_v = create_array(Nx, ny, Nz, 0);
  data->nodes_y_v = create_array(Nx, ny, Nz, 0);
  data->nodes_z_v = create_array(Nx, ny, Nz, 0);
  data->Delta_xe_v = create_array(Nx, ny, Nz, 0);
  data->Delta_xE_v = create_array(Nx, ny, Nz, 0);
  data->Delta_xw_v = create_array(Nx, ny, Nz, 0);
  data->Delta_xW_v = create_array(Nx, ny, Nz, 0);
  data->Delta_yn_v = create_array(Nx, ny, Nz, 0);
  data->Delta_yN_v = create_array(Nx, ny, Nz, 0);
  data->Delta_ys_v = create_array(Nx, ny, Nz, 0);
  data->Delta_yS_v = create_array(Nx, ny, Nz, 0);
  data->Delta_zt_v = create_array(Nx, ny, Nz, 0);
  data->Delta_zT_v = create_array(Nx, ny, Nz, 0);
  data->Delta_zb_v = create_array(Nx, ny, Nz, 0);
  data->Delta_zB_v = create_array(Nx, ny, Nz, 0);
  data->Delta_x_v = create_array(Nx, ny, Nz, 0);
  data->Delta_y_v = create_array(Nx, ny, Nz, 0);
  data->Delta_z_v = create_array(Nx, ny, Nz, 0);
  data->vertex_y_v = create_array(Ny, 1, 1, 0);

  /* W GRID */
  data->nodes_x_w = create_array(Nx, Ny, nz, 0);
  data->nodes_y_w = create_array(Nx, Ny, nz, 0);
  data->nodes_z_w = create_array(Nx, Ny, nz, 0);
  data->Delta_xe_w = create_array(Nx, Ny, nz, 0);
  data->Delta_xE_w = create_array(Nx, Ny, nz, 0);
  data->Delta_xw_w = create_array(Nx, Ny, nz, 0);
  data->Delta_xW_w = create_array(Nx, Ny, nz, 0);
  data->Delta_yn_w = create_array(Nx, Ny, nz, 0);
  data->Delta_yN_w = create_array(Nx, Ny, nz, 0);
  data->Delta_ys_w = create_array(Nx, Ny, nz, 0);
  data->Delta_yS_w = create_array(Nx, Ny, nz, 0);
  data->Delta_zt_w = create_array(Nx, Ny, nz, 0);
  data->Delta_zT_w = create_array(Nx, Ny, nz, 0);
  data->Delta_zb_w = create_array(Nx, Ny, nz, 0);
  data->Delta_zB_w = create_array(Nx, Ny, nz, 0);
  data->Delta_x_w = create_array(Nx, Ny, nz, 0);
  data->Delta_y_w = create_array(Nx, Ny, nz, 0);
  data->Delta_z_w = create_array(Nx, Ny, nz, 0);
  data->vertex_z_w = create_array(Nz, 1, 1, 0);

  data->norm_vec_x_x = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_x_y = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_x_z = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_y_x = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_y_y = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_y_z = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_z_x = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_z_y = create_array(Nx, Ny, Nz, 0);
  data->norm_vec_z_z = create_array(Nx, Ny, Nz, 0);
  data->par_vec_x_x = create_array(Nx, Ny, Nz, 0);
  data->par_vec_x_y = create_array(Nx, Ny, Nz, 0);
  data->par_vec_x_z = create_array(Nx, Ny, Nz, 0);
  data->par_vec_y_x = create_array(Nx, Ny, Nz, 0);
  data->par_vec_y_y = create_array(Nx, Ny, Nz, 0);
  data->par_vec_y_z = create_array(Nx, Ny, Nz, 0);
  data->par_vec_z_x = create_array(Nx, Ny, Nz, 0);
  data->par_vec_z_y = create_array(Nx, Ny, Nz, 0);
  data->par_vec_z_z = create_array(Nx, Ny, Nz, 0);

  if(solve_3D_OK){
    data->par_vec_x_x_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_x_y_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_x_z_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_y_x_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_y_y_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_y_z_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_z_x_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_z_y_2 = create_array(Nx, Ny, Nz, 0);
    data->par_vec_z_z_2 = create_array(Nx, Ny, Nz, 0);    
  }
  
  data->norm_x = create_array(Nx, Ny, Nz, 0);
  data->norm_y = create_array(Nx, Ny, Nz, 0);
  data->norm_z = create_array(Nx, Ny, Nz, 0);
  data->par_x = create_array(Nx, Ny, Nz, 0);
  data->par_y = create_array(Nx, Ny, Nz, 0);
  data->par_z = create_array(Nx, Ny, Nz, 0);

  if(solve_3D_OK){
    data->par_x_2 = create_array(Nx, Ny, Nz, 0);
    data->par_y_2 = create_array(Nx, Ny, Nz, 0);
    data->par_z_2 = create_array(Nx, Ny, Nz, 0);    
  }

  data->epsilon_x = create_array(Nx, Ny, Nz, 0);
  data->epsilon_y = create_array(Nx, Ny, Nz, 0);
  data->epsilon_z = create_array(Nx, Ny, Nz, 0);
  data->epsilon_x_sol = create_array(Nx, Ny, Nz, 0);
  data->epsilon_y_sol = create_array(Nx, Ny, Nz, 0);
  data->epsilon_z_sol = create_array(Nx, Ny, Nz, 0);
  data->v_fraction = create_array(Nx, Ny, Nz, 0);
  data->v_fraction_x = create_array(Nx, Ny, Nz, 0);
  data->v_fraction_y = create_array(Nx, Ny, Nz, 0);
  data->v_fraction_z = create_array(Nx, Ny, Nz, 0);
  data->cut_matrix = create_array_int(Nx, Ny, Nz, 0);
  data->domain_matrix = create_array_int(Nx, Ny, Nz, 0);
  
  data->P_solve_phase_OK = create_array_int(Nx, Ny, Nz, 0);
  data->E_solve_phase_OK = create_array_int(Nx, Ny, Nz, 0);
  data->W_solve_phase_OK = create_array_int(Nx, Ny, Nz, 0);
  data->N_solve_phase_OK = create_array_int(Nx, Ny, Nz, 0);
  data->S_solve_phase_OK = create_array_int(Nx, Ny, Nz, 0);
  data->T_solve_phase_OK = create_array_int(Nx, Ny, Nz, 0);
  data->B_solve_phase_OK = create_array_int(Nx, Ny, Nz, 0);

  data->E_is_fluid_OK = create_array_int(Nx, Ny, Nz, 1);
  data->W_is_fluid_OK = create_array_int(Nx, Ny, Nz, 1);
  data->N_is_fluid_OK = create_array_int(Nx, Ny, Nz, 1);
  data->S_is_fluid_OK = create_array_int(Nx, Ny, Nz, 1);
  data->T_is_fluid_OK = create_array_int(Nx, Ny, Nz, 1);
  data->B_is_fluid_OK = create_array_int(Nx, Ny, Nz, 1);

  data->case_s_type_x = create_array_char(Nx, Ny, Nz, '-');
  data->case_s_type_y = create_array_char(Nx, Ny, Nz, '-');
  data->case_s_type_z = create_array_char(Nx, Ny, Nz, '-');
      
  data->matrix_c = create_array(3, 3, 1, 0);
  data->R_x = create_array(3, 3, 1, 0);
  data->R_y = create_array(3, 3, 1, 0);
  data->R_z = create_array(3, 3, 1, 0);
  data->Rot_ = create_array(3, 3, 1, 0);
  data->Trans_ = create_array(1, 3, 1, 0);
  data->Trans_T = create_array(3, 1, 1, 0);
  data->Rot_T = create_array(3, 3, 1, 0);
  data->matrix_A = create_array(3, 3, 1, 0);
  data->vector_a = create_array(1, 3, 1, 0);

  data->dphidxb = create_array(Nx, Ny, Nz, 0);
  data->dphidyb = create_array(Nx, Ny, Nz, 0);
  data->dphidzb = create_array(Nx, Ny, Nz, 0);

  data->dphidxs = create_array(Nx, Ny, Nz, 0);
  data->dphidys = create_array(Nx, Ny, Nz, 0);
  data->dphidzs = create_array(Nx, Ny, Nz, 0);

  data->J_x = create_array(Nx, Ny, Nz, 0);
  data->J_y = create_array(Nx, Ny, Nz, 0);
  data->J_z = create_array(Nx, Ny, Nz, 0);
  
  data->J_norm = create_array(Nx, Ny, Nz, 0);
  data->J_par = create_array(Nx, Ny, Nz, 0);
  data->J_par_2 = create_array(Nx, Ny, Nz, 0);
  
  data->phi = create_array(Nx, Ny, Nz, phi0f);
  set_nans_corners(data->phi, Nx, Ny, Nz, solve_3D_OK);
  data->phi0 = create_array(Nx, Ny, Nz, phi0f);
  
  data->u = create_array(nx, Ny, Nz, u0_v);
  data->u0 = create_array(nx, Ny, Nz, u0_v);
  
  data->v = create_array(Nx, ny, Nz, v0_v);
  data->v0 = create_array(Nx, ny, Nz, v0_v);

  data->w = create_array(Nx, Ny, nz, w0_v);
  data->w0 = create_array(Nx, Ny, nz, w0_v);

  data->u_at_faces = create_array(nx, Ny, Nz, 0);
  data->v_at_faces = create_array(Nx, ny, Nz, 0);
  data->w_at_faces = create_array(Nx, Ny, nz, 0);

  data->u_at_faces_y = create_array(Nx, ny, Nz, 0);
  data->u_at_faces_z = create_array(Nx, Ny, nz, 0);
  
  data->v_at_faces_x = create_array(nx, Ny, Nz, 0);
  data->v_at_faces_z = create_array(Nx, Ny, nz, 0);

  data->w_at_faces_x = create_array(nx, Ny, Nz, 0);
  data->w_at_faces_y = create_array(Nx, ny, Nz, 0);

  data->u_at_corners = create_array(nx, ny, nz, 0);
  data->v_at_corners = create_array(nx, ny, nz, 0);
  data->w_at_corners = create_array(nx, ny, nz, 0);
  
  data->U = create_array(Nx, Ny, Nz, u0_v);
  data->V = create_array(Nx, Ny, Nz, v0_v);
  data->W = create_array(Nx, Ny, Nz, w0_v);

  data->p = create_array(Nx, Ny, Nz, 0);
  set_nans_corners(data->p, Nx, Ny, Nz, solve_3D_OK);
  
  data->p_corr = create_array(Nx, Ny, Nz, 0);
  set_nans_corners(data->p_corr, Nx, Ny, Nz, solve_3D_OK);
  
  data->ADI_vars.a_T = create_array(Nx, Ny, Nz, 0);
  data->ADI_vars.b_T = create_array(Nx, Ny, Nz, 0);
  data->ADI_vars.c_T = create_array(Nx, Ny, Nz, 0);
  data->ADI_vars.d_T = create_array(Nx, Ny, Nz, 0);
  data->ADI_vars.c_p = create_array(Nx, Ny, Nz, 0);
  data->ADI_vars.d_p = create_array(Nx, Ny, Nz, 0);
  
  data->coeffs_phi.aP = create_array(Nx, Ny, Nz, 1);
  data->coeffs_phi.aE = create_array(Nx, Ny, Nz, 0);
  data->coeffs_phi.aW = create_array(Nx, Ny, Nz, 0);
  data->coeffs_phi.aN = create_array(Nx, Ny, Nz, 0);
  data->coeffs_phi.aS = create_array(Nx, Ny, Nz, 0);
  data->coeffs_phi.aT = create_array(Nx, Ny, Nz, 0);
  data->coeffs_phi.aB = create_array(Nx, Ny, Nz, 0);
  data->coeffs_phi.b = create_array(Nx, Ny, Nz, 0);

  data->coeffs_u.aP = create_array(nx, Ny, Nz, 1);
  data->coeffs_u.aE = create_array(nx, Ny, Nz, 0);
  data->coeffs_u.aW = create_array(nx, Ny, Nz, 0);
  data->coeffs_u.aN = create_array(nx, Ny, Nz, 0);
  data->coeffs_u.aS = create_array(nx, Ny, Nz, 0);
  data->coeffs_u.aT = create_array(nx, Ny, Nz, 0);
  data->coeffs_u.aB = create_array(nx, Ny, Nz, 0);
  data->coeffs_u.b = create_array(nx, Ny, Nz, 0);

  data->coeffs_v.aP = create_array(Nx, ny, Nz, 1);
  data->coeffs_v.aE = create_array(Nx, ny, Nz, 0);
  data->coeffs_v.aW = create_array(Nx, ny, Nz, 0);
  data->coeffs_v.aN = create_array(Nx, ny, Nz, 0);
  data->coeffs_v.aS = create_array(Nx, ny, Nz, 0);
  data->coeffs_v.aT = create_array(Nx, ny, Nz, 0);
  data->coeffs_v.aB = create_array(Nx, ny, Nz, 0);
  data->coeffs_v.b = create_array(Nx, ny, Nz, 0);

  data->coeffs_w.aP = create_array(Nx, Ny, nz, 1);
  data->coeffs_w.aE = create_array(Nx, Ny, nz, 0);
  data->coeffs_w.aW = create_array(Nx, Ny, nz, 0);
  data->coeffs_w.aN = create_array(Nx, Ny, nz, 0);
  data->coeffs_w.aS = create_array(Nx, Ny, nz, 0);
  data->coeffs_w.aT = create_array(Nx, Ny, nz, 0);
  data->coeffs_w.aB = create_array(Nx, Ny, nz, 0);
  data->coeffs_w.b = create_array(Nx, Ny, nz, 0);
  
  data->coeffs_p.aP = create_array(Nx, Ny, Nz, 0);
  data->coeffs_p.aE = create_array(Nx, Ny, Nz, 0);
  data->coeffs_p.aW = create_array(Nx, Ny, Nz, 0);
  data->coeffs_p.aN = create_array(Nx, Ny, Nz, 0);
  data->coeffs_p.aS = create_array(Nx, Ny, Nz, 0);
  data->coeffs_p.aT = create_array(Nx, Ny, Nz, 0);
  data->coeffs_p.aB = create_array(Nx, Ny, Nz, 0);
  data->coeffs_p.b = create_array(Nx, Ny, Nz, 0);

  data->coeffs_k.aP = create_array(Nx, Ny, Nz, 0);
  data->coeffs_k.aE = create_array(Nx, Ny, Nz, 0);
  data->coeffs_k.aW = create_array(Nx, Ny, Nz, 0);
  data->coeffs_k.aN = create_array(Nx, Ny, Nz, 0);
  data->coeffs_k.aS = create_array(Nx, Ny, Nz, 0);
  data->coeffs_k.aT = create_array(Nx, Ny, Nz, 0);
  data->coeffs_k.aB = create_array(Nx, Ny, Nz, 0);
  data->coeffs_k.b = create_array(Nx, Ny, Nz, 0);
  
  data->coeffs_eps.aP = create_array(Nx, Ny, Nz, 0);
  data->coeffs_eps.aE = create_array(Nx, Ny, Nz, 0);
  data->coeffs_eps.aW = create_array(Nx, Ny, Nz, 0);
  data->coeffs_eps.aN = create_array(Nx, Ny, Nz, 0);
  data->coeffs_eps.aS = create_array(Nx, Ny, Nz, 0);
  data->coeffs_eps.aT = create_array(Nx, Ny, Nz, 0);
  data->coeffs_eps.aB = create_array(Nx, Ny, Nz, 0);
  data->coeffs_eps.b = create_array(Nx, Ny, Nz, 0);

  data->rho_m = create_array(Nx, Ny, Nz, 1);
  data->cp_m = create_array(Nx, Ny, Nz, 1);
  data->gamma_m = create_array(Nx, Ny, Nz, 1);
  data->SC_vol_m_0 = create_array(Nx, Ny, Nz, 0);
  data->SC_vol_m = create_array(Nx, Ny, Nz, 0);
  
  data->faces_x = create_array(nx, Ny, Nz, 0);
  data->faces_y = create_array(Nx, ny, Nz, 0);
  data->faces_z = create_array(Nx, Ny, nz, 0);
  
  data->p_faces_x = create_array(nx, Ny, Nz, 0);
  data->p_faces_y = create_array(Nx, ny, Nz, 0);
  data->p_faces_z = create_array(Nx, Ny, nz, 0);
  
  data->u_faces_x = create_array(Nx, Ny, Nz, 0);
  data->u_faces_y = create_array(nx, ny, Nz, 0);
  data->u_faces_z = create_array(nx, Ny, nz, 0);
  data->v_faces_x = create_array(nx, ny, Nz, 0);
  data->v_faces_y = create_array(Nx, Ny, Nz, 0);
  data->v_faces_z = create_array(Nx, ny, nz, 0);
  data->w_faces_x = create_array(nx, Ny, nz, 0);
  data->w_faces_y = create_array(Nx, ny, nz, 0);
  data->w_faces_z = create_array(Nx, Ny, Nz, 0);

  data->p_cut_face_x = create_array_int(nx, Ny, Nz, 0);
  data->p_cut_face_y = create_array_int(Nx, ny, Nz, 0);
  data->p_cut_face_z = create_array_int(Nx, Ny, nz, 0);
  data->u_cut_face_x = create_array_int(Nx, Ny, Nz, 0);
  data->u_cut_face_y = create_array_int(nx, ny, Nz, 0);
  data->u_cut_face_z = create_array_int(nx, Ny, nz, 0);
  data->v_cut_face_x = create_array_int(nx, ny, Nz, 0);
  data->v_cut_face_y = create_array_int(Nx, Ny, Nz, 0);
  data->v_cut_face_z = create_array_int(Nx, ny, nz, 0);
  data->w_cut_face_x = create_array_int(nx, Ny, nz, 0);
  data->w_cut_face_y = create_array_int(Nx, ny, nz, 0);
  data->w_cut_face_z = create_array_int(Nx, Ny, Nz, 0);

  data->alpha_u_x = create_array(Nx, Ny, Nz, 1);
  data->alpha_u_y = create_array(nx, ny, Nz, 1);
  data->alpha_u_z = create_array(nx, Ny, nz, 1);
  data->alpha_v_x = create_array(nx, ny, Nz, 1);
  data->alpha_v_y = create_array(Nx, Ny, Nz, 1);
  data->alpha_v_z = create_array(Nx, ny, nz, 1);
  data->alpha_w_x = create_array(nx, Ny, nz, 1);
  data->alpha_w_y = create_array(Nx, ny, nz, 1);  
  data->alpha_w_z = create_array(Nx, Ny, Nz, 1);

  data->Du_x = create_array(Nx, Ny, Nz, 0);
  data->Du_y = create_array(nx, ny, Nz, 0);
  data->Du_z = create_array(nx, Ny, nz, 0);
  data->Dv_x = create_array(nx, ny, Nz, 0);
  data->Dv_y = create_array(Nx, Ny, Nz, 0);
  data->Dv_z = create_array(Nx, ny, nz, 0);
  data->Dw_x = create_array(nx, Ny, nz, 0);
  data->Dw_y = create_array(Nx, ny, nz, 0);
  data->Dw_z = create_array(Nx, Ny, Nz, 0);
  data->Dk_x = create_array(nx, Ny, Nz, 0);
  data->Dk_y = create_array(Nx, ny, Nz, 0);
  data->Dk_z = create_array(Nx, Ny, nz, 0);
  data->Deps_x = create_array(nx, Ny, Nz, 0);
  data->Deps_y = create_array(Nx, ny, Nz, 0);
  data->Deps_z = create_array(Nx, Ny, nz, 0);
  data->Dphi_x = create_array(nx, Ny, Nz, 0);
  data->Dphi_y = create_array(Nx, ny, Nz, 0);
  data->Dphi_z = create_array(Nx, Ny, nz, 0);

  data->Fu_x = create_array(Nx, Ny, Nz, 0);
  data->Fu_y = create_array(nx, ny, Nz, 0);
  data->Fu_z = create_array(nx, Ny, nz, 0);
  data->Fv_x = create_array(nx, ny, Nz, 0);
  data->Fv_y = create_array(Nx, Ny, Nz, 0);
  data->Fv_z = create_array(Nx, ny, nz, 0);
  data->Fw_x = create_array(nx, Ny, nz, 0);
  data->Fw_y = create_array(Nx, ny, nz, 0);
  data->Fw_z = create_array(Nx, Ny, Nz, 0);
  data->Fk_x = create_array(nx, Ny, Nz, 0);
  data->Fk_y = create_array(Nx, ny, Nz, 0);
  data->Fk_z = create_array(Nx, Ny, nz, 0);
  data->Feps_x = create_array(nx, Ny, Nz, 0);
  data->Feps_y = create_array(Nx, ny, Nz, 0);
  data->Feps_z = create_array(Nx, Ny, nz, 0);
  data->Fphi_x = create_array(nx, Ny, Nz, 0);
  data->Fphi_y = create_array(Nx, ny, Nz, 0);
  data->Fphi_z = create_array(Nx, Ny, nz, 0);

  data->u_aP_SIMPLEC = create_array(nx, Ny, Nz, 1);
  data->v_aP_SIMPLEC = create_array(Nx, ny, Nz, 1);
  data->w_aP_SIMPLEC = create_array(Nx, Ny, nz, 1);

  data->u_dom_matrix = create_array_int(nx, Ny, Nz, 0);
  data->v_dom_matrix = create_array_int(Nx, ny, Nz, 0);
  data->w_dom_matrix = create_array_int(Nx, Ny, nz, 0);
  data->p_dom_matrix = create_array_int(Nx, Ny, Nz, 0);
  data->u_slave_nodes = NULL;
  data->v_slave_nodes = NULL;
  /*
    data->u_master_nodes = NULL;
    data->v_master_nodes = NULL;
  */

  data->e_J = create_array_int(Ny*Nz, 1, 1, 0);
  data->e_K = create_array_int(Ny*Nz, 1, 1, 0);
  data->w_J = create_array_int(Ny*Nz, 1, 1, 0);
  data->w_K = create_array_int(Ny*Nz, 1, 1, 0);
  data->n_I = create_array_int(Nx*Nz, 1, 1, 0);
  data->n_K = create_array_int(Nx*Nz, 1, 1, 0);
  data->s_I = create_array_int(Nx*Nz, 1, 1, 0);
  data->s_K = create_array_int(Nx*Nz, 1, 1, 0);
  data->b_I = create_array_int(Nx*Ny, 1, 1, 0);
  data->b_J = create_array_int(Nx*Ny, 1, 1, 0);
  data->t_I = create_array_int(Nx*Ny, 1, 1, 0);
  data->t_J = create_array_int(Nx*Ny, 1, 1, 0);

  data->u_cut_matrix = create_array_int(nx, Ny, Nz, 0);
  data->v_cut_matrix = create_array_int(Nx, ny, Nz, 0);
  data->w_cut_matrix = create_array_int(Nx, Ny, nz, 0);

  data->gL = create_array(Nx, Ny, Nz, 0);
  data->gL_guess = create_array(Nx, Ny, Nz, 0);
  data->gL_calc = create_array(Nx, Ny, Nz, 0);
  data->enthalpy_diff = create_array(Nx, Ny, Nz, 0);
  data->phase_change_vol = create_array(Nx, Ny, Nz, 0);
  data->y_wall = create_array(Nx, Ny, Nz, 0);
  
  data->nabla_u = create_array(3*Nx, 3*Ny, Nz, 0);
  data->def_tens = create_array(3*Nx, 3*Ny, Nz, 0);
  data->tau_turb = create_array(3*Nx, 3*Ny, Nz, 0);

  data->P_k = create_array(Nx, Ny, Nz, 0);
  data->P_k_lag = create_array(Nx, Ny, Nz, 0);
  data->mu_turb = create_array(Nx, Ny, Nz, 0);
  data->mu_turb_lag = create_array(Nx, Ny, Nz, 0);
  data->f_1 = create_array(Nx, Ny, Nz, 1);
  data->f_2 = create_array(Nx, Ny, Nz, 1);
  data->f_mu = create_array(Nx, Ny, Nz, 1);

  data->k_turb = create_array(Nx, Ny, Nz, 0);
  data->k_turb0 = create_array(Nx, Ny, Nz, 0);
  data->eps_turb = create_array(Nx, Ny, Nz, 0);
  data->eps_turb0 = create_array(Nx, Ny, Nz, 0);
  data->gamma = create_array(Nx, Ny, Nz, 0);

  data->S_CG_eps = create_array(Nx, Ny, Nz, 0);
  data->S_CG_eps_lag = create_array(Nx, Ny, Nz, 0);
  
  data->d2Uidxjdxk2 = create_array(Nx, Ny, Nz, 0);

  data->fe_u = create_array(nx, Ny, Nz, 0);
  data->fw_u = create_array(nx, Ny, Nz, 0);
  data->fn_u = create_array(nx, Ny, Nz, 0);
  data->fs_u = create_array(nx, Ny, Nz, 0);
  data->ft_u = create_array(nx, Ny, Nz, 0);
  data->fb_u = create_array(nx, Ny, Nz, 0);
  
  data->fe_v = create_array(Nx, ny, Nz, 0);
  data->fw_v = create_array(Nx, ny, Nz, 0);
  data->fn_v = create_array(Nx, ny, Nz, 0);
  data->fs_v = create_array(Nx, ny, Nz, 0);
  data->ft_v = create_array(Nx, ny, Nz, 0);
  data->fb_v = create_array(Nx, ny, Nz, 0);

  data->fe_w = create_array(Nx, Ny, nz, 0);
  data->fw_w = create_array(Nx, Ny, nz, 0);
  data->fn_w = create_array(Nx, Ny, nz, 0);
  data->fs_w = create_array(Nx, Ny, nz, 0);
  data->ft_w = create_array(Nx, Ny, nz, 0);
  data->fb_w = create_array(Nx, Ny, nz, 0);
  
  data->u_E_non_uni = create_array(1, Ny, Nz, 0);
  data->u_W_non_uni = create_array(1, Ny, Nz, 0);
  data->v_N_non_uni = create_array(1, Nx, Nz, 0);
  data->v_S_non_uni = create_array(1, Nx, Nz, 0);
  data->w_B_non_uni = create_array(Nx, Ny, 1, 0);
  data->w_T_non_uni = create_array(Nx, Ny, 1, 0);
  data->k_E_non_uni = create_array(1, Ny, Nz, 0);
  data->k_W_non_uni = create_array(1, Ny, Nz, 0);
  data->k_N_non_uni = create_array(1, Nx, Nz, 0);
  data->k_S_non_uni = create_array(1, Nx, Nz, 0);
  data->k_B_non_uni = create_array(Nx, Ny, 1, 0);
  data->k_T_non_uni = create_array(Nx, Ny, 1, 0);
  data->eps_E_non_uni = create_array(1, Ny, Nz, 0);
  data->eps_W_non_uni = create_array(1, Ny, Nz, 0);
  data->eps_N_non_uni = create_array(1, Nx, Nz, 0);
  data->eps_S_non_uni = create_array(1, Nx, Nz, 0);
  data->eps_B_non_uni = create_array(Nx, Ny, 1, 0);
  data->eps_T_non_uni = create_array(Nx, Ny, 1, 0);
  data->p_E_non_uni = create_array(1, Ny, Nz, 0);
  data->p_W_non_uni = create_array(1, Ny, Nz, 0);
  data->p_S_non_uni = create_array(Nx, 1, Nz, 0);
  data->p_N_non_uni = create_array(Nx, 1, Nz, 0);
  data->p_B_non_uni = create_array(Nx, Ny, 1, 0);
  data->p_T_non_uni = create_array(Nx, Ny, 1, 0);

  data->u_cut_fw = create_array(Nx, Ny, Nz, 0);
  data->u_cut_fe = create_array(Nx, Ny, Nz, 0);
  data->v_cut_fs = create_array(Nx, Ny, Nz, 0);
  data->v_cut_fn = create_array(Nx, Ny, Nz, 0);
  data->w_cut_fb = create_array(Nx, Ny, Nz, 0);
  data->w_cut_ft = create_array(Nx, Ny, Nz, 0);

  data->cdE = create_array(Nx, Ny, Nz, 0);
  data->cdW = create_array(Nx, Ny, Nz, 0);
  data->cdN = create_array(Nx, Ny, Nz, 0);
  data->cdS = create_array(Nx, Ny, Nz, 0);
  data->cdT = create_array(Nx, Ny, Nz, 0);
  data->cdB = create_array(Nx, Ny, Nz, 0);

  data->cudE = create_array(nx, Ny, Nz, 0);
  data->cudW = create_array(nx, Ny, Nz, 0);
  data->cudN = create_array(nx, Ny, Nz, 0);
  data->cudS = create_array(nx, Ny, Nz, 0);
  data->cudT = create_array(nx, Ny, Nz, 0);
  data->cudB = create_array(nx, Ny, Nz, 0);
  
  data->cvdE = create_array(Nx, ny, Nz, 0);
  data->cvdW = create_array(Nx, ny, Nz, 0);
  data->cvdN = create_array(Nx, ny, Nz, 0);
  data->cvdS = create_array(Nx, ny, Nz, 0);
  data->cvdT = create_array(Nx, ny, Nz, 0);
  data->cvdB = create_array(Nx, ny, Nz, 0);

  data->cwdE = create_array(Nx, Ny, nz, 0);
  data->cwdW = create_array(Nx, Ny, nz, 0);
  data->cwdN = create_array(Nx, Ny, nz, 0);
  data->cwdS = create_array(Nx, Ny, nz, 0);
  data->cwdT = create_array(Nx, Ny, nz, 0);
  data->cwdB = create_array(Nx, Ny, nz, 0);

  data->mu_turb_at_u_nodes = create_array(nx, Ny, Nz, 0);
  data->mu_turb_at_v_nodes = create_array(Nx, ny, Nz, 0);
  data->mu_turb_at_w_nodes = create_array(Nx, Ny, nz, 0);

  data->phase_change_matrix = create_array_int(Nx, Ny, Nz, 0);

  data->W_coeff = create_array_int(Nx, Ny, Nz, 1);
  data->E_coeff = create_array_int(Nx, Ny, Nz, 1);
  data->N_coeff = create_array_int(Nx, Ny, Nz, 1);
  data->S_coeff = create_array_int(Nx, Ny, Nz, 1);
  data->T_coeff = create_array_int(Nx, Ny, Nz, 1);
  data->B_coeff = create_array_int(Nx, Ny, Nz, 1);

  mem_count(wins);

  return;
}

/* SF_BOUNDARY_SOLID_INDEXS_FINDER */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Data_Mem::solid_boundary_cell_count) Data_Mem::sf_boundary_solid_indexs.

  Array (int, NxNy) Data_Mem::W_is_fluid_OK.

  Array (int, NxNy) Data_Mem::E_is_fluid_OK.

  Array (int, NxNy) Data_Mem::S_is_fluid_OK.

  Array (int, NxNy) Data_Mem::N_is_fluid_OK.

  Array (int, NxNy) Data_Mem::case_s_type_x.

  Array (int, NxNy) Data_Mem::case_s_type_y.

  @param data
  
  @return void
  
  MODIFIED: 05-02-2019
*/
void sf_boundary_solid_indexs_finder(Data_Mem *data){

  int I, J, index_;
  int solid_boundary_cell_count = 0;
  int fluid_nb_OK = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int *dom_matrix = data->domain_matrix;
  
  int *E_is_fluid_OK = data->E_is_fluid_OK;
  int *W_is_fluid_OK = data->W_is_fluid_OK;
  int *N_is_fluid_OK = data->N_is_fluid_OK;
  int *S_is_fluid_OK = data->S_is_fluid_OK;

  char *case_s_type_x = data->case_s_type_x;
  char *case_s_type_y = data->case_s_type_y;
  
  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  
#pragma omp parallel for default(none) private(I, J, index_, fluid_nb_OK) \
  shared(dom_matrix, W_is_fluid_OK, E_is_fluid_OK, S_is_fluid_OK, N_is_fluid_OK, P_solve_phase_OK) \
  reduction(+: solid_boundary_cell_count)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
	  
      if(dom_matrix[index_-1] != 0){
	W_is_fluid_OK[index_] = 0;
      }
	
      if(dom_matrix[index_+1] != 0){
	E_is_fluid_OK[index_] = 0;
      }
	
      if(dom_matrix[index_-Nx] != 0){
	S_is_fluid_OK[index_] = 0;
      }
	
      if(dom_matrix[index_+Nx] != 0){
	N_is_fluid_OK[index_] = 0;
      }
	  
      fluid_nb_OK = 
	(dom_matrix[index_-1] == 0) || (dom_matrix[index_+1] == 0) || 
	(dom_matrix[index_-Nx] == 0) || (dom_matrix[index_+Nx] == 0);
    
      if((dom_matrix[index_] != 0) && P_solve_phase_OK[index_] && fluid_nb_OK){
	solid_boundary_cell_count++;
	
      }
    }
  }

  data->sf_boundary_solid_indexs = 
    create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->I_bm = create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->J_bm = create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->K_bm = create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->solid_boundary_cell_count = solid_boundary_cell_count;

  int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;
  int *I_bm = data->I_bm;
  int *J_bm = data->J_bm;
  int *K_bm = data->K_bm;

  solid_boundary_cell_count = 0;
  
#pragma omp parallel for default(none)					\
  private(I, J, index_, fluid_nb_OK)					\
  shared(dom_matrix, P_solve_phase_OK, sf_boundary_solid_indexs,	\
	 I_bm, J_bm, K_bm, solid_boundary_cell_count,			\
	 case_s_type_x, case_s_type_y)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){
      index_ = J*Nx + I;
          
      fluid_nb_OK = 
	(dom_matrix[index_-1] == 0) || (dom_matrix[index_+1] == 0) || 
	(dom_matrix[index_-Nx] == 0) || (dom_matrix[index_+Nx] == 0);
    
      if((dom_matrix[index_] != 0) && P_solve_phase_OK[index_] && fluid_nb_OK){

#pragma omp critical
	{
	  
	  sf_boundary_solid_indexs[solid_boundary_cell_count] = index_;
	  
	  I_bm[solid_boundary_cell_count] = I;
	  J_bm[solid_boundary_cell_count] = J;
	  K_bm[solid_boundary_cell_count] = 0;
	  
	  solid_boundary_cell_count++;

	  if((I >= 3) && (I <= Nx-4)) {case_s_type_x[index_] = 'a';}
	  else if ((I == 2) || (I == Nx-3)) {case_s_type_x[index_] = 'b';}
	  else if ((I == 1) || (I == Nx-2)) {case_s_type_x[index_] = 'c';}
	  else {case_s_type_x[index_] = 'd';}
	  
	  if((J >= 3) && (J <= Ny-4)) {case_s_type_y[index_] = 'a';}
	  else if ((J == 2) || (J == Ny-3)) {case_s_type_y[index_] = 'b';}
	  else if ((J == 1) || (J == Ny-2)) {case_s_type_y[index_] = 'c';}
	  else {case_s_type_y[index_] = 'd';}
	  
	}
      }
    }
  }
  
  return;
}

/* READ_FROM_MAT_FILE */
/*****************************************************************************/
/**
   Requires libmatio-devel >= 1.5.12.

   @param data

   @return void

   @see save_data_in_MAT_file

   Data read:

   MODIFIED: 20-01-2021

 */
void read_from_MAT_file(Data_Mem *data){

  mat_t *matfp = NULL;

  Wins *wins = &(data->wins);
  
  char msg[SIZE];
    
  const char *res_filename = data->res_filename;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int read_MAT_file_OK = data->read_MAT_file_OK;
  const int turb_model = data->turb_model;
  const int solve_3D_OK = data->solve_3D_OK;

  /* cut cell velocities */
  double *u = data->u;
  double *v = data->v;
  double *w = data->w;
  
  matfp = Mat_Open(res_filename, MAT_ACC_RDONLY);
  
  if(matfp == NULL) {
    free_memory(data);
    error_msg(wins, "Could not open RESULTS file");
  }

  if(data->curves > 0){
    checkCall(read_array_from_MAT_file(u, "u", nx, Ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(v, "v", Nx, ny, Nz, matfp), wins);
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(w, "w", Nx, Ny, nz, matfp), wins);
    }
  }

  /** Center velocities to u,v,w_at_faces arrays */
  center_vel_components(data);
  
  /** Data_Mem::U, Data_Mem::V, Data_Mem::W (if Data_Mem::solve_3D_OK), Data_Mem::p. */
  checkCall(read_array_from_MAT_file(data->U, "U", Nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->V, "V", Nx, Ny, Nz, matfp), wins);
  
  if(solve_3D_OK){
    checkCall(read_array_from_MAT_file(data->W, "W", Nx, Ny, Nz, matfp), wins);
  }
  checkCall(read_array_from_MAT_file(data->p, "p", Nx, Ny, Nz, matfp), wins);
  
  /** Data_Mem::k_turb, Data_Mem::eps_turb (if Data_Mem::turb_model). */
  if(turb_model){    
    checkCall(read_array_from_MAT_file(data->k_turb, "k_turb", Nx, Ny, Nz, matfp),
	      wins);
    checkCall(read_array_from_MAT_file(data->eps_turb, "eps_turb", Nx, Ny, Nz, matfp),
	      wins);
  }

  /** if Data_Mem::read_MAT_file_OK == 2: \n Data_Mem::phi, Data_Mem::gL. */
  if(read_MAT_file_OK == 2 || read_MAT_file_OK == 12){
    checkCall(read_array_from_MAT_file(data->phi, "phi", Nx, Ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->gL, "gL", Nx, Ny, Nz, matfp), wins);
    
    snprintf(msg, SIZE, "Full data read from %s", res_filename);
  }
  else{
    snprintf(msg, SIZE, "Flow field read from %s", res_filename);
  }
  
  Mat_Close(matfp);

  info_msg(wins, msg);
  
  return;   
}

/* READ_FLOW_FIELD_FROM_TMP_FILE */
/*****************************************************************************/
/**
   Requires libmatio-devel >= 1.5.12.

   @param data

   @return void

   @see write_to_tmp_file

   MODIFIED: 28-07-2020
   
   Data read:
 */
void read_flow_field_from_tmp_file(Data_Mem *data){

  mat_t *matfp = NULL;

  Wins *wins = &(data->wins);
  
  char msg[SIZE];
    
  const char *tmp_filename = data->tmp_filename;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  
  const int solve_3D_OK = data->solve_3D_OK;

  double dt[7] = {0};

  double *refs = data->sigmas.refs;
    
  matfp = Mat_Open(tmp_filename, MAT_ACC_RDONLY);
  
  if (matfp == NULL) {
    free_memory(data);
    error_msg(wins, "Could not open TMP file!!");
  }

    /* Reading reference values for residues */
  checkCall(read_array_from_MAT_file(refs, "refs", 1, 9, 1, matfp), wins);
  
  checkCall(read_array_from_MAT_file(dt, "dt", 7, 1, 1, matfp), wins);
  data->dt[0] = dt[0];
  data->dt[6] = dt[6];
  
  /* Read cut cell velocities */
  checkCall(read_array_from_MAT_file(data->u, "u", nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->v, "v", Nx, ny, Nz, matfp), wins);
  if(solve_3D_OK){
    checkCall(read_array_from_MAT_file(data->w, "w", Nx, Ny, nz, matfp), wins);
  }

  /** Center velocities to u,v,w_at_faces arrays */
  center_vel_components(data);

  /** Data_Mem::p_W_non_uni, Data_Mem::p_E_non_uni, Data_Mem::p_S_non_uni, Data_Mem::p_N_non_uni. */  
  checkCall(read_array_from_MAT_file(data->p_W_non_uni, "p_W_non_uni", 1, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->p_E_non_uni, "p_E_non_uni", 1, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->p_S_non_uni, "p_S_non_uni", Nx, 1, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->p_N_non_uni, "p_N_non_uni", Nx, 1, Nz, matfp), wins);

  /** if Data_Mem::solve_3D_OK: Data_Mem::p_B_non_uni, Data_Mem::p_T_non_uni. */
  if(solve_3D_OK){
    checkCall(read_array_from_MAT_file(data->p_B_non_uni, "p_B_non_uni", Nx, Ny, 1, matfp), wins);
    checkCall(read_array_from_MAT_file(data->p_T_non_uni, "p_T_non_uni", Nx, Ny, 1, matfp), wins);
  }
  
  /** Data_Mem::p. */
  checkCall(read_array_from_MAT_file(data->p, "p", Nx, Ny, Nz, matfp), wins);
  /** Data_Mem::k_turb, Data_Mem::eps_turb, Data_Mem::mu_turb. */
  checkCall(read_array_from_MAT_file(data->k_turb, "k_turb", Nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->eps_turb, "eps_turb", Nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->mu_turb, "mu_turb", Nx, Ny, Nz, matfp), wins);
  
  Mat_Close(matfp);
  
  sprintf(msg, "Flow field read from %s", tmp_filename);
  info_msg(wins, msg);
  
  return;   
}

/* READ_FROM_TMP_FILE */
/*****************************************************************************/
/**
   Requires libmatio-devel >= 1.5.12.

   @param data

   @return void

   @see write_to_tmp_file

   MODIFIED: 17-07-2020

   Data read:

 */
void read_from_tmp_file(Data_Mem *data){

  mat_t *matfp = NULL;
  
  Wins *wins = &(data->wins);
  
  char msg[SIZE];

  const char *tmp_filename = data->tmp_filename;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int solve_3D_OK = data->solve_3D_OK;

  double *refs = data->sigmas.refs;
    
  matfp = Mat_Open(tmp_filename, MAT_ACC_RDONLY);
    
  if (matfp == NULL) {
    free_memory(data);
    error_msg(wins, "Could not open TMP file!!");
  }
  
  /* Reading reference values for residues */
  checkCall(read_array_from_MAT_file(refs, "refs", 1, 9, 1, matfp), wins);

  checkCall(read_array_from_MAT_file(data->dt, "dt", 1, 1, 7, matfp), wins);

  /** Data_Mem::u, Data_Mem::v, Data_Mem::w (if Data_Mem::solve_3D_OK) */ 
  checkCall(read_array_from_MAT_file(data->u, "u", nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->v, "v", Nx, ny, Nz, matfp), wins);
  if(solve_3D_OK){
    checkCall(read_array_from_MAT_file(data->w, "w", Nx, Ny, nz, matfp), wins);
  }
  
  /* Center velocity components */
  center_vel_components(data);
  
  /** Data_Mem::p. */
  checkCall(read_array_from_MAT_file(data->p, "p", Nx, Ny, Nz, matfp), wins);
  /** Data_Mem::k_turb, Data_Mem::eps_turb, Data_Mem::mu_turb. */
  checkCall(read_array_from_MAT_file(data->k_turb, "k_turb", Nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->eps_turb, "eps_turb", Nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->mu_turb, "mu_turb", Nx, Ny, Nz, matfp), wins);
  /** Data_Mem::phi, Data_Mem::gL */
  checkCall(read_array_from_MAT_file(data->phi, "phi", Nx, Ny, Nz, matfp), wins);
  checkCall(read_array_from_MAT_file(data->gL, "gL", Nx, Ny, Nz, matfp), wins);
  
  Mat_Close(matfp);
  
  sprintf(msg, "Data read from %s", tmp_filename);
  info_msg(wins, msg);
  
  return;
}

/* WRITE_TO_TMP_FILE */
/*****************************************************************************/
/**
   Requires libmatio-devel >= 1.5.12.

   @param data

   @return void 

   @see read_flow_field_from_tmp_file

   @see read_from_tmp_file 

   MODIFIED: 28-07-2020

   Data written:
*/
void write_to_tmp_file(Data_Mem *data){

  mat_t *matfp = NULL;
  
  Wins *wins = &(data->wins);
  
  const char *tmp_filename = data->tmp_filename;

  int write_OK = 1;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int solve_3D_OK = data->solve_3D_OK;

  double *refs = data->sigmas.refs;
  
  const double *sigmas_v = data->sigmas.values;

  write_OK = !(isnan(sigmas_v[0]) && isnan(sigmas_v[1]) && isnan(sigmas_v[2]) &&
	       isnan(sigmas_v[3]) && isnan(sigmas_v[4]) && isnan(sigmas_v[5]) &&
	       isnan(sigmas_v[6]) && isnan(sigmas_v[7]) && isnan(sigmas_v[8]));
  
  if(write_OK){
    
    matfp = get_matfp(tmp_filename, 1, wins);
    
    /* Printing reference values for residues */
    checkCall(write_array_in_MAT_file(refs, "refs", 1, 9, 1, matfp), wins);

    checkCall(write_array_in_MAT_file(data->dt, "dt", 7, 1, 1, matfp), wins);
    
    /** cut cell velocities */
    checkCall(write_array_in_MAT_file(data->u, "u", nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->v, "v", Nx, ny, Nz, matfp), wins);
    if(solve_3D_OK){
      checkCall(write_array_in_MAT_file(data->w, "w", Nx, Ny, nz, matfp), wins);
    }

    /** Data_Mem::p_W_non_uni, Data_Mem::p_E_non_uni, Data_Mem::p_S_non_uni, Data_Mem::p_N_non_uni. */
    checkCall(write_array_in_MAT_file(data->p_W_non_uni, "p_W_non_uni", 1, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->p_E_non_uni, "p_E_non_uni", 1, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->p_S_non_uni, "p_S_non_uni", Nx, 1, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->p_N_non_uni, "p_N_non_uni", Nx, 1, Nz, matfp), wins);

    /** if Data_Mem::solve_3D_OK: Data_Mem::p_B_non_uni, Data_Mem::p_T_non_uni. */
    if(solve_3D_OK){
      checkCall(write_array_in_MAT_file(data->p_B_non_uni, "p_B_non_uni", Nx, Ny, 1, matfp), wins);
      checkCall(write_array_in_MAT_file(data->p_T_non_uni, "p_T_non_uni", Nx, Ny, 1, matfp), wins);
    }
    
    /** Data_Mem::p. */
    checkCall(write_array_in_MAT_file(data->p, "p", Nx, Ny, Nz, matfp), wins);
    /** Data_Mem::k_turb, Data_Mem::eps_turb, Data_Mem::mu_turb. */
    checkCall(write_array_in_MAT_file(data->k_turb, "k_turb", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->eps_turb, "eps_turb", Nx, Ny, Nz, matfp),
	      wins);
    checkCall(write_array_in_MAT_file(data->mu_turb, "mu_turb", Nx, Ny, Nz, matfp),
	      wins);
    /** Data_Mem::phi, Data_Mem::gL. */
    checkCall(write_array_in_MAT_file(data->phi, "phi", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->gL, "gL", Nx, Ny, Nz, matfp), wins);
    
    Mat_Close(matfp);
    
#ifndef DISPLAY_OK
    char msg[SIZE];
    sprintf(msg, "Data written to %s", tmp_filename);
    info_msg(wins, msg);
#endif
    update_upds_window(wins, 5);
    
  }
  else{
    alert_msg(wins, "Skipping write to TMP due to NAN");
  }
  
  return;
}

/* CHANGE_ALPHAS_PHI */
void change_alphas_phi(Data_Mem *data){

  double alpha_phi = 1, alpha_SC_phi = 1;

  printf("Please input new alpha values, "
	 "current are phi SC_phi = %.2f %.2f: ", 
	 data->alpha_phi, data->alpha_SC_phi);
  scanf("%lf %lf", &alpha_phi, &alpha_SC_phi);

  while(alpha_phi < 0 || alpha_phi > 2.0 || 
	alpha_SC_phi < 0 || alpha_SC_phi > 2.0){
    printf("Not a valid value!!\n");
    printf("Please input new alpha values, "
	   "current are phi SC_phi = %.2f %.2f: ", 
	   data->alpha_phi, data->alpha_SC_phi);
    scanf("%lf %lf", &alpha_phi, &alpha_SC_phi);
  }

  printf("\n");

  data->alpha_phi = alpha_phi;
  data->alpha_SC_phi = alpha_SC_phi;

  return;
}

/* CHANGE_ALPHAS_FLOW */
void change_alphas_flow(Data_Mem *data){

  const int solve_3D_OK = data->solve_3D_OK;
  const int turb_OK = data->turb_model;

  double alpha_u = 1, alpha_v = 1, alpha_w = 1, alpha_p_corr = 1, alpha_p_coeff = 1;
  double alpha_k = 1, alpha_e = 1, alpha_Pk = 1, alpha_SC_k = 1, alpha_SC_e = 1;

  if(solve_3D_OK){

    printf("Please input new alpha values, "
	   "current are u v w p_corr p_coeff = %.4f %.4f %.4f %.4f %.4f: ",
	   data->alpha_u, data->alpha_v, data->alpha_w,
	   data->alpha_p_corr, data->alpha_p_coeff);
    scanf("%lf %lf %lf %lf %lf", &alpha_u, &alpha_v, &alpha_w, &alpha_p_corr, &alpha_p_coeff);
  }
  else{
    
    printf("Please input new alpha values, "
	   "current are u v p_corr p_coeff = %.4f %.4f %.4f %.4f: ", 
	   data->alpha_u, data->alpha_v, 
	   data->alpha_p_corr, data->alpha_p_coeff);
    scanf("%lf %lf %lf %lf", &alpha_u, &alpha_v, &alpha_p_corr, &alpha_p_coeff);
  }

/* In 2D case alpha_w retains its starting value, so the condition evaluates to True */
  while(alpha_u < 0 || alpha_u > 2.0 ||
	alpha_v < 0 || alpha_v > 2.0 ||
	alpha_w < 0 || alpha_w > 2.0 ||
	alpha_p_corr < 0 || alpha_p_corr > 2.0 ||
        alpha_p_coeff < 0 || alpha_p_coeff > 2.0){
    
    printf("Not a valid value!!\n");

    if(solve_3D_OK){

      printf("Please input new alpha values, "
	     "current are u v w p_corr, p_coeff = %.4f %.4f %.4f %.4f %.4f: ",
	     data->alpha_u, data->alpha_v, alpha_w,
	     data->alpha_p_corr, data->alpha_p_coeff);
      scanf("%lf %lf %lf %lf %lf", &alpha_u, &alpha_v, &alpha_w,
	    &alpha_p_corr, &alpha_p_coeff);
    }
    else{

      printf("Please input new alpha values, "
	     "current are u v p_corr p_coeff = %.4f %.4f %.4f %.4f: ", 
	     data->alpha_u, data->alpha_v, 
	     data->alpha_p_corr, data->alpha_p_coeff);
      scanf("%lf %lf %lf %lf", &alpha_u, &alpha_v, 
	    &alpha_p_corr, &alpha_p_coeff);
    }
  }

  printf("\n");

  data->alpha_u = alpha_u;
  data->alpha_v = alpha_v;
  data->alpha_w = (solve_3D_OK)? alpha_w : data->alpha_w;
  data->alpha_p_corr = alpha_p_corr;
  data->alpha_p_coeff = alpha_p_coeff;

  /* Now the turbulent variables */

  if(turb_OK){

    printf("Please input new alpha values, "
	   "current are k e Pk SC_k SC_e = %.4f %.4f %.4f %.4f %.4f: ", 
	   data->alpha_k, data->alpha_eps, 
	   data->alpha_P_k, data->alpha_SC_k, data->alpha_SC_eps);
    scanf("%lf %lf %lf %lf %lf", &alpha_k, &alpha_e, &alpha_Pk,
	  &alpha_SC_k, &alpha_SC_e);

    while(alpha_k < 0 || alpha_k > 2.0 || alpha_e < 0 || alpha_e > 2.0 || 
	  alpha_Pk < 0 || alpha_Pk > 2.0 || alpha_SC_k < 0 || alpha_SC_k > 2.0 ||
	  alpha_SC_e < 0 || alpha_SC_e > 2.0){
    
      printf("Not a valid value!!\n");
      printf("Please input new alpha values, "
	     "current are k e Pk SC_k SC_e = %.4f %.4f %.4f %.4f %.4f: ", 
	     data->alpha_k, data->alpha_eps, 
	     data->alpha_P_k, data->alpha_SC_k, data->alpha_SC_eps);
      scanf("%lf %lf %lf %lf %lf", &alpha_k, &alpha_e, &alpha_Pk,
	    &alpha_SC_k, &alpha_SC_e);
    }

    printf("\n");

    data->alpha_k = alpha_k;
    data->alpha_eps = alpha_e;
    data->alpha_P_k = alpha_Pk;
    data->alpha_SC_k = alpha_SC_k;
    data->alpha_SC_eps = alpha_SC_e;

  }
  
  return;
}

/* WRITE_DATA */
/*****************************************************************************/
/**

   Saves phi data in data.mat

   Requires libmatio-devel >= 1.5.12.

   @param data

   @return void

   MODIFIED: 17-07-2020

   Data written: 
   
 */
void write_data(Data_Mem *data){

  char filename[SIZE];
    
  mat_t *matfp = NULL;

  Wins *wins = &(data->wins);
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  
  const int curves = data->curves;

  snprintf(filename, SIZE, "%s%s", 
	   data->results_dir, "data.mat");

  matfp = get_matfp(filename, 1, wins);

  checkCall(write_array_in_MAT_file(data->norm_x, "norm_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->norm_y, "norm_y", Nx, Ny, Nz, matfp), wins);

  checkCall(write_int_array_in_MAT_file(data->P_solve_phase_OK, "P_solve_phase_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->E_solve_phase_OK, "E_solve_phase_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->W_solve_phase_OK, "W_solve_phase_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->N_solve_phase_OK, "N_solve_phase_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->S_solve_phase_OK, "S_solve_phase_OK", Nx, Ny, Nz, matfp), wins);
  // checkCall(write_int_array_in_MAT_file(data->T_solve_phase_OK, "T_solve_phase_OK", Nx, Ny, Nz, matfp), wins);
  // checkCall(write_int_array_in_MAT_file(data->B_solve_phase_OK, "B_solve_phase_OK", Nx, Ny, Nz, matfp), wins);

  /** Data_Mem::domain_matrix, Data_Mem::cut_matrix. */
  checkCall(write_int_array_in_MAT_file(data->domain_matrix, "dom_matrix", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->cut_matrix, "cut_matrix", Nx, Ny, Nz, matfp), wins);

  /** Data_Mem::nodes_x, Data_Mem::nodes_y, Data_Mem::nodes_z. */
  checkCall(write_array_in_MAT_file(data->nodes_x, "x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->nodes_y, "y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->nodes_z, "z", Nx, Ny, Nz, matfp), wins);
  
  if(data->solve_phase_change_OK){
    /** Data_Mem::phase_change_vol (if Data_Mem::solve_phase_change_OK). */
    checkCall(write_array_in_MAT_file(data->phase_change_vol, "phase_change_vol", Nx, Ny, Nz, matfp), wins);
  }

  /** Data_Mem::u_at_faces, Data_Mem::v_at_faces, Data_Mem::w_at_faces. */
  checkCall(write_array_in_MAT_file(data->u_at_faces, "uf", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_at_faces, "vf", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->w_at_faces, "wf", Nx, Ny, nz, matfp), wins);

  /** Data_Mem::U, Data_Mem::V, Data_Mem::W. */
  checkCall(write_array_in_MAT_file(data->U, "U", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->V, "V", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->W, "W", Nx, Ny, Nz, matfp), wins);
  
  /*
  checkCall(write_array_in_MAT_file(data->u_at_corners, "uc", nx, ny, nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_at_corners, "vc", nx, ny, nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->w_at_corners, "wc", nx, ny, nz, matfp), wins);  

  checkCall(write_array_in_MAT_file(data->d2Uidxjdxk2, "d2Uidxjdxk2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->S_CG_eps, "S_CG_eps", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_int_array_in_MAT_file(data->p_cut_face_x, "p_cut_face_x", nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->p_cut_face_y, "p_cut_face_y", Nx, ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->p_cut_face_z, "p_cut_face_z", Nx, Ny, nz, matfp), wins);  

  */
  
  checkCall(write_array_in_MAT_file(data->v_fraction, "v_fraction", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_fraction_x, "v_frac_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_fraction_y, "v_frac_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_fraction_z, "v_frac_z", Nx, Ny, Nz, matfp), wins);

  /*
  checkCall(write_array_in_MAT_file(data->fe, "fe", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->fn, "fn", Nx, Ny, Nz, matfp), wins);
  
  
  checkCall(write_array_in_MAT_file(data->phi0, "phi0", Nx, Ny, Nz, matfp), wins);
  */
  
  if(curves > 0){
    checkCall(write_array_in_MAT_file(data->gamma_v, "gamma_v", curves+1, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->phi_b_v, "phi_b_v", curves, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->dphidNb_v, "dphidNb_v", curves, 1, 1, matfp), wins);
  }

  checkCall(write_array_in_MAT_file(data->SC_vol_m, "SC_vol_m", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->SC_vol_m_0, "SC_vol_m_0", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->Delta_xE, "DxE", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_xW, "DxW", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_yN, "DyN", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_yS, "DyS", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_zT, "DzT", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_zB, "DzB", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->norm_vec_x_x, "norm_vec_x_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->norm_vec_x_y, "norm_vec_x_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->norm_vec_x_z, "norm_vec_x_z", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->par_vec_x_x, "par_vec_x_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_x_y, "par_vec_x_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_x_z, "par_vec_x_z", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->par_vec_x_x_2, "par_vec_x_x_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_x_y_2, "par_vec_x_y_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_x_z_2, "par_vec_x_z_2", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->norm_vec_y_x, "norm_vec_y_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->norm_vec_y_y, "norm_vec_y_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->norm_vec_y_z, "norm_vec_y_z", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->par_vec_y_x, "par_vec_y_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_y_y, "par_vec_y_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_y_z, "par_vec_y_z", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->par_vec_y_x_2, "par_vec_y_x_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_y_y_2, "par_vec_y_y_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_y_z_2, "par_vec_y_z_2", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->norm_vec_z_x, "norm_vec_z_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->norm_vec_z_y, "norm_vec_z_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->norm_vec_z_z, "norm_vec_z_z", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->par_vec_z_x, "par_vec_z_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_z_y, "par_vec_z_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_z_z, "par_vec_z_z", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->par_vec_z_x_2, "par_vec_z_x_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_z_y_2, "par_vec_z_y_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_vec_z_z_2, "par_vec_z_z_2", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->epsilon_x, "eps_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->epsilon_y, "eps_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->epsilon_z, "eps_z", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->epsilon_x_sol, "eps_x_sol", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->epsilon_y_sol, "eps_y_sol", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->epsilon_z_sol, "eps_z_sol", Nx, Ny, Nz, matfp), wins);

  checkCall(write_int_array_in_MAT_file(data->E_is_fluid_OK, "E_is_fluid_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->W_is_fluid_OK, "W_is_fluid_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->N_is_fluid_OK, "N_is_fluid_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->S_is_fluid_OK, "S_is_fluid_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->T_is_fluid_OK, "T_is_fluid_OK", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->B_is_fluid_OK, "B_is_fluid_OK", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_char_array_in_MAT_file(data->case_s_type_x, "cx", Nx, Ny, Nz, matfp), wins);
  checkCall(write_char_array_in_MAT_file(data->case_s_type_y, "cy", Nx, Ny, Nz, matfp), wins);
  checkCall(write_char_array_in_MAT_file(data->case_s_type_z, "cz", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->gamma_m, "gamma_m", Nx, Ny, Nz, matfp), wins);
    
  checkCall(write_array_in_MAT_file(data->dphidxb, "dphidxb", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->dphidyb, "dphidyb", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->dphidzb, "dphidzb", Nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->dphidxs, "dphidxs", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->dphidys, "dphidys", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->dphidzs, "dphidzs", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->J_x, "J_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->J_y, "J_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->J_z, "J_z", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->J_norm, "J_norm", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->J_par, "J_par", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->J_par_2, "J_par_2", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->norm_z, "norm_z", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_x, "par_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_y, "par_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_z, "par_z", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_x_2, "par_x_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_y_2, "par_y_2", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->par_z_2, "par_z_2", Nx, Ny, Nz, matfp), wins);
  
  checkCall(write_array_in_MAT_file(data->u_at_faces, "u_at_faces", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_at_faces, "v_at_faces", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->w_at_faces, "w_at_faces", Nx, Ny, nz, matfp), wins);

  /*
  checkCall(write_array_in_MAT_file(data->u_at_faces_y, "u_at_faces_y", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_at_faces_x, "v_at_faces_x", nx, Ny, Nz, matfp), wins);
  */
  
  if(data->solve_phase_change_OK){
    checkCall(write_array_in_MAT_file(data->gL_calc, "gL_calc", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->gL_guess, "gL_guess", Nx, Ny, Nz, matfp), wins);
  }

  if(data->solid_boundary_cell_count){
    checkCall(write_array_in_MAT_file(data->nxnx_x, "nxnx_x", data->solid_boundary_cell_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nxny_x, "nxny_x", data->solid_boundary_cell_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nyny_y, "nyny_y", data->solid_boundary_cell_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nxny_y, "nxny_y", data->solid_boundary_cell_count, 1, 1, matfp), wins);
  }

  Mat_Close(matfp);
  info_msg(wins, "Extra data successfully written");

  return;
}

/* MASK_DOMAIN */
void mask_domain(int *A_, const int Nx, const int Ny){

  int i, j, index_;

#pragma omp  parallel for default(none) private(i, j, index_) shared(A_)

  for(j=0; j<Ny; j++){
    for(i=0; i<Nx; i++){
      index_ = j*Nx+i;
      if(A_[index_] != 0){
	A_[index_] = 1;
      }
    }
  }
  return;
}

/* COPY_ARRAY */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) B_: copy values of array A_.

  @return void
  
  MODIFIED: 07-07-2019
*/
void copy_array(const double *A_, double *B_, const int Nx, const int Ny, const int Nz){
  
  int i, j, k, index_;
  
#pragma omp parallel for default(none) private(i, j, k, index_) shared(A_, B_)
  for(k=0; k<Nz; k++){
    for(j=0; j<Ny; j++){
      for(i=0; i<Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	B_[index_] = A_[index_];
      }
    }
  }
  
  return;
}

/* COPY_ARRAY_INT */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) B_: values of array A_.

  @param A_

  @param B_

  @param Nx

  @param Ny

  @param Nz

  @return void
  
  MODIFIED: 07-07-2019
*/
void copy_array_int(const int *A_, int *B_, const int Nx, const int Ny, const int Nz){

  int i, j, k, index_;

#pragma omp parallel for default(none) private(i, j, k, index_) shared(A_, B_)
  
  for(k=0; k<Nz; k++){
    for(j=0; j<Ny; j++){
      for(i=0; i<Nx; i++){
	index_ = k*Nx*Ny + j*Nx + i;
	B_[index_] = A_[index_];
      }
    }
  }
  return;
}

/* SET_NANS_DOMAIN */
/*****************************************************************************/
/**
   Set phi array values to NAN if the corresponding filter array values are different from a certain value.

   @param *phi: pointer to array that will be modified.
   
   @param *filter: pointer to filter array.

   @param value: value that is compared to entries in filter array.

   @param Nx, Ny, Nz: array size.

   @return void

   MODIFIED: 17-07-2020
   
 */
void set_nans_domain(double *phi, const int *filter, const int value,
		     const int Nx, const int Ny, const int Nz){
  
  int I, J, K, index_;
  
#pragma omp parallel for default(none)		\
  private(I, J, K, index_) shared(phi, filter)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	  
	index_ = K*Ny*Nx + J*Nx + I;
	  
	if(filter[index_] != value){
	  phi[index_] = NAN;          
	}       
      }
    }
  }
    
  return;
}

/* SET_NO_NANS_DOMAIN */
/*****************************************************************************/
/**
   Set phi array NAN entries to a certain value.

   @param *phi: pointer to array that will be modified.

   @param value: value that will replace NAN entries.
   
   @param Nx, Ny, Nz: array size.

   @return void

   MODIFIED: 17-07-2020
   
 */
void set_no_nans_domain(double *phi, const double value,
			const int Nx, const int Ny, const int Nz){

  int I, J, K, index_;

#pragma omp parallel for default(none)		\
  private(I, J, K, index_) shared(phi)
  
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	  
	index_ = K*Ny*Nx + J*Nx + I;
	if(isnan(phi[index_])){
	  phi[index_] = value;          
	}       
      }
    }
  }
    
  return;   
}

/* NORMALIZE_SELF */
void normalize_self(double *phi, const int Nx, const int Ny, const int Nz){

  double min, max;
  
  min = get_min(phi, Nx, Ny, Nz);
  max = get_max(phi, Nx, Ny, Nz);

  normalize(phi, min, max, Nx, Ny, Nz);

  return;
}

/* NORMALIZE */
void normalize(double *phi, const double min, const double max,
	       const int Nx, const int Ny, const int Nz){

  int I, J, K, index_;

#pragma omp parallel for default(none)		\
  private(I, J, K, index_) shared(phi)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	  
	index_ = K*Ny*Nx + J*Nx + I;
	phi[index_] = (phi[index_] - min)/(max - min);
      }
    }
  }

  return;
}

/* SET_BC_VALUE */
void set_bc_value(double *phi, const double value, const char face, const int Nx, const int Ny){

  int I, J, index_;

#pragma omp parallel sections default(none) private(I, J, index_) shared(phi)
  {

#pragma omp section 
    {
 
      if((face == 'E') || (face == 'e')){
	I = Nx-1;
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  phi[index_] = value;
	}
      }
    } /* East */

#pragma omp section 
    {
    
      if((face == 'N') || (face == 'n')){
	J = Ny-1;
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  phi[index_] = value;
	}
      }
    } /* North */
    
#pragma omp section 
    {

      if((face == 'W') || (face == 'w')){
	I = 0;
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx + I;
	  phi[index_] = value;
	}
      }
    } /* West */
    
#pragma omp section 
    {

      if((face == 'S') || (face == 's')){
	J = 0;
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  phi[index_] = value;
	}
      }  
    } /* South */
  } /* sections */

  return;
  
}

/* SET_BC_MIRROR */
void set_bc_mirror(double *phi, const char face, const int Nx, const int Ny){

  int I, J, index_;
 
#pragma omp parallel sections default(none) private(I, J, index_) shared(phi)
  {

#pragma omp section
    {
      if((face == 'E') || (face == 'e')){
	I = Nx-1;
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx+I;
	  phi[index_] = phi[index_-1];
	}
      }
    }
    
#pragma omp section
    {
      if((face == 'N') || (face == 'n')){
	J = Ny-1;
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx+I;
	  phi[index_] = phi[index_-Nx];
	}
      }
    }
    
#pragma omp section
    {
      if((face == 'W') || (face == 'w')){
	I = 0;
	for(J=1; J<Ny-1; J++){
	  index_ = J*Nx+I;
	  phi[index_] = phi[index_+1];
	}
      }
    }
    
#pragma omp section
    {
      if((face == 'S') || (face == 's')){
	J = 0;
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx+I;
	  phi[index_] = phi[index_+Nx];
	}
      }
    }
  } /* sections */  

  return;
  
}

/* GET_MAX */
/*****************************************************************************/
/**

   @param *A_ pointer to array.

   @param Nx, Ny, Nz: array size.

   @return max_val: maximum value on array A_.

   MODIFIED: 17-07-2020
 */
double get_max(const double *A_, const int Nx, const int Ny, const int Nz){
  
  int I, J, K, index_, found_value_OK = 0;
  double max_val = -1e10;
  
#pragma omp parallel for default(none) firstprivate(found_value_OK)	\
  private(I, J, K, index_) shared(A_) reduction(max:max_val)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_ = K*Ny*Nx + J*Nx + I;
	
	if(!(isnan(A_[index_])) && (!found_value_OK)){
	  max_val = A_[index_];
	  found_value_OK = 1;
	}
	
	if(!(isnan(A_[index_]))){      
	  max_val = MAX(max_val, A_[index_]);
	}
	
      }
    }
  }
  
  return max_val;
}

/* GET_MIN */
/*****************************************************************************/
/**

   @param *A_ pointer to array.

   @param Nx, Ny, Nz: array size.

   @return min_val: minimum value on array A_.

   MODIFIED: 17-07-2020
 */
double get_min(const double *A_, const int Nx, const int Ny, const int Nz){

  int I, J, K, index_, found_value_OK = 0;
  double min_val = 1e10;

#pragma omp parallel for default(none) firstprivate(found_value_OK)	\
  private(I, J, K, index_) shared(A_) reduction(min:min_val)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_ = K*Ny*Nx + J*Nx + I;
	
	if(!(isnan(A_[index_])) && (!found_value_OK)){
	  min_val = A_[index_];
	  found_value_OK = 1;
	}
	
	if(!(isnan(A_[index_]))){      
	  min_val = MIN(min_val, A_[index_]);
	}
      }
    }
  }
  
  return min_val;
}

/* SET_NANS_CORNERS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) A_: set corner values to NAN.

  @return void
  
  MODIFIED: 07-07-2019
*/
void set_nans_corners(double *A_, const int Nx, const int Ny, const int Nz, const int solve_3D_OK){

  int I, J, K;
  
  if(solve_3D_OK){
    /* 3D case */
    for(I=0; I<Nx; I++){
      A_[I] = NAN;
      A_[(Ny-1)*Nx + I] = NAN;

      A_[(Nz-1)*Ny*Nx + I] = NAN;
      A_[(Nz-1)*Ny*Nx + (Ny-1)*Nx + I] = NAN;
    }

    for(J=0; J<Ny; J++){
      A_[J*Nx] = NAN;
      A_[J*Nx + (Nx-1)] = NAN;

      A_[(Nz-1)*Ny*Nx + J*Nx] = NAN;
      A_[(Nz-1)*Ny*Nx + J*Nx + (Nx-1)] = NAN;
    }

    for(K=0; K<Nz; K++){
      A_[K*Ny*Nx] = NAN;
      A_[K*Ny*Nx + (Nx-1)] = NAN;
    
      A_[K*Ny*Nx + (Ny-1)*Nx] = NAN;
      A_[K*Ny*Nx + (Ny-1)*Nx + (Nx-1)] = NAN;
    }
  }
  else{
    A_[0]                               = NAN;
    A_[Nx-1]                            = NAN;
    A_[(Ny-1)*Nx]                       = NAN;
    A_[(Ny-1)*Nx+(Nx-1)]                = NAN;     
  }
  
  return;
}

/* WRITE_INT_TO_FILE */
int write_int_to_file(const int *A_, const int Nx, const int Ny,
		      const char *msg, const int flag, const char *results_dir){

  FILE *fp;
  char filename[SIZE];
  int I, J;
  int ret_value = 0;
  
  if (strnlen(msg, SIZE) < 1){
    //    error_msg(wins, "Give a correct name for variable to print\n");
    ret_value = 1;
  }
  
  if(results_dir == NULL){

    if (flag == 0){
      snprintf(filename, SIZE, "%s", "./data/CPU/");
      strncat(filename, msg, SIZE-1);
    }
    else{
      snprintf(filename, SIZE, "%s", "./data/GPU/");
      strncat(filename, msg, SIZE-1);
    }

    strncat(filename, ".dat", 5);
  
    fp = fopen(filename, "w");

    if (fp == NULL){
      //      error_msg(wins, "Could not open file, permissions??...");
      ret_value = -1;
      exit(EXIT_FAILURE);
    }

    if (flag == 0){
      fprintf(fp, "%s_cpu_var(:,:) = [\n", msg);
    }
    else{
      fprintf(fp, "%s_gpu_var(:,:) = [\n", msg);
    }
  

  } // end if
  else{
    snprintf(filename, SIZE, "%s%s", results_dir, msg);

    strncat(filename, ".dat", 5);
    
    fp = fopen(filename, "w");
    
    if (fp == NULL){
      //      error_msg(wins, "Could not open file, permissions??...");
      ret_value = -1;
      exit(EXIT_FAILURE);
    }

    fprintf(fp, "%s_cpu_var(:,:) = [\n", msg);
  }

  for (I = 0; I < Nx; I++) {  
    for (J = 0; J < Ny; J++){
      fprintf(fp, "%+4d ", A_[J*Nx + I]);
    }
    if (I == Nx-1){
      fprintf(fp, "\n");
    }
    else {
      fprintf(fp, ";\n");
    }
  }
  
  fprintf(fp, "];\n");
  
  fclose(fp);

  return ret_value;
}

/* VERIFY_ALLOC_INT */
void verify_alloc_int(const int *ptr){
  if(ptr == NULL){
    //    error_msg(wins, "\nCould not allocate memory!!\n");
    exit(EXIT_FAILURE);
  }
  return;
}

/* FILL_ARRAY_INT */
void fill_array_int(int *A_, const int Nx, const int Ny, const int Nz, const int number){
  int I, J, K, index_;

#pragma omp parallel for default(none) private(I, J, K, index_) shared(A_)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	A_[index_] = number;
      }
    }
  }
  return;
}

/* FILL_ARRAY_char */
void fill_array_char(char *A_, const int Nx, const int Ny, const int Nz, const char d){
  
  int I, J, K, index_;

#pragma omp parallel for default(none) private(I, J, K, index_) shared(A_)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	A_[index_] = d;
      }
    }
  }
  return;
}

/* CREATE_ARRAY_INT */
int * create_array_int(const int Nx, const int Ny, const int Nz, const int number){

  int *A_ = NULL;

  if(number == 0){
    A_ = (int *) calloc(Nx*Ny*Nz, sizeof(int));
    verify_alloc_int(A_);
  }
  else{
    A_ = (int *) malloc(Nx*Ny*Nz * sizeof(int));
    verify_alloc_int(A_);      
    fill_array_int(A_, Nx, Ny, Nz, number);
  }

  /* Increase the global variable for memory count */
  count_int = count_int + Nx*Ny*Nz;

  return A_;
}

/* CREATE_ARRAY_CHAR */
char * create_array_char(const int Nx, const int Ny, const int Nz, const char d){

  char *A_ = NULL;

  
  A_ = (char *) malloc(Nx*Ny*Nz * sizeof(char));
  
  if(A_ == NULL){
    exit(EXIT_FAILURE);
  }
  
  fill_array_char(A_, Nx, Ny, Nz, d);
  
  /* Increase the global variable for memory count */
  count_char = count_char + Nx*Ny*Nz;

  return A_;
}

/* ELAPSED_TIME */
u_int64_t elapsed_time(u_int64_t *et) {
  struct timeval t;

  const u_int64_t former_time = *et;

  gettimeofday(&t, NULL);
  *et = t.tv_sec * 1000000 + t.tv_usec;

  return *et - former_time;
}

/* WRITE_DOUBLE_TO_FILE */
int write_double_to_file(const double *A_,
			 const int Nx, const int Ny, 
			 const char *msg, const int flag,
			 const char *results_dir){
  
  FILE *fp;
  char filename[SIZE];
  int I, J;
  int ret_value = 0;

  if(results_dir == NULL){

    if (strnlen(msg, SIZE) < 1){
      //      error_msg(wins, "Give a correct name for variable to print\n");
      ret_value = 1;
    }
    
    if (flag == 0){
      sprintf(filename, "%s", "./data/CPU/");
      strncat(filename, msg, SIZE-1);
    }
    else{
      sprintf(filename, "%s", "./data/GPU/");
      strncat(filename, msg, SIZE-1);
    }
    
    strncat(filename, ".dat", 5);
    
    fp = fopen(filename, "w");
    
    if (fp == NULL){
      //    error_msg(wins, "Could not open file, permissions??...");
      ret_value = -1;
      exit(EXIT_FAILURE);
    }
    
    if (flag == 0){
      fprintf(fp, "%s_cpu_var(:,:) = [\n", msg);
    }
    else{
      fprintf(fp, "%s_gpu_var(:,:) = [\n", msg);
    }
  }
  
  else{
    snprintf(filename, SIZE, "%s%s", results_dir, msg);
    strncat(filename, ".dat", 5);
    
    fp = fopen(filename, "w");
    
    if (fp == NULL){
      //      error_msg(wins, "Could not open file, permissions??...");
      ret_value = -1;
      exit(EXIT_FAILURE);
    }
    
    fprintf(fp, "%s_cpu_var(:,:) = [\n", msg);
  }
  
  for (I = 0; I < Nx; I++) {  
    for (J = 0; J < Ny; J++){
      fprintf(fp, "%+14.7g ", A_[J*Nx + I]);
    }
    if (I == Nx-1){
      fprintf(fp, "\n");
    }
    else {
      fprintf(fp, ";\n");
    }
  }
  
  fprintf(fp, "];\n");
  
  fclose(fp);

  return ret_value;
}

/* FREE_MEMORY */
void free_memory(Data_Mem *data){

  /* Since v7.3 MAT file remains open to avoid growing its size (they cannot
     be open/closed repeatedly), we close it here */
  const int fss_OK = data->flow_steady_state_OK;
  const int ss_OK = data->steady_state_OK;
  
  const int wftd_OK = data->write_flow_transient_data_OK;
  const int wtd_OK = data->write_transient_data_OK;

  const int tos = data->TEG_OK_summary;

  if(!fss_OK && wftd_OK){
    Mat_VarFree(data->matios.U);
    Mat_VarFree(data->matios.V);
    Mat_VarFree(data->matios.W);
    Mat_VarFree(data->matios.p);
    Mat_VarFree(data->matios.k);
    Mat_VarFree(data->matios.e);
    Mat_VarFree(data->matios.tflow);
  }
  
  if(!ss_OK && wtd_OK){
    Mat_VarFree(data->matios.phi);
    Mat_VarFree(data->matios.gL);
    Mat_VarFree(data->matios.tphi);

    if(tos){
      Mat_VarFree(data->matios.current);
      Mat_VarFree(data->matios.voltage);
      Mat_VarFree(data->matios.power);
    }
  }
  
  if ((!fss_OK || !ss_OK) && (wftd_OK || wtd_OK)){
    Mat_Close(data->matios.matfp_tr);
  }

  /* TEG */
  free(data->teg.TE_OK);
  free(data->teg.ntppm);
  
  free(data->teg.QL);
  free(data->teg.Q0);

  free(data->teg.TfH);
  free(data->teg.TfC);
  
  free(data->teg.hL);
  free(data->teg.h0);

  free(data->teg.TL);
  free(data->teg.T0);

  free(data->teg.ALPHA);
  free(data->teg.K);
  free(data->teg.Re);

  free(data->teg.RHOe);
  free(data->teg.k);
  free(data->teg.A);
  free(data->teg.L);

  /* Boundary_Info */
  free_bc_info(&(data->west));
  free_bc_info(&(data->east));
  free_bc_info(&(data->south));
  free_bc_info(&(data->north));
  free_bc_info(&(data->bottom));
  free_bc_info(&(data->top));

  /* Grid_Info */
  free_grid_info(&(data->x_side), 1);
  free_grid_info(&(data->y_side), 1);
  free_grid_info(&(data->z_side), data->solve_3D_OK);
  
  /* ADI_Vars */
  free(data->ADI_vars.a_T);
  free(data->ADI_vars.b_T);
  free(data->ADI_vars.c_T);
  free(data->ADI_vars.d_T);
  free(data->ADI_vars.c_p);
  free(data->ADI_vars.d_p);
  
  /* Coeffs */
  free_coeffs(&(data->coeffs_u));
  free_coeffs(&(data->coeffs_v));
  free_coeffs(&(data->coeffs_w));
  free_coeffs(&(data->coeffs_p));
  free_coeffs(&(data->coeffs_k));
  free_coeffs(&(data->coeffs_eps));
  free_coeffs(&(data->coeffs_phi));
  
  free(data->c_x);
  free(data->c_y);

  free(data->vol);
  free(data->vol_uncut);
  free(data->vol_x);
  free(data->vol_y);
  free(data->vol_z);

  free(data->vf_x);
  free(data->vf_y);
  free(data->vf_z);
  
  free(data->nodes_x); 
  free(data->nodes_y);
  free(data->nodes_z);
  free(data->Delta_xe);
  free(data->Delta_xE);
  free(data->Delta_xw);
  free(data->Delta_xW);
  free(data->Delta_yn);
  free(data->Delta_yN);
  free(data->Delta_ys);
  free(data->Delta_yS);
  free(data->Delta_zt);
  free(data->Delta_zb);
  free(data->Delta_zT);
  free(data->Delta_zB);
  free(data->Delta_x);
  free(data->Delta_y);
  free(data->Delta_z);
  free(data->vertex_x);
  free(data->vertex_y);
  free(data->vertex_z);

  free(data->fe);
  free(data->fw);
  free(data->fn);
  free(data->fs);
  free(data->ft);
  free(data->fb);

  free(data->nodes_x_u); 
  free(data->nodes_y_u);
  free(data->nodes_z_u);
  free(data->Delta_xe_u);
  free(data->Delta_xE_u);
  free(data->Delta_xw_u);
  free(data->Delta_xW_u);
  free(data->Delta_yn_u);
  free(data->Delta_yN_u);
  free(data->Delta_ys_u);
  free(data->Delta_yS_u);
  free(data->Delta_zt_u);
  free(data->Delta_zb_u);
  free(data->Delta_zT_u);
  free(data->Delta_zB_u);
  free(data->Delta_x_u);
  free(data->Delta_y_u);
  free(data->Delta_z_u);
  free(data->vertex_x_u);

  free(data->nodes_x_v); 
  free(data->nodes_y_v);
  free(data->nodes_z_v);
  free(data->Delta_xe_v);
  free(data->Delta_xE_v);
  free(data->Delta_xw_v);
  free(data->Delta_xW_v);
  free(data->Delta_yn_v);
  free(data->Delta_yN_v);
  free(data->Delta_ys_v);
  free(data->Delta_yS_v);
  free(data->Delta_zt_v);
  free(data->Delta_zb_v);
  free(data->Delta_zT_v);
  free(data->Delta_zB_v);
  free(data->Delta_x_v);
  free(data->Delta_y_v);
  free(data->Delta_z_v);
  free(data->vertex_y_v);

  free(data->nodes_x_w); 
  free(data->nodes_y_w);
  free(data->nodes_z_w);
  free(data->Delta_xe_w);
  free(data->Delta_xE_w);
  free(data->Delta_xw_w);
  free(data->Delta_xW_w);
  free(data->Delta_yn_w);
  free(data->Delta_yN_w);
  free(data->Delta_ys_w);
  free(data->Delta_yS_w);
  free(data->Delta_zt_w);
  free(data->Delta_zb_w);
  free(data->Delta_zT_w);
  free(data->Delta_zB_w);
  free(data->Delta_x_w);
  free(data->Delta_y_w);
  free(data->Delta_z_w);
  free(data->vertex_z_w);
  
  free(data->epsilon_x);
  free(data->epsilon_y);
  free(data->epsilon_z);
  free(data->epsilon_x_sol);
  free(data->epsilon_y_sol);
  free(data->epsilon_z_sol);
  free(data->v_fraction);
  free(data->v_fraction_x);
  free(data->v_fraction_y);
  free(data->v_fraction_z);
  free(data->cut_matrix);
  free(data->domain_matrix);
  
  free(data->P_solve_phase_OK);
  free(data->E_solve_phase_OK);
  free(data->W_solve_phase_OK);
  free(data->N_solve_phase_OK);
  free(data->S_solve_phase_OK);
  free(data->T_solve_phase_OK);
  free(data->B_solve_phase_OK);

  free(data->E_is_fluid_OK);
  free(data->W_is_fluid_OK);
  free(data->N_is_fluid_OK);
  free(data->S_is_fluid_OK);
  free(data->T_is_fluid_OK);
  free(data->B_is_fluid_OK);

  free(data->case_s_type_x);
  free(data->case_s_type_y);
  free(data->case_s_type_z);
  
  free(data->norm_vec_x_x);
  free(data->norm_vec_x_y);
  free(data->norm_vec_x_z);
  free(data->norm_vec_y_x);
  free(data->norm_vec_y_y);
  free(data->norm_vec_y_z);
  free(data->norm_vec_z_x);
  free(data->norm_vec_z_y);
  free(data->norm_vec_z_z);
  free(data->par_vec_x_x);
  free(data->par_vec_x_y);
  free(data->par_vec_x_z);
  free(data->par_vec_y_x);
  free(data->par_vec_y_y);
  free(data->par_vec_y_z);
  free(data->par_vec_z_x);
  free(data->par_vec_z_y);
  free(data->par_vec_z_z);

  if(data->solve_3D_OK){
    free(data->par_vec_x_x_2);
    free(data->par_vec_x_y_2);
    free(data->par_vec_x_z_2);
    free(data->par_vec_y_x_2);
    free(data->par_vec_y_y_2);
    free(data->par_vec_y_z_2);
    free(data->par_vec_z_x_2);
    free(data->par_vec_z_y_2);
    free(data->par_vec_z_z_2);
  }
  
  free(data->norm_x);
  free(data->norm_y);
  free(data->norm_z);
  free(data->par_x);
  free(data->par_y);
  free(data->par_z);

  if(data->solve_3D_OK){
    free(data->par_x_2);
    free(data->par_y_2);
    free(data->par_z_2);
  } 

  free(data->matrix_c);
  free(data->R_x);
  free(data->R_y);
  free(data->R_z);
  free(data->Rot_);
  free(data->Trans_);
  free(data->Rot_T);
  free(data->Trans_T);
  free(data->matrix_A);
  free(data->vector_a);

  free(data->phi);
  free(data->phi0);

  free(data->u);
  free(data->u0);
  
  free(data->v);
  free(data->v0);

  free(data->w);
  free(data->w0);

  free(data->u_ps);
  free(data->v_ps);
  free(data->w_ps);
  
  free(data->u_at_faces);
  free(data->v_at_faces);
  free(data->w_at_faces);

  free(data->u_at_faces_y);
  free(data->u_at_faces_z);
    
  free(data->v_at_faces_x);
  free(data->v_at_faces_z);

  free(data->w_at_faces_x);
  free(data->w_at_faces_y);

  free(data->u_at_corners);
  free(data->v_at_corners);
  free(data->w_at_corners);
  
  free(data->U);
  free(data->V);
  free(data->W);
  
  free(data->p);
  free(data->p_corr);

  free(data->u_aP_SIMPLEC);
  free(data->v_aP_SIMPLEC);
  free(data->w_aP_SIMPLEC);
  
  free(data->rho_m);
  free(data->rho_v);
  free(data->cp_m);
  free(data->cp_v);
  free(data->gamma_m);
  free(data->gamma_v);
  free(data->SC_vol_m_0);
  free(data->SC_vol_m);

  free(data->dphidxb);
  free(data->dphidyb);
  free(data->dphidzb);

  free(data->dphidxs);
  free(data->dphidys);
  free(data->dphidzs);
  
  free(data->J_x);
  free(data->J_y);
  free(data->J_z);
  
  free(data->J_norm);
  free(data->J_par);
  free(data->J_par_2);
  
  free(data->solve_this_phase_OK);
  free(data->phi0_v);
  
  if (data->lines > 0){
    free(data->line_points);
  }
  if (data->curves > 0){
    free(data->Isoflux_OK);
    free(data->phi_b_v);

    free(data->quadric_OK);

    free(data->curve_is_solid_OK);
    free(data->lambda_1);
    free(data->lambda_2);
    free(data->lambda_3);
    free(data->angle_x);
    free(data->angle_y);
    free(data->angle_z);
    free(data->tcompx);
    free(data->tcompy);
    free(data->tcompz);
    free(data->constant_d);
    free(data->dphidNb_v);
    free(data->poly_coeffs);
    
    free(data->sf_boundary_solid_indexs);
    free(data->I_bm);
    free(data->J_bm);
    free(data->K_bm);
  
    free(data->Dirichlet_boundary_indexs);
    free(data->I_Dirichlet_v);
    free(data->J_Dirichlet_v);
    free(data->K_Dirichlet_v);
    
    free(data->Isoflux_boundary_indexs);
    free(data->I_Isoflux_v);
    free(data->J_Isoflux_v);
    free(data->K_Isoflux_v);
    
    free(data->Conjugate_boundary_indexs);
    free(data->I_Conjugate_v);
    free(data->J_Conjugate_v);
    free(data->K_Conjugate_v);
    
    free(data->rho_l);
    free(data->cp_l);
    free(data->gamma_l);
    free(data->curve_phase_change_OK);
    free(data->T_phase_change);
    free(data->dT_phase_change);
    free(data->T_ref);
    free(data->dH_S_L);

    if(data->solid_boundary_cell_count){
      free(data->nxnx_x);
      free(data->nxny_x);
      free(data->nyny_y);
      free(data->nxny_y);
    }
   
    if(data->u_sol_factor_count > 0){
      free(data->u_sol_factor_index_u);
      free(data->u_sol_factor_index);
      free(data->u_sol_factor_i);
      free(data->u_sol_factor_J);
      free(data->u_sol_factor_K);
      free(data->u_sol_factor);
      free(data->u_p_factor);
      free(data->u_factor_delta_h);
      free(data->u_factor_area);
      free(data->u_factor_inters_x);
      free(data->u_factor_inters_y);
      free(data->u_factor_inters_z);
      free(data->u_p_factor_W);
      
      free(data->u_sol_type);
      
      if(data->post_process_OK){

	
	if(data->solve_3D_OK){
	  free(data->u_sol_factor_x);
	  free(data->u_sol_factor_y);
	  free(data->u_sol_factor_z);
	  free(data->u_sol_factor_u);
	  free(data->u_sol_factor_F);
	  free(data->u_sol_factor_tau);
	  free(data->u_sol_factor_p);
	  free(data->u_sol_factor_F_p);
	}
      }
    }
    
    if(data->v_sol_factor_count > 0){
      free(data->v_sol_factor_index_v);
      free(data->v_sol_factor_index);
      free(data->v_sol_factor_I);
      free(data->v_sol_factor_j);
      free(data->v_sol_factor_K);
      free(data->v_sol_factor);
      free(data->v_p_factor);
      free(data->v_factor_delta_h);
      free(data->v_factor_area);
      free(data->v_factor_inters_x);
      free(data->v_factor_inters_y);
      free(data->v_factor_inters_z);
      free(data->v_p_factor_S);

      free(data->v_sol_type);
      
      if(data->post_process_OK){

	
	if(data->solve_3D_OK){
	  free(data->v_sol_factor_x);
	  free(data->v_sol_factor_y);
	  free(data->v_sol_factor_z);
	  free(data->v_sol_factor_v);
	  free(data->v_sol_factor_F);
	  free(data->v_sol_factor_tau);
	  free(data->v_sol_factor_p);
	  free(data->v_sol_factor_F_p);
	}
      }
    }

    if(data->w_sol_factor_count > 0){
      free(data->w_sol_factor_index_w);
      free(data->w_sol_factor_index);
      free(data->w_sol_factor_I);
      free(data->w_sol_factor_J);
      free(data->w_sol_factor_k);
      free(data->w_sol_factor);
      free(data->w_p_factor);
      free(data->w_factor_delta_h);
      free(data->w_factor_area);
      free(data->w_factor_inters_x);
      free(data->w_factor_inters_y);
      free(data->w_factor_inters_z);
      free(data->w_p_factor_B);

      free(data->w_sol_type);
      
      if(data->post_process_OK){

	free(data->w_sol_factor_x);
	free(data->w_sol_factor_y);
	free(data->w_sol_factor_z);
	free(data->w_sol_factor_w);
	free(data->w_sol_factor_F);
	free(data->w_sol_factor_tau);
	free(data->w_sol_factor_p);
	free(data->w_sol_factor_F_p);
      }
    }

    if(data->u_cut_face_Fy_count > 0){
      free(data->u_cut_face_Fy_W);
      free(data->u_cut_face_Fy_E);
      free(data->u_cut_face_Fy_index);
      free(data->u_cut_face_Fy_index_v);
    }

    if(data->u_cut_face_Fz_count > 0){
      free(data->u_cut_face_Fz_W);
      free(data->u_cut_face_Fz_E);
      free(data->u_cut_face_Fz_index);
      free(data->u_cut_face_Fz_index_w);
    }    
    
    if(data->v_cut_face_Fx_count > 0){
      free(data->v_cut_face_Fx_S);
      free(data->v_cut_face_Fx_N);
      free(data->v_cut_face_Fx_index);
      free(data->v_cut_face_Fx_index_u);
    }

    if(data->v_cut_face_Fz_count > 0){
      free(data->v_cut_face_Fz_S);
      free(data->v_cut_face_Fz_N);
      free(data->v_cut_face_Fz_index);
      free(data->v_cut_face_Fz_index_w);
    }

    if(data->w_cut_face_Fx_count > 0){
      free(data->w_cut_face_Fx_B);
      free(data->w_cut_face_Fx_T);
      free(data->w_cut_face_Fx_index);
      free(data->w_cut_face_Fx_index_u);
    }

    if(data->w_cut_face_Fy_count > 0){
      free(data->w_cut_face_Fy_B);
      free(data->w_cut_face_Fy_T);
      free(data->w_cut_face_Fy_index);
      free(data->w_cut_face_Fy_index_v);
    }    

    if(data->diff_u_face_x_count > 0){
      free(data->diff_u_face_x_index);
      free(data->diff_u_face_x_index_u);
      free(data->diff_u_face_x_I);
      free(data->diff_u_face_x_Delta);
      free(data->diff_factor_u_face_x);
    }

    if(data->diff_v_face_y_count > 0){
      free(data->diff_v_face_y_index);
      free(data->diff_v_face_y_index_v);
      free(data->diff_v_face_y_J);
      free(data->diff_v_face_y_Delta);
      free(data->diff_factor_v_face_y);
    }

    if(data->diff_w_face_z_count > 0){
      free(data->diff_w_face_z_index);
      free(data->diff_w_face_z_index_w);
      free(data->diff_w_face_z_K);
      free(data->diff_w_face_z_Delta);
      free(data->diff_factor_w_face_z);
    }

  }

  free(data->faces_x);
  free(data->faces_y);
  free(data->faces_z);
  free(data->p_faces_x);
  free(data->p_faces_y);
  free(data->p_faces_z);  
  free(data->u_faces_x);
  free(data->u_faces_y);
  free(data->u_faces_z);
  free(data->v_faces_x);
  free(data->v_faces_y);
  free(data->v_faces_z);
  free(data->w_faces_x);
  free(data->w_faces_y);
  free(data->w_faces_z);
  free(data->p_cut_face_x);
  free(data->p_cut_face_y);
  free(data->p_cut_face_z);
  free(data->u_cut_face_x);
  free(data->u_cut_face_y);
  free(data->u_cut_face_z);
  free(data->v_cut_face_x);
  free(data->v_cut_face_y);
  free(data->v_cut_face_z);
  free(data->w_cut_face_x);
  free(data->w_cut_face_y);
  free(data->w_cut_face_z);

  free(data->alpha_u_x);
  free(data->alpha_u_y);
  free(data->alpha_u_z);
  free(data->alpha_v_x);
  free(data->alpha_v_y);
  free(data->alpha_v_z);
  free(data->alpha_w_x);
  free(data->alpha_w_y);
  free(data->alpha_w_z);

  free(data->Du_x);
  free(data->Du_y);
  free(data->Du_z);
  free(data->Dv_x);
  free(data->Dv_y);
  free(data->Dv_z);
  free(data->Dw_x);
  free(data->Dw_y);
  free(data->Dw_z);
  free(data->Dk_x);
  free(data->Dk_y);
  free(data->Dk_z);
  free(data->Deps_x);
  free(data->Deps_y);
  free(data->Deps_z);
  free(data->Dphi_x);
  free(data->Dphi_y);
  free(data->Dphi_z);

  free(data->Fu_x);
  free(data->Fu_y);
  free(data->Fu_z);
  free(data->Fv_x);
  free(data->Fv_y);
  free(data->Fv_z);
  free(data->Fw_x);
  free(data->Fw_y);
  free(data->Fw_z);
  free(data->Fk_x);
  free(data->Fk_y);
  free(data->Fk_z);
  free(data->Feps_x);
  free(data->Feps_y);
  free(data->Feps_z);
  free(data->Fphi_x);
  free(data->Fphi_y);
  free(data->Fphi_z);

  free(data->e_J);
  free(data->e_K);
  free(data->w_J);
  free(data->w_K);
  free(data->n_I);
  free(data->n_K);
  free(data->s_I);
  free(data->s_K);
  free(data->b_I);
  free(data->b_J);
  free(data->t_I);
  free(data->t_J);

  free(data->u_dom_matrix);
  free(data->v_dom_matrix);
  free(data->w_dom_matrix);
  free(data->p_dom_matrix);
  free(data->u_cut_matrix);
  free(data->v_cut_matrix);
  free(data->w_cut_matrix);

  if(data->u_slave_count > 0){
    free(data->u_slave_nodes);
    free(data->u_slave_i);
    free(data->u_slave_J);
    if(data->solve_3D_OK){
      free(data->u_slave_K);
    }
    //    free(data->u_master_nodes);
    free(data->u_slave_at_E_of_p_OK);
  }

  if(data->v_slave_count > 0){
    free(data->v_slave_nodes);
    free(data->v_slave_I);
    free(data->v_slave_j);
    if(data->solve_3D_OK){
      free(data->v_slave_K);
    }
    //    free(data->v_master_nodes);
    free(data->v_slave_at_N_of_p_OK);
  }

  if(data->w_slave_count > 0){
    free(data->w_slave_nodes);
    free(data->w_slave_I);
    free(data->w_slave_J);
    free(data->w_slave_k);
    free(data->w_slave_at_T_of_p_OK);
  }

  free(data->gL);
  free(data->enthalpy_diff);
  free(data->gL_guess);
  free(data->gL_calc);
  free(data->phase_change_vol);

  free(data->y_wall);

  free(data->nabla_u);
  free(data->def_tens);
  free(data->tau_turb);
  
  free(data->P_k);
  free(data->P_k_lag);
  free(data->mu_turb);
  free(data->mu_turb_lag);
  free(data->f_1);
  free(data->f_2);
  free(data->f_mu);

  free(data->k_turb);
  free(data->k_turb0);
  free(data->eps_turb);
  free(data->eps_turb0);
  free(data->gamma);

  free(data->S_CG_eps);
  free(data->S_CG_eps_lag);
  
  free(data->d2Uidxjdxk2);

  if(data->turb_model){
    free(data->turb_boundary_indexs);
    free(data->turb_boundary_I);
    free(data->turb_boundary_J);
    free(data->turb_boundary_K);
    free(data->depsdxb);
    free(data->depsdyb);
    free(data->SC_vol_k_turb);
    free(data->SC_vol_eps_turb);
    free(data->SC_vol_k_turb_lag);
    free(data->SC_vol_eps_turb_lag);
    free(data->eps_bnd_x);
    free(data->eps_bnd_y);
    free(data->y_plus);
    free(data->turb_bnd_excl_indexs);
    if(data->solve_3D_OK){
      free(data->depsdzb);
      free(data->eps_bnd_z);
    }
  }

  free(data->fe_u);
  free(data->fw_u);
  free(data->fn_u);
  free(data->fs_u);
  free(data->ft_u);
  free(data->fb_u);
  
  free(data->fe_v);
  free(data->fw_v);
  free(data->fn_v);
  free(data->fs_v);
  free(data->ft_v);
  free(data->fb_v);

  free(data->fe_w);
  free(data->fw_w);
  free(data->fn_w);
  free(data->fs_w);
  free(data->ft_w);
  free(data->fb_w);
  
  free(data->u_E_non_uni);
  free(data->u_W_non_uni);
  free(data->v_S_non_uni);
  free(data->v_N_non_uni);
  free(data->w_B_non_uni);
  free(data->w_T_non_uni);
  free(data->k_E_non_uni);
  free(data->k_W_non_uni);
  free(data->k_N_non_uni);
  free(data->k_S_non_uni);
  free(data->k_B_non_uni);
  free(data->k_T_non_uni);
  free(data->eps_E_non_uni);
  free(data->eps_W_non_uni);
  free(data->eps_N_non_uni);
  free(data->eps_S_non_uni);
  free(data->eps_B_non_uni);
  free(data->eps_T_non_uni);
  free(data->p_E_non_uni);
  free(data->p_W_non_uni);
  free(data->p_S_non_uni);
  free(data->p_N_non_uni);
  free(data->p_B_non_uni);
  free(data->p_T_non_uni);
  free(data->u_cut_fw);
  free(data->u_cut_fe);
  free(data->v_cut_fs);
  free(data->v_cut_fn);
  free(data->w_cut_fb);
  free(data->w_cut_ft);

  free(data->cdE);
  free(data->cdW);
  free(data->cdN);
  free(data->cdS);
  free(data->cdT);
  free(data->cdB);
  
  free(data->cudE);
  free(data->cudW);
  free(data->cudN);
  free(data->cudS);
  free(data->cudT);
  free(data->cudB);
  
  free(data->cvdE);
  free(data->cvdW);
  free(data->cvdN);
  free(data->cvdS);
  free(data->cvdT);
  free(data->cvdB);

  free(data->cwdE);
  free(data->cwdW);
  free(data->cwdN);
  free(data->cwdS);
  free(data->cwdT);
  free(data->cwdB);
  
  free(data->mu_turb_at_u_nodes);
  free(data->mu_turb_at_v_nodes);
  free(data->mu_turb_at_w_nodes);
  if(data->p_nodes_in_solid_count > 0){
    free(data->p_nodes_in_solid_indx);
  }
  
  free(data->phase_change_matrix);

  free(data->W_coeff);
  free(data->E_coeff);
  free(data->N_coeff);
  free(data->S_coeff);
  free(data->T_coeff);
  free(data->B_coeff);

  return;

}

void free_grid_info(Grid_Info *bi, const int solve_3D_OK){

  free(bi->per);
  free(bi->N);
  
  if(solve_3D_OK){
    /* deltas array not set for z_side
       if 3rd dimension was ignored */
    free(bi->deltas);
    free(bi->L);
  }
  
  return;
}

void free_coeffs(Coeffs *bi){

  free(bi->aP);
  free(bi->b);
  free(bi->aE);
  free(bi->aW);
  free(bi->aN);
  free(bi->aS);
  free(bi->aT);
  free(bi->aB);
  
  return;
}

void free_bc_info(Boundary_Info *bi){

  if(bi->parts > 0){
    free(bi->per);
    
    free(bi->perx);
    free(bi->pery);
    free(bi->perz);
	
    free(bi->alpha);
    free(bi->beta);
    free(bi->gamma);
    free(bi->phi_b);
  }

  return;
}

/* CREATE_ARRAY */
double * create_array(const int Nx, const int Ny, const int Nz, const double number){

  double *A_ = NULL;

  if(number == 0.0){
    A_ = (double *) calloc(Nx*Ny*Nz, sizeof(double));
    verify_alloc(A_);
  }
  else{
    A_ = (double *) malloc(Nx*Ny*Nz * sizeof(double));
    verify_alloc(A_);      
    fill_array(A_, Nx, Ny, Nz, number);
  }

  /* Increase the global variable for memory count */
  count_double = count_double + Nx*Ny*Nz;
    
  return A_;
}

/* VERIFY_ALLOC */
void verify_alloc(const double *ptr){
  if(ptr == NULL){
    //    error_msg(wins, "\nCould not allocate memory!!\n");
    exit(EXIT_FAILURE);
  }
  return;
}

/* FILL_ARRAY */
void fill_array(double *A_, const int Nx, const int Ny, const int Nz, const double number){
  
  int I, J, K, index_;

#pragma omp parallel for default(none) private(I, J, K, index_) shared(A_)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	A_[index_] = number;
      }
    }
  }
  return;
}

/* PRINT_ARRAY */
void print_array(const double *A_, const int Nx, const int Ny, const char msg[]){
  int i, j, index_;
  struct winsize w;
  
  printf("\n%s\n\n", msg);
  ioctl(0, TIOCGWINSZ, &w);
  if ((Ny >= w.ws_col) && (Nx >= w.ws_row)){  
    for(i = 0; i < w.ws_col; i++){
      for(j = 0; j < w.ws_row; j++){
	index_ = Nx*j + i;
	printf("%7.3f\t", A_[index_]);
      }   
      printf("\n");
    }
    printf("\n");
  }
  else{
    for(i = 0; i < Nx; i++){
      for(j = 0; j < Ny; j++){
	index_ = Nx*j + i;
	printf("%7.3f\t", A_[index_]);
      }
      printf("\n");
    }
  }
  
  return;
}

/* FILL_PHYS_PROP */
/*****************************************************************************/
/**

   Sets:

   Array (double, NxNyNz) Data_Mem::rho_m.

   Array (double, NxNyNz) Data_Mem::cp_m.

   Array (double, NxNyNz) Data_Mem::gamma_m.

   @param data

   @return void

   MODIFIED: 17-07-2020
 */
void fill_phys_prop(Data_Mem *data){
  
  int I, J, K;
  int index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int *dom_mat = data->domain_matrix;
  
  double *rho_m = data->rho_m;
  double *cp_m = data->cp_m;
  double *gamma_m = data->gamma_m;
  
  const double *rho_v = data->rho_v;
  const double *cp_v = data->cp_v;
  const double *gamma_v = data->gamma_v;

#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(rho_m, cp_m, gamma_m, rho_v, cp_v, gamma_v, dom_mat)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
    
	index_ = K*Nx*Ny + J*Nx + I ;
      
	rho_m[index_] = rho_v[ dom_mat[index_] ];
	cp_m[index_] = cp_v[ dom_mat[index_] ];
	gamma_m[index_] = gamma_v[ dom_mat[index_] ];
	  
      }
    }
  }
  
  return;
}

/* WRITE_CUT_CELL_DATA */
/*****************************************************************************/
/**
  
  Saves cut_cell data in cut_cell.mat
  Requires libmatio-devel >= 1.5.12.
  
  @param data
  
  @return void.
    
  MODIFIED: 11-09-2019

  Data written:
*/
void write_cut_cell_data(Data_Mem *data){

  char filename[SIZE];

  Wins *wins = &(data->wins);

  mat_t *matfp = NULL;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int solve_3D_OK = data->solve_3D_OK;
  
  snprintf(filename, SIZE, "%s%s", 
	   data->results_dir, "cut_cell.mat");
  
  matfp = get_matfp(filename, 1, wins);

  /** Data_Mem::u_dom_matrix, Data_Mem::v_dom_matrix, Data_Mem::p_dom_matrix. */
  checkCall(write_int_array_in_MAT_file(data->u_dom_matrix, "u_dom_matrix", nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->v_dom_matrix, "v_dom_matrix", Nx, ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->p_dom_matrix, "p_dom_matrix", Nx, Ny, Nz, matfp), wins);
  /** Data_Mem::u_cut_matrix, Data_Mem::v_cut_matrix. */
  checkCall(write_int_array_in_MAT_file(data->u_cut_matrix, "u_cut_matrix", nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->v_cut_matrix, "v_cut_matrix", Nx, ny, Nz, matfp), wins);
  /** Data_Mem::p_cut_face_x, Data_Mem::p_cut_face_y. */
  checkCall(write_int_array_in_MAT_file(data->p_cut_face_x, "p_cut_face_x", nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->p_cut_face_y, "p_cut_face_y", Nx, ny, Nz, matfp), wins);
  /** Data_Mem::u_cut_face_x, Data_Mem::u_cut_face_y. */
  checkCall(write_int_array_in_MAT_file(data->u_cut_face_x, "u_cut_face_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->u_cut_face_y, "u_cut_face_y", nx, ny, Nz, matfp), wins);
  /** Data_Mem::v_cut_face_y, Data_Mem::v_cut_face_x. */
  checkCall(write_int_array_in_MAT_file(data->v_cut_face_y, "v_cut_face_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_int_array_in_MAT_file(data->v_cut_face_x, "v_cut_face_x", nx, ny, Nz, matfp), wins);

  /** if Data_Mem::solve_3D_OK: */
  if(solve_3D_OK){
    /** Data_Mem::w_dom_matrix, Data_Mem::w_cut_matrix. */
    checkCall(write_int_array_in_MAT_file(data->w_dom_matrix, "w_dom_matrix", Nx, Ny, nz, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->w_cut_matrix, "w_cut_matrix", Nx, Ny, nz, matfp), wins);
    /** Data_Mem::p_cut_face_z, Data_Mem::u_cut_face_z, Data_Mem::v_cut_face_z. */
    checkCall(write_int_array_in_MAT_file(data->p_cut_face_z, "p_cut_face_z", Nx, Ny, nz, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->u_cut_face_z, "u_cut_face_z", nx, Ny, nz, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->v_cut_face_z, "v_cut_face_z", Nx, ny, nz, matfp), wins);
    /** Data_Mem::w_cut_face_x, Data_Mem::w_cut_face_y, Data_Mem::w_cut_face_z. */
    checkCall(write_int_array_in_MAT_file(data->w_cut_face_x, "w_cut_face_x", nx, Ny, nz, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->w_cut_face_y, "w_cut_face_y", Nx, ny, nz, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->w_cut_face_z, "w_cut_face_z", Nx, Ny, Nz, matfp), wins);
  }
  /** end if */

  checkCall(write_array_in_MAT_file(data->u, "u", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v, "v", Nx, ny, Nz, matfp), wins);
  /** Data_Mem::u_faces_x, Data_Mem::u_faces_y. */
  checkCall(write_array_in_MAT_file(data->u_faces_x, "u_faces_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->u_faces_y, "u_faces_y", nx, ny, Nz, matfp), wins);
  /** Data_Mem::v_faces_x, Data_Mem::v_faces_y. */
  checkCall(write_array_in_MAT_file(data->v_faces_x, "v_faces_x", nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_faces_y, "v_faces_y", Nx, Ny, Nz, matfp), wins);
  /** Data_Mem::p_faces_x, Data_Mem::p_faces_y. */
  checkCall(write_array_in_MAT_file(data->p_faces_x, "p_faces_x", nx, Ny, Nz, matfp), wins);  
  checkCall(write_array_in_MAT_file(data->p_faces_y, "p_faces_y", Nx, ny, Nz, matfp), wins);
  /** Data_Mem::nodes_x_u, Data_Mem::nodes_y_u. */
  checkCall(write_array_in_MAT_file(data->nodes_x_u, "nodes_x_u", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->nodes_y_u, "nodes_y_u", nx, Ny, Nz, matfp), wins);
  /** Data_Mem::nodes_x_v, Data_Mem::nodes_y_v. */
  checkCall(write_array_in_MAT_file(data->nodes_x_v, "nodes_x_v", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->nodes_y_v, "nodes_y_v", Nx, ny, Nz, matfp), wins);

  /** Data_Mem::vf_x, Data_Mem::vf_y. */
  checkCall(write_array_in_MAT_file(data->vf_x, "vf_x", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->vf_y, "vf_y", Nx, ny, Nz, matfp), wins);

  /** if Data_Mem::solve_3D_OK: */
  if(solve_3D_OK){
    /** Data_Mem::w_cut_cell. */
    checkCall(write_array_in_MAT_file(data->w, "w", Nx, Ny, nz, matfp), wins);
    /** Data_Mem::u_faces_z, Data_Mem::v_faces_z, Data_Mem::p_faces_z. */
    checkCall(write_array_in_MAT_file(data->u_faces_z, "u_faces_z", nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->v_faces_z, "v_faces_z", Nx, ny, nz, matfp), wins);    
    checkCall(write_array_in_MAT_file(data->p_faces_z, "p_faces_z", Nx, Ny, nz, matfp), wins);
    /** Data_Mem::w_faces_x, Data_Mem::w_faces_y, Data_Mem::w_faces_z. */
    checkCall(write_array_in_MAT_file(data->w_faces_x, "w_faces_x", nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->w_faces_y, "w_faces_y", Nx, ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->w_faces_z, "w_faces_z", Nx, Ny, Nz, matfp), wins);
    /** Data_Mem::nodes_z_u, Data_Mem::nodes_z_v. */
    checkCall(write_array_in_MAT_file(data->nodes_z_u, "nodes_z_u", nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nodes_z_v, "nodes_z_v", Nx, ny, Nz, matfp), wins);
    /** Data_Mem::nodes_x_w, Data_Mem::nodes_y_w, Data_Mem::nodes_z_w. */
    checkCall(write_array_in_MAT_file(data->nodes_x_w, "nodes_x_w", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nodes_y_w, "nodes_y_w", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nodes_z_w, "nodes_z_w", Nx, Ny, nz, matfp), wins);
    /** Data_Mem::vf_z. */
    checkCall(write_array_in_MAT_file(data->vf_z, "vf_z", Nx, Ny, nz, matfp), wins);
  }
  /** end if */
  
  
  if(data->turb_boundary_cell_count > 0){
    /** Data_Mem::y_plus (if Data_Mem::turb_boundary_cell_count > 0) */
    checkCall(write_array_in_MAT_file(data->y_plus, "y_plus", data->turb_boundary_cell_count, 1, 1, matfp), wins);
  }
  
  /*
    if(data->turb_boundary_cell_count > 0){
    checkCall(write_int_array_in_MAT_file(data->turb_boundary_indexs, "turb_bound_indexs", data->turb_boundary_cell_count, 1, 1, matfp), wins);
    
    checkCall(write_array_in_MAT_file(data->y_plus, "y_plus", data->turb_boundary_cell_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->nabla_u, "nabla_u", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->tau_turb, "tau_turb", Nx, Ny, Nz, matfp), wins);
    }
  
    if(data->turb_model){  
    checkCall(write_array_in_MAT_file(data->y_wall, "y_wall", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->P_k, "P_k", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->d2Uidxjdxk2, "d2Uidxjdxk2", Nx, Ny, Nz, matfp), wins);
    }
  */

  checkCall(write_array_in_MAT_file(data->Du_x, "Du_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Du_y, "Du_y", nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Dv_x, "Dv_x", nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Dv_y, "Dv_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Fu_x, "Fu_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Fu_y, "Fu_y", nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Fv_x, "Fv_x", nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Fv_y, "Fv_y", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->alpha_u_x, "alpha_u_x", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->alpha_u_y, "alpha_u_y", nx, ny, Nz, matfp), wins);

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->Du_z, "Du_z", nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Dv_z, "Dv_z", Nx, ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Dw_x, "Dw_x", nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Dw_y, "Dw_y", Nx, ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Dw_z, "Dw_z", Nx, Ny, Nz, matfp), wins);

    checkCall(write_array_in_MAT_file(data->Fu_y, "Fu_z", nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Fv_z, "Fv_z", Nx, ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Fw_x, "Fw_x", nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Fw_y, "Fw_y", Nx, ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->Fw_z, "Fw_z", Nx, Ny, Nz, matfp), wins);
  }

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->alpha_u_z, "alpha_u_z", nx, Ny, nz, matfp), wins);
  }
  
  checkCall(write_array_in_MAT_file(data->alpha_v_x, "alpha_v_x", nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->alpha_v_y, "alpha_v_y", Nx, Ny, Nz, matfp), wins);

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->alpha_v_z, "alpha_v_z", Nx, ny, nz, matfp), wins);
  }

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->alpha_w_x, "alpha_w_x", nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->alpha_w_y, "alpha_w_y", Nx, ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->alpha_w_z, "alpha_w_z", Nx, Ny, Nz, matfp), wins);
  }

  checkCall(write_array_in_MAT_file(data->u_cut_fw, "u_cut_fw", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->u_cut_fe, "u_cut_fe", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_cut_fs, "v_cut_fs", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_cut_fn, "v_cut_fn", Nx, Ny, Nz, matfp), wins);

  if(data->diff_u_face_x_count > 0){
    checkCall(write_array_in_MAT_file(data->diff_factor_u_face_x, "diff_factor_u_face_x", data->diff_u_face_x_count, 1, 1, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->diff_u_face_x_index_u, "diff_u_face_x_index_u", data->diff_u_face_x_count, 1, 1, matfp), wins);
  }
  
  if(data->diff_v_face_y_count > 0){
    checkCall(write_array_in_MAT_file(data->diff_factor_v_face_y, "diff_factor_v_face_y", data->diff_v_face_y_count, 1, 1, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->diff_v_face_y_index_v, "diff_v_face_y_index_v", data->diff_v_face_y_count, 1, 1, matfp), wins);
  }

  if(data->diff_w_face_z_count > 0){
    checkCall(write_array_in_MAT_file(data->diff_factor_w_face_z, "diff_factor_w_face_z", data->diff_w_face_z_count, 1, 1, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->diff_w_face_z_index_w, "diff_w_face_z_index_w", data->diff_w_face_z_count, 1, 1, matfp), wins);
  }
  
  checkCall(write_array_in_MAT_file(data->Delta_xW_u, "xW_u", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->Delta_yN_u, "yN_u", nx, Ny, Nz, matfp), wins);

  if(data->u_slave_count > 0){
    checkCall(write_int_array_in_MAT_file(data->u_slave_nodes, "u_slave_nodes", data->u_slave_count, 1, 1, matfp), wins);
  }
  if(data->v_slave_count > 0){
    checkCall(write_int_array_in_MAT_file(data->v_slave_nodes, "v_slave_nodes", data->v_slave_count, 1, 1, matfp), wins);
  }

  if(data->u_sol_factor_count > 0){
    checkCall(write_array_in_MAT_file(data->u_sol_factor, "u_sol_factor", data->u_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->u_factor_area, "u_factor_area", data->u_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->u_p_factor, "u_p_factor", data->u_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->u_p_factor_W, "u_p_factor_W", data->u_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->u_sol_factor_index_u, "u_sol_factor_index_u", data->u_sol_factor_count, 1, 1, matfp), wins); 
  }
  if(data->v_sol_factor_count > 0){
    checkCall(write_array_in_MAT_file(data->v_sol_factor, "v_sol_factor", data->v_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->v_factor_area, "v_factor_area", data->v_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->v_p_factor, "v_p_factor", data->v_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->v_p_factor_S, "v_p_factor_S", data->v_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->v_sol_factor_index_v, "v_sol_factor_index_v", data->v_sol_factor_count, 1, 1, matfp), wins);
  }

  if(solve_3D_OK && (data->w_sol_factor_count > 0)){
    checkCall(write_array_in_MAT_file(data->w_sol_factor, "w_sol_factor", data->w_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->w_factor_area, "w_factor_area", data->w_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->w_p_factor, "w_p_factor", data->w_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->w_p_factor_B, "w_p_factor_B", data->w_sol_factor_count, 1, 1, matfp), wins);
    checkCall(write_int_array_in_MAT_file(data->w_sol_factor_index_w, "w_sol_factor_index_w", data->w_sol_factor_count, 1, 1, matfp), wins);
  }  

  if(data->turb_boundary_cell_count > 0){

    checkCall(write_int_array_in_MAT_file(data->turb_bnd_excl_indexs, "turb_bnd_excl_indexs", data->turb_boundary_cell_count, 1, 1, matfp), wins);

    checkCall(write_array_in_MAT_file(data->eps_bnd_x, "eps_bnd_x", data->turb_boundary_cell_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->eps_bnd_y, "eps_bnd_y", data->turb_boundary_cell_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->SC_vol_eps_turb, "SC_vol_eps_turb", data->turb_boundary_cell_count, 1, 1, matfp), wins);
    checkCall(write_array_in_MAT_file(data->SC_vol_k_turb, "SC_vol_k_turb", data->turb_boundary_cell_count, 1, 1, matfp), wins);
  }

  if(data->turb_model){  
    checkCall(write_array_in_MAT_file(data->f_mu, "f_mu", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->f_2, "f_2", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->S_CG_eps, "S_CG_eps", Nx, Ny, Nz, matfp), wins);
  }

  if(data->p_nodes_in_solid_count > 0){
    checkCall(write_int_array_in_MAT_file(data->p_nodes_in_solid_indx, "p_nodes_in_solid_indx", data->p_nodes_in_solid_count, 1, 1, matfp), wins);
  }

  checkCall(write_array_in_MAT_file(data->u_E_non_uni, "u_E_non_uni", 1, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->u_W_non_uni, "u_W_non_uni", 1, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_N_non_uni, "v_N_non_uni", Nx, 1, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->v_S_non_uni, "v_S_non_uni", Nx, 1, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->vol_x, "vol_x", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->vol_y, "vol_y", nx, Ny, Nz, matfp), wins);

  checkCall(write_array_in_MAT_file(data->mu_turb_at_u_nodes, "mu_turb_at_u_nodes", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->mu_turb_at_v_nodes, "mu_turb_at_v_nodes", Nx, ny, Nz, matfp), wins);
  
  Mat_Close(matfp);
  info_msg(wins, "Cut cell data successfully written");
 
  return;
}

/* WRITE_COEFFS */
/*****************************************************************************/
/**
  
  Saves coeffs data in coeffs.mat
  Requires libmatio-devel >= 1.5.12.
  
  @param data
  
  @return void.
    
  MODIFIED: 16-09-2019
*/
void write_coeffs(Data_Mem *data){

  char filename[SIZE];
  
  mat_t *matfp = NULL;

  Wins *wins = &(data->wins);

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int solve_3D_OK = data->solve_3D_OK;

  snprintf(filename, SIZE, "%s%s", data->results_dir, "coeffs.mat");
  
  matfp = get_matfp(filename, 1, wins);

  checkCall(write_array_in_MAT_file(data->coeffs_phi.aP, "phi_aP", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_phi.aE, "phi_aE", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_phi.aW, "phi_aW", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_phi.aN, "phi_aN", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_phi.aS, "phi_aS", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_phi.b, "phi_b", Nx, Ny, Nz, matfp), wins);

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->coeffs_phi.aB, "phi_aB", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_phi.aT, "phi_aT", Nx, Ny, Nz, matfp), wins);
  }
  
  checkCall(write_array_in_MAT_file(data->coeffs_u.aP, "u_aP", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_u.aE, "u_aE", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_u.aW, "u_aW", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_u.aN, "u_aN", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_u.aS, "u_aS", nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_u.b, "u_b", nx, Ny, Nz, matfp), wins);

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->coeffs_u.aB, "u_aB", nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_u.aT, "u_aT", nx, Ny, Nz, matfp), wins);
  }  
  
  checkCall(write_array_in_MAT_file(data->coeffs_v.aP, "v_aP", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_v.aE, "v_aE", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_v.aW, "v_aW", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_v.aN, "v_aN", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_v.aS, "v_aS", Nx, ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_v.b, "v_b", Nx, ny, Nz, matfp), wins);

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->coeffs_v.aB, "v_aB", Nx, ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_v.aT, "v_aT", Nx, ny, Nz, matfp), wins);
  }    
  
  checkCall(write_array_in_MAT_file(data->coeffs_p.aP, "p_aP", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_p.aE, "p_aE", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_p.aW, "p_aW", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_p.aN, "p_aN", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_p.aS, "p_aS", Nx, Ny, Nz, matfp), wins);
  checkCall(write_array_in_MAT_file(data->coeffs_p.b, "p_b", Nx, Ny, Nz, matfp), wins);

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->coeffs_p.aB, "p_aB", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_p.aT, "p_aT", Nx, Ny, Nz, matfp), wins);
  }

  if(solve_3D_OK){
    checkCall(write_array_in_MAT_file(data->coeffs_w.aP, "w_aP", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_w.aE, "w_aE", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_w.aW, "w_aW", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_w.aN, "w_aN", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_w.aS, "w_aS", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_w.b, "w_b", Nx, Ny, nz, matfp), wins);    
    checkCall(write_array_in_MAT_file(data->coeffs_w.aB, "w_aB", Nx, Ny, nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_w.aT, "w_aT", Nx, Ny, nz, matfp), wins);
  }    
  
  if(data->turb_model){  

    checkCall(write_array_in_MAT_file(data->coeffs_k.aP, "k_aP", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_k.aE, "k_aE", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_k.aW, "k_aW", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_k.aN, "k_aN", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_k.aS, "k_aS", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_k.b, "k_b", Nx, Ny, Nz, matfp), wins);
    
    checkCall(write_array_in_MAT_file(data->coeffs_eps.aP, "eps_aP", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_eps.aE, "eps_aE", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_eps.aW, "eps_aW", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_eps.aN, "eps_aN", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_eps.aS, "eps_aS", Nx, Ny, Nz, matfp), wins);
    checkCall(write_array_in_MAT_file(data->coeffs_eps.b, "eps_b", Nx, Ny, Nz, matfp), wins);
  }

  Mat_Close(matfp);
  info_msg(wins, "Coeffs successfully written");

  return;
}

/* CENTER_VEL_COMPONENTS */
/*****************************************************************************/
/**

  Center the cut_cell velocity components at the middle point of the p_faces_x,y,z

  @return void
  
  MODIFIED: 15-05-2021
*/
void center_vel_components(Data_Mem *data){
  
  if(data->solve_3D_OK){
    _center_vel_components_3D(data);
  }
  else{
    _center_vel_components(data);
  }

  data->centered_vel_comp_OK = 1;
  
  return;
}

/* _CENTER_VEL_COMPONENTS */
/*****************************************************************************/
/**

  Center the cut_cell velocity components at the middle point of the p_faces_x,y using
  an area scaling. The result is stored in u,v_at_faces arrays.
  
  Centered velocity values satisfy:

  v_center * total_area = v_cut_cell * cut_area

  @return void
  
  MODIFIED: 11-06-2019
*/
void _center_vel_components(Data_Mem *data){

  int i, j, I, J;
  int index_, index_u, index_v;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  /* Flags indicating cut face presence */
  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;

  double *um = data->u_at_faces;
  double *vm = data->v_at_faces;
 
  const double *u = data->u;
  const double *v = data->v;
  
  const double *Delta_y = data->Delta_y;
  const double *Delta_x = data->Delta_x;
  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;

  /* u velocity component */
#pragma omp parallel for default(none)				\
  private(i, J, I, index_, index_u)				\
  shared(p_cut_face_x, p_faces_x, Delta_y, um, u)
  for(J=0; J<Ny; J++){
    for(i=0; i<nx; i++){

      I = i+1;

      index_ = J*Nx + I;
      index_u = J*nx + i;

      if(p_cut_face_x[index_u] != 0){

	um[index_u] = u[index_u] * (p_faces_x[index_u] / Delta_y[index_]);
	
      }
      else {
	
	um[index_u] = u[index_u];
	
      }
    }
  }

  /* v velocity component */
#pragma omp parallel for default(none) private(I, j, J, index_, index_v) \
  shared(p_cut_face_y, p_faces_y, Delta_x, vm, v)
  for(j=0; j<ny; j++){
    for(I=0; I<Nx; I++){

      J = j+1;
	
      index_ = J*Nx + I;
      index_v = j*Nx + I;

      if(p_cut_face_y[index_v] != 0){
 
	vm[index_v] = v[index_v] * (p_faces_y[index_v] / Delta_x[index_]);
	
      }

      else {

	vm[index_v] = v[index_v];
	
      }
    }
  }
  
  return;
  
}

/* _CENTER_VEL_COMPONENTS_3D */
/*****************************************************************************/
/**
  
  Sets velocity components (u_at_faces, v_at_faces, w_at_faces) 
  at the center of their natural faces. 

  u_at_faces: Natural position of x component velocity exists in all domain.
  Similar analysis for the other velocity components.
    
  @return void.
    
  MODIFIED: 04-08-2019
*/
void _center_vel_components_3D(Data_Mem *data){


  int i, j, k, I, J, K, a;
  int index_, index_u, index_v, index_w, curv_ind;
  int curve_factor = 1;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  /* These matrices indicate if the main grid faces are cut */
  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;
  const int *p_cut_face_z = data->p_cut_face_z;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double F = 0;
  
  /* Interpolation parameter */
  double tbeta = 0;
  /* Coordinates of displaced velocity component */
  double xalpha[3];
  /* Center of face coordinate */
  double xM[3];
  /* Intersection in curve of line passing through xalpha ==> xM */
  double xbeta[3];
  
  /* Vector from xalpha to xM */
  double P_vect[3];
  double pc_temp[16];

  const double pol_tol = data->pol_tol;

  double *u_at_faces = data->u_at_faces;
  double *v_at_faces = data->v_at_faces;
  double *w_at_faces = data->w_at_faces;

  /* cut cell velocities */
  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;

  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;

  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;

  const double *nodes_x_w = data->nodes_x_w;
  const double *nodes_y_w = data->nodes_y_w;
  const double *nodes_z_w = data->nodes_z_w;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  
  const double *poly_coeffs = data->poly_coeffs;

  /* Putting the velocities at the center of their respective faces */
  copy_array(u, u_at_faces, nx, Ny, Nz);
  copy_array(v, v_at_faces, Nx, ny, Nz);
  copy_array(w, w_at_faces, Nx, Ny, nz);
  
  /* u velocity component */
#pragma omp parallel for default(none)					\
  private(a, i, I, J, K, index_, index_u, curv_ind, curve_factor,	\
	  xalpha, xM, xbeta, F, P_vect, tbeta, pc_temp)			\
  shared(p_cut_face_x, Delta_x, Delta_y, Delta_z,			\
	 u, u_at_faces,							\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 nodes_x_u, nodes_y_u, nodes_z_u)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(i=0; i<nx; i++){
	
	I = i+1;

	index_ = K*Ny*Nx + J*Nx + I;
	index_u = K*Ny*nx + J*nx + i;

	/* See if face is cut */
	if(p_cut_face_x[index_u] != 0){

	  /* Getting the correct curve */
	  curv_ind = p_cut_face_x[index_u] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }

	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	  xM[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	  xM[1] = nodes_y[index_];
	  xM[2] = nodes_z[index_];

	  /* Evaluating at the midpoint if we are in the solid */
	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    /* xM within solid phase */
	    u_at_faces[index_u] = 0;
	  }
	  else{
	    /* Interpolation needed */

	    xalpha[0] = nodes_x_u[index_u];
	    xalpha[1] = nodes_y_u[index_u];
	    xalpha[2] = nodes_z_u[index_u];

	    /* Get distance betwen xM and xalpha */
	    if(vec_dist(xM, xalpha) <= pol_tol){
	      /* They are essentially the same point */
	      u_at_faces[index_u] = u[index_u];
	    }
	    else{

	      /* Vector is made outwardly normal to surface */
	      P_vect[0] = xalpha[0] - xM[0];
	      P_vect[1] = xalpha[1] - xM[1];
	      P_vect[2] = xalpha[2] - xM[2];

	      /* Solve for xbeta starting from xM */
	      int_curv(xbeta, xM, P_vect, pc_temp,
		       MAX(Delta_y[index_], Delta_z[index_]));

	      /* Now compute tbeta parameter */
	      tbeta = (xbeta[1] - xalpha[1]) / (xM[1] - xalpha[1]);
	      
	      if(tbeta <= 1 || isnan(tbeta)){
		tbeta = (xbeta[2] - xalpha[2]) / (xM[2] - xalpha[2]);
	      }

	      /* Linear interpolation along P_vect to get velocity at x face midpoint */
	      u_at_faces[index_u] = u[index_u]*(1 - 1/tbeta);
	      /* Last resource */
	      if(isnan(u_at_faces[index_u])){u_at_faces[index_u] = u[index_u];}
	      
	    }// if(vect_dist(xM, xalpha) <= pol_tol)
	  } // if(F <= pol_tol)
	} // if(p_cut_face_x[index_u] != 0)
	else{
	  /* No face cut so it is the same value */
	  u_at_faces[index_u] = u[index_u];
	}
      }
    }
  }

  /* v velocity component */
#pragma omp parallel for default(none)					\
  private(a, j, I, J, K, index_, index_v, curv_ind, curve_factor,	\
	  xalpha, xM, xbeta, F, P_vect, tbeta, pc_temp)			\
  shared(p_cut_face_y, Delta_x, Delta_y, Delta_z,			\
	 v, v_at_faces,							\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 nodes_x_v, nodes_y_v, nodes_z_v)
  for(K=0; K<Nz; K++){
    for(j=0; j<ny; j++){
      for(I=0; I<Nx; I++){

	J = j+1;
	
	index_ = K*Ny*Nx + J*Nx + I;
	index_v = K*ny*Nx + j*Nx + I;

	if(p_cut_face_y[index_v] != 0){

	  curv_ind = p_cut_face_y[index_v] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }

	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	  xM[0] = nodes_x[index_];
	  xM[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	  xM[2] = nodes_z[index_];

	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    v_at_faces[index_v] = 0;
	  }
	  else{
	    xalpha[0] = nodes_x_v[index_v];
	    xalpha[1] = nodes_y_v[index_v];
	    xalpha[2] = nodes_z_v[index_v];

	    if(vec_dist(xM, xalpha) <= pol_tol){
	      v_at_faces[index_v] = v[index_v];
	    }
	    else{
	      
	      P_vect[0] = xalpha[0] - xM[0];
	      P_vect[1] = xalpha[1] - xM[1];
	      P_vect[2] = xalpha[2] - xM[2];
	      
	      int_curv(xbeta, xM, P_vect, pc_temp,
		       MAX(Delta_x[index_], Delta_z[index_]));

	      tbeta = (xbeta[0] - xalpha[0]) / (xM[0] - xalpha[0]);
	      
	      if(tbeta <= 1 || isnan(tbeta)){
		tbeta = (xbeta[2] - xalpha[2]) / (xM[2] - xalpha[2]);
	      }
	      
	      v_at_faces[index_v] = v[index_v]*(1 - 1/tbeta);
	      
	      if(isnan(v_at_faces[index_v])){v_at_faces[index_v] = v[index_v];}
	      
	    } // if(vec_dist(xM, xalpha) <= pol_tol)
	  } // if(F <= pol_tol)
	} // if(p_cut_face_y[index_v] != 0)
	else{
	  /* No face cut so it is the same value */
	  v_at_faces[index_v] = v[index_v];
	}
      }
    }
  }

  /* w velocity component */
#pragma omp parallel for default(none)					\
  private(a, k, I, J, K, index_, index_w, curv_ind, curve_factor,	\
	  xalpha, xM, xbeta, F, P_vect, tbeta, pc_temp)			\
  shared(p_cut_face_z, Delta_x, Delta_y, Delta_z,			\
	 w, w_at_faces,							\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 nodes_x_w, nodes_y_w, nodes_z_w)
  for(k=0; k<nz; k++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	K = k+1;

	index_ = K*Ny*Nx + J*Nx + I;
	index_w = k*Ny*Nx + J*Nx + I;

	if(p_cut_face_z[index_w] != 0){

	  curv_ind = p_cut_face_z[index_w] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }

	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

	  xM[0] = nodes_x[index_];
	  xM[1] = nodes_y[index_];
	  xM[2] = nodes_z[index_] - 0.5*Delta_z[index_];

	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    w_at_faces[index_w] = 0;
	  }
	  else{

	    xalpha[0] = nodes_x_w[index_w];
	    xalpha[1] = nodes_y_w[index_w];
	    xalpha[2] = nodes_z_w[index_w];

	    if(vec_dist(xM, xalpha) <= pol_tol){
	      w_at_faces[index_w] = w[index_w];
	    }
	    else{
	
	      P_vect[0] = xalpha[0] - xM[0];
	      P_vect[1] = xalpha[1] - xM[1];
	      P_vect[2] = xalpha[2] - xM[2];

	      int_curv(xbeta, xM, P_vect, pc_temp,
		       MAX(Delta_x[index_], Delta_y[index_]));

	      tbeta = (xbeta[0] - xalpha[0]) / (xM[0] - xalpha[0]);
	      
	      if(tbeta <= 1 || isnan(tbeta)){
		tbeta = (xbeta[1] - xalpha[1]) / (xM[1] - xalpha[1]);
	      }

	      w_at_faces[index_w] = w[index_w]*(1 - 1/tbeta);

	      if(isnan(w_at_faces[index_w])){w_at_faces[index_w] = w[index_w];}
	      
	    }// if(vect_dist(xM, xalpha) <= pol_tol)
	  } // if(F <= pol_tol)
	} // if(p_cut_face_z[index_w] != 0)
	else{
	  /* No face cut so it is the same value */
	  w_at_faces[index_w] = w[index_w];
	}
      }
    }
  }

  /* Now we have all velocities centered at their normal faces */
  /* Must compute at crossed faces */
  return;
}

/* COMPUTE_VEL_AT_MAIN_CELLS */
/*****************************************************************************/
/**

  Computes velocities at main grid points (U, V, W) starting from centered at 
  uncut faces velocities (u_at_faces, v_at_faces, w_at_faces).
  
  @param data

  @return void
  
  MODIFIED: 15-05-2021
*/
void compute_vel_at_main_cells(Data_Mem *data){
  
  if(data->solve_3D_OK){
    _compute_vel_at_main_cells_3D(data);
  }
  else{
    _compute_vel_at_main_cells(data);
  }

  return;
}

/* _COMPUTE_VEL_AT_MAIN_CELLS */
/*****************************************************************************/
/**

  Computes velocities at main grid points.
  
  @param data

  @return void
  
  MODIFIED: 06-03-2019
*/
void _compute_vel_at_main_cells(Data_Mem *data){
	
  int I, J, i, j;
  int index_, index_u, index_v;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int nx = data->nx;
  const int ny = data->ny;
  
  double ue, uw, vn, vs;
  
  double *U = data->U;
  double *V = data->V;
  
  const double *um = data->u_at_faces;
  const double *vm = data->v_at_faces;
  
#pragma omp parallel for default(none)				\
  private(I, J, i, j, index_, index_u, index_v, ue, uw, vn, vs) \
  shared(um, vm, U, V)
  for (J=1; J<Ny-1; J++){

    j = J-1;
	  
    for (I=1; I<Nx-1; I++){

      i = I-1;

      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;
      
      ue = um[index_u + 1];
      uw = um[index_u];
      
      vn = vm[index_v + Nx];
      vs = vm[index_v];

      U[index_] = 0.5 * (ue + uw);
      V[index_] = 0.5 * (vn + vs);

    }
  }





#pragma omp parallel sections default(none)			\
  private(I, J, i, j, index_, index_u, index_v, ue, uw, vn, vs) \
  shared(um, vm, U, V)
  {
#pragma omp section
    {      
      // W
      I = 0;
      i = 0;
      for (J=1; J<Ny-1; J++){
	
	j = J-1;
	
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	
	uw = um[index_u];
	
	vn = vm[index_v + Nx];
	vs = vm[index_v];
	
	U[index_] = uw;
	V[index_] = 0.5 * (vn + vs);
	
      }
    }
      
#pragma omp section
    {
      // E
      I = Nx-1;
      i = nx-1;
      for (J=1; J<Ny-1; J++){
	
	j = J-1;
	
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	
	ue = um[index_u];
	
	vn = vm[index_v + Nx];
	vs = vm[index_v];
	
	U[index_] = ue;
	V[index_] = 0.5 * (vn + vs);
	
      }
    }
#pragma omp section
    {
      // S
      J = 0;
      j = 0;
      
      for (I=1; I<Nx-1; I++){
	
	i = I-1;
	
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	
	ue = um[index_u + 1];
	uw = um[index_u];
	
	vs = vm[index_v];
	
	U[index_] = 0.5 * (ue + uw);
	V[index_] = vs;
	
      }
    }
#pragma omp section
    {
      // N
      J = Ny-1;
      j = ny-1;
      
      for (I=1; I<Nx-1; I++){
	
	i = I-1;
	
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	
	ue = um[index_u + 1];
	uw = um[index_u];
	
	vn = vm[index_v];
	
	U[index_] = 0.5 * (ue + uw);
	V[index_] = vn;
	
      }
    }
    
  }
  
  return;
}

/* _COMPUTE_VEL_AT_MAIN_CELLS_3D */
/*****************************************************************************/
/**
  
  Computes velocities at main grid points (U,V,W) starting from centered at 
  natural faces velocities (u,v,w)_at_faces.

  @return void
  
  MODIFIED: 06-03-2019
*/
void _compute_vel_at_main_cells_3D(Data_Mem *data){

  int I, J, K, i, j, k;
  int index_, index_u, index_v, index_w;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;

  const int *dom_matrix = data->domain_matrix;
  
  double ue, uw, vn, vs, wt, wb;
  
  double *U = data->U;
  double *V = data->V;
  double *W = data->W;

  const double *um = data->u_at_faces;
  const double *vm = data->v_at_faces;
  const double *wm = data->w_at_faces;
  
#pragma omp parallel for default(none)				\
  private(I, J, K, i, j, k, index_, index_u, index_v, index_w,	\
          ue, uw, vn, vs, wt, wb)				\
  shared(um, vm, wm, U, V, W, dom_matrix)
  for (K=0; K<Nz; K++){
    K == 0 ? k = 0 : k = K-1;
    
    for (J=0; J<Ny; J++){
      J == 0 ? j = 0 : j = J-1;
      
      for (I=0; I<Nx; I++){
	I == 0 ? i = 0 : i = I-1;

	index_ = K*Ny*Nx + J*Nx + I;

	if(dom_matrix[index_] == 0){
	
	  index_u = K*Ny*nx + J*nx + i;
	  index_v = K*ny*Nx + j*Nx + I;
	  index_w = k*Ny*Nx + J*Nx + I;

	  uw = um[index_u];
	  (I == 0 || I == Nx-1) ? ue = uw : ue = um[index_u + 1];

	  vs = vm[index_v];
	  (J == 0 || J == Ny-1) ? vn = vs : vn = vm[index_v + Nx];

	  wb = wm[index_w];
	  (K == 0 || K == Nz-1) ? wt = wb : wt = wm[index_w + Ny*Nx];
	  
	  U[index_] = 0.5 * (ue + uw);
	  V[index_] = 0.5 * (vn + vs);
	  W[index_] = 0.5 * (wt + wb);
	}
	else{
	  U[index_] = 0;
	  V[index_] = 0;
	  W[index_] = 0;
	}

      }
    }
  }
  
  return;
}

/* COMPUTE_GL_VALUE */
/*****************************************************************************/
/**
  Sets:

  Array (double, NxNy) gL_to_calculate: returns gL values corresponding to 
  current Data_Mem::phi values.

  @return void
  
  MODIFIED: 14-02-2019
*/
void compute_gL_value(Data_Mem *data, double *gL_to_calculate){
	
  int I, J, index_;
  int phase_index;
	
  const int Nx = data->Nx;
  const int Ny = data->Ny;
	
  const int *phase_change_matrix = data->phase_change_matrix;
  const int *dom_matrix = data->domain_matrix;

  const double *phi = data->phi;
  const double *T_phase_change = data->T_phase_change;
  const double *dT_phase_change = data->dT_phase_change;

  
#pragma omp parallel for default(none) private(J, I, index_, phase_index) \
  shared(phase_change_matrix, gL_to_calculate, phi,			\
	 T_phase_change, dT_phase_change, dom_matrix)

  for(J=1; J<(Ny-1); J++){
    for(I=1; I<(Nx-1); I++){

      index_ = J*Nx+I;
      phase_index = dom_matrix[index_];

      if(phase_change_matrix[index_]){
	gL_to_calculate[index_] = gL_function(phi[index_],
					      T_phase_change[phase_index-1],
					      dT_phase_change[phase_index-1]);
      }
      else{
	gL_to_calculate[index_] = 0;
      }
    }
  }
  
  return;	
}

/* GL_FUNCTION */
double gL_function(const double T, const double TPC, const double dTPC){

  if(T >= (TPC + 0.5*dTPC)){
    return 1;
  }
  else if(T <= (TPC - 0.5*dTPC)){
    return 0;
  }
  else{
    return (T - (TPC - 0.5*dTPC)) / dTPC;
  }
}

/* AVG_PHYS_PROP */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) gamma_m: conductivity matrix values, volume average
  for phase change materials.

  Array (double, NxNy) cp_m: heat capacity matrix values, volume average
  for phase change materials.

  Array (double, NxNy) rho_m: density matrix values, volume average
  for phase change materials.

  @param data

  @return void
  
  MODIFIED: 14-02-2019
*/
void avg_phys_prop(Data_Mem *data){

  int I, J, index_ ;
  int phase_index;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *domain_matrix = data->domain_matrix;
  const int *phase_change_matrix = data->phase_change_matrix;

  double *gamma_m = data->gamma_m;
  double *cp_m = data->cp_m;
  double *rho_m = data->rho_m;

  const double *gL = data->gL_guess;
  const double *rho_v = data->rho_v;
  const double *rho_l = data->rho_l;
  const double *cp_v = data->cp_v;
  const double *cp_l = data->cp_l;
  const double *gamma_v = data->gamma_v;
  const double *gamma_l = data->gamma_l;


#pragma omp parallel for default(none) private(J, I, index_, phase_index) \
  shared(domain_matrix, gamma_m, gamma_l, gamma_v, gL, cp_m,		\
	 cp_l, cp_v, rho_m, rho_l, rho_v, phase_change_matrix)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      phase_index = domain_matrix[index_];

      if(phase_change_matrix[index_]){
	  
	/* EC 6 Voller */
	gamma_m[index_] = gL[index_] * gamma_l[phase_index-1] 
	  + (1 - gL[index_]) * gamma_v[phase_index];
	cp_m[index_] = gL[index_] * cp_l[phase_index-1] 
	  + (1 - gL[index_]) * cp_v[phase_index];
	rho_m[index_] = gL[index_] * rho_l[phase_index-1] 
	  + (1 - gL[index_]) * rho_v[phase_index];
      }
    }
  }

  return;
}

/* AVG_PHYS_PROP_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) gamma_m: conductivity matrix values, volume average
  for phase change materials.

  Array (double, NxNy) cp_m: heat capacity matrix values, volume average
  for phase change materials.

  Array (double, NxNy) rho_m: density matrix values, volume average
  for phase change materials.

  @param data

  @return void
  
  MODIFIED: 14-02-2019
*/
void avg_phys_prop_3D(Data_Mem *data){

  int I, J, K, index_ ;
  int phase_index;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *domain_matrix = data->domain_matrix;
  const int *phase_change_matrix = data->phase_change_matrix;

  double *gamma_m = data->gamma_m;
  double *cp_m = data->cp_m;
  double *rho_m = data->rho_m;

  const double *gL = data->gL_guess;
  const double *rho_v = data->rho_v;
  const double *rho_l = data->rho_l;
  const double *cp_v = data->cp_v;
  const double *cp_l = data->cp_l;
  const double *gamma_v = data->gamma_v;
  const double *gamma_l = data->gamma_l;


#pragma omp parallel for default(none)					\
  private(J, I, K, index_, phase_index)					\
  shared(domain_matrix, gamma_m, gamma_l, gamma_v, gL, cp_m,		\
	 cp_l, cp_v, rho_m, rho_l, rho_v, phase_change_matrix)
  
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;
	
	phase_index = domain_matrix[index_];
	
	if(phase_change_matrix[index_]){
	  
	  /* EC 6 Voller */
	  gamma_m[index_] = gL[index_] * gamma_l[phase_index-1] 
	    + (1 - gL[index_]) * gamma_v[phase_index];
	  cp_m[index_] = gL[index_] * cp_l[phase_index-1] 
	    + (1 - gL[index_]) * cp_v[phase_index];
	  rho_m[index_] = gL[index_] * rho_l[phase_index-1] 
	    + (1 - gL[index_]) * rho_v[phase_index];
	}
      }
    }
  }
  
  return;
}

/* COMPUTE_ENTHALPY_DIFF */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) enthalpy_diff: enthalpy difference for source term calculation.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 14-02-2019
*/
void compute_enthalpy_diff(Data_Mem *data){

  int I, J, index_, phase_index;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *domain_matrix = data->domain_matrix;
  const int *phase_change_matrix = data->phase_change_matrix;

  double *enthalpy_diff = data->enthalpy_diff;

  const double *phi = data->phi;
  const double *T_ref = data->T_ref;
  const double *dH_S_L = data->dH_S_L;
  const double *cp_v = data->cp_v;
  const double *cp_l = data->cp_l;
  const double *rho_v = data->rho_v;
  const double *rho_l = data->rho_l;
  

#pragma omp parallel for default(none) private(J, I, index_, phase_index) \
  shared(enthalpy_diff, phi, T_ref, phase_change_matrix,		\
	 rho_l, cp_l, rho_v, cp_v, dH_S_L, domain_matrix)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      phase_index = domain_matrix[index_];

      if(phase_change_matrix[index_]){
	/* EC 11 Voller */
	enthalpy_diff[index_] = (phi[index_] - T_ref[phase_index-1]) * 
	  (rho_l[phase_index-1] * cp_l[phase_index-1] -
	   rho_v[phase_index] * cp_v[phase_index]) +
	  rho_l[phase_index-1] * dH_S_L[phase_index-1];
      }
      
    }
  }

  return;
}

/* MAX_ABS_MATRIX_DIFF */
double max_abs_matrix_diff(const double *A, const double *B,
			   const int Nx, const int Ny){

  int I, J, index_;
  double max_val = 0;

#pragma omp parallel for default(none) private(J, I, index_)	\
  shared(A, B) reduction(max: max_val)
  for(J=0; J<Ny; J++){
    for(I=0; I<Nx; I++){
      index_ = J*Nx + I;
      max_val = MAX(max_val, fabs(A[index_] - B[index_]));
    }
  }
  return max_val;
}

/* MAX_ABS_MATRIX_DIFF_3D */
double max_abs_matrix_diff_3D(const double *A, const double *B,
			      const int Nx, const int Ny, const int Nz){

  int I, J, K, index_;
  double max_val = 0;

#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(A, B) reduction(max: max_val)
  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;
	
	max_val = MAX(max_val, fabs(A[index_] - B[index_]));
      }
    }
  }
  return max_val;
}

/* UPDATE_GL_GUESS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) gL_guess: Update gL_guess using gL_calc values,
  applying relaxation.

  @param data

  @return void
  
  MODIFIED: 15-02-2019
*/
void update_gL_guess(Data_Mem *data){

  int I, J, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *phase_change_matrix = data->phase_change_matrix;

  double *gL_guess = data->gL_guess;
  const double alpha_gL = data->alpha_gL;
  const double *gL_calc = data->gL_calc;

  
#pragma omp parallel for default(none) private(J, I, index_)	\
  shared(gL_guess, gL_calc, phase_change_matrix)

  for(J=0; J<Ny; J++){
    for(I=0; I<Nx; I++){

      index_ = J*Nx + I;

      if(phase_change_matrix[index_]){
	gL_guess[index_] = gL_calc[index_] * alpha_gL + gL_guess[index_] * (1 - alpha_gL);
      }
    }
  }

  return;
}

/* PHASE_CHANGE_CELLS_VOL */
void phase_change_cells_vol(Data_Mem *data){

  int I, J, index_;
  int curv_ind;
  int flag_int0, flag_int1;
  
  const int curves = data->curves;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  const int *cut_matrix = data->cut_matrix;
  const int *curve_is_solid_OK = data->curve_is_solid_OK;
  const int *phase_change_matrix = data->phase_change_matrix;

  double diff_x, diff_y;
  double f0, f1;
  double curve_factor;
  double x0[3] = {0};
  double x1[3] = {0};
  double norm_v[3] = {0};
  double x_int0[3] = {0};
  double x_int1[3] = {0};
  double delta0[2] = {0};
  double delta1[2] = {0};
  double pc_temp[16];
  
  double *phase_change_vol = data->phase_change_vol;
  
  const double cell_size = data->cell_size;
  const double pol_tol = data->pol_tol;
  const double *poly_coeffs = data->poly_coeffs;
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *vol = data->vol_uncut;

  for(curv_ind=0; curv_ind<curves; curv_ind++){
    for(I=0; I<16; I++){
      pc_temp[I] = poly_coeffs[curv_ind*16 + I];
    }

    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) firstprivate(delta0, delta1, norm_v, x0, x1, x_int0, x_int1) \
  private(I, J, index_, flag_int0, flag_int1, f0, f1, diff_x, diff_y)	\
  shared(curv_ind, curve_factor, cut_matrix, phase_change_matrix,	\
	 nodes_x, nodes_y, Delta_x, Delta_y, pc_temp, phase_change_vol, vol)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	flag_int0 = 0;
	flag_int1 = 0;
	index_ = J*Nx + I;

	if(phase_change_matrix[index_]){
	  if(cut_matrix[index_] == curv_ind + 1){
	    delta0[0] = 0;
	    delta1[0] = 0;
	    delta0[1] = 0;
	    delta1[1] = 0;
	    //West side
	    norm_v[0] = 0;
	    x0[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	    x0[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	    x1[0] = x0[0];
	    x1[1] = nodes_y[index_] + 0.5*Delta_y[index_];
	    // South west point
	    f0 = polyn(x0, pc_temp) * curve_factor;
	    // North west point
	    f1 = polyn(x1, pc_temp) * curve_factor;
	    if((fabs(f0) <= pol_tol) || (fabs(f1) <= pol_tol)){
	      if(fabs(f0) <= pol_tol){
		x_int0[0] = x0[0];
		x_int0[1] = x0[1];
		flag_int0 = 1;
	      }
	      if(fabs(f1) <= pol_tol){
		if(flag_int0){
		  x_int1[0] = x1[0];
		  x_int1[1] = x1[1];
		  flag_int1 = 1;
		}
		else{
		  x_int0[0] = x1[0];
		  x_int0[1] = x1[1];
		  flag_int0 = 1;
		}
	      }
	    }
	    else if(f0*f1 < 0){
	      flag_int0 = 1;
	      if(f0 < 0){
		norm_v[1] = 1;
		int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		delta0[1] = x_int0[1] - x0[1];
	      }
	      else{
		norm_v[1] = -1;
		int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		delta0[1] = x1[1] - x_int0[1];
	      }
	    }
	      
	    //North side
	    norm_v[1] = 0;
	    x0[0] = nodes_x[index_] + 0.5*Delta_x[index_];
	    x0[1] = x1[1];
	    //North east point
	    f0 = polyn(x0, pc_temp) * curve_factor;
	    if((fabs(f0) <= pol_tol) || (fabs(f1) <= pol_tol)){
	      if(fabs(f0) <= pol_tol){
		if(flag_int0){
		  x_int1[0] = x0[0];
		  x_int1[1] = x0[1];
		  flag_int1 = 1;
		}
		else{
		  x_int0[0] = x0[0];
		  x_int0[1] = x0[1];
		  flag_int0 = 1;
		}
	      }
	    }
	    else if(f0*f1 < 0){
	      if(f0 < 0){
		norm_v[0] = -1;
		if(flag_int0){
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  delta1[0] = x0[0] - x_int1[0];
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  delta0[0] = x0[0] - x_int0[0];
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[0] = 1;
		if(flag_int0){
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  delta1[0] = x_int1[0] - x1[0];
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  delta0[0] = x_int0[0] - x1[0];
		  flag_int0 = 1;
		}
	      }
	    }
	    //East side
	    norm_v[0] = 0;
	    x1[0] = x0[0];
	    x1[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	    //South east
	    f1 = polyn(x1, pc_temp) * curve_factor;
	    if((fabs(f0) <= pol_tol) || (fabs(f1) <= pol_tol)){
	      if(fabs(f1) <= pol_tol){
		if(flag_int0){
		  x_int1[0] = x1[0];
		  x_int1[1] = x1[1];
		  flag_int1 = 1;
		}
		else{
		  x_int0[0] = x1[0];
		  x_int0[1] = x1[1];
		  flag_int0 = 1;
		}
	      }
	    }
	    else if(f0*f1 < 0){
	      if(f0 < 0){
		norm_v[1] = -1;
		if(flag_int0){
		  int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		  delta1[1] = x0[1] - x_int1[1];
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		  delta0[1] = x0[1] - x_int0[1];
		  flag_int0 = 1;
		}
	      }
	      else{
		norm_v[1] = 1;
		if(flag_int0){
		  int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		  delta1[1] = x_int1[1] - x1[1];
		  flag_int1 = 1;
		}
		else{
		  int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		  delta0[1] = x_int0[1] - x1[1];
		  flag_int0 = 1;
		}
	      }
	    }
	    //South side
	    norm_v[1] = 0;
	    x0[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	    x0[1] = x1[1];
	    // South west
	    f0 = polyn(x0, pc_temp) * curve_factor;
	    if((fabs(f0) > pol_tol) && (fabs(f1) > pol_tol)){
	      if(f0*f1 < 0){
		if(f0 < 0){
		  norm_v[0] = 1;
		  if(flag_int0){
		    int_curv(x_int1, x1, norm_v, pc_temp, cell_size);
		    delta1[0] = x_int1[0] - x0[0];
		    flag_int1 = 1;
		  }
		  else{
		    int_curv(x_int0, x1, norm_v, pc_temp, cell_size);
		    delta0[0] = x_int0[1] - x0[0];
		    flag_int0 = 1;
		  }
		}
		else{
		  norm_v[0] = -1;
		  if(flag_int0){
		    int_curv(x_int1, x0, norm_v, pc_temp, cell_size);
		    delta1[0] = x1[0] - x_int1[0];
		    flag_int1 = 1;
		  }
		  else{
		    int_curv(x_int0, x0, norm_v, pc_temp, cell_size);
		    delta0[0] = x1[0] - x_int0[0];
		    //flag_int0 = 1; Una sola interseccion al final del algoritmo no resta volumen
		  }
		}
	      }
	    }
	    if(flag_int0 && flag_int1){
	      diff_x = fabs(x_int0[0] - x_int1[0]);
	      diff_y = fabs(x_int0[1] - x_int1[1]);
	      if(diff_x > diff_y){
		phase_change_vol[index_] = vol[index_] - diff_x * 0.5 * (delta0[1] + delta1[1]);
	      }
	      else{
		phase_change_vol[index_] = vol[index_] - diff_y * 0.5 * (delta0[0] + delta1[0]);
	      }
	    }
	    else{
	      phase_change_vol[index_] = vol[index_];
	    }
	  }
	  else{
	    phase_change_vol[index_] = vol[index_];
	  }
	}//	  if(phase_change_matrix[index_])
      }//      for(I=1; I<Nx-1; I++)
    }//     for(J=1; J<Ny-1; J++)
  }

  return;
}

/* CENTER_VEL_FOR_SERVER */
void center_vel_for_server(Data_Mem *data){

  data->sdata.u = data->u_at_faces;
  data->sdata.v = data->v_at_faces;
  data->sdata.w = data->w_at_faces;

  data->sdata.U = data->U;
  data->sdata.V = data->V;
  data->sdata.W = data->W;

  center_vel_components(data);
  compute_vel_at_main_cells(data);
  
  return;
}

/* ADD_GAMMA_TURB */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) gamma_m: add turbulent contribution to gamma_m values

  @param data

  @return void
  
  MODIFIED: 24-02-2019
*/
void add_gamma_turb(Data_Mem *data){
  
  int I, J;
  int index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const double Pr_t = 0.85; //Valor provisional

  double *gamma_m = data->gamma_m;

  const double *cp_m = data->cp_m;
  const double *mu_turb = data->mu_turb;  

#pragma omp parallel for default(none)			\
  private(I, J, index_) shared(gamma_m, cp_m, mu_turb)  

  for(J=0; J<Ny; J++){
    for(I=0; I<Nx; I++){
      index_ = J*Nx + I;
      gamma_m[index_] = gamma_m[index_] + cp_m[index_] * mu_turb[index_] / Pr_t;
    }
  }
  
  return;
}

/* ADD_GAMMA_TURB_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) gamma_m: add turbulent contribution to gamma_m values

  @param data

  @return void
  
  MODIFIED: 24-02-2019
*/
void add_gamma_turb_3D(Data_Mem *data){
  
  int I, J, K;
  int index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const double Pr_t = 0.85; //Valor provisional

  double *gamma_m = data->gamma_m;

  const double *cp_m = data->cp_m;
  const double *mu_turb = data->mu_turb;  

#pragma omp parallel for default(none)			\
  private(I, J, K, index_) shared(gamma_m, cp_m, mu_turb)  

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	gamma_m[index_] = gamma_m[index_] + cp_m[index_] * mu_turb[index_] / Pr_t;
      }
    }
  }
  
  return;
}

/* ROW_ADDITION */
/*****************************************************************************/
/** 
    Returns matrix with row row_B replaced by row_B + row_A*alpha 
*/
void row_addition(double *matrix, const int row_A, const int row_B, const double alpha, const int mat_size){

  int i, index_A, index_B;

  for(i=0; i<mat_size; i++){
    index_A = i * mat_size + row_A;
    index_B = i * mat_size + row_B;
    matrix[index_B] = matrix[index_B] + matrix[index_A] * alpha;
  }

  return;
}

/* FLIP_ROW */
/*****************************************************************************/
/** 

    Flips rows row_A and row_B from matrix.

    @return 
    0 if succesful\n
    -1 otherwise 
*/
int flip_row(double *matrix, const int row_A, const int row_B, const int mat_size){ 
  
  int i, index_A, index_B;
  int ret_value = 0;

  double *temp_row = NULL;

  temp_row = (double *)malloc(mat_size * sizeof(double));

  if(temp_row == NULL){
    ret_value = -1;
  }
  else{
    
    for(i=0; i<mat_size; i++){
      index_A = i * mat_size + row_A;
      temp_row[i] = matrix[index_A];
    }
    
    for(i=0; i<mat_size; i++){
      index_A = i * mat_size + row_A;
      index_B = i * mat_size + row_B;
      matrix[index_A] = matrix[index_B];
      matrix[index_B] = temp_row[i];
    }

    free(temp_row);
  }
  
  return ret_value;
}

/* INV_MATRIX*/
/*****************************************************************************/
/** 
    Returns the inverse of a square matrix
    
    Initialization: inv_matrix <= identity matrix, aux_matrix <= matrix

    End of algorithm: inv_matrix <= inverse of matrix, aux_matrix <= identity matrix    
*/
void calc_inv_matrix(double *inv_matrix, const double *matrix, double *aux_matrix, const int mat_size){

  int i, j, z, index_;
  int pivot_flag;

  double ref_coeff;

  /* Initialize matrix values */

  for(j=0; j<mat_size; j++){
    for(i=0; i<mat_size; i++){
      index_ = j * mat_size + i;
      aux_matrix[index_] = matrix[index_];
      inv_matrix[index_] = 0;
    }
  }
  
  for(j=0; j<mat_size; j++){
    index_ = j * mat_size + j;
    inv_matrix[index_] = 1;
  }

  for(j=0; j<mat_size; j++){
    i = j;
    pivot_flag = 0;
    while((i < mat_size) && (!pivot_flag)){
      index_ = j * mat_size + i;
      if(aux_matrix[index_] != 0){
	pivot_flag = 1;
	/* Displace row to upper position */
	flip_row(aux_matrix, i, j, mat_size);
	flip_row(inv_matrix, i, j, mat_size);
      
	/* Normalize row */
	ref_coeff = aux_matrix[j * mat_size + j];
	for(z=0; z<mat_size; z++){
	  index_ = z * mat_size + j;
	  aux_matrix[index_] = aux_matrix[index_] / ref_coeff;
	  inv_matrix[index_] = inv_matrix[index_] / ref_coeff;
	}
	/* Use pivot row to cancel all elements in a column */
	for(z=0; z<mat_size; z++){
	  if(z != j){
	    index_ = j * mat_size + z;
	    row_addition(inv_matrix, j, z, -aux_matrix[index_], mat_size);
	    row_addition(aux_matrix, j, z, -aux_matrix[index_], mat_size);
	  }
	}
      }
      i = i + 1;
    }
  }


  return;
}

/* COMPUTE_FORCE_ON_SOLID */
/*****************************************************************************/
/**

 Report information used to calculate friction factor.

 @param data

 @return ret_value not implemented.

*/
void compute_force_on_solid(Data_Mem *data){
  
  const char *post_process_filename = data->post_process_filename;
  
  int z, index_u, index_v, index_;
  int I, J, i, j, k;
  int p0_flag, p1_flag;
  int step;
  int wake_length_found = 0;
  int solid_found = 0;
  int domain_end_reached = 0;
  int end_i, end_j;
  int end_u, end_v;
  int slave_found;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int u_slave_count = data->u_slave_count;
  const int v_slave_count = data->v_slave_count;

  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *p_dom_matrix = data->p_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *u_sol_type = data->u_sol_type;
  const int *v_sol_type = data->v_sol_type;
  
  double v_inlet = NAN, rsqr, x_cyl, y_cyl;
  double dF, dh, vel;
  double p0, p1, press;
  double flow_dir;

  const double mu = data->mu;
  const double rho = data->rho_v[0];

  const double *u = data->u;
  const double *v = data->v;
  const double *constant_d = data->constant_d;
  const double *tcompx = data->tcompx;
  const double *tcompy = data->tcompy;
  const double *u_sol_factor = data->u_sol_factor;
  const double *v_sol_factor = data->v_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *p = data->p;
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;
  const double *u_factor_delta_h = data->u_factor_delta_h;
  const double *v_factor_delta_h = data->v_factor_delta_h;
  const double *u_factor_area = data->u_factor_area;
  const double *v_factor_area = data->v_factor_area;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *u_factor_inters_x = data->u_factor_inters_x;
  const double *u_factor_inters_y = data->u_factor_inters_y;
  const double *u_factor_inters_z = data->u_factor_inters_z;
  const double *v_factor_inters_x = data->v_factor_inters_x;
  const double *v_factor_inters_y = data->v_factor_inters_y;
  const double *v_factor_inters_z = data->v_factor_inters_z;

  const b_cond_flow *bc_w = &(data->bc_flow_west);
  const b_cond_flow *bc_e = &(data->bc_flow_east);
  const b_cond_flow *bc_s = &(data->bc_flow_south);
  const b_cond_flow *bc_n = &(data->bc_flow_north);

  FILE *fp = NULL;

  Wins *wins = &(data->wins);

  /* OBS: POSTPROCESO VALIDO PARA UNA SOLA CURVA Y UNA SOLA ENTRADA
     OBS: LLEVAR A CABO POSTPROCESO ANTES DE CENTRAR LAS COMPONENTES DE LA VELOCIDAD */

  open_check_file(post_process_filename, &fp, wins, 0);

  /* Search for boundary with fixed value boundary condition */
  
  if(bc_w->type == 1){
    v_inlet = bc_w->v_value[0];
  }
  else if(bc_e->type == 1){
    v_inlet = bc_e->v_value[0];
  }
  else if(bc_s->type == 1){
    v_inlet = bc_s->v_value[1];
  }
  else if(bc_n->type == 1){
    v_inlet = bc_n->v_value[1];
  }

  rsqr = -constant_d[0];
  x_cyl = tcompx[0];
  y_cyl = tcompy[0];
 
  fprintf(fp, "\n%% Friction coefficient results \n");
  fprintf(fp, "%% ============================ \n\n");

  /* Velocity component */

  fprintf(fp, "%% Velocity component \n");
  fprintf(fp, "%% ****************** \n");

  /* U */

  fprintf(fp, "\n%% Results for x direction \n");
  fprintf(fp, "%% ----------------------- \n");
  fprintf(fp, "\n%% %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s \n", 
	  "index_u", "nodes_x_u", "nodes_y_u", "nodes_z_u", "sol_factor", "u", "delta_h", "area", "F", "tau");
  fprintf(fp, "\n%% %10s %10s %10s  %10s %10s %10s %10s %10s %10s %10s \n\n", "", "m", "m", "m", "m", "m/s", "m", 
	  "m**2", "kg m/s**2", "Pa");
  fprintf(fp, "results0Vx = [ \n");
  
  for(z=0; z<u_sol_factor_count; z++){
    index_u = u_sol_factor_index_u[z];
    vel = u[index_u];
    dF = mu * u_sol_factor[z] * vel;
    dh = u_factor_delta_h[z];
    fprintf(fp, "%10i %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g; \n",
	    index_u, nodes_x_u[index_u], nodes_y_u[index_u], nodes_z_u[index_u],
	    u_sol_factor[z], vel, dh, u_factor_area[z], dF, mu * vel / dh);
  }

  fprintf(fp, "];\n");

  /* V */

  fprintf(fp, "\n%% Results for y direction \n");
  fprintf(fp, "%% ----------------------- \n");
  fprintf(fp, "\n%% %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s \n", 
	  "index_v", "nodes_x_u", "nodes_y_v", "nodes_z_v", "sol_factor", "v", "delta_h", "area", "F", "tau");
  fprintf(fp, "\n%% %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s \n\n", "", "m", "m", "m", "m", "m/s", "m", 
	  "m**2", "kg m/s**2", "Pa");
  fprintf(fp, "results0Vy = [ \n");

  for(z=0; z<v_sol_factor_count; z++){
    index_v = v_sol_factor_index_v[z];
    vel = v[index_v];
    dF = mu * v_sol_factor[z] * vel;
    dh = v_factor_delta_h[z];
    fprintf(fp, "%10i %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g; \n",
	    index_v, nodes_x_v[index_v], nodes_y_v[index_v], nodes_z_v[index_v],
	    v_sol_factor[z], vel, dh, v_factor_area[z], dF, mu * vel / dh);
  }

  fprintf(fp, "];\n\n");

  /* Pressure component */

  fprintf(fp, "%% Pressure component \n");
  fprintf(fp, "%% ****************** \n");

  fprintf(fp, "\n%% Results for x direction \n");
  fprintf(fp, "%% ----------------------- \n");
  fprintf(fp, "\n%% %10s %10s %10s %10s \n", "index_u", "u_p_factor", "p", "F");
  fprintf(fp, "%% %10s %10s %10s %10s \n\n", "", "m**2", "Pa", "kg m /s**2");
  fprintf(fp, "results0Px = [ \n");

  for(z=0; z<u_sol_factor_count; z++){
    index_u = u_sol_factor_index_u[z];
    index_ = u_sol_factor_index[z];
    p0_flag = (p_dom_matrix[index_] == 0);
    p1_flag = (p_dom_matrix[index_-1] == 0);
    p0 = p[index_];
    p1 = p[index_-1];
 
    if(p0_flag && p1_flag){
      press = p0 * (1 - u_p_factor_W[z]) + p1 * u_p_factor_W[z];
      dF = - press * u_p_factor[z];
      fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_u, u_p_factor[z], press, dF);
    }
    else if(p0_flag){
      dF = - p[index_] * u_p_factor[z];
      fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_u, u_p_factor[z], p0, dF);
    }
    else if(p1_flag){
      dF = - p[index_-1] * u_p_factor[z];
      fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_u, u_p_factor[z], p1, dF);
    }
    else{
      fprintf(fp, "%10i %10.5g %10s %10s; \n", index_u, u_p_factor[z], "NaN", "NaN");
    }
  }

  fprintf(fp, "];\n\n");

  fprintf(fp, "\n%% Results for y direction\n");
  fprintf(fp, "%% -----------------------\n");
  fprintf(fp, "\n%% %10s %10s %10s %10s \n", "index_v", "v_p_factor", "p", "F");
  fprintf(fp, "%% %10s %10s %10s %10s \n\n", "", "m**2", "Pa", "kg m /s**2");
  fprintf(fp, "results0Py = [ \n");

  for(z=0; z<v_sol_factor_count; z++){
    index_v = v_sol_factor_index_v[z];
    index_ = v_sol_factor_index[z];
    p0_flag = (p_dom_matrix[index_] == 0);
    p1_flag = (p_dom_matrix[index_-Nx] == 0);
    p0 = p[index_];
    p1 = p[index_-Nx];

    if(p0_flag && p1_flag){
      press = p0 * (1 - v_p_factor_S[z]) + p1 * v_p_factor_S[z];
      dF = - press * v_p_factor[z];
      fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_v, v_p_factor[z], press, dF);
    }
    else if(p0_flag){
      dF = - p[index_] * v_p_factor[z];
      fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_v, v_p_factor[z], p0, dF);
    }
    else if(p1_flag){
      dF = - p[index_-Nx] * v_p_factor[z];
      fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_v, v_p_factor[z], p1, dF);
    }
    else{
      fprintf(fp, "%10i %10.5g %10s %10s; \n", index_v, v_p_factor[z], "NaN", "NaN");
    }
  }

  fprintf(fp, "];\n\n");

  
  /* Results with slave cells velocity values interpolated */

  fprintf(fp, "\n%% Velocity cells classification \n");
  fprintf(fp, "%% =====================================================\n");

  fprintf(fp, "\n%% type 0: regular fluid node, no interpolation performed.");
  fprintf(fp, "\n%% type 1: slave node, velocity and stress interpolated.");
  fprintf(fp, "\n%% type 2: solid node, stress interpolated.\n");

  /* Cell classification */

  /* U */

  fprintf(fp, "\n%% Results for x direction\n");
  fprintf(fp, "%% -----------------------\n");
  fprintf(fp, "\n%% %10s %10s \n", "index_u", "type");
  fprintf(fp, "results1Vx = [ \n");
    
  for(z=0; z<u_sol_factor_count; z++){
    index_u = u_sol_factor_index_u[z];
    fprintf(fp, "%10i %10i ; \n", index_u, u_sol_type[z]);
  }

  fprintf(fp, "];\n\n");

  /* V */

  fprintf(fp, "\n%% Results for y direction\n");
  fprintf(fp, "%% -----------------------\n");
  fprintf(fp, "\n%% %10s %10s \n", "index_v", "type");
  fprintf(fp, "results1Vy = [ \n");

  for(z=0; z<v_sol_factor_count; z++){
    index_v = v_sol_factor_index_v[z];
    fprintf(fp, "%10i %10i ; \n", index_v, v_sol_type[z]);
  }

  fprintf(fp, "];\n\n");
  
  /* Coordinates of center of cut segment */

  fprintf(fp, "\n%% Coordinates of center of cut segment\n");
  fprintf(fp, "%% ====================================\n\n");

  fprintf(fp, "\n%% Results for u cells \n");
  fprintf(fp, "%% ------------------- \n");  
  fprintf(fp, "\n%% %10s %10s %10s %10s\n", "index_u", "x_coord", "y_coord", "z_coord");
  fprintf(fp, "%% %10s %10s %10s %10s\n\n", "", "m", "m", "m");
  fprintf(fp, "cutSegmentCenterUCells = [\n");

  for(z=0; z<u_sol_factor_count; z++){
    index_u = u_sol_factor_index_u[z];
    fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_u, u_factor_inters_x[z],
	    u_factor_inters_y[z], u_factor_inters_z[z]);
  }

  fprintf(fp, "];\n\n");

  fprintf(fp, "\n%% Results for v cells \n");
  fprintf(fp, "%% ------------------- \n");  
  fprintf(fp, "\n%% %10s %10s %10s %10s\n", "index_v", "x_coord", "y_coord", "z_coord");
  fprintf(fp, "%% %10s %10s %10s %10s\n\n", "", "m", "m", "m");
  fprintf(fp, "cutSegmentCenterVCells = [\n");

  for(z=0; z<v_sol_factor_count; z++){
    index_v = v_sol_factor_index_v[z];
    fprintf(fp, "%10i %10.5g %10.5g %10.5g; \n", index_v, v_factor_inters_x[z],
	    v_factor_inters_y[z], v_factor_inters_z[z]);
  }

  fprintf(fp, "];\n\n");
  
  /* Wake length calculations*/
  
  fprintf(fp, "\n%% Wake length results\n");
  fprintf(fp, "%% ===================\n\n");

  /* Inlet flow in the horizontal direction */
  if((bc_w->type == 1) || (bc_e->type == 1)){

    if(bc_w->type == 1){
      step = -1;
      i = nx - 1;
      flow_dir = 1;
    }
    else{
      step = 1;
      i = 0;
      flow_dir = -1;
    }

    J = Ny / 2;

    /* Search for a change of sign of velocity, that indicates presence of closed streamline */

    while((!solid_found) && (!wake_length_found) && (!domain_end_reached)){
      i += step;
      index_u = J * nx + i;

      if(u_dom_matrix[index_u] != 0){
	solid_found = 1;
      }
      else if(flow_dir * u[index_u] < 0){
	wake_length_found = 1;
      }
      if((i == 0) || (i == nx-1)){
	domain_end_reached = 1;
      }
    }

    end_j = J;    
    end_u = index_u;          

    if(solid_found){
      fprintf(fp, "%% Solid found at x = %10.5g [m] , y = %10.5g [m] , i = %i , J = %i\n", nodes_x_u[end_u], nodes_y_u[end_u], i, end_j);
    }
    else if(wake_length_found){
      fprintf(fp, "%% Wake tip found at x = %10.5g [m] , (y = %10.5g [m]) , i = %i , J = %i, u = %10.5g [m/s]\n",
	      nodes_x_u[end_u], nodes_y_u[end_u], i, end_j, u[end_u]);
      if(nodes_x_u[end_u] > x_cyl){
	fprintf(fp, "%% Wake length = %10.5g [m]\n", nodes_x_u[end_u] - x_cyl - sqrt(rsqr));
      }
      else{
	fprintf(fp, "%% Wake length = %10.5g [m]\n", x_cyl - sqrt(rsqr) - nodes_x_u[end_u]);
      }
    }
    else{
      fprintf(fp, "%% Reached end of fluid domain\n");
    }
    
  }
  /* Inlet flow in the vertical direction */
  else if((bc_s->type ==1) || (bc_n->type == 1)){

    solid_found = 0;
    wake_length_found = 0;
    domain_end_reached = 0;

    if(bc_s->type == 1){
      step = -1;
      j = ny - 1;
      flow_dir = 1;
    }
    else{
      step = 1;
      j = 0;
      flow_dir = -1;
    }
    
    I = Nx / 2;

    while((!solid_found) && (!wake_length_found) && (!domain_end_reached)){
      j += step;
      index_v = j * Nx + I;      

      if(v_dom_matrix[index_v] != 0){
	solid_found = 1;
      }
      else if(flow_dir * v[index_v] < 0){
	wake_length_found = 1;
      }
      if((j == 0) || (j == ny-1)){
	domain_end_reached = 1;
      }
    }

    end_i = I;
    end_v = index_v;    

    if(solid_found){
      fprintf(fp, "%% Solid found at x = %10.5g [m] , y = %10.5g [m] , I = %i , j = %i\n", nodes_x_v[end_v], nodes_y_v[end_v], end_i, j);
    }
    else if(wake_length_found){
      fprintf(fp, "%% Wake tip found at y = %10.5g [m] , (x = %10.5g [m]) , I = %i , j = %i , v = %10.5g [m/s]\n", 
	      nodes_y_v[end_v], nodes_x_v[end_v], end_i, j, v[end_v]);
      if(nodes_y_v[end_v] > y_cyl){
	fprintf(fp, "%% Wake length = %10.5g [m]\n", nodes_y_v[end_v] - y_cyl - sqrt(rsqr));
      }
      else{
	fprintf(fp, "%% Wake length = %10.5g [m]\n", y_cyl - sqrt(rsqr) - nodes_y_v[end_v]);
      }
    }
    else{
      fprintf(fp, "%% Reached end of fluid domain\n");
    }
  }
  
  /* Physical and simulation parameters */
  
  fprintf(fp, "\n %%Physical and simulation parameters \n\n");
  fprintf(fp, "mu = %10.5g ;%% Pa*s\n", mu);
  fprintf(fp, "rho = %10.5g ;%% kg/m3\n", rho);
  fprintf(fp, "D = %10.5g ;%% m\n", 2*sqrt(rsqr));
  fprintf(fp, "v_inf = %10.5g ;%% m/s\n", v_inlet);
  fprintf(fp, "xCyl = %10.5g;%% m\n", x_cyl);
  fprintf(fp, "yCyl = %10.5g;%% m\n", y_cyl);
  
  fclose(fp);

  return;
}

/* REORDER_MATRIX_ELEMENTS */
void reorder_matrix_elements(double *matrix, const int sub_rows, const int sub_cols, const int rows, const int cols){

  int i, j, z;
  int count = 0;


  for(j=0; j<sub_cols; j++){
    for(i=0; i<sub_rows; i++){
      matrix[count] = matrix[j * rows + i];
      count += 1;
    }
  }

  for(z=count; z<rows * cols; z++){
    matrix[z] = 0;
  }

  return;
}

/* INTERP_NEAR_V_NODE */
void interp_near_v_node(Data_Mem *data, const int index_v, const int z, double *int_coeffs, 
			const int *offset_I, const int *offset_J){


  int i;
  int neigh_ind, index_v_neigh;
  int I_neigh, J_neigh;
  int avail_neigh_count;

  int avail_neigh_list[8] = {0};
  
  const int Nx = data->Nx;
  const int nx = data->nx;

  const int *v_dom_matrix = data->v_dom_matrix;
  const int *v_sol_factor_I = data->v_sol_factor_I;
  const int *v_sol_factor_j = data->v_sol_factor_j;

  double x_vel[3] = {0};
  double coord_matrix[8 * 3];
  double coord_matrix_t[8 * 3];
  double temp_mat_0[3 * 3];
  double temp_mat_1[3 * 3];
  double temp_mat_2[3 * 8];
  double check_matrix[3 * 3];
  double neigh_vel_values[8] = {0};

  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_x = data->nodes_x;
  const double *u_faces_x = data->u_faces_x;
  const double *u_faces_y = data->u_faces_y;
  const double *v = data->v;

  for(i=0; i<8; i++){
    avail_neigh_list[i] = 0;
  }
  avail_neigh_count = 0;

  x_vel[0] = nodes_x_v[index_v];
  x_vel[1] = nodes_y_v[index_v];
	  
  for(neigh_ind=0; neigh_ind<8; neigh_ind++){
	
    index_v_neigh = index_v + offset_J[neigh_ind] * Nx + offset_I[neigh_ind];
    // Looking for neighbours in fluid domain
    if(v_dom_matrix[index_v_neigh] == 0){

      // Filling coordinate matrix: Interpolation: f(x,y) = int_coeffs[0] + int_coeffs[1] * x + int_coeffs[2] * y
      avail_neigh_list[neigh_ind] = 1;
      coord_matrix[avail_neigh_count] = 1;
      coord_matrix[1 * 8 + avail_neigh_count] = nodes_x_v[index_v_neigh] - x_vel[0];
      coord_matrix[2 * 8 + avail_neigh_count] = nodes_y_v[index_v_neigh] - x_vel[1];
      avail_neigh_count += 1;
    }
    else{
      // Try to add auxiliary points on solid surface
      I_neigh = v_sol_factor_I[z] + offset_I[neigh_ind];
      J_neigh = v_sol_factor_j[z] + offset_J[neigh_ind];
      if(v_dom_matrix[index_v_neigh + Nx] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[avail_neigh_count] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x[J_neigh * Nx + I_neigh] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y_v[index_v_neigh + 2 * Nx] - u_faces_x[(J_neigh+2) * Nx + I_neigh]
	  - u_faces_x[(J_neigh+1) * Nx + I_neigh] - x_vel[1];
	avail_neigh_count += 1;
      }
      else if(v_dom_matrix[index_v_neigh - Nx] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[avail_neigh_count] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x[J_neigh * Nx + I_neigh] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y_v[index_v_neigh - 2*Nx] + u_faces_x[(J_neigh - 1) * Nx + I_neigh]
	  + u_faces_x[J_neigh * Nx + I_neigh] - x_vel[1];
	avail_neigh_count += 1;
      }
      else if(v_dom_matrix[index_v_neigh + 1] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[avail_neigh_count] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x[J_neigh * Nx + I_neigh + 2] - u_faces_y[J_neigh * nx + I_neigh + 1]
	  - u_faces_y[J_neigh * nx + I_neigh] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y_v[index_v_neigh] - x_vel[1];
	avail_neigh_count += 1;
      }
      else if(v_dom_matrix[index_v_neigh - 1] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[avail_neigh_count] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x[J_neigh * Nx + I_neigh - 2] + u_faces_y[J_neigh * nx + I_neigh - 2]
	  + u_faces_y[J_neigh * nx + I_neigh - 1] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y_v[index_v_neigh] - x_vel[1];
	avail_neigh_count += 1;
      }
    }
  }

  if(avail_neigh_count >= 3){
    // Fill vector with neighbour velocity values

    avail_neigh_count = 0;	  
    for(neigh_ind=0; neigh_ind<8; neigh_ind++){
      index_v_neigh = index_v + offset_J[neigh_ind] * Nx + offset_I[neigh_ind];	  
      if(avail_neigh_list[neigh_ind]){
	neigh_vel_values[avail_neigh_count] = v[index_v_neigh];
	avail_neigh_count += 1;
      }
    }

    transpose_matrix(coord_matrix, coord_matrix_t, 8, 3);
    reorder_matrix_elements(coord_matrix, avail_neigh_count, 3, 8, 3);
    reorder_matrix_elements(coord_matrix_t, 3, avail_neigh_count, 3, 8);	  
    matrix_product(coord_matrix_t, coord_matrix, temp_mat_0, 3, avail_neigh_count, avail_neigh_count, 3 );
    calc_inv_matrix(temp_mat_1, temp_mat_0, check_matrix, 3);
    matrix_product(temp_mat_1, coord_matrix_t, temp_mat_2, 3, 3, 3, avail_neigh_count);
    matrix_product(temp_mat_2, neigh_vel_values, int_coeffs, 3, avail_neigh_count, avail_neigh_count, 1);
  }
  else{
    int_coeffs[0] = 0;
    int_coeffs[1] = 0;
    int_coeffs[2] = 0;
  }

  return;
}


/* INTERP_NEAR_U_NODE */
void interp_near_u_node(Data_Mem *data, const int index_u, const int z, double *int_coeffs, 
			const int *offset_I, const int *offset_J){


  int i;
  int neigh_ind, index_u_neigh;
  int I_neigh, J_neigh;
  int avail_neigh_count;

  int avail_neigh_list[8] = {0};
  
  const int Nx = data->Nx;
  const int nx = data->nx;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *u_sol_factor_i = data->u_sol_factor_i;
  const int *u_sol_factor_J = data->u_sol_factor_J;

  double x_vel[3] = {0};
  double coord_matrix[8 * 3];
  double coord_matrix_t[8 * 3];
  double temp_mat_0[3 * 3];
  double temp_mat_1[3 * 3];
  double temp_mat_2[3 * 8];
  double check_matrix[3 * 3];
  double neigh_vel_values[8] = {0};

  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_y = data->nodes_y;
  const double *v_faces_x = data->v_faces_x;
  const double *v_faces_y = data->v_faces_y;
  const double *u = data->u;


  for(i=0; i<8; i++){
    avail_neigh_list[i] = 0;
  }
  avail_neigh_count = 0;

  x_vel[0] = nodes_x_u[index_u];
  x_vel[1] = nodes_y_u[index_u];
	  
  for(neigh_ind=0; neigh_ind<8; neigh_ind++){
      
    index_u_neigh = index_u + offset_J[neigh_ind] * nx + offset_I[neigh_ind];
    // Looking for neighbours in fluid domain
    if(u_dom_matrix[index_u_neigh] == 0){

      // Filling coordinate matrix: Interpolation: f(x,y) = int_coeffs[0] + int_coeffs[1] * x + int_coeffs[2] * y 
      avail_neigh_list[neigh_ind] = 1;
      coord_matrix[avail_neigh_count] = 1;
      coord_matrix[1 * 8 + avail_neigh_count] = nodes_x_u[index_u_neigh] - x_vel[0];
      coord_matrix[2 * 8 + avail_neigh_count] = nodes_y_u[index_u_neigh] - x_vel[1];
      avail_neigh_count += 1;
    }
    else{
      I_neigh = u_sol_factor_i[z] + offset_I[neigh_ind];
      J_neigh = u_sol_factor_J[z] + offset_J[neigh_ind];
      if(u_dom_matrix[index_u_neigh + 1] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[avail_neigh_count] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x_u[index_u_neigh+2] - v_faces_y[J_neigh * Nx + I_neigh + 2]
	  - v_faces_y[J_neigh * Nx + I_neigh + 1] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y[J_neigh * Nx + I_neigh] - x_vel[1];
	avail_neigh_count += 1;	    
      }
      else if(u_dom_matrix[index_u_neigh - 1] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[avail_neigh_count] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x_u[index_u_neigh-2] + v_faces_y[J_neigh * Nx + I_neigh - 1]
	  + v_faces_y[J_neigh * Nx + I_neigh] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y[J_neigh * Nx + I_neigh] - x_vel[1];
	avail_neigh_count += 1;
      }
      else if(u_dom_matrix[index_u_neigh + nx] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x_u[index_u_neigh] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y[(J_neigh + 2) * Nx + I_neigh] - v_faces_x[(J_neigh + 1) * nx + I_neigh]
	  - v_faces_x[J_neigh * nx + I_neigh] - x_vel[1];
	avail_neigh_count += 1;
      }
      else if(u_dom_matrix[index_u_neigh - nx] == 0){
	avail_neigh_list[neigh_ind] = 1;
	coord_matrix[1 * 8 + avail_neigh_count] = nodes_x_u[index_u_neigh] - x_vel[0];
	coord_matrix[2 * 8 + avail_neigh_count] = nodes_y[(J_neigh - 2) * Nx + I_neigh] + v_faces_x[(J_neigh - 2) * nx + I_neigh]
	  + v_faces_x[(J_neigh - 1) * nx + I_neigh] - x_vel[1];
	avail_neigh_count += 1;
      }
    }
  }

  if(avail_neigh_count >= 3){
    // Fill vector with neighbour velocity values

    avail_neigh_count = 0;	  
    for(neigh_ind=0; neigh_ind<8; neigh_ind++){
      index_u_neigh = index_u + offset_J[neigh_ind] * nx + offset_I[neigh_ind];
      if(avail_neigh_list[neigh_ind]){
	neigh_vel_values[avail_neigh_count] = u[index_u_neigh];
	avail_neigh_count += 1;
      }
    }
    transpose_matrix(coord_matrix, coord_matrix_t, 8, 3);
    reorder_matrix_elements(coord_matrix, avail_neigh_count, 3, 8, 3);
    reorder_matrix_elements(coord_matrix_t, 3, avail_neigh_count, 3, 8);	  
    matrix_product(coord_matrix_t, coord_matrix, temp_mat_0, 3, avail_neigh_count, avail_neigh_count, 3 );
    calc_inv_matrix(temp_mat_1, temp_mat_0, check_matrix, 3);
    matrix_product(temp_mat_1, coord_matrix_t, temp_mat_2, 3, 3, 3, avail_neigh_count);
    matrix_product(temp_mat_2, neigh_vel_values, int_coeffs, 3, avail_neigh_count, avail_neigh_count, 1);
  }
  else{
    int_coeffs[0] = 0;
    int_coeffs[1] = 0;
    int_coeffs[2] = 0;
  }

  return;
}

/* COMPUTE_NORM_GLOB_RES */
/*****************************************************************************/
/**
  
  Computes global normalized residual as sum (a_nb*phi_nb + b - a_P*phi_P) / sum (a_P * phi_aP)

  @return glob_res / norm_factor.
  
  MODIFIED: 16-06-2019
*/
double compute_norm_glob_res(const double *field, Coeffs *coeffs_,
			     const int Nx, const int Ny){

  int I, J, index_;

  double norm_factor = 0;
  double glob_res = 0;

  const double *aP = coeffs_->aP;
  const double *aW = coeffs_->aW;
  const double *aE = coeffs_->aE;
  const double *aS = coeffs_->aS;
  const double *aN = coeffs_->aN;
  const double *b = coeffs_->b;

#pragma omp parallel for default(none) private(I, J, index_)		\
  shared(aW, aE, aS, aN, aP, b, field) reduction(+:norm_factor, glob_res)

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      index_ = J*Nx + I;
      norm_factor += fabs(aP[index_] * field[index_]);
      glob_res += fabs(aW[index_] * field[index_-1] + aE[index_] * field[index_+1] +
		       aS[index_] * field[index_-Nx] + aN[index_] * field[index_+Nx] +
		       b[index_] - aP[index_] * field[index_]);
    }
  }

  return (glob_res / norm_factor);
}


/* SET_VARIABLE_ARRAYS_DEFAULT_SIZE */
/*****************************************************************************/
/**
  
  Sets:

  Initialization of counters to avoid free(NULL) errors 

  @param data

  @return void
  
  MODIFIED: 02-08-2019
*/
void set_variable_arrays_default_size(Data_Mem *data){

  data->u_sol_factor_count = 0;
  data->v_sol_factor_count = 0;
  data->w_sol_factor_count = 0;
  data->turb_boundary_cell_count = 0;
  data->u_slave_count = 0;
  data->v_slave_count = 0;
  data->w_slave_count = 0;
  data->u_cut_face_Fy_count = 0;
  data->u_cut_face_Fz_count = 0;
  data->v_cut_face_Fx_count = 0;
  data->v_cut_face_Fz_count = 0;
  data->w_cut_face_Fx_count = 0;
  data->w_cut_face_Fy_count = 0;  
  data->p_nodes_in_solid_count = 0;
  data->Dirichlet_boundary_cell_count = 0;
  data->Isoflux_boundary_cell_count = 0;
  data->Conjugate_boundary_cell_count = 0;
  data->diff_u_face_x_count = 0;
  data->diff_v_face_y_count = 0;
  data->diff_w_face_z_count = 0;

  return;
}

/* WRITE_IN_VEL */
/*****************************************************************************/
/**

   Writes inlet and outlet average velocities for periodic flow boundary condition.

   @param data

   @return void

   MODIFIED: 11-08-2020
 */
void write_in_vel(Data_Mem *data){
  
  int I, J, flow_SN, flow_WE;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int w_type = data->bc_flow_west.type;  
  const int e_type = data->bc_flow_east.type;
  const int s_type = data->bc_flow_south.type;
  const int n_type = data->bc_flow_north.type;

  static int count_flow = 0;  

  double sum_in = 0;
  double sum_out = 0;

  const double *u = data->u;
  const double *v = data->v;
  
  Wins *wins = &(data->wins);

  const Sigmas *sigmas = &(data->sigmas);
  
  FILE *fp = NULL;

  const char *avg_iv_filename = data->avg_iv_filename;
  
  fp = fopen(avg_iv_filename, "a");

  if(fp == NULL){
    error_msg(wins, "Could not create/open file for average inlet velocity");
  }
  
  flow_SN = (n_type == 5) || (s_type == 5);
  flow_WE = (w_type == 5) || (e_type == 5);

  if(flow_WE){
    for(J=1; J<Ny-1; J++){
      sum_in += u[J*nx];
      sum_out += u[J*nx + nx-1];
    }
  }
  else if(flow_SN){
    for(I=1; I<Nx-1; I++){
      sum_in += v[I];
      sum_out += v[(ny-1)*Nx + I];
    }    
  }

  if(count_flow == 0){
    
    if(flow_WE){
      fprintf(fp, "%% it %11s %11s\n", "West face","East face");
    }
    else if(flow_SN){
      fprintf(fp, "%% it %11s %11s\n", "South face", "North face");
    }
    
    fprintf(fp, "avg_inlet_vel = [\n");
      
  }
  else{
    if(flow_WE){
      fprintf(fp, "%i %11.5g %11.5g\n",
	      sigmas->it, sum_in / (Ny-2), sum_out / (Ny-2));      
    }    
    else if(flow_SN){   
      fprintf(fp, "%i %11.5g %11.5g\n",
	      sigmas->it, sum_in / (Nx-2), sum_out / (Nx-2));
    }
    else{
      fprintf(fp, "%i %11.5g \n",
	      sigmas->it, 0.0);      
    }
  }
  count_flow++;

  
  fclose(fp);
  
  return;  
}

/* CLOSE_IN_VEL */
void close_in_vel(Data_Mem *data){

  Wins *wins = &(data->wins);

  FILE *fp = NULL;

  const char *avg_iv_filename = data->avg_iv_filename;

  fp = fopen(avg_iv_filename, "a");

  if(fp == NULL){
    error_msg(wins, "Could not create/open file for average inlet velocity");
  }
  
  fprintf(fp, "];\n");
  
  fclose(fp);
  
  return;
}


/* SEND_EMAIL */
void send_email(const char *email, const char *file){

  char msg[2*SIZE];
  
  snprintf(msg, 2*SIZE-1,
	   "mutt -s \"Computations done :D\" -- %s < %s", email, file);
  system(msg);

  printf("Notification sent to %s\n", email);
  
  return;
}

/* COMPUTE_NORMAL_PARALLEL_PRODUCTS */
/*****************************************************************************/
/**

  Sets: 

  Array (double, solid_boundary_cell_count) nxnx_x: Product nx*nx at face perpendicular to x axis.

  Array (double, solid_boundary_cell_count) nxny_x: Product nx*ny at face perpendicular to x axis.

  Array (double, solid_boundary_cell_count) nyny_y: Product ny*ny at face perpendicular to y axis.

  Array (double, solid_boundary_cell_count) nxny_y: Product nx*ny at face perpendicular to y axis.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 23-05-2019
*/
void compute_normal_parallel_products(Data_Mem *data){

  int count, index_;

  const int solid_boundary_cell_count = data->solid_boundary_cell_count;
  const int Nx = data->Nx;

  const int *dom_mat = data->domain_matrix; 
  const int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs; 

  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_x_y = data->norm_vec_x_y;
  const double *norm_vec_y_x = data->norm_vec_y_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;

  if(solid_boundary_cell_count){

    data->nxnx_x = create_array(solid_boundary_cell_count, 1, 1, 0);
    data->nxny_x = create_array(solid_boundary_cell_count, 1, 1, 0);
    data->nyny_y = create_array(solid_boundary_cell_count, 1, 1, 0);
    data->nxny_y = create_array(solid_boundary_cell_count, 1, 1, 0);

    double *nxnx_x = data->nxnx_x;
    double *nxny_x = data->nxny_x;
    double *nyny_y = data->nyny_y;
    double *nxny_y = data->nxny_y;

    for(count=0; count<solid_boundary_cell_count; count++){
           
      index_ = sf_boundary_solid_indexs[count];

      /* East fluid cell */
      if (dom_mat[index_+1] == 0){
	nxnx_x[count] = norm_vec_x_x[index_+1] * norm_vec_x_x[index_+1];
	nxny_x[count] = norm_vec_x_x[index_+1] * norm_vec_x_y[index_+1];
      }

      /* West fluid cell */
      if (dom_mat[index_-1] == 0){
	nxnx_x[count] = norm_vec_x_x[index_-1] * norm_vec_x_x[index_-1];
	nxny_x[count] = norm_vec_x_x[index_-1] * norm_vec_x_y[index_-1];
      }

      /* North fluid cell */
      if (dom_mat[index_+Nx] == 0){
	nxny_y[count] = norm_vec_y_x[index_+Nx] * norm_vec_y_y[index_+Nx];
	nyny_y[count] = norm_vec_y_y[index_+Nx] * norm_vec_y_y[index_+Nx];
      }

      /* South fluid cell */
      if (dom_mat[index_-Nx] == 0){
	nxny_y[count] = norm_vec_y_x[index_-Nx] * norm_vec_y_y[index_-Nx];
	nyny_y[count] = norm_vec_y_y[index_-Nx] * norm_vec_y_y[index_-Nx];
      }
    }
  }

  return;
}

/* OPEN_CHECK_FILE */
/*****************************************************************************/
/**
   
   Returns pointer to file specified on file_to_open string.

   @param file_to_open: name of file to be opened.

   @param fp: file pointer to be returned.

   @param wins:

   @param mode 0: write, 1: append.

*/
void open_check_file(const char file_to_open[SIZE], FILE **fp, Wins *wins, const int mode){

  if(mode == 0){
    *fp = fopen(file_to_open, "w");
  }
  else if(mode == 1){
    *fp = fopen(file_to_open, "a");
  }
      
  if(*fp == NULL){
    error_msg(wins, "Could not create file... permissions??");
  }

  return;
}

/* WRITE_NORM_GLOB_RES */
/*****************************************************************************/
/**

   Write glob_norm values to glob_res_filename. Write flow values if
   Data_Mem::flow_done_OK is False, and phi values otherwise.

   @param data

   @return void
 */
void write_norm_glob_res(Data_Mem *data){

  const int flow_done_OK = data->flow_done_OK;
  const int turb_model = data->turb_model;
  
  const char *glob_res_filename = data->glob_res_filename;

  const Sigmas *sigmas = &(data->sigmas);
  Wins *wins = &(data->wins);
  
  FILE *fp = NULL;

  static int count_phi_glob_res = 0;
  static int count_flow_glob_res = 0;

  const double *glob_norm = sigmas->glob_norm;
  
  open_check_file(glob_res_filename, &fp, wins, 1);

  if(!flow_done_OK){
    
    if(count_flow_glob_res == 0){

      /* Write informative heading */

      if(turb_model){
	fprintf(fp, "%% %9s %11s %11s %11s %11s %11s %11s %11s\n",
		"it",
		"u", "v", "w", "c",
		"k", "eps", "mass_io");
      }
      else{
	fprintf(fp, "%% %9s %11s %11s %11s %11s %11s\n",
		"it",
		"u", "v", "w", "c", "mass_io");	
      }
      
      fprintf(fp, "glob_norm_res_var = [\n");
    }
    else{
      if(turb_model){
     
	fprintf(fp, "%11d %11.3e %11.3e %11.3e %11.3e %11.3e %11.3e %11.3e;\n",
		sigmas->it,
		glob_norm[0], glob_norm[1], glob_norm[2],
		glob_norm[3], glob_norm[4], glob_norm[5],
		glob_norm[6]);
      }
      else{

	fprintf(fp, "%11d %11.3e %11.3e %11.3e %11.3e %11.3e;\n",
		sigmas->it,
		glob_norm[0], glob_norm[1], glob_norm[2],
		glob_norm[3], glob_norm[6]);
      }
    }

    count_flow_glob_res++;
  }
  else{
    
    if(count_phi_glob_res == 0){
      
      fprintf(fp, "%11s %11s %11s %11s\n",
	      "it", "sub_it", "phi_glob_norm", "avg_gL_diff");
      fprintf(fp, "phi_gL_glob_norm = [\n");
      
    }
    else{
      if(!(data->steady_state_OK)){
	fprintf(fp, "%11d %11d %11.3e %11.3e;\n",
		sigmas->it, sigmas->sub_it, glob_norm[7], glob_norm[8]);
      }
    }
    
    count_phi_glob_res++;
  }
  
  fclose(fp);
  
  return;

}  

/* COMPUTE_NEIGHBOUR_COEFFS_FACTORS */
/*****************************************************************************/
/**
  
  Compute multiplicative factors that make coefficients corresponding to neighbour 
  nodes that pertain to a different phase equal to zero on energy conservation equation.

  Array (1, NxNyNz) E_coeff: E_neighbour multiplicative factor.  

  Array (1, NxNyNz) W_coeff: W_neighbour multiplicative factor.

  Array (1, NxNyNz) N_coeff: N_neighbour multiplicative factor.

  Array (1, NxNyNz) S_coeff: S_neighbour multiplicative factor.

  Array (1, NxNyNz) T_coeff: T_neighbour multiplicative factor.

  Array (1, NxNyNz) B_coeff: B_neighbour multiplicative factor.

  @param data

  @return 1
  
  MODIFIED: 16-07-2019
*/
void compute_neighbour_coeffs_factors(Data_Mem *data){

  int I, J, K, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int solve_3D_OK = data->solve_3D_OK;

  int *E_coeff = data->E_coeff;
  int *W_coeff = data->W_coeff;
  int *N_coeff = data->N_coeff;
  int *S_coeff = data->S_coeff;
  int *T_coeff = data->T_coeff;
  int *B_coeff = data->B_coeff;

  const int *dom_mat = data->domain_matrix;

  if(solve_3D_OK){
    
#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(dom_mat, E_coeff, W_coeff, N_coeff, S_coeff, T_coeff, B_coeff)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = K*Nx*Ny + J*Nx + I;
      
	  if(dom_mat[index_] != dom_mat[index_+1]){
	    E_coeff[index_] = 0;
	  }
		
	  if(dom_mat[index_] != dom_mat[index_-1]){
	    W_coeff[index_] = 0;
	  }
		
	  if(dom_mat[index_] != dom_mat[index_+Nx]){
	    N_coeff[index_] = 0;
	  }
		
	  if(dom_mat[index_] != dom_mat[index_-Nx]){
	    S_coeff[index_] = 0;
	  }

	  if(dom_mat[index_] != dom_mat[index_+Nx*Ny]){
	    T_coeff[index_] = 0;
	  }

	  if(dom_mat[index_] != dom_mat[index_-Nx*Ny]){
	    B_coeff[index_] = 0;
	  }
	  
	}
      }
    }
    
  }
  else{
    
#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(dom_mat, E_coeff, W_coeff, N_coeff, S_coeff)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
      
	if(dom_mat[index_] != dom_mat[index_+1]){
	  E_coeff[index_] = 0;
	}
		
	if(dom_mat[index_] != dom_mat[index_-1]){
	  W_coeff[index_] = 0;
	}
		
	if(dom_mat[index_] != dom_mat[index_+Nx]){
	  N_coeff[index_] = 0;
	}
		
	if(dom_mat[index_] != dom_mat[index_-Nx]){
	  S_coeff[index_] = 0;
	}
      }
    }
  
  }

  return;
}

/* COMPUTE_AVG_GL_DIFF */
/*****************************************************************************/
/**
  
  Computes weighted average value of difference between gL_guess and gL_calc.

  @param data

  @return (gL_difference / vol).
  
  MODIFIED: 16-06-2019
*/
double compute_avg_gL_diff(Data_Mem *data){
  
  int I, J, index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *phase_change_matrix = data->phase_change_matrix;

  double vol = 0;
  double gL_difference = 0;
  
  const double *phase_change_vol = data->phase_change_vol;
  const double *gL_guess = data->gL_guess;
  const double *gL_calc = data->gL_calc;
  
#pragma omp parallel for default(none) private(J, I, index_)		\
  shared(phase_change_matrix, phase_change_vol, gL_guess, gL_calc)	\
  reduction(+: vol, gL_difference)

  for(J=1; J<(Ny-1); J++){
    for(I=1; I<(Nx-1); I++){

      index_ = J*Nx+I;

      if(phase_change_matrix[index_]){
	vol += phase_change_vol[index_];
	gL_difference += fabs((gL_guess[index_] - gL_calc[index_]) *
			      phase_change_vol[index_]);
      }
    }
  }
  
  return (gL_difference / vol);	
}
	    
/* SF_BOUNDARY_SOLID_INDEXS_FINDER_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, solid_boundary_cell_count) sf_boundary_solid_indexs: Non-fluid 
  cells that are solved (based on P_solve_phase_OK) that have a fluid neighbour.

  Array (int, NxNyNz) W_is_fluid_OK: 1 if neighbour at west position is fluid.

  Array (int, NxNyNz) E_is_fluid_OK: 1 if neighbour at east position is fluid.

  Array (int, NxNyNz) S_is_fluid_OK: 1 if neighbour at south position is fluid.

  Array (int, NxNyNz) N_is_fluid_OK: 1 if neighbour at north position is fluid.

  Array (int, NxNyNz) B_is_fluid_OK: 1 if neighbour at bottom position is fluid.

  Array (int, NxNyNz) T_is_fluid_OK: 1 if neighbour at top position is fluid.

  @param data
  
  @return ret_value not implemented.
  
  MODIFIED: 15-07-2019
*/
void sf_boundary_solid_indexs_finder_3D(Data_Mem *data){

  int I, J, K, index_;
  int solid_boundary_cell_count = 0;
  int fluid_nb_OK = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int *dom_matrix = data->domain_matrix;
  
  int *E_is_fluid_OK = data->E_is_fluid_OK;
  int *W_is_fluid_OK = data->W_is_fluid_OK;
  int *N_is_fluid_OK = data->N_is_fluid_OK;
  int *S_is_fluid_OK = data->S_is_fluid_OK;
  int *T_is_fluid_OK = data->T_is_fluid_OK;
  int *B_is_fluid_OK = data->B_is_fluid_OK;

  char *case_s_type_x = data->case_s_type_x;
  char *case_s_type_y = data->case_s_type_y;
  char *case_s_type_z = data->case_s_type_z;
  
  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  
#pragma omp parallel for default(none) private(I, J, K, index_, fluid_nb_OK) \
  shared(dom_matrix, W_is_fluid_OK, E_is_fluid_OK, S_is_fluid_OK, N_is_fluid_OK, \
	 B_is_fluid_OK, T_is_fluid_OK, P_solve_phase_OK)		\
  reduction(+: solid_boundary_cell_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	  
	if(dom_matrix[index_-1] != 0){
	  W_is_fluid_OK[index_] = 0;
	}
	
	if(dom_matrix[index_+1] != 0){
	  E_is_fluid_OK[index_] = 0;
	}
	
	if(dom_matrix[index_-Nx] != 0){
	  S_is_fluid_OK[index_] = 0;
	}
	
	if(dom_matrix[index_+Nx] != 0){
	  N_is_fluid_OK[index_] = 0;
	}

	if(dom_matrix[index_-Nx*Ny] != 0){
	  B_is_fluid_OK[index_] = 0;
	}

	if(dom_matrix[index_+Nx*Ny] != 0){
	  T_is_fluid_OK[index_] = 0;
	}
	  
	fluid_nb_OK = 
	  (dom_matrix[index_-1] == 0) || (dom_matrix[index_+1] == 0) || 
	  (dom_matrix[index_-Nx] == 0) || (dom_matrix[index_+Nx] == 0) ||
	  (dom_matrix[index_-Nx*Ny] == 0) || (dom_matrix[index_+Nx*Ny] == 0);
    
	if((dom_matrix[index_] != 0) && P_solve_phase_OK[index_] && fluid_nb_OK){
	  solid_boundary_cell_count++;
	
	}
      }
    }
  }

  data->sf_boundary_solid_indexs = 
    create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->I_bm = create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->J_bm = create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->K_bm = create_array_int(solid_boundary_cell_count, 1, 1, 0);
  data->solid_boundary_cell_count = solid_boundary_cell_count;

  int *sf_boundary_solid_indexs = data->sf_boundary_solid_indexs;
  int *I_bm = data->I_bm;
  int *J_bm = data->J_bm;
  int *K_bm = data->K_bm;

  solid_boundary_cell_count = 0;
    
#pragma omp parallel for default(none)					\
  private(I, J, K, index_, fluid_nb_OK)					\
  shared(dom_matrix, P_solve_phase_OK, sf_boundary_solid_indexs,	\
	 I_bm, J_bm, K_bm, solid_boundary_cell_count,			\
	 case_s_type_x, case_s_type_y, case_s_type_z)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = K*Nx*Ny + J*Nx + I;
          
	fluid_nb_OK = 
	  (dom_matrix[index_-1] == 0) || (dom_matrix[index_+1] == 0) || 
	  (dom_matrix[index_-Nx] == 0) || (dom_matrix[index_+Nx] == 0) ||
	  (dom_matrix[index_-Nx*Ny] == 0) || (dom_matrix[index_+Nx*Ny] == 0);
    
	if((dom_matrix[index_] != 0) && P_solve_phase_OK[index_] && fluid_nb_OK){

#pragma omp critical
	  {

	    sf_boundary_solid_indexs[solid_boundary_cell_count] = index_;

	    I_bm[solid_boundary_cell_count] = I;
	    J_bm[solid_boundary_cell_count] = J;
	    K_bm[solid_boundary_cell_count] = K;

	    solid_boundary_cell_count++;

	    if((I >= 3) && (I <= Nx-4)) {case_s_type_x[index_] = 'a';}
	    else if ((I == 2) || (I == Nx-3)) {case_s_type_x[index_] = 'b';}
	    else if ((I == 1) || (I == Nx-2)) {case_s_type_x[index_] = 'c';}
	    else {case_s_type_x[index_] = 'd';}

	    if((J >= 3) && (J <= Ny-4)) {case_s_type_y[index_] = 'a';}
	    else if ((J == 2) || (J == Ny-3)) {case_s_type_y[index_] = 'b';}
	    else if ((J == 1) || (J == Ny-2)) {case_s_type_y[index_] = 'c';}
	    else {case_s_type_y[index_] = 'd';}

	    if((K >= 3) && (K <= Nz-4)) {case_s_type_z[index_] = 'a';}
	    else if ((K == 2) || (K == Nz-3)) {case_s_type_z[index_] = 'b';}
	    else if ((K == 1) || (K == Nz-2)) {case_s_type_z[index_] = 'c';}
	    else {case_s_type_z[index_] = 'd';}
	    
	  }
	}
      }
    }
  }
  
  return;
}

/* DIRICHLET_BOUNDARY_INDEXS_FINDER_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Dirichlet_boundary_cell_count) Dirichlet_boundary_indexs: Cells 
  that are solved (based on P_solve_phase_OK) and have a non-solved neighbour, 
  this neighbour pertaining to a domain defined by a curve with a Dirichlet 
  boundary condition.

  @param data
  
  @return void
  
  MODIFIED: 15-07-2019
*/
void Dirichlet_boundary_indexs_finder_3D(Data_Mem *data){

  int I, J, K, index_;
  int Dirichlet_boundary_cell_count = 0;
  int cell_OK = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *dom_mat = data->domain_matrix;
  const int *Isoflux_OK = data->Isoflux_OK;
  
  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  const int *T_solve_phase_OK = data->T_solve_phase_OK;
  const int *B_solve_phase_OK = data->B_solve_phase_OK;
  
#pragma omp parallel for default(none) private(I, J, K, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, \
	 P_solve_phase_OK, T_solve_phase_OK, B_solve_phase_OK, Isoflux_OK, dom_mat) \
  reduction(+:Dirichlet_boundary_cell_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
        
	index_ = K*Nx*Ny + J*Nx + I;

	cell_OK = 
	  (!E_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+1] - 1 ]) ||
	  (!W_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-1] - 1 ]) ||
	  (!N_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+Nx] - 1 ]) ||
	  (!S_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-Nx] - 1 ]) ||
	  (!B_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+Nx*Ny] - 1]) ||
	  (!T_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-Nx*Ny] - 1]);

	if(P_solve_phase_OK[index_] && cell_OK){
	  Dirichlet_boundary_cell_count++;
	}
      }
    }
  }
  
  data->Dirichlet_boundary_indexs = 
    create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->I_Dirichlet_v = create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->J_Dirichlet_v = create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->K_Dirichlet_v = create_array_int(Dirichlet_boundary_cell_count, 1, 1, 0);
  data->Dirichlet_boundary_cell_count = Dirichlet_boundary_cell_count;

  int *Dirichlet_boundary_indexs = data->Dirichlet_boundary_indexs;
  int *I_Dirichlet_v = data->I_Dirichlet_v;
  int *J_Dirichlet_v = data->J_Dirichlet_v;
  int *K_Dirichlet_v = data->K_Dirichlet_v;
  
  Dirichlet_boundary_cell_count = 0;

#pragma omp parallel for default(none) private(I, J, K, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, \
	 P_solve_phase_OK, T_solve_phase_OK, B_solve_phase_OK, Isoflux_OK, dom_mat, \
	 Dirichlet_boundary_indexs, I_Dirichlet_v, J_Dirichlet_v, K_Dirichlet_v, Dirichlet_boundary_cell_count) 

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
        
	index_ = K*Nx*Ny + J*Nx + I;

	cell_OK = 
	  (!E_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+1] - 1 ]) ||
	  (!W_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-1] - 1 ]) ||
	  (!N_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+Nx] - 1 ]) ||
	  (!S_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-Nx] - 1 ]) ||
	  (!T_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_+Nx*Ny] - 1]) ||
	  (!B_solve_phase_OK[index_] && !Isoflux_OK[ dom_mat[index_-Nx*Ny] - 1]);

	if(P_solve_phase_OK[index_] && cell_OK){
#pragma omp critical
	  {
	    Dirichlet_boundary_indexs[Dirichlet_boundary_cell_count] = index_;
	    I_Dirichlet_v[Dirichlet_boundary_cell_count] = I;
	    J_Dirichlet_v[Dirichlet_boundary_cell_count] = J;
	    K_Dirichlet_v[Dirichlet_boundary_cell_count] = K;
	    Dirichlet_boundary_cell_count++;
	  }
	}
      
      }
    }
  }
  
  return;
}

/* ISOFLUX_BOUNDARY_INDEXS_FINDER_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Isoflux_boundary_cell_count) Dirichlet_boundary_indexs: Cells 
  that are solved(based on P_solve_phase_OK) and have a non-solved neighbour, 
  this neighbour pertaining to a domain defined by a curve with an Isoflux
  boundary condition.

  @param data
  
  @return void
  
  MODIFIED: 16-07-2019
*/
void Isoflux_boundary_indexs_finder_3D(Data_Mem *data){

  int I, J, K, index_;
  int cell_OK = 0;
  int Isoflux_boundary_cell_count = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int *dom_mat = data->domain_matrix;
  const int *Isoflux_OK = data->Isoflux_OK;

  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  const int *T_solve_phase_OK = data->T_solve_phase_OK;
  const int *B_solve_phase_OK = data->B_solve_phase_OK;
  
#pragma omp parallel for default(none) private(I, J, K, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, \
	 T_solve_phase_OK, B_solve_phase_OK, P_solve_phase_OK, Isoflux_OK, dom_mat) \
  reduction(+:Isoflux_boundary_cell_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;
	  
	cell_OK = 
	  (!E_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+1] -1 ]) ||
	  (!W_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-1] -1 ]) ||
	  (!N_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+Nx] -1 ]) ||
	  (!S_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-Nx] -1 ]) ||
	  (!T_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+Nx*Ny] - 1 ]) ||
	  (!B_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-Nx*Ny] - 1 ]);

	if(P_solve_phase_OK[index_] && cell_OK){
	  Isoflux_boundary_cell_count++;
	}
      }
    }
  }
   
  data->Isoflux_boundary_indexs = 
    create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->I_Isoflux_v = create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->J_Isoflux_v = create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->K_Isoflux_v = create_array_int(Isoflux_boundary_cell_count, 1, 1, 0);
  data->Isoflux_boundary_cell_count = Isoflux_boundary_cell_count;
    
  int *Isoflux_boundary_indexs = data->Isoflux_boundary_indexs;
  int *I_Isoflux_v = data->I_Isoflux_v;
  int *J_Isoflux_v = data->J_Isoflux_v;
  int *K_Isoflux_v = data->K_Isoflux_v;

  Isoflux_boundary_cell_count = 0;
    
#pragma omp parallel for default(none) private(I, J, K, index_, cell_OK)	\
  shared(E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, \
	 T_solve_phase_OK, B_solve_phase_OK, P_solve_phase_OK, Isoflux_OK, dom_mat, \
	 Isoflux_boundary_cell_count, Isoflux_boundary_indexs, I_Isoflux_v, J_Isoflux_v, K_Isoflux_v)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	
	cell_OK = 
	  (!E_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+1] -1 ]) ||
	  (!W_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-1] -1 ]) ||
	  (!N_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+Nx] -1 ]) ||
	  (!S_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-Nx] -1 ]) ||
	  (!T_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_+Nx*Ny] - 1 ]) ||
	  (!B_solve_phase_OK[index_] && Isoflux_OK[ dom_mat[index_-Nx*Ny] - 1 ]);

	if(P_solve_phase_OK[index_] && cell_OK){
#pragma omp critical
	  {
	    Isoflux_boundary_indexs[Isoflux_boundary_cell_count] = index_;
	    I_Isoflux_v[Isoflux_boundary_cell_count] = I;
	    J_Isoflux_v[Isoflux_boundary_cell_count] = J;
	    K_Isoflux_v[Isoflux_boundary_cell_count] = K;
	    Isoflux_boundary_cell_count++;
	  }
	}
      }
    }
  }
 
  return;
}

/* CONJUGATE_BOUNDARY_INDEXS_FINDER_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (int, Conjugate_boundary_cell_count) Conjugate_boundary_indexs: Cells 
  that are solved (based on P_solve_phase_OK) that have a neighbour that is 
  also solved, if domain index of neighbour cell is different to domain
  index of P cell.

  @param data

  @return void
  
  MODIFIED: 15-07-2019
*/
void Conjugate_boundary_indexs_finder_3D(Data_Mem *data){

  int I, J, K, index_;
  int valid_cell_OK = 0;
  int Conjugate_boundary_cell_count = 0;
  
  int current_domain;
  int E_domain, W_domain, N_domain, S_domain;
  int T_domain, B_domain;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int *domain_matrix = data->domain_matrix;
  
  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  const int *T_solve_phase_OK = data->T_solve_phase_OK;
  const int *B_solve_phase_OK = data->B_solve_phase_OK;
  
#pragma omp parallel for default(none)					\
  private(I, J, K, index_, current_domain, E_domain, W_domain, N_domain, \
	  S_domain, T_domain, B_domain, valid_cell_OK)			\
  shared(domain_matrix, E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, \
	 S_solve_phase_OK, T_solve_phase_OK, B_solve_phase_OK, P_solve_phase_OK) \
  reduction(+:Conjugate_boundary_cell_count)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;
      
	current_domain = domain_matrix[index_];
      
	E_domain = domain_matrix[index_+1];
	W_domain = domain_matrix[index_-1];
	N_domain = domain_matrix[index_+Nx];
	S_domain = domain_matrix[index_-Nx];
	T_domain = domain_matrix[index_+Nx*Ny];
	B_domain = domain_matrix[index_-Nx*Ny];
	  
	valid_cell_OK = 
	  (E_solve_phase_OK[index_] && (current_domain != E_domain)) || 
	  (W_solve_phase_OK[index_] && (current_domain != W_domain)) || 
	  (N_solve_phase_OK[index_] && (current_domain != N_domain)) || 
	  (S_solve_phase_OK[index_] && (current_domain != S_domain)) ||
	  (T_solve_phase_OK[index_] && (current_domain != T_domain)) ||
	  (B_solve_phase_OK[index_] && (current_domain != B_domain));
    
	if(P_solve_phase_OK[index_] && valid_cell_OK){
	  Conjugate_boundary_cell_count++;
	}
      }
    }
  }
    
  data->Conjugate_boundary_indexs = 
    create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->I_Conjugate_v = create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->J_Conjugate_v = create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->K_Conjugate_v = create_array_int(Conjugate_boundary_cell_count, 1, 1, 0);
  data->Conjugate_boundary_cell_count = Conjugate_boundary_cell_count;  

  int *Conjugate_boundary_indexs = data->Conjugate_boundary_indexs;
  int *I_Conjugate_v = data->I_Conjugate_v;
  int *J_Conjugate_v = data->J_Conjugate_v;
  int *K_Conjugate_v = data->K_Conjugate_v;
  
  Conjugate_boundary_cell_count = 0;
    
#pragma omp parallel for default(none) private(I, J, K, index_, current_domain, E_domain, \
					       W_domain, N_domain, S_domain, T_domain, B_domain, valid_cell_OK) \
  shared(domain_matrix, E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, \
	 T_solve_phase_OK, B_solve_phase_OK, P_solve_phase_OK, Conjugate_boundary_indexs, I_Conjugate_v, \
	 J_Conjugate_v,  K_Conjugate_v, Conjugate_boundary_cell_count) 

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	index_ = K*Nx*Ny + J*Nx + I;
          
	current_domain = domain_matrix[index_];
      
	E_domain = domain_matrix[index_+1];
	W_domain = domain_matrix[index_-1];
	N_domain = domain_matrix[index_+Nx];
	S_domain = domain_matrix[index_-Nx];
	T_domain = domain_matrix[index_+Nx*Ny];
	B_domain = domain_matrix[index_-Nx*Ny];
	  
	valid_cell_OK = 
	  (E_solve_phase_OK[index_] && (current_domain != E_domain)) || 
	  (W_solve_phase_OK[index_] && (current_domain != W_domain)) || 
	  (N_solve_phase_OK[index_] && (current_domain != N_domain)) || 
	  (S_solve_phase_OK[index_] && (current_domain != S_domain)) ||
	  (T_solve_phase_OK[index_] && (current_domain != T_domain)) ||
	  (B_solve_phase_OK[index_] && (current_domain != B_domain));
    
	if(P_solve_phase_OK[index_] && valid_cell_OK){
#pragma omp critical
	  {
	    Conjugate_boundary_indexs[Conjugate_boundary_cell_count] = index_;
	    I_Conjugate_v[Conjugate_boundary_cell_count] = I;
	    J_Conjugate_v[Conjugate_boundary_cell_count] = J;
	    K_Conjugate_v[Conjugate_boundary_cell_count] = K;
	    Conjugate_boundary_cell_count++;
	  }
	}
      }
    }
  }

  return;
}

/* RELAX_COEFFS_3D */
void relax_coeffs_3D(const double *field, const int *sel_mat, const int sel_val, 
		     Coeffs coeffs_, const int Nx, const int Ny, const int Nz, const double alpha){
  int I, J, K, index_;

  double *aP = coeffs_.aP;
  double *b  = coeffs_.b;

#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(sel_mat, b, aP, field)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = K*Nx*Ny + J*Nx + I;
	if(sel_mat[index_] == sel_val){
	  b[index_] = b[index_] + ((1 - alpha) * aP[index_] / alpha) * field[index_];
	  aP[index_] = aP[index_] / alpha;
	}
      }
    }
  }

  return;
}

/* COMPUTE_RESIDUES_3D */
/*****************************************************************************/
/**
  
  Gets the absolute maximum value of residue for given field and coeffs values.

  @param phi, coeffs, Nx, Ny, Nz

  @return norm_phi.
    
  MODIFIED: 20-11-2019
*/
double compute_residues_3D(double *phi, Coeffs *coeffs, 
			   const int Nx, const int Ny, const int Nz){
  
  int I, J, K, index_;
  double norm_phi = 0;
  double r_ABS;
	
  double *aP = coeffs->aP;
  double *aE = coeffs->aE;
  double *aW = coeffs->aW;
  double *aN = coeffs->aN;
  double *aS = coeffs->aS;
  double *aT = coeffs->aT;
  double *aB = coeffs->aB;
  double *b = coeffs->b;
  
#pragma omp parallel for default(none)		\
  private(I, J, K, index_, r_ABS)		\
  shared(b, aE, aW, aN, aS, aT, aB, aP, phi)	\
  reduction(max: norm_phi)

  for(K=1; K<(Nz-1); K++){
    for(J=1; J<(Ny-1); J++){
      for(I=1; I<(Nx-1); I++){
	
	index_ = K*Nx*Ny + J*Nx + I;

	r_ABS = fabs(b[index_] + 
		     aE[index_] * phi[index_+1] + aW[index_] * phi[index_-1] + 
		     aN[index_] * phi[index_+Nx] + aS[index_] * phi[index_-Nx] +
		     aT[index_] * phi[index_+Nx*Ny] + aB[index_] * phi[index_-Nx*Ny] - 
		     aP[index_] * phi[index_]);
      
	if (r_ABS > norm_phi){
	  norm_phi = r_ABS;
	}
      }
    }
  }
	
  return norm_phi;	
}

/* COMPUTE_NORM_GLOB_RES_3D */
/*****************************************************************************/
/**
  
  Computes global normalized residual as sum (a_nb*phi_nb + b - a_P*phi_P) / sum (a_P * phi_aP)

  @return  glob_res / norm_factor.
  
  MODIFIED: 27-11-2019
*/
double compute_norm_glob_res_3D(const double *field, Coeffs *coeffs_,
				const int Nx, const int Ny, const int Nz){

  int I, J, K, index_;

  double norm_factor = 0;
  double glob_res = 0;

  const double *aP = coeffs_->aP;
  const double *aW = coeffs_->aW;
  const double *aE = coeffs_->aE;
  const double *aS = coeffs_->aS;
  const double *aN = coeffs_->aN;
  const double *aB = coeffs_->aB;
  const double *aT = coeffs_->aT;
  const double *b = coeffs_->b;

#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(aW, aE, aS, aN, aP, aT, aB, b, field) reduction(+:norm_factor, glob_res)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	norm_factor += fabs(aP[index_] * field[index_]);
	glob_res += fabs(aW[index_] * field[index_-1] +
			 aE[index_] * field[index_+1] +
			 aS[index_] * field[index_-Nx] +
			 aN[index_] * field[index_+Nx] +
			 aB[index_] * field[index_-Nx*Ny] +
			 aT[index_] * field[index_+Nx*Ny] +
			 b[index_] - aP[index_] * field[index_]);
      }
    }
  }

  return (glob_res / norm_factor);
}

/* MAKE_P_INIT_PROFILE_3D */
int make_p_init_profile_3D(Data_Mem *data){
  
  int ret_value = 0;
  int I, J, K, index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int p_gradient_direction = data->p_gradient_direction;

  double lmin, lmax;

  double p_aux;
  double p0 = data->p0;
  double pL = data->pL;
  const double rise_to = data->rise_to;

  double *p = data->p;

  const double *x = data->nodes_x;
  const double *y = data->nodes_y;
  const double *z = data->nodes_z;
  
  Wins *wins = &(data->wins);


/* 0: south->north  1: north->south */
  if((p_gradient_direction == 0) || (p_gradient_direction == 1)){

    lmin = y[Nx*Ny + 1*Nx + 1];
    lmax = y[Nx*Ny + (Ny-2)*Nx + 1];

    /* Redefine values */
    if(p_gradient_direction == 1){
      p_aux = p0;
      p0 = pL;
      pL = p_aux;
    }

    /* Internal nodes */

#pragma omp parallel for default(none)		\
  private(I, J, K, index_)			\
  shared(y, p, lmin, lmax, p0, pL)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
      
	  index_ = K*Nx*Ny + J*Nx + I;      
	  p[index_] = p0 + (pL - p0) * powf((lmin - y[index_]) / 
					    (lmin - lmax), rise_to);      
	}
      }
    }

    /* West - East faces */

    I = 0;

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	/* West index */
	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = p0 + (pL - p0) * powf((lmin - y[index_]) /
					  (lmin - lmax), rise_to);
	/* East value equals West value */
	p[index_ + Nx-1] = p[index_];
      }
    }

    /* Bottom - Top faces */

    K = 0;

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	/* Bottom index */
	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = p0 + (pL - p0) * powf((lmin - y[index_]) /
					  (lmin - lmax), rise_to);
	/* Top value equals Bottom value */
	p[index_+ (Nz-1)*Nx*Ny] = p[index_];
      }
    }

    /* South face */
    
    J = 0;

    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){
      
	index_ = K*Nx*Ny + J*Nx + I;      
	p[index_] = p0;
      
      }
    }

    /* North face */
    J = Ny - 1;

    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){
    
	index_ = K*Nx*Ny + J*Nx + I;    
	p[index_] = pL;
    
      }
    }

    if(p_gradient_direction == 0){
      info_msg(wins, "Pressure initialized for S ==> N flow");
    }
    else{
      info_msg(wins, "Pressure initialized for N ==> S flow");
    }
  
  }

  /* 2: west->east  3: east->west */
  if((p_gradient_direction == 2) || (p_gradient_direction == 3)){
    
    lmin = x[Nx*Ny + 1*Nx + 1];
    lmax = x[Nx*Ny + 1*Nx + Nx-2];

    /* Redefine values */
    if(p_gradient_direction == 3){
      p_aux = p0;
      p0 = pL;
      pL = p_aux;
    }

    /* Internal nodes */
    
#pragma omp parallel for default(none)		\
  private(I, J, K, index_)			\
  shared(x, p, lmin, lmax, p0, pL)
   
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
      
	  index_ = K*Nx*Ny + J*Nx + I;	  
	  p[index_] = p0 + (pL - p0) * powf((lmin - x[index_]) / 
					   (lmin - lmax), rise_to);     
	}
      }
    }

    /* South - North faces */

    J = 0;

    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){
	/* South index */
	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = p0 + (pL - p0) * powf((lmin - x[index_]) /
					  (lmin - lmax), rise_to);
	/* North value equals South value */
	p[index_+(Ny-1)*Nx] = p[index_];
      }
    }

    /* Bottom - Top faces */

    K = 0;

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	/* Bottom index */
	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = p0 + (pL - p0) * powf((lmin - x[index_]) /
					  (lmin - lmax), rise_to);
	/* Top value equals Bottom value */
	p[index_+(Nz-1)*Nx*Ny] = p[index_];
      }
    }

    /* West face */
    I = 0;

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
      
	index_ = K*Nx*Ny + J*Nx + I;      
	p[index_] = p0;
      
      }
    }

    /* East face */
    I = Nx - 1;

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
    
	index_ = K*Nx*Ny + J*Nx + I;    
	p[index_] = pL;
    
      }
    }

    if(p_gradient_direction == 2){
      info_msg(wins, "Pressure initialized for W ==> E flow");
    }
    else{
      info_msg(wins, "Pressure initialized for E ==> W flow");
    }
  
  }

  /* 4: bottom->top  5: top->bottom */
  if((p_gradient_direction == 4) || (p_gradient_direction == 5)){

    lmin = z[Nx*Ny + 1*Nx + 1];
    lmax = z[(Nz-2)*Nx*Ny + 1*Nx + 1];

    /* Redefine values */
    if(p_gradient_direction == 5){
      p_aux = p0;
      p0 = pL;
      pL = p_aux;
    }

    /* Internal nodes */

#pragma omp parallel for default(none)		\
  private(I, J, K, index_) \
  shared(z, p, lmin, lmax, p0, pL)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;
	  p[index_] = p0 + (pL - p0) * powf((lmin - z[index_]) /
					    (lmin - lmax), rise_to);
	}
      }
    }

    /* South - North faces */

    J = 0;

    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){
	/* South index */
	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = p0 + (pL - p0) * powf((lmin - z[index_]) /
					  (lmin - lmax), rise_to);
	/* North value equals South value */
	p[index_+(Ny-1)*Nx] = p[index_];
      }
    }

    /* West - East faces */

    I = 0;

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	/* West index */
	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = p0 + (pL - p0) * powf((lmin - z[index_]) /
					  (lmin- lmax), rise_to);
	/* East value equals West value */
	p[index_+Nx-1] = p[index_];
      }
    }

    /* Bottom face */
    K = 0;

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = p0;
      }
    }

    /* Top face */
    K = Nz - 1;

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	p[index_] = pL;
      }
    }


    if(p_gradient_direction == 4){
      info_msg(wins, "Pressure initialized for B ==> T flow");
    }
    else{
      info_msg(wins, "Pressure initialized for T ==> B flow");
    }
    
  }

  return ret_value;
}

/* COMPUTE_FORCE_ON_SOLID_3D */
/*****************************************************************************/
/**

 Report information used to calculate friction factor for 3D case.

 @param data

 @return ret_value not implemented.

*/
void compute_force_on_solid_3D(Data_Mem *data){
    
  int z, index_u, index_v, index_w, index_;
  int I, J, K, i, j, k;
  int p0_is_fluid, p1_is_fluid;
  int step, slave_found, wake_status;
  int wake_length_found = 0;
  int solid_found = 0;
  int domain_end_reached = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int u_sol_factor_count = data->u_sol_factor_count;
  const int v_sol_factor_count = data->v_sol_factor_count;
  const int w_sol_factor_count = data->w_sol_factor_count;
  const int u_slave_count = data->u_slave_count;
  const int v_slave_count = data->v_slave_count;
  const int w_slave_count = data->w_slave_count;
  const int w_type = data->bc_flow_west.type;
  const int e_type = data->bc_flow_east.type;
  const int s_type = data->bc_flow_south.type;
  const int n_type = data->bc_flow_north.type;
  const int b_type = data->bc_flow_bottom.type;
  const int t_type = data->bc_flow_top.type;
  const int write_in_GPU_OK = 0;

  const int *u_sol_factor_index_u = data->u_sol_factor_index_u;
  const int *v_sol_factor_index_v = data->v_sol_factor_index_v;
  const int *w_sol_factor_index_w = data->w_sol_factor_index_w;
  const int *u_sol_factor_index = data->u_sol_factor_index;
  const int *v_sol_factor_index = data->v_sol_factor_index;
  const int *w_sol_factor_index = data->w_sol_factor_index;
  const int *p_dom_matrix = data->p_dom_matrix;
  const int *u_slave_nodes = data->u_slave_nodes;
  const int *v_slave_nodes = data->v_slave_nodes;
  const int *w_slave_nodes = data->w_slave_nodes;
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;

  double v_inlet, rsqr, diam;
  double p0, p1;
  double flow_dir, wake_L;

  const double mu = data->mu;
  const double w_u = data->bc_flow_west.v_value[0];
  const double e_u = data->bc_flow_east.v_value[0];
  const double s_v = data->bc_flow_south.v_value[1];
  const double n_v = data->bc_flow_north.v_value[1];
  const double b_w = data->bc_flow_bottom.v_value[2];
  const double t_w = data->bc_flow_top.v_value[2];

  double solid_surf_coord[3];
  double wake_coord[3];
  double solid_center_coord[3];

  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  const double *p = data->p;  
  const double *constant_d = data->constant_d;
  const double *tcompx = data->tcompx;
  const double *tcompy = data->tcompy;
  const double *tcompz = data->tcompz;
  const double *u_sol_factor = data->u_sol_factor;
  const double *v_sol_factor = data->v_sol_factor;
  const double *w_sol_factor = data->w_sol_factor;
  const double *u_p_factor = data->u_p_factor;
  const double *v_p_factor = data->v_p_factor;
  const double *w_p_factor = data->w_p_factor;
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;
  const double *nodes_x_w = data->nodes_x_w;
  const double *nodes_y_w = data->nodes_y_w;
  const double *nodes_z_w = data->nodes_z_w;
  const double *u_factor_delta_h = data->u_factor_delta_h;
  const double *v_factor_delta_h = data->v_factor_delta_h;
  const double *w_factor_delta_h = data->w_factor_delta_h;
  const double *u_p_factor_W = data->u_p_factor_W;
  const double *v_p_factor_S = data->v_p_factor_S;
  const double *w_p_factor_B = data->w_p_factor_B;

  mat_t *matfp = NULL;

  Wins *wins = &(data->wins);

  char post_process_info_file[SIZE];
  
  const char *post_process_filename = data->post_process_filename;
  const char *results_dir = data->results_dir;

  FILE *fp = NULL;

  /* OBS: POSTPROCESO VALIDO PARA UNA SOLA CURVA Y UNA SOLA ENTRADA
     OBS: LLEVAR A CABO POSTPROCESO ANTES DE CENTRAR LAS COMPONENTES DE LA VELOCIDAD */

  /* Search for boundary with fixed value boundary condition */

  if(w_type == 1){
    v_inlet = w_u;
  }
  else if(e_type == 1){
    v_inlet = e_u;
  }
  else if(s_type == 1){
    v_inlet = s_v;
  }
  else if(n_type == 1){
    v_inlet = n_v;
  }
  else if(b_type == 1){
    v_inlet = b_w;
  }
  else if(t_type == 1){
    v_inlet = t_w;
  }
  
  rsqr = -constant_d[0];
  solid_center_coord[0] = tcompx[0];
  solid_center_coord[1] = tcompy[0];
  solid_center_coord[2] = tcompz[0];
  diam = 2 * sqrt(rsqr);

  /* Open .mat file to store postprocessing results */
  
  matfp = get_matfp(post_process_filename, 1, wins);
  
  /* U */
  
  data->u_sol_factor_x = create_array(u_sol_factor_count, 1, 1, 0);
  data->u_sol_factor_y = create_array(u_sol_factor_count, 1, 1, 0);
  data->u_sol_factor_z = create_array(u_sol_factor_count, 1, 1, 0);
  data->u_sol_factor_u = create_array(u_sol_factor_count, 1, 1, 0);
  data->u_sol_factor_F = create_array(u_sol_factor_count, 1, 1, 0);
  data->u_sol_factor_tau = create_array(u_sol_factor_count, 1, 1, 0);

  double *u_sol_factor_x = data->u_sol_factor_x;
  double *u_sol_factor_y = data->u_sol_factor_y;
  double *u_sol_factor_z = data->u_sol_factor_z;
  double *u_sol_factor_u = data->u_sol_factor_u;
  double *u_sol_factor_F = data->u_sol_factor_F;
  double *u_sol_factor_tau = data->u_sol_factor_tau;
  
  /* Calculate derived values */
  
  for(z=0; z<u_sol_factor_count; z++){
    
    index_u = u_sol_factor_index_u[z];
    u_sol_factor_x[z] = nodes_x_u[index_u];
    u_sol_factor_y[z] = nodes_y_u[index_u];
    u_sol_factor_z[z] = nodes_z_u[index_u];
    u_sol_factor_u[z] = u[index_u];
    u_sol_factor_F[z] = mu * u_sol_factor[z] * u_sol_factor_u[z];
    u_sol_factor_tau[z] = mu * u_sol_factor_u[z] / u_factor_delta_h[z];
  }


  /* Write fields to file */
  write_int_array_in_MAT_file(data->u_sol_type, "u_sol_factor_type",
			      u_sol_factor_count, 1, 1, matfp);
  write_int_array_in_MAT_file(data->v_sol_type, "v_sol_factor_type",
			      v_sol_factor_count, 1, 1, matfp);
  write_int_array_in_MAT_file(data->w_sol_type, "w_sol_factor_type",
			      w_sol_factor_count, 1, 1, matfp);
  
  write_int_array_in_MAT_file(data->u_sol_factor_index_u, "u_sol_factor_index_u",
			      u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_x, "u_sol_factor_x",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_y, "u_sol_factor_y",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_z, "u_sol_factor_z",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor, "u_sol_factor",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_u, "u_sol_factor_u",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_factor_delta_h, "u_sol_factor_delta_h",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_factor_area, "u_sol_factor_area",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_F, "u_sol_factor_F",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_tau, "u_sol_factor_tau",
			  u_sol_factor_count, 1, 1, matfp);


 /* V */

  data->v_sol_factor_x = create_array(v_sol_factor_count, 1, 1, 0);
  data->v_sol_factor_y = create_array(v_sol_factor_count, 1, 1, 0);
  data->v_sol_factor_z = create_array(v_sol_factor_count, 1, 1, 0);
  data->v_sol_factor_v = create_array(v_sol_factor_count, 1, 1, 0);
  data->v_sol_factor_F = create_array(v_sol_factor_count, 1, 1, 0);
  data->v_sol_factor_tau = create_array(v_sol_factor_count, 1, 1, 0);

  double *v_sol_factor_x = data->v_sol_factor_x;
  double *v_sol_factor_y = data->v_sol_factor_y;
  double *v_sol_factor_z = data->v_sol_factor_z;
  double *v_sol_factor_v = data->v_sol_factor_v;
  double *v_sol_factor_F = data->v_sol_factor_F;
  double *v_sol_factor_tau = data->v_sol_factor_tau;  

  /* Calculate derived values */
  
  for(z=0; z<v_sol_factor_count; z++){
    
    index_v = v_sol_factor_index_v[z];
    v_sol_factor_x[z] = nodes_x_v[index_v];
    v_sol_factor_y[z] = nodes_y_v[index_v];
    v_sol_factor_z[z] = nodes_z_v[index_v];
    v_sol_factor_v[z] = v[index_v];
    v_sol_factor_F[z] = mu * v_sol_factor[z] * v_sol_factor_v[z];
    v_sol_factor_tau[z] = mu * v_sol_factor_v[z] / v_factor_delta_h[z];
  }  

  /* Write fields to file */
  
  write_int_array_in_MAT_file(data->v_sol_factor_index_v, "v_sol_factor_index_v",
			      v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_sol_factor_x, "v_sol_factor_x",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_sol_factor_y, "v_sol_factor_y",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_sol_factor_z, "v_sol_factor_z",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_sol_factor, "v_sol_factor",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_sol_factor_v, "v_sol_factor_v",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_factor_delta_h, "v_sol_factor_delta_h",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_factor_area, "v_sol_factor_area",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_sol_factor_F, "v_sol_factor_F",
			  v_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->v_sol_factor_tau, "v_sol_factor_tau",
			  v_sol_factor_count, 1, 1, matfp);


  /* W */

  data->w_sol_factor_x = create_array(w_sol_factor_count, 1, 1, 0);
  data->w_sol_factor_y = create_array(w_sol_factor_count, 1, 1, 0);
  data->w_sol_factor_z = create_array(w_sol_factor_count, 1, 1, 0);
  data->w_sol_factor_w = create_array(w_sol_factor_count, 1, 1, 0);
  data->w_sol_factor_F = create_array(w_sol_factor_count, 1, 1, 0);
  data->w_sol_factor_tau = create_array(w_sol_factor_count, 1, 1, 0);

  double *w_sol_factor_x = data->w_sol_factor_x;
  double *w_sol_factor_y = data->w_sol_factor_y;
  double *w_sol_factor_z = data->w_sol_factor_z;
  double *w_sol_factor_w = data->w_sol_factor_w;
  double *w_sol_factor_F = data->w_sol_factor_F;
  double *w_sol_factor_tau = data->w_sol_factor_tau; 
  
  /* Calculate derived values */
  
  for(z=0; z<w_sol_factor_count; z++){
    
    index_w = w_sol_factor_index_w[z];
    w_sol_factor_x[z] = nodes_x_w[index_w];
    w_sol_factor_y[z] = nodes_y_w[index_w];
    w_sol_factor_z[z] = nodes_z_w[index_w];
    w_sol_factor_w[z] = w[index_w];
    w_sol_factor_F[z] = mu * w_sol_factor[z] * w_sol_factor_w[z];
    w_sol_factor_tau[z] = mu * w_sol_factor_w[z] / w_factor_delta_h[z];
  }

  write_int_array_in_MAT_file(data->w_sol_factor_index_w, "w_sol_factor_index_w",
			      w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_sol_factor_x, "w_sol_factor_x",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_sol_factor_y, "w_sol_factor_y",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_sol_factor_z, "w_sol_factor_z",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_sol_factor, "w_sol_factor",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_sol_factor_w, "w_sol_factor_w",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_factor_delta_h, "w_sol_factor_delta_h",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_factor_area, "w_sol_factor_area",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_sol_factor_F, "w_sol_factor_F",
			  w_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->w_sol_factor_tau, "w_sol_factor_tau",
			  w_sol_factor_count, 1, 1, matfp);

  /* U */

  data->u_sol_factor_p = create_array(u_sol_factor_count, 1, 1, NAN);
  data->u_sol_factor_F_p = create_array(u_sol_factor_count, 1, 1, 0);

  double *u_sol_factor_p = data->u_sol_factor_p;
  double *u_sol_factor_F_p = data->u_sol_factor_F_p;
  
  for(z=0; z<u_sol_factor_count; z++){
    
    index_ = u_sol_factor_index[z];
    p0_is_fluid = (p_dom_matrix[index_] == 0);
    p1_is_fluid = (p_dom_matrix[index_-1] == 0);
    p0 = p[index_];
    p1 = p[index_-1];
 
    if(p0_is_fluid && p1_is_fluid){
      u_sol_factor_p[z] = p0 * (1 - u_p_factor_W[z]) + p1 * u_p_factor_W[z];
    }
    else if(p0_is_fluid){
      u_sol_factor_p[z] = p0;
    }
    else if(p1_is_fluid){
      u_sol_factor_p[z] = p1;      
    }

    u_sol_factor_F_p[z] = - u_sol_factor_p[z] * u_p_factor[z];
  }

  write_array_in_MAT_file(data->u_p_factor, "u_p_factor",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_p, "u_sol_factor_p",
			  u_sol_factor_count, 1, 1, matfp);
  write_array_in_MAT_file(data->u_sol_factor_F_p, "u_sol_factor_F_p",
			  u_sol_factor_count, 1, 1, matfp);

  /* V */
  
  data->v_sol_factor_p = create_array(v_sol_factor_count, 1, 1, NAN);
  data->v_sol_factor_F_p = create_array(v_sol_factor_count, 1, 1, 0);

  double *v_sol_factor_p = data->v_sol_factor_p;
  double *v_sol_factor_F_p = data->v_sol_factor_F_p;

   for(z=0; z<v_sol_factor_count; z++){
     
    index_ = v_sol_factor_index[z];
    p0_is_fluid = (p_dom_matrix[index_] == 0);
    p1_is_fluid = (p_dom_matrix[index_-Nx] == 0);
    p0 = p[index_];
    p1 = p[index_-Nx];

    if(p0_is_fluid && p1_is_fluid){
      v_sol_factor_p[z] = p0 * (1 - v_p_factor_S[z]) + p1 * v_p_factor_S[z];
    }
    else if(p0_is_fluid){
      v_sol_factor_p[z] = p0;
    }
    else if(p1_is_fluid){
      v_sol_factor_p[z] = p1;
    }

    v_sol_factor_F_p[z] = -v_sol_factor_p[z] * v_p_factor[z];
   }
   
   write_array_in_MAT_file(data->v_p_factor, "v_p_factor",
			   v_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->v_sol_factor_p, "v_sol_factor_p",
			   v_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->v_sol_factor_F_p, "v_sol_factor_F_p",
			   v_sol_factor_count, 1, 1, matfp);

   /* W */

   data->w_sol_factor_p = create_array(w_sol_factor_count, 1, 1, NAN);
   data->w_sol_factor_F_p = create_array(w_sol_factor_count, 1, 1, NAN);

   double *w_sol_factor_p = data->w_sol_factor_p;
   double *w_sol_factor_F_p = data->w_sol_factor_F_p;

   for(z=0; z<w_sol_factor_count; z++){
     
     index_ = w_sol_factor_index[z];
     p0_is_fluid = (p_dom_matrix[index_] == 0);
     p1_is_fluid = (p_dom_matrix[index_-Nx*Ny] == 0);
     p0 = p[index_];
     p1 = p[index_-Nx*Ny];

     if(p0_is_fluid && p1_is_fluid){
       w_sol_factor_p[z] = p0 * (1 - w_p_factor_B[z]) + p1 * w_p_factor_B[z];
     }
     else if(p0_is_fluid){
       w_sol_factor_p[z] = p0;
     }
     else if(p1_is_fluid){
       w_sol_factor_p[z] = p1;
     }

     w_sol_factor_F_p[z] = -w_sol_factor_p[z] * w_p_factor[z];
   }

   write_array_in_MAT_file(data->w_p_factor, "w_p_factor",
			   w_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->w_sol_factor_p, "w_sol_factor_p",
			   w_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->w_sol_factor_F_p, "w_sol_factor_F_p",
			   w_sol_factor_count, 1, 1, matfp);
   

   /* Write post_process_info.dat file */

   if(results_dir == NULL && !write_in_GPU_OK){
     snprintf(post_process_info_file, SIZE, "./data/CPU/%s",
	      "post_process_info.dat");
   }
   else if(results_dir == NULL && write_in_GPU_OK){
     snprintf(post_process_info_file, SIZE, "./data/GPU/%s",
	      "post_process_info.dat");
   }
   else{
     snprintf(post_process_info_file, SIZE, "%s%s",
	      results_dir, "post_process_info.dat");
   }

   fp = fopen(post_process_info_file, "w");

   if(fp == NULL){
     error_msg(wins, "Could not create/open post_process_info_file file\n");
   }
   
   fprintf(fp, "_sol_factor_x, y, z: coordinates of velocity node, [m].\n");
   fprintf(fp, "_sol_factor: factor associated to tangential force due to solid, [m]\n");
   fprintf(fp, "_sol_factor_u, v, w: velocity values, [m/s]\n");
   fprintf(fp, "_sol_factor_delta_h: distance from velocity node to solid surface, [m]\n");
   fprintf(fp, "_sol_factor_area: solid area of cut cell, [m**2]\n");
   fprintf(fp, "_sol_factor_F: force in tangential direction due to solid surface, [kg m/s**2]\n");
   fprintf(fp, "_sol_factor_tau: stress at solid surface in tangential direction, [Pa]\n");

   fprintf(fp, "_p_factor: factor associated to perpendicular force due to solid, [m**2]\n");
   fprintf(fp, "_sol_factor_p: pressure at solid surface, [Pa]\n");
   fprintf(fp, "_sol_factor_F_p: force in perpendicular direction due to solid surface, [kg m /s**2]\n");

   fprintf(fp, "_sol_factor_inters_x, y, z: coordinates approximate center of solid surface, [m]\n");

   fprintf(fp, "mu: fluid dynamic viscosity, [Pa*s]\n");
   fprintf(fp, "rho: fluid density, [kg/m3]\n");
   fprintf(fp, "D: solid diameter, [m]\n");
   fprintf(fp, "v_inf: approximation velocity, [m/s]\n");
   fprintf(fp, "solid_center_coord: coordinates of solid center, [m]\n");

   fprintf(fp, "wake_status: 1: reached solid surface, 2: found wake length, 3: reached end of domain.\n");
   fprintf(fp, "solid_surf_coord: solid surface coordinates, if found, [m]\n");
   fprintf(fp, "wake_coord: wake end coordinates, if found, [m]\n"); 
     
   fprintf(fp, "_sol_type: 0: regular fluid node, 1: slave node, 2: solid node\n");

  /* Cell classification */

   /* U */

   write_array_in_MAT_file(data->u_factor_inters_x, "u_sol_factor_inters_x",
			   u_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->u_factor_inters_y, "u_sol_factor_inters_y",
			   u_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->u_factor_inters_z, "u_sol_factor_inters_z",
			   u_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->v_factor_inters_x, "v_sol_factor_inters_x",
			   v_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->v_factor_inters_y, "v_sol_factor_inters_y",
			   v_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->v_factor_inters_z, "v_sol_factor_inters_z",
			   v_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->w_factor_inters_x, "w_sol_factor_inters_x",
			   w_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->w_factor_inters_y, "w_sol_factor_inters_y",
			   w_sol_factor_count, 1, 1, matfp);
   write_array_in_MAT_file(data->w_factor_inters_z, "w_sol_factor_inters_z",
			   w_sol_factor_count, 1, 1, matfp);        

   /* Wake length calculations*/
   
   /* Inlet flow in the horizontal direction */
   
   if((w_type == 1) || (e_type == 1)){

    step = (w_type == 1)? -1 : 1;
    i = (w_type == 1)? nx-1 : 0;
    flow_dir = (w_type == 1)? 1 : -1;

    J = Ny / 2;
    K = Nz / 2;

    /* Search for a change of sign of velocity, that indicates presence of closed streamline */

    while((!solid_found) && (!wake_length_found) && (!domain_end_reached)){
      i += step;
      index_u = K*nx*Ny + J*nx + i;

      if(u_dom_matrix[index_u]){
	solid_found = 1;
	wake_status = 1;
	solid_surf_coord[0] = nodes_x_u[index_u];
	solid_surf_coord[1] = nodes_y_u[index_u];
	solid_surf_coord[2] = nodes_z_u[index_u];	
      }
      else if(flow_dir * u[index_u] < 0){
	wake_length_found = 1;
	wake_status = 2;
	wake_coord[0] = nodes_x_u[index_u];
	wake_coord[1] = nodes_y_u[index_u];
	wake_coord[2] = nodes_z_u[index_u];	
      }
      if((i == 0) || (i == nx-1)){
	domain_end_reached = 1;
	wake_status = 3;
      }
    }

    write_int_array_in_MAT_file(&wake_status, "wake_status", 1, 1, 1, matfp);
    
    if(solid_found){
      write_array_in_MAT_file(solid_surf_coord, "solid_surf_coord", 3, 1, 1, matfp);
    }
    else if(wake_length_found){

      wake_L = (nodes_x_u[index_u] > solid_center_coord[0])?
	nodes_x_u[index_u] - solid_center_coord[0] - sqrt(rsqr) :
	solid_center_coord[0] - sqrt(rsqr) - nodes_x_u[index_u];

      write_array_in_MAT_file(&wake_L, "wake_L", 1, 1, 1, matfp);
      write_array_in_MAT_file(wake_coord, "wake_coord", 3, 1, 1, matfp);
    }
    
  }
  /* Inlet flow in the vertical direction */
  else if((s_type ==1) || (n_type == 1)){

    solid_found = 0;
    wake_length_found = 0;
    domain_end_reached = 0;

    step = (s_type == 1)? -1 : 1;
    j = (s_type == 1)? ny - 1 : 0;
    flow_dir = (s_type == 1)? 1 : -1;
    
    I = Nx / 2;
    K = Nz / 2;

    while((!solid_found) && (!wake_length_found) && (!domain_end_reached)){
      j += step;
      index_v = K*Nx*ny + j*Nx + I;

      if(v_dom_matrix[index_v]){
	solid_found = 1;
	wake_status = 1;
	solid_surf_coord[0] = nodes_x_v[index_v];
	solid_surf_coord[1] = nodes_y_v[index_v];
	solid_surf_coord[2] = nodes_z_v[index_v];	
      }
      else if(flow_dir * v[index_v] < 0){
	wake_length_found = 1;
	wake_status = 2;
	wake_coord[0] = nodes_x_v[index_v];
	wake_coord[1] = nodes_y_v[index_v];
	wake_coord[2] = nodes_z_v[index_v];
      }
      if((j == 0) || (j == ny-1)){
	domain_end_reached = 1;
	wake_status = 3;
      }
    }

    write_int_array_in_MAT_file(&wake_status, "wake_status", 1, 1, 1, matfp);

    if(solid_found){
      write_array_in_MAT_file(solid_surf_coord, "solid_surf_coord", 3, 1, 1, matfp);
    }
    else if(wake_length_found){

      wake_L = (nodes_y_v[index_v] > solid_center_coord[1])?
	nodes_y_v[index_v] - solid_center_coord[1] - sqrt(rsqr) :
	solid_center_coord[1] - sqrt(rsqr) - nodes_y_v[index_v];

      write_array_in_MAT_file(&wake_L, "wake_L", 1, 1, 1, matfp);      
      write_array_in_MAT_file(wake_coord, "wake_coord", 3, 1, 1, matfp);            
    }
          
  }
  else if((b_type == 1) || (t_type == 1)){

    solid_found = 0;
    wake_length_found = 0;
    domain_end_reached = 0;

    step = (b_type == 1)? -1 : 1;
    k = (b_type == 1)? nz - 1 : 0;
    flow_dir = (b_type == 1)? 1 : -1;

    I = Nz / 2;
    J = Ny / 2;

    while((!solid_found) && (!wake_length_found) && (!domain_end_reached)){
      k += step;
      index_w = k*Nx*Ny + J*Nx + I;

      if(w_dom_matrix[index_w]){
	solid_found = 1;
	wake_status = 1;
	solid_surf_coord[0] = nodes_x_w[index_w];
	solid_surf_coord[1] = nodes_y_w[index_w];
	solid_surf_coord[2] = nodes_z_w[index_w];
      }
      else if(flow_dir * w[index_w] < 0){
	wake_length_found = 1;
	wake_status = 2;
	wake_coord[0] = nodes_x_w[index_w];
	wake_coord[1] = nodes_y_w[index_w];
	wake_coord[2] = nodes_z_w[index_w];
      }
      if((k == 0) || (k == nz-1)){
	domain_end_reached = 1;
	wake_status = 3;
      }
    }

    write_int_array_in_MAT_file(&wake_status, "wake_status", 1, 1, 1, matfp);
    
    if(solid_found){
      write_array_in_MAT_file(solid_surf_coord, "solid_surf_coord", 3, 1, 1, matfp);
    }
    else if(wake_length_found){

      wake_L = (nodes_z_w[index_w] > solid_center_coord[2])?
	nodes_z_w[index_w] - solid_center_coord[2] - sqrt(rsqr) :
	solid_center_coord[2] - sqrt(rsqr) - nodes_z_w[index_w];

      write_array_in_MAT_file(&wake_L, "wake_L", 1, 1, 1, matfp);
      write_array_in_MAT_file(wake_coord, "wake_coord", 3, 1, 1, matfp);      
    }      
    
  }

  /* Physical and simulation parameters */
  
  write_array_in_MAT_file(&(data->mu), "mu", 1, 1, 1, matfp);
  write_array_in_MAT_file(data->rho_v, "rho", 1, 1, 1, matfp);
  write_array_in_MAT_file(&diam, "D", 1, 1, 1, matfp);
  write_array_in_MAT_file(&v_inlet, "v_inf", 1, 1, 1, matfp);
  write_array_in_MAT_file(solid_center_coord, "solid_center_coord", 3, 1, 1, matfp);
  
  Mat_Close(matfp);
  fclose(fp);

  return;
}

/* WRITE_IN_VEL_3D */

void write_in_vel_3D(Data_Mem *data){
  
  int I, J, K, flow_SN, flow_WE, flow_BT;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int w_type = data->bc_flow_west.type;  
  const int e_type = data->bc_flow_east.type;
  const int s_type = data->bc_flow_south.type;
  const int n_type = data->bc_flow_north.type;
  const int b_type = data->bc_flow_bottom.type;
  const int t_type = data->bc_flow_top.type;

  static int count_flow = 0;  

  double sum_in = 0;
  double sum_out = 0;

  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;
  
  Wins *wins = &(data->wins);

  const Sigmas *sigmas = &(data->sigmas);
  
  FILE *fp = NULL;

  const char *avg_iv_filename = data->avg_iv_filename;  
  
  fp = fopen(avg_iv_filename, "a");

  if(fp == NULL){
    error_msg(wins, "Could not create/open file for average inlet velocity");
  }
  
  flow_SN = (n_type == 5) || (s_type == 5);
  flow_WE = (w_type == 5) || (e_type == 5);
  flow_BT = (b_type == 5) || (t_type == 5);

  if(flow_WE){
    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	sum_in += u[K*nx*Ny + J*nx];
	sum_out += u[K*nx*Ny + J*nx + nx-1];
      }
    }
  }
  else if(flow_SN){   
    for(K=1; K<Nz-1; K++){
      for(I=1; I<Nx-1; I++){
	sum_in += v[K*Nx*ny + I];
	sum_out += v[K*Nx*ny + (ny-1)*Nx + I];
      }
    }    
  }
  else if(flow_BT){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	sum_in += w[J*Nx + I];
	sum_out += w[(nz-1)*Nx*Ny + J*Nx + I];
      }
    }
  }

  if(count_flow == 0){

    if(flow_WE){
      fprintf(fp, "%% it %11s %11s\n", "West face","East face");
    }
    else if(flow_SN){
      fprintf(fp, "%% it %11s %11s\n", "South face", "North face");
    }
    else if(flow_BT){
      fprintf(fp, "%% it %11s %11s\n", "Bottom face", "Top face");      
    }
      
    fprintf(fp, "avg_inlet_vel = [\n");
      
  }
  else{

    if(flow_WE){
      fprintf(fp, "%i %11.5g %11.5g\n",
	      sigmas->it, sum_in / ((Nz-2)*(Ny-2)), sum_out / ((Nz-2)*(Ny-2)));      
    }
    else if(flow_SN){   
      fprintf(fp, "%i %11.5g %11.5g\n",
	      sigmas->it, sum_in / ((Nz-2)*(Nx-2)), sum_out / ((Nz-2)*(Nx-2)));
    }    
    else if(flow_BT){
      fprintf(fp, "%i %11.5g %11.5g\n",
	      sigmas->it, sum_in / ((Ny-2)*(Nx-2)), sum_out / ((Ny-2)*(Nx-2)));
    }
    else{
      fprintf(fp, "%i %11.5g %11.5g\n",
	      sigmas->it, 0.0, 0.0);      
    }
  }
  count_flow++;

  
  fclose(fp);
  
  return;  
}

/* READ_FROM_GEOM_FILE */
/*****************************************************************************/
/**
   Requires libmatio-devel >= 1.5.12

   @param data
   
   @return 0 on success, 1 if file couldn't be opened.

   @see write_to_geom_file 

   Data read:
*/
int read_from_geom_file(Data_Mem *data){

  mat_t *matfp = NULL;

  Wins *wins = &(data->wins);

  const char *geom_filename = data->geom_filename;

  int compute_cut_cell_jobs_OK;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int solve_3D_OK = data->solve_3D_OK;

  matfp = Mat_Open(geom_filename, MAT_ACC_RDONLY);

  if(matfp == NULL){
    alert_msg(wins, "Could not open geom file!! ... computing cut_cell_jobs instead");
    compute_cut_cell_jobs_OK = 1;
  }
  else{

    /** Data_Mem::u_dom_matrix, Data_Mem::v_dom_matrix. */
    checkCall(read_int_array_from_MAT_file(data->u_dom_matrix, "u_dom_matrix",
					   nx, Ny, Nz, matfp), wins);
    checkCall(read_int_array_from_MAT_file(data->v_dom_matrix, "v_dom_matrix",
					   Nx, ny, Nz, matfp), wins);

    
    if(solve_3D_OK){
      /** if Data_Mem::solve_3D_OK Data_Mem::w_dom_matrix. */      
      checkCall(read_int_array_from_MAT_file(data->w_dom_matrix, "w_dom_matrix",
					     Nx, Ny, nz, matfp), wins);
    }

    /** Data_Mem::p_dom_matrix */    
    checkCall(read_int_array_from_MAT_file(data->p_dom_matrix, "p_dom_matrix",
					   Nx, Ny, Nz, matfp), wins);

    /** Data_Mem::u_cut_matrix, Data_Mem::v_cut_matrix. */
    checkCall(read_int_array_from_MAT_file(data->u_cut_matrix, "u_cut_matrix",
					   nx, Ny, Nz, matfp), wins);
    checkCall(read_int_array_from_MAT_file(data->v_cut_matrix, "v_cut_matrix",
					   Nx, ny, Nz, matfp), wins);

    /** if Data_Mem::solve_3D_OK Data_Mem::w_cut_matrix. */
    if(solve_3D_OK){
      checkCall(read_int_array_from_MAT_file(data->w_cut_matrix, "w_cut_matrix",
					     Nx, Ny, nz, matfp), wins);
    }


    /** Data_Mem::u_faces_x, Data_Mem::u_faces_y */
    checkCall(read_array_from_MAT_file(data->u_faces_x, "u_faces_x",
				       Nx, Ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->u_faces_y, "u_faces_y",
				       nx, ny, Nz, matfp), wins);
    /** if Data_Mem::solve_3D_OK Data_Mem::u_faces_z */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->u_faces_z, "u_faces_z",
					 nx, Ny, nz, matfp), wins);
    }

    /** Data_Mem::v_faces_x, Data_Mem::v_faces_y */
    checkCall(read_array_from_MAT_file(data->v_faces_x, "v_faces_x",
				       nx, ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->v_faces_y, "v_faces_y",
				       Nx, Ny, Nz, matfp), wins);
    /** if Data_Mem::solve_3D_OK Data_Mem::v_faces_z */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->v_faces_z, "v_faces_z",
					 Nx, ny, nz, matfp), wins);
    }

    /** if Data_Mem::solve_3D_OK Data_Mem::w_faces_x, Data_Mem::w_faces_y, Data_Mem::w_faces_z */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->w_faces_x, "w_faces_x",
					 nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->w_faces_y, "w_faces_y",
					 Nx, ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->w_faces_z, "w_faces_z",
					 Nx, Ny, Nz, matfp), wins);
    }

    /** Data_Mem::p_cut_face_x, Data_Mem::p_cut_face_y */    
    checkCall(read_int_array_from_MAT_file(data->p_cut_face_x, "p_cut_face_x",
					   nx, Ny, Nz, matfp), wins);
    checkCall(read_int_array_from_MAT_file(data->p_cut_face_y, "p_cut_face_y",
					   Nx, ny, Nz, matfp), wins);
    /** if Data_Mem::solve_3D_OK Data_Mem::p_cut_face_z */
    if(solve_3D_OK){
      checkCall(read_int_array_from_MAT_file(data->p_cut_face_z, "p_cut_face_z",
					    Nx, Ny, nz, matfp), wins);
    }


    /** Displaced coordinates: */
    /** Data_Mem::nodes_y_u, Data_Mem::nodes_x_v */    
    checkCall(read_array_from_MAT_file(data->nodes_y_u, "nodes_y_u",
				       nx, Ny, Nz, matfp), wins);    
    checkCall(read_array_from_MAT_file(data->nodes_x_v, "nodes_x_v",
				       Nx, ny, Nz, matfp), wins);
    /** if Data_Mem::solve_3D_OK Data_Mem::nodes_z_u, Data_Mem::nodes_z_v,
	Data_Mem::nodes_x_w, Data_Mem::nodes_y_w */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->nodes_z_u, "nodes_z_u",
					nx, Ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->nodes_z_v, "nodes_z_v",
					Nx, ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->nodes_x_w, "nodes_x_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->nodes_y_w, "nodes_y_w",
					Nx, Ny, nz, matfp), wins);
    }

    /** Data_Mem::p_faces_x, Data_Mem::p_faces_y */
    checkCall(read_array_from_MAT_file(data->p_faces_x, "p_faces_x",
				       nx, Ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->p_faces_y, "p_faces_y",
				       Nx, ny, Nz, matfp), wins);    
    /** Data_Mem::solve_3D_OK Data_Mem::p_faces_z */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->p_faces_z, "p_faces_z",
					 Nx, Ny, nz, matfp), wins);
    }

    /** Distance to neighbours for displaced coordinates: */
    /** Data_Mem::Delta_yN_u, Data_Mem::Delta_yS_u, Data_Mem::Delta_yn_u, Data_Mem::Delta_ys_u */    
    checkCall(read_array_from_MAT_file(data->Delta_yN_u, "Delta_yN_u",
				       nx, Ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->Delta_yS_u, "Delta_yS_u",
				       nx, Ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->Delta_yn_u, "Delta_yn_u",
				       nx, Ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->Delta_ys_u, "Delta_ys_u",
				       nx, Ny, Nz, matfp), wins);

    /** if Data_Mem::solve_3D_OK Data_Mem::Delta_zT_u, Data_Mem::Delta_zB_u, 
	Data_Mem::Delta_zt_u, Data_Mem::Delta_zb_u */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->Delta_zT_u, "Delta_zT_u",
					nx, Ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_zB_u, "Delta_zB_u",
					nx, Ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_zt_u, "Delta_zt_u",
					nx, Ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_zb_u, "Delta_zb_u",
					nx, Ny, Nz, matfp), wins);
    }

    /** Data_Mem::Delta_xE_v, Data_Mem::Delta_xW_v, Data_Mem::Delta_xe_v, Data_Mem::Delta_xw_v */    
    checkCall(read_array_from_MAT_file(data->Delta_xE_v, "Delta_xE_v",
				       Nx, ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->Delta_xW_v, "Delta_xW_v",
				       Nx, ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->Delta_xe_v, "Delta_xe_v",
				       Nx, ny, Nz, matfp), wins);
    checkCall(read_array_from_MAT_file(data->Delta_xw_v, "Delta_xw_v",
				       Nx, ny, Nz, matfp), wins);

    /** if Data_Mem::solve_3D_OK Data_Mem::Delta_zT_v, Data_Mem::Delta_zB_v,
	Data_Mem::Delta_zt_v, Data_Mem::Delta_zb_v */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->Delta_zT_v, "Delta_zT_v",
					Nx, ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_zB_v, "Delta_zB_v",
					Nx, ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_zt_v, "Delta_zt_v",
					Nx, ny, Nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_zb_v, "Delta_zb_v",
					Nx, ny, Nz, matfp), wins);
    }

    /** if Data_Mem::solve_3D_OK Data_Mem::Delta_xE_w, Data_Mem::Delta_xW_w,
	Data_Mem::Delta_xe_w, Data_Mem::Delta_xw_w, Data_Mem::Delta_yN_w, 
	Data_Mem::Delta_yS_w, Data_Mem::Delta_yn_w, Data_Mem::Delta_ys_w */
    if(solve_3D_OK){
      checkCall(read_array_from_MAT_file(data->Delta_xE_w, "Delta_xE_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_xW_w, "Delta_xW_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_xe_w, "Delta_xe_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_xw_w, "Delta_xw_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_yN_w, "Delta_yN_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_yS_w, "Delta_yS_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_yn_w, "Delta_yn_w",
					Nx, Ny, nz, matfp), wins);
      checkCall(read_array_from_MAT_file(data->Delta_ys_w, "Delta_ys_w",
					Nx, Ny, nz, matfp), wins); 
    }    
    
    compute_cut_cell_jobs_OK = 0;
  }

  Mat_Close(matfp);

  return compute_cut_cell_jobs_OK;
}

/* COMPUTE_GL_VALUE_3D */
/*****************************************************************************/
/**
  Sets:

  Array (double, NxNy) gL_to_calculate: returns gL values corresponding to 
  current Data_Mem::phi values.

  @return void
  
  MODIFIED: 07-07-2020
*/
void compute_gL_value_3D(Data_Mem *data, double *gL_to_calculate){
	
  int I, J, K, index_;
  int phase_index;
	
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
	
  const int *phase_change_matrix = data->phase_change_matrix;
  const int *dom_matrix = data->domain_matrix;

  const double *phi = data->phi;
  const double *T_phase_change = data->T_phase_change;
  const double *dT_phase_change = data->dT_phase_change;
  
#pragma omp parallel for default(none) private(I, J, K, index_, phase_index) \
  shared(phase_change_matrix, gL_to_calculate, phi,			\
	 T_phase_change, dT_phase_change, dom_matrix)

  for(K=1; K<(Nz-1); K++){
    for(J=1; J<(Ny-1); J++){
      for(I=1; I<(Nx-1); I++){

	index_ = K*Nx*Ny + J*Nx + I;
	phase_index = dom_matrix[index_];

	if(phase_change_matrix[index_]){
	  gL_to_calculate[index_] = gL_function(phi[index_],
						T_phase_change[phase_index-1],
						dT_phase_change[phase_index-1]);
	}
	else{
	  gL_to_calculate[index_] = 0;
	}
      }
    }
  }
  
  return;	
}

/* UPDATE_GL_GUESS_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNyNz) Data_Mem::gL_guess: Update Data_Mem::gL_guess using 
  Data_Mem::gL_calc values, applying relaxation.

  @param data

  @return void
  
  MODIFIED: 07-07-2020
*/
void update_gL_guess_3D(Data_Mem *data){

  int I, J, K, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *phase_change_matrix = data->phase_change_matrix;

  const double alpha_gL = data->alpha_gL;
  
  double *gL_guess = data->gL_guess;

  const double *gL_calc = data->gL_calc;

  
#pragma omp parallel for default(none) private(I, J, K, index_)	\
  shared(gL_guess, gL_calc, phase_change_matrix)

  for(K=0; K<Nz; K++){
    for(J=0; J<Ny; J++){
      for(I=0; I<Nx; I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(phase_change_matrix[index_]){
	  gL_guess[index_] = gL_calc[index_] * alpha_gL + gL_guess[index_] * (1 - alpha_gL);
	}
      }
    }
  }

  return;
}

/* COMPUTE_ENTHALPY_DIFF_3D */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, NxNy) enthalpy_diff: enthalpy difference for source term calculation.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 07-07-2020
*/
void compute_enthalpy_diff_3D(Data_Mem *data){

  int I, J, K, index_, phase_index;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *domain_matrix = data->domain_matrix;
  const int *phase_change_matrix = data->phase_change_matrix;

  double *enthalpy_diff = data->enthalpy_diff;

  const double *phi = data->phi;
  const double *T_ref = data->T_ref;
  const double *dH_S_L = data->dH_S_L;
  const double *cp_v = data->cp_v;
  const double *cp_l = data->cp_l;
  const double *rho_v = data->rho_v;
  const double *rho_l = data->rho_l;
  

#pragma omp parallel for default(none) private(I, J, K, index_, phase_index) \
  shared(enthalpy_diff, phi, T_ref, phase_change_matrix,		\
	 rho_l, cp_l, rho_v, cp_v, dH_S_L, domain_matrix)

  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){

	index_ = K*Nx*Ny + J*Nx + I;
	phase_index = domain_matrix[index_];

	if(phase_change_matrix[index_]){
	  /* EC 11 Voller */
	  enthalpy_diff[index_] = (phi[index_] - T_ref[phase_index-1]) * 
	    (rho_l[phase_index-1] * cp_l[phase_index-1] -
	     rho_v[phase_index] * cp_v[phase_index]) +
	    rho_l[phase_index-1] * dH_S_L[phase_index-1];
	}
      
      }
    }
  }

  return;
}

/* COMPUTE_AVG_GL_DIFF_3D */
/*****************************************************************************/
/**
  
  Computes weighted average value of difference between gL_guess and gL_calc.

  @param data

  @return (sum gL_difference * vol / sum vol).
  
  MODIFIED: 07-07-2020
*/
double compute_avg_gL_diff_3D(Data_Mem *data){
  
  int I, J, K, index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int *phase_change_matrix = data->phase_change_matrix;

  double vol = 0;
  double gL_difference = 0;
  
  const double *phase_change_vol = data->phase_change_vol;
  const double *gL_guess = data->gL_guess;
  const double *gL_calc = data->gL_calc;
  
#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(phase_change_matrix, phase_change_vol, gL_guess, gL_calc)	\
  reduction(+: vol, gL_difference)

  for(K=1; K<(Nz-1); K++){
    for(J=1; J<(Ny-1); J++){
      for(I=1; I<(Nx-1); I++){

	index_ = K*Nx*Ny + J*Nx + I;

	if(phase_change_matrix[index_]){
	  vol += phase_change_vol[index_];
	  gL_difference += fabs((gL_guess[index_] - gL_calc[index_]) *
				phase_change_vol[index_]);
	}
      }
    }
  }
  
  return (gL_difference / vol);	
}
