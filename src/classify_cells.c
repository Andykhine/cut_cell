#include "include/some_defs.h"
/* CLASSIFY_CELLS */
void classify_cells(Data_Mem *data){

  Wins *wins = &(data->wins);

  const int curves = data->curves;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int turb_model = data->turb_model;
  const int solve_flow_OK = data->solve_flow_OK;
  const int solve_3D_OK = data->solve_3D_OK;

  progress_msg(wins, "classify cells", 0.0);
  
  /* SET_CUT_MATRIX */
  /* Sets:
     - cut_matrix
     Using:
     - poly_coeffs
     - curve_is_solid_OK
  */
  if(solve_3D_OK){
    if(set_cut_matrix_3D(data) != 0){
      error_msg(wins, "Cut matrix");
    }
  }
  else{
    if(set_cut_matrix(data) != 0){
      error_msg(wins, "Cut matrix");
    }
  }

  progress_msg(wins, "classify cells", 10.0);
  
  /* SET_DOM_MATRIX */
  /* Sets:
     - domain_matrix
     - u_dom_matrix
     - v_dom_matrix
     - w_dom_matrix (3D case)
     Using:
     - poly_coeffs
     - curve_is_solid_OK
  */
  if(solve_3D_OK){
    if(set_dom_matrix_3D(data) != 0){
      error_msg(wins, "Domain matrix");
    }
  }
  else{
    if(set_dom_matrix(data) != 0){
      error_msg(wins, "Domain matrix");
    }
  }

  progress_msg(wins, "classify cells", 20.0);
  
  /* SET_PHASE_FLAGS */
  /* Sets:
     - P_solve_phase_OK
     - E_solve_phase_OK
     - W_solve_phase_OK
     - N_solve_phase_OK
     - S_solve_phase_OK
     - T_solve_phase_OK (3D case)
     - B_solve_phase_OK (3D case)
     Using:
     - solve_this_phase_OK
  */
  if(set_phase_flags(data) != 0){
    error_msg(wins, "Phase flags");
  }

  progress_msg(wins, "classify cells", 30.0);

  /* COMPUTE_CDIST_PHI */
  /* Sets:
     - cdEWNSTB
     Using;
     - domain_matrix
     - nodes_xyz
     - poly_coeffs
  */

  if(compute_cdist_phi(data) != 0){
    error_msg(wins, "CDISTs phi");
  }

  progress_msg(wins, "classify cells", 40.0);
  
  /* COMPUTE_VOLUME_FRACTIONS */
  /* Sets: 
     - v_fraction 
     - v_fraction_x
     - v_fraction_y
     - v_fraction_z (3D case)
     Using:
     - dom_matrix.
  */

  if(solve_3D_OK){
    if(compute_solid_vol_fractions_3D(data) != 0){
      error_msg(wins, "Solid volume fraction");
    }
  }
  else{  
    if(compute_solid_vol_fractions(data) != 0){
      error_msg(wins, "Solid volume fraction");
    }
  }

  progress_msg(wins, "classify cells", 50.0);

  /* SET_PHASE_CHANGE_MATRIX */
  /* Sets:
     - phase_change_matrix
     Using:
     - domain_matrix
     - solve_this_phase_OK
     - curve_phase_change_OK
  */
  if(solve_phase_change_OK){
    if(set_phase_change_matrix(data) != 0){
      error_msg(wins, "Phase change matrix");
    }
  }

  progress_msg(wins, "classify cells", 60.0);

  /* COMPUTE_VOLUMES */
  /* Sets:
     vol, vol_uncut, phase_change_vol
     Using:
     dom_matrix, v_fraction
  */
  if(compute_volumes(data) != 0){
    error_msg(wins, "Cell volumes");
  }
  
  progress_msg(wins, "classify cells", 70.0);
  
  /* LINES */
  if(compute_lines(data) != 0){
    error_msg(wins, "Cell lines");
  }

  progress_msg(wins, "classify cells", 80.0);
    
  /* Creacion de matriz de dominio y determinacion de 
     normal y tangente para una sola linea recta */
  /*if (lines == 1){

    pend = (data->line_points[3] - data->line_points[2]) / 
    (data->line_points[1] - data->line_points[0]);
    inters = data->line_points[2] - pend*data->line_points[0];
    for (i = 0; i < Nx; i++){
    for (j = 0; j < Ny; j++){
    index_ = Nx*j + i;
    if (f_test_line(nodes_x[index_], nodes_y[index_], pend, inters)<0){
    dom_matrix[index_] = 1;
    }
    }
    }

    svect[0] = 1/powf((1+ powf(pend, 2)),0.5);
    svect[1] = pend/powf((1+ powf(pend, 2)),0.5);
    nvect[0] = svect[1];
    nvect[1] = -svect[0];
    x_test[0] = 0.5*(data->line_points[1] + data->line_points[0])+0.1*nvect[0];
    x_test[1] = 0.5*(data->line_points[3] + data->line_points[2])+0.1*nvect[1];
  */

  /*La normal debe apuntar hacia adentro del dominio*/
  /*
    if (f_test_line(x_test[0], x_test[1], pend, inters)<0){
    nvect[0] = -nvect[0];
    nvect[1] = -nvect[1];
    }

    for (i = 1; i< Nx-1; i++){
    for (j = 1; j< Ny-1; j++){
    index_ = Nx*j+i;
    dom_bnd_cell = 0;
    if (dom_matrix[index_] == 0){
    if (dom_matrix[index_+1] == 1){
    x_inters[0] = (nodes_y[index_]-inters)/pend;
    eps_x[index_] = fabs(x_inters[0]-nodes_x[index_])/Delta_x[index_];
    dom_bnd_cell = 1;
    }
    if (dom_matrix[index_-1] == 1){
    x_inters[0] = (nodes_y[index_]-inters)/pend;
    eps_x[index_] = fabs(x_inters[0]-nodes_x[index_])/Delta_x[index_];
    dom_bnd_cell = 1;
    }
    if (dom_matrix[index_+Nx] == 1){
    x_inters[1] = nodes_x[index_]*pend+inters;
    eps_y[index_] = fabs(x_inters[1]-nodes_y[index_])/Delta_y[index_];
    dom_bnd_cell = 1;
    }
    if (dom_matrix[index_-Nx] == 1){
    x_inters[1] = nodes_x[index_]*pend+inters;
    eps_y[index_] = fabs(x_inters[1]-nodes_y[index_])/Delta_y[index_];
    dom_bnd_cell = 1;
    }
    if (dom_bnd_cell == 1){
    norm_vec_x[index_] = nvect[0];
    norm_vec_y[index_] = nvect[1];
    par_vec_x[index_] = svect[0];
    par_vec_y[index_] = svect[1];
    }
    }  
    }
    }
    }*/

  if(curves > 0){
    /* Sets:
       - sf_boundary_solid_indexs: Non-fluid cells that are solved (based on P_solve_phase_OK) that have a fluid neighbour 
       - I_bm: I index corresponding to sf_boundary_solid_indexs
       - J_bm: J index corresponding to sf_boundary_solid_indexs
       - W_is_fluid_OK: True if neighbour at west position is fluid 
       - E_is_fluid_OK: True if neighbour at east position is fluid
       - S_is_fluid_OK: True if neighbour at south position is fluid
       - N_is_fluid_OK: True if neighbour at north position is fluid
    */

    if(solve_3D_OK){
      sf_boundary_solid_indexs_finder_3D(data);
    }    
    else{
      sf_boundary_solid_indexs_finder(data);
    }

    /* SET_EPS_EPS_S_NORM_PAR */
    /* Sets:
       - eps_x
       - eps_x_sol
       - eps_y
       - eps_y_sol
       - eps_z (3D case)
       - eps_z_sol (3D case)
       - norm_vec_x_x
       - norm_vec_x_y
       - norm_vec_x_z (3D case)
       - par_vec_x_x
       - par_vec_x_y
       - norm_vec_y_x
       - norm_vec_y_y
       - norm_vec_y_z (3D case)
       - par_vec_y_x
       - par_vec_y_y
       - norm_vec_z_x (3D case)
       - norm_vec_z_y (3D case)
       - norm_vec_z_z (3D case)
       - norm_x
       - norm_y
       - norm_z (3D case)
       - par_x
       - par_y
       - par_z (3D case)
       Using:
       - domain_matrix
       - poly_coeffs
       - EWNSTB_is_fluid_OK
    */


    if(solve_3D_OK){
      if(set_eps_eps_s_norm_par_legacy_3D(data) != 0){
	error_msg(wins, "Epsilon and normal vectors (legacy)");
      }
    }
    else{
    
      /* SETTING EPS NORM AND PAR IN THE NEW SCHEME OF SOLVABLE PHASES */
      /* This does not work properly when alternating 
	 solve_this_phase_OK flags are used */
    
      if(set_eps_eps_s_norm_par(data) != 0){
	error_msg(wins, "Epsilon and normal vectors");
      }
    
      /* Legacy
	 if(set_eps_eps_s_norm_par_legacy(data) == 0){
	 info_msg(wins, "Epsilon and normal vectors computed (legacy)");
	 }
	 else{
	 error_msg(wins, "Epsilon and normal vectors (legacy)");
	 }
      */

      progress_msg(wins, "classify cells", 90.0);
    
    }
    
    /* Sets:
       - Dirichlet_boundary_indexs: Cells that are solved (based on P_solve_phase_OK) and  have a non-solved neighbour, 
       this neighbour pertaining to a domain defined by a curve with a Dirichlet boundary condition
       - I_Dirichlet_v: I index corresponding to Dirichlet_boundary_indexs
       - J_Dirichlet_v: J index corresponding to Dirichlet_boundary_indexs
    */
    if(solve_3D_OK){
      Dirichlet_boundary_indexs_finder_3D(data);
    }
    else{
      Dirichlet_boundary_indexs_finder(data);
    }
    /* Sets:
       - Isoflux_boundary_indexs: Cells that are solved(based on P_solve_phase_OK) and have a non-solved neighbour, 
       this neighbour pertaining to a domain defined by a curve with an Isoflux boundary condition
       - I_Isoflux_v: I index corresponding to Isoflux_boundary_indexs
       - J_Isoflux_v: J index corresponding to Isoflux_boundary_indexs
    */
    if(solve_3D_OK){
      Isoflux_boundary_indexs_finder_3D(data);
    }
    else{
      Isoflux_boundary_indexs_finder(data);
    }
    /* Sets:
       - Conjugate_boundary_indexs: Cells that are solved (based on P_solve_phase_OK) that have a neighbour that is also solved, 
       if domain index of neighbour cell is different to domain index of P cell
       - I_Conjugate_v: I index corresponding to Conjugate_boundary_indexs
       - J_Conjugate_v: J index corresponding to Conjugate_boundary_indexs
    */
    if(solve_3D_OK){
      Conjugate_boundary_indexs_finder_3D(data);
    }
    else{
      Conjugate_boundary_indexs_finder(data);
    }

    /* Sets:
       - nxnx_x: Product nx*nx at face perpendicular to x axis.
       - nxny_x: Product nx*ny at face perpendicular to x axis.
       - nyny_y: Product ny*ny at face perpendicular to y axis.
       - nxny_y: Product nx*ny at face perpendicular to y axis.
    */
    compute_normal_parallel_products(data);    
    
  }

  
  /* COMPUTE_NEIGHBOUR_COEFFS_FACTORS 
     Sets:
     - E_coeff
     - W_coeff
     - N_coeff
     - S_coeff
     Using:
     - domain_matrix
  */
  compute_neighbour_coeffs_factors(data);

  progress_msg(wins, "classify cells", 100.0);

  if((turb_model != 0) && ((solve_flow_OK == 1) || (solve_flow_OK == 2) || (solve_flow_OK == 4))){
    /* Compute distance to nearest wall 
       Sets:
       - y_wall
    */

    /* Needed here because the value of y_wall is used on k_turb initialization */
    if(solve_3D_OK){
      if(compute_y_wall_3D(data) != 0){
	error_msg(wins, "y_wall");
      }
    }
    else if(compute_y_wall(data) != 0){
      error_msg(wins, "y_wall");
    }
  }

  return;
}

/* COMPUTE_Y_WALL */
/*****************************************************************************/
/**
  
   Sets:

   Array (double, NxNy) Data_Mem::y_wall

   @return 0 (ret_value not fully implemented).
  
   MODIFIED: 05-02-2019
*/
int compute_y_wall(Data_Mem *data){

  mat_t *matfp = NULL;

  char msg[SIZE];
  
  const char *geom_filename = data->geom_filename;

  int I, J, Z, index_, curv_ind;
  int compute_y_wall_OK = 1;
  int ret_value = 0;
  int per_counter = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int curves = data->curves;
  const int read_y_wall_OK = data->read_y_wall_OK;
  const int bc_west_type = data->bc_flow_west.type;
  const int bc_east_type = data->bc_flow_east.type;
  const int bc_south_type = data->bc_flow_south.type;
  const int bc_north_type = data->bc_flow_north.type;

  const int *domain_matrix = data->domain_matrix;

  double min_dist, curve_dist;
  double pc_temp[16];
  double x[3];
  
  const double Lx = data->Lx;
  const double Ly = data->Ly;
  const double cell_size = data->cell_size;

  const double total = 2 + (Nx-2)*(Ny-2)*curves;

  double *y_wall = data->y_wall;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *poly_coeffs = data->poly_coeffs;
  
  Wins *wins = &(data->wins);
  
  if(read_y_wall_OK){

    matfp = Mat_Open(geom_filename, MAT_ACC_RDONLY);
    
    if(matfp == NULL){
      alert_msg(wins, "Could not open GEOM file!!... computing y_wall instead");
      compute_y_wall_OK = 1;
    }
    else{
      
      checkCall(read_array_from_MAT_file(y_wall, "y_wall", Nx, Ny, Nz, matfp), wins);
      
      sprintf(msg, "y_wall read from %s", geom_filename);
      info_msg(wins, msg);

      Mat_Close(matfp);
      
      compute_y_wall_OK = 0;
    }
  }
  
  if(compute_y_wall_OK){

    progress_msg(wins, "y_wall", 0.0);
    
    x[2] = 0;

    /* Distance to walls */
#pragma omp parallel for default(none)					\
  firstprivate(x) private(I, J, index_, min_dist)			\
  shared(per_counter,  wins,						\
	 domain_matrix, nodes_x, nodes_y, y_wall)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	index_ = J*Nx + I;
	min_dist = 1e6;
	if(domain_matrix[index_] == 0){
	  x[0] = nodes_x[index_];
	  x[1] = nodes_y[index_];
	  if(bc_west_type == 3){
	    min_dist = MIN(min_dist, x[0]);
	  }
	  if(bc_east_type == 3){
	    min_dist = MIN(min_dist, Lx - x[0]);
	  }
	  if(bc_south_type == 3){
	    min_dist = MIN(min_dist, x[1]);
	  }
	  if(bc_north_type == 3){
	    min_dist = MIN(min_dist, Ly - x[1]);
	  }
	  y_wall[index_] = min_dist;
	}
	else{
	  y_wall[index_] = NAN;
	}
      }
    }


    progress_msg(wins, "y_wall", 1.0/total * 100.0);
    
    /* Distance to curves */
    /* With new scheme is only needed to pass the first element (coefficient
       of the curve, but still will do it classically */

    per_counter = 0;
    
    for(curv_ind=0; curv_ind<curves; curv_ind++){
      
      for(Z=0; Z<16; Z++){
	pc_temp[Z] = poly_coeffs[curv_ind*16 + Z];
      }

#pragma omp parallel for default(none)					\
  firstprivate(x) private(I, J, index_, curve_dist)			\
  shared(per_counter, wins,						\
	 domain_matrix, nodes_x, nodes_y, y_wall, pc_temp, curv_ind)
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;
	  if(domain_matrix[index_] == 0){
	    x[0] = nodes_x[index_];
	    x[1] = nodes_y[index_];

	    curve_dist = calc_dist_far(x, pc_temp, cell_size);
	    y_wall[index_] = MIN( y_wall[index_], curve_dist );
	  }
	}

#pragma omp critical
	{
	  per_counter++;
	  
	  progress_msg(wins, "y_wall", (Nx-2) * per_counter / total * 100.0);
	}
      }
    }

    /* Boundary values */
#pragma omp parallel sections default(none) private(I, J, index_)	\
  shared(domain_matrix, y_wall)
    {
#pragma omp section 
      {
	/* West */

	I = 0;

	/* Wall */
	if(bc_west_type == 3){
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    y_wall[index_] = 0;
	  }
	}
	/* Periodic boundary */
	else if(bc_west_type == 5){
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[J*Nx+Nx-2];
	    }
	  }
	}
	/* Zero gradient, Fixed value, Symmetry, Constant Pressure */
	else{
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[index_+1];
	    }
	  }
	}
      } /* West section */

#pragma omp section 
      {
	/* East */
	I = Nx - 1;

	if(bc_east_type == 3){
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    y_wall[index_] = 0;
	  }
	}
	else if(bc_east_type == 5){
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[J*Nx+1];
	    }
	  }
	}
	else{
	  for(J=1; J<Ny-1; J++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[index_-1];
	    }
	  }
	}
      } /* East section */

#pragma omp section 
      {
	/* South */

	J = 0;
    
	if(bc_south_type == 3){
	  for(I=1; I<Nx-1; I++){
	    index_ = J*Nx + I;
	    y_wall[index_] = 0;
	  }
	}
	else if(bc_south_type == 5){
	  for(I=1; I<Nx-1; I++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[(Ny-2)*Nx+I];
	    }
	  }
	}
	else{
	  for(I=1; I<Nx-1; I++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[index_+Nx];
	    }
	  }
	}
      } /* South section */

#pragma omp section 
      {
	/* North */

	J = Ny - 1;

	if(bc_north_type == 3){
	  for(I=1; I<Nx-1; I++){
	    index_ = J*Nx + I;
	    y_wall[index_] = 0;
	  }
	}
	else if(bc_north_type == 5){
	  for(I=1; I<Nx-1; I++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[Nx+I];
	    }
	  }
	}
	else{
	  for(I=1; I<Nx-1; I++){
	    index_ = J*Nx + I;
	    if(domain_matrix[index_] == 0){
	      y_wall[index_] = y_wall[index_-Nx];
	    }
	  }
	}
      } /* North section */
    } /* sections */
    
    progress_msg(wins, "y_wall", 100.0);

  }

  return ret_value;
}

/* COMPUTE_VOLUMES */
/*****************************************************************************/
/**
  
   Sets:

   Array (int, NxNy) vol_uncut: cells volume.

   Array (int, NxNy) vol: fluid volume of fluid cells .

   Array (int, NxNy) phase_change_vol: solid volume of solved solid cells that
   experience phase change.

   @param data

   @return 0
  
   MODIFIED: 15-07-2019
*/
int compute_volumes(Data_Mem *data){

  int ret_value = 0;

  int I, J, K;
  int index_;

  const int Nx = data->Nx; 
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int solve_3D_OK = data->solve_3D_OK;

  const int *phase_change_matrix = data->phase_change_matrix;
  const int *dom_matrix = data->domain_matrix;

  double *vol = data->vol;
  double *vol_uncut = data->vol_uncut;
  double *phase_change_vol = data->phase_change_vol;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;

  /* These are solid volumetric fractions */
  const double *vf = data->v_fraction;
 
  if(solve_3D_OK){

#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(dom_matrix, vol_uncut, vol, Delta_x, Delta_y, Delta_z, vf)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){

	  index_ = K*Nx*Ny + J*Nx + I;
	  vol_uncut[index_] = Delta_x[index_] * Delta_y[index_] * Delta_z[index_];

	  if(dom_matrix[index_] == 0){
	    vol[index_] = vol_uncut[index_] * (1 - vf[index_]);
	  }
	}
      }
    }

  }
  else{

#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(dom_matrix, vol_uncut, vol, Delta_x, Delta_y, vf)

    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
      
	index_ = J*Nx + I;
	vol_uncut[index_] = Delta_x[index_] * Delta_y[index_];
	  
	if(dom_matrix[index_] == 0){
	  vol[index_] = Delta_x[index_] * Delta_y[index_] * (1 - vf[index_]);
	}
      
      }
    }
    
  }

  if(solve_phase_change_OK){

    if(solve_3D_OK){

#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(phase_change_matrix, phase_change_vol, Delta_x, Delta_y, Delta_z, vf)
    
      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    index_ = K*Nx*Ny + J*Nx + I;

	    if(phase_change_matrix[index_]){
	      phase_change_vol[index_] = Delta_x[index_] * Delta_y[index_]
		* Delta_z[index_] * vf[index_];
	    }
	  }
	}
      }      

    }
    else{

#pragma omp parallel for private(I, J, index_)				\
  shared(phase_change_matrix, phase_change_vol, Delta_x, Delta_y, vf)

      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	  index_ = J*Nx + I;

	  if(phase_change_matrix[index_]){
	    phase_change_vol[index_] = Delta_x[index_] * Delta_y[index_] * vf[index_];
	  }
	}
      }
    }
    
  }

  return ret_value;
  
}

/* COMPUTE_CDIST_PHI */
/*****************************************************************************/
/**

   Sets:

   Array (double, NxNyNz) cdEWNSTB: distance between fluid cell center and 
   interface in xyz direction.

*/

int compute_cdist_phi(Data_Mem *data){

  int ret_value = 0;

  const int Nx = data->Nx; 
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int curves = data->curves;
  const int solve_3D_OK = data->solve_3D_OK;

  const int *dom = data->domain_matrix;
  
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  const double cell_size = data->cell_size;

  double *cdE = data->cdE;
  double *cdW = data->cdW;
  double *cdN = data->cdN;
  double *cdS = data->cdS;
  double *cdT = data->cdT;
  double *cdB = data->cdB;

  const double *x = data->nodes_x;
  const double *y = data->nodes_y;
  const double *z = data->nodes_z;
  
  const double *poly_coeffs = data->poly_coeffs;

  /* Initialize distances */
  /* These are for turbulence and phi */
  copy_array(data->Delta_xE, cdE, Nx, Ny, Nz);
  copy_array(data->Delta_xW, cdW, Nx, Ny, Nz);
  copy_array(data->Delta_yN, cdN, Nx, Ny, Nz);
  copy_array(data->Delta_yS, cdS, Nx, Ny, Nz);
  
  if(solve_3D_OK){
    /* These are for turbulence and phi */  
    copy_array(data->Delta_zT, cdT, Nx, Ny, Nz);
    copy_array(data->Delta_zB, cdB, Nx, Ny, Nz);
  }

  /* Correct distances */
  if(solve_3D_OK){

    /* phi */
    __compute_cdist_3D(cdE, cdW, cdN, cdS, cdT, cdB,
		       Nx, Ny, Nz, curves,
		       dom, curve_is_solid_OK,
		       x, y, z,
		       poly_coeffs, cell_size);
  }
  else{

    /* phi */
    __compute_cdist(cdE, cdW, cdN, cdS,
		    Nx, Ny, curves,
		    dom, curve_is_solid_OK,
		    x, y,
		    poly_coeffs, cell_size);
  }

  return ret_value;
}

/* SET_EPS_EPS_S_NORM_PAR_LEGACY */
/*****************************************************************************/
/**

   Sets:

   Array (int, NxNy) eps_x: normalized distance between fluid cell center and 
   interface in x direction.

   Array (int, NxNy) eps_x_sol: normalized distance between solid cell center and
   interface in x direction.

   Arrays eps_y and eps_y_sol measure the corresponding distance in y direction.

   Array (int, NxNy) norm_vec_x_x: x component of normal vector at interface,
   when interface is found in x direction.

   Array (int, NxNy) norm_vec_x_y: y component of normal vector at interface
   when interface is found in x direction.

   Array (int, NxNy) par_vec_x_x: x component of parallel vector at interface
   when interface is found in x direction.

   Array (int, NxNy) par_vec_x_y: y component of parallel vector at interface
   when interface is found in x direction.

   Arrays norm_vec_y_* and par_vec_y_* contain corresponding components when
   interface is found in y direction.

   Array (int, NxNy) norm_x: x component of normal vector evaluated at fluid 
   cell center.

   Array (int, NxNy) norm_y: y component of normal vector evaluated at fluid
   cell center.

   Array (int, NxNy) par_x: x component of parallel vector evaluated at fluid
   cell center.

   Array (int, NxNy) par_y: y component of parallel vector evaluated at fluid
   cell center.

   @param data
  
   @return 0 (ret_value not fully implemented).
  
   MODIFIED: 05-02-2019
*/
int set_eps_eps_s_norm_par_legacy(Data_Mem *data){

  int ret_value = 0;

  int I, J, index_, curv_ind;
  int int_cell_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int curves = data->curves;

  const int *dom_matrix = data->domain_matrix;

  double dist;

  double pc_temp[16];
  /* Coordenadas del centro de la celda */
  double x_cell[3] = {0};
  double x_inters[3] = {0};
  double nvect[3] = {0};

  const double cell_size = data->cell_size;

  double *eps_x = data->epsilon_x;
  double *eps_y = data->epsilon_y;
  double *eps_x_sol = data->epsilon_x_sol;
  double *eps_y_sol = data->epsilon_y_sol;
  
  double *norm_vec_x_x = data->norm_vec_x_x;
  double *norm_vec_x_y = data->norm_vec_x_y;
  double *norm_vec_y_x = data->norm_vec_y_x;
  double *norm_vec_y_y = data->norm_vec_y_y;
  double *par_vec_x_x = data->par_vec_x_x;
  double *par_vec_x_y = data->par_vec_x_y;
  double *par_vec_y_x = data->par_vec_y_x;
  double *par_vec_y_y = data->par_vec_y_y;

  double *norm_x = data->norm_x;
  double *norm_y = data->norm_y;
  double *par_x = data->par_x;
  double *par_y = data->par_y;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;

  const double *poly_coeffs = data->poly_coeffs;
  
  if(curves > 0){

    for (curv_ind = 0; curv_ind < curves; curv_ind++){
      
      for (I=0; I<16; I++){
	pc_temp[I] = poly_coeffs[curv_ind*16 + I];
      }

#pragma omp parallel for default(none) firstprivate(curv_ind, x_cell)	\
  private(I, J, index_, int_cell_OK, dist, x_inters, nvect)		\
  shared(nodes_x, nodes_y, Delta_xE, Delta_xW, Delta_yN, Delta_yS,	\
	 pc_temp, dom_matrix,						\
	 eps_x, eps_y, eps_x_sol, eps_y_sol,				\
	 norm_x, norm_y, par_x, par_y,					\
  	 norm_vec_x_x, norm_vec_x_y, norm_vec_y_x, norm_vec_y_y,	\
  	 par_vec_x_x, par_vec_x_y, par_vec_y_x, par_vec_y_y)

      for (J=1; J<Ny-1; J++){
	for (I=1; I<Nx-1; I++){
	  
	  index_ = J*Nx+I;
	  int_cell_OK = 0;

	  x_cell[0] = nodes_x[index_];
	  x_cell[1] = nodes_y[index_];
		
	  if (dom_matrix[index_] == 0){
	    
	    if (dom_matrix[index_ + 1] == curv_ind + 1){
	      int_cell_OK = 1;
	      /* Get normal vector from Kirkpatrick 
		 quadric general equation on the cell center */
	      normal(nvect, x_cell, pc_temp);
	      /* Consider only horizontal displacement*/
	      nvect[1] = 0;
	      /* Get the intersection between a line that passes through 
		 x_cell and the curve from pc_temp */
	      int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	      dist = vec_dist(x_cell, x_inters);
	      /* Get normal vector now on the curve */
	      normal(nvect, x_inters, pc_temp);
	      /* Get epsilon values */
	      eps_x[index_] = dist / Delta_xE[index_];
	      eps_x_sol[index_ + 1] = 1 - eps_x[index_];
	      norm_vec_x_x[index_] = nvect[0];
	      norm_vec_x_y[index_] = nvect[1];
	      par_vec_x_x[index_] = -nvect[1];
	      par_vec_x_y[index_] = nvect[0];
	    }
	    if (dom_matrix[index_ - 1] == curv_ind + 1){
	      int_cell_OK = 1;
	      normal(nvect, x_cell, pc_temp);
	      nvect[1] = 0;                     
	      int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	      dist = vec_dist(x_cell, x_inters);
	      normal(nvect, x_inters, pc_temp);
	      eps_x[index_] = dist / Delta_xW[index_];
	      eps_x_sol[index_ - 1] = 1 - eps_x[index_];
	      norm_vec_x_x[index_] = nvect[0];
	      norm_vec_x_y[index_] = nvect[1];
	      par_vec_x_x[index_] = -nvect[1];
	      par_vec_x_y[index_] = nvect[0];
	    }
	    if (dom_matrix[index_ + Nx] == curv_ind + 1){
	      int_cell_OK = 1;
	      normal(nvect, x_cell, pc_temp);
	      nvect[0] = 0;                     
	      int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	      dist = vec_dist(x_cell, x_inters);
	      normal(nvect, x_inters, pc_temp);
	      eps_y[index_] = dist / Delta_yN[index_];
	      eps_y_sol[index_ + Nx] = 1- eps_y[index_];
	      norm_vec_y_x[index_] = nvect[0];
	      norm_vec_y_y[index_] = nvect[1];
	      par_vec_y_x[index_] = -nvect[1];
	      par_vec_y_y[index_] = nvect[0];
	    }
	    if (dom_matrix[index_ - Nx] == curv_ind + 1){
	      int_cell_OK = 1;
	      normal(nvect, x_cell, pc_temp);
	      nvect[0] = 0;                     
	      int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	      dist = vec_dist(x_cell, x_inters);
	      normal(nvect, x_inters, pc_temp);
	      eps_y[index_] = dist / Delta_yS[index_];
	      eps_y_sol[index_ - Nx] = 1 - eps_y[index_];
	      norm_vec_y_x[index_] = nvect[0];
	      norm_vec_y_y[index_] = nvect[1];
	      par_vec_y_x[index_] = -nvect[1];
	      par_vec_y_y[index_] = nvect[0];
	    }

	    if(int_cell_OK){
	      /* Get normal vector for cell at cell center */
	      normal(nvect, x_cell, pc_temp);
	      norm_x[index_] = nvect[0];
	      norm_y[index_] = nvect[1];
	      par_x[index_] = -nvect[1];
	      par_y[index_] = nvect[0];
	    }
	  } // if (dom_matrix[index_] == 0)
	}// for (i = 1; i < Nx-1; i++){
      }// for (j = 1; j < Ny-1; j++){
    }//   for (curv_ind = 0; curv_ind < curves; curv_ind++)

  }

  return ret_value;

}

/* SET_EPS_EPS_S_NORM_PAR */
int set_eps_eps_s_norm_par(Data_Mem *data){

  int ret_value = 0;

  int I, J, index_, curv_ind, ipoly;
  int int_cell_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *dom_matrix = data->domain_matrix;

  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;

  double dist;

  double pc_temp[16];
  /* Coordenadas del centro de la celda */
  double x_cell[3] = {0};
  double x_inters[3] = {0};
  double nvect[3] = {0};

  const double cell_size = MIN(data->Lx/(Nx-2), data->Ly/(Ny-2));

  double *eps_x = data->epsilon_x;
  double *eps_y = data->epsilon_y;
  double *eps_x_sol = data->epsilon_x_sol;
  double *eps_y_sol = data->epsilon_y_sol;
  
  double *norm_vec_x_x = data->norm_vec_x_x;
  double *norm_vec_x_y = data->norm_vec_x_y;
  double *norm_vec_y_x = data->norm_vec_y_x;
  double *norm_vec_y_y = data->norm_vec_y_y;
  double *par_vec_x_x = data->par_vec_x_x;
  double *par_vec_x_y = data->par_vec_x_y;
  double *par_vec_y_x = data->par_vec_y_x;
  double *par_vec_y_y = data->par_vec_y_y;

  double *norm_x = data->norm_x;
  double *norm_y = data->norm_y;
  double *par_x = data->par_x;
  double *par_y = data->par_y;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;

  /* Distances to neighborg or interface */
  const double *cdE = data->cdE;
  const double *cdW = data->cdW;
  const double *cdN = data->cdN;
  const double *cdS = data->cdS;

  const double *poly_coeffs = data->poly_coeffs;

  // eps_sol remember!!....... testing!!

  data->cell_size = cell_size;
   
#pragma omp parallel for default(none)					\
  private(I,J,index_,curv_ind,ipoly,int_cell_OK,dist,			\
	  x_cell,x_inters,nvect,pc_temp)				\
  shared(nodes_x,nodes_y,poly_coeffs,dom_matrix,			\
	 E_is_fluid_OK, W_is_fluid_OK,					\
	 N_is_fluid_OK, S_is_fluid_OK,					\
	 Delta_xE, Delta_xW, Delta_yN, Delta_yS,			\
	 cdE, cdW, cdN, cdS,						\
	 eps_x,eps_y,eps_x_sol,eps_y_sol,				\
	 norm_x,norm_y,par_x,par_y,					\
  	 norm_vec_x_x,norm_vec_x_y,norm_vec_y_x,norm_vec_y_y,		\
  	 par_vec_x_x,par_vec_x_y,par_vec_y_x,par_vec_y_y)
  
  for (J=1; J<Ny-1; J++){
    for (I=1; I<Nx-1; I++){
      
      index_ = J*Nx+I;
      int_cell_OK = 0;
      
      if(dom_matrix[index_] == 0){

	x_cell[0] = nodes_x[index_];
	x_cell[1] = nodes_y[index_];
		  
	if(!E_is_fluid_OK[index_]){
	  int_cell_OK = 1;	  
	  /* Get polynomial expresion */
	  curv_ind = dom_matrix[index_+1] - 1;
	  for (ipoly = 0; ipoly < 16; ipoly++){
	    pc_temp[ipoly] = poly_coeffs[curv_ind*16 + ipoly];
	  }

	  
	  /* Get normal vector from Kirkpatrick 
	     quadric general equation on the cell center */
	  //	  normal(nvect, x_cell, pc_temp);
	  /* Consider only horizontal displacement*/
	  // nvect[1] = 0;
	  /* Get the intersection between a line that passes through 
	     x_cell and the curve from pc_temp */
	  // int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	  // dist = vec_dist(x_cell, x_inters);

	  dist = cdE[index_];
	  x_inters[0] = x_cell[0] + dist;
	  x_inters[1] = x_cell[1];
	  x_inters[2] = 0;
	  
	  /* Get normal vector now on the curve */
	  normal(nvect, x_inters, pc_temp);
	  /* Get epsilon values */
	  eps_x[index_] = dist/Delta_xE[index_];
	  
	  eps_x_sol[index_+1] = 1 - eps_x[index_];
	  
	  norm_vec_x_x[index_] = nvect[0];
	  norm_vec_x_y[index_] = nvect[1];
	  par_vec_x_x[index_] = -nvect[1];
	  par_vec_x_y[index_] = nvect[0];
	}
	
	if(!W_is_fluid_OK[index_]){
	  int_cell_OK = 1;
	  /* Get polynomial expresion */
	  curv_ind = dom_matrix[index_-1] - 1;
	  for (ipoly = 0; ipoly < 16; ipoly++){
	    pc_temp[ipoly] = poly_coeffs[curv_ind*16 + ipoly];
	  }

	  /*
	    normal(nvect, x_cell, pc_temp);
	    nvect[1] = 0;                     
	    int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	    dist = vec_dist(x_cell, x_inters);
	  */

	  dist = cdW[index_];
	  x_inters[0] = x_cell[0] - dist;
	  x_inters[1] = x_cell[1];
	  x_inters[2] = 0;
	  
	  normal(nvect, x_inters, pc_temp);
	  eps_x[index_] = dist/Delta_xW[index_];

	  eps_x_sol[index_-1] = 1 - eps_x[index_];
	  
	  norm_vec_x_x[index_] = nvect[0];
	  norm_vec_x_y[index_] = nvect[1];
	  par_vec_x_x[index_] = -nvect[1];
	  par_vec_x_y[index_] = nvect[0]; 
	}
	
	if(!N_is_fluid_OK[index_]){
	  int_cell_OK = 1;
	  /* Get polynomial expresion */
	  curv_ind = dom_matrix[index_+Nx] - 1;
	  for (ipoly = 0; ipoly < 16; ipoly++){
	    pc_temp[ipoly] = poly_coeffs[curv_ind*16 + ipoly];
	  }

	  /*
	    normal(nvect, x_cell, pc_temp);
	    nvect[0] = 0;                     
	    int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	    dist = vec_dist(x_cell, x_inters);
	  */

	  dist = cdN[index_];
	  x_inters[0] = x_cell[0];
	  x_inters[1] = x_cell[1] + dist;
	  x_inters[2] = 0;
	  
	  normal(nvect, x_inters, pc_temp);
	  eps_y[index_] = dist/Delta_yN[index_];

	  eps_y_sol[index_+Nx] = 1- eps_y[index_];
	  
	  norm_vec_y_x[index_] = nvect[0];
	  norm_vec_y_y[index_] = nvect[1];
	  par_vec_y_x[index_] = -nvect[1];
	  par_vec_y_y[index_] = nvect[0];
	}
		
	if(!S_is_fluid_OK[index_]){
	  int_cell_OK = 1;
	  /* Get polynomial expresion */
	  curv_ind = dom_matrix[index_-Nx] - 1;
	  for (ipoly = 0; ipoly < 16; ipoly++){
	    pc_temp[ipoly] = poly_coeffs[curv_ind*16 + ipoly];
	  }

	  /*
	    normal(nvect, x_cell, pc_temp);
	    nvect[0] = 0;                     
	    int_curv(x_inters, x_cell, nvect, pc_temp, cell_size);
	    dist = vec_dist(x_cell, x_inters);
	  */

	  dist = cdS[index_];
	  x_inters[0] = x_cell[0];
	  x_inters[1] = x_cell[1] - dist;
	  x_inters[2] = 0;
	  
	  normal(nvect, x_inters, pc_temp);
	  eps_y[index_] = dist/Delta_yS[index_];

	  eps_y_sol[index_ - Nx] = 1 - eps_y[index_];
	  
	  norm_vec_y_x[index_] = nvect[0];
	  norm_vec_y_y[index_] = nvect[1];
	  par_vec_y_x[index_] = -nvect[1];
	  par_vec_y_y[index_] = nvect[0];
	}
	
	if(int_cell_OK){
	  /* Get normal vector for cell at cell center */
	  normal(nvect, x_cell, pc_temp);
	  norm_x[index_] = nvect[0];
	  norm_y[index_] = nvect[1]; 
	  par_x[index_] = -nvect[1];
	  par_y[index_] = nvect[0];
	}
      } // if(P_solve_phase_OK[index_]){
    }// for (i = 1; i < Nx-1; i++)       
  }// for (j = 1; j < Ny-1; j++)
  
  return ret_value;

}

/* SET_PHASE_FLAGS */
int set_phase_flags(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: set_phase_flags
    Sets array 
    (int, NxNyNz) P_solve_phase_OK, 
    (int, NxNyNz) E_solve_phase_OK, 
    (int, NxNyNz) W_solve_phase_OK, 
    (int, NxNyNz) N_solve_phase_OK, 
    (int, NxNyNz) S_solve_phase_OK,
    (int, NxNyNz) T_solve_phase_OK,
    (int, NxNyNz) B_solve_phase_OK,
    to 1 if the corresponding cell is to be solved according to solve_this_phase_OK
    vector.
  
    RETURNS: 0 (ret_value not fully implemented).
  
    MODIFIED: 11-07-2019
  */
  /*****************************************************************************/
  int ret_value = 0;
  int I, J, K, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int solve_3D_OK = data->solve_3D_OK;

  int *P_solve_phase_OK = data->P_solve_phase_OK;
  int *E_solve_phase_OK = data->E_solve_phase_OK;
  int *W_solve_phase_OK = data->W_solve_phase_OK;
  int *N_solve_phase_OK = data->N_solve_phase_OK;
  int *S_solve_phase_OK = data->S_solve_phase_OK;
  int *T_solve_phase_OK = data->T_solve_phase_OK;
  int *B_solve_phase_OK = data->B_solve_phase_OK;

  const int *dom_matrix = data->domain_matrix;
  
  const int *solve_this_phase_OK = data->solve_this_phase_OK;

  if(solve_3D_OK){
  
#pragma omp parallel for default(none) private(I, J, K, index_)		\
  shared(solve_this_phase_OK, dom_matrix, P_solve_phase_OK, E_solve_phase_OK, \
	 W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK, T_solve_phase_OK, B_solve_phase_OK)

    for (K=0; K<Nz; K++){
      for (J=0; J<Ny; J++){
	for (I=0; I<Nx; I++){
      
	  index_ = K*Nx*Ny + J*Nx + I;

	  /* Setting phase flags */

	  if(solve_this_phase_OK[ dom_matrix[index_] ]){
	    P_solve_phase_OK[index_] = 1;
	  }
	  else{
	    P_solve_phase_OK[index_] = 0;
	  }

	  if(I != Nx-1){
	    if(solve_this_phase_OK[ dom_matrix[index_+1] ]){
	      E_solve_phase_OK[index_] = 1;
	    }
	    else{
	      E_solve_phase_OK[index_] = 0;
	    }
	  }

	  if(I != 0){
	    if(solve_this_phase_OK[ dom_matrix[index_-1] ]){
	      W_solve_phase_OK[index_] = 1;
	    }
	    else{
	      W_solve_phase_OK[index_] = 0;
	    }
	  }
	
	  if(J != Ny-1){
	    if(solve_this_phase_OK[ dom_matrix[index_+Nx] ]){
	      N_solve_phase_OK[index_] = 1;
	    }
	    else{
	      N_solve_phase_OK[index_] = 0;
	    }
	  }

	  if(J != 0){
	    if(solve_this_phase_OK[ dom_matrix[index_-Nx] ]){
	      S_solve_phase_OK[index_] = 1;
	    }
	    else{
	      S_solve_phase_OK[index_] = 0;
	    }
	  }

	  if(K != Nz-1){
	    if(solve_this_phase_OK[ dom_matrix[index_+Nx*Ny] ]){
	      T_solve_phase_OK[index_] = 1;
	    }
	    else{
	      T_solve_phase_OK[index_] = 0;
	    }
	  }
	  if(K != 0){ 
	    if(solve_this_phase_OK[ dom_matrix[index_-Nx*Ny] ]){
	      B_solve_phase_OK[index_] = 1;
	    }
	    else{
	      B_solve_phase_OK[index_] = 0;
	    }
	  }
	}
      }
    }
  } //   if(solve_3D_OK)
  else{

#pragma omp parallel for default(none) private(I, J, index_)		\
  shared(solve_this_phase_OK, dom_matrix, P_solve_phase_OK,		\
	 E_solve_phase_OK, W_solve_phase_OK, N_solve_phase_OK, S_solve_phase_OK)

    for (J=0; J<Ny; J++){
      for (I=0; I<Nx; I++){
      
	index_ = J*Nx + I;

	/* Setting phase flags */
	if(solve_this_phase_OK[ dom_matrix[index_] ]){
	  P_solve_phase_OK[index_] = 1;
	}
	else{
	  P_solve_phase_OK[index_] = 0;
	}

	if(I != Nx-1){
	  if(solve_this_phase_OK[ dom_matrix[index_+1] ]){
	    E_solve_phase_OK[index_] = 1;
	  }
	  else{
	    E_solve_phase_OK[index_] = 0;
	  }
	}

	if(I != 0){
	  if(solve_this_phase_OK[ dom_matrix[index_-1] ]){
	    W_solve_phase_OK[index_] = 1;
	  }
	  else{
	    W_solve_phase_OK[index_] = 0;
	  }
	}
	
	if(J != Ny-1){
	  if(solve_this_phase_OK[ dom_matrix[index_+Nx] ]){
	    N_solve_phase_OK[index_] = 1;
	  }
	  else{
	    N_solve_phase_OK[index_] = 0;
	  }
	}

	if(J != 0){
	  if(solve_this_phase_OK[ dom_matrix[index_-Nx] ]){
	    S_solve_phase_OK[index_] = 1;
	  }
	  else{
	    S_solve_phase_OK[index_] = 0;
	  }
	}
		
      }
    }
    
  }
  
  return ret_value;
}

/* COMPUTE LINES */
int compute_lines(Data_Mem *data){

  int ret_value = 0;

  int i, j, line_ind, index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int lines = data->lines;

  double *nodes_x = data->nodes_x;
  double *nodes_y = data->nodes_y;
  double *Delta_x = data->Delta_x;
  double *Delta_y = data->Delta_y;
  
  int *cut_matrix = data->cut_matrix;

  /* Vertices inferior (1) y superior (2) 
     que determinan la arista de una celda de la grilla */
  double x1[3] = {0};
  double x2[3] = {0};
  /* Vertices que definen una linea */
  double x1l[3], x2l[3];
  double pend, inters;
  /* Valor de function test en los vértices */
  double f1, f2;
  
  for (line_ind = 0; line_ind < lines; line_ind++){
	
    x1l[0] = data->line_points[0 + 2*line_ind];
    x2l[0] = data->line_points[1 + 2*line_ind];
    x1l[1] = data->line_points[2*lines + 2*line_ind];
    x2l[1] = data->line_points[2*lines + 1 + 2*line_ind];

    pend = (x2l[1] - x1l[1]) / (x2l[0] - x1l[0]);
    inters = x1l[1] - pend*x1l[0];

    /* we faces */
    for (j = 1; j < Ny-1; j++){
      for (i = 1; i < Nx -1; i++){
	index_ = Nx*j + i;
	x1[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	x2[0] = x1[0];
	x1[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	x2[1] = nodes_y[index_] + 0.5*Delta_y[index_];
	f1 = f_test_line(x1[0], x1[1], pend, inters);
	f2 = f_test_line(x2[0], x2[1], pend, inters);
	if (f1*f2 < 0){
	  cut_matrix[index_] = 1;
	}
	x1[0] = nodes_x[index_] + 0.5*Delta_x[index_];
	x2[0] = x1[0];
	f1 = f_test_line(x1[0], x1[1], pend, inters);
	f2 = f_test_line(x2[0], x2[1], pend, inters);
	if (f1*f2 < 0){
	  cut_matrix[index_] = 1;
	}
      }
    }

    /* Caras horizontales */
    for (i = 1; i < Nx-1; i++){
      for (j = 1; j < Ny -1; j++){
	index_ = Nx*j + i;
	x1[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	x2[0] = nodes_x[index_] + 0.5*Delta_x[index_];
	x1[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	x2[1] = x1[1];
	f1 = f_test_line(x1[0], x1[1], pend, inters);
	f2 = f_test_line(x2[0], x2[1], pend, inters);
	if (f1*f2 < 0){
	  cut_matrix[index_] = 1;
	}
	x1[1] = nodes_y[index_] + 0.5*Delta_y[index_];
	x2[1] = x1[1];
	f1 = f_test_line(x1[0], x1[1], pend, inters);
	f2 = f_test_line(x2[0], x2[1], pend, inters);
	if (f1*f2 < 0){
	  cut_matrix[index_] = 1;
	}
      }
    }
  }
  
  return ret_value;
}

/* SETTING CUT_MATRIX */
int set_cut_matrix(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: set_cut_matrix
    Sets array (int, NxNy) cut_matrix to the number (curve+1) for cells cut by curve.
  
    RETURNS: 0 (ret_value not fully implemented).
  
    MODIFIED: 11-06-2019
  */
  /*****************************************************************************/

  int ret_value = 0;

  int I, J, index_, curv_ind;

  int curve_factor;

  const int curves = data->curves;
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  int *cut_matrix = data->cut_matrix;
  
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  /* Function value at lower (1) and upper (2) vertexes */
  double f1, f2;
  
  /* Lower (1) and upper (2) vertexes that define the cell face */
  double x1[3] = {0};
  double x2[3] = {0};
  
  /* Temporal storage for the quadric coefficients */
  double pc_temp[16];

  const double pol_tol = data->pol_tol; 

  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *poly_coeffs = data->poly_coeffs;
  
  for (curv_ind = 0; curv_ind < curves; curv_ind++){
    
    for (I = 0; I < 16; I++){
      pc_temp[I] = poly_coeffs[curv_ind*16 + I];
    }
    
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

    /* Edges parallel to x axis */
    
#pragma omp parallel for default(none) firstprivate(x1, x2) private(I, J, index_, f1, f2) \
  shared(vertex_x, vertex_y, curve_factor, pc_temp, curv_ind, cut_matrix)
    
    for(J=0; J<Ny-1; J++){
      for(I=0; I<Nx-2; I++){

	index_ = J*Nx + I + 1;
	x1[0] = vertex_x[I];
	x2[0] = vertex_x[I+1];
	x1[1] = vertex_y[J];
	x2[1] = x1[1];

	f1 = polyn(x1, pc_temp) * curve_factor;
	f2 = polyn(x2, pc_temp) * curve_factor;
	if (((f1 < 0) && (fabs(f2) < pol_tol)) || 
	    ((f2 < 0) && (fabs(f1) < pol_tol))){

#pragma omp critical
	  {
	    cut_matrix[index_] = curv_ind + 1;
	    cut_matrix[index_+Nx] = curv_ind + 1;
	  }
	}
	else if (f1*f2 < 0){
	  
#pragma omp critical
	  {
	    cut_matrix[index_] = curv_ind + 1;
	    cut_matrix[index_+Nx] = curv_ind + 1;
	  }
	}
	
      }
    }

    
#pragma omp parallel for default(none) firstprivate(x1, x2) private(I, J, index_, f1, f2) \
  shared(vertex_x, vertex_y, curve_factor, pc_temp, cut_matrix, curv_ind)
    
    /* Edges parallel to y axis */

    for(J=0; J<Ny-2; J++){
      for(I=0; I<Nx-1; I++){
	
	index_ = (J+1)*Nx + I;
	x1[0] = vertex_x[I];
	x2[0] = x1[0];
	x1[1] = vertex_y[J];
	x2[1] = vertex_y[J+1];

	f1 = polyn(x1, pc_temp) * curve_factor;
	f2 = polyn(x2, pc_temp) * curve_factor;
	if (((f1 < 0) && (fabs(f2) < pol_tol)) || 
	    ((f2 < 0) && (fabs(f1) < pol_tol))){

#pragma omp critical
	  {
	    cut_matrix[index_] = curv_ind + 1;
	    cut_matrix[index_+1] = curv_ind + 1;
	  }
	}
	else if (f1*f2 < 0){

#pragma omp critical
	  {
	    cut_matrix[index_] = curv_ind + 1;
	    cut_matrix[index_+1] = curv_ind + 1;
	  }
	}
	
      }
    }
  } //   for (curv_ind = 0; curv_ind < curves; curv_ind++)
   
  return ret_value;
}

/* SETTING DOM_MATRIX */ 
int set_dom_matrix(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: set_dom_matrix
    Sets array (int, NxNy) domain_matrix, (int, nxNy) u_dom_matrix and 
    (int, Nxny) v_dom_matrix to the number curve+1 for cells inside curve if
    curve factor is positive or outside curve if curve factor is negative.
  
    RETURNS: 0 (ret_value not fully implemented).
  
    MODIFIED: 05-02-2019
  */
  /*****************************************************************************/
  int ret_value = 0;
  int i, j, curv_ind, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  const int curves = data->curves;

  int *u_dom_matrix = data->u_dom_matrix;
  int *v_dom_matrix = data->v_dom_matrix; 
  int *dom_matrix = data->domain_matrix;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double curve_factor;
  /* Vector que almacena temporalmente los coeficientes 
     del polinomio de una curva */
  double pc_temp[16];
  /* Coordenadas del centro de la celda */
  double x_cell[3] = {0};

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *poly_coeffs = data->poly_coeffs;  
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;

  for (curv_ind = 0; curv_ind < curves; curv_ind++){
    for (i = 0; i < 16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) firstprivate(x_cell) private(i, j, index_) \
  shared(nodes_x, nodes_y, pc_temp, curve_factor, dom_matrix, curv_ind)
    
    for(j=0; j<Ny; j++){
      for(i=0; i<Nx; i++){
	index_ = Nx*j + i;
	/* Get cell center coordinates */
        x_cell[0] = nodes_x[index_];
        x_cell[1] = nodes_y[index_];
	/* Sets index in domain matrix for different phases */
	if(polyn(x_cell, pc_temp) * curve_factor < 0){
          dom_matrix[index_] = curv_ind + 1;
	}
      }
    }

    /* u */
#pragma omp parallel for default(none) firstprivate(x_cell) private(i, j, index_) \
  shared(nodes_x_u, nodes_y_u, pc_temp, curve_factor, u_dom_matrix, curv_ind)
    for(j=0; j<Ny; j++){
      for(i=0; i<nx; i++){
	index_ = j*nx + i;
	x_cell[0] = nodes_x_u[index_];
	x_cell[1] = nodes_y_u[index_];
	if(polyn(x_cell, pc_temp) * curve_factor < 0){
          u_dom_matrix[index_] = curv_ind + 1;
	}	
      }
    }

    /* v */
#pragma omp parallel for default(none) firstprivate(x_cell) private(i, j, index_) \
  shared(nodes_x_v, nodes_y_v, pc_temp, curve_factor, v_dom_matrix, curv_ind)
    for(j=0; j<ny; j++){
      for(i=0; i<Nx; i++){
	index_ = j*Nx + i;
	x_cell[0] = nodes_x_v[index_];
	x_cell[1] = nodes_y_v[index_];
	if(polyn(x_cell, pc_temp) * curve_factor < 0){
          v_dom_matrix[index_] = curv_ind + 1;
	}	
      }
    }
  }
  
  return ret_value;
}

/* SET_PHASE_CHANGE_MATRIX */
/*****************************************************************************/
/**
   Sets:

   Array Data_Mem::phase_change_matrix

   @param data
  
   @return 0
  
   MODIFIED: 15-07-2019
*/
int set_phase_change_matrix(Data_Mem *data){

  int I, J, K;
  int index_, phase_index;
  int ret_value = 0; 
 
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int solve_3D_OK = data->solve_3D_OK;

  int *phase_change_matrix = data->phase_change_matrix;

  const int *dom_matrix = data->domain_matrix;
  const int *solve_this_phase_OK = data->solve_this_phase_OK;
  const int *curve_phase_change_OK = data->curve_phase_change_OK;

  if(solve_3D_OK){
  
#pragma omp parallel for default(none) private(I, J, K, index_, phase_index) \
  shared(solve_this_phase_OK, curve_phase_change_OK, dom_matrix, phase_change_matrix)

    for(K=1; K<(Nz-1); K++){
      for(J=1; J<(Ny-1); J++){
	for(I=1; I<(Nx-1); I++){

	  index_ = K*Nx*Ny + J*Nx + I;
	  phase_index = dom_matrix[index_];

	  if (phase_index != 0){

	    if(solve_this_phase_OK[phase_index] && curve_phase_change_OK[phase_index-1]){
	      phase_change_matrix[index_] = 1;
	    }
	  }
	}
      }
    }

  }
  else{

#pragma omp parallel for default(none) private(I, J, index_, phase_index) \
  shared(solve_this_phase_OK, curve_phase_change_OK, dom_matrix, phase_change_matrix)

    for(J=1; J<(Ny-1); J++){
      for(I=1; I<(Nx-1); I++){

	index_ = J*Nx + I;
	phase_index = dom_matrix[index_];

	if (phase_index != 0){

	  if(solve_this_phase_OK[phase_index] && curve_phase_change_OK[phase_index-1]){
	    phase_change_matrix[index_] = 1;
	  }
	}
      }
    }

  }

 
  return ret_value;
}

/* SET_CUT_MATRIX_3D */
int set_cut_matrix_3D(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: set_cut_matrix_3D
    Sets array (int, NxNyNz) cut_matrix to the number (curve+1) for cells cut by curve for 3D case.
  
    RETURNS: 0 (ret_value not fully implemented).
  
    MODIFIED: 11-06-2019
  */
  /*****************************************************************************/

  int ret_value = 0;

  int I, J, K, index_, curv_ind;

  int curve_factor;

  const int curves = data->curves;
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  int *cut_matrix = data->cut_matrix;
  
  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  /* Function value at lower (1) and upper (2) vertexes */
  double f1, f2;
  
  /* Lower (1) and upper (2) vertexes that define the cell edge */
  double x1[3] = {0};
  double x2[3] = {0};
  
  /* Temporal storage for the quadric coefficients */
  double pc_temp[16];

  const double pol_tol = data->pol_tol; 

  const double *poly_coeffs = data->poly_coeffs;
  const double *vertex_x = data->vertex_x;
  const double *vertex_y = data->vertex_y;
  const double *vertex_z = data->vertex_z;
  
  for (curv_ind = 0; curv_ind < curves; curv_ind++){
    
    for (I = 0; I < 16; I++){
      pc_temp[I] = poly_coeffs[curv_ind*16 + I];
    }
    
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

    /* Edges parallel to x axis */

#pragma omp parallel for default(none) private(I, J, K, index_, x1, x2, f1, f2) \
  shared(vertex_x, vertex_y, vertex_z, curve_factor, pc_temp, cut_matrix, curv_ind)
    
    for(K=0; K<Nz-1; K++){
      for(J=0; J<Ny-1; J++){
	for(I=0; I<Nx-2; I++){

	  index_ = K*Nx*Ny + J*Nx + I + 1;
	  x1[0] = vertex_x[I];
	  x2[0] = vertex_x[I+1];
	  x1[1] = vertex_y[J];
	  x2[1] = x1[1];
	  x1[2] = vertex_z[K];
	  x2[2] = x1[2];
	  
	  f1 = polyn(x1, pc_temp) * curve_factor;
	  f2 = polyn(x2, pc_temp) * curve_factor;
	  if (((f1 < 0) && (fabs(f2) < pol_tol)) || 
	      ((f2 < 0) && (fabs(f1) < pol_tol))){
#pragma omp critical
	    {
	      cut_matrix[index_] = curv_ind + 1;
	      cut_matrix[index_+Nx] = curv_ind + 1;
	      cut_matrix[index_+Nx*Ny] = curv_ind + 1;
	      cut_matrix[index_+Nx+Nx*Ny] = curv_ind + 1;
	    }
	  }
	  else if (f1*f2 < 0){
#pragma omp critical
	    {
	      cut_matrix[index_] = curv_ind + 1;
	      cut_matrix[index_+Nx] = curv_ind + 1;
	      cut_matrix[index_+Nx*Ny] = curv_ind + 1;
	      cut_matrix[index_+Nx+Nx*Ny] = curv_ind + 1;
	    }
	  
	  }
	}
      }
    } //     for(K=0; K<Nz-1; K++)
    

    /* Edges parallel to y axis */

#pragma omp parallel for default(none) private(I, J, K, index_, x1, x2, f1, f2) \
  shared(vertex_x, vertex_y, vertex_z, curve_factor, pc_temp, cut_matrix, curv_ind)
    
    for(K=0; K<Nz-1; K++){
      for(J=0; J<Ny-2; J++){
	for(I=0; I<Nx-1; I++){

	  index_ = K*Nx*Ny + (J+1)*Nx + I;
	  x1[0] = vertex_x[I];
	  x2[0] = x1[0];
	  x1[1] = vertex_y[J];
	  x2[1] = vertex_y[J+1];
	  x1[2] = vertex_z[K];
	  x2[2] = x1[2];

	  f1 = polyn(x1, pc_temp) * curve_factor;
	  f2 = polyn(x2, pc_temp) * curve_factor;
	  if (((f1 < 0) && (fabs(f2) < pol_tol)) || 
	      ((f2 < 0) && (fabs(f1) < pol_tol))){
#pragma omp critical
	    {
	      cut_matrix[index_] = curv_ind + 1;
	      cut_matrix[index_+1] = curv_ind + 1;
	      cut_matrix[index_+Nx*Ny] = curv_ind + 1;
	      cut_matrix[index_+1+Nx*Ny] = curv_ind + 1;
	    }
	  }
	  else if (f1*f2 < 0){
#pragma omp critical
	    {
	      cut_matrix[index_] = curv_ind + 1;
	      cut_matrix[index_+1] = curv_ind + 1;
	      cut_matrix[index_+Nx*Ny] = curv_ind + 1;
	      cut_matrix[index_+1+Nx*Ny] = curv_ind + 1;
	    }

	  }
	}
      }
    }

    /* Edges parallel to z axis */

#pragma omp parallel for default(none) private(I, J, K, index_, x1, x2, f1, f2) \
  shared(vertex_x, vertex_y, vertex_z, curve_factor, pc_temp, cut_matrix, curv_ind)    

    for(K=0; K<Nz-2; K++){
      for(J=0; J<Ny-1; J++){
	for(I=0; I<Nx-1; I++){
	  
	  index_ = (K+1)*Nx*Ny + J*Nx + I;
	  x1[0] = vertex_x[I];
	  x2[0] = x1[0];
	  x1[1] = vertex_y[J];
	  x2[1] = x1[1];
	  x1[2] = vertex_z[K];
	  x2[2] = vertex_z[K+1];
	  
	  f1 = polyn(x1, pc_temp) * curve_factor;
	  f2 = polyn(x2, pc_temp) * curve_factor;
	  if (((f1 < 0) && (fabs(f2) < pol_tol)) || 
	      ((f2 < 0) && (fabs(f1) < pol_tol))){
#pragma omp critical
	    {
	      cut_matrix[index_] = curv_ind + 1;
	      cut_matrix[index_+1] = curv_ind + 1;
	      cut_matrix[index_+Nx] = curv_ind + 1;
	      cut_matrix[index_+1+Nx] = curv_ind + 1;
	    }
	  }
	  else if (f1*f2 < 0){
#pragma omp critical
	    {
	      cut_matrix[index_] = curv_ind + 1;
	      cut_matrix[index_+1] = curv_ind + 1;
	      cut_matrix[index_+Nx] = curv_ind + 1;
	      cut_matrix[index_+1+Nx] = curv_ind + 1;
	    }

	  }
	}
      }
    }
      
  } //   for (curv_ind = 0; curv_ind < curves; curv_ind++)
  
  return ret_value;
}

/* SETTING DOM_MATRIX_3D */ 
int set_dom_matrix_3D(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: set_dom_matrix_3D
    Sets array (int, NxNyNz) domain_matrix, (int, nxNyNz) u_dom_matrix, 
    (int, NxnyNz) v_dom_matrix and (int, NxNynz) w_dom_matrix, to the number 
    (curve+1) for cells inside surface if curve factor is positive or outside 
    surface if curve factor is negative.
  
    RETURNS: 0 (ret_value not fully implemented).
  
    MODIFIED: 10-07-2019
  */
  /*****************************************************************************/
  int ret_value = 0;
  int i, j, k, curv_ind, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;
  const int curves = data->curves;

  int *u_dom_matrix = data->u_dom_matrix;
  int *v_dom_matrix = data->v_dom_matrix;
  int *w_dom_matrix = data->w_dom_matrix;
  int *dom_matrix = data->domain_matrix;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double curve_factor;
  /* Vector que almacena temporalmente los coeficientes 
     del polinomio de una curva */
  double pc_temp[16];
  /* Coordenadas del centro de la celda */
  double x_cell[3] = {0};

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;
  const double *poly_coeffs = data->poly_coeffs;  
  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;
  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;
  const double *nodes_x_w = data->nodes_x_w;
  const double *nodes_y_w = data->nodes_y_w;
  const double *nodes_z_w = data->nodes_z_w;

  
  for (curv_ind = 0; curv_ind < curves; curv_ind++){
    
    for (i = 0; i < 16; i++){
      pc_temp[i] = poly_coeffs[curv_ind*16 + i];
    }
    curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);

#pragma omp parallel for default(none) private(i, j, k, index_, x_cell)	\
  shared(nodes_x, nodes_y, nodes_z, pc_temp, curve_factor, dom_matrix, curv_ind)

    for(k=0; k<Nz; k++){
      for(j=0; j<Ny; j++){
	for(i=0; i<Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  /* Get cell center coordinates */
	  x_cell[0] = nodes_x[index_];
	  x_cell[1] = nodes_y[index_];
	  x_cell[2] = nodes_z[index_];
	  /* Sets index in domain matrix for different phases */
	  if(polyn(x_cell, pc_temp) * curve_factor < 0){
	    dom_matrix[index_] = curv_ind + 1;
	  }
	}
      }
    }

    /* u */
#pragma omp parallel for default(none) private(i, j, k, index_, x_cell)	\
  shared(nodes_x_u, nodes_y_u, nodes_z_u, pc_temp, curve_factor, u_dom_matrix, curv_ind)

    for(k=0; k<Nz; k++){
      for(j=0; j<Ny; j++){
	for(i=0; i<nx; i++){
	  index_ = k*nx*Ny + j*nx + i;
	  x_cell[0] = nodes_x_u[index_];
	  x_cell[1] = nodes_y_u[index_];
	  x_cell[2] = nodes_z_u[index_];
	  if(polyn(x_cell, pc_temp) * curve_factor < 0){
	    u_dom_matrix[index_] = curv_ind + 1;
	  }	
	}
      }
    }

    /* v */
#pragma omp parallel for default(none) private(i, j, k, index_, x_cell)	\
  shared(nodes_x_v, nodes_y_v, nodes_z_v, pc_temp, curve_factor, v_dom_matrix, curv_ind)

    for(k=0; k<Nz; k++){
      for(j=0; j<ny; j++){
	for(i=0; i<Nx; i++){
	  index_ = k*Nx*ny + j*Nx + i;
	  x_cell[0] = nodes_x_v[index_];
	  x_cell[1] = nodes_y_v[index_];
	  x_cell[2] = nodes_z_v[index_];
	  if(polyn(x_cell, pc_temp) * curve_factor < 0){
	    v_dom_matrix[index_] = curv_ind + 1;
	  }	
	}
      }
    }

    /* w */
#pragma omp parallel for default(none) private(i, j, k, index_, x_cell)	\
  shared(nodes_x_w, nodes_y_w, nodes_z_w, pc_temp, curve_factor, w_dom_matrix, curv_ind)
    
    for(k=0; k<nz; k++){
      for(j=0; j<Ny; j++){
	for(i=0; i<Nx; i++){
	  index_ = k*Nx*Ny + j*Nx + i;
	  x_cell[0] = nodes_x_w[index_];
	  x_cell[1] = nodes_y_w[index_];
	  x_cell[2] = nodes_z_w[index_];
	  if(polyn(x_cell, pc_temp) * curve_factor < 0){
	    w_dom_matrix[index_] = curv_ind + 1;	   
	  }
	}
      }
    }
    
  } //   for (curv_ind = 0; curv_ind < curves; curv_ind++)
  
  return ret_value;
}

/* SET_EPS_EPS_S_NORM_PAR_LEGACY_3D */
/*****************************************************************************/
/**
  
   Sets:

   Array (int, NxNyNz) Data_Mem::epsilon_x.

   Array (int, NxNyNz) Data_Mem::epsilon_x_sol

   Arrays eps_y and eps_y_sol measure the corresponding distance in y direction.

   Arrays eps_z and eps_z_sol measure the corresponding distance in z direction.

   Array (int, NxNyNz) norm_vec_x_x: x component of normal vector at interface,
   when interface is found in x direction.

   Array (int, NxNyNz) norm_vec_x_y: y component of normal vector at interface
   when interface is found in x direction.

   Array (int, NxNyNz) norm_vec_x_z: z component of normal vector at interface
   when interface is found in z direction.

   Arrays norm_vec_y_* contains corresponding components when
   interface is found in y direction.

   Arrays norm_vec_z_* contains corresponding components when 
   interface is found in z direction.

   Array (int, NxNyNz) norm_x: x component of normal vector evaluated at fluid 
   cell center.

   Array (int, NxNyNz) norm_y: y component of normal vector evaluated at fluid
   cell center.

   Array (int, NxNyNz) norm_z: z component of normal vector evaluated at fluid 
   cell center.

   @param data
  
   @return 0 (ret_value not fully implemented).
  
   MODIFIED: 11-07-2019
*/
int set_eps_eps_s_norm_par_legacy_3D(Data_Mem *data){

  int ret_value = 0;

  int I, J, K, index_, curv_ind;
  int int_cell_OK = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int curves = data->curves;

  const int *dom_matrix = data->domain_matrix;

  double dist;

  double pc_temp[16];
  /* Coordenadas del centro de la celda */
  double x_cell[3] = {0};
  double x_inters[3] = {0};
  double x_nb[3] = {0};
  double nvect[3] = {0};
  double aux_vec[3] = {0};

  double *eps_x = data->epsilon_x;
  double *eps_y = data->epsilon_y;
  double *eps_z = data->epsilon_z;
  double *eps_x_sol = data->epsilon_x_sol;
  double *eps_y_sol = data->epsilon_y_sol;
  double *eps_z_sol = data->epsilon_z_sol;
  
  double *norm_vec_x_x = data->norm_vec_x_x;
  double *norm_vec_x_y = data->norm_vec_x_y;
  double *norm_vec_x_z = data->norm_vec_x_z;
  double *norm_vec_y_x = data->norm_vec_y_x;
  double *norm_vec_y_y = data->norm_vec_y_y;
  double *norm_vec_y_z = data->norm_vec_y_z;
  double *norm_vec_z_x = data->norm_vec_z_x;
  double *norm_vec_z_y = data->norm_vec_z_y;
  double *norm_vec_z_z = data->norm_vec_z_z;
  double *par_vec_x_x = data->par_vec_x_x;
  double *par_vec_x_y = data->par_vec_x_y;
  double *par_vec_x_z = data->par_vec_x_z;
  double *par_vec_y_x = data->par_vec_y_x;
  double *par_vec_y_y = data->par_vec_y_y;
  double *par_vec_y_z = data->par_vec_y_z;
  double *par_vec_z_x = data->par_vec_z_x;
  double *par_vec_z_y = data->par_vec_z_y;
  double *par_vec_z_z = data->par_vec_z_z;
  double *par_vec_x_x_2 = data->par_vec_x_x_2;
  double *par_vec_x_y_2 = data->par_vec_x_y_2;
  double *par_vec_x_z_2 = data->par_vec_x_z_2;
  double *par_vec_y_x_2 = data->par_vec_y_x_2;
  double *par_vec_y_y_2 = data->par_vec_y_y_2;
  double *par_vec_y_z_2 = data->par_vec_y_z_2;
  double *par_vec_z_x_2 = data->par_vec_z_x_2;
  double *par_vec_z_y_2 = data->par_vec_z_y_2;
  double *par_vec_z_z_2 = data->par_vec_z_z_2;  

  double *norm_x = data->norm_x;
  double *norm_y = data->norm_y;
  double *norm_z = data->norm_z;
  double *par_x = data->par_x;
  double *par_y = data->par_y;
  double *par_z = data->par_z;
  double *par_x_2 = data->par_x_2;
  double *par_y_2 = data->par_y_2;
  double *par_z_2 = data->par_z_2;  

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;
  
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;
  const double *Delta_zB = data->Delta_zB;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_zT = data->Delta_zT;

  const double *poly_coeffs = data->poly_coeffs;
  
  if(curves > 0){

    for (curv_ind = 0; curv_ind < curves; curv_ind++){
      
      for (I=0; I<16; I++){
	pc_temp[I] = poly_coeffs[curv_ind*16 + I];
      }

#pragma omp parallel for default(none) firstprivate(x_cell, x_nb, aux_vec) \
  private(I, J, K, index_, int_cell_OK, dist, x_inters, nvect)		\
  shared(curv_ind, nodes_x, nodes_y, nodes_z,				\
	 Delta_xE, Delta_xW, Delta_yN, Delta_yS, Delta_zT, Delta_zB,	\
	 pc_temp, dom_matrix,						\
	 eps_x, eps_y, eps_z, eps_x_sol, eps_y_sol, eps_z_sol,		\
	 norm_x, norm_y, norm_z, norm_vec_x_x, norm_vec_x_y, norm_vec_x_z, \
	 norm_vec_y_x, norm_vec_y_y, norm_vec_y_z, norm_vec_z_x, norm_vec_z_y, \
	 norm_vec_z_z, par_vec_x_x, par_vec_x_y, par_vec_x_z, par_vec_y_x, \
	 par_vec_y_y, par_vec_y_z, par_vec_z_x, par_vec_z_y, par_vec_z_z, \
	 par_vec_x_x_2, par_vec_x_y_2, par_vec_x_z_2, par_vec_y_x_2, par_vec_y_y_2, \
	 par_vec_y_z_2, par_vec_z_x_2, par_vec_z_y_2, par_vec_z_z_2, par_x, par_y, \
	 par_z, par_x_2, par_y_2, par_z_2)

      for (K=1; K<Nz-1; K++){
	for (J=1; J<Ny-1; J++){
	  for (I=1; I<Nx-1; I++){
	  
	    index_ = K*Nx*Ny + J*Nx + I;
	    int_cell_OK = 0;

	    x_cell[0] = nodes_x[index_];
	    x_cell[1] = nodes_y[index_];
	    x_cell[2] = nodes_z[index_];
		
	    if (dom_matrix[index_] == 0){
	      
	      if (dom_matrix[index_ + 1] == curv_ind + 1){
		
		int_cell_OK = 1;
		/* Get intersection */
		x_nb[0] = nodes_x[index_+1];
		x_nb[1] = nodes_y[index_+1];
		x_nb[2] = nodes_z[index_+1];
		bisection(x_inters, x_cell, x_nb, pc_temp);	       
		dist = vec_dist(x_cell, x_inters);
		/* Get normal vector on the curve */
		normal(nvect, x_inters, pc_temp);
		/* Get epsilon values */
		eps_x[index_] = dist / Delta_xE[index_];
		eps_x_sol[index_ + 1] = 1 - eps_x[index_];
		norm_vec_x_x[index_] = nvect[0];
		norm_vec_x_y[index_] = nvect[1];
		norm_vec_x_z[index_] = nvect[2];
		/* Get parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_1_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_1(aux_vec, nvect);
		}
		
		par_vec_x_x[index_] = aux_vec[0];
		par_vec_x_y[index_] = aux_vec[1];
		par_vec_x_z[index_] = aux_vec[2];
		/* Get second parallel vector */

		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_2_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_2(aux_vec, nvect);
		}
		
		par_vec_x_x_2[index_] = aux_vec[0];
		par_vec_x_y_2[index_] = aux_vec[1];
		par_vec_x_z_2[index_] = aux_vec[2];
	      }
	      
	      if (dom_matrix[index_ - 1] == curv_ind + 1){
		
		int_cell_OK = 1;
		/* Get intersection */		
		x_nb[0] = nodes_x[index_-1];
		x_nb[1] = nodes_y[index_-1];
		x_nb[2] = nodes_z[index_-1];
		bisection(x_inters, x_cell, x_nb, pc_temp);
		dist = vec_dist(x_cell, x_inters);
		/* Get normal vector on the curve */		
		normal(nvect, x_inters, pc_temp);
		/* Get epsilon values */		
		eps_x[index_] = dist / Delta_xW[index_];
		eps_x_sol[index_ - 1] = 1 - eps_x[index_];
		norm_vec_x_x[index_] = nvect[0];
		norm_vec_x_y[index_] = nvect[1];
		norm_vec_x_z[index_] = nvect[2];
		/* Get parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_1_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_1(aux_vec, nvect);
		}
		
		par_vec_x_x[index_] = aux_vec[0];
		par_vec_x_y[index_] = aux_vec[1];
		par_vec_x_z[index_] = aux_vec[2];
		/* Get second parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_2_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_2(aux_vec, nvect);
		}
		
		par_vec_x_x_2[index_] = aux_vec[0];
		par_vec_x_y_2[index_] = aux_vec[1];
		par_vec_x_z_2[index_] = aux_vec[2];		
	      }
	      
	      if (dom_matrix[index_ + Nx] == curv_ind + 1){
		
		int_cell_OK = 1;
		/* Get intersection */				
		x_nb[0] = nodes_x[index_+Nx];
		x_nb[1] = nodes_y[index_+Nx];
		x_nb[2] = nodes_z[index_+Nx];
		bisection(x_inters, x_cell, x_nb, pc_temp);
		dist = vec_dist(x_cell, x_inters);
		/* Get normal vector on the curve */				
		normal(nvect, x_inters, pc_temp);
		/* Get epsilon values */				
		eps_y[index_] = dist / Delta_yN[index_];
		eps_y_sol[index_ + Nx] = 1- eps_y[index_];
		norm_vec_y_x[index_] = nvect[0];
		norm_vec_y_y[index_] = nvect[1];
		norm_vec_y_z[index_] = nvect[2];
		/* Get parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_1_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_1(aux_vec, nvect);
		}
		
		par_vec_y_x[index_] = aux_vec[0];
		par_vec_y_y[index_] = aux_vec[1];
		par_vec_y_z[index_] = aux_vec[2];
		/* Get second parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_2_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_2(aux_vec, nvect);
		}
		
		par_vec_y_x_2[index_] = aux_vec[0];
		par_vec_y_y_2[index_] = aux_vec[1];
		par_vec_y_z_2[index_] = aux_vec[2];		
	      }
	      
	      if (dom_matrix[index_ - Nx] == curv_ind + 1){
		
		int_cell_OK = 1;
		/* Get intersection */				
		x_nb[0] = nodes_x[index_-Nx];
		x_nb[1] = nodes_y[index_-Nx];
		x_nb[2] = nodes_z[index_-Nx];
		bisection(x_inters, x_cell, x_nb, pc_temp);
		dist = vec_dist(x_cell, x_inters);
		/* Get normal vector on the curve */				
		normal(nvect, x_inters, pc_temp);
		eps_y[index_] = dist / Delta_yS[index_];
		eps_y_sol[index_ - Nx] = 1 - eps_y[index_];
		norm_vec_y_x[index_] = nvect[0];
		norm_vec_y_y[index_] = nvect[1];
		norm_vec_y_z[index_] = nvect[2];
		/* Get parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_1_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_1(aux_vec, nvect);
		}
		
		par_vec_y_x[index_] = aux_vec[0];
		par_vec_y_y[index_] = aux_vec[1];
		par_vec_y_z[index_] = aux_vec[2];
		/* Get second parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_2_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_2(aux_vec, nvect);
		}
		
		par_vec_y_x_2[index_] = aux_vec[0];
		par_vec_y_y_2[index_] = aux_vec[1];
		par_vec_y_z_2[index_] = aux_vec[2];				
	      }

	      if (dom_matrix[index_+Nx*Ny] == curv_ind + 1){
		
		int_cell_OK = 1;
		/* Get intersection */				
		x_nb[0] = nodes_x[index_+Nx*Ny];
		x_nb[1] = nodes_y[index_+Nx*Ny];
		x_nb[2] = nodes_z[index_+Nx*Ny];
		bisection(x_inters, x_cell, x_nb, pc_temp);
		dist = vec_dist(x_cell, x_inters);
		/* Get normal vector on the curve */				
		normal(nvect, x_inters, pc_temp);
		eps_z[index_] = dist / Delta_zT[index_];
		eps_z_sol[index_+Nx*Ny] = 1 - eps_z[index_];
		norm_vec_z_x[index_] = nvect[0];
		norm_vec_z_y[index_] = nvect[1];
		norm_vec_z_z[index_] = nvect[2];
		/* Get parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_1_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_1(aux_vec, nvect);
		}
		
		par_vec_z_x[index_] = aux_vec[0];
		par_vec_z_y[index_] = aux_vec[1];
		par_vec_z_z[index_] = aux_vec[2];
		/* Get second parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_2_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_2(aux_vec, nvect);
		}
		
		par_vec_z_x_2[index_] = aux_vec[0];
		par_vec_z_y_2[index_] = aux_vec[1];
		par_vec_z_z_2[index_] = aux_vec[2];				
	      }

	      if (dom_matrix[index_-Nx*Ny] == curv_ind + 1){
		
		int_cell_OK = 1;
		/* Get intersection */				
		x_nb[0] = nodes_x[index_-Nx*Ny];
		x_nb[1] = nodes_y[index_-Nx*Ny];
		x_nb[2] = nodes_z[index_-Nx*Ny];
		bisection(x_inters, x_cell, x_nb, pc_temp);
		dist = vec_dist(x_cell, x_inters);
		/* Get normal vector on the curve */				
		normal(nvect, x_inters, pc_temp);
		eps_z[index_] = dist / Delta_zB[index_];
		eps_z_sol[index_-Nx*Ny] = 1 - eps_z[index_];
		norm_vec_z_x[index_] = nvect[0];
		norm_vec_z_y[index_] = nvect[1];
		norm_vec_z_z[index_] = nvect[2];
		/* Get parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_1_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_1(aux_vec, nvect);
		}
		
		par_vec_z_x[index_] = aux_vec[0];
		par_vec_z_y[index_] = aux_vec[1];
		par_vec_z_z[index_] = aux_vec[2];
		/* Get second parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_2_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_2(aux_vec, nvect);
		}
		
		par_vec_z_x_2[index_] = aux_vec[0];
		par_vec_z_y_2[index_] = aux_vec[1];
		par_vec_z_z_2[index_] = aux_vec[2];						
	      }

	      if(int_cell_OK){
		/* Get normal vector for cell at cell center */
		normal(nvect, x_cell, pc_temp);
		norm_x[index_] = nvect[0];
		norm_y[index_] = nvect[1];
		norm_z[index_] = nvect[2];
		/* Get parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_1_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_1(aux_vec, nvect);
		}
		
		par_x[index_] = aux_vec[0];
		par_y[index_] = aux_vec[1];
		par_z[index_] = aux_vec[2];
		/* Get second parallel vector */
		
		if(all_pos_neg_octant(nvect)){
		  compute_par_vec_2_2(aux_vec, nvect);
		}
		else{
		  compute_par_vec_2(aux_vec, nvect);
		}
		
		par_x_2[index_] = aux_vec[0];
		par_y_2[index_] = aux_vec[1];
		par_z_2[index_] = aux_vec[2];						
	      }
	    } // if (dom_matrix[index_] == 0)
	  }// for (i = 1; i < Nx-1; i++){
	}// for (j = 1; j < Ny-1; j++){
      }//  for (K=1; K<Nz-1; K++)
    }//   for (curv_ind = 0; curv_ind < curves; curv_ind++)

  }

  return ret_value;

}

/* COMPUTE_Y_WALL_3D */
/*****************************************************************************/
/**
  
   Sets:

   Array (double, NxNy) Data_Mem::y_wall

   @return 0 (ret_value not fully implemented).
  
   MODIFIED: 05-02-2019
*/
int compute_y_wall_3D(Data_Mem *data){

  mat_t *matfp = NULL;

  char msg[SIZE];
  
  const char *geom_filename = data->geom_filename;

  int I, J, K, index_, curv_ind, i;
  int compute_y_wall_OK = 1;
  int ret_value = 0;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int curves = data->curves;
  const int read_y_wall_OK = data->read_y_wall_OK;
  const int bc_west_type = data->bc_flow_west.type;
  const int bc_east_type = data->bc_flow_east.type;
  const int bc_south_type = data->bc_flow_south.type;
  const int bc_north_type = data->bc_flow_north.type;
  const int bc_bottom_type = data->bc_flow_bottom.type;
  const int bc_top_type = data->bc_flow_top.type;

  const int *domain_matrix = data->domain_matrix;

  double min_dist, curve_dist;
  double pc_temp[16];
  double x[3];
  
  const double Lx = data->Lx;
  const double Ly = data->Ly;
  const double Lz = data->Lz;
  const double cell_size = data->cell_size;

  double *y_wall = data->y_wall;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;
  const double *poly_coeffs = data->poly_coeffs;
  
  Wins *wins = &(data->wins);
  
  if(read_y_wall_OK){

    matfp = Mat_Open(geom_filename, MAT_ACC_RDONLY);
    
    if(matfp == NULL){
      alert_msg(wins, "Could not open GEOM file!!... computing y_wall instead");
      compute_y_wall_OK = 1;
    }
    else{
      
      checkCall(read_array_from_MAT_file(y_wall, "y_wall", Nx, Ny, Nz, matfp), wins);
      
      sprintf(msg, "y_wall read from %s", geom_filename);
      info_msg(wins, msg);

      Mat_Close(matfp);
      
      compute_y_wall_OK = 0;
    }
  }
  
  if(compute_y_wall_OK){

    /* Distance to walls */
#pragma omp parallel for default(none) private(I, J, K, index_, min_dist, x) \
  shared(domain_matrix, nodes_x, nodes_y, nodes_z, y_wall)

    for(K=1; K<Nz-1; K++){
      for(J=1; J<Ny-1; J++){
	for(I=1; I<Nx-1; I++){
	
	  index_ = K*Nx*Ny + J*Nx + I;
	  min_dist = 1e6;
	  
	  if(domain_matrix[index_] == 0){
	    
	    x[0] = nodes_x[index_];
	    x[1] = nodes_y[index_];
	    x[2] = nodes_z[index_];

	    
	    /* Type = 3, wall. */
	    if(bc_west_type == 3){
	      min_dist = MIN(min_dist, x[0]);
	    }
	    if(bc_east_type == 3){
	      min_dist = MIN(min_dist, Lx - x[0]);
	    }
	    if(bc_south_type == 3){
	      min_dist = MIN(min_dist, x[1]);
	    }
	    if(bc_north_type == 3){
	      min_dist = MIN(min_dist, Ly - x[1]);
	    }
	    if(bc_bottom_type == 3){
	      min_dist = MIN(min_dist, x[2]);
	    }
	    if(bc_top_type == 3){
	      min_dist = MIN(min_dist, Lz - x[2]);
	    }
	    
	    y_wall[index_] = min_dist;
	  }
	  else{
	    y_wall[index_] = NAN;
	  }
	}
      }
    }

    /* Distance to curves */
    for(curv_ind=0; curv_ind<curves; curv_ind++){
      
      for(i=0; i<16; i++){
	pc_temp[i] = poly_coeffs[curv_ind*16 + i];
      }

#pragma omp parallel for default(none) private(I, J, K, index_, curve_dist, x) \
  shared(domain_matrix, nodes_x, nodes_y, nodes_z, y_wall, pc_temp, curv_ind)

      for(K=1; K<Nz-1; K++){
	for(J=1; J<Ny-1; J++){
	  for(I=1; I<Nx-1; I++){
	    
	    index_ = K*Nx*Ny + J*Nx + I;
	    
	    if(domain_matrix[index_] == 0){
	      x[0] = nodes_x[index_];
	      x[1] = nodes_y[index_];
	      x[2] = nodes_z[index_];

	      curve_dist = calc_dist_far(x, pc_temp, cell_size);
	      y_wall[index_] = MIN( y_wall[index_], curve_dist );
	    }
	  }
	}
      }
    }

    /* Boundary values */
#pragma omp parallel sections default(none) private(I, J, K, index_)	\
  shared(domain_matrix, y_wall)
    {
#pragma omp section 
      {
	/* West */

	I = 0;

	/* Wall */
	if(bc_west_type == 3){
	  
	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      y_wall[index_] = 0;
	    }
	  }
	}
	/* Periodic boundary */
	else if(bc_west_type == 5){
	  
	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[K*Nx*Ny + J*Nx + Nx-2];
	      }
	    }
	  }
	}
	/* Zero gradient, Fixed value, Symmetry, Constant Pressure */
	else{

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[index_+1];
	      }
	    }
	  }
	}
      } /* West section */

#pragma omp section 
      {
	/* East */
	I = Nx - 1;

	/* Wall */
	if(bc_east_type == 3){

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      y_wall[index_] = 0;
	    }
	  }
	}
	/* Periodic boundary */
	else if(bc_east_type == 5){

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[K*Nx*Ny + J*Nx + 1];
	      }
	    }
	  }
	}
	/* Zero gradient, Fixed value, Symmetry, Constant Pressure */	
	else{

	  for(K=1; K<Nz-1; K++){
	    for(J=1; J<Ny-1; J++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[index_-1];
	      }
	    }
	  }
	}
      } /* East section */

#pragma omp section 
      {
	/* South */

	J = 0;

	/* Wall */	
	if(bc_south_type == 3){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      y_wall[index_] = 0;
	    }
	  }
	}
	/* Periodic boundary */	
	else if(bc_south_type == 5){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[K*Nx*Ny + (Ny-2)*Nx + I];
	      }
	    }
	  }
	}
	/* Zero gradient, Fixed value, Symmetry, Constant Pressure */	
	else{

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[index_+Nx];
	      }
	    }
	  }
	}
      } /* South section */

#pragma omp section 
      {
	/* North */

	J = Ny - 1;

	/* Wall */
	if(bc_north_type == 3){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      y_wall[index_] = 0;
	    }
	  }
	}
	/* Periodic boundary */	
	else if(bc_north_type == 5){

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[K*Nx*Ny + 1*Nx + I];
	      }
	    }
	  }
	}
	/* Zero gradient, Fixed value, Symmetry, Constant Pressure */		
	else{

	  for(K=1; K<Nz-1; K++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[index_-Nx];
	      }
	    }
	  }
	}
      } /* North section */

#pragma omp section
      {
	/* Bottom */

	K = 0;

	/* Wall */
	if(bc_bottom_type == 3){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      y_wall[index_] = 0;
	    }
	  }
	}
	/* Periodic boundary */
	else if(bc_bottom_type == 5){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[(Nz-2)*Nx*Ny + J*Nx + I];
	      }
	    }
	  }
	}
	/* Zero gradient, Fixed value, Symmetry, Constant Pressure */
	else{

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[index_+Nx*Ny];
	      }
	    }
	  }
	}	
      } /* Bottom section */

#pragma omp section
      {
	/* Top */

	K = Nz - 1;

	/* Wall */	
	if(bc_top_type == 3){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      y_wall[index_] = 0;
	    }
	  }	  
	}
	/* Periodic boundary */
	else if(bc_top_type == 5){

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[1*Nx*Ny + J*Nx + I];
	      }
	    }
	  }
	}
	/* Zero gradient, Fixed value, Symmetry, Constant Pressure */
	else{

	  for(J=1; J<Ny-1; J++){
	    for(I=1; I<Nx-1; I++){
	      index_ = K*Nx*Ny + J*Nx + I;
	      if(domain_matrix[index_] == 0){
		y_wall[index_] = y_wall[index_-Nx*Ny];
	      }
	    }
	  }
	}
      } /* Top section */
      
    } /* sections */

  }

  return ret_value;
}

/* COMPUTE_PAR_VEC_1 */
/*****************************************************************************/
/**
   Compute parallel vector according to:

   p = (ny-nz, -nx+nz, nx-ny)

   and returns it after normalization. This vector is perpendicular to normal vector
   and also to the one computed using compute_par_vec_2 function.

   NAN is returned if normal vector has the form (a,a,a).

   @param *par_vec: Pointer to array where parallel vector will be returned.

   @param *norm_vec: Pointer to normal vector.

   @return void

   @see compute_par_vec_2

*/
void compute_par_vec_1(double *par_vec, const double *norm_vec){

  par_vec[0] = norm_vec[1] - norm_vec[2];
  par_vec[1] = -norm_vec[0] + norm_vec[2];
  par_vec[2] = norm_vec[0] - norm_vec[1];
  
  normalize_vec(par_vec);

  return;
}

/* COMPUTE_PAR_VEC_2 */
/*****************************************************************************/
/**
   Compute parallel vector according to:

   p = (ny*ny+nz*nz-nx*(ny+nz), nx*nx+nz*nz-ny*(nx+nz), nx*nx+ny*ny-nz*(nx+ny))

   and returns it after normalization. This vector is perpendicular to normal vector
   and also to the one computed using compute_par_vec_1 function.

   NAN is returned if normal vector has the form (a,a,a).

   @param *par_vec: Pointer to array where parallel vector will be returned.

   @param *norm_vec: Pointer to normal vector.

   @return void

   @see compute_par_vec_1

*/
void compute_par_vec_2(double * par_vec, const double *norm_vec){

  par_vec[0] = norm_vec[1] * norm_vec[1] + norm_vec[2] * norm_vec[2]
    - norm_vec[0] * (norm_vec[1] + norm_vec[2]);
  par_vec[1] = norm_vec[0] * norm_vec[0] + norm_vec[2] * norm_vec[2]
    - norm_vec[1] * (norm_vec[0] + norm_vec[2]);
  par_vec[2] = norm_vec[0] * norm_vec[0] + norm_vec[1] * norm_vec[1]
    - norm_vec[2] * (norm_vec[0] + norm_vec[1]);

  normalize_vec(par_vec);

  return;
}

/* ALL_POS_NEG_OCTANT */
/*****************************************************************************/
/**
   True if vector points to the (+++) or (---) octants.

*/
int all_pos_neg_octant(const double *vec){

  int flag = 0;

  if((vec[0] > 0) && (vec[1] > 0) && (vec[2] > 0)){
    flag  = 1;
  }

  if((vec[0] < 0) && (vec[1] < 0) && (vec[2] < 0)){
    flag = 1;
  }
  
  return flag;
}

/* COMPUTE_PAR_VEC_1_2 */
/*****************************************************************************/
/**
   Compute parallel vector according to:

   p = (ny+nz, -nx-nz, -nx+ny)

   and returns it after normalization. This vector is perpendicular to normal vector
   and also to the one computed using compute_par_vec_2_2 function.

   NAN is returned if normal vector has the form (a,a,-a).

   @param *par_vec: Pointer to array where parallel vector will be returned.

   @param *norm_vec: Pointer to normal vector.

   @return void

   @see compute_par_vec_2_2

*/
void compute_par_vec_1_2(double *par_vec, const double *norm_vec){

  par_vec[0] = norm_vec[1] + norm_vec[2];
  par_vec[1] = -norm_vec[0] - norm_vec[2];
  par_vec[2] = -norm_vec[0] + norm_vec[1];
  
  normalize_vec(par_vec);

  return;
}

/* COMPUTE_PAR_VEC_2 */
/*****************************************************************************/
/**
   Compute parallel vector according to:

   p = (ny*ny+nz*nz-nx*(ny-nz), nx*nx+nz*nz-ny*(nx-nz), -nx*nx-ny*ny-nz*(nx+ny))

   and returns it after normalization. This vector is perpendicular to normal vector
   and also to the one computed using compute_par_vec_1_2 function.

   NAN is returned if normal vector has the form (a,a,-a).

   @param *par_vec: Pointer to array where parallel vector will be returned.

   @param *norm_vec: Pointer to normal vector.

   @return void

   @see compute_par_vec_1_2

*/
void compute_par_vec_2_2(double * par_vec, const double *norm_vec){

  par_vec[0] = norm_vec[1] * norm_vec[1] + norm_vec[2] * norm_vec[2]
    - norm_vec[0] * (norm_vec[1] - norm_vec[2]);
  par_vec[1] = norm_vec[0] * norm_vec[0] + norm_vec[2] * norm_vec[2]
    - norm_vec[1] * (norm_vec[0] - norm_vec[2]);
  par_vec[2] = -(norm_vec[0] * norm_vec[0] + norm_vec[1] * norm_vec[1]
		 + norm_vec[2] * (norm_vec[0] + norm_vec[1]));

  normalize_vec(par_vec);

  return;
}
