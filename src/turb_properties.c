#include "include/some_defs.h"

#define D2UIDXJ2(UW,UP,UE,dxW,dxE,dx) ((((UE)-(UP))/(dxE) - ((UP)-(UW))/(dxW)) / (dx))

/* COMPUTE_TURB_PROP */
/*****************************************************************************/
/**

   Computes intermediate fields and source terms for turbulent models.

   @param data

   @return void
  
   MODIFIED: 18-03-2020
*/
void compute_turb_prop(Data_Mem *data){
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int turb_model = data->turb_model;

  const double alpha_P_k = data->alpha_P_k;
  const double alpha_S_CG_eps = data->alpha_S_CG_eps;

  double *P_k = data->P_k;
  double *P_k_lag = data->P_k_lag;

  double *S_CG_eps = data->S_CG_eps;
  double *S_CG_eps_lag = data->S_CG_eps_lag;

  /* sets:
     u_at_faces, u_at_faces_y
     v_at_faces, v_at_faces_x
     using:
     u, v, cut_cell variables
  */
  vel_at_crossed_faces(data);
  
  /* sets:
     nabla_u
     using:
     u_at_faces, u_at_faces_y
     v_at_faces, v_at_faces_x 
  */
  compute_nabla_u(data);

  /* This is needed for the model CG */
  if(turb_model == 2){
    /* sets:
       u_at_corners, v_at_corners
       using:
       u_at_faces, v_at_faces
    */
    vel_at_corners(data);
    
    compute_d2Uidxjdxk2(data);
  }
  /* sets:
     def_tens
     using: 
     nabla_u
  */
  compute_def_tens(data);

  /* sets:
     tau_turb, gamma
     using:
     def_tens
  */
  compute_tau_turb(data);

  /* This is needed for the model CG */
  if(turb_model == 2){
    compute_S_CG_eps(data);
    
    sum_relax_array(S_CG_eps, S_CG_eps_lag, S_CG_eps, alpha_S_CG_eps, Nx, Ny, Nz);
    copy_array(S_CG_eps, S_CG_eps_lag, Nx, Ny, Nz);
  }

  /* sets:
     P_k
     using:
     tau_turb, nabla_u
  */
  compute_P_k(data);
  
  /* Relax P_k */
  sum_relax_array(P_k, P_k_lag, P_k, alpha_P_k, Nx, Ny, Nz);
  copy_array(P_k, P_k_lag, Nx, Ny, Nz);

  return;
}

/* COMPUTE_P_K */
/*****************************************************************************/
/**

   Sets:

   Array (double, NxNy) Data_Mem::P_k: Turbulent kinetic energy production term, forcing it to be >= 0.

   @param data

   @return void
  
   MODIFIED: 18-03-2020
*/
void compute_P_k(Data_Mem *data){

  int I, J;
  int index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int offset = Nx*Ny;

  const int *dom_matrix = data->domain_matrix;

  double *P_k = data->P_k;

  const double *tau_turb = data->tau_turb;
  const double *nabla_u = data->nabla_u;

#pragma omp parallel for default(none)		\
  private(I, J, index_)				\
  shared(dom_matrix, P_k, tau_turb, nabla_u)
  
  for (J=1; J<(Ny-1); J++){
    for (I=1; I<(Nx-1); I++){
      
      index_ = J*Nx + I;

      if(dom_matrix[index_] == 0){
	/* P_k = -(rho u'u') : nabla u = (tau_turb : nabla_u) [=] W/m3 = Pa/s = kg/m/s3 */
	P_k[index_] = MAX(0, (tau_turb[index_ + 0*offset] * nabla_u[index_ + 0*offset] + 
			      tau_turb[index_ + 1*offset] * nabla_u[index_ + 1*offset] +
			      tau_turb[index_ + 2*offset] * nabla_u[index_ + 2*offset] + 
			      tau_turb[index_ + 3*offset] * nabla_u[index_ + 3*offset]));
      }
    }
  }
    
  return;
}

/* COMPUTE_TAU_TURB */
/*****************************************************************************/
/**

  Sets:

  Array (double, NxNy) Data_Mem::tau_turb: turbulent stress tensor.

  @param data

  @return void
  
  MODIFIED: 18-03-2020
*/
void compute_tau_turb(Data_Mem *data){

  int I, J;
  int index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int offset = Nx*Ny;

  const int turb_model = data->turb_model;

  const int *dom_matrix = data->domain_matrix;
  
  double kin_visc;
  double kin_visc_turb, kin_visc_turb_min;
  double l_ast;
  double y_ast;
  double pre_prod_for_l_ast;
  double re_turb;

  const double rho = data->rho_v[0];
  const double mu = data->mu;
  /* Fraction of the molecular kinematic viscosity to be considered as 
     minimum value of mu_turb */
  const double kin_visc_frac = 0.1;
  const double C_mu = data->C_mu;
  /* Size of the largest eddies is the width of the domain */
  const double max_mix_l = data->max_mix_l;

  double *mu_turb = data->mu_turb;
  double *gamma = data->gamma;
  double *tau_turb = data->tau_turb;
  double *f_1 = data->f_1;
  double *f_2 = data->f_2;
  double *f_mu = data->f_mu;

  const double *def_tens = data->def_tens;
  const double *k_turb = data->k_turb;
  const double *eps_turb = data->eps_turb;
  const double *y_wall = data->y_wall;

  kin_visc = mu/rho;
  kin_visc_turb_min = kin_visc_frac * kin_visc;
  
#pragma omp parallel for default(none)					\
  private(I, J, index_, l_ast, pre_prod_for_l_ast, re_turb, y_ast, kin_visc_turb) \
  shared(dom_matrix, k_turb, eps_turb, y_wall, f_1, f_2, f_mu,		\
	 mu_turb, gamma, tau_turb, def_tens, kin_visc, kin_visc_turb_min)
  for (J=1; J<(Ny-1); J++){
    for (I=1; I<(Nx-1); I++){
      
      index_ = J*Nx + I;
      
      if(dom_matrix[index_] == 0){
	
	/* Calculating turbulent viscosity */
	pre_prod_for_l_ast = C_mu * k_turb[index_] * k_turb[index_] /
	  (sqrt(k_turb[index_]));
	
	if (k_turb[index_] == 0){
	  l_ast = 0;
	}
	else if ((k_turb[index_] != 0) && (eps_turb[index_] == 0)){
	  l_ast = max_mix_l;
	}
	else if (pre_prod_for_l_ast < eps_turb[index_] * max_mix_l){
	  l_ast = pre_prod_for_l_ast / eps_turb[index_];
	}
	else {
	  l_ast = max_mix_l;
	}
      
	if ((k_turb[index_] == 0) || (eps_turb[index_] == 0)){
	  re_turb = 0.0;
	}
	else {
	  re_turb = k_turb[index_] * k_turb[index_] / (eps_turb[index_] * kin_visc);
	}

	if(turb_model == 1){
	  /* Abe, Kondoh & Nagano (1994) (pp 80 de Lemos) */
	  y_ast = pow(kin_visc*eps_turb[index_], 0.25) * y_wall[index_] / kin_visc;
	  
	  f_1[index_]  = 1.0;
	  f_2[index_]  = pow(1 - exp((-1) * y_ast / 3.1), 2.0) * 
	    (1 - 0.3*exp((-1)*pow(re_turb / 6.5, 2.0)));
	  f_mu[index_] = pow(1 - exp((-1) * y_ast / 14), 2.0) * 
	    (1 + (5/(pow(re_turb, 0.75))) * exp((-1)*pow(re_turb / 200.0, 2.0)));
	}
	else if(turb_model == 2){
	  /* Cho & Goldstein (1994) */
	  f_1[index_]  = 1.0;
	  f_2[index_]  = 1 - 0.222 * exp( (-1) * re_turb * re_turb / 36.0);
	  f_mu[index_] = 1 - 0.95 * exp( (-5.0e-5) * re_turb * re_turb);
	}
	
	/* ARBITRARY */
	if(re_turb == 0){
	  f_2[index_] = 0;
	  f_mu[index_] = 0;
	}
      
	/* END Section DAMPING FUNCTIONS */
	kin_visc_turb = l_ast * sqrt(k_turb[index_]);
      
	/* Selection of maxima value */
	mu_turb[index_] = rho * MAX(kin_visc_turb_min, f_mu[index_]*kin_visc_turb);
      
	gamma[index_] = C_mu * k_turb[index_] * rho * f_mu[index_] / mu_turb[index_];
      
	tau_turb[index_ + 0*offset] = 2 * mu_turb[index_] * def_tens[index_ + 0*offset] - (2.0/3.0)*rho*k_turb[index_];
	tau_turb[index_ + 1*offset] = 2 * mu_turb[index_] * def_tens[index_ + 1*offset];
	tau_turb[index_ + 2*offset] = 2 * mu_turb[index_] * def_tens[index_ + 2*offset];
	tau_turb[index_ + 3*offset] = 2 * mu_turb[index_] * def_tens[index_ + 3*offset] - (2.0/3.0)*rho*k_turb[index_];
      }
    }
  }

  return;
}

/* COMPUTE_DEF_TENS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, 9NxNyNz) Data_Mem::def_tens: Deformation tensor.
  (0...NxNy-1): du/dx, (NxNy...2NxNy-1): 0.5*(du/dy+dv/dx) 
  (2NxNy...3NxNy-1): 0.5*(du/dy+dv/dx), (3NxNy...4*Nx*Ny-1): dv/dy.

  @param data

  @return void
  
  MODIFIED: 18-03-2020
*/
void compute_def_tens(Data_Mem *data){
  
  int I, J;
  int index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int offset = Nx*Ny;

  const int *dom_matrix = data->domain_matrix;

  double *def_tens = data->def_tens;

  const double *nabla_u = data->nabla_u;
  
#pragma omp parallel for default(none) private(I, J, index_)	\
  shared(dom_matrix, def_tens, nabla_u)
  for (J=1; J<(Ny-1); J++){
    for (I=1; I<(Nx-1); I++){
      
      index_ = J*Nx + I;
      
      if(dom_matrix[index_] == 0){
	/* CALCULATING THE MACROSCOPIC DEFORMATION TENSOR "D" */
	def_tens[index_ + 0*offset] = (nabla_u[index_ + 0*offset] + nabla_u[index_ + 0*offset])*0.5;
	def_tens[index_ + 1*offset] = (nabla_u[index_ + 1*offset] + nabla_u[index_ + 2*offset])*0.5;
	def_tens[index_ + 2*offset] = (nabla_u[index_ + 2*offset] + nabla_u[index_ + 1*offset])*0.5;
	def_tens[index_ + 3*offset] = (nabla_u[index_ + 3*offset] + nabla_u[index_ + 3*offset])*0.5;
      }
    }
  }

  return;
}

/* COMPUTE_NABLA_U */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, 9NxNyNz) nabla_u: (0...NxNy-1): du/dx, (NxNy...2NxNy-1): du/dy
  (2NxNy...3NxNy-1): dv/dx, (3NxNy...4*Nx*Ny-1): dv/dy.

  @param data

  @return void
  
  MODIFIED: 18-03-2020
*/
void compute_nabla_u(Data_Mem *data){

  int i, j, I, J, z;
  int index_, index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int turb_boundary_cell_count = data->turb_boundary_cell_count;
  
  const int offset = Nx*Ny;
  const int *dom_matrix = data->domain_matrix;
  const int *turb_boundary_indexs = data->turb_boundary_indexs;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *turb_boundary_I = data->turb_boundary_I;
  const int *turb_boundary_J = data->turb_boundary_J;

  double *nabla_u = data->nabla_u;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  
  const double *u_at_faces_y = data->u_at_faces_y;
  const double *v_at_faces_x = data->v_at_faces_x;

  const double *Delta_y = data->Delta_y;
  const double *Delta_x = data->Delta_x;
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;

#pragma omp parallel for default(none) private(I, J, i, j, index_, index_u, index_v) \
  shared(dom_matrix, nabla_u, u, Delta_x, u_at_faces_y, v_at_faces_x, Delta_y, v)
  for (J=1; J<(Ny-1); J++){
    for (I=1; I<(Nx-1); I++){

      i = I - 1;
      j = J - 1;
      
      index_ = J*Nx + I;

      if(dom_matrix[index_] == 0){

	index_u = J*nx + i;
	index_v = j*Nx + I;
      
	/* 0 = 11 = du/dx */
	nabla_u[index_+0*offset] = (u[index_u+1]-u[index_u])/Delta_x[index_];
	/* 1 = 12 = du/dy */
	nabla_u[index_+1*offset] = (u_at_faces_y[index_v+Nx]-u_at_faces_y[index_v])/Delta_y[index_];
	/* 2 = 21 = dv/dx */
	nabla_u[index_+ 2*offset] = (v_at_faces_x[index_u+1]-v_at_faces_x[index_u])/Delta_x[index_];
	/* 3 = 22 = dv/dy */
	nabla_u[index_+3*offset] = (v[index_v+Nx]-v[index_v])/Delta_y[index_];
      }
    }
  }

  /* Correction for nodes located in the vicinity of the solid */
  
#pragma omp parallel for default(none) private(z, index_, I, J, index_u, index_v) \
  shared(turb_boundary_indexs, turb_boundary_I, turb_boundary_J, W_is_fluid_OK, E_is_fluid_OK, S_is_fluid_OK, N_is_fluid_OK, \
	 eps_x, eps_y, nabla_u, u, v, u_at_faces_y, v_at_faces_x, Delta_x, Delta_y)

  for(z=0; z<turb_boundary_cell_count; z++){
    index_ = turb_boundary_indexs[z];
    I = turb_boundary_I[z];
    J = turb_boundary_J[z];

    /* Derivatives in x direction */
    if((!W_is_fluid_OK[index_] || !E_is_fluid_OK[index_]) && (eps_x[index_] <= 0.5)){
      index_u = J * nx + I - 1;
      if(W_is_fluid_OK[index_]){
	nabla_u[index_] = -u[index_u] / (Delta_x[index_] * (0.5 + eps_x[index_]));
	nabla_u[index_ + 2*offset] = -v_at_faces_x[index_u] / (Delta_x[index_] * (0.5 + eps_x[index_]));
      }
      else{
	nabla_u[index_] = u[index_u+1] / (Delta_x[index_] * (0.5 + eps_x[index_]));
	nabla_u[index_ + 2*offset] = v_at_faces_x[index_u+1] / (Delta_x[index_] * (0.5 + eps_x[index_]));
      }
    }

    /* Derivatives in y direction */
    if((!S_is_fluid_OK[index_] || !N_is_fluid_OK[index_]) && (eps_y[index_] <= 0.5)){
      index_v = (J - 1) * Nx + I;
      if(S_is_fluid_OK[index_]){
	nabla_u[index_ + offset] = -u_at_faces_y[index_v] / (Delta_y[index_] * (0.5 + eps_y[index_]));
	nabla_u[index_ + 3*offset] = -v[index_v] / (Delta_y[index_] * (0.5 + eps_y[index_]));
      }
      else{
	nabla_u[index_ + offset] = u_at_faces_y[index_v+Nx] / (Delta_y[index_] * (0.5 + eps_y[index_]));
	nabla_u[index_ + 3*offset] = v[index_v+Nx] / (Delta_y[index_] * (0.5 + eps_y[index_]));
      }
    }
  } // for(z=0; z<turb_boundary_cell_count; z++) 

  return;
}

/* VEL_AT_CROSSED_FACES */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxNyNz) Data_Mem::u_at_faces: u at center of face perpendicular to x axis, on p cell.

  Array (double, NxnyNz) Data_Mem::u_at_faces_y: u at center of face perpendicular to y axis, on p cell.

  Array (double, nxNyNz) Data_Mem::v_at_faces_x: v at center of face perpendicular to x axis, on p cell.

  Array (double, NxnyNz) Data_Mem::v_at_faces: v at center of face perpendicular to y axis, on p cell.

  @param data

  @return void
  
  MODIFIED: 18-03-2020
*/
void vel_at_crossed_faces(Data_Mem *data){

  int i, j, I, J;
  int index_, index_u, index_v;
  int v_int_0_on_fluid, v_int_1_on_fluid;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;

  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;

  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;

  double v_int_0, v_int_1;

  double *u_at_faces = data->u_at_faces;
  double *v_at_faces = data->v_at_faces;

  double *u_at_faces_y = data->u_at_faces_y;
  double *v_at_faces_x = data->v_at_faces_x;

  const double *u = data->u;
  const double *v = data->v;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *p_faces_x = data->p_faces_x;
  const double *p_faces_y = data->p_faces_y;
  const double *fw_v = data->fw_v;
  const double *fs_u = data->fs_u;

  /* Putting the velocities at the center of their respective faces */
  copy_array(u, u_at_faces, nx, Ny, Nz);
  copy_array(v, v_at_faces, Nx, ny, Nz);

  /* u velocity component */
#pragma omp parallel for default(none) private(I, J, i, index_, index_u) \
  shared(p_cut_face_x, p_faces_x, Delta_y, u_at_faces, u)
  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      I = i+1;

      index_ = J*Nx + I;
      index_u = J*nx + i;

      if(p_cut_face_x[index_u] != 0){

	if(p_faces_x[index_u] <= 0.5 * Delta_y[index_]){
	  u_at_faces[index_u] = 0;
	}
	else{
	  u_at_faces[index_u] = u[index_u] * (2 - Delta_y[index_] / p_faces_x[index_u]);
	}
      }
    }
  }

  /* v velocity component */
#pragma omp parallel for default(none) private(I, j, J, index_, index_v) \
  shared(p_cut_face_y, p_faces_y, Delta_x, v_at_faces, v)
  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){

      J = j+1;
	
      index_ = J*Nx + I;
      index_v = j*Nx + I;

      if(p_cut_face_y[index_v] != 0){

	if(p_faces_y[index_v] <= 0.5 * Delta_x[index_]){
	  v_at_faces[index_v] = 0;	    
	}
	else{
	  v_at_faces[index_v] = v[index_v] * (2 - Delta_x[index_] / p_faces_y[index_v]);
	}
      }
    }
  }

  /* Now we have all velocities centered at their faces */
  /* Must compute every property with these values */

  /* u velocities at y faces */
#pragma omp parallel for default(none)					\
  private(i, j, I, J, index_, index_u, index_v,				\
	  v_int_0_on_fluid, v_int_1_on_fluid, v_int_0, v_int_1)		\
  shared(u_dom_matrix, u, fs_u, p_cut_face_y, p_faces_y, Delta_x,	\
	 u_at_faces_y, v_dom_matrix)
  for(j=1; j<ny-1; j++){
    for(I=1; I<Nx-1; I++){
	
      J = j + 1;
      i = I - 1;
      
      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;
      
      v_int_0_on_fluid = 0;
      v_int_1_on_fluid = 0;
      
      v_int_0 = 0;
      v_int_1 = 0;

      /* If one of the required neighbour points is outside the fluid domain, 
	 the intermediate point will also be outside the fluid domain */
      /* South analysis */
      /* Get interpolation point 0: E & ES */
      if((u_dom_matrix[index_u+1] == 0) && (u_dom_matrix[index_u+1-nx] == 0)){
	v_int_0 = u[index_u+1] * fs_u[index_u+1] + u[index_u+1-nx] * (1 - fs_u[index_u+1]);
	v_int_0_on_fluid = 1;
      }
      /* Get interpolation point 1: P & PS */
      if((u_dom_matrix[index_u] == 0) && (u_dom_matrix[index_u-nx] == 0)){
	v_int_1 = u[index_u] * fs_u[index_u] + u[index_u-nx] * (1 - fs_u[index_u]);
	v_int_1_on_fluid = 1;
      }
      /* Checking cut faces */
      if(p_cut_face_y[index_v] != 0){
	/* We have a s cut face (relative to P) */
	if(p_faces_y[index_v] <= 0.5 * Delta_x[index_]){
	  /* The s cut face is of length less than half its uncut face.
	     This means that the midpoint is under the solid curve */
	  u_at_faces_y[index_v] = 0;
	}
	/* Now only options that have the midpoint on fluid remain */
	else if(v_int_0_on_fluid && v_int_1_on_fluid){
	  /* The s cut face has both uP on fluid phase */
	  u_at_faces_y[index_v] = 0.5 * (v_int_0 + v_int_1);
	}
	else if(v_int_0_on_fluid){
	  /* The s cut face has only point (E & ES) on fluid phase */
	  u_at_faces_y[index_v] = v_int_0 * (1 - 0.5 * Delta_x[index_] / p_faces_y[index_v]);
	}
	else if(v_int_1_on_fluid){
	  /* The s cut face has only point (P & PS) on fluid phase */
	  u_at_faces_y[index_v] = v_int_1 * (1 - 0.5 * Delta_x[index_] / p_faces_y[index_v]);
	}
	else{
	  /* The algorithm is not supposed to arrive at this point, included as "default" case*/
	  u_at_faces_y[index_v] = 0;
	}
      }
      else{
	/* Face is not cut */
	u_at_faces_y[index_v] = 0.5 * (v_int_0 + v_int_1);
      }
    } // for(J=1; J<Ny-1; J++)
  } // for(i=1; i<nx-1; i++)

#pragma omp parallel sections default(none)				\
  private(I, J, i, j, index_, index_u, index_v,				\
	  v_int_0_on_fluid, v_int_1_on_fluid, v_int_0, v_int_1)		\
  shared(u_dom_matrix, u, p_cut_face_y, p_faces_y, Delta_x, u_at_faces_y)
  {

    /* South boundary */
#pragma omp section 
    {
      J = 0;
      j = 0;
  
      for(I=1; I<Nx-1; I++){
    
	i = I - 1;
	  
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	  
	v_int_0_on_fluid = 0;
	v_int_1_on_fluid = 0;
	  
	v_int_0 = 0;
	v_int_1 = 0;

	if(u_dom_matrix[index_u+1] == 0){
	  v_int_0 = u[index_u+1];
	  v_int_0_on_fluid = 1;
	}

	if(u_dom_matrix[index_u] == 0){
	  v_int_1 = u[index_u];
	  v_int_1_on_fluid = 1;
	}

	if(p_cut_face_y[index_v] != 0){

	  if(p_faces_y[index_v] <= 0.5 * Delta_x[index_]){
	    u_at_faces_y[index_v] = 0;
	  }
	  else if((v_int_0_on_fluid) && (v_int_1_on_fluid)){
	    u_at_faces_y[index_v] = 0.5 * (v_int_0 + v_int_1);
	  }
	  else if((v_int_0_on_fluid)){
	    u_at_faces_y[index_v] = v_int_0 * (1 - 0.5 * Delta_x[index_] / p_faces_y[index_v]);
	  }
	  else if(v_int_1_on_fluid){
	    u_at_faces_y[index_v] = v_int_1 * (1 - 0.5 * Delta_x[index_] / p_faces_y[index_v]);
	  }
	  else{
	    u_at_faces_y[index_v] = 0;
	  }
	}
	else{
	  u_at_faces_y[index_v] = 0.5 * (v_int_0 + v_int_1);
	}    
   
      } // end for(I=1; I<Nx-1; I++){
    } /* South section */

      /* North boundary */

#pragma omp section 
    {
      J = Ny - 1;
      j = ny - 1;
  
      for(I=1; I<Nx-1; I++){
    
	i = I-1;
	  
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
	  
	v_int_0_on_fluid = 0;
	v_int_1_on_fluid = 0;
	  
	v_int_0 = 0;
	v_int_1 = 0;

	if(u_dom_matrix[index_u+1] == 0){
	  v_int_0 = u[index_u+1];
	  v_int_0_on_fluid = 1;
	}

	if(u_dom_matrix[index_u] == 0){
	  v_int_1 = u[index_u];
	  v_int_1_on_fluid = 1;
	}

	if(p_cut_face_y[index_v] != 0){

	  if(p_faces_y[index_v] <= 0.5 * Delta_x[index_]){
	    u_at_faces_y[index_v] = 0;
	  }
	  else if((v_int_0_on_fluid) && (v_int_1_on_fluid)){
	    u_at_faces_y[index_v] = 0.5 * (v_int_0 + v_int_1);
	  }
	  else if(v_int_0_on_fluid){
	    u_at_faces_y[index_v] = v_int_0 * (1 - 0.5 * Delta_x[index_] / p_faces_y[index_v]);
	  }
	  else if(v_int_1_on_fluid){
	    u_at_faces_y[index_v] = v_int_1 * (1 - 0.5 * Delta_x[index_] / p_faces_y[index_v]);
	  }
	  else{
	    u_at_faces_y[index_v] = 0;
	  }
	}
	else{
	  u_at_faces_y[index_v] = 0.5 * (v_int_0 + v_int_1);
	}            
      } // end for(I=1; I<Nx-1; I++){
    } /* North section */

      /* West boundary */
#pragma omp section 
    {
      I = 0;
      i = 0;

      for(j=1; j<ny-1; j++){

	J = j + 1;
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;

	if((u_dom_matrix[index_u] == 0) && (u_dom_matrix[index_u-nx] == 0)){
	  u_at_faces_y[index_v] = 0.5 * (u[index_u] + u[index_u-nx]);
	}
	else{
	  u_at_faces_y[index_v] = 0;
	}
      }
    } /* West section */

      /* East boundary */
#pragma omp section 
    {
      I = Nx - 1;
      i = nx - 1;

      for(j=1; j<ny-1; j++){

	J = j + 1;
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;

	if((u_dom_matrix[index_u] == 0) && (u_dom_matrix[index_u-nx] == 0)){
	  u_at_faces_y[index_v] = 0.5 * (u[index_u] + u[index_u-nx]);
	}
	else{
	  u_at_faces_y[index_v] = 0;
	}
      }
    } /* West section */

  } /* sections */ 

  /* v velocities at x faces */
#pragma omp parallel for default(none) private(i, j, I, J, index_, index_u, index_v, v_int_0_on_fluid, v_int_1_on_fluid, \
					       v_int_0, v_int_1)	\
  shared(v_dom_matrix, v, fw_v, p_cut_face_x, p_faces_x, Delta_y, v_at_faces_x)
  for(J=1; J<Ny-1; J++){
    for(i=1; i<nx-1; i++){

      I = i+1;
      j = J - 1;
      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;

      v_int_0_on_fluid = 0;
      v_int_1_on_fluid = 0;
      v_int_0 = 0;
      v_int_1 = 0;

      if((v_dom_matrix[index_v+Nx] == 0) && (v_dom_matrix[index_v-1+Nx] == 0)){
	v_int_0 = v[index_v+Nx] * fw_v[index_v+Nx] + v[index_v-1+Nx] * (1 - fw_v[index_v+Nx]);
	v_int_0_on_fluid = 1;
      }
      if((v_dom_matrix[index_v] == 0) && (v_dom_matrix[index_v-1] == 0)){
	v_int_1 = v[index_v] * fw_v[index_v] + v[index_v-1] * (1 - fw_v[index_v]);
	v_int_1_on_fluid = 1;
      }

      if(p_cut_face_x[index_u] != 0){

	if(p_faces_x[index_u] <= 0.5 * Delta_y[index_]){
	  v_at_faces_x[index_u] = 0;
	}
	else if((v_int_0_on_fluid) && (v_int_1_on_fluid)){
	  v_at_faces_x[index_u] = 0.5 * (v_int_0 + v_int_1);
	}
	else if(v_int_0_on_fluid){
	  v_at_faces_x[index_u] = v_int_0 * (1 - 0.5 * Delta_y[index_] / p_faces_x[index_u]);
	}
	else if(v_int_1_on_fluid){
	  v_at_faces_x[index_u] = v_int_1 * (1 - 0.5 * Delta_y[index_] / p_faces_x[index_u]);
	}
	else{
	  v_at_faces_x[index_u] = 0;
	}
      }
      else{
	v_at_faces_x[index_u] = 0.5 * (v_int_0 + v_int_1);
      }
    } // for(J=1; J<Ny-1; J++)
  } // for(i=1; i<nx-1; i++)

#pragma omp parallel sections default(none)				\
  private(I, J, i, j, index_, index_u, index_v,				\
	  v_int_0_on_fluid, v_int_1_on_fluid, v_int_0, v_int_1)		\
  shared(v_dom_matrix, v, p_cut_face_x, Delta_y, p_faces_x, v_at_faces_x)
  {

    /* West boundary */
#pragma omp section
    {

      I = 0;
      i = 0;

      for(J=1; J<Ny-1; J++){
 
	j = J - 1;
    
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
    
	v_int_0_on_fluid = 0;
	v_int_1_on_fluid = 0;
	v_int_0 = 0;
	v_int_1 = 0;

	if(v_dom_matrix[index_v+Nx] == 0){
	  v_int_0 = v[index_v+Nx];
	  v_int_0_on_fluid = 1;
	}
	if(v_dom_matrix[index_v] == 0){
	  v_int_1 = v[index_v];
	  v_int_1_on_fluid = 1;
	}

	if(p_cut_face_x[index_u] != 0){

	  if(p_faces_x[index_u] <= 0.5 * Delta_y[index_]){
	    v_at_faces_x[index_u] = 0;
	  }
	  else if((v_int_0_on_fluid) && (v_int_1_on_fluid)){
	    v_at_faces_x[index_u] = 0.5 * (v_int_0 + v_int_1);
	  }
	  else if(v_int_0_on_fluid){
	    v_at_faces_x[index_u] = v_int_0 * (1 - 0.5 * Delta_y[index_] / p_faces_x[index_u]);
	  }
	  else if(v_int_1_on_fluid){
	    v_at_faces_x[index_u] = v_int_1 * (1 - 0.5 * Delta_y[index_] / p_faces_x[index_u]);
	  }
	  else{
	    v_at_faces_x[index_u] = 0;
	  }
	}
	else{
	  v_at_faces_x[index_u] = 0.5 * (v_int_0 + v_int_1);
	}
    
      } // end for(J=1; J<Ny-1; J++){

    }  /* West section */
  
    /* East boundary */
#pragma omp section 
    {

      I = Nx - 1;
      i = nx - 1;

      for(J=1; J<Ny-1; J++){
 
	j = J - 1;
    
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
    
	v_int_0_on_fluid = 0;
	v_int_1_on_fluid = 0;
	v_int_0 = 0;
	v_int_1 = 0;

	if(v_dom_matrix[index_v+Nx] == 0){
	  v_int_0 = v[index_v+Nx];
	  v_int_0_on_fluid = 1;
	}
	if(v_dom_matrix[index_v] == 0){
	  v_int_1 = v[index_v];
	  v_int_1_on_fluid = 1;
	}

	if(p_cut_face_x[index_u] != 0){

	  if(p_faces_x[index_u] <= 0.5 * Delta_y[index_]){
	    v_at_faces_x[index_u] = 0;
	  }
	  else if((v_int_0_on_fluid) && (v_int_1_on_fluid)){
	    v_at_faces_x[index_u] = 0.5 * (v_int_0 + v_int_1);
	  }
	  else if(v_int_0_on_fluid){
	    v_at_faces_x[index_u] = v_int_0 * (1 - 0.5 * Delta_y[index_] / p_faces_x[index_u]);
	  }
	  else if(v_int_1_on_fluid){
	    v_at_faces_x[index_u] = v_int_1 * (1 - 0.5 * Delta_y[index_] / p_faces_x[index_u]);
	  }
	  else{
	    v_at_faces_x[index_u] = 0;
	  }
	}
	else{
	  v_at_faces_x[index_u] = 0.5 * (v_int_0 + v_int_1);
	}
    
      } // end for(J=1; J<Ny-1; J++){
    } /* East section */

    /* South boundary */
#pragma omp section 
    {

      J = 0;
      j = 0;

      for(I=2; I<Nx-1; I++){
    
	i = I - 1;
    
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;

	if((v_dom_matrix[index_v] == 0) && (v_dom_matrix[index_v-1] == 0)){
	  v_at_faces_x[index_u] = 0.5 * (v[index_v]  + v[index_v-1]);
	}
	else{
	  v_at_faces_x[index_u] = 0;
	}
      } // end for(I=2; I<Nx-1; I++){
    } /* South section */

    /* North boundary */
#pragma omp section 
    {
      J = Ny - 1;
      j = ny - 1;

      for(I=2; I<Nx-1; I++){
    
	i = I-1;
    
	index_ = J*Nx + I;
	index_u = J*nx + i;
	index_v = j*Nx + I;
    
	if((v_dom_matrix[index_v] == 0) && (v_dom_matrix[index_v-1] == 0)){
	  v_at_faces_x[index_u] = 0.5 * (v[index_v]  + v[index_v-1]);
	}
	else{
	  v_at_faces_x[index_u] = 0;
	}
      } // end for(I=2; I<Nx-1; I++){
    } /* North section */
  } /* sections */
 
  return;
}

/* COMPUTE_S_CG_EPS */
/*****************************************************************************/
/**

  Sets:

  Array (double, NxNy) S_CG_eps: Source term for CG turbulence model.

  @param data

  @return void
  
  MODIFIED: 18-03-2020
*/
void compute_S_CG_eps(Data_Mem *data){

  int I, J;
  int index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  
  double skn, sks, ske, skw;
  double dskdx, dskdy, dsk2;
  double fi;
  double lClyw;

  double S11, S12;
  double S1, S2;

  double skP, skE, skW, skN, skS;

  const double rho = data->rho_v[0];
  const double mu = data->mu;

  double *S_CG_eps = data->S_CG_eps;

  const double *d2Uidxjdxk2 = data->d2Uidxjdxk2;

  const double *k = data->k_turb;
  //  const double *eps = data->eps_turb;
  const double *gamma = data->gamma;
  const double *mu_t = data->mu_turb;
  
  const double *f_mu = data->f_mu;
  const double *y_wall = data->y_wall;
  
  const double *fe = data->fe;
  const double *fw = data->fw;
  const double *fn = data->fn;
  const double *fs = data->fs;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

  // set_k_profile_for_debug(data);

#pragma omp parallel for default(none)					\
  private(I, J, index_, fi, skn, sks, ske, skw,				\
	  dskdx, dskdy, dsk2, lClyw, S11, S12, S1, S2,			\
	  skP, skE, skW, skN, skS)					\
  shared(k, fn, fs, fw, fe, Delta_x, Delta_y, gamma, y_wall, mu_t,	\
	 d2Uidxjdxk2, f_mu, S_CG_eps)
  for (J=1; J<(Ny-1); J++){
    for (I=1; I<(Nx-1); I++){
      
      index_ = J*Nx + I;

      skP = sqrt(k[index_]);

      skE = sqrt(k[index_+1]);
      skW = sqrt(k[index_-1]);
      skN = sqrt(k[index_+Nx]);
      skS = sqrt(k[index_-Nx]);
      
      fi = fn[index_];
      skn = fi * skP + (1-fi) * skN;

      fi = fs[index_];
      sks = fi * skP + (1-fi) * skS;

      fi = fe[index_];
      ske = fi * skP + (1-fi) * skE;

      fi = fw[index_];
      skw = fi * skP + (1-fi) * skW;

      dskdx = (ske - skw)/Delta_x[index_];
      dskdy = (skn - sks)/Delta_y[index_];

      dsk2 = dskdx*dskdx + dskdy*dskdy;

      lClyw = (sqrt(k[index_])/gamma[index_]) / (2.44*y_wall[index_]);
      
      S11 = (2*mu*mu_t[index_]/rho) * d2Uidxjdxk2[index_];
      S12 =  2*mu * dsk2 * gamma[index_];
      
      S1 = 1.44 * (1 - f_mu[index_]) * ( S11 + S12 );
      
      S2 = MAX( 0.83 * k[index_] * gamma[index_]*gamma[index_] * (lClyw-1) * lClyw*lClyw , 0 );
      
      S_CG_eps[index_] = S1 + S2;
     
    }
  }
  
  return;
}

/* COMPUTE_D2UIDXJDXK2 */
/*****************************************************************************/
/**

  Sets:

  Array (double, NxNy) d2Uidxjdxk2: Used for calculation of source term on CG turbulence model.

  @param data

  @return ret_value not implemented.
  
  MODIFIED: 18-03-2020
*/
void compute_d2Uidxjdxk2(Data_Mem *data){

  int i, j, I, J;
  int index_, index_c;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;

  double d2Udx2, d2Udy2, d2Vdx2, d2Vdy2;
  double d2Udxdy, d2Vdxdy;

  double dx, dy;
  double dxE, dxW, dyN, dyS;
  
  double UP, UE, UW, UN, US;
  double VP, VE, VW, VN, VS;

  double d2Uidxjdxk;
  double *d2Uidxjdxk2 = data->d2Uidxjdxk2;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;

  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;
  
  const double *U = data->U;
  const double *V = data->V;

  const double *u_at_corners = data->u_at_corners;
  const double *v_at_corners = data->v_at_corners;

#pragma omp parallel for default(none)					\
  private(I, J, i, j, index_, index_c,					\
	  d2Uidxjdxk,							\
	  d2Udx2, d2Vdx2, d2Udy2, d2Vdy2, d2Udxdy, d2Vdxdy,		\
	  dx, dy, dxE, dxW, dyN, dyS,					\
	  UP, UE, UW, UN, US,						\
	  VP, VE, VW, VN, VS)						\
  shared(d2Uidxjdxk2,							\
	 Delta_x, Delta_y, Delta_xE, Delta_xW, Delta_yN, Delta_yS,	\
	 U, V, u_at_corners, v_at_corners)
  for (J=1; J<Ny-1; J++){
    for (I=1; I<Nx-1; I++){
      
      i = I-1;
      j = J-1;
      
      index_ = J*Nx + I;
      index_c = j*nx+i;
      
      dx = Delta_x[index_];
      dy = Delta_y[index_];

      dxE = Delta_xE[index_];
      dxW = Delta_xW[index_];
      
      dyN = Delta_yN[index_];
      dyS = Delta_yS[index_];

      UP = U[index_];
      UE = U[index_+1];
      UW = U[index_-1];
      UN = U[index_+Nx];
      US = U[index_-Nx];

      VP = V[index_];
      VE = V[index_+1];
      VW = V[index_-1];
      VN = V[index_+Nx];
      VS = V[index_-Nx];
      
      d2Udx2 = ((UE-UP)/dxE - (UP-UW)/dxW) / dx;
      d2Vdy2 = ((VN-VP)/dyN - (VP-VS)/dyS) / dy;

      d2Udy2 = ((UN-UP)/dyN - (UP-US)/dyS) / dy;
      d2Vdx2 = ((VE-VP)/dxE - (VP-VW)/dxW) / dx;
      
      d2Udxdy = (u_at_corners[index_c] - u_at_corners[index_c + 1] -
		 u_at_corners[index_c + nx] + u_at_corners[index_c + nx + 1]) / (dx*dy);
      
      d2Vdxdy = (v_at_corners[index_c] - v_at_corners[index_c + 1] -
		 v_at_corners[index_c + nx] + v_at_corners[index_c + nx + 1]) / (dx*dy);

      d2Uidxjdxk = d2Udx2 + d2Vdx2 + d2Udy2 + d2Vdy2 +	2*d2Udxdy + 2*d2Vdxdy;

      d2Uidxjdxk2[index_] = d2Uidxjdxk * d2Uidxjdxk;
      
    }
  }
  
  return;
}

/* COMPUTE_VEL_AT_CENTERS_FULL_FROM_FACES */
void compute_vel_at_centers_full_from_faces(Data_Mem *data){

  int i, j, I, J;
  int index_, index_u, index_v;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  double ue, uw, vn, vs;
  
  double *U = data->U;
  double *V = data->V;

  /* Here we consider velocity centered at their respective faces */
  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;

#pragma omp parallel for default(none)				\
  private(I, J, i, j, index_, index_u, index_v, ue, uw, vn, vs) \
  shared(u, v, U, V)
  for (J=1; J<Ny-1; J++){
    for (I=1; I<Nx-1; I++){
      
      i = I-1;
      j = J-1;
      
      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;
      
      ue = u[index_u + 1];
      uw = u[index_u];
      
      vn = v[index_v + Nx];
      vs = v[index_v];
      
      U[index_] = 0.5 * (ue + uw);
      V[index_] = 0.5 * (vn + vs);
      
    }
  }

  /* Borders */
  /* S */
  J = 0;
  j = 0;
  
#pragma omp parallel for default(none)			\
  private(I, i, index_, index_u, index_v, ue, uw)	\
  shared(J, j, u, v, U, V)
  for (I=1; I<Nx-1; I++){
    
    i = I-1;
    
    index_ = J*Nx + I;
    index_u = J*nx + i;
    index_v = j*Nx + I;
    
    ue = u[index_u + 1];
    uw = u[index_u];

    U[index_] = (ue + uw) * 0.5;
    V[index_] = v[index_v];

  }
  
  /* N */
  J = Ny-1;
  j = ny-1;

#pragma omp parallel for default(none)			\
  private(I, i, index_, index_u, index_v, ue, uw)	\
  shared(J, j, u, v, U, V)
  for (I=1; I<Nx-1; I++){
    
    i = I-1;
    
    index_ = J*Nx + I;
    index_u = J*nx + i;
    index_v = j*Nx + I;
    
    ue = u[index_u + 1];
    uw = u[index_u];
    
    U[index_] = (ue + uw) * 0.5;
    V[index_] = v[index_v];
  }
    
  /* W */
  I = 0;
  i = 0;

#pragma omp parallel for default(none)			\
  private(J, j, index_, index_u, index_v, vn, vs)	\
  shared(I, i, u, v, U, V)
  for (J=1; J<Ny-1; J++){
      
    j = J-1;
      
    index_ = J*Nx + I;
    index_u = J*nx + i;
    index_v = j*Nx + I;
    
    vn = v[index_v + Nx];
    vs = v[index_v];
      
    U[index_] = u[index_u];
    V[index_] = (vn + vs) * 0.5;
  }
  
  /* E */
  I = Nx-1;
  i = nx-1;

#pragma omp parallel for default(none)			\
  private(J, j, index_, index_u, index_v, vn, vs)	\
  shared(I, i, u, v, U, V)
  for (J=1; J<Ny-1; J++){
      
    j = J-1;
      
    index_ = J*Nx + I;
    index_u = J*nx + i;
    index_v = j*Nx + I;
      
    vn = v[index_v + Nx];
    vs = v[index_v];
      
    U[index_] = u[index_u];
    V[index_] = (vn + vs) * 0.5;
  }
  
  return;
}

/* VEL_AT_CORNERS */
/*****************************************************************************/
/**
  
  Sets:

  Array (double, nxny) u_at_corners: values of u at corners of P grid.
  Array (double, nxny) v_at_corners: values of v at corners of P grid.

  @param data

  @return void
  
  MODIFIED: 05-02-2019
*/
void vel_at_corners(Data_Mem *data){
  
  int i, j, I, J;
  int index_, index_c, index_u, index_v;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;
  
  double fi;

  double *u_at_corners = data->u_at_corners;
  double *v_at_corners = data->v_at_corners;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;

  const double *fw = data->fw;
  const double *fs = data->fs;
  
  /* Starting from I == J == 2 we do not touch any borders EWNS */
  for(J=2; J<Ny-1; J++){
    for(I=2; I<Nx-1; I++){

      i = I-1;
      j = J-1;

      index_ = J*Nx+I;
      index_c = j*nx+i; // -- coordinates
      index_u = J*nx+i;
      index_v = j*Nx+I;

      /* Crossed interpolations */
      fi = fs[index_];
      u_at_corners[index_c] = fi * u[index_u] + (1-fi) * u[index_u-nx];

      fi = fw[index_];
      v_at_corners[index_c] = fi * v[index_v] + (1-fi) * v[index_v-1];
      
    }
  }

  /* Corners */
  I = 0; i = 0;
  J = 0; j = 0;
  
  index_c = j*nx+i;
  index_u = J*nx+i;
  index_v = j*Nx+I;
  
  u_at_corners[index_c] = u[index_u];
  v_at_corners[index_c] = v[index_v];

  I = Nx-1; i = nx-1;
  J = 0; j = 0;
  
  index_c = j*nx+i;
  index_u = J*nx+i;
  index_v = j*Nx+I;
  
  u_at_corners[index_c] = u[index_u];
  v_at_corners[index_c] = v[index_v];

  I = Nx-1; i = nx-1;
  J = Ny-1; j = ny-1;
  
  index_c = j*nx+i;
  index_u = J*nx+i;
  index_v = j*Nx+I;
  
  u_at_corners[index_c] = u[index_u];
  v_at_corners[index_c] = v[index_v];

  I = 0; i = 0;
  J = Ny-1; j = ny-1;
  
  index_c = j*nx+i;
  index_u = J*nx+i;
  index_v = j*Nx+I;
  
  u_at_corners[index_c] = u[index_u];
  v_at_corners[index_c] = v[index_v];

  /* S */
  J = 0;
  j = 0;
  for(I=2; I<Nx-1; I++){

    i = I-1;
    
    index_ = J*Nx+I;
    index_c = j*nx+i; // -- coordinates
    index_u = J*nx+i;
    index_v = j*Nx+I;

    u_at_corners[index_c] = u[index_u];
    
    fi = fw[index_];
    v_at_corners[index_c] = fi * v[index_v] + (1-fi) * v[index_v-1];
    
  }

  /* E */
  I = Nx-1;
  i = nx-1;

  for(J=2; J<Ny-1; J++){

    j = J-1;

    index_ = J*Nx+I;
    index_c = j*nx+i; // -- coordinates
    index_u = J*nx+i;
    index_v = j*Nx+I;

    fi = fs[index_];
    u_at_corners[index_c] = fi * u[index_u] + (1-fi) * u[index_u-nx];

    v_at_corners[index_c] = v[index_v];
    
  }
  
  /* N */
  J = Ny-1;
  j = ny-1;

  for(I=2; I<Nx-1; I++){

    i = I-1;
    
    index_ = J*Nx+I;
    index_c = j*nx+i; // -- coordinates
    index_u = J*nx+i;
    index_v = j*Nx+I;
    
    u_at_corners[index_c] = u[index_u];
    
    fi = fw[index_];
    v_at_corners[index_c] = fi * v[index_v] + (1-fi) * v[index_v-1];

  }

  /* W */
  I = 0;
  i = 0;
  
  for(J=2; J<Ny-1; J++){
    
    j = J-1;
    
    index_ = J*Nx+I;
    index_c = j*nx+i; // -- coordinates
    index_u = J*nx+i;
    index_v = j*Nx+I;

    fi = fs[index_];
    u_at_corners[index_c] = fi * u[index_u] + (1-fi) * u[index_u-nx];

    v_at_corners[index_c] = v[index_v];
    
  }

  /* This is included here for symmetry with *_3D CG model calls since 
     there is no simil in the _3D implementation of the function below */
  /* sets:
     U, V (including EWNS, excluding corners)
     using:
     u_at_faces, v_at_faces
  */
  compute_vel_at_centers_full_from_faces(data);
  
  return;
}

/* VEL_AT_CROSSED_FACES_3D */
/*****************************************************************************/
/**

  Sets velocities at all faces surrounding main grip P point. 
  This is required for computing nabla u.

  Data_Mem::u_at_faces: Natural position of x component velocity exists in all domain.

  Data_Mem::u_at_faces_y: Velocity x component placed at n and s faces of P main node, 
  must include j == 0 and j == ny-1 
  (I == 0, I == Nx-1, K == 0, K == Nz-1 are not needed).

  Data_Mem::u_at_faces_z: Velocity x component placed at t and b faces of P main node, 
  must include k == 0 and k == nz-1 
  (I == 0, I == Nx-1, J == 0, J == Ny-1 are not needed).

  Similar analysis for the other velocity components.

  @param data
    
  @return void.
    
  MODIFIED: 15-05-2021
*/
void vel_at_crossed_faces_3D(Data_Mem *data){

  int i, j, k, I, J, K, a;
  int idxa, idxb, idxc, idxd;
  int index_, index_u, index_v, index_w, curv_ind;
  int curve_factor = 1;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  const int curves = data->curves;

  /* These matrices indicate if the face, in any fraction is, 
     available in the fluid (0) or (curve+1) otherwise */
  const int *u_dom_matrix = data->u_dom_matrix;
  const int *v_dom_matrix = data->v_dom_matrix;
  const int *w_dom_matrix = data->w_dom_matrix;
  
  /* These matrices indicate if the main grid faces are cut */
  const int *p_cut_face_x = data->p_cut_face_x;
  const int *p_cut_face_y = data->p_cut_face_y;
  const int *p_cut_face_z = data->p_cut_face_z;

  const int *curve_is_solid_OK = data->curve_is_solid_OK;

  double F = 0;
  double sum_wi = 0;
  
  /* Center of face coordinate */
  double xM[3];
  /* Coordinates for four point interpolation */
  double xa[3], xb[3], xc[3], xd[3];
  /* Lengths */
  double la, lb, lc, ld, L;
  /* Weights */
  double wa, wb, wc, wd;
  /* Velocities */
  double ua, ub, uc, ud;
  
  double pc_temp[16];

  const double pol_tol = data->pol_tol;

  double *u_at_faces_y = data->u_at_faces_y;
  double *u_at_faces_z = data->u_at_faces_z;
  double *v_at_faces_x = data->v_at_faces_x;
  double *v_at_faces_z = data->v_at_faces_z;
  double *w_at_faces_x = data->w_at_faces_x;
  double *w_at_faces_y = data->w_at_faces_y;  

  const double *u = data->u;
  const double *v = data->v;
  const double *w = data->w;

  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  const double *nodes_z = data->nodes_z;

  const double *nodes_x_u = data->nodes_x_u;
  const double *nodes_y_u = data->nodes_y_u;
  const double *nodes_z_u = data->nodes_z_u;

  const double *nodes_x_v = data->nodes_x_v;
  const double *nodes_y_v = data->nodes_y_v;
  const double *nodes_z_v = data->nodes_z_v;

  const double *nodes_x_w = data->nodes_x_w;
  const double *nodes_y_w = data->nodes_y_w;
  const double *nodes_z_w = data->nodes_z_w;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  
  /*
    const double *p_faces_x = data->p_faces_x;
    const double *p_faces_y = data->p_faces_y;
    const double *p_faces_z = data->p_faces_z;
        
    const double *fs_u = data->fs_u;
    const double *ft_u = data->ft_u;
  
    const double *fw_v = data->fw_v;
    const double *ft_v = data->ft_v;

    const double *fw_w = data->fw_w;
    const double *fs_w = data->fs_w;
  */
  
  const double *poly_coeffs = data->poly_coeffs;

  /* Centering at natural faces first */
  center_vel_components(data);

  /* Now we have all velocities centered at their normal faces */
  /* Must compute at crossed faces */

  /* u velocities at y faces */
#pragma omp parallel for default(none)					\
  firstprivate(curves)							\
  private(a, i, j, I, J, K, index_, index_u, index_v,			\
	  idxa, idxb, idxc, idxd,					\
	  curv_ind, curve_factor,					\
	  xa, xb, xc, xd, xM, F, pc_temp,				\
	  la, lb, lc, ld, L,						\
	  wa, wb, wc, wd, sum_wi,					\
	  ua, ub, uc, ud)						\
  shared(p_cut_face_y, u, u_at_faces_y,	u_dom_matrix,			\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 Delta_x, Delta_y, nodes_x_u, nodes_y_u, nodes_z_u)
  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
	
	J = j + 1;
	i = I - 1;
      
	index_ = K*Ny*Nx + J*Nx + I;
	index_u = K*Ny*nx + J*nx + i;
	index_v = K*ny*Nx + j*Nx + I;

	/* Midpoint coordinates */
	xM[0] = nodes_x[index_];
	xM[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	xM[2] = nodes_z[index_];
	
	/* See if face is cut */
	if(p_cut_face_y[index_v] != 0){
	  /* Getting the correct curve */
	  curv_ind = p_cut_face_y[index_v] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }
	  
	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	  
	  /* Evaluating at the midpoint if we are in the solid */
	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    /* xM within solid phase */
	    u_at_faces_y[index_v] = 0;
	    /* Get to next point */
	    break;
	  }
	}// if(p_cut_face_y[index_v] != 0)

	/* From this point only fluid M point is considered */
	/* Now we get the coordinates of the 4 neighborgs */
    
	idxa = index_u + 1;
	idxb = index_u;
	idxc = index_u - nx;
	idxd = index_u - nx + 1;
    
	get_xi(xa, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxa);

	get_xi(xb, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxb);

	get_xi(xc, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxc);

	get_xi(xd, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxd);

	/* Velocities are */
	ua = u[idxa];
	ub = u[idxb];
	uc = u[idxc];
	ud = u[idxd];

	/* Compute distance for each point to the middlepoint */
	la = vec_dist(xM, xa);
	lb = vec_dist(xM, xb);
	lc = vec_dist(xM, xc);
	ld = vec_dist(xM, xd);
      
	/* Form the weights: note that sum(wi) = 3L */
	L = la + lb + lc + ld;
	wa = L - la;
	wb = L - lb;
	wc = L - lc;
	wd = L - ld;
	sum_wi = 3*L;
	  
	/* The average is */
	u_at_faces_y[index_v] = (wa*ua + wb*ub + wc*uc + wd*ud) / sum_wi;
	  
      } // for(I=1; I<Nx-1; I++)
    } // for(j=0; j<ny; j++)
  } // for(K=1; K<Nz-1; K++)

  /* u velocities at z faces */
#pragma omp parallel for default(none)					\
  firstprivate(curves)							\
  private(a, i, k, I, J, K, index_, index_u, index_w,			\
	  idxa, idxb, idxc, idxd,					\
	  curv_ind, curve_factor,					\
	  xa, xb, xc, xd, xM, F, pc_temp,				\
	  la, lb, lc, ld, L,						\
	  wa, wb, wc, wd, sum_wi,					\
	  ua, ub, uc, ud)						\
  shared(p_cut_face_z, u, u_at_faces_z,	u_dom_matrix,			\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 Delta_x, Delta_z, nodes_x_u, nodes_y_u, nodes_z_u)
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	K = k + 1;
	i = I - 1;
	
	index_ = K*Ny*Nx + J*Nx + I;
	index_u = K*Ny*nx + J*nx + i;
	index_w = k*Ny*Nx + J*Nx + I;
	
	/* Midpoint coordinates */
	xM[0] = nodes_x[index_];
	xM[1] = nodes_y[index_];
	xM[2] = nodes_z[index_] - 0.5*Delta_z[index_];
	
	/* See if face is cut */
	if(p_cut_face_z[index_w] != 0){
	  /* Getting the correct curve */
	  curv_ind = p_cut_face_z[index_w] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }
	  
	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	  
	  /* Evaluating at the midpoint if we are in the solid */
	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    /* xM within solid phase */
	    u_at_faces_z[index_w] = 0;
	    /* Get to next point */
	    break;
	  }
	}// if(p_cut_face_z[index_w] != 0)

	/* From this point only fluid M point is considered */
	/* Now we get the coordinates of the 4 neighborgs */
    
	idxa = index_u + 1;
	idxb = index_u;
	idxc = index_u - nx*Ny;
	idxd = index_u - nx*Ny + 1;
    
	get_xi(xa, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxa);

	get_xi(xb, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxb);

	get_xi(xc, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxc);

	get_xi(xd, xM, poly_coeffs,
	       u_dom_matrix,
	       nodes_x_u, nodes_y_u, nodes_z_u,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxd);

	/* Velocities are */
	ua = u[idxa];
	ub = u[idxb];
	uc = u[idxc];
	ud = u[idxd];

	/* Compute distance for each point to the middlepoint */
	la = vec_dist(xM, xa);
	lb = vec_dist(xM, xb);
	lc = vec_dist(xM, xc);
	ld = vec_dist(xM, xd);
      
	/* Form the weights: note that sum(wi) = 3L */
	L = la + lb + lc + ld;
	wa = L - la;
	wb = L - lb;
	wc = L - lc;
	wd = L - ld;
	sum_wi = 3*L;
	  
	/* The average is */
	u_at_faces_z[index_w] = (wa*ua + wb*ub + wc*uc + wd*ud) / sum_wi;
	  
      } // for(I=1; I<Nx-1; I++)
    } // for(J=1; J<Ny-1; J++)
  } // for(k=0; k<nz; k++)
  
  /* v velocities at x faces */
#pragma omp parallel for default(none)					\
  firstprivate(curves)							\
  private(a, i, j, I, J, K, index_, index_u, index_v,			\
	  idxa, idxb, idxc, idxd,					\
	  curv_ind, curve_factor,					\
	  xa, xb, xc, xd, xM, F, pc_temp,				\
	  la, lb, lc, ld, L,						\
	  wa, wb, wc, wd, sum_wi,					\
	  ua, ub, uc, ud)						\
  shared(p_cut_face_x, v, v_at_faces_x,	v_dom_matrix,			\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 Delta_x, Delta_y, nodes_x_v, nodes_y_v, nodes_z_v)
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	I = i + 1;
	j = J - 1;
	
	index_ = K*Ny*Nx + J*Nx + I;
	index_u = K*Ny*nx + J*nx + i;
	index_v = K*ny*Nx + j*Nx + I;

	/* Midpoint coordinates */
	xM[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	xM[1] = nodes_y[index_];
	xM[2] = nodes_z[index_];
	
	/* See if face is cut */
	if(p_cut_face_x[index_u] != 0){
	  /* Getting the correct curve */
	  curv_ind = p_cut_face_x[index_u] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }
	  
	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	  
	  /* Evaluating at the midpoint if we are in the solid */
	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    /* xM within solid phase */
	    v_at_faces_x[index_u] = 0;
	    /* Get to next point */
	    break;
	  }
	}// if(p_cut_face_x[index_u] != 0)

	/* From this point only fluid M point is considered */
	/* Now we get the coordinates of the 4 neighborgs */
    
	idxa = index_v + Nx;
	idxb = index_v;
	idxc = index_v - 1;
	idxd = index_v - 1 + Nx;
    
	get_xi(xa, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxa);

	get_xi(xb, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxb);

	get_xi(xc, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxc);

	get_xi(xd, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_x[index_], Delta_y[index_]),
	       curves, idxd);

	/* Velocities are */
	ua = v[idxa];
	ub = v[idxb];
	uc = v[idxc];
	ud = v[idxd];

	/* Compute distance for each point to the middlepoint */
	la = vec_dist(xM, xa);
	lb = vec_dist(xM, xb);
	lc = vec_dist(xM, xc);
	ld = vec_dist(xM, xd);
      
	/* Form the weights: note that sum(wi) = 3L */
	L = la + lb + lc + ld;
	wa = L - la;
	wb = L - lb;
	wc = L - lc;
	wd = L - ld;
	sum_wi = 3*L;
	  
	/* The average is */
	v_at_faces_x[index_u] = (wa*ua + wb*ub + wc*uc + wd*ud) / sum_wi;
	  
      } // for(i=0; i<nx; i++)
    } // for(J=1; J<Ny-1; J++)
  } // for(K=0; K<Nz-1; K++)

  /* v velocities at z faces */
#pragma omp parallel for default(none)					\
  firstprivate(curves)							\
  private(a, j, k, I, J, K, index_, index_v, index_w,			\
	  idxa, idxb, idxc, idxd,					\
	  curv_ind, curve_factor,					\
	  xa, xb, xc, xd, xM, F, pc_temp,				\
	  la, lb, lc, ld, L,						\
	  wa, wb, wc, wd, sum_wi,					\
	  ua, ub, uc, ud)						\
  shared(p_cut_face_z, v, v_at_faces_z,	v_dom_matrix,			\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 Delta_y, Delta_z, nodes_x_v, nodes_y_v, nodes_z_v)
  for(k=0; k<nz; k++){
    for(J=1; J<Ny-1; J++){
      for(I=1; I<Nx-1; I++){
	
	K = k + 1;
	j = J - 1;
	
	index_ = K*Ny*Nx + J*Nx + I;
	index_v = K*ny*Nx + j*Nx + I;
	index_w = k*Ny*Nx + J*Nx + I;
	
	/* Midpoint coordinates */
	xM[0] = nodes_x[index_];
	xM[1] = nodes_y[index_];
	xM[2] = nodes_z[index_] - 0.5*Delta_z[index_];
	
	/* See if face is cut */
	if(p_cut_face_z[index_w] != 0){
	  /* Getting the correct curve */
	  curv_ind = p_cut_face_z[index_w] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }
	  
	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	  
	  /* Evaluating at the midpoint if we are in the solid */
	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    /* xM within solid phase */
	    v_at_faces_z[index_w] = 0;
	    /* Get to next point */
	    break;
	  }
	}// if(p_cut_face_z[index_w] != 0)

	/* From this point only fluid M point is considered */
	/* Now we get the coordinates of the 4 neighborgs */
    
	idxa = index_v + Nx;
	idxb = index_v;
	idxc = index_v - ny*Nx;
	idxd = index_v - ny*Nx + Nx;
    
	get_xi(xa, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxa);

	get_xi(xb, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxb);

	get_xi(xc, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxc);

	get_xi(xd, xM, poly_coeffs,
	       v_dom_matrix,
	       nodes_x_v, nodes_y_v, nodes_z_v,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxd);

	/* Velocities are */
	ua = v[idxa];
	ub = v[idxb];
	uc = v[idxc];
	ud = v[idxd];

	/* Compute distance for each point to the middlepoint */
	la = vec_dist(xM, xa);
	lb = vec_dist(xM, xb);
	lc = vec_dist(xM, xc);
	ld = vec_dist(xM, xd);
      
	/* Form the weights: note that sum(wi) = 3L */
	L = la + lb + lc + ld;
	wa = L - la;
	wb = L - lb;
	wc = L - lc;
	wd = L - ld;
	sum_wi = 3*L;
	  
	/* The average is */
	v_at_faces_z[index_w] = (wa*ua + wb*ub + wc*uc + wd*ud) / sum_wi;
	  
      } // for(I=1; I<Nx-1; I++)
    } // for(J=1; J<Ny-1; J++)
  } // for(k=0; k<nz; k++)

    /* w velocities at x faces */
#pragma omp parallel for default(none)					\
  firstprivate(curves)							\
  private(a, i, k, I, J, K, index_, index_u, index_w,			\
	  idxa, idxb, idxc, idxd,					\
	  curv_ind, curve_factor,					\
	  xa, xb, xc, xd, xM, F, pc_temp,				\
	  la, lb, lc, ld, L,						\
	  wa, wb, wc, wd, sum_wi,					\
	  ua, ub, uc, ud)						\
  shared(p_cut_face_x, w, w_at_faces_x,	w_dom_matrix,			\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 Delta_x, Delta_z, nodes_x_w, nodes_y_w, nodes_z_w)
  for(K=1; K<Nz-1; K++){
    for(J=1; J<Ny-1; J++){
      for(i=0; i<nx; i++){
	
	I = i + 1;
	k = K - 1;
	
	index_ = K*Ny*Nx + J*Nx + I;
	index_u = K*Ny*nx + J*nx + i;
	index_w = k*Ny*Nx + J*Nx + I;

	/* Midpoint coordinates */
	xM[0] = nodes_x[index_] - 0.5*Delta_x[index_];
	xM[1] = nodes_y[index_];
	xM[2] = nodes_z[index_];
	
	/* See if face is cut */
	if(p_cut_face_x[index_u] != 0){
	  /* Getting the correct curve */
	  curv_ind = p_cut_face_x[index_u] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }
	  
	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	  
	  /* Evaluating at the midpoint if we are in the solid */
	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    /* xM within solid phase */
	    w_at_faces_x[index_u] = 0;
	    /* Get to next point */
	    break;
	  }
	}// if(p_cut_face_x[index_u] != 0)

	/* From this point only fluid M point is considered */
	/* Now we get the coordinates of the 4 neighborgs */
    
	idxa = index_w + Nx*Ny;
	idxb = index_w;
	idxc = index_w - 1;
	idxd = index_w - 1 + Nx*Ny;
    
	get_xi(xa, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxa);

	get_xi(xb, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxb);

	get_xi(xc, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxc);

	get_xi(xd, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_x[index_], Delta_z[index_]),
	       curves, idxd);

	/* Velocities are */
	ua = w[idxa];
	ub = w[idxb];
	uc = w[idxc];
	ud = w[idxd];

	/* Compute distance for each point to the middlepoint */
	la = vec_dist(xM, xa);
	lb = vec_dist(xM, xb);
	lc = vec_dist(xM, xc);
	ld = vec_dist(xM, xd);
      
	/* Form the weights: note that sum(wi) = 3L */
	L = la + lb + lc + ld;
	wa = L - la;
	wb = L - lb;
	wc = L - lc;
	wd = L - ld;
	sum_wi = 3*L;
	  
	/* The average is */
	w_at_faces_x[index_u] = (wa*ua + wb*ub + wc*uc + wd*ud) / sum_wi;
	  
      } // for(i=0; i<nx; i++)
    } // for(J=1; J<Ny-1; J++)
  } // for(K=1; K<Nz-1; K++)

  /* w velocities at y faces */
#pragma omp parallel for default(none)					\
  firstprivate(curves)							\
  private(a, j, k, I, J, K, index_, index_v, index_w,			\
	  idxa, idxb, idxc, idxd,					\
	  curv_ind, curve_factor,					\
	  xa, xb, xc, xd, xM, F, pc_temp,				\
	  la, lb, lc, ld, L,						\
	  wa, wb, wc, wd, sum_wi,					\
	  ua, ub, uc, ud)						\
  shared(p_cut_face_y, w, w_at_faces_y,	w_dom_matrix,			\
	 poly_coeffs, curve_is_solid_OK, nodes_x, nodes_y, nodes_z,	\
	 Delta_y, Delta_z, nodes_x_w, nodes_y_w, nodes_z_w)
  for(K=1; K<Nz-1; K++){
    for(j=0; j<ny; j++){
      for(I=1; I<Nx-1; I++){
	
	J = j + 1;
	k = K - 1;
	
	index_ = K*Ny*Nx + J*Nx + I;
	index_v = K*ny*Nx + j*Nx + I;
	index_w = k*Ny*Nx + J*Nx + I;

	/* Midpoint coordinates */
	xM[0] = nodes_x[index_];
	xM[1] = nodes_y[index_] - 0.5*Delta_y[index_];
	xM[2] = nodes_z[index_];
	
	/* See if face is cut */
	if(p_cut_face_y[index_v] != 0){
	  /* Getting the correct curve */
	  curv_ind = p_cut_face_y[index_v] - 1;
	  for(a=0; a<16; a++){
	    pc_temp[a] = poly_coeffs[curv_ind*16 + a];
	  }
	  
	  curve_factor = pow(-1, 1 + curve_is_solid_OK[curv_ind]);
	  
	  /* Evaluating at the midpoint if we are in the solid */
	  F = polyn(xM, pc_temp) * curve_factor;
	  if(F <= pol_tol){
	    /* xM within solid phase */
	    w_at_faces_y[index_v] = 0;
	    /* Get to next point */
	    break;
	  }
	}// if(p_cut_face_y[index_v] != 0)

	/* From this point only fluid M point is considered */
	/* Now we get the coordinates of the 4 neighborgs */
    
	idxa = index_w + Nx*Ny;
	idxb = index_w;
	idxc = index_w - Nx;
	idxd = index_w - Nx + Nx*Ny;
    
	get_xi(xa, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxa);

	get_xi(xb, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxb);

	get_xi(xc, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxc);

	get_xi(xd, xM, poly_coeffs,
	       w_dom_matrix,
	       nodes_x_w, nodes_y_w, nodes_z_w,
	       MAX(Delta_y[index_], Delta_z[index_]),
	       curves, idxd);

	/* Velocities are */
	ua = w[idxa];
	ub = w[idxb];
	uc = w[idxc];
	ud = w[idxd];

	/* Compute distance for each point to the middlepoint */
	la = vec_dist(xM, xa);
	lb = vec_dist(xM, xb);
	lc = vec_dist(xM, xc);
	ld = vec_dist(xM, xd);
      
	/* Form the weights: note that sum(wi) = 3L */
	L = la + lb + lc + ld;
	wa = L - la;
	wb = L - lb;
	wc = L - lc;
	wd = L - ld;
	sum_wi = 3*L;
	  
	/* The average is */
	w_at_faces_y[index_v] = (wa*ua + wb*ub + wc*uc + wd*ud) / sum_wi;
	  
      } // for(I=1; I<Nx-1; I++)
    } // for(j=0; j<ny; j++)
  } // for(K=1; K<Nz-1; K++)
  
  return;
}

/* GET_XI */
/*****************************************************************************/
/**
  
  Gets xi coordinates of the velocity point in a cut face. 
  In case that the velocity face is completely cover it seeks the 
  coordinates on the surface with a normal vector that passes through xM.
  Here array phase is for instance u_dom_matrix that is != 0 only when 
  the face is completely covered due to node displacement in the 
  cut cell treatment.
    
  @return void.
    
  MODIFIED: 30-07-2019
*/
inline void get_xi(double *xi, const double *xM, const double *poly_coeffs,
		   const int *phase,
		   const double *x_, const double *y_, const double *z_,
		   const double cell_size,
		   const int curves, const int idx){
  
  int a = 0;
  int curv_ind = 0;

  double pc_temp[16];
  
  /* See if velocity face is within solid body */
  if(phase[idx] != 0){
    /* Face is within solid, we need to get distance to M */
    /* Get the correct curve */
    curv_ind = phase[idx] - 1;
    for(a=0; a<16; a++){
      pc_temp[a] = poly_coeffs[curv_ind*16 + a];
    }
    
    /* Here we get xi directly */
    calc_xint(xi, xM, pc_temp, cell_size);
    
  }
  else{
    /* Face is not within solid body, could be cut or uncut same treatment */
    xi[0] = x_[idx];
    xi[1] = y_[idx];
    xi[2] = z_[idx];
  }
  
  return;
}

/* COMPUTE_NABLA_U_3D */
/*****************************************************************************/
/**
  
  Computes the 9 elements of the nablau tensor from the velocities centered 
  in all faces:

  Data_Mem::u_at_faces, Data_Mem::u_at_faces_y, Data_Mem::u_at_faces_z, ...
  Data_Mem::w_at_faces.

  Results are stored via an offset (4D) that points to 
  the 9 tensor components as indicated below:

  0: xx ==> du/dx

  1: yx ==> dv/dx

  2: zx ==> dw/dx

  3: xy ==> du/dy

  4: yy ==> dv/dy

  5: zy ==> dw/dy

  6: xz ==> du/dz

  7; yz ==> dv/dz

  8: zz ==> dw/dz

  Derivatives are estimated as the velocity components are effectively in the 
  center of the face regardless of the presence of a solid in the line joining
  the midpoint with the face.

  @param data

  @return void.
    
  MODIFIED: 04-08-2019
*/
void compute_nabla_u_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int index_, index_u, index_v, index_w;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;
  
  const int offset = Nz*Ny*Nx;
  
  const int *dom_matrix = data->domain_matrix;
  
  double *nabla_u = data->nabla_u;

  const double *ux = data->u_at_faces;
  const double *uy = data->u_at_faces_y;
  const double *uz = data->u_at_faces_z;
  
  const double *vx = data->v_at_faces_x;
  const double *vy = data->v_at_faces;
  const double *vz = data->v_at_faces_z;

  const double *wx = data->w_at_faces_x;
  const double *wy = data->w_at_faces_y;
  const double *wz = data->w_at_faces;
  
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;
  
#pragma omp parallel for default(none)				\
  private(I, J, K, i, j, k, index_, index_u, index_v, index_w)	\
  shared(dom_matrix, nabla_u, Delta_x, Delta_y, Delta_z,	\
	 ux, uy, uz, vx, vy, vz, wx, wy, wz)
  
  for (K=1; K<(Nz-1); K++){
    for (J=1; J<(Ny-1); J++){
      for (I=1; I<(Nx-1); I++){
	
	i = I - 1;
	j = J - 1;
	k = K - 1;
	
	index_ = K*Ny*Nx + J*Nx + I;
	
	if(dom_matrix[index_] == 0){
	  
	  index_u = K*Ny*nx + J*nx + i;
	  index_v = K*ny*Nx + j*Nx + I;
	  index_w = k*Ny*Nx + J*Nx + I;
	  
	  /* 0 = 11 = du/dx */
	  nabla_u[index_+0*offset] = (ux[index_u+1]-ux[index_u])/Delta_x[index_];
	  /* 1 = 21 = dv/dx */
	  nabla_u[index_+1*offset] = (vx[index_u+1]-vx[index_u])/Delta_x[index_];
	  /* 2 = 31 = dw/dx */
	  nabla_u[index_+2*offset] = (wx[index_u+1]-wx[index_u])/Delta_x[index_];
	  
	  /* 3 = 12 = du/dy */
	  nabla_u[index_+3*offset] = (uy[index_v+Nx]-uy[index_v])/Delta_y[index_];
	  /* 4 = 22 = dv/dy */
	  nabla_u[index_+4*offset] = (vy[index_v+Nx]-vy[index_v])/Delta_y[index_];
	  /* 5 = 32 = dw/dy */
	  nabla_u[index_+5*offset] = (wy[index_v+Nx]-wy[index_v])/Delta_y[index_];

	  /* 6 = 13 = du/dz */
	  nabla_u[index_+6*offset] = (uz[index_w+Ny*Nx]-uz[index_w])/Delta_z[index_];
	  /* 7 = 23 = dv/dz */
	  nabla_u[index_+7*offset] = (vz[index_w+Ny*Nx]-vz[index_w])/Delta_z[index_];
	  /* 8 = 33 = dw/dz */
	  nabla_u[index_+8*offset] = (wz[index_w+Ny*Nx]-wz[index_w])/Delta_z[index_];
	}
      }
    }
  }
  
  return;
}

/* COMPUTE_DEF_TENS_3D */
/*****************************************************************************/
/**
  
  Computes the 9 elements of the deformation tensor with offset as indicated in 
  nabla u computation.

  @see compute_nabla_u_3D

  @param data

  @return void.
    
  MODIFIED: 04-08-2019
*/
void compute_def_tens_3D(Data_Mem *data){

  int I, J, K;
  int index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int offset = Nz*Ny*Nx;
  
  const int *dom_matrix = data->domain_matrix;
  
  const double *nu = data->nabla_u;

  double *D = data->def_tens;
  
#pragma omp parallel for default(none)		\
  private(I, J, K, index_)			\
  shared(D, nu, dom_matrix)
  for (K=1; K<(Nz-1); K++){
    for (J=1; J<(Ny-1); J++){
      for (I=1; I<(Nx-1); I++){
	
	index_ = K*Ny*Nx + J*Nx + I;
	
	if(dom_matrix[index_] == 0){

	  D[index_+0*offset] = 0.5*(nu[index_+0*offset] + nu[index_+0*offset]);
	  D[index_+1*offset] = 0.5*(nu[index_+1*offset] + nu[index_+3*offset]);
	  D[index_+2*offset] = 0.5*(nu[index_+2*offset] + nu[index_+6*offset]);

	  D[index_+3*offset] = 0.5*(nu[index_+1*offset] + nu[index_+3*offset]);
	  D[index_+4*offset] = 0.5*(nu[index_+4*offset] + nu[index_+4*offset]);
	  D[index_+5*offset] = 0.5*(nu[index_+5*offset] + nu[index_+7*offset]);

	  D[index_+6*offset] = 0.5*(nu[index_+2*offset] + nu[index_+6*offset]);
	  D[index_+7*offset] = 0.5*(nu[index_+5*offset] + nu[index_+7*offset]);
	  D[index_+8*offset] = 0.5*(nu[index_+8*offset] + nu[index_+8*offset]);

	}
      }
    }
  }
  
  return;
}

/* COMPUTE_TAU_TURB_3D */
/*****************************************************************************/
/**
  
  Computes the 9 elements of the stress tensor with offset as indicated in 
  nabla u computation.

  @see compute_nabla_u_3D

  @param data

  @return void.
    
  MODIFIED: 07-08-2019
*/
void compute_tau_turb_3D(Data_Mem *data){

  int I, J, K;
  int index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int offset = Nz*Ny*Nx;

  const int turb_model = data->turb_model;

  const int *dom_matrix = data->domain_matrix;
  
  double kin_visc;
  double kin_visc_turb, kin_visc_turb_min;
  double l_ast;
  double y_ast;
  double pre_prod_for_l_ast;
  double re_turb;

  const double rho = data->rho_v[0];
  const double mu = data->mu;
  /* Fraction of the molecular kinematic viscosity to be considered as 
     minimum value of mu_turb */
  const double kin_visc_frac = 0.1;
  const double C_mu = data->C_mu;
  /* Size of the largest eddies is the width of the domain */
  const double max_mix_l = data->max_mix_l;

  double *mu_turb = data->mu_turb;
  double *gamma = data->gamma;
  double *tau_turb = data->tau_turb;
  double *f_1 = data->f_1;
  double *f_2 = data->f_2;
  double *f_mu = data->f_mu;

  const double *D = data->def_tens;
  const double *k_turb = data->k_turb;
  const double *eps_turb = data->eps_turb;
  const double *y_wall = data->y_wall;

  kin_visc = mu/rho;
  kin_visc_turb_min = kin_visc_frac * kin_visc;
  
#pragma omp parallel for default(none)					\
  private(I, J, K, index_, l_ast, pre_prod_for_l_ast, re_turb, y_ast, kin_visc_turb) \
  shared(dom_matrix, k_turb, eps_turb, y_wall, f_1, f_2, f_mu,		\
	 mu_turb, gamma, tau_turb, D, kin_visc, kin_visc_turb_min)
  for(K=1; K<(Nz-1); K++){
    for(J=1; J<(Ny-1); J++){
      for(I=1; I<(Nx-1); I++){
      
	index_ = K*Ny*Nx + J*Nx + I;
      
	if(dom_matrix[index_] == 0){
	
	  /* Calculating turbulent viscosity */
	  pre_prod_for_l_ast = C_mu * k_turb[index_] * k_turb[index_] /
	    (sqrt(k_turb[index_]));
	
	  if (k_turb[index_] == 0){
	    l_ast = 0;
	  }
	  else if ((k_turb[index_] != 0) && (eps_turb[index_] == 0)){
	    l_ast = max_mix_l;
	  }
	  else if (pre_prod_for_l_ast < eps_turb[index_] * max_mix_l){
	    l_ast = pre_prod_for_l_ast / eps_turb[index_];
	  }
	  else {
	    l_ast = max_mix_l;
	  }
      
	  if ((k_turb[index_] == 0) || (eps_turb[index_] == 0)){
	    re_turb = 0.0;
	  }
	  else {
	    re_turb = k_turb[index_] * k_turb[index_] / (eps_turb[index_] * kin_visc);
	  }

	  if(turb_model == 1){
	    /* Abe, Kondoh & Nagano (1994) (pp 80 de Lemos) */
	    y_ast = pow(kin_visc*eps_turb[index_], 0.25) * y_wall[index_] / kin_visc;
	  
	    f_1[index_]  = 1.0;
	    f_2[index_]  = pow(1 - exp((-1) * y_ast / 3.1), 2.0) * 
	      (1 - 0.3*exp((-1)*pow(re_turb / 6.5, 2.0)));
	    f_mu[index_] = pow(1 - exp((-1) * y_ast / 14), 2.0) * 
	      (1 + (5/(pow(re_turb, 0.75))) * exp((-1)*pow(re_turb / 200.0, 2.0)));
	  }
	  else if(turb_model == 2){
	    /* Cho & Goldstein (1994) */
	    f_1[index_]  = 1.0;
	    f_2[index_]  = 1 - 0.222 * exp( (-1) * re_turb * re_turb / 36.0);
	    f_mu[index_] = 1 - 0.95 * exp( (-5.0e-5) * re_turb * re_turb);
	  }
	
	  /* ARBITRARY */
	  if(re_turb == 0){
	    f_2[index_] = 0;
	    f_mu[index_] = 0;
	  }
      
	  /* END Section DAMPING FUNCTIONS */
	  kin_visc_turb = l_ast * sqrt(k_turb[index_]);
      
	  /* Selection of maxima value */
	  mu_turb[index_] = rho * MAX(kin_visc_turb_min, f_mu[index_]*kin_visc_turb);
      
	  gamma[index_] = C_mu * k_turb[index_] * rho * f_mu[index_] / mu_turb[index_];
      
	  tau_turb[index_ + 0*offset] = 2 * mu_turb[index_] * D[index_ + 0*offset] -
	    (2.0/3.0)*rho*k_turb[index_];
	  tau_turb[index_ + 1*offset] = 2 * mu_turb[index_] * D[index_ + 1*offset];
	  tau_turb[index_ + 2*offset] = 2 * mu_turb[index_] * D[index_ + 2*offset];
	  
	  tau_turb[index_ + 3*offset] = 2 * mu_turb[index_] * D[index_ + 3*offset];
	  tau_turb[index_ + 4*offset] = 2 * mu_turb[index_] * D[index_ + 4*offset] -
	    (2.0/3.0)*rho*k_turb[index_];
	  tau_turb[index_ + 5*offset] = 2 * mu_turb[index_] * D[index_ + 5*offset];

	  tau_turb[index_ + 6*offset] = 2 * mu_turb[index_] * D[index_ + 6*offset];
	  tau_turb[index_ + 7*offset] = 2 * mu_turb[index_] * D[index_ + 7*offset];
	  tau_turb[index_ + 8*offset] = 2 * mu_turb[index_] * D[index_ + 8*offset] -
	    (2.0/3.0)*rho*k_turb[index_];
	}
      }
    }
  }

  return;
}

/* COMPUTE_P_K_3D */
/*****************************************************************************/
/**
  
   Sets:

   Array (double, NxNyNz) Data_Mem::P_k: Turbulent kinetic energy production term, forcing it to be >= 0.

  @param data

  @return void.
    
  MODIFIED: 07-08-2019
*/
void compute_P_k_3D(Data_Mem *data){

  int I, J, K;
  int index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int offset = Nz*Ny*Nx;

  const int *dom_matrix = data->domain_matrix;

  double *P_k = data->P_k;

  const double *tau_turb = data->tau_turb;
  const double *nu = data->nabla_u;

#pragma omp parallel for default(none)		\
  private(I, J, K, index_)			\
  shared(dom_matrix, P_k, tau_turb, nu)
  
  for (K=1; K<(Nz-1); K++){
    for (J=1; J<(Ny-1); J++){
      for (I=1; I<(Nx-1); I++){
      
	index_ = K*Ny*Nx + J*Nx + I;

	if(dom_matrix[index_] == 0){
	  /* P_k = -(rho u'u') : nu = (tau_turb : nu) [=] W/m3 = Pa/s = kg/m/s3 */
	  P_k[index_] = MAX(0, (tau_turb[index_ + 0*offset] * nu[index_ + 0*offset] + 
				tau_turb[index_ + 1*offset] * nu[index_ + 1*offset] +
				tau_turb[index_ + 2*offset] * nu[index_ + 2*offset] + 
				tau_turb[index_ + 3*offset] * nu[index_ + 3*offset] +
				tau_turb[index_ + 4*offset] * nu[index_ + 4*offset] + 
				tau_turb[index_ + 5*offset] * nu[index_ + 5*offset] +
				tau_turb[index_ + 6*offset] * nu[index_ + 6*offset] + 
				tau_turb[index_ + 7*offset] * nu[index_ + 7*offset] +
				tau_turb[index_ + 8*offset] * nu[index_ + 8*offset]));
	}
      }
    }
  }
    
  return;
}

/* COMPUTE_D2UIDXJDXK2_3D */
/*****************************************************************************/
/**
  
  Computes part of the source term related to derivatives for the 
  CG turbulence model.

  @param data

  @return void.
    
  MODIFIED: 08-08-2019
*/
void compute_d2Uidxjdxk2_3D(Data_Mem *data){
  
  int i, j, k, I, J, K;
  int index_, index_c;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  const int nx = data->nx;
  const int ny = data->ny;

  const int *dom_matrix = data->domain_matrix;

  double d2Udx2, d2Udy2, d2Udz2, d2Udxdy, d2Udxdz, d2Udydz;
  double d2Vdx2, d2Vdy2, d2Vdz2, d2Vdxdy, d2Vdxdz, d2Vdydz;
  double d2Wdx2, d2Wdy2, d2Wdz2, d2Wdxdy, d2Wdxdz, d2Wdydz;
  
  double dx, dy, dz;
  double dxE, dxW, dyN, dyS, dzT, dzB;
  
  double UP, UE, UW, UN, US, UT, UB;
  double VP, VE, VW, VN, VS, VT, VB;
  double WP, WE, WW, WN, WS, WT, WB;

  double d2Uidxjdxk;
  double *d2Uidxjdxk2 = data->d2Uidxjdxk2;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;

  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;

  const double *Delta_zT = data->Delta_zT;
  const double *Delta_zB = data->Delta_zB;
  
  const double *U = data->U;
  const double *V = data->V;
  const double *W = data->W;

  const double *uc = data->u_at_corners;
  const double *vc = data->v_at_corners;
  const double *wc = data->w_at_corners;

#pragma omp parallel for default(none)					\
  private(I, J, K, i, j, k, index_, index_c,				\
	  d2Uidxjdxk,							\
	  d2Udx2, d2Udy2, d2Udz2,					\
	  d2Udxdy, d2Udxdz, d2Udydz,					\
	  d2Vdx2, d2Vdy2, d2Vdz2,					\
  	  d2Vdxdy, d2Vdxdz, d2Vdydz,					\
  	  d2Wdx2, d2Wdy2, d2Wdz2,					\
	  d2Wdxdy, d2Wdxdz, d2Wdydz,					\
  	  dx, dy, dz, dxE, dxW, dyN, dyS, dzT, dzB,			\
	  UP, UE, UW, UN, US, UT, UB,					\
	  VP, VE, VW, VN, VS, VT, VB,					\
	  WP, WE, WW, WN, WS, WT, WB)					\
  shared(d2Uidxjdxk2,							\
	 Delta_x, Delta_y, Delta_z,					\
	 Delta_xE, Delta_xW, Delta_yN, Delta_yS, Delta_zT, Delta_zB,	\
	 U, V, W, uc, vc, wc, dom_matrix)
  for (K=1; K<(Nz-1); K++){
    for (J=1; J<(Ny-1); J++){
      for (I=1; I<(Nx-1); I++){
	
	index_ = K*Ny*Nx + J*Nx + I;

	if(dom_matrix[index_] == 0){
	  
	  i = I-1;
	  j = J-1;
	  k = K-1;
	  
	  index_c = k*ny*nx + j*nx + i;

	  dx = Delta_x[index_];
	  dy = Delta_y[index_];
	  dz = Delta_z[index_];

	  dxE = Delta_xE[index_];
	  dxW = Delta_xW[index_];
      
	  dyN = Delta_yN[index_];
	  dyS = Delta_yS[index_];

	  dzT = Delta_zT[index_];
	  dzB = Delta_zB[index_];
	
	  UP = U[index_];
	  UE = U[index_+1];
	  UW = U[index_-1];
	  UN = U[index_+Nx];
	  US = U[index_-Nx];
	  UT = U[index_+Ny*Nx];
	  UB = U[index_-Ny*Nx];

	  VP = V[index_];
	  VE = V[index_+1];
	  VW = V[index_-1];
	  VN = V[index_+Nx];
	  VS = V[index_-Nx];
	  VT = V[index_+Ny*Nx];
	  VB = V[index_-Ny*Nx];

	  WP = W[index_];
	  WE = W[index_+1];
	  WW = W[index_-1];
	  WN = W[index_+Nx];
	  WS = W[index_-Nx];
	  WT = W[index_+Ny*Nx];
	  WB = W[index_-Ny*Nx];
	
	  d2Udx2 = D2UIDXJ2(UW,UP,UE,dxW,dxE,dx);
	  d2Udy2 = D2UIDXJ2(US,UP,UN,dyS,dyN,dy);
	  d2Udz2 = D2UIDXJ2(UB,UP,UT,dzB,dzT,dz);

	  d2Vdx2 = D2UIDXJ2(VW,VP,VE,dxW,dxE,dx);
	  d2Vdy2 = D2UIDXJ2(VS,VP,VN,dyS,dyN,dy);
	  d2Vdz2 = D2UIDXJ2(VB,VP,VT,dzB,dzT,dz);

	  d2Wdx2 = D2UIDXJ2(WW,WP,WE,dxW,dxE,dx);
	  d2Wdy2 = D2UIDXJ2(WS,WP,WN,dyS,dyN,dy);
	  d2Wdz2 = D2UIDXJ2(WB,WP,WT,dzB,dzT,dz);

	  d2Udxdy = _cross_der(index_c, 1, nx, ny*nx, uc, dx*dy);
	  d2Udxdz = _cross_der(index_c, 1, ny*nx, nx, uc, dx*dz);
	  d2Udydz = _cross_der(index_c, nx, ny*nx, 1, uc, dy*dz);

	  d2Vdxdy = _cross_der(index_c, 1, nx, ny*nx, vc, dx*dy);
	  d2Vdxdz = _cross_der(index_c, 1, ny*nx, nx, vc, dx*dz);
	  d2Vdydz = _cross_der(index_c, nx, ny*nx, 1, vc, dy*dz);

	  d2Wdxdy = _cross_der(index_c, 1, nx, ny*nx, wc, dx*dy);
	  d2Wdxdz = _cross_der(index_c, 1, ny*nx, nx, wc, dx*dz);
	  d2Wdydz = _cross_der(index_c, nx, ny*nx, 1, wc, dy*dz);
	
	  d2Uidxjdxk =
	    d2Udx2 + d2Udy2 + d2Udz2 +
	    2*(d2Udxdy + d2Udxdz + d2Udydz) +
	    d2Vdx2 + d2Vdy2 + d2Vdz2 +
	    2*(d2Vdxdy + d2Vdxdz + d2Vdydz) +
	    d2Wdx2 + d2Wdy2 + d2Wdz2 +
	    2*(d2Wdxdy + d2Wdxdz + d2Wdydz);

	  d2Uidxjdxk2[index_] = d2Uidxjdxk * d2Uidxjdxk;

	}
              
      }
    }
  }
  
  return;
}

/* _CROSS_DER */
/*****************************************************************************/
/**
  
  Interpolates velocities and computes crossed derivatives within a cell.
  The three different derivatives can be obtained calling the function with 
  the appropriate offsets.
    
  Case xy: (1, nx, nynx)

  Case xz: (1, nynx, nx)

  Case yz: (nx, nynx, 1)
    
  This function does not discriminate if points are within a curve.  
  
  @return Value of cross derivative at center of the cell with O(h2).
    
  MODIFIED: 11-08-2019
*/
inline double _cross_der(const int ic,
			 const int offset1, const int offset2, const int offset3,
			 const double *u, const double dxdy){

  
  double ua, ub, uc, ud;

  double d2Udxdy;
  
  ua = 0.5*( u[ic + offset1 + offset2] + u[ic + offset1 + offset2 + offset3] );
  ub = 0.5*( u[ic + offset2] + u[ic + offset2 + offset3] );
  uc = 0.5*( u[ic] + u[ic + offset3] );
  ud = 0.5*( u[ic + offset1] + u[ic + offset1 + offset3] );

  d2Udxdy = (ua + uc - ub - ud) / dxdy;
  
  return d2Udxdy;

}

/* VEL_AT_CORNERS_3D*/
/*****************************************************************************/
/**
  
  Computes velocities at vertexes using an eight point interpolation scheme.
  This function does not discriminate if vertex is within a curve.

  The results: u_at_cornes, v_at_corners, w_at_corners can be used to
  further interpolate values within a cell to estimate crossed derivatives. 

  @param data

  @return void.
    
  MODIFIED: 14-08-2019
*/
void vel_at_corners_3D(Data_Mem *data){

  int i, j, k, I, J, K;
  int idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8;
  int index_, index_c;

  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int nx = data->nx;
  const int ny = data->ny;
  const int nz = data->nz;

  double u1, u2, u3, u4, u5, u6, u7, u8;
  double x1[3], x2[3], x3[3], x4[3], x5[3], x6[3], x7[3], x8[3];
  double l1, l2, l3, l4, l5, l6, l7, l8;
  double w1, w2, w3, w4, w5, w6, w7, w8;
  double sum_wi;
  double L;
  double xPc[3];

  double *uc = data->u_at_corners;
  double *vc = data->v_at_corners;
  double *wc = data->w_at_corners;

  const double *xc = data->vertex_x;
  const double *yc = data->vertex_y;
  const double *zc = data->vertex_z;

  const double *U = data->U;
  const double *V = data->V;
  const double *W = data->W;

  const double *x = data->nodes_x;
  const double *y = data->nodes_y;
  const double *z = data->nodes_z;
  
#pragma omp parallel for default(none)				\
  private(i, j, k, I, J, K, index_, index_c,			\
	  idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8,	\
	  u1, u2, u3, u4, u5, u6, u7, u8,			\
	  x1, x2, x3, x4, x5, x6, x7, x8,			\
	  l1, l2, l3, l4, l5, l6, l7, l8,			\
	  w1, w2, w3, w4, w5, w6, w7, w8,			\
	  sum_wi, L, xPc)					\
  shared(U, V, W, uc, vc, wc, x, y, z, xc, yc, zc)
  for (k=0; k<nz; k++){
    for (j=0; j<ny; j++){
      for (i=0; i<nx; i++){

	I = i+1;
	J = j+1;
	K = k+1;
      
	index_ = K*Ny*Nx + J*Nx + I;
	index_c = k*ny*nx + j*nx + i;

	idx1 = index_;
	idx2 = index_-1;
	idx3 = index_-Ny*Nx;
	idx4 = index_-1-Ny*Nx;
	idx5 = index_-Nx;
	idx6 = index_-1-Nx;
	idx7 = index_-Nx-Ny*Nx;
	idx8 = index_-1-Nx-Ny*Nx;

	xPc[0] = xc[i];
	xPc[1] = yc[j];
	xPc[2] = zc[k];

	x1[0] = x[idx1];
	x1[1] = y[idx1];
	x1[2] = z[idx1];

	x2[0] = x[idx2];
	x2[1] = y[idx2];
	x2[2] = z[idx2];

	x3[0] = x[idx3];
	x3[1] = y[idx3];
	x3[2] = z[idx3];

	x4[0] = x[idx4];
	x4[1] = y[idx4];
	x4[2] = z[idx4];

	x5[0] = x[idx5];
	x5[1] = y[idx5];
	x5[2] = z[idx5];

	x6[0] = x[idx6];
	x6[1] = y[idx6];
	x6[2] = z[idx6];

	x7[0] = x[idx7];
	x7[1] = y[idx7];
	x7[2] = z[idx7];

	x8[0] = x[idx8];
	x8[1] = y[idx8];
	x8[2] = z[idx8];
	
	/* Compute distance for each point to the middlepoint */
	l1 = vec_dist(xPc, x1);
	l2 = vec_dist(xPc, x2);
	l3 = vec_dist(xPc, x3);
	l4 = vec_dist(xPc, x4);
	l5 = vec_dist(xPc, x5);
	l6 = vec_dist(xPc, x6);
	l7 = vec_dist(xPc, x7);
	l8 = vec_dist(xPc, x8);
	
	/* Form the weights: note that sum(wi) = 7L */
	L = l1 + l2 + l3 + l4 + l5 + l6 + l7 + l8;
	w1 = L - l1;
	w2 = L - l2;
	w3 = L - l3;
	w4 = L - l4;
	w5 = L - l5;
	w6 = L - l6;
	w7 = L - l7;
	w8 = L - l8;
	sum_wi = 7*L;

	/* Velocities are */
	u1 = U[idx1];
	u2 = U[idx2];
	u3 = U[idx3];
	u4 = U[idx4];
	u5 = U[idx5];
	u6 = U[idx6];
	u7 = U[idx7];
	u8 = U[idx8];

	uc[index_c] =
	  (w1*u1 + w2*u2 + w3*u3 + w4*u4 + w5*u5 + w6*u6 + w7*u7 + w8*u8)/sum_wi;

	/* Velocities are */
	u1 = V[idx1];
	u2 = V[idx2];
	u3 = V[idx3];
	u4 = V[idx4];
	u5 = V[idx5];
	u6 = V[idx6];
	u7 = V[idx7];
	u8 = V[idx8];

	vc[index_c] =
	  (w1*u1 + w2*u2 + w3*u3 + w4*u4 + w5*u5 + w6*u6 + w7*u7 + w8*u8)/sum_wi;

	/* Velocities are */
	u1 = W[idx1];
	u2 = W[idx2];
	u3 = W[idx3];
	u4 = W[idx4];
	u5 = W[idx5];
	u6 = W[idx6];
	u7 = W[idx7];
	u8 = W[idx8];

	wc[index_c] =
	  (w1*u1 + w2*u2 + w3*u3 + w4*u4 + w5*u5 + w6*u6 + w7*u7 + w8*u8)/sum_wi;
	
      }
    }
  }

  return;
}

/* COMPUTE_S_CG_EPS_3D */
/*****************************************************************************/
/**
  
  Computes the source term for the CG Low Re turbulence model. 
  This function does not discriminate if point is within a curve.

  @param data

  @return void.
    
  MODIFIED: 11-08-2019
*/
void compute_S_CG_eps_3D(Data_Mem *data){

  int I, J, K;
  int index_;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  
  double ske, skw, skn, sks, skt, skb;
  double dskdx, dskdy, dskdz, dsk2;
  double fi;
  double lClyw;

  double S11, S12;
  double S1, S2;

  double skP, skE, skW, skN, skS, skT, skB;

  const double rho = data->rho_v[0];
  const double mu = data->mu;

  double *S_CG_eps = data->S_CG_eps;

  const double *d2Uidxjdxk2 = data->d2Uidxjdxk2;

  const double *k = data->k_turb;
  
  const double *gamma = data->gamma;
  const double *mu_t = data->mu_turb;
  
  const double *f_mu = data->f_mu;
  const double *y_wall = data->y_wall;
  
  const double *fe = data->fe;
  const double *fw = data->fw;
  const double *fn = data->fn;
  const double *fs = data->fs;
  const double *ft = data->ft;
  const double *fb = data->fb;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_z = data->Delta_z;

#pragma omp parallel for default(none)		\
  private(I, J, K, index_, fi,			\
	  ske, skw, skn, sks, skt, skb,		\
	  skP, skE, skW, skN, skS, skT, skB,	\
	  dskdx, dskdy, dskdz, dsk2, lClyw,	\
	  S11, S12, S1, S2)			\
  shared(k, fe, fw, fn, fs, ft, fb,		\
	 Delta_x, Delta_y, Delta_z,		\
	 gamma, y_wall, mu_t,			\
	 d2Uidxjdxk2, f_mu, S_CG_eps)
  for (K=1; K<(Nz-1); K++){
    for (J=1; J<(Ny-1); J++){
      for (I=1; I<(Nx-1); I++){
      
	index_ = K*Ny*Nx + J*Nx + I;

	skP = sqrt(k[index_]);

	skE = sqrt(k[index_+1]);
	skW = sqrt(k[index_-1]);
	skN = sqrt(k[index_+Nx]);
	skS = sqrt(k[index_-Nx]);
	skT = sqrt(k[index_+Ny*Nx]);
	skB = sqrt(k[index_-Ny*Nx]);

	fi = fe[index_];
	ske = fi * skP + (1-fi) * skE;

	fi = fw[index_];
	skw = fi * skP + (1-fi) * skW;
      
	fi = fn[index_];
	skn = fi * skP + (1-fi) * skN;

	fi = fs[index_];
	sks = fi * skP + (1-fi) * skS;
      
	fi = ft[index_];
	skt = fi * skP + (1-fi) * skT;

	fi = fb[index_];
	skb = fi * skP + (1-fi) * skB;

	dskdx = (ske - skw)/Delta_x[index_];
	dskdy = (skn - sks)/Delta_y[index_];
	dskdz = (skt - skb)/Delta_z[index_];

	dsk2 = dskdx*dskdx + dskdy*dskdy + dskdz*dskdz;

	lClyw = (sqrt(k[index_])/gamma[index_]) / (2.44*y_wall[index_]);
      
	S11 = (2*mu*mu_t[index_]/rho) * d2Uidxjdxk2[index_];
	S12 =  2*mu * dsk2 * gamma[index_];
      
	S1 = 1.44 * (1 - f_mu[index_]) * ( S11 + S12 );
      
	S2 = MAX( 0.83 * k[index_] * gamma[index_]*gamma[index_] *
		  (lClyw-1) * lClyw*lClyw , 0 );
      
	S_CG_eps[index_] = S1 + S2;
     
      }
    }
  }
  
  return;
}

/* COMPUTE_TURB_PROP_3D */
/*****************************************************************************/
/**

   Computes intermediate fields and source terms for turbulent models.

   @param data

   @return void
  
   MODIFIED: 19-03-2020
*/
void compute_turb_prop_3D(Data_Mem *data){
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int turb_model = data->turb_model;

  const double alpha_P_k = data->alpha_P_k;
  const double alpha_S_CG_eps = data->alpha_S_CG_eps;

  double *P_k = data->P_k;
  double *P_k_lag = data->P_k_lag;

  double *S_CG_eps = data->S_CG_eps;
  double *S_CG_eps_lag = data->S_CG_eps_lag;

  /* Sets:
     u_at_faces, u_at_faces_y, u_at_faces_z
     v_at_faces, v_at_faces_x, v_at_faces_z
     w_at_faces, w_at_faces_x, w_at_faces_y

     Using:
     u, v, w, cut_cell variables
  */
  vel_at_crossed_faces_3D(data);
  
  /* Sets:
     nabla_u

     Using:
     u_at_faces, u_at_faces_y, u_at_faces_z
     v_at_faces, v_at_faces_x, v_at_faces_z
     w_at_faces, w_at_faces_x, w_at_faces_y
  */
  compute_nabla_u_3D(data);

  /* This is needed for the model CG */
  if(turb_model == 2){
    /* Sets:
       u_at_corners, v_at_corners, w_at_corners

       Using:
       u_at_faces, v_at_faces, w_at_faces
    */
    vel_at_corners_3D(data);
    
    compute_d2Uidxjdxk2_3D(data);
  }
  /* Sets:
     def_tens

     Using: 
     nabla_u
  */
  compute_def_tens_3D(data);

  /* Sets:
     tau_turb, gamma

     Using:
     def_tens
  */
  compute_tau_turb_3D(data);

  /* This is needed for the model CG */
  if(turb_model == 2){
    compute_S_CG_eps_3D(data);
    
    sum_relax_array(S_CG_eps, S_CG_eps_lag, S_CG_eps, alpha_S_CG_eps, Nx, Ny, Nz);
    copy_array(S_CG_eps, S_CG_eps_lag, Nx, Ny, Nz);
  }

  /* Sets:
     P_k

     Using:
     tau_turb, nabla_u
  */
  compute_P_k_3D(data);
  
  /* Relax P_k */
  sum_relax_array(P_k, P_k_lag, P_k, alpha_P_k, Nx, Ny, Nz);
  copy_array(P_k, P_k_lag, Nx, Ny, Nz);

  return;
}
