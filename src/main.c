#include "include/some_defs.h"
#include <getopt.h>

/* Variables, Flags and mutexes */
// Local animations
sig_atomic_t gr_flag;
pthread_cond_t gr_flag_cv;
pthread_mutex_t gr_mutex = PTHREAD_MUTEX_INITIALIZER;
// Handling SIGINT
sig_atomic_t action_on_signal_OK = 0;
// Wait for server to be launched
sig_atomic_t sgr_flag = 0;
pthread_cond_t sgr_flag_cv;
pthread_mutex_t sgr_mutex = PTHREAD_MUTEX_INITIALIZER;
// Update list of clients
sig_atomic_t update_clients_OK = 1;
pthread_cond_t update_cv;
pthread_mutex_t update_mutex = PTHREAD_MUTEX_INITIALIZER;
// Access server data (clients fds)
pthread_mutex_t sdata_mutex = PTHREAD_MUTEX_INITIALIZER;
// Timer flag
int timer_flag_OK = 0;
// Counters alloc
int count_double = 0;
int count_int = 0;
int count_char = 0;
// Variables for perp_dist.c
double Lx, Ly, Lz, cell_size, pol_tol;
// Measure performance
u_int64_t time_i, time_f, timer;

/* MAIN */
int main(int argc, char *argv[]){
  
  pthread_t thr_id;
  
  pthread_t server_thr_id;
  pthread_attr_t attr;
  
  char hostname[SIZE];
  char log_file[SIZE];
  char tmp_file[SIZE];
  char config_file[SIZE];
  char geom_file[SIZE];
  char sigmas_file[SIZE];
  char glob_res_file[SIZE];
  char msg[SIZE];
  char post_process_file[SIZE];
  char avg_iv_file[SIZE];
  char res_file[SIZE];
  char res_tr_file[SIZE];
  char time_string[40];

  const char *config_filename = "server.dat";
  const char *config_dir = "CONFIG/";
  const char *email_addr = NULL;
  const char *server_name = "localhost";
  const char *log_filename = "log.dat";
  const char *log_dir = "LOG/";
  const char *tmp_filename = "tmp.mat";
  const char *tmp_dir = "TMP/";
  const char *results_dir = NULL;
  const char *geom_filename = "geom.mat";
  const char *geom_dir = "GEOM/";
  const char *sigmas_filename = "sigmas.dat";
  const char *glob_res_filename = "glob_res.dat";
  const char *post_process_filename = NULL;
  const char *avg_iv_filename = "avg_inlet_vel.dat";
  const char *res_filename = "results.mat";
  
  int Nx = -1, Ny = -1, Nz = -1;
  int nx, ny, nz;
  int read_tmp_file_OK;
  int write_data_on_file_OK;
  int write_coeffs_OK;
  int graphs_OK;
  int animation_phi_OK;
  int animation_flow_OK;
  int launch_server_OK;

  int solve_3D_OK;
  
  int next_option = 0;
  int num_omp_threads = 4;
  int server_port = 4223;
  int timer_min = 30;

  int flow_scheme = -1;
  
  const int write_in_GPU_OK = 0;

  double kappa = -1;
  
  double *buffer = NULL;
   
  struct sigaction sa;
  struct itimerval stimer;
  /* To get a name for the transient file */
  struct timeval tv;
  // This pointer should not be freed!!
  struct tm *ptm = NULL;

  /* Master struct for data */
  Data_Mem data;
  Wins *wins = NULL;
  wins = &(data.wins);
  wins->flow_done_OK = &(data.flow_done_OK);
  wins->turb_model = &(data.turb_model);
  wins->svalues = data.sigmas.values;
  wins->iter = &(data.sigmas.it);
  wins->stride = &(data.res_sigma_iter);
  
  /* Pointer to flow solving function */
  void (*solve_flow)(Data_Mem *);

  /* Pointer to phi solver function */
  void (*solve_phi)(Data_Mem *);

  /* A string listing valid short options letters */
  const char* const short_options = "c:C:e:f:g:G:hk:l:L:m:n:o:p:r:R:t:T:w:x:y:z:";
  /* An array describing valid long options. */
  const struct option long_options[] = {
    {"config-file", 1, NULL, 'c'},
    {"config-dir", 1, NULL, 'C'},
    {"email", 1, NULL, 'e'},
    {"flow-scheme", 1, NULL, 'f'},
    {"geom-file", 1, NULL, 'g'},
    {"geom-dir", 1, NULL, 'G'},
    {"help", 0, NULL, 'h'},
    {"kappa", 1, NULL, 'k'},
    {"log-file", 1, NULL, 'l'},
    {"log-dir", 1, NULL, 'L'},
    {"name-server", 1, NULL, 'n'},
    {"omp-threads", 1, NULL, 'o'},
    {"port-server", 1, NULL, 'p'},
    {"results-file", 1, NULL, 'r'},
    {"results-dir", 1, NULL, 'R'},
    {"tmp-file", 1, NULL, 't'},
    {"tmp-dir", 1, NULL, 'T'},
    {"timer-min", 1, NULL, 'w'},
    {"nodes-x", 1, NULL, 'x'},
    {"nodes-y", 1, NULL, 'y'},
    {"nodes-z", 1, NULL, 'z'},
    {NULL, 0, NULL, 0}
  };

  /* We have not yet resolved the flow field
     This is an information for the server-client part */
  data.flow_done_OK = 0;
  /* This is information for the reading and writing of tmp files */
  data.centered_vel_comp_OK = 0;
  /* Flags for callings graphs on demand */
  data.peek_flow_OK = 0;
  data.peek_phi_OK = 0;

  /* Get the host name */
  if(gethostname(hostname, SIZE) == 0){
    server_name = hostname;
  }

  while(next_option != -1){
    next_option = getopt_long(argc, argv, short_options,
			      long_options, NULL);
    switch(next_option){

    case 'c':
      config_filename = optarg;
      break;

    case 'C':
      config_dir = optarg;
      break;

    case 'e':
      email_addr = optarg;
      break;

    case 'f':
      flow_scheme = atoi(optarg);
      break;
      
    case 'g':
      geom_filename = optarg;
      break;
      
    case 'G':
      geom_dir = optarg;
      break;
      
    case 'h':
      /* -h or --help */
      /* User has requested usage information. Print it to standard
	 output, and exit with exit code zero (normal termination). */
      print_usage(stdout, 0, argv[0]);
      break;

    case 'k':
      kappa = atof(optarg);
      break;
      
    case 'l':
      log_filename = optarg;
      break;

    case 'L':
      log_dir = optarg;
      break;

    case 'n':
      server_name = optarg;
      break;
      
    case 'o':
      num_omp_threads = atoi(optarg);
      break;

    case 'p':
      server_port = atoi(optarg);
      break;

    case 'r':
      res_filename = optarg;
      break;

    case 'R':
      results_dir = optarg;
      break;

    case 't':
      tmp_filename = optarg;
      break;

    case 'T':
      tmp_dir = optarg;
      break;

    case 'w':
      timer_min = atoi(optarg);
      break;

    case 'x':
      Nx = atoi(optarg);
      break;

    case 'y':
      Ny = atoi(optarg);
      break;

    case 'z':
      Nz = atoi(optarg);
      break;
      
    case '?':
      /* The user specified an invalid option */
      /* Print usage information to standard error, 
	 and exit with exit code one */
      print_usage(stderr, 1, argv[0]);
    case -1:
      break;
      /* Done with options */
    default:
      /* Something else: unexpected */
      exit(EXIT_FAILURE);
    }
  }

  /* Config filename setup */
  snprintf(config_file, SIZE, "%s%s", 
	   config_dir, config_filename);
  config_filename = config_file;

  /* TEXT PARSER AND SET UP VARIABLES */
  if(text_parser(&data, config_filename)){
    printf("Could not process CONFIG file\n");
    exit(EXIT_FAILURE);
  }

  solve_3D_OK = data.solve_3D_OK;

  /* Override values if set from CLI */
  if(flow_scheme > 0){
    data.flow_scheme = flow_scheme;
  }
  
  if(kappa > 0){
    data.kappa = kappa;
  }

  if(Nx > 0){
    data.x_side.N[0] = Nx;
    data.Nx = Nx;
    data.wins.Nx = Nx;
  }

  if(Ny > 0){
    data.y_side.N[0] = Ny;
    data.Ny = Ny;
    data.wins.Ny = Ny;
  }
  
  if(Nz > 0 && solve_3D_OK){
    data.z_side.N[0] = Nz;
    data.Nz = Nz;
    data.wins.Nz = Nz;
  }

  if(Nx > 0 || Ny > 0 || Nz >0){
    smooth_grid(&data);
  }
  
  if(solve_3D_OK){
    data.cell_size = MIN(MIN(data.Lx/(data.Nx-2), data.Ly/(data.Ny-2)), data.Lz/(data.Nz-2));
  }
  else{
    data.cell_size = MIN(data.Lx/(data.Nx-2), data.Ly/(data.Ny-2));
  }
  
  /* This data set as global variables is for perp_dist.c functions */
  Lx = data.Lx;
  Ly = data.Ly;
  Lz = data.Lz;
  cell_size = data.cell_size;
  pol_tol = data.pol_tol;

  /* Reading from MAT file superseeds other options */
  if(data.read_MAT_file_OK){
    data.read_tmp_file_OK = 0;
    /* Key to maintain solve_flow_OK flag in its value while reading data from MAT file */
    if(data.read_MAT_file_OK < 10){ 
	data.solve_flow_OK = 3;
      }
  }
  
  /* Initialize variable sized arrays' size to 0 */
  set_variable_arrays_default_size(&data);
  
  /* Initialize ncurses */
  wins->res_sigma_iter = data.res_sigma_iter;
  init_ncurses_processes(wins, config_filename);
  /* Print pendings TODOs  */
  print_pendings(wins);

  memset(msg, '\0', sizeof(msg));
  sprintf(msg, "Data read from %s", config_filename);
  info_msg(wins, msg);
  
  /* Setting up the data structure */  
  /* Checking (numerical) data consistency
     and overriding any possible set in the configuration file */
  if(num_omp_threads < 1){
    alert_msg(wins, "Setting num_omp_threads = 4");
    num_omp_threads = 4;
  }
  
  if(server_port < 1800 || server_port > 65530){
    alert_msg(wins, "Setting server_port = 4223");
    server_port = 4223;
  }
  
  if(timer_min < 1){
    alert_msg(wins, "No timer set");
    timer_min = 0;
  }

  /* Set filenames */
  /* Generate a MAT file for the transient results */

  /* Obtain the time of day, and convert it to a tm struct. */
  gettimeofday(&tv, NULL);
  ptm = localtime(&tv.tv_sec);
  /* Format the date and time, down to a single second. */
  strftime(time_string, sizeof(time_string),
	   "%d-%m-%Y_%H:%M:%S", ptm);
  
  if(results_dir == NULL && !write_in_GPU_OK){
    results_dir = "data/CPU/";
  }
  else if(results_dir == NULL && write_in_GPU_OK){
    results_dir = "data/GPU/";
  }

  /* Set post_process_filename filetype */

  if(solve_3D_OK){
    post_process_filename = "post_process.mat";
  }
  else{
    post_process_filename = "post_process.dat";
  }
    
  data.results_dir = results_dir;
  
  snprintf(sigmas_file, SIZE, "%s%s", 
	   results_dir, sigmas_filename);
  snprintf(glob_res_file, SIZE, "%s%s",
	   results_dir, glob_res_filename);
  snprintf(post_process_file, SIZE, "%s%s", 
	   results_dir, post_process_filename);
  snprintf(avg_iv_file, SIZE, "%s%s",
	   results_dir, avg_iv_filename);
  snprintf(res_file, SIZE, "%s%s",
	   results_dir, res_filename);
  snprintf(res_tr_file, SIZE, "%s%s.mat",
	   results_dir, time_string);
  
  sigmas_filename = sigmas_file;
  data.sigmas_filename = sigmas_filename;
  data.glob_res_filename = glob_res_file;
  data.post_process_filename = post_process_file;
  data.avg_iv_filename = avg_iv_file;
  data.res_filename = res_file;
  data.res_tr_filename = res_tr_file;

  snprintf(log_file, SIZE, "%s%s", 
	   log_dir, log_filename);
  
  log_filename = log_file;
  data.log_filename = log_filename;

  snprintf(tmp_file, SIZE, "%s%s", 
	   tmp_dir, tmp_filename);
  
  tmp_filename = tmp_file;
  data.tmp_filename = tmp_filename;

  snprintf(geom_file, SIZE, "%s%s", 
	   geom_dir, geom_filename);
  
  geom_filename = geom_file;
  data.geom_filename = geom_filename;

  data.num_omp_threads = num_omp_threads;
  data.sdata.port = server_port;
  data.sdata.server_name = server_name;

  write_data_on_file_OK = data.write_data_on_file_OK;
  read_tmp_file_OK = data.read_tmp_file_OK;
  write_coeffs_OK = data.write_coeffs_OK;

  /* SET NUMBER OF OMP THREADS */
  omp_set_num_threads(num_omp_threads);

  /* SET FLAG FOR TEG COMPUTATIONS */
  data.TEG_OK_summary = ymy_teg_phase(&(data.teg));
  
  if(timer_min){
    /* Install timer_handler as the signal handler for SIGVTALRM */
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &timer_handler;
    sigaction(SIGALRM, &sa, NULL);
    
    /* Configure the timer to expire after timer_min min... */
    stimer.it_value.tv_sec = (time_t) timer_min * 60;
    stimer.it_value.tv_usec = 0;
    /* ... and every timer_min min after that. */
    stimer.it_interval.tv_sec = (time_t) timer_min * 60;
    stimer.it_interval.tv_usec = 0;
  }
  
  /* Selection of solver */
  if(data.solve_3D_OK){
    if (num_omp_threads > 1){
      data.ADI_solver = ADI_omp_3D;
    }
    else{
      alert_msg(wins, "Using 3D sequential solver");
      data.ADI_solver = ADI_sequential_3D;
    }
  }
  else{
    if (num_omp_threads > 1){
      data.ADI_solver = ADI_omp_2D;
    }
    else{
      alert_msg(wins, "Using 2D sequential solver");
      data.ADI_solver = ADI_sequential_2D;
    }
  }
  
  graphs_OK = data.graphs_flow_OK || data.graphs_phi_OK;
  animation_phi_OK = data.animation_phi_OK;
  animation_flow_OK = data.animation_flow_OK;

  launch_server_OK = data.launch_server_OK;
      
  Nx = data.Nx;
  Ny = data.Ny;
  Nz = data.Nz;

  /* This is for memory requests of arrays nxnynz so that they not go to 0 */
  if(solve_3D_OK){
    /* 3D */
    nx = Nx-1; ny = Ny-1; nz = Nz-1;
  }
  else{
    /* 2D */
    nx = Nx-1; ny = Ny-1; nz = 1;
  }
  
  data.nx = nx;
  data.ny = ny;
  data.nz = nz;
  
  data_resize_for_server(&data);

  /* GET MEMORY FOR DATA STRUCT */
  get_memory(&data);
  
  /* READING FROM TMP FILE */
  if(read_tmp_file_OK){      
    read_from_tmp_file(&data);
  }

  /* Reading from MAT file */
  if(data.read_MAT_file_OK){
    read_from_MAT_file(&data);
  }
  
  /* REGISTERING FLOW SOLVER FUNCTION */
  if(data.flow_steady_state_OK){
    if(data.solve_3D_OK){
      solve_flow = solve_flow_SIMPLES_3D;
    }
    else{
      solve_flow = solve_flow_SIMPLES;
    }
  }
  else{
    if(data.solve_3D_OK){
      solve_flow = solve_flow_SIMPLES_3D_transient;
    }
    else{
      solve_flow = solve_flow_SIMPLES_transient;
    }
  }

  if(data.pv_coupling_algorithm == 0){
    data.flow_solver_name = "SIMPLE";
  }
  else if(data.pv_coupling_algorithm == 1){
    data.flow_solver_name = "SIMPLE_cang";
  }
  else if(data.pv_coupling_algorithm == 2){
    data.flow_solver_name = "SIMPLER";
  }
  else if(data.pv_coupling_algorithm == 3){
    data.flow_solver_name = "SIMPLEC";
  }
  else{
    alert_msg(wins, "Setting SIMPLE_cang as flow solver algorithm");
    data.pv_coupling_algorithm = 1;
    data.flow_solver_name = "SIMPLE_cang";
  }

  /* REGISTERING SIGNAL HANDLER FUNCTION */
  signal(SIGINT, signal_handler);
  
  /* Set thread attribute to detached */
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

  /* Launch animation window in a detached thread */
  if(animation_flow_OK){
    pthread_create(&thr_id, &attr, &anims, (void *) &data);
    /* Here put condition variable as restriction to go on */
    pthread_cond_wait(&gr_flag_cv, &gr_mutex);
  }

  /* GRID MAKER */
  grid_maker_non_uni(&data);

  /* CONSTRUCT QUADRICS */
  construct_quadrics(&data);

  /* CLASSIFY CELLS */
  classify_cells(&data);
  interface_cell_count(&data);
  
  /* PRESSURE INITIALIZATION */
  if(!read_tmp_file_OK && !data.read_MAT_file_OK){
    if(data.solve_3D_OK){
      make_p_init_profile_3D(&data);
    }
    else{
      make_p_init_profile(&data);
    }
    /* K, EPS, MU_T INITIALIZATION */
    if(data.turb_model){
      if(data.solve_3D_OK){
	set_turb_init_values_3D(&data);
      }
      else{
	set_turb_init_values(&data);
      }
    }
  }

  /* PHI INITIALIZATION */
  if(!read_tmp_file_OK && (data.read_MAT_file_OK != 2)){
    
    initialize_phi_in_domain(&data);
    
    if (data.solve_phase_change_OK){
      /* Liquid fraction initialization */
      if(solve_3D_OK){
	compute_gL_value_3D(&data, data.gL);
      }
      else{
	compute_gL_value(&data, data.gL);
      }
      copy_array(data.gL, data.gL_guess, Nx, Ny, Nz);
    }
  }
  copy_array(data.phi, data.phi0, Nx, Ny, Nz);
  /* Swap pointers to get animation working correctly 
     for steady state computations */
  buffer = data.phi0;
  data.phi0 = data.phi;
  data.phi = buffer;

  /* REGISTERING PHI SOLVER FUNCTION */
  if(data.steady_state_OK == 1){
    if(solve_3D_OK){
      solve_phi = solve_phi_steady_3D;
    }
    else{
      solve_phi = solve_phi_steady;
    }
  }
  else{
    if(solve_3D_OK){
      solve_phi = solve_phi_transient_3D;
    }
    else{
      solve_phi = solve_phi_transient;
    }
  }
  
  if(launch_server_OK){
    /* Launching server detached thread */
    pthread_create(&server_thr_id, &attr,
		   &server_thr_function, (void *) &data);
    /* Here put condition variable as restriction to go on */
    /* I will reuse it for the cordinations of childs processes */
    pthread_cond_wait(&sgr_flag_cv, &sgr_mutex);
  }

  if(timer_min){
    /* Start a real timer */
    setitimer(ITIMER_REAL, &stimer, NULL);
  }

  memset(msg, '\0', sizeof(msg));
  
  /* SOLVE FLOW FIELD */
  /* PHYSICAL PROPERTIES */
  fill_phys_prop(&data);

  /* Necessary for correctly compute velocities at faces for solve_phi */
  cut_cell_jobs(&data);
   
  switch(data.solve_flow_OK){

  case 0:
    snprintf(msg, SIZE, "Using flow field from %s", tmp_filename);
    info_msg(wins, msg);
    read_flow_field_from_tmp_file(&data);
    break;
    
  case 1:
    read_flow_field_from_tmp_file(&data);
    init_turb_lag_vars(&data);
    snprintf(msg, SIZE, 
	     "Computing flow field from %s", tmp_filename);
    info_msg(wins, msg);
    table_header_flow_it(msg, SIZE, wins);
    (*solve_flow)(&data);
    break;
    
  case 2:
    info_msg(wins, "Computing flow field");
    table_header_flow_it(msg, SIZE, wins);
    //    write_to_log_file(&data, "Starting flow solver");
    time_i = 0;
    time_f = 0;
    elapsed_time(&timer);
    (*solve_flow)(&data);
    time_i = time_f;
    time_f = time_f + elapsed_time(&timer);
    data.cputime_flow_solver = (time_f - time_i) / 1000000.0;
    // memset(msg, '\0', sizeof(msg));
    // sprintf(msg, "Finished cputime_flow_solver %f", data.cputime_flow_solver);
    // write_to_log_file(&data, msg);
    break;
    
  case 3:
    alert_msg(wins, "Skipping flow computation");
    break;
    
  default:
    alert_msg(wins, "solve_flow_OK flag value not valid, set to 2");
    info_msg(wins, "Computing flow field");
    table_header_flow_it(msg, SIZE, wins);    
    (*solve_flow)(&data);
  }
  
  /* Close flow animation window */
  if(animation_flow_OK){
    update_anims((void *) &data);
    if(!animation_phi_OK){
      pthread_join(thr_id, NULL);
    }
  }
  
  /* Launch animation if only phi animation selected */
  if(animation_phi_OK && !animation_flow_OK){
    pthread_create(&thr_id, &attr, &anims, (void *) &data);
    /* Here put condition variable as restriction to go on */
    pthread_cond_wait(&gr_flag_cv, &gr_mutex);
  }
  
  pthread_attr_destroy(&attr);

  /* Flow field resolved */
  data.flow_done_OK = 1;

  /* Prepares the flow field */
  if(data.solve_flow_OK != 3 && !data.coupled_physics_OK){

    /* This function modifies u, v and w, hence it is put after the flow was solved */
    /* Scale velocity values to p cells face centers */
    if(!data.centered_vel_comp_OK){
      info_msg(wins, "Centering flow field to be used in solve_phi");
           
      center_vel_components(&data);
      
    }
    else{
      info_msg(wins, "Flow field is centered in staggered cells");
    }
    
    if(data.solve_flow_OK != 0){
      write_to_tmp_file(&data);
    }
    
    compute_vel_at_main_cells(&data);
    
    info_msg(wins, "Flow field computed at main cells");
  }

  /* SOLVES PHI DISTRIBUTION */
  if(data.max_it > 0 && !data.coupled_physics_OK){
    // write_to_log_file(&data, "Starting phi solver...");

    data.dt[3] = data.dt[4]; // dt_min of phi
    
    time_i = 0;
    time_f = 0;
    elapsed_time(&timer);
    (*solve_phi)(&data);
    time_i = time_f;
    time_f = time_f + elapsed_time(&timer);
    data.cputime_phi_solver = (time_f - time_i) / 1000000.0;
    
    // memset(msg, '\0', sizeof(msg));
    // sprintf(msg, "Finished cputime_phi_solver %f", data.cputime_phi_solver);
    // write_to_log_file(&data, msg);
  }
  else{
    alert_msg(&(data.wins), "Not solving for phi (it <= 0)");
  }
    
  /* WRITE TO FILE */
  if(write_data_on_file_OK){
    write_data(&data);
  }
  if(write_coeffs_OK){
    write_coeffs(&data);
  }

  write_to_tmp_file(&data);
  
  checkCall(save_data_in_MAT_file(&data), wins);
  
  /* GRAPHICS */
  if(launch_server_OK){
    update_server(&data);
  }

  if(animation_phi_OK){
    update_anims((void *) &data);
    pthread_join(thr_id, NULL);
  }
  
  if(graphs_OK){
    /* Graphs modify some data so it must be the last function to call */
    graphs((void *) &data);
  }

#ifdef DISPLAY_OK
  wprintw(wins->msgs, "Program terminated succesfully!!...\n");
  wrefresh(wins->msgs);
#endif

  destroy_windows(wins);
  
  /* FREE MEMORY */
  free_memory(&data);

  /* Send through email the config file when computations are done */
  if(email_addr != NULL){
    send_email(email_addr, config_filename);
  }
  
  return 0;
}

/* SIGNAL_HANDLER */
void signal_handler(int signal_value){

  char msg1[] = "(0) continue, (1) continue and save TMP file, " 
    "(2) continue and save RESULTS file,";
  char msg2[] = "(3) exit, (4) exit and save TMP file, "
    "(5) declare convergence,";
  char msg3[] = "(6) change alphas, (7) change iterations, "
    "(8) Courant number (flow only),";
  char msg4[] = "(9) peek data, (10) change flow_scheme (laminar 2D/3D), ";
  char msg5[] = "(11) start coupled physics mode now: ";

#ifdef DISPLAY_OK
  /* Exit ncurses */
  endwin();
#endif
  
  printf("\n\n%85s %d\n",
	 "Received interrupt signal:", signal_value);
  
  printf("Code performance metrics will not be representative\n");

  printf("%85s\n%85s\n%85s\n%85s\n%85s", msg1, msg2, msg3, msg4, msg5);
  scanf("%d", &action_on_signal_OK);
    
  while((action_on_signal_OK != 0) && (action_on_signal_OK != 1) && 
	(action_on_signal_OK != 2) && (action_on_signal_OK != 3) && 
	(action_on_signal_OK != 4) && (action_on_signal_OK != 5) &&
	(action_on_signal_OK != 6) && (action_on_signal_OK != 7) &&
	(action_on_signal_OK != 8) && (action_on_signal_OK != 9) &&
	(action_on_signal_OK != 10) && (action_on_signal_OK != 11)){
    
    printf("%85s\n%85s\n%85s", msg1, msg2, msg3);
    scanf("%d", &action_on_signal_OK);
  }
  
  printf("\n");
  
  return;
}

/* TIMER_HANDLER */
void timer_handler (const int signum){

  /* Cheking this flag in an iterations cycle allows to write backup files 
     and the system log */
  timer_flag_OK = 1;
  
  return;
}

/* PRINT_USAGE */
void print_usage(FILE* stream, const int exit_code,
		 const char* program_name){
  fprintf(stream, "Usage: %s options [ arguments ... ]\n",
	  program_name);
  fprintf(stream, "Options (default values are in parenthesis):\n");
  fprintf(stream, 
	  " -c --config-file  Configure filename (server.dat)\n"
	  " -C --config-dir   Configure folder (./CONFIG)\n"
	  " -e --email        Send configure file to this email address (NULL)"
	  " when computations are done\n"
	  " -f --flow-scheme  Set flow scheme (-): upwind (0), QUICK (1), van Leer (2)\n"
	  " -g --geom-file    Geometry (ywall and cut_cell data) file name (geom.mat)\n"
	  " -G --geom-dir     Geometry folder (./GEOM)\n"
	  " -h --help         Display this usage information\n"
	  " -k --kappa        Set geometrical refinement parameter (-)\n"
	  " -l --log-file     Log filename (log.dat)\n"
	  " -L --log-dir      Log folder (./LOG)\n"
	  " -n --name-server  Server name (hostname -i)\n"	  
	  " -o --omp-threads  Number of omp threads (4)\n"
	  " -p --port-server  Server port (4223)\n"
	  " -r --results-file Results filename (results.mat)\n"
	  " -R --results-dir  Results folder (NULL),"
	  " if set to NULL writes to ./data/CPU || ./data/GPU\n"
	  " -t --tmp-file     Tmp filename (tmp.mat)\n"
	  " -T --tmp-dir      Tmp folder (./TMP)\n"
	  " -w --timer-min    Time in (min) to store results (30),"
	  " if set to 0 then no timer is set\n"
	  " -x --nodes-x      Set Nx for simple cases (-)\n"
	  " -y --nodes-y      Set Ny for simple cases (-)\n"
	  " -z --nodes-z      Set Nz for simple cases (-)\n"
	  );
  fprintf(stream, "Any option set overrides the specified in "
	  "--config-file=FILE\n");
  exit(exit_code);

  return;
}

/* PRINT_PENDINGS */
void print_pendings(Wins *wins){
  
  alert_msg(wins, "Interpolation of rho and cp between phases");
  alert_msg(wins, "must be weighted with volume fraction");
  alert_msg(wins, "Transient calculation only for SIMPLE");
  alert_msg(wins, "Race condition in displace_vel_nodes?");
  alert_msg(wins, "TEG is only for z = 0 for the moment");
  alert_msg(wins, "add uvw momentum source 3D function non-existent");
  alert_msg(wins, "QUICK and TVD for k-e non-existent");
  alert_msg(wins, "No coupled_physics for 3D");
  alert_msg(wins, "No TVD for phi 3D");
  
  return;
}
