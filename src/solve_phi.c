#include "include/some_defs.h"
extern sig_atomic_t action_on_signal_OK;
extern int timer_flag_OK;

/* SOLVE_PHI_JOBS */
double solve_phi_jobs(Data_Mem *data){
  
  double norm_phi = 0;
  
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int num_omp_threads = data->num_omp_threads;
  double *phi = data->phi;

  void (*ADI_solver)(double *, Coeffs *, const int, const int,
		     const int, ADI_Vars *) = data->ADI_solver;
  
  /* Setting boundary conditions */
  set_bc(data);
  
  /* Solving algebraic system */
  (*ADI_solver)(data->phi, &(data->coeffs_phi), Nx, Ny,
		num_omp_threads, &(data->ADI_vars));
  
  /* Computing residues */
  norm_phi =
    compute_residues(phi, &(data->coeffs_phi), Nx, Ny);
  
  return norm_phi;
}


/* COMPUTE_PHI_SC_AT_INTERFACE_ISOFLUX */
void compute_phi_SC_at_interface_Isoflux(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: compute_phi_SC_at_interface_Isoflux
    For Isoflux boundary condition
    Sets:
    Array (double, NxNy) SC_vol_m: source term due to Isoflux boundary

    RETURNS: ret_value not implemented.
  
    MODIFIED: 08-02-2019
  */
  
  int count, i, j, I, J, index_, index_u, index_v;
  int iter_count, iter_total = 10;
  int cell_done_OK = 0;

  const int nx = data->nx;
  const int Nx = data->Nx;
  
  const int Isoflux_boundary_cell_count =
    data->Isoflux_boundary_cell_count;

  const int Dirichlet_Isoflux_convective_boundary_OK = data->Dirichlet_Isoflux_convective_boundary_OK;

  const int *dom_mat = data->domain_matrix;
  
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  
  const int *I_Isoflux_v = data->I_Isoflux_v;
  const int *J_Isoflux_v = data->J_Isoflux_v;
  const int *Isoflux_boundary_indexs =
    data->Isoflux_boundary_indexs;
    
  double rho_cp, gamma;
  double dphidx_P, dphidx_W, dphidx_E, dphidx_S, dphidx_N;
  double dphidy_P, dphidy_W, dphidy_E, dphidy_S, dphidy_N;
  double dphidS_P, dphidS_W, dphidS_E, dphidS_S, dphidS_N;

  double dphidNb = 0;
  double phi_m_ew = 0, phi_m_ns = 0;
  double Few = 0, Fns = 0;
  double dphidSface_x, dphidSface_y;
    
  double Jxmol, Jxconv, Jx;
  double Jymol, Jyconv, Jy;

  /* Correction factors */
  double mx = 0, my = 0;
  double Da, eps_val;

  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;

  const double *fw = data->fw;
  const double *fs = data->fs;
  
  const double *fe = data->fe;
  const double *fn = data->fn;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;
  
  const double *phi = data->phi;

  const double *gamma_m = data->gamma_m;
  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;

  double *dphidxb = data->dphidxb;
  double *dphidyb = data->dphidyb;

  double *SC_vol_m = data->SC_vol_m;
  
  const double *dphidNb_v = data->dphidNb_v;

  const double *norm_vec_x_x = data->norm_vec_x_x;
  const double *norm_vec_y_y = data->norm_vec_y_y;
  const double *par_vec_x_x = data->par_vec_x_x;
  const double *par_vec_x_y = data->par_vec_x_y;
  const double *par_vec_y_x = data->par_vec_y_x;
  const double *par_vec_y_y = data->par_vec_y_y;
  
  for(iter_count=0; iter_count<iter_total; iter_count++){

    for(count=0; count<Isoflux_boundary_cell_count; count++){
      
      index_ = Isoflux_boundary_indexs[count];
      
      I = I_Isoflux_v[count];
      J = J_Isoflux_v[count];
      
      i = I-1;
      j = J-1;

      index_u = J*nx+i;
      index_v = j*Nx+I;
      
      cell_done_OK = 0;
      
      rho_cp = rho_m[index_] * cp_m[index_];
      gamma = gamma_m[index_];

      dphidx_P = 0;
      dphidx_W = 0;
      dphidx_E = 0;
      dphidx_S = 0;
      dphidx_N = 0;
      
      dphidy_P = 0;
      dphidy_W = 0;
      dphidy_E = 0;
      dphidy_S = 0;
      dphidy_N = 0;

      /* E-P line intersects with curve */
      if(!E_solve_phase_OK[index_] && !cell_done_OK){ // E

	cell_done_OK = 1;
	dphidx_P = __central_b_der(eps_x[index_], dphidxb[index_], phi[index_], phi[index_-1],
				   Delta_xE[index_], Delta_xW[index_]);

	dphidx_W = __central_der(phi[index_], phi[index_-1], phi[index_-2],
				 Delta_xE[index_-1], Delta_xW[index_-1]);

	/* Case E-N */
	if(!N_solve_phase_OK[index_]){ // N

	  /* Derivatives required for N case at South position:
	     dphidx_S, dphidy_S, dphidz_S */
	  if(!E_solve_phase_OK[index_-Nx]){ // SE
	      
	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx], phi[index_-Nx-1],
				       Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }
	  else if(!W_solve_phase_OK[index_-Nx]){ // SW

	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx+1], phi[index_-Nx],
				       Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	  }
	  else{
	      
	    dphidx_S = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1],
				     Delta_xE[index_-Nx],  Delta_xW[index_-Nx]);
	  }

	  dphidy_S = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx],
				   Delta_yN[index_-Nx], Delta_yS[index_-Nx]);
	}

	/* Case E-S */
	if(!S_solve_phase_OK[index_]){ // S

	  /* Derivatives required for S case at North position:
	     dphidx_N, dphidy_N, dphidz_N */	    
	  if(!E_solve_phase_OK[index_+Nx]){ // NE

	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx], phi[index_+Nx-1],
				       Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }
	  else if(!W_solve_phase_OK[index_+Nx]){ // NW

	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx+1], phi[index_+Nx],
				       Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	  }
	  else{
	    dphidx_N = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1],
				     Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }

	  dphidy_N = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_],
				   Delta_yN[index_+Nx], Delta_yS[index_+Nx]);
	}

	/* Derivatives required for E case */

	dphidy_P = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	dphidy_W = __central_der(phi[index_+Nx-1], phi[index_-1], phi[index_-Nx-1], Delta_yN[index_-1], Delta_yS[index_-1]);

	/* Check if dphidy_P or dphidy_W needs to be corrected */

	/* Case E-N */
	if(!N_solve_phase_OK[index_]){ // N
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_], phi[index_-Nx],
				     Delta_yN[index_], Delta_yS[index_]);
	}

	if(!N_solve_phase_OK[index_-1]){ // NW
	  dphidy_W = __central_b_der(eps_y[index_-1], dphidyb[index_-1], phi[index_-1], phi[index_-Nx-1],
				     Delta_yN[index_-1], Delta_yS[index_-1]);
	}

	/* Case E-S */
	if(!S_solve_phase_OK[index_]){ // S
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_+Nx], phi[index_],
				     Delta_yS[index_], Delta_yN[index_]);
	}

	if(!S_solve_phase_OK[index_-1]){ // SW
	  dphidy_W = __central_b_der(eps_y[index_-1], dphidyb[index_-1], phi[index_+Nx-1], phi[index_-1],
				     Delta_yS[index_-1], Delta_yN[index_-1]);
	}	
	
      } /* End case E-P */

      /*******************************************************************/	
      /* W-P line intersects with curve */
      if(!W_solve_phase_OK[index_] && !cell_done_OK){ // W

	cell_done_OK = 1;
	dphidx_P = __central_b_der(eps_x[index_], dphidxb[index_], phi[index_+1], phi[index_],
				   Delta_xW[index_], Delta_xE[index_]);

	dphidx_E = __central_der(phi[index_+2], phi[index_+1], phi[index_],
				 Delta_xE[index_+1], Delta_xW[index_+1]);

	/* Case W-N */
	if(!N_solve_phase_OK[index_]){ // N

	  /* Derivatives required for N case at South position:
	     dphidx_S, dphidy_S, dphidz_S */
	  if(!S_solve_phase_OK[index_+1]){ // SE

	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx], phi[index_-Nx-1],
				       Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }
	  else if(!S_solve_phase_OK[index_-1]){ // SW
	      
	    dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx+1], phi[index_-Nx],
				       Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	  }
	  else{

	    dphidx_S = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1],
				     Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	  }

	  dphidy_S = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx],
				   Delta_yN[index_-Nx], Delta_yS[index_-Nx]);
	}

	/* Case W-S */
	if(!S_solve_phase_OK[index_]){ // S
	    
	  /* Derivatives required for S case at North position:
	     dphidx_N, dphidy_N, dphidz_N */
	  if(!N_solve_phase_OK[index_+1]){ // NE

	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx], phi[index_+Nx-1],
				       Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }
	  else if(!N_solve_phase_OK[index_-1]){ // NW
	      
	    dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx+1], phi[index_+Nx],
				       Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	  }
	  else{
	      
	    dphidx_N = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1],
				     Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	  }

	  dphidy_N = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_],
				   Delta_yN[index_+Nx], Delta_yS[index_+Nx]);
	}

 
	/* Derivatives required for W case */
	dphidy_P = __central_der(phi[index_+Nx], phi[index_], phi[index_-Nx], Delta_yN[index_], Delta_yS[index_]);
	dphidy_E = __central_der(phi[index_+Nx+1], phi[index_+1], phi[index_-Nx+1], Delta_yN[index_+1], Delta_yS[index_+1]);

	/* Check if dphidy_P or dphidy_E needs to be corrected */

	/* Case W-N */
	if(!N_solve_phase_OK[index_]){ // N
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_], phi[index_-Nx],
				     Delta_yN[index_], Delta_yS[index_]);
	}

	if(!N_solve_phase_OK[index_+1]){ // NE
	  dphidy_E = __central_b_der(eps_y[index_+1], dphidyb[index_+1], phi[index_+1], phi[index_-Nx+1],
				     Delta_yN[index_+1], Delta_yS[index_+1]);
	}

	/* Case W-S */
	if(!S_solve_phase_OK[index_]){ // S
	  dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_+Nx], phi[index_],
				     Delta_yS[index_], Delta_yN[index_]);
	}

	if(!S_solve_phase_OK[index_+1]){ // SE
	  dphidy_E = __central_b_der(eps_y[index_+1], dphidyb[index_+1], phi[index_+Nx+1], phi[index_+1],
				     Delta_yS[index_+1], Delta_yN[index_+1]);
	}
      }/* End case W-P */

      /*******************************************************************/		
      /* N-P line intersects with curve */
      if(!N_solve_phase_OK[index_] && !cell_done_OK){ // N
	  
	cell_done_OK = 1;
	dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_], phi[index_-Nx],
				   Delta_yN[index_], Delta_yS[index_]);

	dphidy_S = __central_der(phi[index_], phi[index_-Nx], phi[index_-2*Nx],
				 Delta_yN[index_-Nx], Delta_yS[index_-Nx]);

	

	/* Derivatives required for N case */
	  
	dphidx_P = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
	dphidx_S = __central_der(phi[index_-Nx+1], phi[index_-Nx], phi[index_-Nx-1], Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	
	/* Check if dphidx_S needs to be corrected */
	if(!S_solve_phase_OK[index_+1]){ // SE
	  dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx], phi[index_-Nx-1],
				     Delta_xE[index_-Nx], Delta_xW[index_-Nx]);
	}

	if(!S_solve_phase_OK[index_-1]){ // SW
	  dphidx_S = __central_b_der(eps_x[index_-Nx], dphidxb[index_-Nx], phi[index_-Nx+1], phi[index_-Nx],
				     Delta_xW[index_-Nx], Delta_xE[index_-Nx]);
	}
		
      } /* End case N-P */

      /*******************************************************************/		
      /* S-P line intersects with curve */
      if(!S_solve_phase_OK[index_] && !cell_done_OK){ // S

	cell_done_OK = 1;
	dphidy_P = __central_b_der(eps_y[index_], dphidyb[index_], phi[index_+Nx], phi[index_],
				   Delta_yS[index_], Delta_yN[index_]);

	dphidy_N = __central_der(phi[index_+2*Nx], phi[index_+Nx], phi[index_],
				 Delta_yN[index_+Nx], Delta_yS[index_+Nx]);

	/* Derivatives required for S case */

	dphidx_P = __central_der(phi[index_+1], phi[index_], phi[index_-1], Delta_xE[index_], Delta_xW[index_]);
	dphidx_N = __central_der(phi[index_+Nx+1], phi[index_+Nx], phi[index_+Nx-1], Delta_xE[index_+Nx], Delta_xW[index_+Nx]);

	/* Check if dphidx_N needs to be corrected */
	if(!N_solve_phase_OK[index_+1]){ // NE
	  dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx], phi[index_+Nx-1],
				     Delta_xE[index_+Nx], Delta_xW[index_+Nx]);
	}

	if(!N_solve_phase_OK[index_-1]){ // NW
	  dphidx_N = __central_b_der(eps_x[index_+Nx], dphidxb[index_+Nx], phi[index_+Nx+1], phi[index_+Nx],
				     Delta_xW[index_+Nx], Delta_xE[index_+Nx]);
	}
      } /* End case S-P */
      
      /* Update the value of dphidxb */
      if(!W_solve_phase_OK[index_] || !E_solve_phase_OK[index_]){ // W || E

	/* Eq 27 Sato */
	dphidS_P = dphidx_P * par_vec_x_x[index_] + dphidy_P * par_vec_x_y[index_];
	  
	if(!W_solve_phase_OK[index_]){ // W

	  dphidNb = dphidNb_v[ dom_mat[index_-1] - 1 ];
	  
	  dphidS_E = dphidx_E * par_vec_x_x[index_] + dphidy_E * par_vec_x_y[index_];
    
	  /* Ec 28 Sato */
	  dphidSface_x =
	    __linear_extrap_to_b(dphidS_P, dphidS_E, eps_x[index_], Delta_xW[index_], Delta_xE[index_]);
	}
	else{

	  dphidNb = dphidNb_v[ dom_mat[index_+1] - 1 ];
	  
	  dphidS_W = dphidx_W * par_vec_x_x[index_] + dphidy_W * par_vec_x_y[index_];

	  /* Ec 28 Sato */
	  dphidSface_x =
	    __linear_extrap_to_b(dphidS_P, dphidS_W, eps_x[index_], Delta_xE[index_], Delta_xW[index_]);
	}
	/* Ec 24 Sato */
	dphidxb[index_] = dphidNb * norm_vec_x_x[index_] + dphidSface_x * par_vec_x_x[index_];
      }
	
      /* Update the value of dphidyb */
      if(!S_solve_phase_OK[index_] || !N_solve_phase_OK[index_]){ // S || N

	/* Eq 27 Sato */
	dphidS_P = dphidx_P * par_vec_y_x[index_] + dphidy_P * par_vec_y_y[index_];	

	if(!S_solve_phase_OK[index_]){ // S

	  dphidNb = dphidNb_v[ dom_mat[index_-Nx] - 1 ];
	  
	  dphidS_N = dphidx_N * par_vec_y_x[index_] + dphidy_N * par_vec_y_y[index_];
	  
	  /* Ec 28 Sato */
	  dphidSface_y =
	    __linear_extrap_to_b(dphidS_P, dphidS_N, eps_y[index_], Delta_yS[index_], Delta_yN[index_]);
	}
	else{

	  dphidNb = dphidNb_v[ dom_mat[index_+Nx] - 1 ];
	  
	  dphidS_S = dphidx_S * par_vec_y_x[index_] + dphidy_S * par_vec_y_y[index_];
	  
	  /* Ec 28 Sato */
	  dphidSface_y =
	    __linear_extrap_to_b(dphidS_P, dphidS_S, eps_y[index_], Delta_yN[index_], Delta_yS[index_]);
	}
	/* Ec 24 Sato */
	dphidyb[index_] = dphidNb * norm_vec_y_y[index_] + dphidSface_y * par_vec_y_y[index_];
      }

      /* Convective part at the face and correction with m factor */
      if(!W_solve_phase_OK[index_]){ // W

	eps_val = eps_x[index_];
	
	Da = (1 - fe[index_])*Delta_xE[index_] + eps_val*Delta_xW[index_];
	
	mx = Delta_x[index_] / Da;
	
	Few = rho_cp * u[index_u];
	phi_m_ew = phi[index_] - 0.5 * Delta_x[index_] * dphidxb[index_];
      }

      if(!E_solve_phase_OK[index_]){ // E

	eps_val = eps_x[index_];
	
	Da = (1 - fw[index_])*Delta_xW[index_] + eps_val*Delta_xE[index_];
	
	mx = Delta_x[index_] / Da;
	
	Few = rho_cp * u[index_u+1];
	phi_m_ew = phi[index_] + 0.5 * Delta_x[index_] * dphidxb[index_];
      }

      if(!S_solve_phase_OK[index_]){ // S
	
	eps_val = eps_y[index_];
	
	Da = (1 - fn[index_])*Delta_yN[index_] + eps_val*Delta_yS[index_];
	
	my = Delta_y[index_] / Da;
	
	Fns = rho_cp * v[index_v];
	phi_m_ns = phi[index_] - 0.5 * Delta_y[index_] * dphidyb[index_];
      }

      if(!N_solve_phase_OK[index_]){ // N

	eps_val = eps_y[index_];
	
	Da = (1 - fs[index_])*Delta_yS[index_] + eps_val*Delta_yN[index_];
	
	my = Delta_y[index_] / Da;
	
	Fns = rho_cp * v[index_v+Nx];
	phi_m_ns = phi[index_] + 0.5 * Delta_y[index_] * dphidyb[index_];
      }

      /* Build the combined fluxes. It must be noted that there are different lengths
	 for the molecular and convective parts which are taken into account by 
	 the eps factors */
      Jxmol = - gamma * dphidxb[index_] * mx;
      Jxconv = Few * phi_m_ew;
      Jxconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jxconv : 0;
      
      Jymol = - gamma * dphidyb[index_] * my;
      Jyconv = Fns * phi_m_ns;
      Jyconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jyconv : 0;

      Jx = Jxmol + Jxconv;
      Jy = Jymol + Jyconv;
      
      SC_vol_m[index_] =
	(-1) * (1 - E_solve_phase_OK[index_]) * Jx * Delta_y[index_] +
	(-1) * (1 - N_solve_phase_OK[index_]) * Jy * Delta_x[index_] +
	(1 - W_solve_phase_OK[index_]) * Jx * Delta_y[index_] + 
	(1 - S_solve_phase_OK[index_]) * Jy * Delta_x[index_];
     
    } /* end for(count=0; count<DI_boundary_cell_count; count++) */
  }

  return;
}

/* COMPUTE_PHI_SC_AT_INTERFACE_DIRICHLET */
/*****************************************************************************/
/**

   For Dirichlet boundary condition

   Sets:

   Array (double, NxNy) Data_Mem::SC_vol_m: source term due to Dirichlet boundary.

   @param data

   @return void
  
   MODIFIED: 17-07-2020
*/
void compute_phi_SC_at_interface_Dirichlet(Data_Mem *data){
	
  int count, i, j, I, J, index_, index_u, index_v;
  const int nx = data->nx;
  const int Nx = data->Nx;
  
  const int Dirichlet_boundary_cell_count =
    data->Dirichlet_boundary_cell_count;

  const int Dirichlet_Isoflux_convective_boundary_OK = data->Dirichlet_Isoflux_convective_boundary_OK;
  
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  
  const int *dom_mat = data->domain_matrix;

  const int *I_Dirichlet_v = data->I_Dirichlet_v;
  const int *J_Dirichlet_v = data->J_Dirichlet_v;
  const int *Dirichlet_boundary_indexs =
    data->Dirichlet_boundary_indexs;

  /* phi at boundary and midpoint */
  double phi_b, phi_m;
  double dphidxb_, dphidyb_;

  double Fe, Fw, Fn, Fs;

  double m, Da, eps_val;

  double Jxmol, Jxconv, Jx;
  double Jymol, Jyconv, Jy;

  const double eps_dis_tol = data->eps_dist_tol;

  double *SC_vol_m = data->SC_vol_m;

  double *dphidxb = data->dphidxb;
  double *dphidyb = data->dphidyb;
  
  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;

  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;

  const double *fw = data->fw;
  const double *fs = data->fs;

  const double *fe = data->fe;
  const double *fn = data->fn;

  const double *u = data->u_at_faces;
  const double *v = data->v_at_faces;

  const double *phi = data->phi;
  const double *phi_b_v = data->phi_b_v;

  const double *gamma_m = data->gamma_m;
  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;

  
  for(count=0; count<Dirichlet_boundary_cell_count; count++){
      
    index_ = Dirichlet_boundary_indexs[count];
      
    I = I_Dirichlet_v[count];
    J = J_Dirichlet_v[count];

    i = I-1;
    j = J-1;

    index_u = J*nx+i;
    index_v = j*Nx+I;

    SC_vol_m[index_] = 0;

    Jxmol = 0;
    Jxconv = 0;
    Jx = 0;

    Jymol = 0;
    Jyconv = 0;
    Jy = 0;

    if(!W_solve_phase_OK[index_]){
      eps_val  = eps_x[index_];

      Da = (1 - fe[index_])*Delta_xE[index_] + eps_val*Delta_xW[index_];
      
      m = Delta_x[index_] / Da;
      /* Convective parameter */
      Fw = rho_m[index_] * cp_m[index_] * u[index_u];
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_-1] - 1 ];
      
      /* Get dphidxb for case W (-+-) : dir_fact == -1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidxb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_+1],
			       Delta_xW[index_], Delta_xE[index_], -1);

      dphidxb[index_] = dphidxb_;

      Jxmol = - gamma_m[index_] * dphidxb_ * Delta_y[index_] * m;
      Jxconv = Fw * phi_m * Delta_y[index_];
      Jxconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jxconv : 0;
      
      Jx = Jxmol + Jxconv;
      
      /* Contribution to SC_vol_m (W: -+) */
      // SC_vol_m[index_] += (-1) * gamma_m[index_] * dphidxb_ * Delta_y[index_] * m + Fw * phi_m * Delta_y[index_];
      SC_vol_m[index_] += Jx;

    }

    if(!E_solve_phase_OK[index_]){
      eps_val = eps_x[index_];

      Da = (1 - fw[index_])*Delta_xW[index_] + eps_val*Delta_xE[index_];
      
      m = Delta_x[index_] / Da;
      /* Convective parameter */
      Fe = rho_m[index_] * cp_m[index_] * u[index_u+1];
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_+1] - 1 ];
      /* Get dphidxb for case E (+-+) : dir_fact == +1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidxb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_-1],
			       Delta_xE[index_], Delta_xW[index_], 1);

      dphidxb[index_] = dphidxb_;

      Jxmol = - gamma_m[index_] * dphidxb_ * Delta_y[index_] * m;
      Jxconv = Fe * phi_m * Delta_y[index_];
      Jxconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jxconv : 0;
      
      Jx = Jxmol + Jxconv;
      
      /* Contribution to SC_vol_m (E: +-) */
      // SC_vol_m[index_] += gamma_m[index_] * dphidxb_ * Delta_y[index_] * m - Fe * phi_m * Delta_y[index_];
      SC_vol_m[index_] += - Jx;
    }

    if(!S_solve_phase_OK[index_]){
      eps_val = eps_y[index_];

      Da = (1 - fn[index_])*Delta_yN[index_] + eps_val*Delta_yS[index_];
      
      m = Delta_y[index_] / Da;
      /* Convective parameter */
      Fs = rho_m[index_] * cp_m[index_] * v[index_v];
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_-Nx] - 1 ];

      /* Get dphidyb for case S (-+-) : dir_fact == -1 
	 and phi_m */
      __get_b_values_Dirichlet(&dphidyb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_+Nx],
			       Delta_yS[index_], Delta_yN[index_], -1);

      dphidyb[index_] = dphidyb_;

      Jymol = - gamma_m[index_] * dphidyb_ * Delta_x[index_] * m;
      Jyconv = Fs * phi_m * Delta_x[index_];
      Jyconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jyconv : 0;
      
      Jy = Jymol + Jyconv;

      /* Contribution to SC_vol_m (S: -+) */
      // SC_vol_m[index_] += (-1) * gamma_m[index_] * dphidyb_ * Delta_x[index_] * m + Fs * phi_m * Delta_x[index_];

      SC_vol_m[index_] += Jy;
    }

    if(!N_solve_phase_OK[index_]){
      eps_val = eps_y[index_];

      Da = (1 - fs[index_])*Delta_yS[index_] + eps_val*Delta_yN[index_];
      
      m = Delta_y[index_] / Da;
      /* Convective parameter */
      Fn = rho_m[index_] * cp_m[index_] * v[index_v+Nx];
      /* Extracting phi_b */
      phi_b = phi_b_v[ dom_mat[index_+Nx] - 1 ];

      /* Get dphidyb for case N (+-+) : dir_fact == +1
	 and phi_m */
      __get_b_values_Dirichlet(&dphidyb_, &phi_m,
			       eps_val, eps_dis_tol,
			       phi_b, phi[index_], phi[index_-Nx],
			       Delta_yN[index_], Delta_yS[index_], 1);

      dphidyb[index_] = dphidyb_;

      Jymol = - gamma_m[index_] * dphidyb_ * Delta_x[index_] * m;
      Jyconv = Fn * phi_m * Delta_x[index_];
      Jyconv = (Dirichlet_Isoflux_convective_boundary_OK) ? Jyconv : 0;
      
      Jy = Jymol + Jyconv;
      
      /* Contribution to SC_vol_m (N: +-) */
      // SC_vol_m[index_] += gamma_m[index_] * dphidyb_ * Delta_x[index_] * m - Fn * phi_m * Delta_x[index_];
      SC_vol_m[index_] += - Jy;
    }
      
  }

  return;
}

/* COMPUTE_PHI_SC_AT_INTERFACE */
void compute_phi_SC_at_interface(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: compute_phi_SC_at_interface
    Sets:
    Array (double, NxNy) SC_vol_m: source term on energy equation
    For conjugate heat transfer:
    - Diffusive part is calculated on function compute_near_boundary_molecular_flux_*
    - Convective part is calculated on function compute_near_boundary_convective_flux
    For Isoflux boundary condition:
    - Diffusive and convective parts are calculated on function compute_phi_SC_at_interface_Isoflux.
    For Dirichlet boundary condition:
    - Diffusive and convective parts are calculated on function compute_phi_SC_at_interface_Dirichlet.

    RETURNS: ret_value not implemented.
  
    MODIFIED: 23-05-2019
  */
  /*****************************************************************************/

  int count, index_;

  const int interface_molecular_flux_model =
    data->interface_molecular_flux_model;
  const int Dirichlet_boundary_cell_count =
    data->Dirichlet_boundary_cell_count;
  const int Isoflux_boundary_cell_count =
    data->Isoflux_boundary_cell_count;
  const int Conjugate_boundary_cell_count =
    data->Conjugate_boundary_cell_count;
  
  const int *E_is_fluid_OK = data->E_is_fluid_OK;
  const int *W_is_fluid_OK = data->W_is_fluid_OK;
  const int *N_is_fluid_OK = data->N_is_fluid_OK;
  const int *S_is_fluid_OK = data->S_is_fluid_OK;
  
  const int *dom_mat = data->domain_matrix;
  
  const int *Conjugate_boundary_indexs =
    data->Conjugate_boundary_indexs;
    
  const double alpha_SC_phi = data->alpha_SC_phi;

  double *SC_vol_m_0 = data->SC_vol_m_0;
  double *SC_vol_m = data->SC_vol_m;
  
  const double *J_x = data->J_x;
  const double *J_y = data->J_y;

  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  
  /* Update fluxes at interface 
     in case of conjugate heat transfer */
  /* Only valid for fluid and solid interfaces as of yet */

  if(Conjugate_boundary_cell_count && (interface_molecular_flux_model < 6)){

    switch(interface_molecular_flux_model){
    case 1: compute_near_boundary_molecular_flux_Tsutsumi(data);
      break;
      
    case 2: compute_near_boundary_molecular_flux_Tsutsumi_def(data);
      break;
      
    case 3: compute_near_boundary_molecular_flux_Sato(data);
      break;
      
    case 4: compute_near_boundary_molecular_flux_Sato_at_face(data);
      break;
      
    case 5: compute_near_boundary_molecular_flux_experimental(data);
      break;
    }
   
    compute_near_boundary_convective_flux(data);
    
    for(count=0; count<Conjugate_boundary_cell_count; count++){
        
      index_ = Conjugate_boundary_indexs[count];
	  
      if (dom_mat[index_] != 0){
	/* Solid phase */
	SC_vol_m[index_] = 
	  (-1) * E_is_fluid_OK[index_] * J_x[index_] * Delta_y[index_] +
	  (-1) * N_is_fluid_OK[index_] * J_y[index_] * Delta_x[index_] +
	  W_is_fluid_OK[index_] * J_x[index_] * Delta_y[index_] + 
	  S_is_fluid_OK[index_] * J_y[index_] * Delta_x[index_];
	    
	SC_vol_m[index_]  = alpha_SC_phi * SC_vol_m[index_] +
	  (1 - alpha_SC_phi) * SC_vol_m_0[index_];
	
      }
      else{
	/* Fluid phase */
	SC_vol_m[index_] = 
	  (-1) * (1 - E_is_fluid_OK[index_]) *
	  J_x[index_] * Delta_y[index_] +
	  (-1) * (1 - N_is_fluid_OK[index_]) *
	  J_y[index_] * Delta_x[index_] +
	      
	  (1 - W_is_fluid_OK[index_]) *
	  J_x[index_] * Delta_y[index_] + 
	  (1 - S_is_fluid_OK[index_]) *
	  J_y[index_] * Delta_x[index_];
	    
	SC_vol_m[index_]  = alpha_SC_phi * SC_vol_m[index_] +
	  (1 - alpha_SC_phi) * SC_vol_m_0[index_];

      }
    }
  } /* end if(Conjugate_boundary_cell_count) */
  
  /* Dirichlet and Isoflux cases include 
     the convection component and write directly into SC_vol */
  if(Dirichlet_boundary_cell_count){
    /* Now the Dirichlet case */
    compute_phi_SC_at_interface_Dirichlet(data);
  }
  if(Isoflux_boundary_cell_count){
    /* Now the Isoflux case */
    compute_phi_SC_at_interface_Isoflux(data);
  }
 
  return;
}

/* SET_BC */
void set_bc(Data_Mem *data){

  int I, J, index_;
  int count_seg;
 
  const int Nx = data->Nx;
  const int Ny = data->Ny;

  const int *dom_mat = data->domain_matrix;
  const int *P_solve_phase_OK = data->P_solve_phase_OK;

  const double Lx = data->Lx;
  const double Ly = data->Ly;
  
  double *aE = data->coeffs_phi.aE;
  double *aW = data->coeffs_phi.aW;
  double *aN = data->coeffs_phi.aN;
  double *aS = data->coeffs_phi.aS;
  double *aP = data->coeffs_phi.aP;
  double *b = data->coeffs_phi.b;
  
  const double *nodes_x = data->nodes_x;
  const double *nodes_y = data->nodes_y;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yN = data->Delta_yN;
  const double *Delta_yS = data->Delta_yS;
  
  const double *gamma_m = data->gamma_m;

  const double *phi0_v = data->phi0_v;
  
  const double *per = NULL;
  const int *alpha = NULL;
  const double *beta = NULL;
  const double *gamma = NULL;
  const double *phi_b = NULL;
  
  /* WEST */
  per = data->west.per;
  alpha = data->west.alpha;
  beta = data->west.beta;
  gamma = data->west.gamma;
  phi_b = data->west.phi_b;

  count_seg = 0;
  I = 0;

  for (J=1; J<Ny-1; J++){
      
    index_ = J*Nx + I;
    
    if(nodes_y[index_] <= per[count_seg] * Ly){
      if(P_solve_phase_OK[index_]){
	aP[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_xE[index_] +
	  beta[count_seg];
	aE[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_xE[index_];
	b[index_] =
	  beta[count_seg] * phi_b[count_seg] + gamma[count_seg];
      }
      else{
	aP[index_] = 1;
	b[index_] = phi0_v[ dom_mat[index_] ];
      }
    }
    else {
      J--;
      count_seg++;
    }
  }
  
  /* EAST */
  per = data->east.per;
  alpha = data->east.alpha;
  beta = data->east.beta;
  gamma = data->east.gamma;
  phi_b = data->east.phi_b;

  count_seg = 0;
  I = Nx-1;

  for (J=1; J<Ny-1; J++){
      
    index_ = J*Nx + I;
    
    if(nodes_y[index_] <= per[count_seg] * Ly){
      if(P_solve_phase_OK[index_]){
	aP[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_xW[index_] +
	  beta[count_seg];
	aW[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_xW[index_];
	b[index_] =
	  beta[count_seg] * phi_b[count_seg] - gamma[count_seg];
      }
      else{
	aP[index_] = 1;
	b[index_] = phi0_v[ dom_mat[index_] ];
      }
    }
    else {
      J--;
      count_seg++;
    }    
  }

  /* SOUTH */
  per = data->south.per;
  alpha = data->south.alpha;
  beta = data->south.beta;
  gamma = data->south.gamma;
  phi_b = data->south.phi_b;

  count_seg = 0;
  J = 0;

  for (I=1; I<Nx-1; I++){
        
    index_ = Nx*J+I;
      
    if(nodes_x[index_] <= per[count_seg] * Lx){
      if(P_solve_phase_OK[index_]){
	aP[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_yN[index_] +
	  beta[count_seg];
	aN[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_yN[index_];
	b[index_] =
	  beta[count_seg] * phi_b[count_seg] + gamma[count_seg];
      }
      else{
	aP[index_] = 1;
	b[index_] = phi0_v[ dom_mat[index_] ];
      }
    }
    else {
      I--;
      count_seg++;
    }    
      
  }	
  
  /* NORTH */
  per = data->north.per;
  alpha = data->north.alpha;
  beta = data->north.beta;
  gamma = data->north.gamma;
  phi_b = data->north.phi_b;

  count_seg = 0;
  J = Ny-1;

  for (I=1; I<Nx-1 ; I++){
      
    index_ = Nx*J+I;
        
    if(nodes_x[index_] <= per[count_seg] * Lx){
      if(P_solve_phase_OK[index_]){
	aP[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_yS[index_] +
	  beta[count_seg];
	aS[index_] =
	  alpha[count_seg] * gamma_m[index_] / Delta_yS[index_];
	b[index_] =
	  beta[count_seg] * phi_b[count_seg] - gamma[count_seg];
      }
      else{
	aP[index_] = 1;
	b[index_] = phi0_v[ dom_mat[index_] ];
      }
    }
    else {
      I--;
      count_seg++;
    }

  }
  
  return;
}

/* COEFFS_PHI_POWER_LAW */
void coeffs_phi_power_law(Data_Mem *data){
  /*****************************************************************************/
  /*
    FUNCTION: coeffs_phi
    Sets:
    Structure coeffs_phi: coefficients of discretized equation for internal nodes

    RETURNS: ret_value not implemented.
  
    MODIFIED: 18-02-2019
  */
  /*****************************************************************************/

  int I, J, index_;
  int i, j, index_u, index_v;
    
  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;

  const int phi_dF_OK = data->phi_dF_OK;
  
  const int steady_state_OK = data->steady_state_OK;
  const int interface_molecular_flux_model = data->interface_molecular_flux_model;

  const int *P_solve_phase_OK = data->P_solve_phase_OK;
  const int *E_solve_phase_OK = data->E_solve_phase_OK;
  const int *W_solve_phase_OK = data->W_solve_phase_OK;
  const int *N_solve_phase_OK = data->N_solve_phase_OK;
  const int *S_solve_phase_OK = data->S_solve_phase_OK;
  const int *W_coeff = data->W_coeff;
  const int *E_coeff = data->E_coeff;
  const int *S_coeff = data->S_coeff;
  const int *N_coeff = data->N_coeff;

  const int *dom_mat = data->domain_matrix;
  const int *curve_phase_change_OK = data->curve_phase_change_OK;
    
  double De, Dw, Ds, Dn;
  double Fe, Fw, Fs, Fn;
  double Pe, Pw, Ps, Pn, PL_arg;
  double SP = 0, SC = 0, SPC = 0;
  
  double aP0 = 0;
  double dF = 0;

  double m, Da, eps_val;

  const double dt = data->dt[3] / 2; // For 2D case

  double *aE = data->coeffs_phi.aE;
  double *aW = data->coeffs_phi.aW;
  double *aN = data->coeffs_phi.aN;
  double *aS = data->coeffs_phi.aS;
  double *aP = data->coeffs_phi.aP;
  double *b = data->coeffs_phi.b;
  
  const double *phi0_v = data->phi0_v;
  const double *SC_vol_m = data->SC_vol_m;
  const double *phi0 = data->phi0;
  const double *cp_m = data->cp_m;
  const double *rho_m = data->rho_m;
  const double *enthalpy_diff = data->enthalpy_diff;
  const double *gL_guess = data->gL_guess;
  const double *gL = data->gL;
  const double *phase_change_vol = data->phase_change_vol;
  const double *vol = data->vol_uncut;
  const double *Dphi_x = data->Dphi_x;
  const double *Dphi_y = data->Dphi_y;
  const double *Fphi_x = data->Fphi_x;
  const double *Fphi_y = data->Fphi_y;

  const double *eps_x = data->epsilon_x;
  const double *eps_y = data->epsilon_y;
  
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;

  const double *Delta_xW = data->Delta_xW;
  const double *Delta_yS = data->Delta_yS;

  const double *fw = data->fw;
  const double *fs = data->fs;
  
  const double *fe = data->fe;
  const double *fn = data->fn;

  set_D_phi(data);
  set_F_phi(data);

  /* Coefficients computation */
  
#pragma omp parallel for default(none)					\
  private(i, j, I, J, index_, index_u, index_v, SC, SPC, SP,		\
	  m, Da, eps_val, dF,						\
	  aP0, De, Dw, Dn, Ds, Fe, Fw, Fn, Fs, Pe, Pw, Pn, Ps, PL_arg)	\
  shared(dom_mat, E_solve_phase_OK, W_solve_phase_OK,			\
	 N_solve_phase_OK, S_solve_phase_OK, eps_x, eps_y,		\
	 P_solve_phase_OK, aP, aE, aW, aN, aS, b,			\
	 curve_phase_change_OK,						\
	 enthalpy_diff, gL, gL_guess, phase_change_vol,			\
	 SC_vol_m, phi0, vol, Dphi_x, Dphi_y, Fphi_x, Fphi_y,		\
	 cp_m, rho_m, phi0_v, E_coeff, W_coeff, N_coeff, S_coeff,	\
	 Delta_x, Delta_y, fe, fw, fn, fs,				\
	 Delta_xE, Delta_xW, Delta_yN, Delta_yS)
	 

  for(J=1; J<Ny-1; J++){
    for(I=1; I<Nx-1; I++){

      i = I-1;
      j = J-1;

      index_ = J*Nx + I;
      index_u = J*nx + i;
      index_v = j*Nx + I;

      SC = 0;
      SPC = 0;
      SP = 0;

      if(steady_state_OK){
	aP0 = 0;
      }
      else{
	aP0 = rho_m[index_] * cp_m[index_] * vol[index_] / dt;
      }

      /* Diffusive */
      De = Dphi_x[index_u+1]  * E_coeff[index_];
      Dw = Dphi_x[index_u]    * W_coeff[index_];
      Dn = Dphi_y[index_v+Nx] * N_coeff[index_];
      Ds = Dphi_y[index_v]    * S_coeff[index_];

      /* Corrections to parameters 
	 in account for Dirichlet or Isoflux effects */
	if(!E_solve_phase_OK[index_]){
	  
	  eps_val = eps_x[index_];
	  
	  Da = (1 - fw[index_])*Delta_xW[index_] + eps_val*Delta_xE[index_];
	  
	  m = Delta_x[index_] / Da;
	  
	  De = De * m;
	  Dw = Dw * m;
	}
	if(!W_solve_phase_OK[index_]){

	  eps_val = eps_x[index_];
	  
	  Da = (1 - fe[index_])*Delta_xE[index_] + eps_val*Delta_xW[index_];
	  
	  m = Delta_x[index_] / Da;
	  
	  De = De * m;
	  Dw = Dw * m;
	}
		
	if(!N_solve_phase_OK[index_]){

	  eps_val = eps_y[index_];
      
	  Da = (1 - fs[index_])*Delta_yS[index_] + eps_val*Delta_yN[index_];
      
	  m = Delta_y[index_] / Da;
      
	  Dn = Dn * m;
	  Ds = Ds * m;
	}

	if(!S_solve_phase_OK[index_]){
	  
	  eps_val = eps_y[index_];
      
	  Da = (1 - fn[index_])*Delta_yN[index_] + eps_val*Delta_yS[index_];
      
	  m = Delta_y[index_] / Da;
      
	  Dn = Dn * m;
	  Ds = Ds * m;
	}
	
      /* Convective */
      Fe = Fphi_x[index_u+1]  * E_coeff[index_];
      Fw = Fphi_x[index_u]    * W_coeff[index_];
      Fn = Fphi_y[index_v+Nx] * N_coeff[index_];
      Fs = Fphi_y[index_v]    * S_coeff[index_];
	
      if(!P_solve_phase_OK[index_]){
	/* We are "not solving" this phase */
	aP[index_] = 1;
	aE[index_] = 0;
	aW[index_] = 0;
	aN[index_] = 0;
	aS[index_] = 0;
	b[index_] = phi0_v[ dom_mat[index_] ];
      }
      else{
	/* This phase must be addressed */
	if(dom_mat[index_] != 0){
	  /* The solid (no convection) case */
	  aE[index_] = De * E_solve_phase_OK[index_] * E_coeff[index_];
	  aW[index_] = Dw * W_solve_phase_OK[index_] * W_coeff[index_];
	  aN[index_] = Dn * N_solve_phase_OK[index_] * N_coeff[index_];
	  aS[index_] = Ds * S_solve_phase_OK[index_] * S_coeff[index_];

	  if((curve_phase_change_OK[dom_mat[index_]-1]) &&
	     !steady_state_OK){
	    /* EC 16 Voller  */
	    SPC = enthalpy_diff[index_] * 
	      (gL[index_] - gL_guess[index_]) *
	      phase_change_vol[index_] / dt;
	  }
	}
	else{
	  /* The fluid (convection) case */
	  /* Peclet number */
	  Pe = Fe/De; Pw = Fw/Dw; Pn = Fn/Dn; Ps = Fs/Ds;
	  /* Power law */
	  PL_arg = powf(1.0 - 0.1 * fabs(Pe), 5.0);
	  aE[index_] = E_solve_phase_OK[index_] * E_coeff[index_] * De *
	    ( MAX(PL_arg, 0) + MAX(-Pe, 0) );

	  PL_arg = powf(1.0 - 0.1 * fabs(Pw), 5.0);
	  aW[index_] = W_solve_phase_OK[index_] * W_coeff[index_] * Dw *
	    ( MAX(PL_arg, 0) + MAX(Pw, 0) );
			
	  PL_arg = powf(1.0 - 0.1 * fabs(Pn), 5.0);
	  aN[index_] = N_solve_phase_OK[index_] * N_coeff[index_] * Dn *
	    ( MAX(PL_arg, 0) + MAX(-Pn, 0) );
			
	  PL_arg = powf(1.0 - 0.1 * fabs(Ps), 5.0);
	  aS[index_] = S_solve_phase_OK[index_] * S_coeff[index_] * Ds *
	    ( MAX(PL_arg, 0) + MAX(Ps, 0) );
	}

	/* Mainly for testing the two forms of the phi equation */
	dF = Fe - Fw + Fn - Fs;
	dF = phi_dF_OK ? dF : 0;
		
	aP[index_] =
	  aE[index_] + aW[index_] + aN[index_] + aS[index_] +
	  aP0 - SP * vol[index_] + dF;
	
	/* With SC_m computes arrays of SC due to 
	   internal boundaries regardless of the case 
	   (Dirichlet, Isoflux, Conjugate) */
	b[index_]  =
	  SC_vol_m[index_] + SC * vol[index_] + SPC + aP0 * phi0[index_];
		
      } /* end else if(!P_solve_phase_OK[index_]) */
	

    } /* end for I */
  } /* end for J */

  /* Conjugate heat transfer semi-implicit scheme */

  if(interface_molecular_flux_model == 6){
    compute_near_boundary_molecular_flux_experimental_SI(data);
  }

  /* Relax coefficients */
  relax_coeffs(data->phi, P_solve_phase_OK, 1, data->coeffs_phi, Nx, Ny, data->alpha_phi);
  
  return;
}

/* SET_D_PHI */
void set_D_phi(Data_Mem *data){

  int I, J, i, j;
  int index_, index_face;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  double gamma_face;

  double *Dphi_x = data->Dphi_x;
  double *Dphi_y = data->Dphi_y;

  const double *gamma_m = data->gamma_m;
  const double *fe = data->fe;
  const double *fn = data->fn;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *Delta_xE = data->Delta_xE;
  const double *Delta_yN = data->Delta_yN;
  
  /* Compute base diffusion parameters, 
     HERE IT IS ASSUMED NO VARIATION OF GAMMA WITH PHI */

  /* Dphi_x */

#pragma omp parallel for default(none) private(i, J, index_face, index_, gamma_face) \
  shared(gamma_m, fe, Delta_y, Delta_xE, Dphi_x)

  for(J=1; J<Ny-1; J++){
    for(i=0; i<nx; i++){
      index_face = J*nx + i;
      index_ = J*Nx + i;
      gamma_face = gamma_m[index_] * gamma_m[index_+1] / 
	((1 - fe[index_]) * gamma_m[index_+1] + fe[index_] * gamma_m[index_]);
      Dphi_x[index_face] = gamma_face * Delta_y[index_] / Delta_xE[index_];
    }
  }

  /* Dphi_y */

#pragma omp parallel for default(none) private(I, j, index_face, index_, gamma_face) \
  shared(gamma_m, fn, Delta_x, Delta_yN, Dphi_y)

  for(j=0; j<ny; j++){
    for(I=1; I<Nx-1; I++){
      index_face = j*Nx + I;
      index_ = j*Nx + I;
      gamma_face = gamma_m[index_] * gamma_m[index_+Nx] / 
	((1 - fn[index_]) * gamma_m[index_+Nx] + fn[index_] * gamma_m[index_]);
      Dphi_y[index_face] = gamma_face * Delta_x[index_] / Delta_yN[index_];      
    }
  }

  return;
}

/* SET_F_PHI */
void set_F_phi(Data_Mem *data){

  int I, J, i, j;
  int index_face, index_;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int nx = data->nx;
  const int ny = data->ny;

  double rho_face, cp_face;

  double *Fphi_x = data->Fphi_x;
  double *Fphi_y = data->Fphi_y;

  const double *rho_m = data->rho_m;
  const double *cp_m = data->cp_m;
  const double *Delta_x = data->Delta_x;
  const double *Delta_y = data->Delta_y;
  const double *um = data->u_at_faces;
  const double *vm = data->v_at_faces;

  /* Compute base convective parameters */
  /* Convective forces parameters 
     THESE INTERPOLATION ARE ACROSS PHASES, 
     CHECK IT OUT IF THIS WORKS */

  /* Fphi_x */

#pragma omp parallel for default(none) private(i, J, index_face, index_, rho_face, cp_face) \
  shared(rho_m, cp_m, Delta_y, um, Fphi_x)

  for(J=1; J<Ny-1; J++){
    for(i=0; i<nx; i++){
      index_face = J*nx + i;
      index_ = J*Nx + i;
      rho_face = 0.5 * (rho_m[index_] + rho_m[index_+1]);
      cp_face = 0.5 * (cp_m[index_] + cp_m[index_+1]);
      Fphi_x[index_face] = rho_face * cp_face * Delta_y[index_] * um[index_face];
    }
  }
     
  /* Fphi_y */

#pragma omp parallel for default(none) private(I, j, index_face, index_, rho_face, cp_face) \
  shared(rho_m, cp_m, Delta_x, vm, Fphi_y)

  for(j=0; j<ny; j++){
    for(I=1; I<Nx-1; I++){
      index_face = j*Nx + I;
      index_ = j*Nx + I;
      rho_face = 0.5 * (rho_m[index_] + rho_m[index_+Nx]);
      cp_face = 0.5 * (cp_m[index_] + cp_m[index_+Nx]);
      Fphi_y[index_face] = rho_face * cp_face * Delta_x[index_] * vm[index_face];
    }
  }

  return;
}

/* SOLVE_PHI_TRANSIENT */
void solve_phi_transient(Data_Mem *data){

  /* Pointer to phi coeffs function */
  void (*coeffs_phi)(Data_Mem *);

  Wins *wins = &(data->wins);
  Sigmas *sigmas = &(data->sigmas);
  
  FILE *fp = NULL;
  FILE *teg_fp = NULL;
  FILE *current_fp = NULL;
  FILE *glob_res_fp = NULL;
  
  char filename[SIZE];
  char teg_filename[SIZE];
  char current_filename[SIZE];
  char msg[SIZE];

  const char *results_dir = data->results_dir;
  const char *glob_res_filename = data->glob_res_filename;

  int convergence_OK = 0, sub_convergence_OK = 0, Tol_OK = 0;
  int it_counter = 0, sub_it_counter = 0, total_it = 0;
  int transient_counter = 1;
  int gL_convergence_OK;
  
  int max_it = data->max_it;
  int max_sub_it = data->max_sub_it;

  const int phi_flow_scheme = data->phi_flow_scheme;

  const int TEG_OK_summary = data->TEG_OK_summary;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;
  const int it_ref_norm_phi = data->it_ref_norm_phi;
  const int it_upd_coeff_phi = data->it_upd_coeff_phi;

  const int interface_flux_update_it =
    data->interface_flux_update_it;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int animation_phi_OK = data->animation_phi_OK;
  const int animation_phi_update_it = data->animation_phi_update_it;

  const int res_sigma_iter = data->res_sigma_iter;
  const int write_transient_data_OK = data->write_transient_data_OK;

  u_int64_t time_i, time_f, timer;

  double norm_phi = 1, norm_phi_ref = 1;
  double gL_diff = 1, gL_diff_ref = 1;

  const double tol_phi = data->tol_phi;
  const double gL_tol = data->gL_tol;
  const double transient_save = data->write_transient_data_interval * 60;

  double *phi = data->phi;
  double *phi0 = data->phi0;
  double *gL = data->gL;
  double *gL_calc = data->gL_calc;
  double *gL_guess = data->gL_guess;
  double *dt = data->dt;
  double *sigmas_v= data->sigmas.values;
  double *glob_norm= data->sigmas.glob_norm;

  sigmas->time_f = &(time_f);
  
  solve_phi_pre_calc_jobs(data);

  if(phi_flow_scheme == 1){
    coeffs_phi = coeffs_phi_power_law;
  }
  else if(phi_flow_scheme == 2){
    coeffs_phi = coeffs_phi_TVD;
  }
  else{
    coeffs_phi = coeffs_phi_power_law;
  }

  if(write_transient_data_OK){
    set_transient_sim_time_filename(filename, "sim_time",
				    0, results_dir);
    
    open_check_file(filename, &fp, wins, 0);
    fprintf(fp, "sim_time_var = [\n");          
    fclose(fp);

    if(TEG_OK_summary){
      set_transient_sim_time_filename(teg_filename, "teg_vars",
				      0, results_dir);
	
      open_check_file(teg_filename, &teg_fp, wins, 0);
      fprintf(teg_fp, "%% %8s %8s %8s %8s %8s %8s ...\n",
	      "t (min)", "I (mA)", "QLi (W)", "Q0i (W)", "TLi (K)", "T0i (K)");
      fprintf(teg_fp, "teg_var = [\n");	
      fclose(teg_fp);

      set_transient_sim_time_filename(current_filename, "IVP", 
				      0, results_dir);

      open_check_file(current_filename, &current_fp, wins, 0);
      fprintf(current_fp, "IVP_var = [\n");
      fclose(current_fp);
      
    }
  }

  if(!data->read_tmp_file_OK){
    dt[6] = 0;
  }
     
  /* Timer to measure it_time */
  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  /* Este es el ciclo principal de iteración */
  do{
    it_counter++;
    dt[6] = dt[6] + dt[3];

    sub_it_counter = 0;
    sub_convergence_OK = 0;
    
    norm_phi_ref = 1;
            
    if(solve_phase_change_OK){
      /* gL_diff Max difference between guess and 
	 calculated value of gL */
      gL_diff = 1;
      gL_diff_ref = 1;
      gL_convergence_OK = 0;
    }
    else{
      gL_convergence_OK = 1;
    }

    /* Store phi values */
    copy_array(phi, phi0, Nx, Ny, Nz);

    /* Saving transient data */
    if( (write_transient_data_OK) &&
	(dt[6] >= data->tsaves[2]) &&
	(dt[6] <= data->tsaves[3]) ){
      
      if( dt[6] >= data->tsaves[2] + transient_save * (transient_counter - 1) ){
	
	save_transient_data(data, filename);

	if(TEG_OK_summary){
	  /* Here compute electric current */ 
	  solve_current(data, current_filename);
	  save_transient_teg_data(data, teg_filename);
	}
	
	transient_counter++;
	
      }
    }

    /* Update coeffs before starting next outer iteration */
    if(solve_phase_change_OK){
      /* gL must be updated before average physical properties calculation */
      update_gL_guess(data);
      avg_phys_prop(data);
      compute_enthalpy_diff(data);
    }

    /* Update coefficients and interface flow */
    coeffs_phi(data);
    compute_phi_SC_at_interface(data);
    
    /* Este es el subciclo de iteración */
    do{
      sub_it_counter++;
      total_it++;

      /* Call solve_flow_jobs with proper update flag */
      if(sub_it_counter % it_upd_coeff_phi == 0){

	/* gL must be updated before average physical properties calculation */
	if(solve_phase_change_OK){
	  update_gL_guess(data);
	  avg_phys_prop(data);
	  compute_enthalpy_diff(data);
	}

	/* Update coeffs */
	coeffs_phi(data);

	/* Computation of flow at interfaces must be after an update
	   of the coefficients */
	if(sub_it_counter % interface_flux_update_it == 0){
	  compute_phi_SC_at_interface(data);
	}	
      }

      /* Solve for phi */
      norm_phi = solve_phi_jobs(data);

      /* Compute glob norm for stopping criteria */
      glob_norm[7] = compute_norm_glob_res(data->phi, &(data->coeffs_phi), Nx, Ny);
      
      if(solve_phase_change_OK){
	/* Update phase average properties */
	compute_gL_value(data, gL_calc);
	
	/* Compute glob norm for stopping criteria */
	glob_norm[8] = compute_avg_gL_diff(data);

	gL_convergence_OK = (glob_norm[8] <= gL_tol);

	/* Legacy stopping criteria based on sigmas.
	 * We are adopting for transient the comparison with glob norm instead.

	 gL_diff = max_abs_matrix_diff(gL_calc, gL_guess, Nx, Ny);

	  if(sub_it_counter == it_ref_norm_phi){
	  gL_diff_ref = (gL_diff > 1e-9) ? gL_diff : 1;
	  }
	  
	  sigmas_v[8] = gL_diff / gL_diff_ref;
	  
	  if(sigmas_v[8] < gL_tol){
	  gL_convergence_OK = 1;
	  }
	  
	  if(sub_it_counter <= it_ref_norm_phi){
	  gL_convergence_OK = 0;
	  }
	  
	*/
	
      }


      /* Legacy stopping criteria based on sigmas.
       * We are adopting for transient the comparison with glob norm instead.

      if(sub_it_counter == it_ref_norm_phi){
        norm_phi_ref = norm_phi;        
      }

      sigmas_v[7] = norm_phi / norm_phi_ref;
      Tol_OK = (sigmas_v[7] <= tol_phi) && gL_convergence_OK;

      */

      Tol_OK = (glob_norm[7] <= tol_phi) && gL_convergence_OK;
      

      if(sub_it_counter <= it_ref_norm_phi){
	Tol_OK = 0;
      }
  
      if(Tol_OK || (sub_it_counter >= max_sub_it)){
	sub_convergence_OK = 1;
      }
      
      /* Here process action upon signal raise */
      if(action_on_signal_OK){
	
	process_action_on_signal(data, &convergence_OK, &sub_convergence_OK);

	/* Update in case of modification the new values */
	max_it = data->max_it;
	max_sub_it = data->max_sub_it;

	action_on_signal_OK = 0;
        
      }

#ifdef DISPLAY_OK
      /* Back to ncurses */
      refresh();
#endif
      
    } while(!sub_convergence_OK);

    /* Update gL value */
    if(solve_phase_change_OK){
      copy_array(gL_guess, gL, Nx, Ny, Nz);
    }

    /* Compute normalized global residues */
    /* This was inside the sub iter loop (!?) under 
       if(sub_it_counter % interface_flux_update_it == 0){ */
    if(it_counter % res_sigma_iter == 0){
      
      glob_norm[7] = compute_norm_glob_res(data->phi, &(data->coeffs_phi), Nx, Ny);
      sigmas_v[7] = glob_norm[7];
      
      if(solve_phase_change_OK){
	glob_norm[8] = compute_avg_gL_diff(data);
	sigmas_v[8] = glob_norm[8];
      }
    }
    
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);
      memset(msg, '\0', sizeof(msg));
      snprintf(msg, SIZE, 
	       "solve_phi: it = "
	       "%7d; sphi = %9.3e; sgL = %9.3e", 
	       it_counter, sigmas_v[7], sigmas_v[8]);

      sigmas->it = it_counter;
      write_to_log_file(data, msg);
      
      timer_flag_OK = 0;
    }
	
    /* Updating the server */
    if(launch_server_OK &&
       (it_counter % server_update_it == 0)){
      update_server(data);
    }
      
    /* Updating the animation */
    if(animation_phi_OK &&
       (it_counter % animation_phi_update_it == 0)){
      update_anims((void *) data);	
    }
      
    time_i = time_f;
    time_f = time_f + elapsed_time(&timer);

    if(it_counter % res_sigma_iter == 0){
      sigmas->it = it_counter;
      sigmas->sub_it = sub_it_counter;

      print_phi_it_info(it_counter, max_it,
			time_i, time_f,
			sigmas,
			wins);
	  
      update_graph_window(data);
	  
      write_sfile(data);

      write_norm_glob_res(data);
    }
    
  } while((it_counter < max_it) && !convergence_OK);

  /* Save iteration */
  sigmas->phi_it = it_counter;
  solve_phi_post_calc_jobs(data);
  
  if(write_transient_data_OK){
      
    open_check_file(filename, &fp, wins, 1);
    fprintf(fp, "];\n");
    fclose(fp);

    if(TEG_OK_summary){

      open_check_file(teg_filename, &teg_fp, wins, 1);
      fprintf(teg_fp, "];\n");
      fclose(teg_fp);

      open_check_file(current_filename, &current_fp, wins, 1);
      fprintf(current_fp, "];\n");
      fclose(current_fp);

    }
      
  }
#ifdef DISPLAY_OK
  wprintw(wins->disp, "Simulation time (s) = %g\n", dt[6]);
  wrefresh(wins->disp);
#endif
  
  close_sfile(data);

  open_check_file(glob_res_filename, &glob_res_fp, wins, 1);
  fprintf(glob_res_fp, "];\n");
  fclose(glob_res_fp);  
  
  return;
}

/* SOLVE_PHI_STEADY */
void solve_phi_steady(Data_Mem *data){

  /* Pointer to phi coeffs function */
  void (*coeffs_phi)(Data_Mem *);

  Wins *wins = &(data->wins);
  Sigmas *sigmas = &(data->sigmas);
    
  char msg[SIZE];

  FILE *glob_res_fp = NULL;

  const char *glob_res_filename = data->glob_res_filename;

  int convergence_OK = 0, Tol_OK = 0;
  int it_counter = 0;
  int gL_convergence_OK;
  
  int max_it = data->max_it;

  int it_ref_norm_phi;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int phi_flow_scheme = data->phi_flow_scheme;

  const int read_tmp_file_OK = data->read_tmp_file_OK;

  const int it_upd_coeff_phi = data->it_upd_coeff_phi;

  const int interface_flux_update_it =
    data->interface_flux_update_it;
  const int solve_phase_change_OK = data->solve_phase_change_OK;
  const int launch_server_OK = data->launch_server_OK;
  const int server_update_it = data->server_update_it;
  const int animation_phi_OK = data->animation_phi_OK;
  const int animation_phi_update_it = data->animation_phi_update_it;

  const int res_sigma_iter = data->res_sigma_iter;

  u_int64_t time_i, time_f, timer;
  
  double norm_phi = 1, norm_phi_ref = 1;
  double gL_diff = 1, gL_diff_ref = 1;

  const double tol_phi = data->tol_phi;
  const double gL_tol = data->gL_tol;

  double *gL = data->gL;
  double *gL_calc = data->gL_calc;
  double *gL_guess = data->gL_guess;
  double *sigmas_v= data->sigmas.values;
  double *refs = data->sigmas.refs;

  sigmas->time_f = &(time_f);
  
  solve_phi_pre_calc_jobs(data);

  if(phi_flow_scheme == 1){
    coeffs_phi = coeffs_phi_power_law;
  }
  else if(phi_flow_scheme == 2){
    coeffs_phi = coeffs_phi_TVD;
  }
  else{
    coeffs_phi = coeffs_phi_power_law;
  }

  if(read_tmp_file_OK){
    it_ref_norm_phi = -1;
    norm_phi_ref = refs[7];
    gL_diff_ref = refs[8];
  }
  else{
    it_ref_norm_phi = data->it_ref_norm_phi;
  }
		
  /* Timer to measure it_time */
  time_i = 0;
  time_f = 0;
  elapsed_time(&timer);

  if(solve_phase_change_OK){
    /* gL_diff Max difference between guess and 
       calculated value of gL */
    gL_diff = 1;
    gL_convergence_OK = 0;
  }
  else{
    gL_convergence_OK = 1;
  }
    
  /* Este es el ciclo de iteración */  
  do{
    it_counter++;

    /* Call solve_phi_jobs with proper update flag */
    if(it_counter % it_upd_coeff_phi == 0){
      coeffs_phi(data);
    }
      
    /* Computation of flow at the interfaces 
       for the conjugate case must be accompanied with
       an update of coefficients */
    if(it_counter % interface_flux_update_it == 0){
      coeffs_phi(data);
      compute_phi_SC_at_interface(data);
    }
     
    if(solve_phase_change_OK){
      /* Update phase average properties */
      avg_phys_prop(data);
      /* For steady state the source term is due to phase change is zero
	 compute_enthalpy_diff(data); */
	
      norm_phi = solve_phi_jobs(data);
	
      compute_gL_value(data, gL_calc);
      gL_diff = max_abs_matrix_diff(gL_calc,
				    gL_guess, Nx, Ny);
      update_gL_guess(data);

      if(it_counter == it_ref_norm_phi){

	gL_diff_ref = (gL_diff > 1e-9) ? gL_diff : 1;
	refs[8] = gL_diff_ref;
      }

      sigmas_v[8] = gL_diff / gL_diff_ref;
	
      if(sigmas_v[8] < gL_tol){
	gL_convergence_OK = 1;
      }
	
      if(it_counter <= it_ref_norm_phi){
	gL_convergence_OK = 0;
      }

    }
    else{
      norm_phi = solve_phi_jobs(data);
    }
	    
    /* Para adimensionalizar los residuos y poder 
       compararlos con tol_phi apropiadamente */
    if(it_counter == it_ref_norm_phi){
      norm_phi_ref = norm_phi;
      refs[7] = norm_phi_ref;
    }
      
    sigmas_v[7] = norm_phi / norm_phi_ref;
    Tol_OK = (sigmas_v[7] <= tol_phi);

    /* Comparison with tolerances must be with 
       normalized residuals */
    if(it_counter <= it_ref_norm_phi){
      Tol_OK = 0;
    }
    
    /* Timer action */
    if(timer_flag_OK){
      // Write or do stuff here
      checkCall(save_data_in_MAT_file(data), wins);
      write_to_tmp_file(data);
      memset(msg, '\0', sizeof(msg));
      snprintf(msg, SIZE, 
	       "solve_phi: it = "
	       "%7d; sphi = %9.3e; sgL = %9.3e", 
	       it_counter, sigmas_v[7], sigmas_v[8]);

      sigmas->it = it_counter;
      write_to_log_file(data, msg);
      
      timer_flag_OK = 0;
    }
    
    /* Updating the server */
    if(launch_server_OK &&
       (it_counter % server_update_it == 0)){
      update_server(data);
    }
	
    /* Updating the animation */
    if(animation_phi_OK &&
       (it_counter % animation_phi_update_it == 0)){

      update_anims((void *) data);
	  
    }
	
    time_i = time_f;
    time_f = time_f + elapsed_time(&timer);

    if(it_counter % res_sigma_iter == 0){
      sigmas->it = it_counter;

      print_phi_it_info(it_counter, max_it,
			time_i, time_f,
			sigmas,
			wins);
	  
      update_graph_window(data);
	  
      write_sfile(data);
    }	
  
    if((Tol_OK && gL_convergence_OK) ||
       (it_counter >= max_it)){
      convergence_OK = 1;
    }
      
    /* Here process action upon signal raise */
    if(action_on_signal_OK){
          
      process_action_on_signal(data, &convergence_OK, &convergence_OK);

      /* Update in case of modification the new values */
      max_it = data->max_it;
      
      action_on_signal_OK = 0;
        
    }

#ifdef DISPLAY_OK
    /* Back to ncurses */
    refresh();
#endif
   
    /* Update gL value */
    if(solve_phase_change_OK){
      copy_array(gL_guess, gL, Nx, Ny, Nz);
    }
   
  } while(!convergence_OK);

  /* Save iteration */
  sigmas->phi_it = it_counter;
  solve_phi_post_calc_jobs(data);

  close_sfile(data);
 
  open_check_file(glob_res_filename, &glob_res_fp, wins, 1);
  fprintf(glob_res_fp, "];\n");
  fclose(glob_res_fp);  
 
  return;
}

/* SOLVE_PHI_PRE_CALC_JOBS */
void solve_phi_pre_calc_jobs(Data_Mem *data){

  /* Pointer to phi coeffs function */
  void (*coeffs_phi)(Data_Mem *);

  MATIOS *matios = &(data->matios);

  Sigmas *sigmas = &(data->sigmas);

  Wins *wins = &(data->wins);

  double *sigmas_v= data->sigmas.values;

  const int phi_flow_scheme = data->phi_flow_scheme;

  const int turb_model = data->turb_model;

  const int Nx = data->Nx;
  const int Ny = data->Ny;
  const int Nz = data->Nz;

  const int ss_OK = data->steady_state_OK;
  const int wtd_OK = data->write_transient_data_OK;
  const int tos = data->TEG_OK_summary;

  size_t dim[2], dims[4];

  if(phi_flow_scheme == 1){
    coeffs_phi = coeffs_phi_power_law;
  }
  else if(phi_flow_scheme == 2){
    coeffs_phi = coeffs_phi_TVD;
  }
  else{
    coeffs_phi = coeffs_phi_power_law;
  }

  if(wtd_OK && !ss_OK){
    dim[0] = 1;
    dim[1] = 1;
    matios->tphi = Mat_VarCreate("tphi", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->dt[6]),
				 MAT_F_DONT_COPY_DATA);

    if(tos){
      matios->current = Mat_VarCreate("current", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->teg.I),
				      MAT_F_DONT_COPY_DATA);
      matios->voltage = Mat_VarCreate("voltage", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->teg.V),
				      MAT_F_DONT_COPY_DATA);
      matios->power = Mat_VarCreate("power", MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dim, &(data->teg.P),
				    MAT_F_DONT_COPY_DATA);
    }
    
    dims[0] = Nx;
    dims[1] = Ny;
    dims[2] = Nz;
    dims[3] = 1;

    /* Setting matio struct for transient data saving */
    matios->phi = Mat_VarCreate("phi_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims,
				data->phi, MAT_F_DONT_COPY_DATA);
    
    if(data->solve_phase_change_OK){
      matios->gL = Mat_VarCreate("gL_tr", MAT_C_DOUBLE, MAT_T_DOUBLE, 4, dims,
				 data->gL, MAT_F_DONT_COPY_DATA);
    }
  }
  
  /* Reset values of sigmas structure */
  sigmas_v[0] = NAN;
  sigmas_v[1] = NAN;
  sigmas_v[2] = NAN;
  sigmas_v[3] = NAN;
  sigmas_v[4] = NAN;
  sigmas_v[5] = NAN;
  sigmas_v[6] = NAN;
  sigmas_v[7] = 1;
  sigmas_v[8] = NAN;

  sigmas->it = 0;

  /* Set ncurses graphics for phi */
  set_it_graph_sizes(wins);
  update_graph_window(data);

  /* Add turbulent conductivity contribution */
  if(turb_model){
    add_gamma_turb(data);
  }

  /* Initial coeffs computation. 
     Later is included in solve_phi_jobs */
  compute_phi_SC_at_interface(data);
  coeffs_phi(data);

  /* Write the table header for iterations without updating the window */
  table_header_phi_it(wins);
  write_sfile(data);
  write_norm_glob_res(data);

  mem_count(wins);

  return;
}

/* SOLVE_PHI_POST_CALC_JOBS */
void solve_phi_post_calc_jobs(Data_Mem *data){

  Sigmas *sigmas = &(data->sigmas);

  /* Copy sigmas values */
  sigmas->phi_values[0] = sigmas->values[7]; // phi
  sigmas->phi_values[1] = sigmas->values[8]; // gL
  
  sigmas->phi_glob_norm[0] = sigmas->glob_norm[7]; // phi
  sigmas->phi_glob_norm[1] = sigmas->glob_norm[8]; // gL

  return;

}
